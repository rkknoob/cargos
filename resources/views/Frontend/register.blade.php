<!doctype html>
<html lang="en">
    <head>
        <!-- START HEADER -->
        @include('Frontend.inc.header')
        <!-- END HEADER -->
    </head>
    <body>
        <!-- START NAVBAR -->
        @include('Frontend.inc.navbar')
        <!-- END NAVBAR -->
        <div class="wrapper">
            <!-- CONTENT -->
            <div class="container my-5 ">
                <div class="row bg-white as_boxshadow register_padding">
                    <div class="col-lg-12 ">
                        <h4 class="my-lg-5 text-center">ลงทะเบียน</h4>
                        <div>
                            <form id="AddRegister">
                                <div class="form-group row">
                                    <label for="add_perfixe_id" class="col-sm-2 col-12 profile_col-form-label">คำนำหน้า</label>
                                    <div class="col-sm-4 col-12">
                                        <select class="form-control" id="add_perfixe_id" name="perfixe_id">
                                            @foreach($Perfixes as $perfixe)
                                                <option value="{{$perfixe->id}}">{{$perfixe->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="add_firstname" class="col-sm-2 col-12 profile_col-form-label">ชื่อ</label>
                                    <div class="col-sm-4 col-12">
                                        <input type="text" class="form-control" id="add_firstname" name="firstname" placeholder="ชื่อ">
                                    </div>
                                    <label for="add_lastname" class="col-sm-2 col-12 profile_col-form-label text-lg-center">นามสกุล</label>
                                    <div class="col-sm-4 col-12">
                                        <input type="text" class="form-control" id="add_lastname" name="lastname" placeholder="นามสกุล" >
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="add_email" class="col-sm-2 profile_col-form-label">อีเมล์</label>
                                    <div class="col-sm-10">
                                        <input type="email" class="form-control" id="add_email" name="email">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="password" class="col-sm-2 profile_col-form-label">รหัสผ่าน</label>
                                    <div class="col-sm-10">
                                        <input type="password" class="form-control" id="password" name="password">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="password_confirmation" class="col-sm-2 profile_col-form-label">ยินยันรหัสผ่าน</label>
                                    <div class="col-sm-10">
                                        <input type="password" class="form-control" id="password_confirmation" name="password_confirmation">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="add_address" class="col-sm-2 profile_col-form-label">ที่อยุ่</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="add_address" name="address">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="add_province_id" class="col-sm-2 profile_col-form-label">จังหวัด</label>
                                    <div class="col-sm-4">
                                        <select class="form-control" id="add_province_id" name="province_id">
                                            <option value="0">เลือกจังหวัด</option>
                                            @foreach($Provinces as $province)
                                                <option value="{{$province->id}}">{{$province->province_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <label for="add_amphure_name" class="col-sm-2 profile_col-form-label text-lg-center">อำเภอ</label>
                                    <div class="col-sm-4">
                                        <select class="form-control" id="add_amphure_name" name="amphure_names">
                                            <option data-zipcode="" value="">เลือกอำเภอ</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="add_district" class="col-sm-2 profile_col-form-label">ตำบล</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="add_district" name="district">
                                    </div>
                                    <label for="add_zipcode" class="col-sm-2 profile_col-form-label text-lg-center">รหัสไปรษณียี</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="add_zipcode" name="zipcode">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="add_main_mobile" class="col-sm-2 profile_col-form-label">เบอร์โทรศัพท์</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="add_main_mobile" name="main_mobile">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword3" class="col-sm-2 profile_col-form-label">ประเภทสินค้า</label>
                                    <div class="col-sm-3 my-2">
                                        <label class="pay-select">
                                            <input type="radio" name="product_type_id" checked="checked" value="5">
                                            <span class="checkmark"></span>
                                            <span for="" class="pay-font5  ml-5">สินค้าทั่วไป</span>
                                        </label>
                                    </div>
                                    <div class="col-sm-3 my-2">
                                        <label class="pay-select">
                                            <input type="radio" name="product_type_id" value="6">
                                            <span class="checkmark"></span>
                                            <span for="" class="pay-font6  ml-5">สินค้าพิเศษ</span>
                                        </label>
                                    </div>
                                    <div class="col-sm-4"></div>
                                </div>
                                <div class="form-group row">
                                    <label for="confirm" class="col-sm-2 profile_col-form-label"></label>
                                    <div class="col-sm-10">
                                        <a href="#"><span>นโยบายความเป็นส่วนตัว</span></a>
                                        <span>และ</span>
                                        <a href="#"><span>เงื่อนไขการให้บริการ</span></a>
                                        <div class="form-group form-check">
                                            <input type="checkbox" class="form-check-input" id="confirm" value="T">
                                            <label class="form-check-label" for="confirm">ยอมรับเงื่อนไขการให้บริการ</label>
                                        </div>
                                    </div>
                                </div>
                                <!-- Button modal -->
                                <div class="text-center">
                                    <!-- <button type="button" class="nc-btn" data-toggle="modal" data-target="#exampleModal">
                                    ลงทะเบียน
                                </button> -->
                                <button type="submit" class="nc-btn">
                                    ลงทะเบียน
                                </button>

                                <!-- Modal -->
                                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content px-3">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">ข้อกำหนดในการสมัครสมาชิก</h5>

                                            </div>
                                            <div class="modal-body text-left">
                                                <p> 1. กรณีสมัครผ่านช่องทางที่ 1 (ช่องทางหลัก)  กรุณาแนบไฟล์หลักฐานแจ้งการชำระเงินส่งกลับมาที่สมาคมฯ ผ่านระบบลงทะเบียนออนไลน
                                                    กรณีสมัครผ่านช่องทางที่ 2  กรุณาส่งหลักฐานแจ้งการชำระเงินกลับมาที่สมาคมฯ  ทาง e-mail: tla2497@yahoo.com หรือ ทางไลน์สมาคมฯ Line ID : tla2497
                                                    2. หากสมัครในนามหน่วยงาน กรุณาใส่ชื่อบรรณารักษ์ หรือบุคคลสำหรับติดต่อกับสมาคมห้องสมุดฯ
                                                    3. หากสมัครในนามบุคคลกรุณากรอกเลขบัตรประจำตัวประชาชน และหากสมัครในนามนิติบุคคลกรุณากรอกเลขประจำตัวผู้เสียภาษีอากร เนื่องจากหากไม่กรอก ทางสำนักงานฯจะออกใบเสร็จรับเงินให้ไม่ได้
                                                    4. สมาชิกที่เปลี่ยนชื่อ นามสกุล ย้ายที่อยู่หรือสำนักงาน หรือย้ายสังกัด ต้องแจ้งให้สำนักงานสมาคมฯ ทราบ
                                                    5. หากต้องการเป็นสมาชิกชมรมที่ 2 หรือมากกว่า ชำระค่าเพิ่มในอัตราปีละ 100 บาท ตลอดชีพ 600 บาท</p>
                                                </div>
                                                <div class="modal-footer my-5 ">
                                                    <div class="col-lg-12">
                                                        <button type="button" class="nc-btn-blck" data-dismiss="modal">ยกเลิก</button>
                                                        <button type="submit" class=" nc-btn">ยืนยัน</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- START FOOTER -->
        @include('Frontend.inc.footer')
        <!-- END FOOTER -->
        <!-- START FOOTER SCRIPT -->
        @include('Frontend.inc.footer-script')
        <!-- END FOOOTER SCRIPT -->
        <script type="text/javascript">

            $('body').on('change', '#add_province_id', function(){
                var province_id = $('#add_province_id').find(':selected').val();
                $.ajax({
                    method : "POST",
                    url : url_gb+"/GetAmphure/" + province_id,
                    dataType : 'json',
                    data : {}
                }).done(function(rec){
                    var html = '<option value="">เลือกอำเภอ</option>';
                    $.each(rec, function(k, v){
                        html += '<option data-zipcode="'+v.zipcode+'" value="'+v.id+'">'+v.amphure_name+'</option>';
                    });
                    $('#add_amphure_name').html(html);
                }).fail(function(){
                    swal("system.system_alert","system.system_error","error");
                    btn.button("reset");
                });
            });

            $('body').on('change', '#add_amphure_name', function(){
                var zipcode = $('#add_amphure_name').find(':selected').attr('data-zipcode');
                $('#add_zipcode').val(zipcode);
            });

            $('#AddRegister').validate({
                errorElement: 'div',
                errorClass: 'invalid-feedback',
                focusInvalid: false,
                rules: {
                    firstname: {
                        required: true,
                    },
                    lastname: {
                        required: true,
                    },
                    main_mobile: {
                        required: true,
                    },
                    email: {
                        required: true,
                        email: true,
                    },
                    password: {
                        required: true,
                        minlength: 6,
                    },
                    password_confirmation: {
                        required: true,
                        equalTo: '#password',
                    },
                },
                messages: {
                    firstname: {
                        required: 'กรุณาระบุ',
                    },
                    lastname: {
                        required: 'กรุณาระบุ',
                    },
                    main_mobile: {
                        required: 'กรุณาระบุ',
                    },
                    email: {
                        required: 'กรุณาระบุ',
                    },
                    password: {
                        required: "กรุณาระบุ",
                        minlength: "ความยาวอย่างน้อย 6 ตัวอักษร",
                    },
                    password_confirmation: {
                        required: 'กรุณาระบุ',
                        equalTo: "ยืนยันรหัสผ่านไม่ถูกต้อง",
                    },
                },
                highlight: function (e) {
                    validate_highlight(e);
                },
                success: function (e) {
                    validate_success(e);
                },
                errorPlacement: function (error, element) {
                    validate_errorplacement(error, element);
                },
                submitHandler: function (form) {
                    /*
                    if(CKEDITOR!==undefined){
                        for ( instance in CKEDITOR.instances ){
                            CKEDITOR.instances[instance].updateElement();
                        }
                    }
                    */
                    if( $('#confirm').prop("checked") ){
                        var btn = $(form).find('[type="submit"]');
                        // var data_ar = removePriceFormat(form,$(form).serializeArray());
                        $.ajax({
                            method : "post",
                            url : url_gb+"/register",
                            dataType : 'json',
                            data : $(form).serialize()
                        }).done(function(rec){
                            btn.button("reset");
                            if(rec.status==1){
                                // TableList.api().ajax.reload();
                                // resetFormCustom(form);
                                 swal(rec.title,rec.content,"success");
                                // $('#ModalAdd').modal('hide');
                                window.location.href = url_gb;
                            }else{
                                 swal(rec.title,rec.content,"error");
                            }
                        }).fail(function(){
                            swal("system.system_alert","system.system_error","error");
                            btn.button("reset");
                        });
                    }else{
                        swal("แจ้งเตือน","กรุณากดยอมรับเงื่อนไขการให้บริการ","warning");
                    }
                },
                invalidHandler: function (form) {

                }
            });

            $('#add_province_id').select2();
            $('#add_amphure_name').select2();

        </script>
    </body>
</html>
