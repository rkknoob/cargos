<!doctype html>
<style type="text/css">
  .pagination li {
    position: relative;
    display: block;
    padding: .5rem .75rem;
    margin-left: -1px;
    color: #6e6f71;
    background-color: #f0f1f1;
    border: 0px solid #dee2e6;
    border-radius: 0px;
    margin-right: 5px;
}
</style>
<html lang="en">
  <head>
    <!-- START HEADER -->
    @include('Frontend.inc.header')
    <!-- END HEADER -->
  </head>

  <body>
  <!-- START NAVBAR -->
    @include('Frontend.inc.navbar')
    <!-- END NAVBAR -->
<div class="wrapper">

    <!-- START CONTENT -->



<!-- ------ start help_center ------------- -->

<div class="container my-5 px-0 ">
  <div class="nc-thumbnail">
      <div class="row">
        <div class="col-12 col-md-6">
          <div class="mt-2 text-md-left text-center">
            <h4 class="head-history">รายการสินค้า</h4>
          </div>
        </div>
        <div class="col-12 col-md-6">
          <form class="form-inline" id="FormSearch" action="{{ url('/list_product') }}" method="get">

            <div class="form-group input-group ml-auto">
              <label for="inputPassword6" class="list_product_pad">Search </label>
              <input type="text" class="form-control ml-3" name="search" placeholder="Search" value="{{ $search }}">
              <button class="btn bg-blue my-sm-0" type="submit" style="border-radius: 0rem;"><i class="fas fa-search text-white"></i></button>
            </div>
          </form>
        </div>
      </div>

    <!-- Start Item -->
      <div id="list-po">
      @if(! $ImportToChainas->iSEmpty())
        @foreach($ImportToChainas as $ImportToChaina)
          @php
            $date_import_day    = date_format(date_create($ImportToChaina->import_to_chaina_date),"Y-m-d");
            $date_5_day = date('d/m/Y', strtotime($date_import_day.'+8 days'));
          @endphp
          <div class="nc-card-item">
            <!-- Start Step -->
            <div class="">
              <div class="process">
                <div class="process-row">
                    <div class="process-step {{ $ImportToChaina->status_id == 1 || $ImportToChaina->status_id == 2 || $ImportToChaina->status_id == 3 || $ImportToChaina->status_id == 4 || $ImportToChaina->status_id == 5 || $ImportToChaina->status_id == 6 ? 'active' : null }}">
                      <div class="process-icon">
                          <i class="flaticon-china fa-3x hvr-pulse"></i>
                          <p>รับสินค้าเข้าโกดังจีน</p>
                          <p>{{ $ImportToChaina->import_to_chaina_date != null ? date_format(date_create($ImportToChaina->import_to_chaina_date),"d/m/Y") : null }}</p>
                          <p>{{ $ImportToChaina->import_to_chaina_date != null ? date_format(date_create($ImportToChaina->import_to_chaina_date),"H:i") : null }}</p>
                      </div>
                    </div>
                    <div class="process-step-nav {{ $ImportToChaina->status_id == 3 || $ImportToChaina->status_id == 4 || $ImportToChaina->status_id == 5 || $ImportToChaina->status_id == 6 ? 'active' : null }}">
                        <i class="nav-step fas fa-chevron-right "></i>
                    </div>
                    <div class="process-step {{ $ImportToChaina->status_id == 3 || $ImportToChaina->status_id == 4 || $ImportToChaina->status_id == 5 || $ImportToChaina->status_id == 6 ? 'active' : null }}">
                        <i class="flaticon-worldwide fa-3x hvr-pulse"></i>
                        <p>ปิดตู้ สินค้าเดินทางมาไทย</p>
                        <p>{{ $ImportToChaina->date_container != null ? date_format(date_create($ImportToChaina->date_container),"d/m/Y") : null }}</p>
                        <p>{{ $ImportToChaina->date_container != null ? date_format(date_create($ImportToChaina->date_container),"H:i") : null }}</p>
                    </div>
                    @php /*
                        <div class="process-step-nav {{ $ImportToChaina->status_id == 4 || $ImportToChaina->status_id == 5 || $ImportToChaina->status_id == 6 ? 'active' : null }}">
                            <i class="nav-step fas fa-chevron-right "></i>
                        </div>
                    <div class="process-step {{ $ImportToChaina->status_id == 4 || $ImportToChaina->status_id == 5 || $ImportToChaina->status_id == 6 ? 'active' : null }}">
                        <i class="flaticon-map fa-3x hvr-pulse"></i>
                        <p>รับสินค้าเข้าโกดังไทย</p>
                        <p>{{ $ImportToChaina->import_to_thai_date != null ? date_format(date_create($ImportToChaina->import_to_thai_date),"d/m/Y") : null }}</p>
                        <p>{{ $ImportToChaina->import_to_thai_date != null ? date_format(date_create($ImportToChaina->import_to_thai_date),"H:i") : null }}</p>
                    </div>
                    */ @endphp
                        <div class="process-step-nav {{ $ImportToChaina->status_id == 5 || $ImportToChaina->status_id == 6 ? 'active' : null }}">
                            <i class="nav-step fas fa-chevron-right"></i>
                        </div>
                     <div class="process-step {{ $ImportToChaina->status_id == 5 || $ImportToChaina->status_id == 6 ? 'active' : null }}">
                        <i class="flaticon-shipped fa-3x hvr-pulse"></i>
                        <p>จัดส่งสิ้นค้าภายในประเทศ</p>
                        <p>{{ $date_5_day }}</p>
                        <!-- <p>{{ $ImportToChaina->delivery_pos_date != null ? date_format(date_create($ImportToChaina->delivery_pos_date),"d/m/Y") : null }}</p>
                        <p>{{ $ImportToChaina->delivery_pos_date != null ? date_format(date_create($ImportToChaina->delivery_pos_date),"H:i") : null }}</p> -->
                    </div>
                        <div class="process-step-nav {{ $ImportToChaina->status_id == 6 ? 'active' : null }}">
                            <i class="nav-step fas fa-chevron-right"></i>
                        </div>
                    <div class="process-step {{ $ImportToChaina->status_id == 6 ? 'active' : null }}">
                        <i class="flaticon-box fa-3x hvr-pulse"></i>
                        <p>เสร็จสิ้น</p>
                    </div>
                </div>
              </div>
            </div>

            <!-- End Step -->

            <div class="nc-card-info">
              <div class="row">
                <div class="col-12 col-md-6 col-lg-5">
                  <div class="table-responsive">
                    <table class="tb-detail">
                      <tr>
                        <th>เลข PO :</th>
                        <td>{{ $ImportToChaina->po_no }}</td>
                      </tr>
                      <!-- <tr>
                        <th >จำนวน :</th>
                        <td>{{ number_format($ImportToChaina->qty_chaina) }} ชิ้น</td>
                      </tr> -->
                      <tr>
                        <th >ประเภทสินค้า :</th>
                        <td>{{ $ImportToChaina->product_type_name }} </td>
                      </tr>
                      <tr>
                        <th>จำนวนแพคเกจ :</th>
                        <td>
                          {{ number_format($ImportToChaina->qty_chaina) }} แพคเกจ
                        </td>
                      </tr>
                    </table>
                  </div>

                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="table-responsive">
                        <table class="tb-detail">
                            <tr>
                                <th>รหัสลูกค้า :</th>
                                <td>{{ $ImportToChaina->customer_general_code }}</td>
                            </tr>
                            <tr>
                                <th>สถานะ :</th>
                                <td>{{ $ImportToChaina->import_to_chaina_status }}</td>
                            </tr>
                            <tr>
                                <th >วันที่รับสินค้า(ไทย) :</th>
                                <td>{{ $ImportToChaina->import_to_thai_date != null ? date_format(date_create($ImportToChaina->import_to_thai_date),"d/m/Y") : '-' }}</td>
                            </tr>
                            <tr>
                                <th>จัดส่งภายในวันที่ :</th>
                                <td>
                                    <!-- {{ $ImportToChaina->delivery_slip_date != null ? date_format(date_create($ImportToChaina->delivery_slip_datev),"d/m/Y") : '-' }} -->
                                    {{ $date_5_day }}
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="col-12 col-lg-3 mt-3 text-lg-right text-center">
                    <a href="{{ url('/list_product_detail/'.$ImportToChaina->id) }}" class="nc-btn">รายละเอียด</a>
                </div>
                <div class="col-12">
                    <b>หมายเหตุ : </b>{{ $ImportToChaina->remark }}
                </div>
              </div>
            </div>
          </div>
        @endforeach
      </div>

    <!-- End Item -->


    <!-- pagination -->
      <div class="container mt-5">
        <ul class="pagination justify-content-center">
          <!-- <li class="page-item pr-1"><a class="news_page-link" href="javascript:void(0);"><i class="fas fa-angle-left"></i></a></li>
          <li class="page-item pr-1"><a class="news_page-link" href="javascript:void(0);">1</a></li>
          <li class="page-item pr-1"><a class="news_page-link" href="javascript:void(0);">2</a></li>
          <li class="page-item pr-1"><a class="news_page-link" href="javascript:void(0);">3</a></li>
          <li class="page-item pr-1"><a class="news_page-link" href="javascript:void(0);"><i class="fas fa-angle-right"></i></a></li> -->
          {{ $ImportToChainas->links() }}
        </ul>

      </div>
      @else
        <h3 style="text-align: center; margin-top: 50px;">ไม่พบข้อมูล!</h3>
      @endif
  </div>

</div>


<!-- ------ end help_center ------------- -->


    <!-- END CONTENT -->
</div>

    <!-- START FOOTER -->
    @include('Frontend.inc.footer')
    <!-- END FOOTER -->

    <!-- START FOOTER SCRIPT -->
    @include('Frontend.inc.footer-script')
    <!-- END FOOOTER SCRIPT -->


  </body>
</html>
