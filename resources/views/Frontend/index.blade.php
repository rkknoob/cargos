<!doctype html>
<html lang="en">
  <head>
    <!-- START HEADER -->
    @include('Frontend.inc.header')
    <!-- END HEADER -->
  </head>

  <body>
  <!-- START NAVBAR -->
    @include('Frontend.inc.navbar')
    <!-- END NAVBAR -->
<div class="wrapper">

    <!-- START CONTENT -->

<!-- ------login------------- -->

<div class="container my-5 animated slideInUp">
  <div class="row bg-white as_boxshadow ">
    @if(Auth::check())
      <div class="col-lg-8 ">
        <div class="login-page_frame ">
          <img class=" w-100 " src="{{ asset('assets/global/images/Banner/banner-01.jpg') }}" >
        </div>
      </div>
    @else
    <div class="col-lg-8 ">
      <div class="login-page_frame ">
        <img class=" w-100 " src="{{ asset('assets/global/images/Banner/banner-01.jpg') }}" >
      </div>
    </div>
      <div class="col-lg-4 login-page_h1-login" >
      <form id="FormLogin">
        <div class="row mx-2">
          <div class="col-12 login-page_font">
            <h2 class="text-center login-page_hade">เข้าสู่ระบบ</h2>
          </div>
          <div class="col-lg-12">
            <div class="input-group mb-lg-4 mb-3 mr-sm-2">
              <div class="input-group-prepend">
                <div class="input-group-text bg-blue"><i class="fas fa-envelope text-white"></i></div>
              </div>
              <input type="email" class="form-control " id="email" name="email" placeholder="example@mail.com" required="">
            </div>
          </div>
          <div class="col-lg-12">
            <div class="input-group mb-lg-4  mb-3 mr-sm-2">
              <div class="input-group-prepend">
                <div class="input-group-text bg-blue"><i class="fas fa-lock text-white"></i></div>
              </div>
              <input type="password" class="form-control" id="password" name="password" placeholder="Password" required="">
            </div>
          </div>
            <div class="col-12">
              <div class="login-page_dis">
                <p>หากยังไม่มีบัญชีผู้ใช้ คลิก </p><a class="login-page_link nc-btn-tran" href="{{ url('/register') }}">ลงทะเบียน</a>
              </div>
            </div>
            <div class="col-12 text-center">
              <button type="submit" class="nc-btn">เข้าสู่ระบบ</button>
            </div>
          <div class="col-12 login-page_dis-1">
            <a class="login-page-color" href="{{ url('/resetpassword') }}">ลืมรหัสผ่าน?</a>
          </div>
        </div>
      </form>
      </div>
    @endif
  </div>
</div>
<!-- ------login end------------- -->

<!-- -------crad-------------------->
<div class="container my-5 animated slideInUp">
  <div class="row bg-white login-page_pad as_boxshadow">
    <div class="col-lg-12">
      <h4 class="text-center login-page_hade mt-lg-4">ข่าวสารและกิจกรรม</h4>
    </div>
    <div class="col-lg-12">
      <hr class="login-page-hr mb-4">
    </div>
    @if(! $News->isEmpty())
      @foreach($News as $New)
        <div class="col-lg-4 col-md-6 col-12">
          <div class="news_card news_mar_10 hvr-float-shadow">
            <div class="news_frame-crad">
              <a href="{{ url('/news_detail/'.$New->id) }}"><img class="news_card-img-top" src="{{ asset($New->viewImage()) }}" alt="Card image cap"></a>
            </div>
            <div class="card-body">
              <a href="{{ url('/news_detail/'.$New->id) }}"><h5  class="news_fix_text">{{ $New->name }}</h5></a>
              <div class="news_fix_text_3">
                <p class="card-text ">{!! $New->title !!}</p>
              </div>
            </div>
          </div>
        </div>
      @endforeach
    @endif
    <div class="col-12 text-center  mt-4 mb-lg-5">
      <a href="{{ url('/news') }}" class="nc-btn ">ดูทั้งหมด</a>
    </div>
  </div>
</div>
<!-- -------crad--END--------------- -->









    <!-- END CONTENT -->
</div>

    <!-- START FOOTER -->
    @include('Frontend.inc.footer')
    <!-- END FOOTER -->

    <!-- START FOOTER SCRIPT -->
    @include('Frontend.inc.footer-script')
    <!-- END FOOOTER SCRIPT -->


  </body>
</html>
<script>

window.onload = function(){
  // กรอบรูป

 // รูป

    // .news_card-img-top {
    //   position:absolute;
    //   top:50%;
    //   left:50%;
    //   transform: translate(-50%, -50%);
    // }

    // .news_fix_text{
    //     position: relative;
    //     overflow: hidden;
    //     width: 100%;
    //     height: 48px;
    //     color: #021a95;
    //     font-weight: 600;
    // }


       $( '.news_card-img-top' ).each( function () {
           var iw = $(this).width();
           var ih = $(this).height();
           if(iw > ih){
             $(this).css({'width':'auto'});
             $(this).css({'height':100+'%'});
           }
           else if(ih > iw){
             $(this).css({'width':100+'%'});
             $(this).css({'height':'auto'});
           }
           else if(ih == iw){
             $(this).css({'width':100+'%'});
             $(this).css({'height':'auto'});
           }
        });

}

</script>
