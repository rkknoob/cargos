<!doctype html>
<html lang="en">
  <head>
    <!-- START HEADER -->
    @include('Frontend.inc.header')
    <!-- END HEADER -->
  </head>

  <body>
  <!-- START NAVBAR -->
    @include('Frontend.inc.navbar')
    <!-- END NAVBAR -->
    <div class="wrapper">
      <!-- start CONTENT -->
      <div class="container  my-5 ">
        <div class="row news_details_pad bg-white as_boxshadow">
          <div class="col-lg-12">
            <div>
              <h4 class="text-blue text-center text-lg-left text-weight-600 mt-4">{{ $News->name }}</h4> 
            </div>
            <div class="news_details_font_12 text-center text-lg-left">
              <span>เมื่อ : 12 มิถุนายน 2560</span>

              <span class="ml-5">เขียนโดย : {{ $News->AdminUser ? $News->AdminUser->firstname.' '.$News->AdminUser->lastname : null}}</span>
            </div>
          </div>
          <div class="col-lg-12 text-center">
            <div class="news_details_frame new_details_pad_img">
              <img class=" news_details_img" src="{{ asset($News->viewImage()) }}">
            </div>
          </div>
          <div class="col-lg-12">
            <div class="news_details_pad">
              {!! $News->title !!}
            </div>
          </div>
          <div class="col-lg-12">
            <div class="news_details_pad">
              {!! $News->detail !!}
            </div>
          </div>
        </div>  
      </div>
    <!-- END CONTENT -->
</div>

    <!-- START FOOTER -->
    @include('Frontend.inc.footer')
    <!-- END FOOTER -->

    <!-- START FOOTER SCRIPT -->
    @include('Frontend.inc.footer-script')
    <!-- END FOOOTER SCRIPT -->


  </body>
</html>
