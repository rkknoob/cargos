<!doctype html>
<html lang="en">
  <head>
    <!-- START HEADER -->
    @include('Frontend.inc.header')
    <!-- END HEADER -->
  </head>

  <body>
  <!-- START NAVBAR -->
    @include('Frontend.inc.navbar')
    <!-- END NAVBAR -->
<div class="wrapper">

    <!-- START CONTENT -->



<!-- ------ start contact ------------- -->

<div class="container my-5 ">
  <div class="row nc-thumbnail">
    <div class="col-lg-6 col-12  d-flex align-items-center">
      <div>
        <img class=" w-100 img-contact" src="{{ asset('assets/global/images/Banner/banner-contact.jpg') }}" >
      </div>

    </div>
    <div class="col-lg-6 col-12">
      <div class="mb-4">
        <h4 class="head-contact">ติดต่อเรา</h4>
      </div>
      <div class="info-contact ml-md-4 mb-4">
        <ul class="contact-in">
          <!-- <li class="contact-item">
            <i class="fas fa-map-marker-alt"></i>
            <label class="text-contact">8/8หมู่ 4 ซอย ถนนบางกรวย-ไทยน้อย ตำบลโสนลอย 
  อำเภอ บางบัวทอง จังหวัดนนทบุรี 11110</label>
          </li> -->
          <li class="contact-item">
            <i class="fas fa-phone"></i>
            <label class="text-contact">
              เปิดรหัสลูกค้า/สอบถามเพิ่มเติม (คุณพัด) 061-705-9994
              <br>แจ้งปัญหา/ติชม/เสนอแนะ (คุณพลู) 085-817-8800
            </label>
          </li>
          <!-- <li class="contact-item">
            <i class="fas fa-fax"></i>
            <label class="text-contact">+66-850-254-376</label>
          </li> -->
          <li class="contact-item">
            <i class="fas fa-envelope"></i>
            <label class="text-contact">Newcargo45@gmail.com</label>
          </li>
        </ul>
      </div>

      <div class="info-contact ml-md-4 mb-4">
        <div class="head-text">
          <h5 class="head-contact">สามารถติดต่อพวกเราได้ที่</h5>
        </div>
        <label style="font-weight: bold;">LineID : @newcargo (มี@)</label>
        <div class="line-contact">
          <img src="{{ asset('assets/global/images/qr_code.png') }}" class="img-fluid" alt="Line Newcargo">
        </div>
      </div>

    </div>
  </div>
</div>


<!-- ------ end contact ------------- -->


    <!-- END CONTENT -->
</div>

    <!-- START FOOTER -->
    @include('Frontend.inc.footer')
    <!-- END FOOTER -->

    <!-- START FOOTER SCRIPT -->
    @include('Frontend.inc.footer-script')
    <!-- END FOOOTER SCRIPT -->


  </body>
</html>
