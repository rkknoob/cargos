<!doctype html>
<html lang="en">
  <head>
    <!-- START HEADER -->
    @include('Frontend.inc.header')
    <!-- END HEADER -->
  </head>

  <body>
  <!-- START NAVBAR -->
    @include('Frontend.inc.navbar')
    <!-- END NAVBAR -->
<div class="wrapper">

    <!-- START CONTENT -->



<!-- ------ start contact ------------- -->

<div class="container my-5 ">
  <div class="row nc-thumbnail" style="height: 500px;">
    
    <div class="col-lg-12 col-12" style="text-align: center;"><br><br><br><br><br>
      <h4 class="head-contact">รีเซ็ตรหัสผ่าน</h4><br>
      <form id="ForgotPasswordForm">
        <div class="row">
          <div class="col-lg-3">
          </div>
          <div class="col-lg-6">
            <div class="input-group">
              <div class="input-group-prepend">
                <div class="input-group-text bg-blue"><i class="fas fa-envelope text-white"></i></div>
              </div>
              <input type="email" class="form-control" name="email" id="email" placeholder="exaple@gmail.com" required="">
            </div>
          </div>
          <div class="col-lg-12" style="text-align: center;">
            <div class="col-12 text-center">
                <button type="submit" class="nc-btn" style="margin-top: 20px;">ยืนยัน</button>
              </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>


<!-- ------ end contact ------------- -->


    <!-- END CONTENT -->
</div>

    <!-- START FOOTER -->
    @include('Frontend.inc.footer')
    <!-- END FOOTER -->

    <!-- START FOOTER SCRIPT -->
    @include('Frontend.inc.footer-script')
    <!-- END FOOOTER SCRIPT -->
    <script type="text/javascript">

          $('#ForgotPasswordForm').validate({
                errorElement: 'div',
                errorClass: 'invalid-feedback',
                focusInvalid: false,
                rules: {
                    
                    email: {
                        required: true,
                        email: true,
                    },
                },
                messages: {
                    
                    email: {
                        required: "กรุณาระบุ",
                        email: "รูปแบบอีเมลไม่ถูกต้อง!",
                    },
                },
                highlight: function (e) {
                    validate_highlight(e);
                },
                success: function (e) {
                    validate_success(e);
                },

                errorPlacement: function (error, element) {
                    validate_errorplacement(error, element);
                },
                submitHandler: function (form) {
                    var btn = $(form).find('[type="submit"]');
                    $.ajax({
                        method : "POST",
                        url : url_gb+"/resetpassword",
                        dataType : 'json',
                        data : $(form).serialize()
                    }).done(function(rec){
                        if(rec.status==1){
                            resetFormCustom(form);
                        swal({
                            title: rec.title,
                            text: rec.content,
                            type: "success",
                        }).then((result) => {
                            window.location.href = url_gb+"/login";
                        });
                        }else{
                            resetFormCustom(form);
                            swal(rec.title,rec.content,"error");
                        }
                    }).fail(function(){
                        swal("system.system_alert","system.system_error","error");
                    });
                },
                invalidHandler: function (form) {

                }
            });

        </script>

  </body>
</html>
