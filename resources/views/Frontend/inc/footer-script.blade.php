    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script type="text/javascript" src="{{ asset('assets/global/node_modules/jquery/dist/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/global/node_modules/popper.js/dist/umd/popper.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/global/node_modules/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{asset('assets/admin/vendors/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets/global/js/modal.js')}}"></script>
    <script src="{{asset('assets/global/js/validate.js')}}"></script>
    <script src="{{asset('assets/global/plugins/select2/js/select2.js')}}"></script>
    <link href="{{asset('assets/admin/vendors/sweetalert/css/sweetalert2.min.css')}}" rel="stylesheet">
    <script src="{{asset('assets/admin/vendors/sweetalert/js/sweetalert2.min.js')}}"></script>
    <script src="{{asset('assets/admin/vendors/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/admin/vendors/dataTables.bootstrap4.js')}}"></script>

    <script>

        var url_gb = '{{url('')}}';
        var asset_gb = '{{asset('')}}';

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('body').on('submit','#FormLogin',function(e){
            //console.log($(this).serialize());
            e.preventDefault();
            var btn = $(this).find('button');
            $.ajax({
              method: "POST",
              url: "{{url('/CheckLogin')}}",
              data: $(this).serialize()
            }).done(function( res ){
                if(res==0){
                    swal('เข้าสู่ระบบ','ชื่อผู้ใช้งานหรือรหัสผ่านไม่ถูกต้อง','error');
                }else{
                    window.location = "{{url('/')}}";
                }
            }).fail(function(){

            });
        });
    </script>
