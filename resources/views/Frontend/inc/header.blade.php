    <title>cargo</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="robots" content="index, follow, all">
    <meta name="author" content="Workbythai">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <meta property="og:title" content="newcargothai" />
    <meta property="og:site_name" content="http://newcargothai.com/" />
    <meta property="og:description" content="new cargo thai ระบบจัดการสินค้าออนไลน์ มีประสบการณ์ขนส่งสินค้าจากประเทศจีนมายังไทยนานนับ 10 ปี" />
    <meta property="og:type" content="article" />
    <!-- <meta property="og:image" content=" http://rodusedjingjai.com/uploads/Slide/rod_used.jpg " /> -->

    <!-- ICON -->
    <link rel="icon" type="image/png" href="{{ asset('assets/global/images/Logo/logo1.png') }}" sizes="16x16" href="" >
    <link rel="apple-touch-icon" sizes="76x76" href="">
    <!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous"> -->

    <!-- Bootstrap CSS -->
<!--     <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" /> -->

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/global/node_modules/bootstrap/dist/css/bootstrap.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/global/node_modules/font-awesome/css/fontawesome-all.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/global/vendor/flaticon/font/flaticon.css') }}">
    <!-- --ain -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/animate.css@3.5.2/animate.min.css">

    <!-- CUSTOM CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/global/css/custom.css') }}">

    <!-- GOOGLE FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Kanit:200,300,500,700" rel="stylesheet">
    <!-- -hover-- -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/global/vendor/animhover/css/hover.css') }}">
    <link rel="stylesheet" href="{{asset('assets/global/plugins/select2/css/select2.css')}}" />
