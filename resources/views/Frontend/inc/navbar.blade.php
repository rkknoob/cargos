<!-- Image and text -->

<!-- <a class="navbar-logo" href="#"></a> -->
<div class="w-100" style="background-color:white;border-bottom: 6px solid #001a95;">
<div class="container ">
    <div class="navbar-frame ">
       <a  href="{{ url('/') }}">
         <img class="navbar-logo" src="{{ asset('assets/global/images/Logo/Logo_SSquare-2.png') }}">
       </a>    
    </div>



   <nav class="navbar-inverse nav-upper ">
      @if(Auth::check())
        <a href="{{ url('/logout') }}" class="float-right nc-btn d-lg-table d-none ml-2">ออกจากระบบ</a>
      @else
        <a href="{{ url('/') }}" class="float-right nc-btn d-lg-table d-none ml-2">เข้าสู่ระบบ</a>
      @endif
        <ul class="nav navbar-upper d-lg-table d-none  mt-xl-2">
          <li><i class="fas fa-envelope mr-1 nav-pad"></i>Newcargo45@gmail.com
          </li>
          <li><i class="fas fa-phone mr-1 nav-pad"></i>061-705-9994
          </li>
          <li><i class="fab fa-line mr-1 nav-pad "></i>@newcargo (มี@)
          </li>
        </ul>
    </nav> 

    <nav class="navbar navbar-expand-lg navbar-light bg-white ">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto navbar-768">
            <?php $link = $_SERVER['HTTP_HOST'] .$_SERVER['REQUEST_URI'];

      ?>
          <li class="nav-item ">
            <a class="nav-link  oop  <?php if($link == 'localhost/cargo/about'){echo 'active';} ?> nav-border-right" href="{{ url('/about') }}">เกี่ยวกับเรา</a>
          </li>
          <li class="nav-item">
            <a class="nav-link oop <?php if($link == 'localhost/cargo/news'){echo 'active';} ?> nav-border-right" href="{{ url('/news') }}">ข่าวสารและกิจกรรม</a>
          </li>
          @if(Auth::check())
            <li class="nav-item">
              <a class="nav-link oop <?php if($link == 'localhost/cargo/list_product'){echo 'active';} ?> nav-border-right" href="{{ url('/list_product') }}">รายการสินค้า</a>
            </li>
            <li class="nav-item">
              <a class="nav-link oop <?php if($link == 'localhost/cargo/history'){echo 'active';} ?> nav-border-right" href="{{ url('/history') }}">ประวัติการทำรายการ</a>
            </li>
            <li class="nav-item">
              <a class="nav-link oop <?php if($link == 'localhost/cargo/profile'){echo 'active';} ?> nav-border-right" href="{{ url('/profile') }}">ข้อมูลส่วนตัว</a>
            </li>
          @endif
          <li class="nav-item">
            <a class="nav-link oop <?php if($link == 'localhost/cargo/contact'){echo 'active';} ?> nav-border-right" href="{{ url('/contact') }}">ติดต่อเรา</a>
          </li>
        </ul>

         
            <ul class="nav navbar-upper d-lg-none d-inline-block nav-768-rep">
              <li><i class="fas fa-envelope mr-2"></i>new_cargo@gmail.com</li>
              <li><i class="fas fa-phone mr-2"></i>02-123-4567</li>
              <li><i class="fab fa-line mr-2"></i>@New_cargo</li> 
            </ul>  

            <ul class="nav-btn-rep">
                <li>
                    <a href="{{ url('/') }}" class="float-left nc-btn d-lg-none d-inline-block">เข้าสู่ระบบ</a>
                </li>
            </ul>

            <!-- <button type="button" class="float-right btn-nar-login  d-lg-none d-inline-block">เข้าสู่ระบบ</button> -->

      </div>
    </nav>
    


</div> 
</div>


