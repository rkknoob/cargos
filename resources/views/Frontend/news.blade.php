<!doctype html>
<style type="text/css">
  .pagination li {
    position: relative;
    display: block;
    padding: .5rem .75rem;
    margin-left: -1px;
    color: #6e6f71;
    background-color: #f0f1f1;
    border: 0px solid #dee2e6;
    border-radius: 0px;
    margin-right: 5px;
}
</style>
<html lang="en">
  <head>
    <!-- START HEADER -->
    @include('Frontend.inc.header')
    <!-- END HEADER -->
  </head>

  <body>
  <!-- START NAVBAR -->
    @include('Frontend.inc.navbar')
    <!-- END NAVBAR -->
<div class="wrapper">

     <!-- START CONTENT -->

    <div class="container my-5 ">
      <div class="row bg-white news_pad as_boxshadow">
        <div class="col-lg-12">
          <h4 class="text-center text-weight-600 mt-lg-4">ข่าวสารและกิจกรรม</h4>
        </div>
        <div class="col-lg-12">
          <hr class="news-hr mb-4">
        </div>
        @if(!$News->isEmpty())
          @foreach($News as $New)
            <div class="col-lg-4 col-md-6 col-12">
              <div class="news_card news_mar_10 hvr-float-shadow">
                <div class="news_frame-crad">
                  <a href="{{ url('news_detail/'.$New->id) }}"><img class="news_card-img-top" src="{{ asset($New->viewImage()) }}" alt="Card image cap"></a>
                </div>
                <div class="card-body">
                  <a href="{{ url('news_detail/'.$New->id) }}"><h5  class="news_fix_text">{{ $New->name }}</h5></a>
                  <div class="news_fix_text_3">
                    <p class="card-text ">{{ $New->title }}</p>
                  </div>
                </div>
              </div>
            </div>
          @endforeach
        @endif
        

        <div class="container mt-5">                  
          <ul class="pagination justify-content-center">
            <!-- <li class="page-item pr-1"><a class="news_page-link" href="javascript:void(0);"><i class="fas fa-angle-left"></i></a></li>
            <li class="page-item pr-1"><a class="news_page-link" href="javascript:void(0);">1</a></li>
            <li class="page-item pr-1"><a class="news_page-link" href="javascript:void(0);">2</a></li>
            <li class="page-item pr-1"><a class="news_page-link" href="javascript:void(0);">3</a></li>
            <li class="page-item pr-1"><a class="news_page-link" href="javascript:void(0);"><i class="fas fa-angle-right"></i></a></li> -->
            {{ $News->links() }}
          </ul>
        </div>  
      </div>   
    </div>













    <!-- END CONTENT -->
</div>

    <!-- START FOOTER -->
    @include('Frontend.inc.footer')
    <!-- END FOOTER -->

    <!-- START FOOTER SCRIPT -->
    @include('Frontend.inc.footer-script')
    <!-- END FOOOTER SCRIPT -->


  </body>
</html>
<script>

window.onload = function(){ 
  // กรอบรูป

 // รูป

    // .news_card-img-top {
    //   position:absolute;
    //   top:50%;
    //   left:50%;
    //   transform: translate(-50%, -50%);
    // }

    // .news_fix_text{
    //     position: relative;
    //     overflow: hidden;
    //     width: 100%;
    //     height: 48px;
    //     color: #021a95;
    //     font-weight: 600;
    // }


       $( '.news_card-img-top' ).each( function () { 
           var iw = $(this).width(); 
           var ih = $(this).height(); 
           if(iw > ih){ 
             $(this).css({'width':'auto'}); 
             $(this).css({'height':100+'%'}); 
           } 
           else if(ih > iw){ 
             $(this).css({'width':100+'%'}); 
             $(this).css({'height':'auto'}); 
           } 
           else if(ih == iw){ 
             $(this).css({'width':100+'%'}); 
             $(this).css({'height':'auto'}); 
           } 
        }); 
 
} 
 
</script>












