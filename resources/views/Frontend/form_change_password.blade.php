<!doctype html>
<html lang="en">
  <head>
    <!-- START HEADER -->
    @include('Frontend.inc.header')
    <!-- END HEADER -->
  </head>

  <body>
  <!-- START NAVBAR -->
    @include('Frontend.inc.navbar')
    <!-- END NAVBAR -->
<div class="wrapper">

    <!-- START CONTENT -->



<!-- ------ start contact ------------- -->

<div class="container my-5 ">
  <div class="row nc-thumbnail" style="height: 500px;">
    
    <div class="col-lg-12 col-12" style="text-align: center;"><br><br><br><br><br>
      <h4 class="head-contact">รีเซ็ตรหัสผ่านใหม่</h4><br>
      <form id="ResetPasswordForm">
        <div class="form-group row">
            <label for="new_password" class="col-sm-4 profile_col-form-label profile_font600">อีเมล</label>
            <div class="col-sm-8">
                <input type="email" class="form-control" name="email" id="email" value="{{ $User ? $User->email : null }}" readonly="">
            </div>
        </div>
        <div class="form-group row">
            <label for="new_password" class="col-sm-4 profile_col-form-label profile_font600">รหัสผ่านใหม่</label>
            <div class="col-sm-8">
                <input type="password" class="form-control" id="new_password" name="new_password" placeholder="รหัสผ่านใหม่">
            </div>
        </div>
        <div class="form-group row">
            <label for="confirm_password" class="col-sm-4 profile_col-form-label profile_font600">ยืนยันรหัสผ่านใหม่</label>
            <div class="col-sm-8">
                <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="ยืนยันรหัสผ่านใหม่">
            </div>
        </div>
       
        <div class="row">
          <div class="col-lg-12" style="text-align: center;">
            <div class="col-12 text-center">
                <button type="submit" class="nc-btn" style="margin-top: 20px;">ยืนยัน</button>
              </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>


<!-- ------ end contact ------------- -->


    <!-- END CONTENT -->
</div>

    <!-- START FOOTER -->
    @include('Frontend.inc.footer')
    <!-- END FOOTER -->

    <!-- START FOOTER SCRIPT -->
    @include('Frontend.inc.footer-script')
    <!-- END FOOOTER SCRIPT -->
    <script type="text/javascript">

          $('#ResetPasswordForm').validate({
                errorElement: 'div',
                errorClass: 'invalid-feedback',
                focusInvalid: false,
                rules: {
                    new_password: {
                        required: true,
                        minlength: 6,
                    },
                    password_confirmation: {
                        required: true,
                        equalTo: '#new_password',
                    },
                },
                messages: {
                    new_password: {
                        required: "กรุณาระบุ",
                        minlength: "ความยาวอย่างน้อย 6 ตัวอักษร",
                    },
                    password_confirmation: {
                        required: 'กรุณาระบุ',
                        equalTo: "ยืนยันรหัสผ่านไม่ถูกต้อง",
                    },
                },
                highlight: function (e) {
                    validate_highlight(e);
                },
                success: function (e) {
                    validate_success(e);
                },

                errorPlacement: function (error, element) {
                    validate_errorplacement(error, element);
                },
                submitHandler: function (form) {
                    var btn = $(form).find('[type="submit"]');
                    $.ajax({
                        method : "POST",
                        url : url_gb+"/resetpassword/update",
                        dataType : 'json',
                        data : $(form).serialize()
                    }).done(function(rec){
                        if(rec.status==1){
                            resetFormCustom(form);
                        swal({
                            title: rec.title,
                            text: rec.content,
                            type: "success",
                        }).then((result) => {
                            window.location.href = url_gb+"/";
                        });
                        }else{
                            resetFormCustom(form);
                            swal(rec.title,rec.content,"error");
                        }
                    }).fail(function(){
                        swal("system.system_alert","system.system_error","error");
                    });
                },
                invalidHandler: function (form) {

                }
            });

        </script>

  </body>
</html>
