<!doctype html>
<html lang="en">
  <head>
    <!-- START HEADER -->
    @include('Frontend.inc.header')
    <!-- END HEADER -->
  </head>

  <body>
  <!-- START NAVBAR -->
    @include('Frontend.inc.navbar')
    <!-- END NAVBAR -->
<div class="wrapper">

    <!-- START CONTENT -->



<!-- ------ start help_center ------------- -->
@if($ImportToChaina)
  @php
    $date_import_day = date_format(date_create($ImportToChaina->import_to_chaina_date),"Y-m-d");
    $date_5_day = date('d/m/Y', strtotime($date_import_day.'+8 days'));
  @endphp
  <div class="container my-5 px-0 ">
    <div class="nc-thumbnail">
        <div class="row">
          <div class="col-12">
            <div class="mt-2 text-md-left text-center">
              <h4 class="head-history">รายละเอียดรายการสินค้า</h4>
            </div>
          </div>
        </div>

      <!-- Start Item -->
        <div class="nc-card-item">

          <!-- Start Step -->
              <div class="">
                <div class="process">
                  <div class="process-row">
                      <div class="process-step {{ $ImportToChaina->status_id == 1 || $ImportToChaina->status_id == 2 || $ImportToChaina->status_id == 3 || $ImportToChaina->status_id == 4 || $ImportToChaina->status_id == 5 || $ImportToChaina->status_id == 6 ? 'active' : null }}">
                        <div class="process-icon">
                            <i class="flaticon-china fa-3x hvr-pulse"></i>
                            <p>รับสินค้าเข้าโกดังจีน</p>
                            <p>{{ $ImportToChaina->import_to_chaina_date != null ? date_format(date_create($ImportToChaina->import_to_chaina_date),"d/m/Y") : null }}</p>
                            <p>{{ $ImportToChaina->import_to_chaina_date != null ? date_format(date_create($ImportToChaina->import_to_chaina_date),"H:i") : null }}</p>
                        </div>
                      </div>
                          <div class="process-step-nav {{ $ImportToChaina->status_id == 2 || $ImportToChaina->status_id == 3 || $ImportToChaina->status_id == 4 || $ImportToChaina->status_id == 5 || $ImportToChaina->status_id == 6 ? 'active' : null }}">
                              <i class="nav-step fas fa-chevron-right "></i>
                          </div>
                      <div class="process-step {{ $ImportToChaina->status_id == 2 || $ImportToChaina->status_id == 3 || $ImportToChaina->status_id == 4 || $ImportToChaina->status_id == 5 || $ImportToChaina->status_id == 6 ? 'active' : null }}">
                          <i class="flaticon-worldwide fa-3x hvr-pulse"></i>
                          <p>ปิดตู้ สินค้าเดินทางมาไทย</p>
                          <p>{{ $ImportToChaina->date_container != null ? date_format(date_create($ImportToChaina->date_container),"d/m/Y") : null }}</p>
                          <p>{{ $ImportToChaina->date_container != null ? date_format(date_create($ImportToChaina->date_container),"H:i") : null }}</p>
                      </div>
                          <div class="process-step-nav {{ $ImportToChaina->status_id == 3 || $ImportToChaina->status_id == 4 || $ImportToChaina->status_id == 5 || $ImportToChaina->status_id == 6 ? 'active' : null }}">
                              <i class="nav-step fas fa-chevron-right "></i>
                          </div>
                      <div class="process-step {{ $ImportToChaina->status_id == 3 || $ImportToChaina->status_id == 4 || $ImportToChaina->status_id == 5 || $ImportToChaina->status_id == 6 ? 'active' : null }}">
                          <i class="flaticon-map fa-3x hvr-pulse"></i>
                          <p>รับสินค้าเข้าโกดังไทย</p>
                          <p>{{ $ImportToChaina->import_to_thai_date != null ? date_format(date_create($ImportToChaina->import_to_thai_date),"d/m/Y") : null }}</p>
                          <p>{{ $ImportToChaina->import_to_thai_date != null ? date_format(date_create($ImportToChaina->import_to_thai_date),"H:i") : null }}</p>
                      </div> 
                          <div class="process-step-nav {{ $ImportToChaina->status_id == 5 || $ImportToChaina->status_id == 6 ? 'active' : null }}">
                              <i class="nav-step fas fa-chevron-right"></i>
                          </div>
                       <div class="process-step {{ $ImportToChaina->status_id == 5 || $ImportToChaina->status_id == 6 ? 'active' : null }}">
                          <i class="flaticon-shipped fa-3x hvr-pulse"></i>
                          <p>จัดส่งสิ้นค้าภายในประเทศ</p>
                          <p>{{ $date_5_day }}</p>
                          <!-- <p>{{ $ImportToChaina->delivery_pos_date != null ? date_format(date_create($ImportToChaina->delivery_pos_date),"d/m/Y") : null }}</p>
                          <p>{{ $ImportToChaina->delivery_pos_date != null ? date_format(date_create($ImportToChaina->delivery_pos_date),"H:i") : null }}</p> -->
                      </div> 
                          <div class="process-step-nav {{ $ImportToChaina->status_id == 6 ? 'active' : null }}">
                              <i class="nav-step fas fa-chevron-right"></i>
                          </div>
                      <div class="process-step {{ $ImportToChaina->status_id == 6 ? 'active' : null }}">
                          <i class="flaticon-box fa-3x hvr-pulse"></i>
                          <p>เสร็จสิ้น</p>
                          <p>{{ $ImportToChaina->delivery_pos_success_date != null ? date_format(date_create($ImportToChaina->delivery_pos_success_date),"d/m/Y") : null }}</p>
                          <p>{{ $ImportToChaina->delivery_pos_success_date != null ? date_format(date_create($ImportToChaina->delivery_pos_success_date),"H:i") : null }}</p>
                      </div>
                  </div>
                </div>
              </div>


          <!-- End Step -->

          <div class="nc-card-info mb-4">
            <div class="row">
              <div class="col-12 col-md-6">
                <div class="table-responsive">
                  <table class="tb-detail">
                        <tr>
                          <th>เลข PO :</th>
                          <td>{{ $ImportToChaina->po_no }}</td>
                        </tr>
                        <!-- <tr>
                          <th >จำนวน :</th>
                          <td>{{ number_format($ImportToChaina->qty_chaina) }} ชิ้น</td>
                        </tr> -->
                        <tr>
                          <th >ประเภทสินค้า :</th>
                          <td>{{ $ImportToChaina->product_type_name }} </td>
                        </tr>
                        <tr>
                          <th>จำนวนแพคเกจ :</th>
                          <td>
                            {{ number_format($ImportToChaina->qty_chaina) }} แพคเกจ
                          </td>
                        </tr>
                      </table>
                </div>
              </div>
              <div class="col-12 col-md-6">
                <div class="table-responsive">
                  <table class="tb-detail">
                    <tr>
                      <th>รหัสลูกค้า :</th>
                      <td>{{ $ImportToChaina->customer_general_code }}</td>
                    </tr>
                    <tr>
                      <th>สถานะ :</th>
                      <td>{{ $ImportToChaina->import_to_chaina_status }}</td>
                    </tr>
                    <tr>
                      <th >วันที่รับสินค้า(ไทย) :</th>
                      <td>{{ $ImportToChaina->import_to_thai_date != null ? date_format(date_create($ImportToChaina->import_to_thai_date),"d/m/Y") : '-' }}</td>
                    </tr>
                    <tr>
                      <th>จัดส่งภายในวันที่ :</th>
                      <td>
                        <!-- {{ $ImportToChaina->delivery_pos_date != null ? date_format(date_create($ImportToChaina->delivery_pos_date),"d/m/Y") : '-' }} -->
                        {{ $date_5_day }}
                      </td>
                    </tr>
                  </table>
                </div>
              </div>
            </div>
          </div>

          <div class="nc-card-info mb-4">
            <div class="row">
              <div class="col-12 col-md-6">
                <div class="table-responsive">
                  <table class="tb-detail">
                    <tr>
                      <th>รหัสลูกค้า :</th>
                      <td>{{ $ImportToChaina->customer_general_code }}</td>
                    </tr>
                    <tr>
                      <th>เบอร์โทรศัพท์ :</th>
                      <td>{{ $ImportToChaina->main_mobile }}</td>
                    </tr>
                  </table>
                </div>
              </div>
              <div class="col-12 col-md-6">
                <div class="table-responsive">
                  <table class="tb-detail">
                    <tr>
                      <th width="160px">ที่อยู่จัดส่ง :</th>
                      <td>{{ $ImportToChaina->address ? $ImportToChaina->address.' '.$ImportToChaina->amphure_name.' '.$ImportToChaina->province_name.' '.$ImportToChaina->zipcode : '-' }}</td>
                    </tr>
                  </table>
                </div>
              </div>
            </div>
          </div>

          <!-- <div class="alert alert-warning" role="alert">
            <span>*หมายเหตุ : </span>
          </div> -->

        </div>
      <!-- End Item -->

      <!-- Start Item -->
        <div class="nc-card-item">
          <div class="table-responsive">
            <table class="table table-bordered text-center">
              <thead>
                <tr>
                  <th scope="col" class="no-wrap">Description of Good</th>
                  <th scope="col" class="no-wrap">Package</th>
                  <th scope="col" class="no-wrap">จำนวนสินค้าในกล่องตามที่ร้านค้าจีนแจ้ง</th>
                  <th scope="col" class="no-wrap">KG</th>
                  <th scope="col" class="no-wrap">Cubic Meter</th>
                  <th scope="col" class="no-wrap">Remark</th>
                </tr>
              </thead>
              <tbody>
                @if(! $ProductImportToChainas->isEmpty())
                  @foreach($ProductImportToChainas as $ProductImportToChaina)
                    <tr>
                      <td class="text-left">{{ $ProductImportToChaina->product_name }}</td>
                      <td>{{ number_format($ProductImportToChaina->qty_chaina) }}</td>
                      <td>{{ $ProductImportToChaina->pcs != '' ? number_format($ProductImportToChaina->pcs) : 'NA' }}</td>
                      <td>{{ number_format($ProductImportToChaina->weight_all,2) }}</td>
                      <td>{{ number_format($ProductImportToChaina->cubic,2) }}</td>
                      <td>{{ $ProductImportToChaina->transport_type_name }}</td>
                    </tr>
                  @endforeach
                @endif
              </tbody>
            </table>
          </div>
          
        </div>
      <!-- End Item -->  

      <div class="row">
        <div class="col-12 col-md-10 text-md-left text-center">
          <b>หากมีปัญหาหรือข้อสงสัย กรุณาติดต่อ (คุณสมร) : 088-222-3333 หรือ LineID : @New_cargo</b>
        </div>
        <div class="col-12 col-md-2 mt-4 mt-lg-0 text-lg-right text-center">
          <a href="list_product.php" class="nc-btn">กลับ</a>
        </div>
      </div>
    </div>
  </div>
@endif




<!-- ------ end help_center ------------- -->


    <!-- END CONTENT -->
</div>

    <!-- START FOOTER -->
    @include('Frontend.inc.footer')
    <!-- END FOOTER -->

    <!-- START FOOTER SCRIPT -->
    @include('Frontend.inc.footer-script')
    <!-- END FOOOTER SCRIPT -->


  </body>
</html>
