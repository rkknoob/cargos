<!doctype html>
<html lang="en">
    <head>
        <!-- START HEADER -->
        @include('Frontend.inc.header')
        <!-- END HEADER -->
    </head>
    <body>
        <!-- START NAVBAR -->
        @include('Frontend.inc.navbar')
        <!-- END NAVBAR -->
        <div class="wrapper">
            <!--  CONTENT -->
            <div class="container my-5 ">
                <div class="row bg-white as_boxshadow profile_padding">
                    <!--
                        <div class="col-lg-4 col-12 mt-4">
                            <div class="portfolio-item"  >
                                <div class="portfolio-thumb profile_fix_frame" data-toggle="modal" data-target="#exampleModalCenter">
                                    <img class="w-100" src="https://pbs.twimg.com/profile_images/926061805645979650/bRdX5oxx_400x400.jpg">
                                    <div class="overlay-p">
                                    <a href="#" data-gal="prettyPhoto">
                                    <i class="fa fa-arrows-alt fa-2x"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    -->
                    <!-- Modal
                        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <img class="w-100" src="https://pbs.twimg.com/profile_images/926061805645979650/bRdX5oxx_400x400.jpg">
                                    </div>
                                </div>
                            </div>
                        </div>
                    -->
                    <div class="col-lg-12   col-12 mt-4">
                        <div class="text-lg-left text-center">
                            <h4 class="text-weight-600 mb-4">ข้อมูลส่วนตัว</h4>
                        </div>
                        <form id="EditProfile">
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 profile_col-form-label profile_font600">คำนำหน้าชื่อ</label>
                                <div class="col-sm-10">
                                    <select id="edit_prefix_id" name="prefix_id" class="form-control">
                                        @if(!$Prefixs->isEmpty())
                                            @foreach($Prefixs as $Prefix)
                                                <option value="{{ $Prefix->id }}" {{ $Prefix->id == $User->prefix_id ? 'selected' : null }} >{{ $Prefix->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-2 profile_col-form-label profile_font600">ชื่อ</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="add_firstname" name="firstname" placeholder="ชื่อ" value="{{ $User->firstname }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-2 profile_col-form-label profile_font600">นามสกุล</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="add_lastname" name="lastname" placeholder="นามสุกล" value="{{ $User->lastname }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-2 profile_col-form-label profile_font600">อีเมล</label>
                                <div class="col-sm-10">
                                    <label for="inputPassword3" class=" profile_col-form-label">{{ $User->email }}</label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-2 profile_col-form-label profile_font600">รหัสผ่าน</label>
                                <div class="col-sm-4">
                                    <a href="#" data-toggle="modal" data-target="#checkpassword"><i class="fas fa-lock mr-3 "></i>เปลี่ยนรหัสผ่าน
                                    </a>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-2 profile_col-form-label profile_font600">เบอร์โทรศัพท์</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="add_main_mobile" name="main_mobile" placeholder="xxx-xxx-xxxx" value="{{ $User->main_mobile }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-2 profile_col-form-label profile_font600">ที่อยู่</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="add_address" name="address" placeholder="ที่อยุ่" value="{{ $User->address }}">
                                </div>
                            </div>
                            <!--
                                <div class="form-group row">
                                    <label for="inputPassword3" class="col-sm-2 profile_col-form-label profile_font600">ตำบล</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="inputPassword3" placeholder="ตำบล" value="ตำบล.6">
                                    </div>
                                </div>
                            -->
                            <div class="form-group row">
                                <label for="add_province_id" class="col-sm-2 profile_col-form-label profile_font600">จังหวัด</label>
                                <div class="col-sm-4">
                                    <select id="add_province_id" name="province_id" class="form-control">
                                        @if(!$Provinces->isEmpty())
                                            @foreach($Provinces as $Province)
                                                <option value="{{ $Province->id }}" {{ $Province->id == $User->province_id ? 'selected' : null }} >{{ $Province->province_name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="add_amphure_name" class="col-sm-2 profile_col-form-label  profile_font600">อำเภอ</label>
                                <div class="col-sm-4">
                                    <select id="add_amphure_name" name="amphur_id" class="form-control">
                                        <option>เลือกอำเภอ</option>
                                        @if(!$Amphures->isEmpty())
                                            @foreach($Amphures as $Amphure)
                                                <option value="{{ $Amphure->id }}" {{ $Amphure->id == $User->amphur_id ? 'selected' : null }} >{{ $Amphure->amphure_name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="add_zipcode" class="col-sm-2 profile_col-form-label profile_font600 ">รหัสไปรษณีย์</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="add_zipcode" placeholder="รหัสไปรษณียี" value="11200" readonly="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="add_zipcode" class="col-sm-2 profile_col-form-label profile_font600 ">เพิ่มที่อยูจัดส่ง</label>
                                <div class="col-sm-4">
                                    <a data-loading-text="<i class=\'fa fa-refresh fa-spin\'></i>" class="btn btn-xs btn-info btn-condensed btn-address btn-tooltip" data-rel="tooltip" data-id="" title="เพิ่มที่อยูจัดส่ง">
                                        <i class="ace-icon fa fa-home bigger-120"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 my-5 text-lg-left text-center">
                            <button class="nc-btn" type="submit">บันทึก</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="checkpassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form id="CheckPassword">
                    <div class="modal-content px-3">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">เปลี่ยนรหัสผ่าน</h5>
                        </div>
                        <div class="modal-body text-left">
                            <div class="form-group row">
                                <label for="current_password" class="col-sm-4 profile_col-form-label profile_font600">รหัสผ่านเดิม</label>
                                <div class="col-sm-8">
                                    <input type="password" class="form-control" id="old_password" name="old_password" placeholder="รหัสผ่านเดิม">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="new_password" class="col-sm-4 profile_col-form-label profile_font600">รหัสผ่านใหม่</label>
                                <div class="col-sm-8">
                                    <input type="password" class="form-control" id="new_password" name="new_password" placeholder="รหัสผ่านใหม่">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="confirm_password" class="col-sm-4 profile_col-form-label profile_font600">ยืนยันรหัสผ่านใหม่</label>
                                <div class="col-sm-8">
                                    <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="ยืนยันรหัสผ่านใหม่">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="col-lg-12" style="text-align: center;">
                                <button type="button" class="nc-btn-blck" data-dismiss="modal">ยกเลิก</button>
                                <button type="submit" class=" nc-btn">ยืนยัน</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div class="modal" id="ModalAddress" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">จัดการที่อยู่{{$title_page or 'ข้อมูลใหม่'}} <label id="car_mark_name" data-userid=""></label></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <form id="FormAddAddress">
                            <input type="hidden" id="edit_address_id" name="id">
                            <input type="hidden" id="edit_user_address_id" name="user_id" value="{{$User->id}}">

                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="modal_name">ชื่อ</label>
                                        <input type="text" class="form-control" name="name" id="modal_name" required="" placeholder="ชื่อ">
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <label for="modal_address">ที่อยู่</label>
                                        <input type="text" class="form-control" name="address" id="modal_user_address" required="" placeholder="ที่อยู่">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="modal_province_id" class="col-12">จังหวัด</label>
                                        <select type="text" class="select2 form-control" name="province_id" id="modal_province_id">
                                            <option value="">เลือกจังหวัด</option>
                                            @if(!$Provinces->isEmpty())
                                                @foreach($Provinces as $Province)
                                                    <option value="{{$Province->id}}">{{$Province->province_name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="modal_amphur_id" class="col-12">อำเภอ</label>
                                        <select type="text" class="select2 form-control" name="amphur_id" id="modal_amphur_id">
                                            <option value="">เลือกอำเภอ</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="modal_zipcode">รหัสไปรษณีย์</label>
                                        <input type="text" class="form-control" name="zipcode" id="modal_zipcode" placeholder="รหัสไปรษณีย์" readonly>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12" style="text-align: center; margin-bottom: 50px;">
                                <button type="button" class="btn btn-danger reset-form-address">ยกเลิก</button>
                                <button type="submit" class="btn btn-primary">บันทึก</button>
                            </div>
                        </form>
                        <div class="material-datatables">
                            <div class="table-responsive">
                                <table id="TableListAddress" class="table table-striped table-no-bordered table-hover" style="width:100%;cellspacing:0">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>ชื่อ</th>
                                            <th>ที่อยู่</th>
                                            <th>อำเภอ</th>
                                            <th>จังหวัด</th>
                                            <th>รหัสไปรษณีย์</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                    </div>
                </div>
            </div>
        </div>

        <!-- END CONTENT -->

        <!-- START FOOTER -->
        @include('Frontend.inc.footer')
        <!-- END FOOTER -->
        <!-- START FOOTER SCRIPT -->
        @include('Frontend.inc.footer-script')
        <!-- END FOOOTER SCRIPT -->

        <script type="text/javascript">
            $('body').on('change', '#add_province_id', function(){
                var province_id = $('#add_province_id').find(':selected').val();
                var html = '';
                $.ajax({
                    method : "POST",
                    url : url_gb+"/GetAmphure/" + province_id,
                    dataType : 'json',
                    data : {}
                }).done(function(rec){
                    html += '<option value="">อำเภอ</option>';
                    $.each(rec, function(k, v){
                        html += '<option data-zipcode="'+v.zipcode+'" value="'+v.id+'">'+v.amphure_name+'</option>';
                    });
                    $('#add_amphure_name').html(html);
                }).fail(function(){
                    swal("system.system_alert","system.system_error","error");
                    btn.button("reset");
                });
            });

            $('body').on('change', '#add_amphure_name', function(){
                var zipcode = $('#add_amphure_name').find(':selected').attr('data-zipcode');
                $('#add_zipcode').val(zipcode);
            });

            $('#CheckPassword').validate({
                errorElement: 'div',
                errorClass: 'invalid-feedback',
                focusInvalid: false,
                rules: {
                    old_password: {
                        required: true,
                    },
                    new_password: {
                        required: true,
                        minlength: 6,
                    },
                    password_confirmation: {
                        required: true,
                        equalTo: '#new_password',
                    },
                },
                messages: {
                    old_password: {
                        required: "กรุณาระบุ",
                    },
                    new_password: {
                        required: "กรุณาระบุ",
                        minlength: "ความยาวอย่างน้อย 6 ตัวอักษร",
                    },
                    password_confirmation: {
                        required: 'กรุณาระบุ',
                        equalTo: "ยืนยันรหัสผ่านไม่ถูกต้อง",
                    },
                },
                highlight: function (e) {
                    validate_highlight(e);
                },
                success: function (e) {
                    validate_success(e);
                },
                errorPlacement: function (error, element) {
                    validate_errorplacement(error, element);
                },
                submitHandler: function (form) {
                    var btn = $(form).find('[type="submit"]');
                    $.ajax({
                        method : "post",
                        url : url_gb+"/CheckPassword",
                        dataType : 'json',
                        data : $(form).serialize()
                    }).done(function(rec){
                        btn.button("reset");
                        if(rec.status==1){
                            $('#old_password').val('');
                            $('#new_password').val('');
                            $('#password_confirmation').val('');
                            $('#checkpassword').modal('hide');
                            swal(rec.title,rec.content,"success");
                        }else{
                            swal(rec.title,rec.content,"error");
                        }
                    }).fail(function(){
                        swal("system.system_alert","system.system_error","error");
                        btn.button("reset");
                    });
                },
                invalidHandler: function (form) {

                }
            });

            $('#EditProfile').validate({
                errorElement: 'div',
                errorClass: 'invalid-feedback',
                focusInvalid: false,
                rules: {
                    firstname: {
                        required: true,
                    },
                    lastname: {
                        required: true,
                    },
                    main_mobile: {
                        required: true,
                    },
                    address: {
                        required: true,
                    },
                    province_id: {
                        required: true,
                    },
                    amphur_id: {
                        required: true,
                    },

                },
                messages: {
                    firstname: {
                        required: "กรุณาระบุ",
                    },
                    lastname: {
                        required: "กรุณาระบุ",
                    },
                    main_mobile: {
                        required: "กรุณาระบุ",
                    },
                    address: {
                        required: "กรุณาระบุ",
                    },
                    province_id: {
                        required: "กรุณาระบุ",
                    },
                    amphur_id: {
                        required: "กรุณาระบุ",
                    },
                },
                highlight: function (e) {
                    validate_highlight(e);
                },
                success: function (e) {
                    validate_success(e);
                },
                errorPlacement: function (error, element) {
                    validate_errorplacement(error, element);
                },
                submitHandler: function (form) {
                    var btn = $(form).find('[type="submit"]');
                    $.ajax({
                        method : "post",
                        url : url_gb+"/EditProfile",
                        dataType : 'json',
                        data : $(form).serialize()
                    }).done(function(rec){
                        btn.button("reset");
                        if(rec.status==1){
                            swal(rec.title,rec.content,"success");
                        }else{
                            swal(rec.title,rec.content,"error");
                        }
                    }).fail(function(){
                        swal("system.system_alert","system.system_error","error");
                        btn.button("reset");
                    });
                },
                invalidHandler: function (form) {

                }
            });

            // CRUD Address
            $('body').on('click','.btn-address',function(data){
                TableListAddress.api().ajax.reload();
                ShowModal('ModalAddress');
            });

            $('body').on('click','.reset-form-address',function(data){
                $('#edit_address_id').val('');
                $('#modal_name').val('');
                $('#modal_user_address').val('');
                $('#modal_zipcode').val('');
                $('#modal_price').val('');

                $("#modal_province_id").val('').trigger('change')
                $("#modal_amphur_id").val('').trigger('change')
                $("#rel_product_type_id").val('').trigger('change')
            });

            $('body').on('click','.btn-address-edit',function(){
                var btn = $(this);
                btn.button('loading');
                var id = $(this).data('id');
                $('#edit_user_id').val(id);
                $.ajax({
                    method : "GET",
                    url : url_gb+"/EditAddress/"+id,
                    dataType : 'json'
                }).done(function(rec){
                    var amphures = rec.Amphures;

                    $('#edit_address_id').val(rec.UserAddress.id);
                    $('#modal_name').val(rec.UserAddress.name);
                    $('#modal_user_address').val(rec.UserAddress.address);
                    $('#modal_zipcode').val(rec.UserAddress.zipcode);

                    $('#modal_province_id').val(rec.UserAddress.province_id);
                    $('#modal_province_id').select2('destroy').select2();
                    $('#modal_amphur_id').val('').empty();
                    $('#modal_amphur_id').append('<option value="">เลือกอำเภอ</option>');
                    $.each(amphures, function(k,v){
                        $('#modal_amphur_id').append('<option value="' + v.id + '" data-zipcode="'+v.zipcode+'">' + v.amphure_name + '</option>');
                    });
                    $('#modal_amphur_id').val(rec.UserAddress.amphur_id);
                    $('#modal_amphur_id').select2('destroy').select2();
                    $('#modal_zipcode').val(rec.UserAddress.zipcode);

                    //btn.button("reset");
                    //ShowModal('ModalEdit');
                }).fail(function(){
                    swal("system.system_alert","system.system_error","error");
                    btn.button("reset");
                });
            });

            var TableListAddress = $('#TableListAddress').dataTable({
                "ajax": {
                    "url": url_gb+"/ListsAddress",
                    "data": function ( d ) {
                        //d.myKey = "myValue";
                        d.id = $('#edit_user_address_id').val();
                        // etc
                    }
                },
                "columns": [
                    { "data": "DT_Row_Index" , "className": "text-center", "searchable": false, "orderable": false  },
                    {"data" : "name"},
                    {"data" : "address"},
                    {"data" : "amphure_name", "name": "Amphure.amphure_name"},
                    {"data" : "province_name", "name": "Province.province_name"},
                    {"data" : "zipcode"},
                    { "data": "action","className":"action text-center" }
                ]
            });

            $('#FormAddAddress').validate({
                errorElement: 'div',
                errorClass: 'invalid-feedback',
                focusInvalid: false,
                rules: {
                    name: {
                        required: true,
                    },
                    address: {
                        required: true,
                    },
                    province_id: {
                        required: true,
                    },
                    amphur_id: {
                        required: true,
                    },
                    zipcode: {
                        required: true,
                    },
                },
                messages: {
                    name : {
                        required : 'กรุณาระบุ',
                    },
                    address : {
                        required : 'กรุณาระบุ',
                    },
                    province_id : {
                        required : 'กรุณาระบุ',
                    },
                    amphur_id : {
                        required : 'กรุณาระบุ',
                    },
                    zipcode : {
                        required : 'กรุณาระบุ',
                    },
                },
                highlight: function (e) {
                    validate_highlight(e);
                },
                success: function (e) {
                    validate_success(e);
                },

                errorPlacement: function (error, element) {
                    validate_errorplacement(error, element);
                },
                submitHandler: function (form) {
                    var btn = $(form).find('[type="submit"]');
                    var id = $('#edit_user_address_id').val();
                    // $('#user_id').val(id);
                    btn.button("loading");
                    $.ajax({
                        method : "POST",
                        url : url_gb+"/AddAddress",
                        dataType : 'json',
                        data : $(form).serialize()
                    }).done(function(rec){
                        btn.button("reset");
                        if(rec.status==1){
                            //resetFormCustom(form);
                            $('#modal_name').val('');
                            $('#modal_user_address').val('');
                            $('#modal_zipcode').val('');
                            $("#modal_province_id").val('').trigger('change')
                            $("#modal_amphur_id").val('').trigger('change')
                            $('#edit_address_id').val('');

                            TableListAddress.api().ajax.reload();
                            swal(rec.title,rec.content,"success");
                        }else{
                            swal(rec.title,rec.content,"error");
                        }
                    }).fail(function(){
                        swal("system.system_alert","system.system_error","error");
                        btn.button("reset");
                    });
                },
                invalidHandler: function (form) {

                }
            });

            $('body').on('click','.btn-address-delete',function(e){
                e.preventDefault();
                var btn = $(this);
                var id = btn.data('id');
                swal({
                    title: "คุณต้องการลบข้อมูลใช่หรือไม่",
                    text: "หากคุณลบจะไม่สามารถเรียกคืนข้อมูกลับมาได้",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    confirmButtonText: "ใช่ ฉันต้องการลบ",
                    cancelButtonText: "ยกเลิก",
                    showLoaderOnConfirm: true,
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        method : "POST",
                        url : url_gb+"/AddressDelete/"+id,
                        data : {ID : id}
                    }).done(function(rec){
                        if(rec.status==1){
                            swal(rec.title,rec.content,"success");
                            TableListAddress.api().ajax.reload();
                        }else{
                            swal("ระบบมีปัญหา","กรุณาติดต่อผู้ดูแล","error");
                        }
                    }).fail(function(data){
                        swal("ระบบมีปัญหา","กรุณาติดต่อผู้ดูแล","error");
                    });
                }).catch(function(e){
                    //console.log(e);
                });
            });

            $('body').on('change','#modal_province_id',function(){
                var province_id = $('#modal_province_id').val();
                $('#modal_amphur_id').html('');
                $('#modal_zipcode').val('');
                $.ajax({
                    method : "POST",
                    url: url_gb + "/GetAmphure/"+province_id,
                    dataType : 'json',
                }).done(function(res){
                    $('#modal_amphur_id').append('<option value="">กรุณาเลือกอำเภอ</option>');
                    $.each(res,function(k,v){
                        $('#modal_amphur_id').append('<option value="'+v.id+'" data-zipcode="'+v.zipcode+'">'+v.amphure_name+'</option>');
                    });
                }).fail(function(){

                });
            });

            $('body').on('change','#modal_amphur_id',function(){
                var zipcode = $('#modal_amphur_id').find('option[value="'+$('#modal_amphur_id').val()+'"]').attr('data-zipcode');
                $('#modal_zipcode').val(zipcode);
            });

            $('body').on('change','#edit_province_id',function(){
                var province_id = $('#edit_province_id').val();
                $('#edit_amphur_id').html('');
                $('#edit_zipcode').val('');
                $.ajax({
                    method : "POST",
                    url: url_gb + "/GetAmphure/"+province_id,
                    dataType : 'json',
                }).done(function(res){
                    $('#edit_amphur_id').append('<option value="">กรุณาเลือกอำเภอ</option>');
                    $.each(res,function(k,v){
                        $('#edit_amphur_id').append('<option value="'+v.id+'" data-zipcode="'+v.zipcode+'">'+v.amphure_name+'</option>');
                    });
                }).fail(function(){

                });
            });

            $('body').on('change','#edit_amphur_id',function(){
                var zipcode = $('#edit_amphur_id').find('option[value="'+$('#edit_amphur_id').val()+'"]').attr('data-zipcode');
                $('#edit_zipcode').val(zipcode);
            });

            $('#modal_province_id').select2();
            $('#modal_amphur_id').select2();
            // END CRUD Address
            $('#add_prefix_id').select2();
            $('#add_product_type_id').select2();
            $('#add_province_id').select2();
            $('#add_amphur_id').select2();
            $('#edit_prefix_id').select2();
            $('#edit_product_type_id').select2();
            $('#rel_product_type_id').select2();

        </script>
    </body>
</html>
