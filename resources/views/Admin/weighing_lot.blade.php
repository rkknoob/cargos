@extends('Admin.layouts.layout')
@section('css_bottom')
@endsection
@section('body')
@php
    $lot_weight_all = 0;
@endphp
<div class="content">
    <div class="col-md-12">
        <div class="container-fluid">
            <!-- ข้อมูล po -->
            <div class="row">
                <div class="col-md-12 col-md-offset-3">
                    <div class="card">
                        <div class="content">
                            <h4 class="modal-title" id="myModalLabel">@lang('lang.detail') {{$title_page or 'ข้อมูลใหม่'}}</h4>
                            <div class="material-datatables">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="myModalLabel">@lang('lang.create_data')</h5>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="edit_po_number">@lang('lang.po_number')</label>
                                                <input type="text" class="form-control" id="edit_po_number" value="{{ $ImportToChaina->po_no }}" readonly>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <label for="add_receiver_date">@lang('lang.import_date')</label>
                                            <input type="text" class="form-control" id="edit_receiver_date" value="{{ $ImportToChaina->ImportToChainaDate()->format('Y-m-d') }}" readonly>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="add_customer_id">@lang('lang.customer_id')</label>
                                                <select name="receiver[customer_id]" class="select2 form-control" tabindex="-1" data-placeholder="เลือกรหัสลูกค้า" id="add_customer_id" disabled>
                                                    <option value="">@lang('lang.customer_id')</option>
                                                    @foreach($Users as $User)
                                                        <option value="{{$User->id}}" {{ $ImportToChaina->user_id == $User->id ? 'selected' : null }} >{{$User->customer_general_code}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ข้อมูลสินค้า ชั่งรวม -->
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="content">
                            <div class="material-datatables">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="myModalLabel">@lang('lang.list_product') <label style="color:red;">* @lang('lang.case_total_weigh_and_scale')</label></h5>
                                </div>
                                <div class="modal-body" id="addWeighFormat_T">
                                    @if(!empty($ProductImportChaina) && count($ProductImportChaina) > 0)
                                        @foreach($ProductImportChaina as $key => $ProductImportChaina_s)
                                            @if(empty($ProductImportChaina_s->lot_product_weight_all) && $ProductImportChaina_s->weigh_format == 'S')
                                                @php $lot_weight_all++; @endphp
                                                <div class="add_product_lists sum_all"><hr>
                                                    <div class="row">
                                                        <div class="col-md-3" style="text-align: right;">
                                                            <div class="form-group">
                                                                <label><b>@lang('lang.weighing')</b></label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <input type="text" name="product[{{ $ProductImportChaina_s->id }}][lot_product][{{ $lot_weight_all }}][product_no_min]" class="form-control insert_product_no_min" placeholder="" value="{{( !empty($ProductImportChaina_s->product_sort_start) ? number_format($ProductImportChaina_s->product_sort_start) : '' )}}" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2" style="text-align: center;">
                                                            <div class="form-group">
                                                                <label><b>@lang('lang.to')</b></label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <input type="number" name="product[{{ $ProductImportChaina_s->id }}][lot_product][{{ $lot_weight_all }}][product_no_max]" class="form-control insert_product_no_max" placeholder=""
                                                                    value="{{( !empty($ProductImportChaina_s->product_sort_end) ? number_format($ProductImportChaina_s->product_sort_end) : '' )}}"
                                                                    max="{{ !empty($ProductImportChaina_s->product_sort_end) ? $ProductImportChaina_s->product_sort_end : '' }}"
                                                                    min="{{ !empty($ProductImportChaina_s->product_sort_start) ? $ProductImportChaina_s->product_sort_start : '' }}"
                                                                />
                                                                <input type="hidden" name="product[{{ $ProductImportChaina_s->id }}][lot_product][{{ $lot_weight_all }}][product_no_max_old]" class="insert_product_no_max_old" value="{{ $ProductImportChaina_s->product_sort_end }}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label>@lang('lang.producttype')</label>
                                                                <input type="hidden" name="product[{{ $ProductImportChaina_s->id }}][product_type_id]" class="form-control insert_product_type_id" value="{{ $ProductImportChaina_s->product_type_id ? $ProductImportChaina_s->product_type_id : null }}" readonly>
                                                                <input type="hidden" name="product[{{ $ProductImportChaina_s->id }}][product_id]" class="form-control insert_product_id" value="{{ $ProductImportChaina_s->id ? $ProductImportChaina_s->id : null }}" readonly>
                                                                <input type="hidden" name="product[{{ $ProductImportChaina_s->id }}][lot_product][{{ $lot_weight_all }}][lot_product_id]" class="form-control insert_lot_product_id" value="{{ $ProductImportChaina_s->lot_product_id ? $ProductImportChaina_s->lot_product_id : null }}" readonly>
                                                                <input type="text" name="product[{{ $ProductImportChaina_s->id }}][product_type_name]" class="form-control insert_product_type_name" value="{{ $ProductImportChaina_s->product_type_name ? $ProductImportChaina_s->product_type_code .' ('. $ProductImportChaina_s->product_type_name .')' : null }}" readonly>
                                                                <!-- <input type="hidden" class="form-control insert_weigh_type" value="{{ $ProductImportChaina_s->weigh_type }}" readonly> -->
                                                                <input type="hidden" name="product[{{ $ProductImportChaina_s->id }}][weigh_format]" class="form-control insert_weigh_format" value="{{ $ProductImportChaina_s->weigh_format }}" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label>@lang('lang.containertype')</label>
                                                                <input type="hidden" name="product[{{ $ProductImportChaina_s->id }}][container_type_id]" class="form-control insert_container_type_id" value="{{ !empty($ProductImportChaina_s->container_type_id) ? $ProductImportChaina_s->container_type_id : null }}" readonly>
                                                                <input type="text" name="product[{{ $ProductImportChaina_s->id }}][container_type_name]" class="form-control insert_container_type_name" value="{{ !empty($ProductImportChaina_s->container_type_name) ? $ProductImportChaina_s->container_type_name : null }}" readonly>
                                                            </div>
                                                        </div>
                                                        <!-- <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label>@lang('lang.product_id')</label>
                                                                <input type="text" name="product[{{ $ProductImportChaina_s->id }}][product_code]" class="form-control insert_product_code" value="{{ $ProductImportChaina_s->product_code }}" readonly>
                                                            </div>
                                                        </div> -->
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label>@lang('lang.product_name')</label>
                                                                <input type="text" name="product[{{ $ProductImportChaina_s->id }}][product_name]" class="form-control insert_product_name" placeholder="" value="{{ $ProductImportChaina_s->product_name }}" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label>@lang('lang.name_store_in_china')</label>
                                                                <input type="text" name="product[{{ $ProductImportChaina_s->id }}][chinese_shop]" class="form-control insert_chinese_shop" value="{{ $ProductImportChaina_s->shop_chaina_name }}" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label>@lang('lang.travel_by')</label>
                                                                <input type="hidden" class="form-control insert_transportation_type_id" value="{{ $ProductImportChaina_s->transport_type_id ? $ProductImportChaina_s->transport_type_id : null }}" readonly>
                                                                <input type="text" class="form-control insert_transportation_type_name" value="{{ $ProductImportChaina_s->transport_types_name ? $ProductImportChaina_s->transport_types_name : null }}" readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label>@lang('lang.quantity')</label>
                                                                <input type="text" name="product[{{ $ProductImportChaina_s->id }}][qty]" class="form-control insert_qty" placeholder="" value="{{ $ProductImportChaina_s->qty }}" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label>@lang('lang.tatal_weigth')</label>
                                                                <input type="text" name="product[{{ $ProductImportChaina_s->id }}][lot_product][{{ $lot_weight_all }}][size_weight_tote]" class="form-control number-only insert_size_weight_tote" placeholder="0" style="border-color: #4CAF50;">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <div class="form-group">
                                                                <label>@lang('lang.detail')</label>
                                                                <input type="text" name="product[{{ $ProductImportChaina_s->id }}][lot_product][{{ $lot_weight_all }}][decription]" class="form-control insert_decription"  value="{{ $ProductImportChaina_s->lot_product_remark }}" placeholder="@lang("lang.detail_container")" style="border-color: #4CAF50;">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label for="add_po_number">@lang('lang.scale')</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-1" style="text-align: center;">
                                                            <div class="form-group">
                                                                <label for="add_po_number">@lang('lang.wide') (@lang("lang.m"))</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-1">
                                                            <div class="form-group">
                                                                <input type="text" class="form-control number-only insert_size_wide cal_size_wide" name="product[{{ $ProductImportChaina_s->id }}][lot_product][{{ $lot_weight_all }}][size_wide]" value="{{ $ProductImportChaina_s->lot_product_width }}" style="border-color: #4CAF50;">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-1" style="text-align: center;">
                                                            <div class="form-group">
                                                                <label for="add_po_number">x @lang('lang.long') (@lang("lang.m"))</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-1">
                                                            <div class="form-group">
                                                                <input type="text" name="product[{{ $ProductImportChaina_s->id }}][lot_product][{{ $lot_weight_all }}][size_long]" class="form-control number-only insert_size_long cal_size_long" value="{{ $ProductImportChaina_s->lot_product_length }}" style="border-color: #4CAF50;">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-1" style="text-align: center;">
                                                            <div class="form-group">
                                                                <label for="add_po_number">x @lang('lang.high') (@lang("lang.m"))</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-1">
                                                            <div class="form-group">
                                                                <input type="text" name="product[{{ $ProductImportChaina_s->id }}][lot_product][{{ $lot_weight_all }}][size_high]" class="form-control number-only insert_size_high cal_size_high" value="{{ $ProductImportChaina_s->lot_product_height }}" style="border-color: #4CAF50;">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-1" style="text-align: center;">
                                                            <div class="form-group">
                                                                <label for="add_po_number">=</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-1">
                                                            <div class="form-group">
                                                                <input type="hidden" name="product[{{ $ProductImportChaina_s->id }}][lot_product][{{ $lot_weight_all }}][size_cubic]" class="form-control number-only insert_size_cubic cal_size_cubic" value="{{ $ProductImportChaina_s->lot_product_cubic }}" readonly>
                                                                <input type="text" name="product[{{ $ProductImportChaina_s->id }}][lot_product][{{ $lot_weight_all }}][show_size_cubic]" class="form-control number-only insert_show_size_cubic show_cal_size_cubic" value="{{ $ProductImportChaina_s->lot_product_cubic * (($ProductImportChaina_s->product_sort_end - $ProductImportChaina_s->product_sort_start)+1) }}" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-1" style="text-align: center;">
                                                            <div class="form-group">
                                                                <label for="add_po_number">@lang('lang.queue_size') (@lang("lang.m"))</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-1" style="text-align: center;">
                                                            <div class="form-group">
                                                                <a class="btn btn-insert-product-t btn-primary">@lang('lang.submit')</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                    @else
                                        <div class="row">
                                            <div class="col-md-12" style="text-align: center;">
                                                @lang('lang.data_not_found')
                                            </div>
                                        </div>
                                    @endif
                                </div>
                                <div class="modal-footer">
                                    <!-- <button type="button" class="btn btn-insert-product-t btn-primary">เพิ่ม</button> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ข้อมูลสินค้า ชั่งแยก -->
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="content">
                            <div class="material-datatables">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="myModalLabel">@lang('lang.list_product') <label style="color:red;">* @lang('lang.case_split_weigh')</label></h5>
                                </div>
                                <div class="modal-body">
                                    <table class="table">
                                        <thead id="addWeighFormat_F_Head">
                                            <tr>
                                                <th></th>
                                                <th style="width: 10%;">@lang('lang.weighing')</th>
                                                <!-- <th>@lang('lang.product_id')</th> -->
                                                <th>@lang('lang.product_name')</th>
                                                <th colspan="9" style="text-align: center;">@lang('lang.scale')</th>
                                                <th style="text-align: center;">@lang('lang.weight_kg')</th>
                                                <th style="text-align: center;">@lang('lang.detail')</th>
                                                <th style="text-align: center;"></th>
                                                <!-- <th></th> -->
                                            </tr>
                                        </thead>
                                        <tbody id="addWeighFormat_F">
                                            @if(!empty($ProductImportChaina) && count($ProductImportChaina) > 0)
                                                @foreach($ProductImportChaina as $key => $ProductImportChaina_e)
                                                    @if(empty($ProductImportChaina_e->lot_product_weight_all) && $ProductImportChaina_e->weigh_format == 'E')
                                                        @php $lot_weight_all++; @endphp
                                                        <tr class="product_weigh_format_f sum_all">
                                                            <td><a class="btn btn-primary btn-copy">Copy</a></td>
                                                            <td style="">
                                                                {{ number_format($ProductImportChaina_e->product_sort_start) }}
                                                                <input type="hidden" class="insert_product_id" name="product[{{ $ProductImportChaina_e->id }}][product_id]" value="{{ $ProductImportChaina_e->id }}">
                                                                <input type="hidden" class="insert_lot_product_id" name="product[{{ $ProductImportChaina_e->id }}][lot_product][{{ $lot_weight_all }}][lot_product_id]" value="{{ $ProductImportChaina_e->lot_product_id }}">

                                                                <input type="hidden" class="insert_product_no_min" name="product[{{ $ProductImportChaina_e->id }}][lot_product][{{ $lot_weight_all }}][product_no_min]" value="{{ $ProductImportChaina_e->product_sort_start }}">
                                                                <input type="hidden" class="insert_product_no_max" name="product[{{ $ProductImportChaina_e->id }}][lot_product][{{ $lot_weight_all }}][product_no_max]" value="{{ $ProductImportChaina_e->product_sort_end }}">
                                                                <input type="hidden" class="insert_product_no_max_old" name="product[{{ $ProductImportChaina_e->id }}][lot_product][{{ $lot_weight_all }}][product_no_max_old]" value="{{ $ProductImportChaina_e->product_sort_end }}">

                                                                <input type="hidden" class="insert_product_type_id" name="product[{{ $ProductImportChaina_e->id }}][product_type_id]" value="{{ $ProductImportChaina_e->product_type_id ? $ProductImportChaina_e->product_type_id : null }}" readonly>
                                                                <input type="hidden" class="insert_product_type_name" name="product[{{ $ProductImportChaina_e->id }}][product_type_name]" value="{{ $ProductImportChaina_e->product_type_name ? $ProductImportChaina_e->product_type_code .' ('. $ProductImportChaina_e->product_type_name .')' : null }}" readonly>
                                                                <input type="hidden" class="insert_container_type_id" name="product[{{ $ProductImportChaina_e->id }}][container_type_id]" value="{{ !empty($ProductImportChaina_e->container_type_id) ? $ProductImportChaina_e->container_type_id : null }}" readonly>
                                                                <input type="hidden" class="insert_container_type_name" name="product[{{ $ProductImportChaina_e->id }}][container_type_name]" value="{{ !empty($ProductImportChaina_e->container_type_name) ? $ProductImportChaina_e->container_type_name : null }}" readonly>
                                                                <input type="hidden" class="insert_product_code" name="product[{{ $ProductImportChaina_e->id }}][product_code]" value="{{ $ProductImportChaina_e->product_code }}" readonly>
                                                                <input type="hidden" class="insert_product_name" name="product[{{ $ProductImportChaina_e->id }}][product_name]" value="{{ $ProductImportChaina_e->product_name }}" readonly>
                                                                <input type="hidden" class="insert_chinese_shop" name="product[{{ $ProductImportChaina_e->id }}][chinese_shop]" value="{{ $ProductImportChaina_e->shop_chaina_name }}" readonly>
                                                                <input type="hidden" class="insert_transportation_type_id" name="product[{{ $ProductImportChaina_e->id }}][transportation_type_id]" value="{{ $ProductImportChaina_e->transport_type_id ? $ProductImportChaina_e->transport_type_id : null }}" readonly>
                                                                <input type="hidden" class="insert_transportation_type_name" name="product[{{ $ProductImportChaina_e->id }}][transportation_type_name]" value="{{ $ProductImportChaina_e->transport_types_name ? $ProductImportChaina_e->transport_types_name : null }}" readonly>
                                                                <input type="hidden" class="form-control insert_weigh_format" name="product[{{ $ProductImportChaina_e->id }}][weigh_format]" value="{{ $ProductImportChaina_e->weigh_format }}" readonly>
                                                                <input type="hidden" class="form-control insert_qty" name="product[{{ $ProductImportChaina_e->id }}][qty]" value="{{$ProductImportChaina_e->qty}}" readonly>
                                                            </td>

                                                            <td>{{ $ProductImportChaina_e->product_name }}</td>
                                                            <td style=" text-align: center;">@lang('lang.wide')</td>
                                                            <td style="">
                                                                <input type="text" class="form-control number-only insert_size_wide cal_size_wide" name="product[{{ $ProductImportChaina_e->id }}][lot_product][{{ $lot_weight_all }}][size_wide]" min="0" value="{{$ProductImportChaina_e->lot_product_width}}" style="border-color: #4CAF50;">
                                                            </td>
                                                            <td style=" text-align: center;"> x @lang('lang.long')</td>
                                                            <td style="">
                                                                <input type="text" class="form-control number-only insert_size_long cal_size_long" name="product[{{ $ProductImportChaina_e->id }}][lot_product][{{ $lot_weight_all }}][size_long]" min="0" value="{{$ProductImportChaina_e->lot_product_length}}"     style="border-color: #4CAF50;">
                                                            </td>
                                                            <td style=" text-align: center;"> x @lang('lang.high')</td>
                                                            <td style="">
                                                                <input type="text" class="form-control number-only insert_size_high cal_size_high" name="product[{{ $ProductImportChaina_e->id }}][lot_product][{{ $lot_weight_all }}][size_high]" min="0" value="{{$ProductImportChaina_e->lot_product_height}}"     style="border-color: #4CAF50;">
                                                            </td>
                                                            <td style=" text-align: center;"> = </td>
                                                            <td style="">
                                                                <input type="hidden" class="form-control number-only insert_size_cubic cal_size_cubic" name="product[{{ $ProductImportChaina_e->id }}][lot_product][{{ $lot_weight_all }}][size_cubic]" min="0" value="{{$ProductImportChaina_e->lot_product_cubic}}" readonly>
                                                                <input type="text" class="form-control number-only insert_show_size_cubic show_cal_size_cubic" name="product[{{ $ProductImportChaina_e->id }}][lot_product][{{ $lot_weight_all }}][show_size_cubic]" min="0" value="{{$ProductImportChaina_e->lot_product_cubic}}" readonly>
                                                            </td>
                                                            <td style=" text-align: center;">@lang("lang.queue")</td>
                                                            <td style="">
                                                                <input type="text" class="form-control number-only insert_size_weight_tote" name="product[{{ $ProductImportChaina_e->id }}][lot_product][{{ $lot_weight_all }}][size_weight_tote]" min="0" value="{{$ProductImportChaina_e->lot_product_weight_all}}" style="border-color: #4CAF50;">
                                                            </td>
                                                            <td style="">
                                                                <input type="text" class="form-control insert_decription" name="product[{{ $ProductImportChaina_e->id }}][lot_product][{{ $lot_weight_all }}][decription]" placeholder="@lang("lang.detail_container")" value="{{$ProductImportChaina_e->lot_product_remark}}"     style="border-color: #4CAF50;">
                                                            </td>
                                                            <td style="">
                                                                <a class="btn btn-insert-product-f btn-primary">@lang("lang.submit")</a>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                            @else
                                                <div class="row">
                                                    <div class="col-md-12" style="text-align: center;">
                                                        @lang('lang.data_not_found')
                                                    </div>
                                                </div>
                                            @endif
                                        </tbody>
                                    </table>

                                </div>
                                <div class="modal-footer">
                                    <!-- <button class="btn btn-insert-product-f btn-primary">@lang("lang.submit")</button> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Form ข้อมูลการชั่ง  -->
            <form id="FormEditWeighingLot">
                <input type="hidden" id="receiver_id" value="{{ $ImportToChaina->id }}">
                <!-- ข้อมูลสินค้า จ่ายแล้ว -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="content">
                                <div class="material-datatables">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="myModalLabel">@lang("lang.details")</h5>
                                    </div>
                                    <div class="modal-body">
                                        <table class="table">
                                            <thead class="text-primary">
                                                <tr>
                                                    <th>#</th>
                                                    <th>@lang('lang.producttype')</th>
                                                    <th>@lang('lang.product_id')</th>
                                                    <th>@lang('lang.product_name')</th>
                                                    <th>@lang('lang.queue_size')</th>
                                                    <th>@lang('lang.quantity')</th>
                                                    <th>@lang('lang.weigh')</th>
                                                    <th>@lang('lang.travel_by')</th>
                                                    <th>@lang('lang.detail')</th>
                                                    <th>@lang('lang.edit')</th>
                                                </tr>
                                            </thead>
                                            <tbody id="list-products">
                                                @if(!empty($ProductImportChaina) && count($ProductImportChaina) > 0)
                                                    @foreach($ProductImportChaina as $key => $ProductImportChaina_e)
                                                        @if(!empty($ProductImportChaina_e->lot_product_weight_all))
                                                            <tr class="list-product-sub">
                                                                <td>{{ number_format($key+1) }}
                                                                    <input class="edit_product_id" type="hidden" name="product[{{ $ProductImportChaina_e->id }}][product_id]" value="{{ $ProductImportChaina_e->id ? $ProductImportChaina_e->id : null }}">
                                                                    <input class="edit_product_type_id" type="hidden" name="product[{{ $ProductImportChaina_e->id }}][product_type_id]" value="{{ $ProductImportChaina_e->product_type_id ? $ProductImportChaina_e->product_type_id : null }}">
                                                                    <input class="edit_product_type_name" type="hidden" name="product[{{ $ProductImportChaina_e->id }}][product_type_name]" value="{{ $ProductImportChaina_e->product_type_name ? $ProductImportChaina_e->product_type_code .' ('. $ProductImportChaina_e->product_type_name .')' : null }}">
                                                                    <input class="edit_product_name" type="hidden" name="product[{{ $ProductImportChaina_e->id }}][product_name]" value="{{ $ProductImportChaina_e->product_name }}">
                                                                    <input class="edit_product_code" type="hidden" name="product[{{ $ProductImportChaina_e->id }}][product_code]" value="{{ $ProductImportChaina_e->product_code }}">
                                                                    <input class="edit_qty" type="hidden" name="product[{{ $ProductImportChaina_e->id }}][qty]" value="{{ $ProductImportChaina_e->qty }}">
                                                                    <input class="edit_transportation_type_id" type="hidden" name="product[{{ $ProductImportChaina_e->id }}][transportation_type_id]" value="{{ $ProductImportChaina_e->transport_type_id }}">
                                                                    <input class="edit_transportation_type_name" type="hidden" name="product[{{ $ProductImportChaina_e->id }}][transportation_type_name]" value="{{ $ProductImportChaina_e->transport_types_name }}">
                                                                    <input class="edit_container_type_id" type="hidden" name="product[{{ $ProductImportChaina_e->id }}][container_type_id]" value="{{ !empty($ProductImportChaina_e->container_type_id) ? $ProductImportChaina_e->container_type_id : '' }}">
                                                                    <input class="edit_container_type_name" type="hidden" name="product[{{ $ProductImportChaina_e->id }}][container_type_name]" value="{{ !empty($ProductImportChaina_e->container_type_name) ? $ProductImportChaina_e->container_type_name : '' }}">
                                                                    <input class="edit_chinese_shop" type="hidden" name="product[{{ $ProductImportChaina_e->id }}][chinese_shop]" value="{{ $ProductImportChaina_e->shop_chaina_name }}">
                                                                    <input class="edit_weigh_format" type="hidden" name="product[{{ $ProductImportChaina_e->id }}][weigh_format]" value="{{ $ProductImportChaina_e->weigh_format }}">
                                                                </td>
                                                                <td>{{ $ProductImportChaina_e->product_type_name ? $ProductImportChaina_e->product_type_code .' ('. $ProductImportChaina_e->product_type_name .')' : null }}</td>
                                                                <td>{{ $ProductImportChaina_e->product_code }}</td>
                                                                <td>{{ $ProductImportChaina_e->product_name }}</td>
                                                                <td>{{ !empty($ProductImportChaina_e->lot_product_cubic) ? number_format($ProductImportChaina_e->lot_product_cubic * (($ProductImportChaina_e->product_sort_end-$ProductImportChaina_e->product_sort_start)+1), 2) : '0' }}
                                                                    <input class="edit_product_no_min" type="hidden" name="product[{{ $ProductImportChaina_e->id }}][lot_product][{{ $lot_weight_all }}][product_no_min]" value="{{ $ProductImportChaina_e->product_sort_start }}">
                                                                    <input class="edit_product_no_max" type="hidden" name="product[{{ $ProductImportChaina_e->id }}][lot_product][{{ $lot_weight_all }}][product_no_max]" value="{{ $ProductImportChaina_e->product_sort_end }}">
                                                                    <input class="edit_product_no_max_old" type="hidden" name="product[{{ $ProductImportChaina_e->id }}][lot_product][{{ $lot_weight_all }}][product_no_max_old]" value="{{ $ProductImportChaina_e->product_sort_end }}">
                                                                    <input class="edit_lot_product_id" type="hidden" name="product[{{ $ProductImportChaina_e->id }}][lot_product][{{ $lot_weight_all }}][lot_product_id]" value="{{ $ProductImportChaina_e->lot_product_id ? $ProductImportChaina_e->lot_product_id : null }}">
                                                                    <input class="edit_size_wide" type="hidden" name="product[{{ $ProductImportChaina_e->id }}][lot_product][{{ $lot_weight_all }}][size_wide]" value="{{ $ProductImportChaina_e->lot_product_width ? $ProductImportChaina_e->lot_product_width : null }}">
                                                                    <input class="edit_size_high" type="hidden" name="product[{{ $ProductImportChaina_e->id }}][lot_product][{{ $lot_weight_all }}][size_high]" value="{{ $ProductImportChaina_e->lot_product_height ? $ProductImportChaina_e->lot_product_height : null }}">
                                                                    <input class="edit_size_long" type="hidden" name="product[{{ $ProductImportChaina_e->id }}][lot_product][{{ $lot_weight_all }}][size_long]" value="{{ $ProductImportChaina_e->lot_product_length ? $ProductImportChaina_e->lot_product_length : null }}">
                                                                    <input class="edit_size_cubic" type="hidden" name="product[{{ $ProductImportChaina_e->id }}][lot_product][{{ $lot_weight_all }}][size_cubic]" value="{{ $ProductImportChaina_e->lot_product_cubic ? $ProductImportChaina_e->lot_product_cubic : null }}">
                                                                    <input class="edit_show_size_cubic" type="hidden" name="product[{{ $ProductImportChaina_e->id }}][lot_product][{{ $lot_weight_all }}][show_size_cubic]" value="{{ $ProductImportChaina_e->lot_product_cubic ? $ProductImportChaina_e->lot_product_cubic * (($ProductImportChaina_e->product_sort_end-$ProductImportChaina_e->product_sort_start)+1) : null }}">
                                                                    <input class="edit_size_weight_tote" type="hidden" name="product[{{ $ProductImportChaina_e->id }}][lot_product][{{ $lot_weight_all }}][size_weight_tote]" value="{{ $ProductImportChaina_e->lot_product_weight_all ? $ProductImportChaina_e->lot_product_weight_all : null }}">
                                                                    <input class="edit_decription" type="hidden" name="product[{{ $ProductImportChaina_e->id }}][lot_product][{{ $lot_weight_all }}][decription]" value="{{ $ProductImportChaina_e->lot_product_remark ? $ProductImportChaina_e->lot_product_remark : null }}">
                                                                    <input type="hidden" name="product[{{ $ProductImportChaina_e->id }}][lot_product][{{ $lot_weight_all }}][qty]" value="{{ ($ProductImportChaina_e->product_sort_end - $ProductImportChaina_e->product_sort_start) + 1 }}">
                                                                </td>
                                                                <td>{{ number_format(($ProductImportChaina_e->product_sort_end - $ProductImportChaina_e->product_sort_start) + 1) }}</td>
                                                                <td>{{ !empty($ProductImportChaina_e->lot_product_weight_all) ? number_format($ProductImportChaina_e->lot_product_weight_all, 2) : null }}</td>
                                                                <td>{{ $ProductImportChaina_e->transport_types_name ? $ProductImportChaina_e->transport_types_name : null }}</td>
                                                                <td>{{ $ProductImportChaina_e->lot_product_remark ? $ProductImportChaina_e->lot_product_remark : null }}</td>
                                                                <td>
                                                                    <button class="btn btn-sm btn-warning btn-condensed btn-edit-product btn-tooltip" data-id="1" data-rel="tooltip" title="" data-original-title="Delete">
                                                                        <i class="ace-icon fa fa-edit"></i>
                                                                    </button>
                                                                </td>
                                                            </tr>
                                                        @endif
                                                        @php
                                                            $lot_weight_all++
                                                        @endphp
                                                    @endforeach
                                                @else
                                                    <tr id="product-not-found">
                                                        <td colspan=10 style="text-align: center;">@lang('lang.data_not_found')</td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-primary">@lang('lang.save')</button>
                                        <a href="{{ url('/admin/'.$lang.'/ImportToChaina') }}" type="cancel" class="btn btn-danger">@lang('lang.cancel')</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('js_bottom')
<script src="{{asset('assets/global/plugins/orakuploader/orakuploader.js')}}"></script>
<script>

    var lot_weight_all = parseInt('{{$lot_weight_all}}');
    $('body').on('click', '.btn-insert-product-t', function(){
        lot_weight_all++;
        var ele = $(this).closest('.add_product_lists');
        // $('.add_product_lists').each(function(){
        var product_id                  = $(ele).find('.insert_product_id').val();
        var lot_product_id              = $(ele).find('.insert_lot_product_id').val();
        var product_no_min              = $(ele).find('.insert_product_no_min').val();
        var product_no_max              = $(ele).find('.insert_product_no_max').val();
        var product_no_max_old          = $(ele).find('.insert_product_no_max_old').val();
        var product_type_id             = $(ele).find('.insert_product_type_id').val();
        var product_type_name           = $(ele).find('.insert_product_type_name').val();
        var container_type_id           = $(ele).find('.insert_container_type_id').val();
        var container_type_name         = $(ele).find('.insert_container_type_name').val();
        var product_name                = $(ele).find('.insert_product_name').val();
        var product_code                = $(ele).find('.insert_product_code').val();
        var chinese_shop                = $(ele).find('.insert_chinese_shop').val();
        var transportation_type_id      = $(ele).find('.insert_transportation_type_id').val();
        var transportation_type_name    = $(ele).find('.insert_transportation_type_name').val();
        var qty                         = $(ele).find('.insert_qty').val();
        var weigh_format                = $(ele).find('.insert_weigh_format').val();
        var size_weight_tote            = $(ele).find('.insert_size_weight_tote').val();
        var decription                  = $(ele).find('.insert_decription').val();
        var size_wide                   = $(ele).find('.insert_size_wide').val();
        var size_high                   = $(ele).find('.insert_size_high').val();
        var size_long                   = $(ele).find('.insert_size_long').val();
        var size_cubic                  = $(ele).find('.insert_size_cubic').val();
        var show_size_cubic             = $(ele).find('.insert_show_size_cubic').val();
        var decription                  = $(ele).find('.insert_decription').val();
        var html                        = '';
        if(size_weight_tote){
            html += '<tr class="list-product-sub">\
                        <td>#\
                            <input class="edit_product_id" type="hidden" name="product['+product_id+'][product_id]" value="'+product_id+'">\
                            <input class="edit_product_type_id" type="hidden" name="product['+product_id+'][product_type_id]" value="'+product_type_id+'">\
                            <input class="edit_product_type_name" type="hidden" name="product['+product_id+'][product_type_name]" value="'+product_type_name+'">\
                            <input class="edit_product_name" type="hidden" name="product['+product_id+'][product_name]" value="'+product_name+'">\
                            <input class="edit_product_code" type="hidden" name="product['+product_id+'][product_code]" value="'+product_code+'">\
                            <input class="edit_qty" type="hidden" name="product['+product_id+'][qty]" value="'+qty+'">\
                            <input class="edit_transportation_type_id" type="hidden" name="product['+product_id+'][transportation_type_id]" value="'+transportation_type_id+'">\
                            <input class="edit_transportation_type_name" type="hidden" name="product['+product_id+'][transportation_type_name]" value="'+transportation_type_name+'">\
                            <input class="edit_container_type_id" type="hidden" name="product['+product_id+'][container_type_id]" value="'+container_type_id+'">\
                            <input class="edit_container_type_name" type="hidden" name="product['+product_id+'][container_type_name]" value="'+container_type_name+'">\
                            <input class="edit_chinese_shop" type="hidden" name="product['+product_id+'][chinese_shop]" value="'+chinese_shop+'">\
                            <input class="edit_weigh_format" type="hidden" name="product['+product_id+'][weigh_format]" value="'+weigh_format+'">\
                        </td>\
                        <td>'+product_type_name+'</td>\
                        <td>'+product_code+'</td>\
                        <td>'+product_name+'</td>\
                        <td>'+addNumformat(Math.round(show_size_cubic * 1000) / 1000)+'\
                            <input class="edit_product_no_min" type="hidden" name="product['+product_id+'][lot_product]['+lot_weight_all+'][product_no_min]" value="'+product_no_min+'">\
                            <input class="edit_product_no_max" type="hidden" name="product['+product_id+'][lot_product]['+lot_weight_all+'][product_no_max]" value="'+product_no_max+'">\
                            <input class="edit_product_no_max_old" type="hidden" name="product['+product_id+'][lot_product]['+lot_weight_all+'][product_no_max_old]" value="'+product_no_max_old+'">\
                            <input class="edit_lot_product_id" type="hidden" name="product['+product_id+'][lot_product]['+lot_weight_all+'][lot_product_id]" value="'+lot_product_id+'">\
                            <input class="edit_size_wide" type="hidden" name="product['+product_id+'][lot_product]['+lot_weight_all+'][size_wide]" value="'+size_wide+'">\
                            <input class="edit_size_high" type="hidden" name="product['+product_id+'][lot_product]['+lot_weight_all+'][size_high]" value="'+size_high+'">\
                            <input class="edit_size_long" type="hidden" name="product['+product_id+'][lot_product]['+lot_weight_all+'][size_long]" value="'+size_long+'">\
                            <input class="edit_size_cubic" type="hidden" name="product['+product_id+'][lot_product]['+lot_weight_all+'][size_cubic]" value="'+size_cubic+'">\
                            <input class="edit_show_size_cubic" type="hidden" name="product['+product_id+'][lot_product]['+lot_weight_all+'][show_size_cubic]" value="'+show_size_cubic+'">\
                            <input class="edit_size_weight_tote" type="hidden" name="product['+product_id+'][lot_product]['+lot_weight_all+'][size_weight_tote]" value="'+size_weight_tote+'">\
                            <input class="edit_decription" type="hidden" name="product['+product_id+'][lot_product]['+lot_weight_all+'][decription]" value="'+decription+'">\
                            <input type="hidden" name="product['+product_id+'][lot_product]['+lot_weight_all+'][qty]" value="'+((parseInt(product_no_max)-parseInt(product_no_min))+1)+'">\
                        </td>\
                        <td>'+addNumformat((parseInt(product_no_max)-parseInt(product_no_min))+1)+'</td>\
                        <td>'+addNumformat(Math.round(size_weight_tote * 1000) / 1000)+'</td>\
                        <td>'+transportation_type_name+'</td>\
                        <td>'+decription+'</td>\
                        <td>\
                            <button class="btn btn-sm btn-danger btn-condensed btn-edit-product btn-tooltip" data-id="'+lot_product_id+'" data-rel="tooltip" title="" data-original-title="Delete">\
                                <i class="ace-icon fa fa-edit"></i>\
                            </button>\
                        </td>\
                    </tr>';

            $('#product-not-found').hide();
            $('#list-products').append(html);
            // $('#addWeighFormat_T').html('');
            if(parseInt(product_no_max) >= parseInt(product_no_max_old)){
                $(ele).remove();
            }else{
                $(ele).find('.insert_product_no_min').val(parseInt(product_no_max)+1);
                $(ele).find('.insert_product_no_max').val(parseInt(product_no_max_old));
                $(ele).find('.insert_product_no_max').attr("min", parseInt(product_no_max)+1);
                $(ele).find('.insert_size_weight_tote').val('');
                $(ele).find('.insert_decription').val('');
                $(ele).find('.insert_size_wide').val('');
                $(ele).find('.insert_size_long').val('');
                $(ele).find('.insert_size_high').val('');
                $(ele).find('.insert_size_cubic').val('');
            }
            // });
        }else{
            swal('ชั่งน้ำหนัก', 'กรุณากรอกน้ำหนัก', "warning");
        }
    });

    $('body').on('click', '.btn-insert-product-f', function(){
        var html = '';
        lot_weight_all++;
        var ele = $(this).closest('tr');
        // $('.product_weigh_format_f').each(function(k, v){
        var product_id                  = $(ele).find('.insert_product_id').val();
        var lot_product_id              = $(ele).find('.insert_lot_product_id').val();
        var product_no_min              = $(ele).find('.insert_product_no_min').val();
        var product_no_max              = $(ele).find('.insert_product_no_max').val();
        var product_no_max_old          = $(ele).find('.insert_product_no_max_old').val();
        var product_type_id             = $(ele).find('.insert_product_type_id').val();
        var product_type_name           = $(ele).find('.insert_product_type_name').val();
        var container_type_id           = $(ele).find('.insert_container_type_id').val();
        var container_type_name         = $(ele).find('.insert_container_type_name').val();
        var product_name                = $(ele).find('.insert_product_name').val();
        var product_code                = $(ele).find('.insert_product_code').val();
        var chinese_shop                = $(ele).find('.insert_chinese_shop').val();
        var transportation_type_id      = $(ele).find('.insert_transportation_type_id').val();
        var transportation_type_name    = $(ele).find('.insert_transportation_type_name').val();
        var qty                         = $(ele).find('.insert_qty').val();
        var weigh_format                = $(ele).find('.insert_weigh_format').val();
        var size_weight_tote            = $(ele).find('.insert_size_weight_tote').val();
        var decription                  = $(ele).find('.insert_decription').val();
        var size_wide                   = $(ele).find('.insert_size_wide').val();
        var size_high                   = $(ele).find('.insert_size_high').val();
        var size_long                   = $(ele).find('.insert_size_long').val();
        var size_cubic                  = $(ele).find('.insert_size_cubic').val();
        var show_size_cubic             = $(ele).find('.insert_show_size_cubic').val();
        if(size_weight_tote){
            html += '<tr class="list-product-sub">\
                        <td>#\
                            <input class="edit_product_id" type="hidden" name="product['+product_id+'][product_id]" value="'+product_id+'">\
                            <input class="edit_product_type_id" type="hidden" name="product['+product_id+'][product_type_id]" value="'+product_type_id+'">\
                            <input class="edit_product_type_name" type="hidden" name="product['+product_id+'][product_type_name]" value="'+product_type_name+'">\
                            <input class="edit_product_name" type="hidden" name="product['+product_id+'][product_name]" value="'+product_name+'">\
                            <input class="edit_product_code" type="hidden" name="product['+product_id+'][product_code]" value="'+product_code+'">\
                            <input class="edit_qty" type="hidden" name="product['+product_id+'][qty]" value="'+qty+'">\
                            <input class="edit_transportation_type_id" type="hidden" name="product['+product_id+'][transportation_type_id]" value="'+transportation_type_id+'">\
                            <input class="edit_transportation_type_name" type="hidden" name="product['+product_id+'][transportation_type_name]" value="'+transportation_type_name+'">\
                            <input class="edit_container_type_id" type="hidden" name="product['+product_id+'][container_type_id]" value="'+container_type_id+'">\
                            <input class="edit_container_type_name" type="hidden" name="product['+product_id+'][container_type_name]" value="'+container_type_name+'">\
                            <input class="edit_chinese_shop" type="hidden" name="product['+product_id+'][chinese_shop]" value="'+chinese_shop+'">\
                            <input class="edit_weigh_format" type="hidden" name="product['+product_id+'][weigh_format]" value="'+weigh_format+'">\
                        </td>\
                        <td>'+product_type_name+'</td>\
                        <td>'+product_code+'</td>\
                        <td>'+product_name+'</td>\
                        <td>'+addNumformat(Math.round(show_size_cubic * 1000) / 1000)+'\
                            <input class="edit_product_no_min" type="hidden" name="product['+product_id+'][lot_product]['+lot_weight_all+'][product_no_min]" value="'+product_no_min+'">\
                            <input class="edit_product_no_max" type="hidden" name="product['+product_id+'][lot_product]['+lot_weight_all+'][product_no_max]" value="'+product_no_max+'">\
                            <input class="edit_product_no_max_old" type="hidden" name="product['+product_id+'][lot_product]['+lot_weight_all+'][product_no_max_old]" value="'+product_no_max_old+'">\
                            <input class="edit_lot_product_id" type="hidden" name="product['+product_id+'][lot_product]['+lot_weight_all+'][lot_product_id]" value="'+lot_product_id+'">\
                            <input class="edit_size_wide" type="hidden" name="product['+product_id+'][lot_product]['+lot_weight_all+'][size_wide]" value="'+size_wide+'">\
                            <input class="edit_size_high" type="hidden" name="product['+product_id+'][lot_product]['+lot_weight_all+'][size_high]" value="'+size_high+'">\
                            <input class="edit_size_long" type="hidden" name="product['+product_id+'][lot_product]['+lot_weight_all+'][size_long]" value="'+size_long+'">\
                            <input class="edit_size_cubic" type="hidden" name="product['+product_id+'][lot_product]['+lot_weight_all+'][size_cubic]" value="'+size_cubic+'">\
                            <input class="edit_show_size_cubic" type="hidden" name="product['+product_id+'][lot_product]['+lot_weight_all+'][show_size_cubic]" value="'+show_size_cubic+'">\
                            <input class="edit_size_weight_tote" type="hidden" name="product['+product_id+'][lot_product]['+lot_weight_all+'][size_weight_tote]" value="'+size_weight_tote+'">\
                            <input class="edit_decription" type="hidden" name="product['+product_id+'][lot_product]['+lot_weight_all+'][decription]" value="'+decription+'">\
                            <input type="hidden" name="product['+product_id+'][lot_product]['+lot_weight_all+'][qty]" value="1">\
                        </td>\
                        <td>1</td>\
                        <td>'+addNumformat(Math.round(size_weight_tote * 1000) / 1000)+'</td>\
                        <td>'+transportation_type_name+'</td>\
                        <td>'+decription+'</td>\
                        <td>\
                            <button class="btn btn-sm btn-danger btn-condensed btn-edit-product btn-tooltip" data-id="'+lot_weight_all+'" data-rel="tooltip" title="" data-original-title="Delete">\
                                <i class="ace-icon fa fa-edit"></i>\
                            </button>\
                        </td>\
                    </tr>';
            // });
            $('#product-not-found').hide();
            $('#list-products').append(html);
            // $('#addWeighFormat_F').html('');
            // $('#addWeighFormat_F_main').html('');
            // $('#data-not-found').show();
            $(ele).remove();
        }else{
            swal('ชั่งน้ำหนัก', 'กรุณากรอกน้ำหนัก', "warning");
        }
    });

    $('body').on('click', '.btn-edit-product', function(){
        lot_weight_all++;
        var ele = $(this).closest('tr');
        var product_id                  = $(ele).find('.edit_product_id').val();
        var lot_product_id              = $(ele).find('.edit_lot_product_id').val();
        var product_no_min              = $(ele).find('.edit_product_no_min').val();
        var product_no_max              = $(ele).find('.edit_product_no_max').val();
        var product_no_max_old          = $(ele).find('.edit_product_no_max_old').val();
        var product_type_id             = $(ele).find('.edit_product_type_id').val();
        var product_type_name           = $(ele).find('.edit_product_type_name').val();
        var container_type_id           = $(ele).find('.edit_container_type_id').val();
        var container_type_name         = $(ele).find('.edit_container_type_name').val();
        var product_name                = $(ele).find('.edit_product_name').val();
        var product_code                = $(ele).find('.edit_product_code').val();
        var chinese_shop                = $(ele).find('.edit_chinese_shop').val();
        var transportation_type_id      = $(ele).find('.edit_transportation_type_id').val();
        var transportation_type_name    = $(ele).find('.edit_transportation_type_name').val();
        var qty                         = $(ele).find('.edit_qty').val();
        var weigh_format                = $(ele).find('.edit_weigh_format').val();
        var size_weight_tote            = $(ele).find('.edit_size_weight_tote').val();
        var decription                  = $(ele).find('.edit_decription').val();
        var size_wide                   = $(ele).find('.edit_size_wide').val();
        var size_high                   = $(ele).find('.edit_size_high').val();
        var size_long                   = $(ele).find('.edit_size_long').val();
        var size_cubic                  = $(ele).find('.edit_size_cubic').val();
        var show_size_cubic             = $(ele).find('.edit_show_size_cubic').val();
        var decription                  = $(ele).find('.edit_decription').val();
        var html                        = '';
        // console.log(transportation_type_name);
        if(weigh_format == 'S'){ // เช็ค ชั่งรวม
            html = '\
                <div class="add_product_lists sum_all"><hr>\
                    <div class="row">\
                        <div class="col-md-3" style="text-align: right;">\
                            <div class="form-group">\
                                <label><b>@lang('lang.weighing')</b></label>\
                            </div>\
                        </div>\
                        <div class="col-md-2">\
                            <div class="form-group">\
                                <input type="text" name="product['+product_id+'][lot_product]['+lot_weight_all+'][product_no_min]" class="form-control insert_product_no_min" value="'+product_no_min+'" readonly>\
                            </div>\
                        </div>\
                        <div class="col-md-2" style="text-align: center;">\
                            <div class="form-group">\
                                <label><b>@lang('lang.to')</b></label>\
                            </div>\
                        </div>\
                        <div class="col-md-2">\
                            <div class="form-group">\
                                <input type="number" class="form-control insert_product_no_max" name="product['+product_id+'][lot_product]['+lot_weight_all+'][product_no_max]"\
                                    value="'+product_no_max+'"\
                                    max="'+product_no_max+'"\
                                    min="'+product_no_min+'"\
                                />\
                                <input type="hidden" name="product['+product_id+'][lot_product]['+lot_weight_all+'][product_no_max_old]" class="insert_product_no_max_old" value="'+product_no_max_old+'">\
                            </div>\
                        </div>\
                    </div>\
                    <div class="row">\
                        <div class="col-md-2">\
                            <div class="form-group">\
                                <label>@lang("lang.product_category")</label>\
                                <input type="hidden" name="product['+product_id+'][product_type_id]" class="form-control insert_product_type_id" value="'+product_type_id+'" readonly>\
                                <input type="hidden" name="product['+product_id+'][product_id]" class="form-control insert_product_id" value="'+product_id+'" readonly>\
                                <input type="hidden" name="product['+product_id+'][lot_product]['+lot_weight_all+'][lot_product_id]" class="form-control insert_lot_product_id" value="'+lot_product_id+'" readonly>\
                                <input type="text" name="product['+product_id+'][product_type_name]" class="form-control insert_product_type_name" value="'+product_type_name+'" readonly>\
                                <input type="hidden" name="product['+product_id+'][weigh_format]" class="form-control insert_weigh_format" value="'+weigh_format+'" readonly>\
                            </div>\
                        </div>\
                        <!--<div class="col-md-2">\
                            <div class="form-group">\
                                <label>@lang("lang.product_id")</label>\
                                <input type="text" name="product['+product_id+'][product_code]" class="form-control insert_product_code" value="'+product_code+'" readonly>\
                            </div>\
                        </div>-->\
                        <div class="col-md-2">\
                            <div class="form-group">\
                                <label>@lang("lang.product_name")</label>\
                                <input type="text" name="product['+product_id+'][product_name]" class="form-control insert_product_name" placeholder="" value="'+product_name+'" readonly>\
                            </div>\
                        </div>\
                        <div class="col-md-2">\
                            <div class="form-group">\
                                <label>@lang("lang.name_store_in_china")</label>\
                                <input type="text" class="form-control insert_chinese_shop" name="product['+product_id+'][chinese_shop]" value="'+chinese_shop+'" readonly>\
                            </div>\
                        </div>\
                        <div class="col-md-2">\
                            <div class="form-group">\
                                <label>@lang("lang.travel_by")</label>\
                                <input type="hidden" name="product['+product_id+'][transportation_type_id]" class="form-control insert_transportation_type_id" placeholder="" value="'+transportation_type_id+'" readonly>\
                                <input type="text" name="product['+product_id+'][transportation_type_name]" class="form-control insert_transportation_type_name" placeholder="" value="'+transportation_type_name+'" readonly>\
                            </div>\
                        </div>\
                        <div class="col-md-2">\
                            <div class="form-group">\
                                <label>@lang("lang.quantity")</label>\
                                <input type="text" class="form-control insert_qty" name="product['+product_id+'][lot_product]['+lot_weight_all+'][qty]" value="'+qty+'" readonly>\
                            </div>\
                        </div>\
                    </div>\
                    <div class="row">\
                        <div class="col-md-2">\
                            <div class="form-group">\
                                <label>@lang("lang.tatal_weigth")</label>\
                                <input type="text" name="product['+product_id+'][lot_product]['+lot_weight_all+'][size_weight_tote]" class="form-control number-only insert_size_weight_tote" value="'+size_weight_tote+'" style="border-color: #4CAF50;">\
                            </div>\
                        </div>\
                        <div class="col-md-10">\
                            <div class="form-group">\
                                <label>@lang("lang.detail_container")</label>\
                                <input type="text" name="product['+product_id+'][lot_product]['+lot_weight_all+'][decription]" class="form-control insert_decription" value="'+decription+'" style="border-color: #4CAF50;">\
                            </div>\
                        </div>\
                    </div>\
                    <div class="row">\
                        <div class="col-md-2">\
                            <div class="form-group">\
                                <label for="add_po_number">@lang("lang.scale")</label>\
                            </div>\
                        </div>\
                        <div class="col-md-1" style="text-align: center;">\
                            <div class="form-group">\
                                <label for="add_po_number">@lang('lang.wide') (@lang("lang.m"))</label>\
                            </div>\
                        </div>\
                        <div class="col-md-1">\
                            <div class="form-group">\
                                <input type="text" name="product['+product_id+'][lot_product]['+lot_weight_all+'][size_wide]" class="form-control number-only insert_size_wide cal_size_wide" value="'+size_wide+'" style="border-color: #4CAF50;">\
                            </div>\
                        </div>\
                        <div class="col-md-1" style="text-align: center;">\
                            <div class="form-group">\
                                <label for="add_po_number">x @lang('lang.long') (@lang("lang.m"))</label>\
                            </div>\
                        </div>\
                        <div class="col-md-1">\
                            <div class="form-group">\
                                <input type="text" name="product['+product_id+'][lot_product]['+lot_weight_all+'][size_long]" class="form-control number-only insert_size_long cal_size_long" value="'+size_long+'" style="border-color: #4CAF50;">\
                            </div>\
                        </div>\
                        <div class="col-md-1" style="text-align: center;">\
                            <div class="form-group">\
                                <label for="add_po_number">x @lang('lang.high') (@lang("lang.m"))</label>\
                            </div>\
                        </div>\
                        <div class="col-md-1">\
                            <div class="form-group">\
                                <input type="text" name="product['+product_id+'][lot_product]['+lot_weight_all+'][size_high]" class="form-control number-only insert_size_high cal_size_high" value="'+size_high+'" style="border-color: #4CAF50;">\
                            </div>\
                        </div>\
                        <div class="col-md-1" style="text-align: center;">\
                            <div class="form-group">\
                                <label for="add_po_number">=</label>\
                            </div>\
                        </div>\
                        <div class="col-md-1">\
                            <div class="form-group">\
                                <input type="hidden" name="product['+product_id+'][lot_product]['+lot_weight_all+'][size_cubic]" class="form-control number-only insert_size_cubic cal_size_cubic" value="'+size_cubic+'" readonly>\
                                <input type="text" name="product['+product_id+'][lot_product]['+lot_weight_all+'][size_cubic]" class="form-control number-only insert_show_size_cubic show_cal_size_cubic" value="'+show_size_cubic+'" readonly>\
                            </div>\
                        </div>\
                        <div class="col-md-1" style="text-align: center;">\
                            <div class="form-group">\
                                <label for="add_po_number">@lang("lang.queue") (@lang("lang.m"))</label>\
                            </div>\
                        </div>\
                        <div class="col-md-1" style="text-align: center;">\
                            <div class="form-group">\
                                <a class="btn btn-insert-product-t btn-primary">@lang("lang.submit")</a>\
                            </div>\
                        </div>\
                    </div>\
                </div>\
            ';
            $('#addWeighFormat_T').append(html);

        }
        else if(weigh_format == 'E'){ // เช็ค ชั่งแยก
            html = '\
                <tr class="product_weigh_format_f sum_all">\
                    <td><a class="btn btn-primary btn-copy">Copy</a></td>\
                    <td style="">\
                        '+addNumformat(product_no_min)+'\
                        <input type="hidden" class="insert_product_id" name="product['+product_id+'][product_id]" value="'+product_id+'">\
                        <input type="hidden" class="insert_lot_product_id" name="product['+product_id+'][lot_product]['+lot_weight_all+'][lot_product_id]" value="'+lot_product_id+'">\
                        <input type="hidden" class="insert_product_no_min" name="product['+product_id+'][lot_product]['+lot_weight_all+'][product_no_min]" value="'+product_no_min+'">\
                        <input type="hidden" class="insert_product_no_max" name="product['+product_id+'][lot_product]['+lot_weight_all+'][product_no_max]" value="'+product_no_max+'">\
                        <input type="hidden" class="insert_product_no_max_old" name="product['+product_id+'][lot_product]['+lot_weight_all+'][product_no_max_old]" value="'+product_no_max_old+'">\
                        <input type="hidden" class="insert_product_type_id" name="product['+product_id+'][product_type_id]" value="'+product_type_id+'" readonly>\
                        <input type="hidden" class="insert_product_type_name" name="product['+product_id+'][product_type_name]" value="'+product_type_name+'" readonly>\
                        <input type="hidden" class="insert_container_type_id" name="product['+product_id+'][container_type_id]" value="'+container_type_id+'" readonly>\
                        <input type="hidden" class="insert_container_type_name" name="product['+product_id+'][container_type_name]" value="'+container_type_name+'" readonly>\
                        <input type="hidden" class="insert_product_code" name="product['+product_id+'][product_code]" value="'+product_code+'" readonly>\
                        <input type="hidden" class="insert_product_name" name="product['+product_id+'][product_name]" value="'+product_name+'" readonly>\
                        <input type="hidden" class="insert_chinese_shop" name="product['+product_id+'][chinese_shop]" value="'+chinese_shop+'" readonly>\
                        <input type="hidden" class="insert_transportation_type_id" name="product['+product_id+'][transportation_type_id]" value="'+transportation_type_id+'" readonly>\
                        <input type="hidden" class="insert_transportation_type_name" name="product['+product_id+'][transportation_type_name]" value="'+transportation_type_name+'" readonly>\
                        <input type="hidden" class="form-control insert_weigh_format" name="product['+product_id+'][weigh_format]" value="'+weigh_format+'" readonly>\
                        <input type="hidden" class="form-control insert_qty" name="product['+product_id+'][qty]" value="'+qty+'" readonly>\
                        <input type="hidden" class="form-control insert_qty" name="product['+product_id+'][lot_product]['+lot_weight_all+'][qty]" value="1" readonly>\
                    </td>\
                    <td>'+product_name+'</td>\
                    <td style=" text-align: center;">@lang('lang.wide') (@lang('lang.m'))</td>\
                    <td style="">\
                        <input type="text" name="product['+product_id+'][lot_product]['+lot_weight_all+'][size_wide]" class="form-control number-only insert_size_wide cal_size_wide" value="'+size_wide+'" min="0" style="border-color: #4CAF50;">\
                    </td>\
                    <td style=" text-align: center;"> x @lang('lang.long') (@lang('lang.m'))</td>\
                    <td style="">\
                        <input type="text" name="product['+product_id+'][lot_product]['+lot_weight_all+'][size_long]" class="form-control number-only insert_size_long cal_size_long" value="'+size_long+'" min="0" style="border-color: #4CAF50;">\
                    </td>\
                    <td style=" text-align: center;"> x @lang('lang.high') (@lang('lang.m'))</td>\
                    <td style="">\
                        <input type="text"name="product['+product_id+'][lot_product]['+lot_weight_all+'][size_high]" class="form-control number-only insert_size_high cal_size_high" value="'+size_high+'" min="0" style="border-color: #4CAF50;">\
                    </td>\
                    <td style=" text-align: center;"> = </td>\
                    <td style="">\
                        <input type="hidden" name="product['+product_id+'][lot_product]['+lot_weight_all+'][size_cubic]" class="form-control number-only insert_size_cubic cal_size_cubic" value="'+size_cubic+'" min="0" readonly>\
                        <input type="text" name="product['+product_id+'][lot_product]['+lot_weight_all+'][size_cubic]" class="form-control number-only insert_show_size_cubic show_cal_size_cubic" value="'+show_size_cubic+'" min="0" readonly>\
                    </td>\
                    <td style=" text-align: center;">@lang("lang.queue")</td>\
                    <td style="">\
                        <input type="text" name="product['+product_id+'][lot_product]['+lot_weight_all+'][size_weight_tote]" class="form-control number-only insert_size_weight_tote" value="'+size_weight_tote+'" min="0" style="border-color: #4CAF50;">\
                    </td>\
                    <td style="">\
                        <input type="text" name="product['+product_id+'][lot_product]['+lot_weight_all+'][decription]" class="form-control insert_decription" value="'+decription+'" style="border-color: #4CAF50;">\
                    </td>\
                    <td style="">\
                        <a class="btn btn-insert-product-f btn-primary">@lang("lang.submit")</a>\
                    </td>\
                </tr>\
            ';
            $('#addWeighFormat_F').append(html);
        }
        $(this).closest('tr').remove();
    });

    $('body').on('keyup', '.cal_size_wide, .cal_size_long, .cal_size_high, .insert_product_no_max', function(e){
        console.log('success');
        var ele = $(this).closest('.sum_all');
        var size_wide   = $(ele).find('.cal_size_wide').val();
        var size_long   = $(ele).find('.cal_size_long').val();
        var size_high   = $(ele).find('.cal_size_high').val();
        var product_no_min   = $(ele).find('.insert_product_no_min').val();
        var product_no_max   = $(ele).find('.insert_product_no_max').val();
        var qty   = ((parseInt(product_no_max)-parseInt(product_no_min))+1);
        var cubic = ((size_wide) * (size_long) * (size_high));
        $(ele).find('.cal_size_cubic').val(Math.round(cubic * 1000) / 1000)
        $(ele).find('.show_cal_size_cubic').val(Math.round((cubic * qty) * 1000) / 1000)
    });

    $('body').on('click', '.btn-copy', function(){
        var ele = $(this).closest('tr');
        var pre_ele = $(this).closest('tr').prev();
        // console.log('test-copy');
        var size_wide = $(pre_ele).find('.insert_size_wide').val();
        if(typeof size_wide != 'undefined'){
            // console.log(1);
            // get data
            // var product_id                  = $(pre_ele).find('.insert_product_id').val();
            // var lot_product_id              = $(pre_ele).find('.insert_lot_product_id').val();
            // var product_no_min              = $(pre_ele).find('.insert_product_no_min').val();
            // var product_no_max              = $(pre_ele).find('.insert_product_no_max').val();
            // var product_no_max_old          = $(pre_ele).find('.insert_product_no_max_old').val();
            // var product_type_id             = $(pre_ele).find('.insert_product_type_id').val();
            // var product_type_name           = $(pre_ele).find('.insert_product_type_name').val();
            // var container_type_id           = $(pre_ele).find('.insert_container_type_id').val();
            // var container_type_name         = $(pre_ele).find('.insert_container_type_name').val();
            // var product_name                = $(pre_ele).find('.insert_product_name').val();
            // var product_code                = $(pre_ele).find('.insert_product_code').val();
            // var chinese_shop                = $(pre_ele).find('.insert_chinese_shop').val();
            // var transportation_type_id      = $(pre_ele).find('.insert_transportation_type_id').val();
            // var transportation_type_name    = $(pre_ele).find('.insert_transportation_type_name').val();
            // var qty                         = $(pre_ele).find('.insert_qty').val();
            // var weigh_format                = $(pre_ele).find('.insert_weigh_format').val();
            // var size_weight_tote            = $(pre_ele).find('.insert_size_weight_tote').val();
            // var decription                  = $(pre_ele).find('.insert_decription').val();
            var size_wide                   = $(pre_ele).find('.insert_size_wide').val();
            var size_high                   = $(pre_ele).find('.insert_size_high').val();
            var size_long                   = $(pre_ele).find('.insert_size_long').val();
            var size_cubic                  = $(pre_ele).find('.insert_size_cubic').val();
            var show_size_cubic             = $(pre_ele).find('.insert_show_size_cubic').val();
            // set data
            // $(ele).find('.insert_product_id').val(product_id);
            // $(ele).find('.insert_lot_product_id').val(lot_product_id);
            // $(ele).find('.insert_product_no_min').val(product_no_min);
            // $(ele).find('.insert_product_no_max').val(product_no_max);
            // $(ele).find('.insert_product_no_max_old').val(product_no_max_old);
            // $(ele).find('.insert_product_type_id').val(product_type_id);
            // $(ele).find('.insert_product_type_name').val(product_type_name);
            // $(ele).find('.insert_container_type_id').val(container_type_id);
            // $(ele).find('.insert_container_type_name').val(container_type_name);
            // $(ele).find('.insert_product_name').val(product_name);
            // $(ele).find('.insert_product_code').val(product_code);
            // $(ele).find('.insert_chinese_shop').val(chinese_shop);
            // $(ele).find('.insert_transportation_type_id').val(transportation_type_id);
            // $(ele).find('.insert_transportation_type_name').val(transportation_type_name);
            // $(ele).find('.insert_qty').val(qty);
            // $(ele).find('.insert_weigh_format').val(weigh_format);
            // $(ele).find('.insert_size_weight_tote').val(size_weight_tote);
            // $(ele).find('.insert_decription').val(decription);
            $(ele).find('.insert_size_wide').val(size_wide);
            $(ele).find('.insert_size_high').val(size_high);
            $(ele).find('.insert_size_long').val(size_long);
            $(ele).find('.insert_size_cubic').val(size_cubic);
            $(ele).find('.insert_show_size_cubic').val(show_size_cubic);
        }else{
            // console.log('ไม่มีแถวก่อนหน้า');
        }
    });

    $('#FormEditWeighingLot').validate({
        errorElement: 'div',
        errorClass: 'invalid-feedback',
        focusInvalid: false,
        rules: {
            'receiver[customer_id]': {
                required: true,
            },
            'receiver[po_number]': {
                required: true,
            },
            'receiver[receiver_date]': {
                required: true,
            },
            // product_type_id: {
            //     required: true,
            // },
            // container_type_id: {
            //     required: true,
            // },
            // add_customer_id: {
            //     required: true,
            // },
            // product_name: {
            //     required: true,
            // },
            // chinese_shop: {
            //     required: true,
            // },
        },
        messages: {
            'receiver[customer_id]': {
                required: "กรุณาระบุ",
            },
            'receiver[po_number]': {
                required: "กรุณาระบุ",
            },
            'receiver[receiver_date]': {
                required: "กรุณาระบุ",
            },
            // product_type_id: {
            //     required: "กรุณาระบุ",
            // },
            // container_type_id: {
            //     required: "กรุณาระบุ",
            // },
            // add_customer_id: {
            //     required: "กรุณาระบุ",
            // },
            // product_name: {
            //     required: "กรุณาระบุ",
            // },
            // chinese_shop: {
            //     required: "กรุณาระบุ",
            // },
        },
        highlight: function (e) {
            validate_highlight(e);
        },
        success: function (e) {
            validate_success(e);
        },

        errorPlacement: function (error, element) {
            validate_errorplacement(error, element);
        },
        submitHandler: function (form) {
            /*
            if(CKEDITOR!==undefined){
                for ( instance in CKEDITOR.instances ){
                    CKEDITOR.instances[instance].updateElement();
                }
            }
            */
            $('.sum_all').find('.insert_size_weight_tote').val('');
            $('.sum_all').find('.insert_decription').val('');
            $('.sum_all').find('.insert_size_wide').val('');
            $('.sum_all').find('.insert_size_long').val('');
            $('.sum_all').find('.insert_size_high').val('');
            $('.sum_all').find('.insert_size_cubic').val('');

            var btn = $(form).find('[type="submit"]');
            var data_ar = removePriceFormat(form,$(form).serializeArray());
            var id = $('#receiver_id').val();
            btn.button("loading");
            $.ajax({
                method : "POST",
                url : url_gb+"/admin/ImportToChaina/WeighLot/"+id,
                dataType : 'json',
                data : $(form).serialize()
            }).done(function(rec){
                btn.button("reset");
                if(rec.status==1){
                    swal(rec.title,rec.content,"success");
                    window.location.href = url_gb+"/admin/{{$lang}}/ImportToChaina";
                }else{
                    swal(rec.title,rec.content,"error");
                }
            }).fail(function(){
                swal("system.system_alert","system.system_error","error");
                btn.button("reset");
            });
        },
        invalidHandler: function (form) {

        }
    });

</script>
@endsection
