<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>เอกสารตู้จีน</title>
<style>
    @font-face {
        font-family: 'THSarabunNew';
        font-style: normal;
        font-weight: normal;
        src: url("{{ public_path('fonts/THSarabunNew.ttf') }}") format('truetype');
    }
    div {
        float: left;
    }
    .img_background{
        /* background-image: url("http://localhost/ThaweSubSomdet/public/uploads/guarantee.jpg"); */
        background-size: 100%;
        background-repeat: no-repeat;
    }
    .text-right{
        text-align: right;
    }
    .text-left{
        text-align: left;
    }
    .text-center{
        text-align: center;
    }
    .col-12{
        width: 100%;
    }
    .border{
        border: #000 solid 1px;
    }
    .border_none{
        border: none;
    }
    .border_l_r{
        border-top: 0px;
        border-bottom: 0px;
        border-left: #000 solid 1px;
        border-right: #000 solid 1px;
    }
    .border_t_b{
        border-left: 0px;
        border-right: 0px;
        border-top: #000 solid 1px;
        border-bottom: #000 solid 1px;
    }
    th{
        border: #000 solid 1px;
        font-size: 10px;
        line-height: 20px;
    }
    td{
        line-height: 20px;
    }
</style>
<htmlpageheader name="page-header">
</htmlpageheader>
<htmlpagebody>
    <body lang="th" style="font-size: 12px; line-height: 20px;">
        <table class="col-12" border="1" cellspacing="0">
            <thead>
                <tr>
                    <td class="text-center" colspan="11">NEW CARGO (EK)
                        @if(!empty($QrCodeProduct[0]))
                            {{ date('d', strtotime($QrCodeProduct[0]->container_created_at)) }} 
                            (2)/{{ date('m/Y', strtotime($QrCodeProduct[0]->container_created_at)) }}
                        @endif
                    </td>
                    <td colspan="2" class="text-center">
                        รหัสตู้ที่มา: {{ !empty($QrCodeProduct[0]) ? $QrCodeProduct[0]->container_code : null }}
                    </td>
                </tr>
                <tr>
                    <td class="text-center">Receipt date</td>
                    <td class="text-center">No. Container</td>
                    <td class="text-center">Customer Code</td>
                    <td class="text-center">Po Number</td>
                    <td class="text-center">Item</td>
                    <td class="text-center" style="width: 20%;">Descriptions Of Good</td>
                    <td class="text-center">PCS</td>
                    <td class="text-center">Package</td>
                    <td class="text-center">Brand</td>
                    <td class="text-center">KG</td>
                    <td class="text-center">Cubic Meter</td>
                    <td class="text-center">Remark</td>
                    <td class="text-center">Remark</td>
                </tr>
            </thead>
            <tbody>
                @php
                    $check_lot_id = '';
                @endphp
                @foreach($QrCodeProduct as $product)
                    @php
                        $user_product_type_id = '';
                        if($product->user_product_type_id == 5){
                            $user_product_type_id = 'NO';
                        }else if($product->user_product_type_id == 6){
                            $user_product_type_id = 'YES';
                        }
                    @endphp
                    <tr>
                        <td class="text-center">{{date('Y/m/d', strtotime($product->product_created_at))}}</td>
                        <td class="text-center">{{$product->container_no}}</td>
                        <td class="text-center">{{$product->customer_general_code}}</td>
                        <td class="text-center">{{$product->po_no}}</td>
                        <td class="text-center">{{number_format($product->sort_id)}}</td>
                        <td class="text-left">{{$product->product_name}}</td>
                        @if($check_lot_id != $product->lot_product_id)
                            <td class="text-center">{{$product->product_pcs}}</td>
                            <td class="text-center">{{$product->qr_code_container_qty}}</td>
                            <td class="text-center">{{$user_product_type_id}}</td>
                        @else
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                        @endif
                        @if($check_lot_id != $product->lot_product_id)
                            @php $check_lot_id = $product->lot_product_id @endphp
                            <td class="text-center">{{number_format($product->weight_per_item, 3)}}</td>
                            <td class="text-center">{{number_format(($product->product_cobic/$product->lot_product_qty)/100, 3)}}</td>
                        @else
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                        @endif
                        <td class="text-center">{{$product->transport_type_name}}</td>
                        <td></td>
                    </tr>
                @endforeach
            
            </tbody>
        </table>
    </body>
</htmlpagebody>
<htmlpagefooter name="page-footer">
</htmlpagefooter>
