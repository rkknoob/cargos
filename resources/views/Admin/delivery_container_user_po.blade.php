﻿@extends('Admin.layouts.layout')
@section('css_bottom')
<style>
.fix-scroll-dashboard {
    width: 100%;
    overflow-x: scroll;
}
</style>
@endsection
@section('body')
<div class="content">
    <input type="hidden" id="edit_id" value="{{ $delivery_sheet_id }}">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="content">
                        <h4 class="title">
                            {{$title_page or '' }}
                            <!-- <a href="{{ url('/admin/'.$lang.'/DeliveryContainer/Create') }}" class="btn btn-success btn-add pull-right" >
                                + @lang('lang.create_data')
                            </a> -->
                        </h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <div class="material-datatables">
                            <div class="table-responsive">
                                <table id="TableList" class="table table-striped table-no-bordered table-hover" style="width:100%;cellspacing:0">
                                    <thead>
                                        <tr>
                                            <th>@lang('lang.invoice_no')</th>
                                            <th>@lang('lang.number_po')</th>
                                            <th>@lang('lang.container_no')</th>
                                            <th>@lang('lang.customer_id')</th>
                                            <th>@lang('lang.delivery_date')</th>
                                            <th>@lang('lang.total')</th>
                                            <th>@lang('lang.status')</th>
                                            <th>@lang('lang.tool')</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('modal')
<!-- <div class="modal" id="ModalDelivery" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <input type="hidden" id="edit_dalivery_by_container_id">
            <form id="FormAddressShipping">
                <div class="modal-header"><h4>@lang('lang.manage_address')</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="content" id="show-address-shipping" style="text-align: center;">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-check">
                        <label for="edit_user_address_id">เลือกที่อยู่จัดส่ง</label>
                        <select name="user_address_id" class="select2 form-control" tabindex="-1" data-placeholder="เลือกที่อยู่จัดส่ง" id="edit_user_address_id" >

                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">@lang('lang.save')</button>
                </div>
            </form>
        </div>
    </div>
</div> -->

<div class="modal" id="ModalAddressShipping" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <input type="hidden" name="dalivery_sheets_id" id="edit_dalivery_sheets_id">
            <form id="FormAddressShipping">
                <div class="modal-header"><h4>@lang('lang.address_delivery')</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="">
                                    <div class="content" id="show-address-shipping" style="text-align: center;">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="form-check">
                        <label for="edit_user_address_id">เลือกที่อยู่จัดส่ง</label>
                        <select name="user_address_id" class="select2 form-control" tabindex="-1" data-placeholder="เลือกที่อยู่จัดส่ง" id="edit_user_address_id" >

                        </select>
                    </div> -->
                </div>
                <div class="modal-footer">
                    <!-- <button type="submit" class="btn btn-primary">@lang('lang.save')</button> -->
                    <button type="button" class="btn btn-warning" data-dismiss="modal" aria-label="Close">@lang('lang.close')</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" id="ModalStatus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="FormShippingStatus">
                <input type="hidden" id="edit_delivery_id">
                <div class="modal-header"><h4>@lang('lang.change_status_delivery')</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                   <!--  <div class="form-check">
                        <label for="add_container_id">เลือกสถานะการชำระ</label>
                        <select name="status_payment" class="select2 form-control" tabindex="-1" data-placeholder="เลือกสถานะการชำระ" id="edit_status_payment" >
                            <option value="F">ยังไม่มีการชำระ</option>
                            <option value="T">ชำระเรียบร้อยแล้ว</option>
                        </select>
                    </div> -->
                    <div class="form-check">
                        <label for="add_container_id">เลือกสถานะการจัดส่ง</label>
                        <select name="status_delivery" class="select2 form-control" tabindex="-1" data-placeholder="เลือกสถานะการชำระ" id="edit_status_delivery" >
                            <option value="">เลือกสถานะ</option>
                            <option value="W">กำลังจัดส่งสินค้า</option>
                            <option value="S">จัดส่งเรียบร้อย</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">@lang('lang.save')</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('js_bottom')
<script src="{{asset('assets/global/plugins/orakuploader/orakuploader.js')}}"></script>
<script>

    var TableList = $('#TableList').dataTable({
        "ajax": {
            "url": url_gb+"/admin/{{$lang}}/DeliveryContainerUserPoLists",
            "data": function ( d ) {
                d.id = $('#edit_id').val();
                // d.custom = $('#myInput').val();
                // etc
            }
        },
        //"order": [[ 3, "desc" ]],
        "columns": [
            {"data" : "delivery_no", "name": "dalivery_sheets.delivery_no"},
            {"data" : "po_no", "name": "import_to_chaina.po_no"},
            {"data" : "container_code", "name": "containers.container_code"},
            {"data" : "customer_general_code", "name": "users.customer_general_code"},
            {"data" : "delivery_slip_date", "name": "dalivery_sheets.delivery_slip_date"},
            {"data" : "total_price", "searchable" : false, "orderable" : false},
            {"data" : "status_delivery", "searchable" : false, "orderable" : false},
            { "data": "action","className":"action text-center","searchable" : false , "orderable" : false }
        ]
    });

    // $('body').on('click','.btn-delivery',function(data){
    //     var id = $(this).data('id');
    //     $('#edit_dalivery_by_container_id').val(id);
    //     ShowModal('ModalDelivery');
    // });

    $('body').on('click','.btn-ststus',function(data){
        var id = $(this).data('id');
        $('#edit_delivery_id').val(id);
        $.ajax({
            method : "GET",
            url : url_gb+"/admin/DeliveryContainer/GetStatusShippingUserPo/"+id,
            dataType : 'json'
        }).done(function(rec){
            $('#edit_status_payment').val(rec.status_payment);
            ShowModal('ModalStatus');
        }).fail(function(){
            swal("system.system_alert","system.system_error","error");
        });
    });

    $('body').on('click','.btn-change-delivery-slip',function(data){
        var id = $(this).data('id');
        var user_id = $(this).data('user-id');
        var html = '';
        var html_address = '';
        $('#edit_dalivery_sheets_id').val(id);
        $('#show-address-shipping').html('');
        $('#edit_user_address_id').html('');
        $.ajax({
            method : "GET",
            url : url_gb+"/admin/DeliveryContainer/GetUserAddress/"+id,
            dataType : 'json'
        }).done(function(rec){
            ShowModal('ModalAddressShipping');
            html += '<h5>'+rec.DaliverySheet.address+'</h5>';
            html += rec.DaliverySheet.amphure != null ? rec.DaliverySheet.amphure.amphure_name+' ' : '';
            html += rec.DaliverySheet.province != null ? rec.DaliverySheet.province.province_name+' ' : '';
            html += rec.DaliverySheet.amphure != null ? rec.DaliverySheet.zipcode : '';

            $('#show-address-shipping').append(html);
            
        }).fail(function(){
            swal("system.system_alert","system.system_error","error");
        });
    });

    $('#FormAddressShipping').validate({
        errorElement: 'div',
        errorClass: 'invalid-feedback',
        focusInvalid: false,
        rules: {
            user_address_id: {
                required: true,
            }
        },
        messages: {
            user_address_id: {
                required: "กรุณาระบุ",
            }
        },
        highlight: function (e) {
            validate_highlight(e);
        },
        success: function (e) {
            validate_success(e);
        },

        errorPlacement: function (error, element) {
            validate_errorplacement(error, element);
        },
        submitHandler: function (form) {
            
            if(CKEDITOR!==undefined){
                for ( instance in CKEDITOR.instances ){
                    CKEDITOR.instances[instance].updateElement();
                }
            }
            
            var btn = $(form).find('[type="submit"]');
            var id = $('#edit_dalivery_sheets_id').val();
            var data_ar = removePriceFormat(form,$(form).serializeArray());


            btn.button("loading");
            $.ajax({
                method : "POST",
                url : url_gb+"/admin/{{$lang}}/DeliveryContainer/UserAddress/"+id,
                dataType : 'json',
                data : $(form).serialize()
            }).done(function(rec){
                btn.button("reset");

                if(rec.status==1){
                    swal(rec.title,rec.content,"success");
                    resetFormCustom(form);
                    $('#ModalAddressShipping').modal('hide');
                }else{
                    swal(rec.title,rec.content,"error");
                }
            }).fail(function(){
                swal("system.system_alert","system.system_error","error");
                btn.button("reset");
            });
        },
        invalidHandler: function (form) {

        }
    });

    $('#FormShippingStatus').validate({
        errorElement: 'div',
        errorClass: 'invalid-feedback',
        focusInvalid: false,
        rules: {

        },
        messages: {
        },
        highlight: function (e) {
            validate_highlight(e);
        },
        success: function (e) {
            validate_success(e);
        },

        errorPlacement: function (error, element) {
            validate_errorplacement(error, element);
        },
        submitHandler: function (form) {
            /*
            if(CKEDITOR!==undefined){
                for ( instance in CKEDITOR.instances ){
                    CKEDITOR.instances[instance].updateElement();
                }
            }
            */
            var btn = $(form).find('[type="submit"]');
            var id = $('#edit_delivery_id').val();
            btn.button("loading");
            $.ajax({
                method : "POST",
                url : url_gb+"/admin/{{$lang}}/DeliveryContainerUserPo/StatusShippingUserPo/"+id,
                dataType : 'json',
                data : $(form).serialize()
            }).done(function(rec){
                if(rec.status==1){
                    TableList.api().ajax.reload();
                    swal(rec.title,rec.content,"success");
                    $('#ModalStatus').modal('hide');
                }else{
                    swal(rec.title,rec.content,"error");
                }
            }).fail(function(){
                swal("system.system_alert","system.system_error","error");
                btn.button("reset");
            });
        },
        invalidHandler: function (form) {

        }
    });


</script>
@endsection
