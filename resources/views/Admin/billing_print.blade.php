<!--
 <style>
    @page{
        header: page-header;
        footer: page-footer;
        margin-top: 50px;
        margin-bottom: 150px;
    }
    .page-break {
        page-break-after: always;
    }
    body{
        font-family: Arial;
    }
    td {
        /height: 25px;
    }
    th {
        font-weight: normal;
    }
</style>
-->

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>ใบวางบิล / รายการสินค้า</title>
        <style>
            @font-face {
                font-family: 'THSarabunNew';
                font-style: normal;
                font-weight: normal;
                src: url("{{ asset('fonts/THSarabunNew.ttf') }}") format('truetype');
            }

            body {
                font-family: "THSarabunNew";
            }
            .table_css td {
                height: 25px;
                font-family: "THSarabunNew";
                font-size: 16px;
            }
            .table_css th {
                font-weight: normal;
                font-family: "THSarabunNew";
                font-size: 16px;
            }
             .table_list td {
                height: 25px;
                font-family: "THSarabunNew";
                font-size: 16px;
            }
            .table_list th {
                font-weight: normal;
                font-family: "THSarabunNew";
                font-size: 16px;
            }
        </style>
    </head>
    <body lang="th">
        <htmlpageheader name="page-header">

        </htmlpageheader>

        <htmlpagebody>
            <div class="row">
                <div style="color: black; text-align: left; font-size: 13px; ">

                </div>
            </div>
            <div class="row">
                <div style="color: black; text-align: left;">
                    <p style="font-size: 16px;"><b>นิวคาร์โก้</b></p>
                    <p style="font-size: 14px; padding-top: -25px;">104/8 แขวงศาลาธรรมสพน์ เขตทวีวัฒนา กรุงเทพมหานคร 10170</p>
                    <p style="font-size: 18px; text-align: center;"><b>ใบวางบิล</b></p>
                </div>
            </div>
            <div class="row">
                <table style="font-size: 16px; width: 800px;" class="table_css" border="0">
                    <tr>
                        <th style="color: black; text-align: left; width: 150px;">รหัสลูกค้า:</th>
                        <th style="color: black; text-align: left; width: 250px;">{{ $Billing->customer_general_code }}</th>
                        <th style="color: black; text-align: left; width: 150px;">เลขที่ใบวางบิล</th>
                        <th style="color: black; text-align: left; width: 250px;">{{ $Billing->billing_no }}</th>
                    </tr>
                    <tr>
                        <th style="color: black; text-align: left;">คุณ:</th>
                        <th style="color: black; text-align: left;">{{ $Billing->firstname.' '.$Billing->lastname }}</th>
                        <th style="color: black; text-align: left;">วันที่</th>
                        <th style="color: black; text-align: left;">{{ $Billing->billing_date }}</th>
                    </tr>
                    <tr>
                        <th style="color: black; text-align: left;"></th>
                        <th style="color: black; text-align: left;"></th>
                        <th style="color: black; text-align: left;">เงื่อนไขการชำระ</th>
                        <th style="color: black; text-align: left;"></th>
                    </tr>
                    <!-- <tr>
                        <th style="color: black; text-align: left;">หมายเหตุ:</th>
                        <th colspan="3" style="color: black; text-align: left;">______________________________________________________________________</th>
                    </tr> -->
                </table>
            </div>

            <div class="row" style="padding-top: 20px;">
                <table style="font-size: 16px; width: 800px;" class="table_css" border="1" cellspacing="0">
                    <tr>
                        <th style="color: black; text-align: center;">No.</th>
                        <th style="color: black; text-align: center;">เลขที่ใบส่งของ</th>
                        <th style="color: black; text-align: center;">วันที่จัดส่ง</th>
                        <th style="color: black; text-align: center;">เลข PO</th>
                        <th style="color: black; text-align: center;">จำนวน</th>
                        <th style="color: black; text-align: center;">น้ำหนัก</th>
                        <th style="color: black; text-align: center;">ขนาดคิว</th>
                        <th style="color: black; text-align: center;">สถานะ</th>
                        <th style="color: black; text-align: center;">จำนวนเงิน</th>
                    </tr>
                    @php
                        $accrued = 0;
                    @endphp
                    @if(! $BillingLists->isEmpty())\
                        @foreach($BillingLists as $key => $BillingList)
                            <tr>
                                <td style="color: black; text-align: center;">{{ $key+1 }}</td>
                                <td style="color: black; text-align: left;">{{ $BillingList->delivery_no }}</td>
                                <td style="color: black; text-align: left;">{{ $BillingList->delivery_slip_date }}</td>
                                <td style="color: black; text-align: center;">{{ $BillingList->po_no }}</td>
                                <td style="color: black; text-align: right;">{{ number_format($BillingList->qty) }}</td>
                                <td style="color: black; text-align: right;">{{ number_format($BillingList->weight,2) }}</td>
                                <td style="color: black; text-align: right;">{{ number_format($BillingList->cubic,1) }}</td>
                                <td style="color: black; text-align: center;">{{ $BillingList->status == 'T' ? 'ชำระแล้ว' : 'ยังไม่มีชำระ' }}</td>
                                <td style="color: black; text-align: right;">{{ number_format($BillingList->subtotal,2) }}</td>
                            </tr>
                                @php
                                    $accrued += $BillingList->subtotal;
                                @endphp
                        @endforeach
                        <tr>
                            <th colspan="2" style="color: black; text-align: center;">รวมเงินทั้งสิ้น</th>
                            <th colspan="6" style="color: black; text-align: left;">({{ m2t($accrued) }})</th>
                            <th style="color: black; text-align: right;">{{ number_format($accrued, 2) }}</th>
                        </tr>
                    @endif
                </table>
            </div>

            <div class="row" style="padding-top: 20px;">
                <table style="font-size: 16px; width: 800px;" class="table_css" border="0" cellspacing="0">
                    <tr>
                        <th colspan="4" style="color: black; text-align: left;">หมายเหตุ: {!! $Billing->remark !!}</th>
                    </tr>
                    <br><br><br><br>
                    <tr>
                        <th style="color: black; text-align: left; width: 100px;">ชื่อผู้รับวางบิล</th>
                        <th style="color: black; text-align: left; width: 300px;">_________________________</th>
                        <th colspan="2" style="color: black; text-align: left; width: 400px;">ในนาม นิวคาร์โก้</th>
                    </tr>
                    <tr>
                        <th style="color: black; text-align: left; width: 100px;">วันที่รับ</th>
                        <th style="color: black; text-align: left; width: 300px;">_____/_____/_____</th>
                        <th style="color: black; text-align: left; width: 100px;"></th>
                        <th style="color: black; text-align: left; width: 300px;"></th>
                    </tr>
                    <tr>
                        <th style="color: black; text-align: left;">วันที่นัดรับเช็ค</th>
                        <th style="color: black; text-align: left;">_____/_____/_____</th>
                        <th style="color: black; text-align: left;">ชื่อผู้วางบิล</th>
                        <th style="color: black; text-align: left;">_________________________</th>
                    </tr>
                </table>
            </div>

        </htmlpagebody>

        <htmlpagefooter name="page-footer">
            <div style="padding-bottom: 40px;text-align: center;">

            </div>
        </htmlpagefooter>
    </body>
