@extends('Admin.layouts.layout')
@section('css_bottom')
<style>
.fix-scroll-dashboard {
    width: 100%;
    overflow-x: scroll;
}
</style>
@endsection
@section('body')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="content">
                        <h4 class="title">
                            {{$title_page or '' }}
                            <button class="btn btn-success btn-add pull-right" >
                                + @lang('lang.create_data')
                            </button>
                        </h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <div class="material-datatables fix-scroll-dashboard">
                            <div class="table-responsive" style="overflow: scroll;">
                                <table id="TableList" class="table table-striped table-no-bordered table-hover" style="width:100%;cellspacing:0">
                                    <thead>
                                        <tr>
                                            <th>@lang('lang.customer_id')</th>
                                            <th>@lang('lang.first_name') - @lang('lang.last_name')</th>
                                            <th>@lang('lang.first_name')</th>
                                            <th>@lang('lang.last_name')</th>
                                            <th>@lang('lang.type_customer')</th>
                                            <th>@lang('lang.telephone_number_main')</th>
                                            <th>@lang('lang.telephone_number_second')</th>
                                            <th>@lang('lang.line_id')</th>
                                            <th>@lang('lang.wechat_id')</th>
                                            <th>@lang('lang.email')</th>
                                            <th>@lang('lang.tool')</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('modal')
<div class="modal" id="ModalAdd" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form id="FormAdd">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">@lang('lang.create_data') {{$title_page or 'ข้อมูลใหม่'}}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="add_customer_general_code">@lang('lang.customer_id')</label>
                            <input type="text" class="form-control" name="customer_general_code" id="add_customer_general_code" placeholder="@lang('lang.customer_id')">
                        </div>
                    </div>
                    <!-- div class="col-md-4">
                        <div class="form-group">
                            <label for="add_customer_brand_code">รหัสแบรนด์ลูกค้า</label>
                            <input type="text" class="form-control" name="customer_brand_code" id="add_customer_brand_code" placeholder="รหัสแบรนด์ลูกค้า">
                        </div>
                    </div> -->
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="add_product_type_id">@lang('lang.type_product')</label>
                            <select type="text" class="select2 form-control" name="product_type_id" id="add_product_type_id">
                                <option value="">@lang('lang.type_product')</option>
                                @if(! $ProductTypes->isEmpty())
                                    @foreach($ProductTypes as $ProductType)
                                        <option value="{{$ProductType->id}}">{{$ProductType->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">

                        <div class="form-group">
                            <label for="add_prefix_id">@lang('lang.perfix')</label>
                            <select type="text" class="select2 form-control" name="prefix_id" id="add_prefix_id">
                                <option value="">@lang('lang.perfix')</option>
                                @if(! $Perfixs->isEmpty())
                                    @foreach($Perfixs as $Perfix)
                                        <option value="{{$Perfix->id}}">{{$Perfix->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="add_firstname">@lang('lang.first_name')</label>
                            <input type="text" class="form-control" name="firstname" id="add_firstname" placeholder="@lang('lang.name')">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="add_lastname">@lang('lang.last_name')</label>
                            <input type="text" class="form-control" name="lastname" id="add_lastname" placeholder="@lang('lang.last_name')">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="add_nickname">@lang('lang.nickname')</label>
                            <input type="text" class="form-control" name="nickname" id="add_nickname" placeholder="@lang('lang.nickname')">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="add_main_mobile">@lang('lang.telephone_number_main')</label>
                            <input type="text" class="form-control" name="main_mobile" id="add_main_mobile" placeholder="@lang('lang.telephone_number_main')">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="add_sub_mobile">@lang('lang.telephone_number_second')</label>
                            <input type="text" class="form-control" name="sub_mobile" id="add_sub_mobile" placeholder="@lang('lang.telephone_number_second')">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="add_address">@lang('lang.address')</label>
                            <input type="text" class="form-control" name="address" id="add_address" placeholder="@lang('lang.address')">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="add_email">@lang('lang.email')</label>
                            <input type="email" class="form-control" name="email" id="add_email" placeholder="@lang('lang.email')">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="add_wechat_id">@lang('lang.wechat_id')</label>
                            <input type="text" class="form-control" name="wechat_id" id="add_wechat_id" placeholder="@lang('lang.wechat_id')">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="add_line_id">@lang('lang.line_id')</label>
                            <input type="text" class="form-control" name="line_id" id="add_line_id" placeholder="@lang('lang.line_id')">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="password">@lang('lang.password')</label>
                            <input type="password" class="form-control" name="password" id="password" placeholder="@lang('lang.password')">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="form-group">
                                <label for="password_confirmation">@lang('lang.confirm_password')</label>
                                <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="@lang('lang.confirm_password')">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="add_time_start_recive">เวลาเริ่มต้นได้รับ</label>
                            <input type="time" class="form-control" name="time_start_recive" id="add_time_start_recive" placeholder="เวลาเริ่มต้นได้รับ">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="form-group">
                                <label for="add_time_end_recive">เวลาสิ้นสุดได้รับ</label>
                                <input type="time" class="form-control" name="time_end_recive" id="add_time_end_recive" placeholder="เวลาสิ้นสุดได้รับ">
                            </div>
                        </div>
                    </div>
                </div> -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="add_address">@lang('lang.note')</label>
                            <input type="text" class="form-control" name="remark" id="add_remark" placeholder="@lang('lang.note')">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">@lang('lang.close')</button>
                <button type="submit" class="btn btn-primary">@lang('lang.save')</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" id="ModalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form id="FormEdit">
            <input type="hidden" id="edit_user_id">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">@lang('lang.edit_data') {{$title_page or 'ข้อมูลใหม่'}}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="edit_customer_general_code">@lang('lang.customer_id')</label>
                            <input type="text" class="form-control" name="customer_general_code" id="edit_customer_general_code" placeholder="@lang('lang.customer_id')">
                        </div>
                    </div>
                    <!-- <div class="col-md-4">
                        <div class="form-group">
                            <label for="edit_customer_brand_code">รหัสแบรนด์ลูกค้า</label>
                            <input type="text" class="form-control" name="customer_brand_code" id="edit_customer_brand_code" placeholder="รหัสแบรนด์ลูกค้า">
                        </div>
                    </div> -->
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="edit_product_type_id">@lang('lang.producttype')</label>
                            <select type="text" class="select2 form-control" name="product_type_id" id="edit_product_type_id">
                                <option value="">@lang('lang.producttype')</option>
                                @if(! $ProductTypes->isEmpty())
                                    @foreach($ProductTypes as $ProductType)
                                        <option value="{{$ProductType->id}}">{{$ProductType->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="edit_prefix_id">@lang('lang.perfix')</label>
                            <select type="text" class="select2 form-control" name="prefix_id" id="edit_prefix_id">
                                <option value="">@lang('lang.perfix')</option>
                                @if(! $Perfixs->isEmpty())
                                    @foreach($Perfixs as $Perfix)
                                        <option value="{{$Perfix->id}}">{{$Perfix->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="edit_firstname">@lang('lang.first_name')</label>
                            <input type="text" class="form-control" name="firstname" id="edit_firstname" placeholder="@lang('lang.first_name')">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="edit_lastname">@lang('lang.last_name')</label>
                            <input type="text" class="form-control" name="lastname" id="edit_lastname" placeholder="@lang('lang.last_name')">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="edit_nickname">@lang('lang.nickname')</label>
                            <input type="text" class="form-control" name="nickname" id="edit_nickname" placeholder="@lang('lang.nickname')">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="edit_main_mobile">@lang('lang.telephone_number_main')</label>
                            <input type="text" class="form-control" name="main_mobile" id="edit_main_mobile" placeholder="@lang('lang.telephone_number_main')">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="edit_sub_mobile">@lang('lang.telephone_number_second')</label>
                            <input type="text" class="form-control" name="sub_mobile" id="edit_sub_mobile" placeholder="@lang('lang.telephone_number_second')">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="edit_address">@lang('lang.address')</label>
                            <input type="text" class="form-control" name="address" id="edit_address" placeholder="@lang('lang.address')">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="edit_email">@lang('lang.email')</label>
                            <input type="email" class="form-control" name="email" id="edit_email" placeholder="@lang('lang.email')">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="edit_wechat_id">@lang('lang.wechat_id')</label>
                            <input type="text" class="form-control" name="wechat_id" id="edit_wechat_id" placeholder="@lang('lang.wechat_id')">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="edit_line_id">@lang('lang.line_id')</label>
                            <input type="text" class="form-control" name="line_id" id="edit_line_id" placeholder="@lang('lang.line_id')">
                        </div>
                    </div>
                </div>
                <!-- <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="edit_time_start_recive">เวลาเริ่มต้นได้รับ</label>
                            <input type="time" class="form-control" name="time_start_recive" id="edit_time_start_recive" placeholder="เวลาเริ่มต้นได้รับ">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="form-group">
                                <label for="edit_time_end_recive">เวลาสิ้นสุดได้รับ</label>
                                <input type="time" class="form-control" name="time_end_recive" id="edit_time_end_recive" placeholder="เวลาสิ้นสุดได้รับ">
                            </div>
                        </div>
                    </div>
                </div> -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="edit_remark">@lang('lang.note')</label>
                            <input type="text" class="form-control" name="remark" id="edit_remark" placeholder="@lang('lang.note')">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">@lang('lang.close')</button>
                <button type="submit" class="btn btn-primary">@lang('lang.save')</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" id="ModalChangePassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="FormChangePassword">
            <input type="hidden" id="change_user_id">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">@lang('lang.change_password')</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="change_password">@lang('lang.new_password')</label>
                    <input type="password" class="form-control bg-gray-lighter" name="password" id="change_password" placeholder="@lang('lang.new_password')">
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> @lang('lang.save')</button>
                <button type="button" class="btn btn-warning text-left" data-dismiss="modal" aria-label="Close">@lang('lang.close')</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" id="ModalAddress" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">@lang('lang.manage_address'){{$title_page or 'ข้อมูลใหม่'}} <label id="car_mark_name" data-userid=""></label></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <form id="FormAddAddress">
                        <input type="hidden" id="edit_user_address_id" name="user_id">
                        <input type="hidden" id="edit_address_id" name="id">

                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="add_name">@lang('lang.name')</label>
                                    <input type="text" class="form-control" name="name" id="add_name" required="" placeholder="@lang('lang.name')">
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <label for="add_address">@lang('lang.address')</label>
                                    <input type="text" class="form-control" name="address" id="add_user_address" required="" placeholder="@lang('lang.address')">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="add_province_id">@lang('lang.province_name')</label>
                                    <select type="text" class="select2 form-control" name="province_id" id="add_province_id">
                                        <option value="">@lang('lang.province_name')</option>
                                        @if(! $Provinces->isEmpty())
                                            @foreach($Provinces as $Province)
                                                <option value="{{$Province->id}}">{{$Province->province_name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="add_amphur_id">@lang('lang.amphure_name')</label>
                                    <select type="text" class="select2 form-control" name="amphur_id" id="add_amphur_id">
                                        <option value="">@lang('lang.amphure_name')</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="add_zipcode">@lang('lang.zipcode')</label>
                                    <input type="text" class="form-control" name="zipcode" id="add_zipcode" placeholder="@lang('lang.zipcode')" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12" style="text-align: center; margin-bottom: 50px;">
                            <button type="button" class="btn btn-danger reset-form-address">@lang('lang.cancel')</button>
                            <button type="submit" class="btn btn-primary">@lang('lang.save')</button>
                        </div>
                    </form>

                    <div class="material-datatables">
                        <div class="table-responsive">
                            <table id="TableListAddress" class="table table-striped table-no-bordered table-hover" style="width:100%;cellspacing:0">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>@lang('lang.name')</th>
                                        <th>@lang('lang.address')</th>
                                        <th>@lang('lang.amphure_name')</th>
                                        <th>@lang('lang.province_name')</th>
                                        <th>@lang('lang.zipcode')</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
        </div>
    </div>
</div>

<div class="modal" id="ModalPrice" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">@lang('lang.manage_price'){{$title_page or 'ข้อมูลใหม่'}} <label id="" data-userid=""></label></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <form id="FormAddPrice">
                        <input type="hidden" id="edit_user_price_id" name="user_id">
                        <input type="hidden" id="edit_rel_user_type_product_id" name="id">

                        <div class="row">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="rel_product_type_id">@lang('lang.type_product')</label>
                                    <select type="text" class="select2 form-control" name="product_type_id" id="rel_product_type_id">
                                        <option value="">@lang('lang.type_product')</option>
                                        @if(! $ProductTypes->isEmpty())
                                            @foreach($ProductTypes as $ProductType)
                                                <option value="{{$ProductType->id}}">{{$ProductType->name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="add_address">@lang('lang.manage_price')</label>
                                    <input type="text" class="form-control price" name="price" id="add_price" required="" placeholder="@lang('lang.manage_price')">
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12" style="text-align: center; margin-bottom: 50px;">
                            <button type="button" class="btn btn-danger reset-form-address">@lang('lang.cancel')</button>
                            <button type="submit" class="btn btn-primary">@lang('lang.save')</button>
                        </div>
                    </form>

                    <div class="material-datatables">
                        <div class="table-responsive" style="overflow: scroll;">
                            <table id="TableListPrice" class="table table-striped table-no-bordered table-hover" style="width:100%;cellspacing:0">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>@lang('lang.type_product')</th>
                                        <th>@lang('lang.manage_price')</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
        </div>
    </div>
</div>
@endsection
@section('js_bottom')
<script src="{{asset('assets/global/plugins/orakuploader/orakuploader.js')}}"></script>
<script>

    var TableList = $('#TableList').dataTable({
        "ajax": {
            "url": url_gb+"/admin/{{$lang}}/User/Lists",
            "data": function ( d ) {
                //d.myKey = "myValue";
                // d.custom = $('#myInput').val();
                // etc
            }
        },
        "columns": [
            {"data" : "customer_general_code"},
            {"data" : "customer_name", "searchable" : false, "orderable": false},
            {"data" : "firstname", "visible" : false},
            {"data" : "lastname", "visible" : false},
            {"data" : "customer_type", name:"product_types.name"},
            {"data" : "main_mobile"},
            {"data" : "sub_mobile"},
            {"data" : "line_id"},
            {"data" : "wechat_id"},
            {"data" : "email"},
            { "data": "action","className":"action text-center" , "searchable" : false, "orderable": false }
        ]
    });

    var TableListAddress = $('#TableListAddress').dataTable({
        "ajax": {
            "url": url_gb+"/admin/{{$lang}}/User/ListsAddress",
            "data": function ( d ) {
                //d.myKey = "myValue";
                d.id = $('#edit_user_address_id').val();
                // etc
            }
        },
        "columns": [
            { "data": "DT_Row_Index" , "className": "text-center", "searchable": false, "orderable": false  },
            {"data" : "name"},
            {"data" : "address"},
            {"data" : "amphure_name", "name": "Amphure.amphure_name"},
            {"data" : "province_name", "name": "Province.province_name"},
            {"data" : "zipcode"},
            { "data": "action","className":"action text-center" }
        ]
    });

    var TableListPrice = $('#TableListPrice').dataTable({
        "ajax": {
            "url": url_gb+"/admin/{{$lang}}/User/ListPrice",
            "data": function ( d ) {
                //d.myKey = "myValue";
                d.id = $('#edit_user_price_id').val();
                // etc
            }
        },
        "columns": [
            {"data": "DT_Row_Index" , "className": "text-center", "searchable": false, "orderable": false  },
            {"data" : "product_type_name"},
            {"data" : "price"},
            { "data": "action","className":"action text-center" }
        ]
    });

    $('body').on('click','.btn-add',function(data){
        ShowModal('ModalAdd');
    });

    $('body').on('click','.btn-address',function(data){
        var user_id = $(this).data('id');
        $('#edit_user_address_id').val(user_id);
        TableListAddress.api().ajax.reload();
        ShowModal('ModalAddress');
    });

    $('body').on('click','.btn-price',function(data){
        var user_id = $(this).data('id');
        $('#edit_user_price_id').val(user_id);
        TableListPrice.api().ajax.reload();
        ShowModal('ModalPrice');
    });

    $('body').on('click','.reset-form-address',function(data){
        $('#edit_address_id').val('');
        $('#add_name').val('');
        $('#add_user_address').val('');
        $('#add_zipcode').val('');
        $('#add_price').val('');

        $("#add_province_id").val('').trigger('change')
        $("#add_amphur_id").val('').trigger('change')
        $("#rel_product_type_id").val('').trigger('change')
    });

    $('body').on('click','.btn-change-password',function(data){
        var id = $(this).data('id');
        $('#change_user_id').val(id);
        ShowModal('ModalChangePassword');
    });

    $('body').on('click','.btn-edit',function(data){
        var btn = $(this);
        btn.button('loading');
        var id = $(this).data('id');
        $('#edit_user_id').val(id);
        $.ajax({
            method : "GET",
            url : url_gb+"/admin/User/"+id,
            dataType : 'json'
        }).done(function(rec){

            $('#edit_user_id').val(rec.id);
            $('#edit_customer_general_code').val(rec.customer_general_code);
            //$('#edit_customer_brand_code').val(rec.customer_brand_code);
            $('#edit_firstname').val(rec.firstname);
            $('#edit_lastname').val(rec.lastname);
            $('#edit_product_type_id').val(rec.product_type_id);
            $('#edit_product_type_id').select2('destroy').select2();
            $('#edit_prefix_id').val(rec.prefix_id);
            $('#edit_prefix_id').select2('destroy').select2();
            $('#edit_main_mobile').val(rec.main_mobile);
            $('#edit_sub_mobile').val(rec.sub_mobile);
            $('#edit_email').val(rec.email);
            $('#edit_wechat_id').val(rec.wechat_id);
            $('#edit_line_id').val(rec.line_id);
            $('#edit_address').val(rec.address);
            $('#edit_remark').val(rec.remark);
            $('#edit_time_start_recive').val(rec.time_start_recive);
            $('#edit_time_end_recive').val(rec.time_end_recive);

            btn.button("reset");
            ShowModal('ModalEdit');
        }).fail(function(){
            swal("system.system_alert","@lang('lang.system.system_error')","error");
            btn.button("reset");
        });
    });

    $('body').on('click','.btn-address-edit',function(){
        var btn = $(this);
        btn.button('loading');
        var id = $(this).data('id');
        $('#edit_user_id').val(id);
        $.ajax({
            method : "GET",
            url : url_gb+"/admin/{{$lang}}/User/EditAddress/"+id,
            dataType : 'json'
        }).done(function(rec){
            var amphures = rec.Amphures;

            $('#edit_address_id').val(rec.UserAddress.id);
            $('#add_name').val(rec.UserAddress.name);
            $('#add_user_address').val(rec.UserAddress.address);
            $('#add_zipcode').val(rec.UserAddress.zipcode);

            $('#add_province_id').val(rec.UserAddress.province_id);
            $('#add_province_id').select2('destroy').select2();
            $('#add_amphur_id').val('').empty();
            $('#add_amphur_id').append('<option value="">เลือกอำเภอ</option>');
            $.each(amphures, function(k,v){
               $('#add_amphur_id').append('<option value="' + v.id + '" data-zipcode="'+v.zipcode+'">' + v.amphure_name + '</option>');
            });
            $('#add_amphur_id').val(rec.UserAddress.amphur_id);
            $('#add_amphur_id').select2('destroy').select2();
            $('#add_zipcode').val(rec.UserAddress.zipcode);

            //btn.button("reset");
            //ShowModal('ModalEdit');
        }).fail(function(){
            swal("system.system_alert","@lang('lang.system.system_error')","error");
            btn.button("reset");
        });
    });

    $('body').on('click','.btn-price-edit',function(){
        var btn = $(this);
        btn.button('loading');
        var id = $(this).data('id');
        $.ajax({
            method : "GET",
            url : url_gb+"/admin/{{$lang}}/User/EditPrice/"+id,
            dataType : 'json'
        }).done(function(rec){
            $('#edit_rel_user_type_product_id').val(rec.RelUserTypeProduct.id);
            $('#add_price').val(rec.RelUserTypeProduct.price);
            $('#rel_product_type_id').val(rec.RelUserTypeProduct.product_type_id);
            $('#rel_product_type_id').select2('destroy').select2();

            //btn.button("reset");
            //ShowModal('ModalEdit');
        }).fail(function(){
            swal("system.system_alert","@lang('lang.system.system_error')","error");
            btn.button("reset");
        });
    });

    $('#FormAdd').validate({
        errorElement: 'div',
        errorClass: 'invalid-feedback',
        focusInvalid: false,
        rules: {
            // prefix_id: {
            //     required: true,
            // },
            customer_general_code: {
                required: true,
                // minlength: 5,
            },
            // customer_brand_code: {
            //     required: true,
            //     minlength: 5,
            // },
            // firstname: {
            //     required: true,
            // },
            // lastname: {
            //     required: true,
            // },
            // main_mobile: {
            //     required: true,
            // },
            product_type_id: {
                required: true,
            },
            email: {
                required: true,
                email: true,
            },
            time_start_recive: {
                required: true,
            },
            time_end_recive: {
                required: true,
            },
            password: {
                required: true,
                minlength: 6,
            },
            password_confirmation: {
                required: true,
                equalTo: '#password',
            },
        },
        messages: {
            prefix_id: {
                required: '@lang("lang.please_specify")',
            },
            customer_general_code: {
                required: '@lang("lang.please_specify")',
                minlength: '@lang("lang.length_of_at_least_5_characters")',
            },
            customer_brand_code: {
                required: '@lang("lang.please_specify")',
                minlength: '@lang("lang.length_of_at_least_5_characters")',
            },
            firstname: {
                required: '@lang("lang.please_specify")',
            },
            lastname: {
                required: '@lang("lang.please_specify")',
            },
            main_mobile: {
                required: '@lang("lang.please_specify")',
            },
            product_type_id: {
                required: '@lang("lang.please_specify")',
            },
            email: {
                required: '@lang("lang.please_specify")',
            },
            time_start_recive: {
                required: '@lang("lang.please_specify")',
            },
            time_end_recive: {
                required: '@lang("lang.please_specify")',
            },
            password: {
                required: '@lang("lang.please_specify")',
                minlength: "ความยาวอย่างน้อย 6 ตัวอักษร",
            },
            password_confirmation: {
                required: '@lang("lang.please_specify")',
                equalTo: "ยืนยันรหัสผ่านไม่ถูกต้อง",
            },
        },
        highlight: function (e) {
            validate_highlight(e);
        },
        success: function (e) {
            validate_success(e);
        },

        errorPlacement: function (error, element) {
            validate_errorplacement(error, element);
        },
        submitHandler: function (form) {
            /*
            if(CKEDITOR!==undefined){
                for ( instance in CKEDITOR.instances ){
                    CKEDITOR.instances[instance].updateElement();
                }
            }
            */
            var btn = $(form).find('[type="submit"]');
            var data_ar = removePriceFormat(form,$(form).serializeArray());
            btn.button("loading");
            $.ajax({
                method : "POST",
                url : url_gb+"/admin/{{$lang}}/User",
                dataType : 'json',
                data : $(form).serialize()
            }).done(function(rec){
                btn.button("reset");
                if(rec.status==1){
                    TableList.api().ajax.reload();
                    resetFormCustom(form);
                    swal(rec.title,rec.content,"success");
                    $('#ModalAdd').modal('hide');
                }else{
                    swal(rec.title,rec.content,"error");
                }
            }).fail(function(){
                swal("system.system_alert","@lang('lang.system.system_error')","error");
                btn.button("reset");
            });
        },
        invalidHandler: function (form) {

        }
    });

    $('#FormEdit').validate({
        errorElement: 'div',
        errorClass: 'invalid-feedback',
        focusInvalid: false,
        rules: {
            // prefix_id: {
            //     required: true,
            // },
            customer_general_code: {
                required: true,
                // minlength: 5,
            },
            // customer_brand_code: {
            //     required: true,
            //     minlength: 5,
            // },
            // firstname: {
            //     required: true,
            // },
            // lastname: {
            //     required: true,
            // },
            // main_mobile: {
            //     required: true,
            // },
            product_type_id: {
                required: true,
            },
            email: {
                required: true,
                email: true,
            },
            time_start_recive: {
                required: true,
            },
            time_end_recive: {
                required: true,
            },
            password: {
                required: true,
                minlength: 6,
            },
            password_confirmation: {
                required: true,
                equalTo: '#password',
            },
        },
        messages: {
            prefix_id: {
                required: '@lang("lang.please_specify")',
            },
            customer_general_code: {
                required: '@lang("lang.please_specify")',
                minlength: '@lang("lang.length_of_at_least_5_characters")',
            },
            customer_brand_code: {
                required: '@lang("lang.please_specify")',
                minlength: '@lang("lang.length_of_at_least_5_characters")',
            },
            firstname: {
                required: '@lang("lang.please_specify")',
            },
            lastname: {
                required: '@lang("lang.please_specify")',
            },
            main_mobile: {
                required: '@lang("lang.please_specify")',
            },
            product_type_id: {
                required: '@lang("lang.please_specify")',
            },
            email: {
                required: '@lang("lang.please_specify")',
            },
            time_start_recive: {
                required: '@lang("lang.please_specify")',
            },
            time_end_recive: {
                required: '@lang("lang.please_specify")',
            },
            password: {
                required: '@lang("lang.please_specify")',
                minlength: "ความยาวอย่างน้อย 6 ตัวอักษร",
            },
            password_confirmation: {
                required: '@lang("lang.please_specify")',
                equalTo: "ยืนยันรหัสผ่านไม่ถูกต้อง",
            },
        },
        highlight: function (e) {
            validate_highlight(e);
        },
        success: function (e) {
            validate_success(e);
        },

        errorPlacement: function (error, element) {
            validate_errorplacement(error, element);
        },
        submitHandler: function (form) {
            /*
            if(CKEDITOR!==undefined){
                for ( instance in CKEDITOR.instances ){
                    CKEDITOR.instances[instance].updateElement();
                }
            }
            */
            var btn = $(form).find('[type="submit"]');
            var id = $('#edit_user_id').val();
            btn.button("loading");
            $.ajax({
                method : "POST",
                url : url_gb+"/admin/{{$lang}}/User/"+id,
                dataType : 'json',
                data : $(form).serialize()
            }).done(function(rec){
                btn.button("reset");
                if(rec.status==1){
                    TableList.api().ajax.reload();
                    resetFormCustom(form);
                    swal(rec.title,rec.content,"success");
                    $('#ModalEdit').modal('hide');
                }else{
                    swal(rec.title,rec.content,"error");
                }
            }).fail(function(){
                swal("system.system_alert","@lang('lang.system.system_error')","error");
                btn.button("reset");
            });
        },
        invalidHandler: function (form) {

        }
    });

    $('#FormChangePassword').validate({
        errorElement: 'span',
        errorClass: 'help-block',
        focusInvalid: false,
        rules: {
            password: {
                required: true,
                minlength: 6,
                maxlength: 16
            }
        },
        messages: {
            password : {
                required : '@lang("lang.please_specify")',
                minlength: "ความยาวอย่างน้อย 6 ตัวอักษร",
                maxlength: "ความยาวไม่เกิน 16 ตัวอักษร",
            },
        },
        highlight: function (e) {
            validate_highlight(e);
        },
        success: function (e) {
            validate_success(e);
        },

        errorPlacement: function (error, element) {
            validate_errorplacement(error, element);
        },
        submitHandler: function (form) {
            var btn = $(form).find('[type="submit"]');
            var id = $('#change_user_id').val();
            // $('#user_id').val(id);
            btn.button("loading");
            $.ajax({
                method : "POST",
                url : url_gb+"/admin/{{$lang}}/User/change_password/"+id,
                dataType : 'json',
                data : $(form).serialize()
            }).done(function(rec){
                btn.button("reset");
                if(rec.status==1){
                    resetFormCustom(form);
                    swal(rec.title,rec.content,"success");
                    $('#ModalChangePassword').modal('hide');
                }else{
                    swal(rec.title,rec.content,"error");
                }
            }).fail(function(){
                swal("system.system_alert","@lang('lang.system.system_error')","error");
                btn.button("reset");
            });
        },
        invalidHandler: function (form) {

        }
    });

    $('#FormAddPrice').validate({
        errorElement: 'div',
        errorClass: 'invalid-feedback',
        focusInvalid: false,
        rules: {
            product_type_id: {
                required: true,
            },
            price: {
                required: true,
            },

        },
        messages: {
            product_type_id : {
                required : '@lang("lang.please_specify")',
            },
            price : {
                required : '@lang("lang.please_specify")',
            },
        },
        highlight: function (e) {
            validate_highlight(e);
        },
        success: function (e) {
            validate_success(e);
        },

        errorPlacement: function (error, element) {
            validate_errorplacement(error, element);
        },
        submitHandler: function (form) {
            var btn = $(form).find('[type="submit"]');
            var id = $('#edit_user_address_id').val();
            // $('#user_id').val(id);
            btn.button("loading");
            $.ajax({
                method : "POST",
                url : url_gb+"/admin/{{$lang}}/User/AddPrice",
                dataType : 'json',
                data : $(form).serialize()
            }).done(function(rec){
                btn.button("reset");
                if(rec.status==1){
                    //resetFormCustom(form);
                    console.log('success');
                    $('#add_price').val('');
                    $("#rel_product_type_id").val('').trigger('change')
                    TableListPrice.api().ajax.reload();
                    swal(rec.title,rec.content,"success");
                }else{
                    swal(rec.title,rec.content,"error");
                }
            }).fail(function(){
                swal("system.system_alert","@lang('lang.system.system_error')","error");
                btn.button("reset");
            });
        },
        invalidHandler: function (form) {

        }
    });

    $('#FormAddAddress').validate({
        errorElement: 'div',
        errorClass: 'invalid-feedback',
        focusInvalid: false,
        rules: {
            name: {
                required: true,
            },
            address: {
                required: true,
            },
            province_id: {
                required: true,
            },
            amphur_id: {
                required: true,
            },
            zipcode: {
                required: true,
            },
        },
        messages: {
            name : {
                required : '@lang("lang.please_specify")',
            },
            address : {
                required : '@lang("lang.please_specify")',
            },
            province_id : {
                required : '@lang("lang.please_specify")',
            },
            amphur_id : {
                required : '@lang("lang.please_specify")',
            },
            zipcode : {
                required : '@lang("lang.please_specify")',
            },
        },
        highlight: function (e) {
            validate_highlight(e);
        },
        success: function (e) {
            validate_success(e);
        },

        errorPlacement: function (error, element) {
            validate_errorplacement(error, element);
        },
        submitHandler: function (form) {
            var btn = $(form).find('[type="submit"]');
            var id = $('#edit_user_address_id').val();
            // $('#user_id').val(id);
            btn.button("loading");
            $.ajax({
                method : "POST",
                url : url_gb+"/admin/{{$lang}}/User/AddAddress",
                dataType : 'json',
                data : $(form).serialize()
            }).done(function(rec){
                btn.button("reset");
                if(rec.status==1){
                    //resetFormCustom(form);
                    $('#add_name').val('');
                    $('#add_user_address').val('');
                    $('#add_zipcode').val('');
                    $("#add_province_id").val('').trigger('change')
                    $("#add_amphur_id").val('').trigger('change')
                    $('#edit_address_id').val('');

                    TableListAddress.api().ajax.reload();
                    swal(rec.title,rec.content,"success");
                }else{
                    swal(rec.title,rec.content,"error");
                }
            }).fail(function(){
                swal("system.system_alert","@lang('lang.system.system_error')","error");
                btn.button("reset");
            });
        },
        invalidHandler: function (form) {

        }
    });

    $('body').on('click','.btn-address-delete',function(e){
        e.preventDefault();
        var btn = $(this);
        var id = btn.data('id');
        swal({
            title: "@lang('lang.do_you_want_to_delete')",
            text: "@lang('lang.if_you_delete_you_will_not_be_able_to_restore_it')้",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: "@lang('lang.yes_i_want_to_delete')",
            cancelButtonText: "@lang('lang.cancel')",
            showLoaderOnConfirm: true,
            buttonsStyling: false
        }).then(function() {
            $.ajax({
                method : "POST",
                url : url_gb+"/admin/{{$lang}}/User/AddressDelete/"+id,
                data : {ID : id}
            }).done(function(rec){
                if(rec.status==1){
                    swal(rec.title,rec.content,"success");
                    TableListAddress.api().ajax.reload();
                }else{
                    swal("ระบบมีปัญหา","กรุณาติดต่อผู้ดูแล","error");
                }
            }).fail(function(data){
                swal("ระบบมีปัญหา","กรุณาติดต่อผู้ดูแล","error");
            });
        }).catch(function(e){
            //console.log(e);
        });
    });

    $('body').on('click','.btn-price-delete',function(e){
        e.preventDefault();
        var btn = $(this);
        var id = btn.data('id');
        swal({
            title: "@lang('lang.do_you_want_to_delete')",
            text: "@lang('lang.if_you_delete_you_will_not_be_able_to_restore_it')้",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: "@lang('lang.yes_i_want_to_delete')",
            cancelButtonText: "@lang('lang.cancel')",
            showLoaderOnConfirm: true,
            buttonsStyling: false
        }).then(function() {
            $.ajax({
                method : "POST",
                url : url_gb+"/admin/{{$lang}}/User/PriceDelete/"+id,
                data : {ID : id}
            }).done(function(rec){
                if(rec.status==1){
                    swal(rec.title,rec.content,"success");
                    TableListPrice.api().ajax.reload();
                }else{
                    swal("ระบบมีปัญหา","กรุณาติดต่อผู้ดูแล","error");
                }
            }).fail(function(data){
                swal("ระบบมีปัญหา","กรุณาติดต่อผู้ดูแล","error");
            });
        }).catch(function(e){
            //console.log(e);
        });
    });

    $('body').on('click','.btn-delete',function(e){
        e.preventDefault();
        var btn = $(this);
        var id = btn.data('id');
        swal({
            title: "@lang('lang.do_you_want_to_delete')",
            text: "@lang('lang.if_you_delete_you_will_not_be_able_to_restore_it')้",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: "@lang('lang.yes_i_want_to_delete')",
            cancelButtonText: "@lang('lang.cancel')",
            showLoaderOnConfirm: true,
            buttonsStyling: false
        }).then(function() {
            $.ajax({
                method : "POST",
                url : url_gb+"/admin/{{$lang}}/User/Delete/"+id,
                data : {ID : id}
            }).done(function(rec){
                if(rec.status==1){
                    swal(rec.title,rec.content,"success");
                    TableList.api().ajax.reload();
                }else{
                    swal("ระบบมีปัญหา","กรุณาติดต่อผู้ดูแล","error");
                }
            }).fail(function(data){
                swal("ระบบมีปัญหา","กรุณาติดต่อผู้ดูแล","error");
            });
        }).catch(function(e){
            //console.log(e);
        });
    });

    $('body').on('change','#add_province_id',function(){
       var province_id = $('#add_province_id').val();
       $('#add_amphur_id').html('');
       $('#add_zipcode').val('');
       $.ajax({
           method : "GET",
           url: url_gb + "/admin/{{$lang}}/Amphure/GetAmphur/"+province_id,
           dataType : 'json',
       }).done(function(res){
           $('#add_amphur_id').append('<option value="">กรุณาเลือกอำเภอ</option>');
           $.each(res,function(k,v){
               $('#add_amphur_id').append('<option value="'+v.id+'" data-zipcode="'+v.zipcode+'">'+v.amphure_name+'</option>');
           });
       }).fail(function(){

       });
    });

    $('body').on('change','#add_amphur_id',function(){
        var zipcode = $('#add_amphur_id').find('option[value="'+$('#add_amphur_id').val()+'"]').attr('data-zipcode');
        $('#add_zipcode').val(zipcode);

    });

    $('body').on('change','#edit_province_id',function(){
       var province_id = $('#edit_province_id').val();
       $('#edit_amphur_id').html('');
       $('#edit_zipcode').val('');
       $.ajax({
           method : "GET",
           url: url_gb + "/admin/{{$lang}}/Amphure/GetAmphur/"+province_id,
           dataType : 'json',
       }).done(function(res){
           $('#edit_amphur_id').append('<option value="">กรุณาเลือกอำเภอ</option>');
           $.each(res,function(k,v){
               $('#edit_amphur_id').append('<option value="'+v.id+'" data-zipcode="'+v.zipcode+'">'+v.amphure_name+'</option>');
           });
       }).fail(function(){

       });
    });

   $('body').on('change','#edit_amphur_id',function(){
        var zipcode = $('#edit_amphur_id').find('option[value="'+$('#edit_amphur_id').val()+'"]').attr('data-zipcode');
        $('#edit_zipcode').val(zipcode);
    });

    $('#add_prefix_id').select2();
    $('#add_product_type_id').select2();
    $('#add_province_id').select2();
    $('#add_amphur_id').select2();
    $('#edit_prefix_id').select2();
    $('#edit_product_type_id').select2();
    $('#rel_product_type_id').select2();

</script>
@endsection
