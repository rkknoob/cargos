<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>เอกสารส่งด่าน</title>
<style>
    /*@font-face {
        font-family: 'THSarabunNew';
        font-style: normal;
        font-weight: normal;
        src: url("{{ asset('fonts/THSarabunNew.ttf') }}") format('truetype');
    }*/
    @font-face {
        font-family: 'SentyTang';
        font-style: normal;
        font-weight: normal;
        src: url("{{ asset('fonts/SentyTang.ttf') }}") format('truetype');
    }
    body {
        font-family: 'SentyTang';
        /*font-family: "THSarabunNew";*/
    }
    div {
        float: left;
    }
    .img_background{
        /* background-image: url("http://localhost/ThaweSubSomdet/public/uploads/guarantee.jpg"); */
        background-size: 100%;
        background-repeat: no-repeat;
    }
    .text-right{
        text-align: right;
    }
    .text-left{
        text-align: left;
    }
    .text-center{
        text-align: center;
    }
    .col-12{
        width: 100%;
    }
    .border{
        border: #000 solid 1px;
    }
    .border_none{
        border: none;
    }
    .border_l_r{
        border-top: 0px;
        border-bottom: 0px;
        border-left: #000 solid 1px;
        border-right: #000 solid 1px;
    }
    .border_t_b{
        border-left: 0px;
        border-right: 0px;
        border-top: #000 solid 1px;
        border-bottom: #000 solid 1px;
    }
    th{
        border: #000 solid 1px;
        font-size: 10px;
        line-height: 20px;
    }
    td{
        line-height: 20px;
    }
</style>
<htmlpageheader name="page-header">
</htmlpageheader>
<htmlpagebody>
    <body lang="th" style="font-size: 11px; line-height: 20px;">
        <div class="text-center col-12">
            <h3>NEW CARGO</h3>
        </div>
        <table class="col-12" border="1" cellspacing="0">
            <thead>
                <tr>
                    <td class="text-center">Receipt date</td>
                    <td class="text-center">Out of date</td>
                    <td class="text-center">No.Container<br>[Cabinet No.]</td>
                    <td class="text-center">Customer Code</td>
                    <td class="text-center">Po Number</td>
                    <td class="text-center">Item</td>
                    <td class="text-center">Descriptions Of Good</td>
                    <td class="text-center">Descriptions Of Good (ENG)</td>
                    <td class="text-center">Company</td>
                    <td class="text-center">(件数 PCS)</td>
                    <td class="text-center">Package</td>
                    <td class="text-center">Brand</td>
                    <td class="text-center">重量 (KG)</td>
                    <td class="text-center">重量 (Total KG)</td>
                    <td class="text-center" colspan="3">规格 (Size)</td>
                    <td class="text-center">立方米 (CBM)</td>
                    <td class="text-center">总立方米 (Total CBM)</td>
                    <td class="text-center">Remark</td>
                    <td class="text-center">Remark</td>
                </tr>
            </thead>
            <tbody>
                @php
                    $check_lot_id = '';
                    $cbm_total = 0;
                @endphp
                @foreach($LotProducts as $LotProduct)
                    @php
                        $user_product_type_id = '';
                        if($LotProduct->user_product_type_id == 5){
                            $user_product_type_id = 'NO';
                        }else if($LotProduct->user_product_type_id == 6){
                            $user_product_type_id = 'YES';
                        }

                        $width = $LotProduct->width;
                        $length = $LotProduct->length;
                        $height = $LotProduct->height;
                        // $LotProduct->lot_product_qty
                        $cbm = ($width * $length) * $height;
                        $cbm_total = ($cbm * $LotProduct->qr_code_container_qty);
                    @endphp
                    <tr>
                        <td class="text-center">{{date('Y/m/d', strtotime($LotProduct->product_created_at))}}</td>
                        <td class="text-center">{{date('Y/m/d', strtotime($LotProduct->container_created_at))}}</td>
                        <td class="text-center">{{$LotProduct->container_no}}</td>
                        <td class="text-center">{{$LotProduct->customer_general_code}}</td>
                        <td class="text-center">{{$LotProduct->po_no}}</td>
                        <td class="text-center">
                            @if($LotProduct->product_sort_start == $LotProduct->product_sort_end)
                                {{ $LotProduct->product_sort_start }}
                            @else
                                {{number_format($LotProduct->product_sort_start) .'-'. number_format($LotProduct->product_sort_end)}}
                            @endif
                        </td>
                        <td class="text-left">{{(!empty($LotProduct->product_name) ? $LotProduct->product_name : '' )}}</td>
                        <td class="text-left">{{(!empty($LotProduct->product_name_en) ? $LotProduct->product_name_en : '' )}}</td>
                        <td class="text-center">
                            {{(!empty($Company->company_name_th) ? $Company->company_name_th : '' )}}
                        </td>
                        @if($check_lot_id != $LotProduct->product_import_to_chaina_id)
                            @php $check_lot_id = $LotProduct->product_import_to_chaina_id @endphp
                            <td class="text-center">{{$LotProduct->product_pcs}}</td>
                        @else
                            <td class="text-center"></td>
                        @endif
                        <td class="text-center">{{$LotProduct->qr_code_container_qty}}</td>
                        <td class="text-center">{{$user_product_type_id}}</td>
                        <td class="text-center">{{number_format($LotProduct->weight_per_item, 2)}}</td>
                        <td class="text-center">{{number_format($LotProduct->weight_per_item * $LotProduct->qr_code_container_qty, 2)}}</td>
                        <td class="text-center">{{number_format($width,2)}}</td>
                        <td class="text-center">{{number_format($length,2)}}</td>
                        <td class="text-center">{{number_format($height,2)}}</td>
                        <td class="text-center">{{number_format($cbm,3)}}</td>
                        <td class="text-center">{{number_format($cbm_total,3)}}</td>
                        <td class="text-center">{{($LotProduct->transport_type_name_th == 'รถ') ? 'EK' : 'SEA'}}</td>
                        <td class="text-left">{{$LotProduct->remark}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </body>
</htmlpagebody>
<htmlpagefooter name="page-footer">
</htmlpagefooter>
