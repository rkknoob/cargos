<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>QR code</title>
<style>
    @font-face {
        font-family: 'THSarabunNew';
        font-style: normal;
        font-weight: normal;
        src: url("{{ public_path('fonts/THSarabunNew.ttf') }}") format('truetype');
    }
    div {
        float: left;
    }
    .img_background{
        /* background-image: url("http://localhost/ThaweSubSomdet/public/uploads/guarantee.jpg"); */
        background-size: 100%;
        background-repeat: no-repeat;
    }
    .text-right{
        text-align: right;
    }
    .text-left{
        text-align: left;
    }
    .text-center{
        text-align: center;
    }
    .col-12{
        width: 100%;
    }
    .border{
        border: #000 solid 1px;
    }
    .border_none{
        border: none;
    }
    .border_l_r{
        border-top: 0px;
        border-bottom: 0px;
        border-left: #000 solid 1px;
        border-right: #000 solid 1px;
    }
    .border_t_b{
        border-left: 0px;
        border-right: 0px;
        border-top: #000 solid 1px;
        border-bottom: #000 solid 1px;
    }
    th{
        border: #000 solid 1px;
        font-size: 10px;
        line-height: 20px;
    }
    td{
        line-height: 20px;
    }
    table{
        border: 0px;
    }
</style>
<htmlpageheader name="page-header">
</htmlpageheader>
<htmlpagebody>
    <body lang="th" style="font-size: 12px; line-height: 25px;">
        @php
            $run_lot = 1;
            $check_lot = 1;
        @endphp
        @if(!empty($QRCodes) && count($QRCodes) > 0)
            @foreach($QRCodes as $key => $QRCode)
            <!-- <div class="text-center" style="float: left; width: 80%; height: 100%; padding-top: 1.5cm; margin-left: 10%;">
                <barcode code="{{$QRCode->qr_code}}" type="QR" size="3"/>
                <br><br>รหัสลูกค้า: {{$PO->customer_general_code}}
                <br>PO: {{$QRCode->qr_code}}
                <br>วันที่รับเข้า: {{date("Y-m-d", strtotime($PO->import_date))}}
            </div> -->
            <div class="" style="float: left; width: 80%; height: 100%; padding-top: 1.5cm; margin-left: 10%;">
                <barcode code="{{$QRCode->qr_code}}" type="QR" size="3"/>
                <!-- <br>PO: {{$QRCode->qr_code}} -->
                <br><br>
                <table border="1" style="width: 92%;" cellpadding="0" cellspacing="0">
                    <tr>
                        <td colspan="2">&nbsp;Customer: <b>{{$PO->customer_general_code}}</b></td>
                    </tr>
                    <tr>
                        <td>&nbsp;PO No.: <b>{{$PO->po_no}}</b></td>
                        <td>&nbsp;PO Order: <b>{{$key+1 .'/'. $PO->qr_code_product_count}}</b></td>
                    </tr>
                    <tr>
                        <td>&nbsp;Lot No.: <b>{{$QRCode->lot_product_id}}</b></td>
                        @if($check_lot != $QRCode->lot_product_id)
                            @php
                                $run_lot = 1;
                                $check_lot = $QRCode->lot_product_id;
                            @endphp
                            <td>&nbsp;Lot Q Order: <b>{{ ($run_lot .'/'. $QRCode->lot_product_qty) }}</b></td>
                        @else
                            @php $run_lot++; @endphp
                            <td>&nbsp;Lot Q Order: <b>{{ ($run_lot .'/'. $QRCode->lot_product_qty) }}</b></td>
                        @endif
                    </tr>
                    <tr>
                        <!-- set date  -->
                        <td colspan="2">&nbsp;Receive Date: <b>{{date("d/m/y", strtotime($PO->import_date))}}</b></td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;Transport by: <b>{{ $QRCode->transport_type_name_en }}</b></td>
                    </tr>
                </table>
            </div>
            @endforeach
        @else
            <div class="text-center col-12">
                ไม่พบสินค้า
            </div>
        @endif
    </body>
</htmlpagebody>
<htmlpagefooter name="page-footer">
</htmlpagefooter>
