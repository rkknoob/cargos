
<table>
    <tr>
        <td colspan="19">NEW CARGO เอกสารตู้จีน</td>
    </tr>
    <tr>
        <tr>
            <td>Receipt date</td>
            <td>Out of date</td>
            <td>No.Container<br>[Cabinet No.]</td>
            <td>Customer Code</td>
            <td>Po Number</td>
            <td>Item</td>
            <td>Descriptions Of Good</td>
            <td>Descriptions Of Good (ENG)</td>
            <td>Company</td>
            <td>(件数 PCS)</td>
            <td>Package</td>
            <td>Brand</td>
            <td>重量 (KG)</td>
            <td>重量 (Total KG)</td>
            <td colspan="3">规格 (Size)</td>
            <td>立方米 (CBM)</td>
            <td>总立方米 (Total CBM)</td>
            <td>Remark</td>
            <td>Remark</td>
        </tr>
    </tr>
    @php
        $check_lot_id = '';
        $cbm_total = 0;
    @endphp
    @foreach($LotProducts as $LotProduct)
        @php
            $user_product_type_id = '';
            if($LotProduct->user_product_type_id == 5){
                $user_product_type_id = 'NO';
            }else if($LotProduct->user_product_type_id == 6){
                $user_product_type_id = 'YES';
            }

            $width = $LotProduct->width;
            $length = $LotProduct->length;
            $height = $LotProduct->height;
            // $LotProduct->lot_product_qty
            $cbm = ($width * $length) * $height;
            $cbm_total = ($cbm * $LotProduct->qr_code_container_qty);
        @endphp
        <tr>
            <td class="text-center">{{date('Y/m/d', strtotime($LotProduct->product_created_at))}}</td>
            <td class="text-center">{{date('Y/m/d', strtotime($LotProduct->container_created_at))}}</td>
            <td class="text-center">{{$LotProduct->container_no}}</td>
            <td class="text-center">{{$LotProduct->customer_general_code}}</td>
            <td class="text-center">{{$LotProduct->po_no}}</td>
            <td class="text-center">
                @if($LotProduct->product_sort_start == $LotProduct->product_sort_end)
                    {{ $LotProduct->product_sort_start }}
                @else
                    {{number_format($LotProduct->product_sort_start) .'-'. number_format($LotProduct->product_sort_end)}}
                @endif
            </td>
            <td class="text-left">{{(!empty($LotProduct->product_name) ? $LotProduct->product_name : '' )}}</td>
            <td class="text-left">{{(!empty($LotProduct->product_name_en) ? $LotProduct->product_name_en : '' )}}</td>
            <td class="text-center">
                {{(!empty($Company->company_name_th) ? $Company->company_name_th : '' )}}
            </td>
            @if($check_lot_id != $LotProduct->product_import_to_chaina_id)
                @php $check_lot_id = $LotProduct->product_import_to_chaina_id @endphp
                <td class="text-center">{{$LotProduct->product_pcs}}</td>
            @else
                <td class="text-center"></td>
            @endif
            <td class="text-center">{{$LotProduct->qr_code_container_qty}}</td>
            <td class="text-center">{{$user_product_type_id}}</td>
            <td class="text-center">{{number_format($LotProduct->weight_per_item, 2)}}</td>
            <td class="text-center">{{number_format($LotProduct->weight_per_item * $LotProduct->qr_code_container_qty, 2)}}</td>
            <td class="text-center">{{number_format($width,2)}}</td>
            <td class="text-center">{{number_format($length,2)}}</td>
            <td class="text-center">{{number_format($height,2)}}</td>
            <td class="text-center">{{number_format($cbm,3)}}</td>
            <td class="text-center">{{number_format($cbm_total,3)}}</td>
            <td class="text-center">{{($LotProduct->transport_type_name_th == 'รถ') ? 'EK' : 'SEA'}}</td>
            <td class="text-left">{{$LotProduct->remark}}</td>
        </tr>
    @endforeach
</table>
