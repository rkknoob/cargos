﻿@extends('Admin.layouts.layout')
@section('css_bottom')
@endsection
@section('body')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="content">
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                            <h4 class="title">
                                {{$title_page or '' }}

                            </h4>
                        </div>
                        <div class="material-datatables">
                            <div class="table-responsive" style="overflow: scroll;">
                                <table id="TableList" class="table table-striped table-no-bordered table-hover" style="width:100%; cellspacing:0">
                                    <thead>
                                        <tr>
                                            <th>@lang('lang.no')</th>
                                            <th>@lang('lang.number_po')</th>
                                            <th>@lang('lang.customer_id')</th>
                                            <th>@lang('lang.import_date')</th>
                                            <th>@lang('lang.type_product')</th>
                                            <th>@lang('lang.product_name')</th>
                                            <th>@lang('lang.travel_by')</th>
                                            <th>@lang('lang.name_store_in_china')</th>
                                            <!-- <th>@lang('lang.type_container')</th> -->
                                            <th>@lang('lang.quantity')</th>
                                            <th>@lang('lang.weigh')</th>
                                            <th>@lang('lang.queue_size')</th>
                                            <!-- <th>@lang('lang.old_price')</th> -->
                                            <th>@lang('lang.current_price')</th>
                                            <th>@lang('lang.status_rate')</th>
                                            <th>@lang('lang.tool')</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('modal')
<div class="modal" id="ModalEdit" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <input type="hidden" name="edit_id" id="edit_id">
            <form id="FormEdit">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">@lang('lang.edit_data') {{$title_page or 'ข้อมูลใหม่'}}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="add_customer_id">@lang('lang.customer_id')</label> <label style="color: red;"> (@lang('lang.assign'))</label>
                                <input type="text" class="form-control" id="add_customer_id" readonly>
                                <input type="hidden" class="form-control" id="add_user_id" name="user_id" readonly>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="add_product_name">@lang('lang.product_name')</label> <label style="color: red;"> (@lang('lang.assign'))</label>
                                <input type="text" class="form-control" id="add_product_name" readonly>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" class="form-control" id="add_product_type_id" name="product_type_id" readonly>
                    <div class="row">
                        
                        <!-- <div class="col-md-6">
                            <div class="form-group">
                                <label for="add_product_code">@lang('lang.product_id')</label> <label style="color: red;"> (@lang('lang.assign'))</label>
                                <input type="text" class="form-control" id="add_product_code" readonly>
                            </div>
                        </div> -->
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="add_chinese_shop">@lang('lang.name_store_in_china')</label> <label style="color: red;"> (@lang('lang.assign'))</label>
                                <input type="text" class="form-control" id="add_chinese_shop" readonly>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="add_qty">@lang('lang.quantity')</label> <label style="color: red;"> (@lang('lang.assign'))</label>
                                <input type="text" class="form-control number-only" id="add_qty" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="add_product_type_id">@lang('lang.type_product')</label> <label style="color: red;"> (@lang('lang.assign'))</label>
                                <input type="text" class="form-control" id="add_product_type_name" readonly>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="add_transportation_type_id">@lang('lang.travel_by')</label>
                                <input type="text" class="form-control number-only" id="add_transportation_type_id" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="add_rel_user_type_product_id_old">@lang('lang.old_price')</label>
                                <input type="text" class="form-control" tabindex="-1" id="add_rel_user_type_product_old" readonly>
                                <input type="hidden" class="form-control" tabindex="-1" id="add_rel_user_type_product_id_old" name="rel_user_type_product_id_old" readonly>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="add_transportation_type_id">@lang('lang.should_be_calculated_from')</label>
                                <input type="text" class="form-control" id="add_should_type_rate" name="" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="add_price">@lang('lang.current_price')</label>
                                <input type="text" class="form-control number-only" tabindex="-1" id="add_price" name="price">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="add_type_rate">@lang('lang.calculated_from')</label>
                                <select class="select2 form-control" tabindex="-1" id="add_type_rate" name="type_rate">
                                    <option value="">เลือกหน่วยที่นำไปคำนวณ</option>
                                    <option value="KG">@lang('lang.weight_kg')</option>
                                    <option value="CBM">@lang('lang.cbm')</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">@lang('lang.save')</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('js_bottom')
<script src="{{asset('assets/global/plugins/orakuploader/orakuploader.js')}}"></script>
<script>
    var id_po = '{{$id_po}}';
    var TableList = $('#TableList').dataTable({
        "ajax": {
            "type": "POST",
            "url": url_gb+"/admin/OrderImportToChaina/EditPriceLists/"+id_po,
            "data": function ( d ) {
                //d.myKey = "myValue";
                // d.custom = $('#myInput').val();
                // etc
            }
        },
        "columns": [
            {"data": "DT_Row_Index", "className": "text-center", "searchable": false, "orderable": false  },
            {"data": "po_no", "name": "import_to_chaina.po_no"},
            {"data": "customer_general_code", "name": "users.customer_general_code"},
            {"data": "import_date", "name": "import_to_chaina.import_date"},
            {"data": "product_type_name", "name": "product_types.name", "className": "text-center"},
            {"data": "product_name", "name": "products.name", "className": "text-center"},
            {"data": "transport_type_name_{{$lang}}", "searchable": false, "orderable": false},
            {"data": "shop_chaina_name", "name": "product_import_to_chaina.shop_chaina_name", "className": "text-center"},
            // {"data": "container_type_name", "name": "container_types.name", "className": "text-center"},
            {"data": "qty", "name": "product_import_to_chaina.qty", "className": "text-center"},
            {"data": "weight_all", "product_import_to_chaina.name": "weight_all", "className": "text-center"},
            {"data": "cubic", "name": "product_import_to_chaina.cubic", "className": "text-center"},
            // {"data": "price_old", "name": "rel_user_type_products.price", "searchable": false},
            {"data": "price", "name": "rel_user_type_products.price", "searchable": false},
            {"data": "status_rate"},
            {"data": "action","className":"action text-center","searchable" : false, "orderable" : false }
        ]
    });

    $('body').on('click', '.btn-edit', function(data){
        var btn = $(this);
        btn.button('loading');
        var id = $(this).data('id');
        $('#edit_id').val(id);
        $.ajax({
            method : "get",
            url : url_gb+"/admin/OrderImportToChaina/ShowEditPrice/"+id,
            dataType : 'json'
        }).done(function(rec){
            // console.log(rec);
            $('#add_customer_id').val(rec.ProductToChaina.import_to_chaina.user.customer_general_code);
            $('#add_user_id').val(rec.ProductToChaina.import_to_chaina.user_id);
            $('#add_product_name').val(rec.ProductToChaina.product.name);
            $('#add_product_type_name').val(rec.ProductToChaina.product.product_type.name);
            $('#add_product_type_id').val(rec.ProductToChaina.product.product_type_id);
            $('#add_product_code').val(rec.ProductToChaina.product.code);
            $('#add_chinese_shop').val(rec.ProductToChaina.shop_chaina_name);
            $('#add_qty').val(rec.ProductToChaina.qty);
            $('#add_transportation_type_id').val(rec.ProductToChaina.transport_type.name);

            var html = '<option value="">เลือกราคา</option>';
            $.each(rec.price, function(k, v){
                var selected = '';
                if(rec.ProductToChaina.qr_code_product_price.rel_user_type_product_id == v.id){
                    selected = 'selected';
                }
                html += '<option '+selected+' value="'+v.id+'">'+v.price+'</option>';
            });
            console.log(rec.ProductToChaina.qr_code_product_price);
            $('#add_price').val(
                (rec.ProductToChaina.qr_code_product_price.rel_user_type_product ? rec.ProductToChaina.qr_code_product_price.rel_user_type_product.price :
                    ''
                )
            );
            $('#add_type_rate').val(
                (
                    rec.ProductToChaina.qr_code_product_price.type_rate ?
                        rec.ProductToChaina.qr_code_product_price.type_rate
                        :
                        ''
                ));
            $('#add_type_rate').select2();
            $('#add_rel_user_type_product_id_old').val(
                (rec.ProductToChaina.qr_code_product_price.rel_user_type_product_old || rec.ProductToChaina.qr_code_product_price.rel_user_type_product ? // ถ้า ราคาเก่ามี
                    (rec.ProductToChaina.qr_code_product_price.rel_user_type_product_old ? rec.ProductToChaina.qr_code_product_price.rel_user_type_product_old.id : '') // แสดง ราคาเก่า
                    : // อื่นๆ
                    (rec.ProductToChainaOldPrice ? // ถ้าราคาล่าสุดมี
                        rec.ProductToChainaOldPrice.rel_user_type_product_id // แสดงราคาล่าสุด
                        : '' // อื่นๆ ไม่มีอะไรแสดง
                    )
                )
            );
            $('#add_rel_user_type_product_old').val(
                (rec.ProductToChaina.qr_code_product_price.rel_user_type_product_old || rec.ProductToChaina.qr_code_product_price.rel_user_type_product ? // ถ้า ราคาเก่ามี
                    (rec.ProductToChaina.qr_code_product_price.rel_user_type_product_old ? rec.ProductToChaina.qr_code_product_price.rel_user_type_product_old.price : '' ) // แสดง ราคาเก่า
                    : // อื่นๆ
                    (rec.ProductToChainaOldPrice ? // ถ้าราคาล่าสุดมี
                        rec.ProductToChainaOldPrice.price // แสดงราคาล่าสุด
                        : '' // อื่นๆ ไม่มีอะไรแสดง
                    )
                )
            );
            $('#add_should_type_rate').val(
                (rec.ProductToChaina.qr_code_product_price.rel_user_type_product_old || rec.ProductToChaina.qr_code_product_price.rel_user_type_product ? // ถ้า ราคาเก่ามี
                    (rec.ProductToChaina.qr_code_product_price.rel_user_type_product_old ? rec.ProductToChaina.qr_code_product_price.rel_user_type_product_old.type_rate : '') // แสดง ราคาเก่า
                    : // อื่นๆ
                    (rec.ProductToChainaOldPrice ?
                        rec.ProductToChainaOldPrice.type_rate
                        : ''
                    )
                )
            );
            // $('#add_rel_user_type_product_id').html(html);
            // $('#add_rel_user_type_product_id').select2();
            btn.button("reset");
            ShowModal('ModalEdit');
        }).fail(function(){
            swal("system.system_alert","@lang('lang.system_system_error')","error");
            btn.button("reset");
        });
    });

    $('#FormEdit').validate({
        errorElement: 'div',
        errorClass: 'invalid-feedback',
        focusInvalid: false,
        rules: {
            price: {
                required: true,
            },
            type_rate: {
                required: true,
            },
        },
        messages: {
            price: {
                required: "@lang('lang.please_specify')",
            },
            type_rate: {
                required: "@lang('lang.please_specify')",
            },
        },
        highlight: function (e) {
            validate_highlight(e);
        },
        success: function (e) {
            validate_success(e);
        },
        errorPlacement: function (error, element) {
            validate_errorplacement(error, element);
        },
        submitHandler: function (form) {
            /*
            if(CKEDITOR!==undefined){
                for ( instance in CKEDITOR.instances ){
                    CKEDITOR.instances[instance].updateElement();
                }
            }
            */
            var btn = $(form).find('[type="submit"]');
            var id = $('#edit_id').val();
            btn.button("loading");
            $.ajax({
                method : "POST",
                url : url_gb+"/admin/OrderImportToChaina/EditPrice/"+id,
                dataType : 'json',
                data : $(form).serialize()
            }).done(function(rec){
                btn.button("reset");
                if(rec.status==1){
                    TableList.api().ajax.reload();
                    resetFormCustom(form);
                    swal(rec.title,rec.content,"success");
                    $('#ModalEdit').modal('hide');
                }else{
                    swal(rec.title,rec.content,"error");
                }
            }).fail(function(){
                swal("system.system_alert","@lang('lang.system_system_error')","error");
                btn.button("reset");
            });
        },
        invalidHandler: function (form) {

        }
    });

    $('#add_rel_user_type_product_id').select2();
    $('#add_type_rate').select2();
</script>
@endsection
