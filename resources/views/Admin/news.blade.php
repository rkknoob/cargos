﻿@extends('Admin.layouts.layout')
@section('css_bottom')
@endsection
@section('body')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="content">
                        <h4 class="title">
                            {{$title_page or '' }}
                            <button class="btn btn-success btn-add pull-right" >
                                + @lang('lang.create_data')
                            </button>
                        </h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <div class="material-datatables">
                            <div class="table-responsive">
                                <table id="TableList" class="table table-striped table-no-bordered table-hover" style="width:100%;cellspacing:0">
                                    <thead>
                                        <tr>
                                        <th>@lang('lang.name')</th>
                                        <th>@lang('lang.news_date')</th>
                                        <th>@lang('lang.active')</th>
                                        <th>@lang('lang.tool')</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('modal')
<div class="modal" id="ModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form id="FormAdd">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">@lang('lang.create_data') {{$title_page or 'ข้อมูลใหม่'}}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">

                <label for="add_news_date">@lang('lang.news_date')</label>
                <div class="input-group" data-date="{{date('Y-m-d')}}">
                    <input type="text" value="" readonly="readonly" class="form-control form_date" name="news_date" id="add_news_date" required="" placeholder="news_date">
                    <span class="input-group-addon remove_date_time"><i class="glyphicon glyphicon-remove icon-remove"></i></span>
                    <span class="input-group-addon trigger_date_time" for="add_news_date"><i class="glyphicon glyphicon-calendar icon-calendar"></i></span>
                </div>
                    
                <div class="form-group">
                    <label for="add_name">@lang('lang.name')</label>
                    <input type="text" class="form-control" name="name" id="add_name" required="" placeholder="@lang('lang.name')">
                </div>
        
                <div class="form-group">
                    <label for="add_title">@lang('lang.title')</label>
                    <input type="text" class="form-control" name="title" id="add_title" required="" placeholder="@lang('lang.title')">
                </div>
        
                <div class="form-group">
                    <label for="add_detail">@lang('lang.detail')</label>
                    <textarea id="add_detail" name="detail" class="form-control"></textarea>
                </div>
                
                <div class="form-group">
                    <label for="add_image">@lang('lang.image')</label>
                    <div id="orak_add_image">
                        <div id="add_image" orakuploader="on"></div>
                    </div>
                </div>

                <div class="form-check">
                    <label for="add_active" class="checkbox form-check-label">
                        <input type="checkbox" class="form-check-input" data-toggle="checkbox" name="active" id="add_active" required="" value="T" checked="checked"> @lang('lang.active')
                    </label>
                </div>
        
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">@lang('lang.save')</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" id="ModalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <input type="hidden" name="edit_id" id="edit_id">
            <form id="FormEdit">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">@lang('lang.edit_data') {{$title_page or 'ข้อมูลใหม่'}}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">

                <label for="edit_news_date">@lang('lang.news_date')</label>
                <div class="input-group" data-date="{{date('Y-m-d')}}">
                    <input type="text" value="" readonly="readonly" class="form-control form_date" name="news_date" id="edit_news_date" required="" placeholder="news_date">
                    <span class="input-group-addon remove_date_time"><i class="glyphicon glyphicon-remove icon-remove"></i></span>
                    <span class="input-group-addon trigger_date_time" for="edit_news_date"><i class="glyphicon glyphicon-calendar icon-calendar"></i></span>
                </div>
                    
                <div class="form-group">
                    <label for="edit_name">@lang('lang.name')</label>
                    <input type="text" class="form-control" name="name" id="edit_name" required="" placeholder="name">
                </div>
        
                <div class="form-group">
                    <label for="edit_title">@lang('lang.name')</label>
                    <input type="text" class="form-control" name="title" id="edit_title" required="" placeholder="title">
                </div>
        
                <div class="form-group">
                    <label for="edit_detail">@lang('lang.detail')</label>
                    <textarea id="edit_detail" name="detail" class="form-control"></textarea>
                </div>
        
                <input type="hidden" name="org_image" id="org_image">
                <div class="form-group">
                    <label for="edit_image">@lang('lang.image')</label>
                    <div id="orak_edit_image">
                        <div id="edit_image" orakuploader="on"></div>
                    </div>
                </div>

                <div class="form-check">
                    <label for="edit_active" class="checkbox form-check-label">
                        <input type="checkbox" class="form-check-input" data-toggle="checkbox" name="active" id="edit_active" required="" value="T"> @lang('lang.active')
                    </label>
                </div>
        
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">@lang('lang.save')</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('js_bottom')
<script src="{{asset('assets/global/plugins/orakuploader/orakuploader.js')}}"></script>
<script>
    $(".form_date").daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        minYear: 1901,
        startDate: '{{date("Y-m-d")}}',
        // maxYear: parseInt(moment().format('YYYY'),10),
        locale: {
            format: 'YYYY-MM-DD'
        }
    }, function(start, end, label) {
            var years = moment().diff(start, 'years');
            // alert("You are " + years + " years old!");
    });

     var TableList = $('#TableList').dataTable({
        "ajax": {
            "url": url_gb+"/admin/{{$lang}}/News/Lists",
            "data": function ( d ) {
                //d.myKey = "myValue";
                // d.custom = $('#myInput').val();
                // etc
            }
        },
        "columns": [
            {"data" : "name"},
            {"data" : "news_date"},
            {"data" : "active"},
            { "data": "action","className":"action text-center","searchable" : false , "orderable" : false }
        ]
    });
    $('body').on('click','.btn-add',function(data){
        ShowModal('ModalAdd');
    });
    $('body').on('click','.btn-edit',function(data){
        var btn = $(this);
        btn.button('loading');
        var id = $(this).data('id');
        $('#edit_id').val(id);
        $.ajax({
            method : "GET",
            url : url_gb+"/admin/News/"+id,
            dataType : 'json'
        }).done(function(rec){
            $('#edit_name').val(rec.name);
            $('#edit_title').val(rec.title);
            CKEDITOR.instances['edit_detail'].setData(rec.detail);
            $('#edit_news_date').val(rec.news_date);
            if(rec.active=='T'){
                $('#edit_active').prop('checked','checked').closest('label').addClass('checked');
            }else{
                $('#edit_active').removeAttr('checked').closest('label').removeClass('checked');
            }
            
            $('#edit_image').closest('#orak_edit_image').html('<div id="edit_image" orakuploader="on"></div>');
            $('#org_image').val(rec.image);
            if(rec.image){
                var max_file = 0;
                var file = [];
                    file[0] = rec.image;
                var image = rec.image;
            }else{
                var max_file = 1;
                var file = [];
                var image = rec.image;
            }
            $('#edit_image').orakuploader({
                orakuploader_path               : url_gb+'/',
                orakuploader_ckeditor           : false,
                orakuploader_use_dragndrop      : true,
                orakuploader_main_path          : 'uploads/temp/',
                orakuploader_thumbnail_path     : 'uploads/temp/',
                orakuploader_thumbnail_real_path: asset_gb+'uploads/temp/',
                orakuploader_add_image          : asset_gb+'images/add.png',
                orakuploader_loader_image       : asset_gb+'images/loader.gif',
                orakuploader_no_image           : asset_gb+'images/no-image.jpg',
                orakuploader_add_label          : 'เลือกรูปภาพ',
                orakuploader_use_rotation       : false,
                orakuploader_maximum_uploads    : max_file,
                orakuploader_hide_on_exceed     : true,
                orakuploader_attach_images      : file,
                orakuploader_field_name         : 'image',
                orakuploader_finished           : function(){

                }
            });
            $('#org_image').val(image)

            btn.button("reset");
            ShowModal('ModalEdit');
        }).fail(function(){
            swal("system.system_alert","system.system_error","error");
            btn.button("reset");
        });
    });

    $('#FormAdd').validate({
        errorElement: 'div',
        errorClass: 'invalid-feedback',
        focusInvalid: false,
        rules: {
            
            name: {
                required: true,
            },
            title: {
                required: true,
            },
            news_date: {
                required: true,
            },
            active: {
                required: true,
            },
            'image[]': {
                required: true,
            },
        },
        messages: {
            
            name: {
                required: "@lang('lang.please_specify')",
            },
            title: {
                required: "@lang('lang.please_specify')",
            },
            news_date: {
                required: "@lang('lang.please_specify')",
            },
            active: {
                required: "@lang('lang.please_specify')",
            },
            'image[]': {
                required: "@lang('lang.please_specify')",
            },
        },
        highlight: function (e) {
            validate_highlight(e);
        },
        success: function (e) {
            validate_success(e);
        },

        errorPlacement: function (error, element) {
            validate_errorplacement(error, element);
        },
        submitHandler: function (form) {
            
            if(CKEDITOR!==undefined){
                for ( instance in CKEDITOR.instances ){
                    CKEDITOR.instances[instance].updateElement();
                }
            }
            
            var btn = $(form).find('[type="submit"]');
            var data_ar = removePriceFormat(form,$(form).serializeArray());
            btn.button("loading");
            $.ajax({
                method : "POST",
                url : url_gb+"/admin/News",
                dataType : 'json',
                data : $(form).serialize()
            }).done(function(rec){
                btn.button("reset");
                if(rec.status==1){
                    TableList.api().ajax.reload();
                    resetFormCustom(form);
                    swal(rec.title,rec.content,"success");
                    $('#ModalAdd').modal('hide');
                }else{
                    swal(rec.title,rec.content,"error");
                }
            }).fail(function(){
                swal("system.system_alert","system.system_error","error");
                btn.button("reset");
            });
        },
        invalidHandler: function (form) {

        }
    });

    $('#FormEdit').validate({
        errorElement: 'div',
        errorClass: 'invalid-feedback',
        focusInvalid: false,
        rules: {
            
            name: {
                required: true,
            },
            title: {
                required: true,
            },
            news_date: {
                required: true,
            },
            active: {
                required: true,
            },
            'image[]': {
                required: true,
            },
        },
        messages: {
            
            name: {
                required: "@lang('lang.please_specify')",
            },
            title: {
                required: "@lang('lang.please_specify')",
            },
            news_date: {
                required: "@lang('lang.please_specify')",
            },
            active: {
                required: "@lang('lang.please_specify')",
            },
            'image[]': {
                required: "@lang('lang.please_specify')",
            },
        },
        highlight: function (e) {
            validate_highlight(e);
        },
        success: function (e) {
            validate_success(e);
        },

        errorPlacement: function (error, element) {
            validate_errorplacement(error, element);
        },
        submitHandler: function (form) {
            
            if(CKEDITOR!==undefined){
                for ( instance in CKEDITOR.instances ){
                    CKEDITOR.instances[instance].updateElement();
                }
            }
            
            var btn = $(form).find('[type="submit"]');
            var id = $('#edit_id').val();
            btn.button("loading");
            $.ajax({
                method : "POST",
                url : url_gb+"/admin/News/"+id,
                dataType : 'json',
                data : $(form).serialize()
            }).done(function(rec){
                btn.button("reset");
                if(rec.status==1){
                    TableList.api().ajax.reload();
                    resetFormCustom(form);
                    swal(rec.title,rec.content,"success");
                    $('#ModalEdit').modal('hide');
                }else{
                    swal(rec.title,rec.content,"error");
                }
            }).fail(function(){
                swal("system.system_alert","system.system_error","error");
                btn.button("reset");
            });
        },
        invalidHandler: function (form) {

        }
    });

    $('body').on('click','.btn-delete',function(e){
        e.preventDefault();
        var btn = $(this);
        var id = btn.data('id');
        swal({
            title: "@lang('lang.do_you_want_to_delete')",
            text: "@lang('lang.if_you_delete_you_will_not_be_able_to_restore_it')้",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: "@lang('lang.yes_i_want_to_delete')",
            cancelButtonText: "@lang('lang.cancel')",
            showLoaderOnConfirm: true,
            buttonsStyling: false
        }).then(function() {
            $.ajax({
                method : "POST",
                url : url_gb+"/admin/News/Delete/"+id,
                data : {ID : id}
            }).done(function(rec){
                if(rec.status==1){
                    swal(rec.title,rec.content,"success");
                    TableList.api().ajax.reload();
                }else{
                    swal("ระบบมีปัญหา","กรุณาติดต่อผู้ดูแล","error");
                }
            }).fail(function(data){
                swal("ระบบมีปัญหา","กรุณาติดต่อผู้ดูแล","error");
            });
        }).catch(function(e){
            //console.log(e);
        });
    });

    CKEDITOR.replace('add_detail');
    CKEDITOR.replace('edit_detail');

        $('#add_image').orakuploader({
            orakuploader_path               : url_gb+'/',
            orakuploader_ckeditor           : false,
            orakuploader_use_dragndrop      : true,
            orakuploader_main_path          : 'uploads/temp/',
            orakuploader_thumbnail_path     : 'uploads/temp/',
            orakuploader_thumbnail_real_path: asset_gb+'uploads/temp/',
            orakuploader_add_image          : asset_gb+'images/add.png',
            orakuploader_loader_image       : asset_gb+'images/loader.gif',
            orakuploader_no_image           : asset_gb+'images/no-image.jpg',
            orakuploader_add_label          : 'เลือกรูปภาพ',
            orakuploader_use_rotation       : false,
            orakuploader_maximum_uploads    : 1,
            orakuploader_hide_on_exceed     : true,
            orakuploader_field_name         : 'image',
            orakuploader_finished           : function(){

            }
        });
        
</script>
@endsection