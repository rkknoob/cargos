<!--
 <style>
    @page{
        header: page-header;
        footer: page-footer;
        margin-top: 50px;
        margin-bottom: 150px;
    }
    .page-break {
        page-break-after: always;
    }
    body{
        font-family: Arial;
    }
    td {
        /height: 25px;
    }
    th {
        font-weight: normal;
    }
</style>
-->

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>ใบ Sale / รายการสินค้า</title>
        <style>
            @font-face {
                font-family: 'SentyTang';
                font-style: normal;
                font-weight: normal;
                src: url("{{ asset('fonts/SentyTang.ttf') }}") format('truetype');
            }

            body {
                font-family: 'SentyTang';
            }
            .table_css {
                height: 25px;
                font-size: 14px;
                width: 800px;
            }
            .table_css td {
                height: 25px;
                font-size: 14px;
            }
            .table_css th {
                font-weight: normal;
                font-size: 14px;
            }
        </style>
    </head>
    <body lang="th">
        <htmlpageheader name="page-header">

        </htmlpageheader>

        <htmlpagebody>
            <div class="row">
                <div style="color: black; text-align: left; font-size: 13px; ">

                </div>
            </div>
            <div class="row" style="padding-top: 20px;">
                <div style="color: black; text-align: center; font-size: 16px; ">
                    <b>ใบ Sale</b>
                </div>
            </div>
            <div class="row" style="padding-top: 20px;">
                <table class="table_css">
                    <tr>
                        <td colspan="3" style="text-align: right; width: 700px;"><b>รหัสตู้: {{ $Container ? $Container->container_code : null }}</b></td>
                    </tr>
                    <tr>
                        <td colspan="3" style="text-align: right;"><b>วันที่ปิดตู้: {{ $Container ? $Container->created_at : null }}</b></td>
                    </tr>
                </table>
            </div>

            <div class="row" style="padding-top: 20px;">
                <table class="table_css" border="1" cellspacing="0">
                    <tr>
                        <th>รหัสลูกค้า</th>
                        <th>จำนวน</th>
                        <th>KG รวม</th>
                        <th>CBM รวม</th>
                        <th>จำนวน  PO</th>
                        <th>จำนวนรวม</th>
                    </tr>
                    @if(! $ImportToChainas->isEmpty())
                        @php

                        @endphp
                        @foreach($ImportToChainas as $key => $ImportToChaina)
                            @php
                                $cubil = 0;
                                if(!empty($ImportToChaina->product_amount_real) && !empty($ImportToChaina->product_cubic)){
                                    $cubil = $ImportToChaina->product_cubic * $ImportToChaina->product_amount_real;
                                }
                            @endphp
                            <tr>
                                <td>{{  $ImportToChaina->customer_general_code }}</td>
                                <td style="text-align: right;">{{ number_format($ImportToChaina->product_amount) }}</td>
                                <td style="text-align: right;">{{ number_format($ImportToChaina->product_KG) }}</td>
                                <td style="text-align: right;">{{ number_format($cubil, 3) }}</td>
                                <td style="text-align: right;">{{ number_format($ImportToChaina->po_amount) }}</td>
                                <td style="text-align: right;">{{ number_format($ImportToChaina->product_amount_real) }}/{{ number_format($ImportToChaina->product_amount) }}</td>
                            </tr>
                        @endforeach
                    @endif
                </table>
                <br>
                <table class="table_css" border="1" cellspacing="0">
                    <tr>
                        <th>ลำดับ</th>
                        <th>รหัสลูกค้า</th>
                        <th>เลขที่ PO</th>
                        <th>รายการ</th>
                        <th>จำนวน</th>
                        <th>KG</th>
                        <th>CBM</th>
                        <th>เรทราคา</th>
                        <th>รวม</th>
                        <th>kg/cbm</th>
                        <th>ควรคิดเป็น</th>
                        <th>ปัจจุบันคิดเป็น</th>
                        <th>ตรงกันหรือไม่</th>
                    </tr>
                    @if(! $QrCodeProducts->isEmpty())
                        @php
                            $total_qty_container = 0;
                            $total_weight_all = 0;
                            $total_cubic = 0;
                            $amount = 0;
                            $total = 0;
                        @endphp
                        @foreach($QrCodeProducts as $key => $QrCodeProduct)
                            @php
                                $cubil = 0;
                                $subtotal = 0;
                                $should_rate = '-';
                                $kg_cbm = 0;

                                /* วิธีคำนวนน้ำหนักต่อคิว = กิโล/คิว
                                เช่นหนัก 400 กิโล 2 คิว
                                ตกคิวละ 400/2 = 200 กิโล
                                ถ้าหนัก 250 กิโลต่อคิวขึ้นไป = ควรคิดเป็นกิโล
                                ถ้าหนัก ต่ำกว่า 250 กิโล = ควรคิดเป็นคิว
                                ถ้าไม่ใส่คิวมา ขึ้นเป็น n/a */

                                $amount += $QrCodeProduct->product_amount;
                                $total_weight_all += $QrCodeProduct->product_weight_per_item;
                                if(!empty($QrCodeProduct->cubic_item) && !empty($QrCodeProduct->product_amount)){
                                    $cubil = $QrCodeProduct->cubic_item * $QrCodeProduct->product_amount ;
                                }
                                /// $QrCodeProduct->product_amount
                                $weight_product = $QrCodeProduct->product_weight_per_item;

                                // set สิ่งที่ควณนำมาคำนวณ
                                $check_price = 250;
                                //$check_price = ($QrCodeProduct->rate_price * $weight_product) / $cubil;

                                if($cubil > 0 && $weight_product > 0){
                                    if(($cubil/$weight_product) >= $check_price ){
                                        $should_rate = 'KG';
                                    }else if( ($cubil/$weight_product) < $check_price ){
                                        $should_rate = 'CBM';
                                    }
                                }

                                // set เงินที่คำนวน
                                if( $QrCodeProduct->type_rate == 'KG' ){
                                    $subtotal = $QrCodeProduct->rate_price * $weight_product;
                                }else if( $QrCodeProduct->type_rate == 'CBM' ){
                                    if( $QrCodeProduct->cubic_item > 0 ){
                                        $subtotal = $QrCodeProduct->rate_price * $cubil;
                                    }else{
                                        $subtotal = 0;
                                    }
                                }
                                $subtotal = $subtotal;

                                $total_cubic += $cubil;
                                $total += $subtotal;
                                $rate_type_price = '';
                                if($QrCodeProduct->rel_user_type_product_id_old != null){
                                    $rate_type_price = \App\Models\RelUserTypeProduct::where('id', $QrCodeProduct->rel_user_type_product_id_old)->first();
                                }

                                $q = 0;
                                if($QrCodeProduct->product_weight_per_item > 0 && $cubil > 0){
                                    $q = number_format($cubil,2,".","");
                                    if($QrCodeProduct->product_weight_per_item > 0 && $q > 0){
                                        $kg_cbm = ($QrCodeProduct->product_weight_per_item / $q);
                                    }
                                }
                            @endphp
                            <tr>
                                <td>&nbsp;{{ $key+1 }}&nbsp;
                                </td>
                                <td>&nbsp;{{ $QrCodeProduct->customer_general_code }}&nbsp;</td>
                                <td>&nbsp;{{ $QrCodeProduct->po_no }}&nbsp;</td>
                                <td>&nbsp;{{ $QrCodeProduct->product_name_en != null ? $QrCodeProduct->product_name_en : $QrCodeProduct->product_name_th }}&nbsp;</td>
                                <td style="text-align: right;">&nbsp;{{ number_format($QrCodeProduct->product_amount) }}&nbsp;</td>
                                <td style="text-align: right;">&nbsp;{{ number_format($QrCodeProduct->product_weight_per_item) }}&nbsp;</td>
                                <td style="text-align: right;">&nbsp;{{ ($cubil > 0 ? number_format($cubil,2) : 'N/A' ) }}&nbsp;</td>
                                <td style="text-align: right;">&nbsp;{{ number_format($QrCodeProduct->rate_price, 2) }}&nbsp;</td>
                                <td style="text-align: center;">&nbsp;{{ number_format($subtotal, 2) }}&nbsp;</td>
                                <td style="text-align: right;">&nbsp;{{ number_format($kg_cbm,2) }}&nbsp;</td>
                                <td style="text-align: center;">&nbsp;{{ $should_rate }}&nbsp;</td>
                                <td style="text-align: center;">&nbsp;{{ $QrCodeProduct->type_rate }}&nbsp;</td>
                                <td style="text-align: center;">&nbsp;
                                    {{ $should_rate == $QrCodeProduct->type_rate ? 'ตรงกัน' : 'ไม่ตรงกัน' }}
                                &nbsp;</td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="4" style="text-align: center;"><b>Total</b>&nbsp;</td>
                            <td style="text-align: right;">&nbsp;{{ number_format($amount) }}&nbsp;</td>
                            <td style="text-align: right;">&nbsp;{{ number_format($total_weight_all) }}&nbsp;</td>
                            <td style="text-align: right;">&nbsp;{{ number_format($total_cubic,2) }}&nbsp;</td>
                            <td style="text-align: right;"></td>
                            <td style="text-align: right;">&nbsp;{{ number_format($total,2) }}&nbsp;</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    @endif
                </table>
            </div>

        </htmlpagebody>

        <htmlpagefooter name="page-footer">
            <div style="padding-bottom: 40px;text-align: center;">

            </div>
        </htmlpagefooter>
    </body>
