<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('assets/admin/img/apple-icon.png')}}">
    <link rel="icon" type="image/png" href="{{asset('assets/admin/img/favicon.png')}}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>{{ isset($title_page)? $title_page.' |':'' }} WorkByThai</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no' name='viewport'>
    <meta name="viewport" content="width=device-width">

    <!-- Bootstrap core CSS     -->
    <link href="{{asset('assets/admin/css/bootstrap.min.css')}}" rel="stylesheet">
    @yield('css_top')
    <!--  Material Dashboard CSS    -->
    <link href="{{asset('assets/admin/css/inspire.css')}}" rel="stylesheet">

    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="{{asset('assets/admin/css/demo.css')}}" rel="stylesheet">

    <!--     Fonts and icons     -->
    <link href="{{asset('assets/admin/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/admin/css/font-muli.css')}}" rel='stylesheet' type='text/css'>
    <link href="{{asset('assets/admin/css/themify-icons.css')}}" rel="stylesheet">
    <link href="{{asset('assets/admin/vendors/sweetalert/css/sweetalert2.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/global/css/modal.css')}}" />
    <link href="{{asset('assets/admin/vendors/jquery-ui-1.12.0/jquery-ui.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/global/plugins/orakuploader/orakuploader.css')}}" />
    <link rel="stylesheet" href="{{asset('assets/global/plugins/bootstrap-datetimepicker-master/css/bootstrap-datetimepicker.css')}}" />
    <link rel="stylesheet" href="{{asset('assets/global/plugins/bootstrap-daterangepicker-master/daterangepicker.css')}}" />
    <link rel="stylesheet" href="{{asset('assets/global/plugins/select2/css/select2.css')}}" />
    <link href="{{asset('assets/admin/css/custom.css')}}" rel="stylesheet">
    <style>
        .modal-maximize {
            max-width: 100%;
        }
    </style>
    @yield('css_bottom')
</head>

<body>
    <div class="wrapper">
        @include('Admin.layouts.sidebar')
        <div class="main-panel">
            <nav class="navbar navbar-default navbar-toggleable-md navbar-inverse">
                <div class="navbar-minimize">
                    <button id="minimizeSidebar" class="btn btn-round btn-white btn-fill btn-just-icon">
                                           <i class="ti-menu"></i>
                        </button>
                </div>
                <div class="navbar-header">
                    <button type="button" class="navbar-toggler navbar-toggler-right" data-toggle="collapse" data-target="#navbarItems" aria-controls="navbarsItems" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <a class="navbar-brand" href="#"> {{$title or ''}} </a>
                </div>
                <div class="collapse navbar-collapse" id="navbarItems">
                    <ul class="nav navbar-nav ml-auto">
                        <li>
                            <a href="{{ url('admin/th') }}" class="dropdown-toggle nav-link">
                                <img src="{{asset('images/th.jpg')}}" height="23" title="Change CH" style="margin-top: -6px;" alt="">
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('admin/en') }}" class="dropdown-toggle nav-link">
                                <img src="{{asset('images/en.jpg')}}" height="20" title="Change EN" style="margin-top: -6px;" alt="">
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('admin/ch') }}" class="dropdown-toggle nav-link">
                                <img src="{{asset('images/ch.jpg')}}" height="23" title="Change CH" style="margin-top: -6px;" alt="">
                            </a>
                        </li>
                        <!-- <li class="dropdown">
                            <a href="#" class="dropdown-toggle nav-link" id="notificationList" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="ti-bell nav-icon"></i>
                                <span class="notification">6</span>
                                <p class="hidden-lg-up">
                                    Notifications
                                    <i class="fa fa-sort-desc submenu-toggle"></i>
                                </p>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="notificationList">
                                <a class="dropdown-item" href="#">You have 5 new messages</a>
                                <a class="dropdown-item" href="#">You're now friend with Mike</a>
                                <a class="dropdown-item" href="#">Wish Mary on her birthday!</a>
                                <a class="dropdown-item" href="#">5 warnings in Server Console</a>
                                <a class="dropdown-item" href="#">Jane completed 'Induction Training'</a>
                                <a class="dropdown-item" href="#">'Prepare Marketing Report' is overdue</a>
                            </div>
                        </li>
                        <li>
                            <a href="#pablo" class="dropdown-toggle nav-link" data-toggle="dropdown">
                                <i class="ti-layout-grid3-alt nav-icon"></i>
                                <p class="hidden-lg-up">Apps</p>
                            </a>
                        </li>
                        <li>
                            <a href="#pablo" class="dropdown-toggle nav-link" data-toggle="dropdown">
                                <i class="ti-user nav-icon"></i>
                                <p class="hidden-lg-up">Profile</p>
                            </a>
                        </li>
                        <li>
                            <a href="#pablo" class="dropdown-toggle nav-link" data-toggle="dropdown">
                                <i class="ti-settings nav-icon"></i>
                                <p class="hidden-lg-up">Settings</p>
                            </a>
                        </li>
                        <li class="separator hidden-lg-up"></li> -->
                    </ul>
                </div>
            </nav>
            @yield('body')
            <footer class="footer">
                <div class="ml-4">
                    <p class="copyright float-left">
                        &copy;
                        <script>
                            document.write(new Date().getFullYear())
                        </script>
                        <a href="https://workbythai.com/" target="_blank">Workbythai Internet and marketing Co., Ltd.</a> โทร: 064 351 7519  V.2.0b
                    </p>
                </div>
            </footer>
        </div>

    </div>
    @yield('modal')
    <script>
        var url_gb = '{{url('')}}';
        var asset_gb = '{{asset('')}}';
    </script>
    <!--   Core JS Files   -->
    <script src="{{asset('assets/admin/vendors/jquery-3.1.1.min.js')}}" type="text/javascript"></script>
    @yield('js_top')
    <script src="{{asset('assets/admin/vendors/jquery-ui.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/admin/vendors/tether.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/admin/vendors/bootstrap.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/admin/vendors/material.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/admin/vendors/perfect-scrollbar.jquery.min.js')}}" type="text/javascript"></script>
    <!-- Forms Validations Plugin -->
    <script src="{{asset('assets/admin/vendors/jquery.validate.min.js')}}"></script>
    <!--  Plugin for Date Time Picker and Full Calendar Plugin-->
    <script src="{{asset('assets/admin/vendors/moment.min.js')}}"></script>
    <!--  Charts Plugin -->
    <script src="{{asset('assets/admin/vendors/charts/flot/jquery.flot.js')}}"></script>
    <script src="{{asset('assets/admin/vendors/charts/flot/jquery.flot.resize.js')}}"></script>
    <script src="{{asset('assets/admin/vendors/charts/flot/jquery.flot.pie.js')}}"></script>
    <script src="{{asset('assets/admin/vendors/charts/flot/jquery.flot.stack.js')}}"></script>
    <script src="{{asset('assets/admin/vendors/charts/flot/jquery.flot.categories.js')}}"></script>
    <script src="{{asset('assets/admin/vendors/charts/chartjs/Chart.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/admin/vendors/jquery.sparkline.min.js')}}"></script>

    <!--  Plugin for the Wizard -->
    <script src="{{asset('assets/admin/vendors/jquery.bootstrap-wizard.js')}}"></script>
    <!--  Notifications Plugin    -->
    <script src="{{asset('assets/admin/vendors/bootstrap-notify.js')}}"></script>
    <!-- DateTimePicker Plugin -->
    <script src="{{asset('assets/admin/vendors/bootstrap-datepicker.min.js')}}"></script>
    <!-- Sliders Plugin -->
    <script src="{{asset('assets/admin/vendors/nouislider.min.js')}}"></script>
    <!--  Google Maps Plugin    -->
    <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAurmSUEQDwY86-wOG3kCp855tCI8lHL-I"></script> -->
    <!-- Select Plugin -->
    <script src="{{asset('assets/admin/vendors/jquery.select-bootstrap.js')}}"></script>

    <!--  Checkbox, Radio, Switch and Tags Input Plugins -->
    <script src="{{asset('assets/admin/js/bootstrap-checkbox-radio-switch-tags.js')}}"></script>

    <!-- Circle Percentage-chart -->
    <script src="{{asset('assets/admin/js/jquery.easypiechart.min.js')}}"></script>
    <!-- Sweet Alert 2 plugin -->
    <script src="{{asset('assets/admin/vendors/sweetalert/js/sweetalert2.min.js')}}"></script>
    <!--    Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
    <script src="{{asset('assets/admin/vendors/jasny-bootstrap.min.js')}}"></script>
    <!--  Full Calendar Plugin    -->
    <script src="{{asset('assets/admin/vendors/fullcalendar.min.js')}}"></script>
    <!-- TagsInput Plugin -->
    <script src="{{asset('assets/admin/vendors/jquery.tagsinput.js')}}"></script>
    <!-- Material Dashboard javascript methods -->
    <script src="{{asset('assets/admin/js/inspire.js')}}"></script>
    <!-- Material Dashboard DEMO methods, don't include it in your project! -->
    <script src="{{asset('assets/admin/js/demo.js')}}"></script>

    <script src="{{asset('assets/admin/js/charts/flot-charts.js')}}"></script>
    <script src="{{asset('assets/admin/js/charts/chartjs-charts.js')}}"></script>
    <script src="{{asset('assets/admin/js/charts/sparkline-charts.js')}}"></script>
    <script src="{{asset('assets/admin/vendors/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/admin/vendors/dataTables.bootstrap4.js')}}"></script>
    <script src="{{asset('assets/admin/vendors/sweetalert/js/sweetalert2.min.js')}}"></script>
    <script src="{{asset('assets/global/js/modal.js')}}"></script>
    <script src="{{asset('assets/global/js/validate.js')}}"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-datetimepicker-master/js/bootstrap-datetimepicker.js')}}"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-daterangepicker-master/daterangepicker.js')}}"></script>
    <script src="{{asset('assets/global/plugins/Jquery-Price-Format/jquery.priceformat.js')}}"></script>
    <script src="{{asset('assets/global/plugins/ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('assets/global/plugins/ckeditor/config.js')}}"></script>
    <script src="{{asset('assets/global/plugins/select2/js/select2.js')}}"></script>
    <script src="{{asset('assets/admin/js/function.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function() {

        });

        function beep() {
            var snd = new Audio("data:audio/wav;base64,//uQRAAAAWMSLwUIYAAsYkXgoQwAEaYLWfkWgAI0wWs/ItAAAGDgYtAgAyN+QWaAAihwMWm4G8QQRDiMcCBcH3Cc+CDv/7xA4Tvh9Rz/y8QADBwMWgQAZG/ILNAARQ4GLTcDeIIIhxGOBAuD7hOfBB3/94gcJ3w+o5/5eIAIAAAVwWgQAVQ2ORaIQwEMAJiDg95G4nQL7mQVWI6GwRcfsZAcsKkJvxgxEjzFUgfHoSQ9Qq7KNwqHwuB13MA4a1q/DmBrHgPcmjiGoh//EwC5nGPEmS4RcfkVKOhJf+WOgoxJclFz3kgn//dBA+ya1GhurNn8zb//9NNutNuhz31f////9vt///z+IdAEAAAK4LQIAKobHItEIYCGAExBwe8jcToF9zIKrEdDYIuP2MgOWFSE34wYiR5iqQPj0JIeoVdlG4VD4XA67mAcNa1fhzA1jwHuTRxDUQ//iYBczjHiTJcIuPyKlHQkv/LHQUYkuSi57yQT//uggfZNajQ3Vmz+Zt//+mm3Wm3Q576v////+32///5/EOgAAADVghQAAAAA//uQZAUAB1WI0PZugAAAAAoQwAAAEk3nRd2qAAAAACiDgAAAAAAABCqEEQRLCgwpBGMlJkIz8jKhGvj4k6jzRnqasNKIeoh5gI7BJaC1A1AoNBjJgbyApVS4IDlZgDU5WUAxEKDNmmALHzZp0Fkz1FMTmGFl1FMEyodIavcCAUHDWrKAIA4aa2oCgILEBupZgHvAhEBcZ6joQBxS76AgccrFlczBvKLC0QI2cBoCFvfTDAo7eoOQInqDPBtvrDEZBNYN5xwNwxQRfw8ZQ5wQVLvO8OYU+mHvFLlDh05Mdg7BT6YrRPpCBznMB2r//xKJjyyOh+cImr2/4doscwD6neZjuZR4AgAABYAAAABy1xcdQtxYBYYZdifkUDgzzXaXn98Z0oi9ILU5mBjFANmRwlVJ3/6jYDAmxaiDG3/6xjQQCCKkRb/6kg/wW+kSJ5//rLobkLSiKmqP/0ikJuDaSaSf/6JiLYLEYnW/+kXg1WRVJL/9EmQ1YZIsv/6Qzwy5qk7/+tEU0nkls3/zIUMPKNX/6yZLf+kFgAfgGyLFAUwY//uQZAUABcd5UiNPVXAAAApAAAAAE0VZQKw9ISAAACgAAAAAVQIygIElVrFkBS+Jhi+EAuu+lKAkYUEIsmEAEoMeDmCETMvfSHTGkF5RWH7kz/ESHWPAq/kcCRhqBtMdokPdM7vil7RG98A2sc7zO6ZvTdM7pmOUAZTnJW+NXxqmd41dqJ6mLTXxrPpnV8avaIf5SvL7pndPvPpndJR9Kuu8fePvuiuhorgWjp7Mf/PRjxcFCPDkW31srioCExivv9lcwKEaHsf/7ow2Fl1T/9RkXgEhYElAoCLFtMArxwivDJJ+bR1HTKJdlEoTELCIqgEwVGSQ+hIm0NbK8WXcTEI0UPoa2NbG4y2K00JEWbZavJXkYaqo9CRHS55FcZTjKEk3NKoCYUnSQ0rWxrZbFKbKIhOKPZe1cJKzZSaQrIyULHDZmV5K4xySsDRKWOruanGtjLJXFEmwaIbDLX0hIPBUQPVFVkQkDoUNfSoDgQGKPekoxeGzA4DUvnn4bxzcZrtJyipKfPNy5w+9lnXwgqsiyHNeSVpemw4bWb9psYeq//uQZBoABQt4yMVxYAIAAAkQoAAAHvYpL5m6AAgAACXDAAAAD59jblTirQe9upFsmZbpMudy7Lz1X1DYsxOOSWpfPqNX2WqktK0DMvuGwlbNj44TleLPQ+Gsfb+GOWOKJoIrWb3cIMeeON6lz2umTqMXV8Mj30yWPpjoSa9ujK8SyeJP5y5mOW1D6hvLepeveEAEDo0mgCRClOEgANv3B9a6fikgUSu/DmAMATrGx7nng5p5iimPNZsfQLYB2sDLIkzRKZOHGAaUyDcpFBSLG9MCQALgAIgQs2YunOszLSAyQYPVC2YdGGeHD2dTdJk1pAHGAWDjnkcLKFymS3RQZTInzySoBwMG0QueC3gMsCEYxUqlrcxK6k1LQQcsmyYeQPdC2YfuGPASCBkcVMQQqpVJshui1tkXQJQV0OXGAZMXSOEEBRirXbVRQW7ugq7IM7rPWSZyDlM3IuNEkxzCOJ0ny2ThNkyRai1b6ev//3dzNGzNb//4uAvHT5sURcZCFcuKLhOFs8mLAAEAt4UWAAIABAAAAAB4qbHo0tIjVkUU//uQZAwABfSFz3ZqQAAAAAngwAAAE1HjMp2qAAAAACZDgAAAD5UkTE1UgZEUExqYynN1qZvqIOREEFmBcJQkwdxiFtw0qEOkGYfRDifBui9MQg4QAHAqWtAWHoCxu1Yf4VfWLPIM2mHDFsbQEVGwyqQoQcwnfHeIkNt9YnkiaS1oizycqJrx4KOQjahZxWbcZgztj2c49nKmkId44S71j0c8eV9yDK6uPRzx5X18eDvjvQ6yKo9ZSS6l//8elePK/Lf//IInrOF/FvDoADYAGBMGb7FtErm5MXMlmPAJQVgWta7Zx2go+8xJ0UiCb8LHHdftWyLJE0QIAIsI+UbXu67dZMjmgDGCGl1H+vpF4NSDckSIkk7Vd+sxEhBQMRU8j/12UIRhzSaUdQ+rQU5kGeFxm+hb1oh6pWWmv3uvmReDl0UnvtapVaIzo1jZbf/pD6ElLqSX+rUmOQNpJFa/r+sa4e/pBlAABoAAAAA3CUgShLdGIxsY7AUABPRrgCABdDuQ5GC7DqPQCgbbJUAoRSUj+NIEig0YfyWUho1VBBBA//uQZB4ABZx5zfMakeAAAAmwAAAAF5F3P0w9GtAAACfAAAAAwLhMDmAYWMgVEG1U0FIGCBgXBXAtfMH10000EEEEEECUBYln03TTTdNBDZopopYvrTTdNa325mImNg3TTPV9q3pmY0xoO6bv3r00y+IDGid/9aaaZTGMuj9mpu9Mpio1dXrr5HERTZSmqU36A3CumzN/9Robv/Xx4v9ijkSRSNLQhAWumap82WRSBUqXStV/YcS+XVLnSS+WLDroqArFkMEsAS+eWmrUzrO0oEmE40RlMZ5+ODIkAyKAGUwZ3mVKmcamcJnMW26MRPgUw6j+LkhyHGVGYjSUUKNpuJUQoOIAyDvEyG8S5yfK6dhZc0Tx1KI/gviKL6qvvFs1+bWtaz58uUNnryq6kt5RzOCkPWlVqVX2a/EEBUdU1KrXLf40GoiiFXK///qpoiDXrOgqDR38JB0bw7SoL+ZB9o1RCkQjQ2CBYZKd/+VJxZRRZlqSkKiws0WFxUyCwsKiMy7hUVFhIaCrNQsKkTIsLivwKKigsj8XYlwt/WKi2N4d//uQRCSAAjURNIHpMZBGYiaQPSYyAAABLAAAAAAAACWAAAAApUF/Mg+0aohSIRobBAsMlO//Kk4soosy1JSFRYWaLC4qZBYWFRGZdwqKiwkNBVmoWFSJkWFxX4FFRQWR+LsS4W/rFRb/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////VEFHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAU291bmRib3kuZGUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMjAwNGh0dHA6Ly93d3cuc291bmRib3kuZGUAAAAAAAAAACU=");  
            snd.play();
        }

    </script>
    @yield('js_bottom')
</body>

</html>
