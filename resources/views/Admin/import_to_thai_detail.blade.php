@extends('Admin.layouts.layout')
@section('css_bottom')
@endsection
@section('body')
<div class="content">
    <div class="col-md-12">
    <div class="container-fluid">
        <form id="FormGetQRCode">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="content">
                            <h4 class="title">
                                <h4 class="modal-title" id="myModalLabel">{{$title_page or 'ข้อมูลใหม่'}}</h4>
                            </h4>
                            <div class="material-datatables">
                                <div class="modal-header">

                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-3">
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="add_container_id">@lang('lang.container_no')</label>
                                                <input type="text" class="form-control" name="container_id" id="add_container_id" value="{{ $Container->container_code }}" readonly>
                                                <!-- <select name="container_id" class="select2 form-control" tabindex="-1" data-placeholder="เลือกตู้" id="add_container_id" >
                                                    <option value="">เลือกตู้</option>
                                                    @if(!$Containers->isEmpty())
                                                        @foreach($Containers as $Container)
                                                            <option value="{{ $Container->id }}" {{ $Container && $Container->id == $Container->id ? 'selected' : 'null' }} >{{ $Container->container_code }}</option>
                                                        @endforeach
                                                    @endif
                                                </select> -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <div class="row">
                                        <div class="col-md-3">
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="add_qrcode">Barcode สินค้า</label>
                                                <input type="text" class="form-control" name="qr_code" id="add_qrcode" value="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12" style="text-align: center;">
                                            <button type="submit" class="btn btn-get-qrcode btn-primary">ตกลง</button>
                                        </div>
                                    </div> -->
                                </div>
                                <div class="modal-footer">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <form id="FormAddImportToThai">
            <input type="hidden" name="container_id" id="show_container_id">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="content">
                            <div class="material-datatables">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="myModalLabel">@lang('lang.list_po')</h5>
                                </div>
                                    <div class="modal-body">
                                        <table class="table">
                                            <thead class="text-primary">
                                                <tr>
                                                    <th>#</th>
                                                    <th>@lang('lang.customer_id')</th>
                                                    <th>@lang('lang.number_po')</th>
                                                    <th>@lang('lang.delivery_dates')</th>
                                                    <th>@lang('lang.number_of_po')</th>
                                                    <th>@lang('lang.weigh')</th>
                                                    <th>@lang('lang.conclude')</th>
                                                </tr>
                                            </thead>
                                            <tbody id="list-po">
                                                @if(! $ImportToChainas->isEmpty())
                                                    @foreach($ImportToChainas as $ImportToChaina)
                                                        <tr>
                                                            <td>#</td>
                                                            <td>{{ $ImportToChaina->User ? $ImportToChaina->User->customer_general_code : null }}</td>
                                                            <td>{{ $ImportToChaina->po_no }}</td>
                                                            <td>{{ $ImportToChaina->import_to_thai_date }}</td>
                                                            <td>{{ number_format($ImportToChaina->qty_chaina) }}</td>
                                                            <td>{{ number_format($ImportToChaina->weight_all, 3) }}</td>
                                                            <td>{{ number_format($ImportToChaina->cubic * $ImportToChaina->qty_thai, 3) }}</td>
                                                        </tr>
                                                    @endforeach
                                                @else
                                                    <tr id="op-not-found">
                                                        <td colspan=10 style="text-align: center;">@lang('lang.data_not_found')</td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                            <tfooter>

                                            </tfooter>
                                        </table>
                                    </div>
                                    <div class="modal-footer">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="content">
                                <div class="material-datatables">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="myModalLabel">@lang('lang.barcode_list')</h5>
                                    </div>
                                    <div class="modal-body">
                                        <table class="table">
                                            <thead class="text-primary">
                                                <tr>
                                                    <th>#</th>
                                                    <th>@lang('lang.number_po')</th>
                                                    <th>@lang('lang.qrcode')</th>
                                                    <th>@lang('lang.lot_id')</th>
                                                    <th>@lang('lang.customer_id')</th>
                                                    <th>@lang('lang.type_product')</th>
                                                    <th>@lang('lang.product_name')</th>
                                                </tr>
                                            </thead>
                                            <tbody id="list-qr-code">
                                                @if(! $QrCodeProducts->isEmpty())
                                                    @foreach($QrCodeProducts as $QrCodeProduct)
                                                        <tr>
                                                            <td>#
                                                                <input type="hidden" class="po_no" value="{{$QrCodeProduct->po_no}}">
                                                                <input type="hidden" class="lot_product_id" value="{{$QrCodeProduct->lot_id}}">
                                                                <input type="hidden" class="qr_code_name" value="{{$QrCodeProduct->qr_code}}">
                                                                <input type="hidden" class="po_qty_chaina" value="{{$QrCodeProduct->po_qty_chaina}}">
                                                                <input type="hidden" class="po_qty_container" value="{{$QrCodeProduct->po_qty_container}}">
                                                                <input type="hidden" class="po_qty_thai" value="{{$QrCodeProduct->po_qty_thai}}">
                                                                <input type="hidden" class="lot_product_qty" value="{{$QrCodeProduct->lot_product_qty}}">
                                                            </td>
                                                            <td>{{ $QrCodeProduct->po_no }}</td>
                                                            <td>{{ $QrCodeProduct->qr_code }}</td>
                                                            <td>{{ 'Lot'.$QrCodeProduct->lot_id }}</td>
                                                            <td>{{ $QrCodeProduct->customer_general_code }}</td>
                                                            <td>{{ $QrCodeProduct->product_type_name }}</td>
                                                            <td>{{ $QrCodeProduct->product_name }}</td>
                                                        </tr>
                                                    @endforeach
                                                @else
                                                    <tr id="qr-code-not-found">
                                                        <td colspan=10 style="text-align: center;">@lang('lang.data_not_found')</td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                            <tfooter>

                                            </tfooter>
                                        </table>
                                    </div>
                                    <div class="modal-footer">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="content">
                                <div class="material-datatables">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="myModalLabel">@lang('lang.table_of_products_scanned_by_po')</h5>
                                    </div>
                                    <div class="modal-body">
                                        <table class="table">
                                            <thead class="text-primary">
                                                <tr>
                                                    <th>#</th>
                                                    <th>@lang('lang.number_po')</th>
                                                    <th>@lang('lang.pieces_by_po')</th>
                                                    <th>@lang('lang.number_piece_close')</th>
                                                    <th>@lang('lang.actual_number')</th>
                                                </tr>
                                            </thead>
                                            <tbody id="list-products">
                                                <!-- @if(! $ImportToChainas->isEmpty())
                                                    @foreach($ImportToChainas as $ImportToChaina)
                                                        @if($ImportToChaina->qty_chaina == $ImportToChaina->qty_container && $ImportToChaina->qty_container == $ImportToChaina->qty_thai)
                                                            @php $style = 'rgba(76, 175, 80, 0.42)'; @endphp
                                                        @elseif($ImportToChaina->qty_chaina == $ImportToChaina->qty_container && $ImportToChaina->qty_container != $ImportToChaina->qty_thai)
                                                            @php $style = 'rgba(255, 235, 59, 0.5)'; @endphp
                                                        @else
                                                            @php $style = 'rgba(255, 0, 0, 0.33)'; @endphp
                                                        @endif
                                                        <tr style="background-color: {{ $style }}">
                                                            <td>#</td>
                                                            <td>{{ $ImportToChaina->po_no }}</td>
                                                            <td>{{ number_format($ImportToChaina->qty_chaina) }}</td>
                                                            <td>{{ number_format($ImportToChaina->qty_container) }}</td>
                                                            <td>{{ number_format($ImportToChaina->qty_thai) }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td></td>
                                                            <td>Lot ID</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                        </tr>
                                                    @endforeach
                                                @else
                                                    <tr id="products-not-found">
                                                        <td colspan=10 style="text-align: center;">@lang('lang.data_not_found')</td>
                                                    </tr>
                                                @endif -->
                                            </tbody>
                                        <tfooter>

                                        </tfooter>
                                        </table>

                                    </div>
                                    <div class="modal-footer">
                                        <!-- <button type="submit" class="btn btn-primary">บันทึก</button>
                                        <button type="cancel" class="btn btn-danger">ยกเลิก</button> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
@section('js_bottom')
<script src="{{asset('assets/global/plugins/orakuploader/orakuploader.js')}}"></script>
<script>

    $(function(){
        showPOandLot();
    });

    function showPOandLot(){
        var product_for_show = $('#list-qr-code > tr');
        var groups = {};
        var html = '';
        if (product_for_show.length > 0) {
            $.each(product_for_show, function(k, v){
                var po = $(v).find('.po_no').val();
                var lot_id = $(v).find('.lot_product_id').val();
                var qr_code_name = $(v).find('.qr_code_name').val();
                var po_qty_chaina = $(v).find('.po_qty_chaina').val();
                var po_qty_container = $(v).find('.po_qty_container').val();
                var po_qty_thai = $(v).find('.po_qty_thai').val();
                var lot_product_qty = $(v).find('.lot_product_qty').val();
                if (!groups[po]) {
                    groups[po] = {};
                }
                if (!groups[po][lot_id]) {
                    groups[po][lot_id] = {};
                }
                if (!groups[po][lot_id]['amount']) {
                    groups[po][lot_id]['amount'] = [];
                }
                groups[po]['po_qty_chaina'] = po_qty_chaina;
                groups[po]['po_qty_container'] = po_qty_container;
                groups[po]['po_qty_thai'] = po_qty_thai;
                groups[po][lot_id]['lot_product_qty'] = lot_product_qty;
                groups[po][lot_id]['amount'].push(qr_code_name);
            });
            // console.log(groups);
            var No_Po = 1;
            var style = '';
            $.each(groups, function(k1, v1){
                var sumallproduct = 0;
                $.each(v1, function(k2, v2){
                    if (k2 != 'po_qty_chaina' && k2 != 'po_qty_container' && k2 != 'po_qty_thai') {
                        sumallproduct += v2.amount.length
                    }
                });
                if(v1.po_qty_container == v1.po_qty_thai){
                    style = 'rgba(76, 175, 80, 0.42)';
                }else if(v1.po_qty_container != v1.po_qty_thai){
                    style = 'rgba(255, 235, 59, 0.5)';
                }else{
                    style = 'rgba(255, 0, 0, 0.33)';
                }
                html += '\
                    <tr style="background-color: '+style+'">\
                        <td>'+No_Po+'</td>\
                        <td>'+k1+'</td>\
                        <td>'+v1.po_qty_chaina+'</td>\
                        <td class="text-center">'+addNumformat(v1.po_qty_container)+'</td>\
                        <td class="text-center">'+addNumformat(sumallproduct)+'</td>\
                    </tr>';
                $.each(v1, function(k2, v2){
                    if (k2 != 'po_qty_chaina' && k2 != 'po_qty_container' && k2 != 'po_qty_thai') {
                        html += '\
                            <tr style="background-color: '+style+'">\
                                <td></td>\
                                <td></td>\
                                <td class="text-right">Lot '+k2+'</td>\
                                <td class="text-center">'+addNumformat(v2.lot_product_qty)+'</td>\
                                <td class="text-center">'+addNumformat(v2.amount.length)+'</td>\
                            </tr>';
                    }
                });
                No_Po++;
            });
        }else{
            html += '\
                <tr>\
                    <td colspan="5" class="text-center">@lang('lang.data_not_found')</td>\
                </tr>';
        }

        $('#list-products').html(html)
    }

</script>
@endsection
