﻿@extends('Admin.layouts.layout')
@section('css_bottom')
@endsection
@section('body')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="content">
                        <h4 class="title">
                            {{$title_page or '' }}
                            <button class="btn btn-success btn-add pull-right" >
                                + @lang('lang.create_data')
                            </button>
                        </h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <div class="material-datatables">
                            <div class="table-responsive">
                                <table id="TableList" class="table table-striped table-no-bordered table-hover" style="width:100%;cellspacing:0">
                                    <thead>
                                        <tr>
                                            <th>
                                                <input type="checkbox" class="f" id="checkbox" name="feature"/>
                                            </th>
                                            <th>@lang('lang.type_id')</th>
                                            <th>@lang('lang.name_type')</th>
                                            <th>@lang('lang.description')</th>
                                            <th>@lang('lang.status')</th>
                                            <th>@lang('lang.tool')</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            <div class="row" style="margin-left: 20px;">
                                <div class="form-group">
                                    <select type="text" class="select2 form-control" name="product_type_id" id="add_product_type_id">
                                        <option value="">@lang('lang.select_proceed')</option>
                                        <option value="D">ลบรายการที่ถูกเลือกทั้งหมด</option>
                                        <!-- <option value="T">ปิดสถานะทั้งหมด</option>
                                        <option value="F">เปิดสถานะทั้งหมด</option> -->
                                    </select>
                                </div>
                                <div class="form-group" style="margin-left: 10px;">
                                     <button class="btn btn-info btn-confirm-delete pull-right" >
                                        @lang('lang.proceed')
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('modal')
<div class="modal" id="ModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="FormAdd">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">@lang('lang.create') {{$title_page or 'ข้อมูลใหม่'}}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">

                <div class="form-group">
                    <label for="add_code">@lang('lang.code')</label>
                    <input type="text" class="form-control" name="code" id="add_code" required="" placeholder="@lang('lang.code')">
                </div>

                <div class="form-group">
                    <label for="add_name">@lang('lang.name')</label>
                    <input type="text" class="form-control" name="name" id="add_name" required="" placeholder="@lang('lang.name')">
                </div>

                <div class="form-group">
                    <label for="add_detail">@lang('lang.detail')</label>
                    <textarea class="form-control" name="detail" id="add_detail"  placeholder="@lang('lang.detail')"></textarea>
                </div>

                <div class="form-check">
                    <label for="add_active" class="checkbox form-check-label">
                        <input type="checkbox" class="form-check-input" data-toggle="checkbox" name="active" id="add_active" required="" value="T" checked="checked"> @lang('lang.active')
                    </label>
                </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">@lang('lang.save')</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" id="ModalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <input type="hidden" name="edit_id" id="edit_id">
            <form id="FormEdit">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">@lang('lang.edit_data') {{$title_page or 'ข้อมูลใหม่'}}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">

                <div class="form-group">
                    <label for="edit_code">@lang('lang.code')</label>
                    <input type="text" class="form-control" name="code" id="edit_code" required="" placeholder="@lang('lang.code')">
                </div>

                <div class="form-group">
                    <label for="edit_name">@lang('lang.name')</label>
                    <input type="text" class="form-control" name="name" id="edit_name" required="" placeholder="@lang('lang.name')">
                </div>

                <div class="form-group">
                    <label for="edit_detail">@lang('lang.detail')</label>
                    <textarea class="form-control" name="detail" id="edit_detail"  placeholder="@lang('lang.detail')"></textarea>
                </div>

                <div class="form-check">
                    <label for="edit_active" class="checkbox form-check-label">
                        <input type="checkbox" class="form-check-input" data-toggle="checkbox" name="active" id="edit_active" required="" value="T"> @lang('lang.active')
                    </label>
                </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">@lang('lang.save')</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('js_bottom')
<script src="{{asset('assets/global/plugins/orakuploader/orakuploader.js')}}"></script>
<script>

     var TableList = $('#TableList').dataTable({
        "ajax": {
            "url": url_gb+"/admin/{{$lang}}/ProductType/Lists",
            "data": function ( d ) {
                //d.myKey = "myValue";
                // d.custom = $('#myInput').val();
                // etc
            }
        },
        "columns": [
            {"data" : "id","className":"action text-center","searchable" : false , "orderable" : false },
            {"data" : "code"},
            {"data" : "name"},
            {"data" : "detail"},
            {"data" : "active"},
            {"data" : "action","className":"action text-center","searchable" : false , "orderable" : false }
        ]
    });
    $('body').on('click','.btn-add',function(data){
        ShowModal('ModalAdd');
    });
    $('body').on('click','.btn-edit',function(data){
        var btn = $(this);
        btn.button('loading');
        var id = $(this).data('id');
        $('#edit_id').val(id);
        $.ajax({
            method : "GET",
            url : url_gb+"/admin/ProductType/"+id,
            dataType : 'json'
        }).done(function(rec){
            $('#edit_code').val(rec.code);
            $('#edit_name').val(rec.name);
            $('#edit_detail').val(rec.detail);
            if(rec.active=='T'){
                $('#edit_active').prop('checked','checked').closest('label').addClass('checked');
            }else{
                $('#edit_active').removeAttr('checked').closest('label').removeClass('checked');
            }

            btn.button("reset");
            ShowModal('ModalEdit');
        }).fail(function(){
            swal("system.system_alert","system.system_error","error");
            btn.button("reset");
        });
    });

    $('#FormAdd').validate({
        errorElement: 'div',
        errorClass: 'invalid-feedback',
        focusInvalid: false,
        rules: {

            code: {
                required: true,
            },
            name: {
                required: true,
            },
            active: {
                required: true,
            },
        },
        messages: {

            code: {
                required: "@lang('lang.please_specify')",
            },
            name: {
                required: "@lang('lang.please_specify')",
            },
            active: {
                required: "@lang('lang.please_specify')",
            },
        },
        highlight: function (e) {
            validate_highlight(e);
        },
        success: function (e) {
            validate_success(e);
        },

        errorPlacement: function (error, element) {
            validate_errorplacement(error, element);
        },
        submitHandler: function (form) {
            /*
            if(CKEDITOR!==undefined){
                for ( instance in CKEDITOR.instances ){
                    CKEDITOR.instances[instance].updateElement();
                }
            }
            */
            var btn = $(form).find('[type="submit"]');
            var data_ar = removePriceFormat(form,$(form).serializeArray());
            btn.button("loading");
            $.ajax({
                method : "POST",
                url : url_gb+"/admin/ProductType",
                dataType : 'json',
                data : $(form).serialize()
            }).done(function(rec){
                btn.button("reset");
                if(rec.status==1){
                    TableList.api().ajax.reload();
                    resetFormCustom(form);
                    swal(rec.title,rec.content,"success");
                    $('#ModalAdd').modal('hide');
                }else{
                    swal(rec.title,rec.content,"error");
                }
            }).fail(function(){
                swal("system.system_alert","system.system_error","error");
                btn.button("reset");
            });
        },
        invalidHandler: function (form) {

        }
    });

    $('#FormEdit').validate({
        errorElement: 'div',
        errorClass: 'invalid-feedback',
        focusInvalid: false,
        rules: {

            code: {
                required: true,
            },
            name: {
                required: true,
            },
            active: {
                required: true,
            },
        },
        messages: {

            code: {
                required: "@lang('lang.please_specify')",
            },
            name: {
                required: "@lang('lang.please_specify')",
            },
            active: {
                required: "@lang('lang.please_specify')",
            },
        },
        highlight: function (e) {
            validate_highlight(e);
        },
        success: function (e) {
            validate_success(e);
        },

        errorPlacement: function (error, element) {
            validate_errorplacement(error, element);
        },
        submitHandler: function (form) {
            /*
            if(CKEDITOR!==undefined){
                for ( instance in CKEDITOR.instances ){
                    CKEDITOR.instances[instance].updateElement();
                }
            }
            */
            var btn = $(form).find('[type="submit"]');
            var id = $('#edit_id').val();
            btn.button("loading");
            $.ajax({
                method : "POST",
                url : url_gb+"/admin/ProductType/"+id,
                dataType : 'json',
                data : $(form).serialize()
            }).done(function(rec){
                btn.button("reset");
                if(rec.status==1){
                    TableList.api().ajax.reload();
                    resetFormCustom(form);
                    swal(rec.title,rec.content,"success");
                    $('#ModalEdit').modal('hide');
                }else{
                    swal(rec.title,rec.content,"error");
                }
            }).fail(function(){
                swal("system.system_alert","system.system_error","error");
                btn.button("reset");
            });
        },
        invalidHandler: function (form) {

        }
    });

    $('body').on('click','.btn-delete',function(e){
        e.preventDefault();
        var btn = $(this);
        var id = btn.data('id');
        swal({
            title: "@lang('lang.do_you_want_to_delete')",
            text: "@lang('lang.if_you_delete_you_will_not_be_able_to_restore_it')",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: "@lang('lang.do_you_want_to_delete')",
            cancelButtonText: "@lang('lang.cancel')",
            showLoaderOnConfirm: true,
            buttonsStyling: false
        }).then(function() {
            $.ajax({
                method : "POST",
                url : url_gb+"/admin/ProductType/Delete/"+id,
                data : {ID : id}
            }).done(function(rec){
                if(rec.status==1){
                    swal(rec.title,rec.content,"success");
                    TableList.api().ajax.reload();
                }else{
                    swal("ระบบมีปัญหา","กรุณาติดต่อผู้ดูแล","error");
                }
            }).fail(function(data){
                swal("ระบบมีปัญหา","กรุณาติดต่อผู้ดูแล","error");
            });
        }).catch(function(e){
            //console.log(e);
        });
    });

    $('body').on('change','#checkbox',function(data){
        if($(this).is(':checked')) {
            $('.checkbox-product-type').prop('checked', true);
        }else{
            $('.checkbox-product-type').prop('checked', false);
        }

    });

    $('body').on('click','.btn-confirm-delete',function(e){
        var data_select = {};
        var status = $('#add_product_type_id').val();
        $("input[name='product_type_id[]']:checked").each( function (k,v) {
            data_select[k] = $(this).val();
        });

        swal({
            title: "@lang('lang.do_you_want_to_delete')",
            text: "@lang('lang.if_you_delete_you_will_not_be_able_to_restore_it')",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: "@lang('lang.do_you_want_to_delete')",
            cancelButtonText: "@lang('lang.cancel')",
            showLoaderOnConfirm: true,
            buttonsStyling: false
        }).then(function() {
            $.ajax({
                method : "POST",
                url : url_gb+"/admin/ProductType/DeleteAll",
                data : {
                    id : data_select,
                    status: status,
                }
            }).done(function(rec){
                if(rec.status==1){
                    swal(rec.title,rec.content,"success");
                    TableList.api().ajax.reload();
                }else{
                    swal(rec.title,rec.content,"error");
                }
            }).fail(function(data){
                swal("ระบบมีปัญหา","กรุณาติดต่อผู้ดูแล","error");
            });
        }).catch(function(e){
            //console.log(e);
        });

    });
</script>
@endsection
