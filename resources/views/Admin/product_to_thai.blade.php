﻿@extends('Admin.layouts.layout')
@section('css_bottom')
<style>
    .fix-scroll-dashboard {
        width: 100%;
        overflow-x: scroll;
    }   
</style>
@endsection
@section('body')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="content">
                        <h4 class="title">
                            {{$title_page or '' }}
                            <!-- <a href="{{ url('/admin/ImportToThai/Create') }}" class="btn btn-success btn-add pull-right" >
                                + เพิ่มข้อมูล
                            </a> -->
                        </h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <div class="material-datatables fix-scroll-dashboard">
                            <div class="table-responsive">
                                <table id="TableList" class="table table-striped table-no-bordered table-hover" style="width:100%;cellspacing:0">
                                    <thead>
                                        <tr>
                                            <th>@lang('lang.customer_id')</th>
                                            <!-- <th>@lang('lang.customer_name')</th> -->
                                            <th>@lang('lang.container_id')</th>
                                            <th>@lang('lang.number_po')</th>
                                            <th>@lang('lang.pieces_by_po')</th>
                                            <th>@lang('lang.number_piece_close')</th>
                                            <th>@lang('lang.number_of_warehouse_in_thai')</th>
                                            <th>@lang('lang.tatal_weigth')</th>
                                            <th>@lang('lang.total_queue')</th>
                                            <th>@lang('lang.date_import_from_china')</th>
                                            <th>@lang('lang.status')</th>
                                            <th>@lang('lang.tool')</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('modal')
<div class="modal" id="ModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form id="FormAdd">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">@lang('lang.create_data') {{$title_page or 'ข้อมูลใหม่'}}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">

                <div class="form-group">
                    <label for="add_container_id">@lang('lang.container_id')</label>
                    <select name="container_id" class="select2 form-control" tabindex="-1" data-placeholder="@lang('lang.select_container')" id="add_container_id" required="" >
                        <option value="">@lang('lang.select_container')</option>
                        @foreach($Containers as $Container)
                        <option value="{{$Container->id}}">{{$Container->container_code}}</option>
                        @endforeach
                    </select>
                </div>

                <label for="add_date_import">@lang('lang.date_import')</label>
                <div class="input-group" data-date="{{date('Y-m-d')}}">
                    <input type="text" value="" readonly="readonly" class="form-control form_date_time" name="date_import" id="add_date_import" required="" placeholder="@lang('lang.date_import')">
                    <span class="input-group-addon remove_date_time"><i class="glyphicon glyphicon-remove icon-remove"></i></span>
                    <span class="input-group-addon trigger_date_time" for="add_date_import"><i class="glyphicon glyphicon-calendar icon-calendar"></i></span>
                </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">@lang('lang.save')</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" id="ModalDetail" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form id="FormEdit">
                <input type="hidden" name="import_to_thai_id" id="import_to_thai_id">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">@lang('lang.view_detail') {{$title_page or 'ข้อมูลใหม่'}}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="show_po_no">@lang('lang.number_po')</label>
                            <input type="text" class="form-control" id="show_po_no" readonly>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="show_user_code">@lang('lang.customer_id')</label>
                            <input type="text" class="form-control" id="show_user_code" readonly>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="show_user_name">@lang('lang.customer_name')</label>
                            <input type="text" class="form-control" id="show_user_name" readonly>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="show_user_main_mobile">@lang('index.mobile')</label>
                            <input type="text" class="form-control" id="show_user_main_mobile" readonly>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="show_plate_no">@lang('lang.truck_registration')</label>
                            <input type="text" class="form-control" id="show_plate_no" readonly>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="show_delivery_date">@lang('lang.delivery_date')</label>
                            <input type="text" class="form-control" id="show_delivery_date" readonly>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="show_user_address">@lang('lang.address_delivery')</label>
                            <input type="text" class="form-control" id="show_user_address" readonly>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="show_qty_chaina">@lang('lang.pieces_by_po')</label>
                            <input type="text" class="form-control" id="show_qty_chaina" readonly>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="show_qty_container">@lang('lang.number_piece_close')</label>
                            <input type="text" class="form-control" id="show_qty_container" readonly>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="show_qty_thai">@lang('lang.actual_number')</label>
                            <input type="text" class="form-control" id="show_qty_thai" readonly>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="show_import_to_chaina_status">@lang('lang.status')</label>
                            <input type="text" class="form-control" id="show_import_to_chaina_status" readonly>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="show_import_to_thai_date">@lang('lang.delivery_dates')</label>
                            <input type="text" class="form-control" id="show_import_to_thai_date" readonly>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" style="text-align: center;">
                        <h4>@lang('lang.barcode_list')</h4>
                    </div>
                </div>
                <div class="row">
                    <!-- <table class="table table-bordered table-lg mt-lg mb-0">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>ชื่อร้านค้าจีน</th>
                                <th>ประเภท</th>
                                <th>รหัสสินค้า</th>
                                <th>ชื่อสินค้า</th>
                                <th>จำนวน</th>
                                <th>น้ำหนัก</th>
                                <th>เดินทางโดย</th>
                                <th>รายละเอียด</th>
                            </tr>
                        </thead>
                        <tbody id="list-po-products">

                        </tbody>
                    </table> -->
                    <table id="TableListProducts" class="table table-striped table-no-bordered table-hover" style="width:100%;cellspacing:0">
                        <thead>
                            <tr>
                                <th>@lang('lang.no')</th>
                                <th>@lang('lang.name_store_in_china')</th>
                                <th>@lang('lang.type_product')</th>
                                <!-- <th>@lang('lang.product_id')</th> -->
                                <th>@lang('lang.product_name')</th>
                                <th>@lang('lang.quantity')</th>
                                <th>@lang('lang.weigh')</th>
                                <th>@lang('lang.travel_by')</th>
                            </tr>
                        </thead>
                    </table>
                </div>

                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-primary Close">@lang('lang.close')</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('js_bottom')
<script src="{{asset('assets/global/plugins/orakuploader/orakuploader.js')}}"></script>
<script>

     var TableList = $('#TableList').dataTable({
        "ajax": {
            "url": url_gb+"/admin/{{$lang}}/ProductToThai/Lists",
            "data": function ( d ) {
                //d.myKey = "myValue";
                // d.custom = $('#myInput').val();
                // etc
            }
        },
        "order": [[ 9, "desc" ]],
        "columns": [
            {"data" : "customer_general_code", "name": "users.customer_general_code"},
            // {"data" : "customer", "name": "users.firstname", "searchable" : false},
            {"data" : "container_code", "name": "containers.container_code"},
            {"data" : "po_no", "name": "import_to_chaina.po_no"},
            {"data" : "qty_chaina", "searchable" : false},
            {"data" : "qty_container", "searchable" : false},
            {"data" : "qty_thai", "searchable" : false},
            {"data" : "weight_all", "searchable" : false},
            {"data" : "cubic_all", "searchable" : false},
            {"data" : "date_import", "name": "import_to_thai.created_at"},
            {"data" : "status", "searchable" : false , "orderable" : false},
            { "data": "action","className":"action text-center", "searchable" : false , "orderable" : false}
        ]
    });

    var TableListProducts = $('#TableListProducts').dataTable({
       "ajax": {
           "url": url_gb+"/admin/ProductToThai/DetailLists",
           "data": function ( d ) {
               //d.myKey = "myValue";
               d.id = $('#import_to_thai_id').val();
               // etc
           }
       },
       "columns": [
           {"data": "DT_Row_Index" , "className": "text-center", "searchable": false, "orderable": false },
           {"data" : "shop_chaina_name", "name": "product_import_to_chaina.shop_chaina_name"},
           {"data" : "product_type_name", "name": "product_types.name"},
           // {"data" : "product_code", "name": "products.code"},
           {"data" : "product_name", "name": "products.name"},
           {"data" : "qty", "name": "product_import_to_chaina.qty"},
           {"data" : "weight_all", "name": "product_import_to_chaina.weight_all"},
           {"data" : "transport_type_name_{{$lang}}", "searchable": false, "orderable": false },
       ]
    });

    $('body').on('click','.btn-add',function(data){
        //ShowModal('ModalAdd');
    });
    $('body').on('click','.btn-detail',function(data){

        var btn = $(this);
        btn.button('loading');
        var id = $(this).data('id');
        $('#edit_id').val(id);
        $.ajax({
            method : "GET",
            url : url_gb+"/admin/ProductToThai/"+id,
            dataType : 'json'
        }).done(function(rec){
            $('#list-po-products').html('');
            var qty_chaina = 0;
            $.each( rec.ImportToThai.import_to_chaina.product_import_to_chaina, function( key, value ) {
                qty_chaina += value.qty;
            });

            if(rec.DeliveryToUser != null){
                $('#show_delivery_date').val(rec.DeliveryToUser.delivery.date_delivery);
                $('#show_plate_no').val(rec.DeliveryToUser.delivery.plate_no);
                $('#show_status_name').val(rec.DeliveryToUser.delivery.delivery_status.name);
            }

            if(rec.DeliverySlipList != null){
                $('#show_user_address').val(rec.DeliverySlipList.delivery_slip.user_address.address
                    +' '+rec.DeliverySlipList.delivery_slip.user_address.amphure.amphure_name
                    +' '+rec.DeliverySlipList.delivery_slip.user_address.province.province_name
                    +' '+rec.DeliverySlipList.delivery_slip.user_address.amphure.zipcode)
            }else{
                console.log('error');
            }

            var html = '';
            $('#show_po_no').val(rec.ImportToThai.import_to_chaina.po_no);
            $('#show_user_code').val(rec.ImportToThai.import_to_chaina.user.customer_general_code);
            $('#show_user_name').val(rec.ImportToThai.import_to_chaina.user.firstname+' '+rec.ImportToThai.import_to_chaina.user.lastname);
            $('#import_to_thai_id').val(rec.ImportToThai.id);
            $('#show_user_main_mobile').val(rec.ImportToThai.import_to_chaina.user.main_mobile);
            $('#show_import_to_chaina_status').val(rec.ImportToThai.import_to_chaina.import_to_chaina_status.name);
            $('#show_import_to_thai_date').val(rec.ImportToThai.date_import);
            $('#show_qty_thai').val(addNumformat(rec.ImportToThai.product_import_to_thai.length));
            $('#show_qty_container').val(addNumformat(rec.ImportToThai.container.rel_container_product.length));
            $('#show_qty_chaina').val(addNumformat(qty_chaina));

            TableListProducts.api().ajax.reload();
            ShowModal('ModalDetail');
        }).fail(function(){
            swal("system.system_alert","system.system_error","error");
            btn.button("reset");
        });
    });

    $('#FormAdd').validate({
        errorElement: 'div',
        errorClass: 'invalid-feedback',
        focusInvalid: false,
        rules: {

            container_id: {
                required: true,
            },
            date_import: {
                required: true,
            },
        },
        messages: {

            container_id: {
                required: "กรุณาระบุ",
            },
            date_import: {
                required: "กรุณาระบุ",
            },
        },
        highlight: function (e) {
            validate_highlight(e);
        },
        success: function (e) {
            validate_success(e);
        },

        errorPlacement: function (error, element) {
            validate_errorplacement(error, element);
        },
        submitHandler: function (form) {
            /*
            if(CKEDITOR!==undefined){
                for ( instance in CKEDITOR.instances ){
                    CKEDITOR.instances[instance].updateElement();
                }
            }
            */
            var btn = $(form).find('[type="submit"]');
            var data_ar = removePriceFormat(form,$(form).serializeArray());
            btn.button("loading");
            $.ajax({
                method : "POST",
                url : url_gb+"/admin/ImportToThai",
                dataType : 'json',
                data : $(form).serialize()
            }).done(function(rec){
                btn.button("reset");
                if(rec.status==1){
                    TableList.api().ajax.reload();
                    resetFormCustom(form);
                    swal(rec.title,rec.content,"success");
                    $('#ModalAdd').modal('hide');
                }else{
                    swal(rec.title,rec.content,"error");
                }
            }).fail(function(){
                swal("system.system_alert","system.system_error","error");
                btn.button("reset");
            });
        },
        invalidHandler: function (form) {

        }
    });

    $('#FormEdit').validate({
        errorElement: 'div',
        errorClass: 'invalid-feedback',
        focusInvalid: false,
        rules: {

            container_id: {
                required: true,
            },
            date_import: {
                required: true,
            },
        },
        messages: {

            container_id: {
                required: "กรุณาระบุ",
            },
            date_import: {
                required: "กรุณาระบุ",
            },
        },
        highlight: function (e) {
            validate_highlight(e);
        },
        success: function (e) {
            validate_success(e);
        },

        errorPlacement: function (error, element) {
            validate_errorplacement(error, element);
        },
        submitHandler: function (form) {
            /*
            if(CKEDITOR!==undefined){
                for ( instance in CKEDITOR.instances ){
                    CKEDITOR.instances[instance].updateElement();
                }
            }
            */
            var btn = $(form).find('[type="submit"]');
            var id = $('#edit_id').val();
            btn.button("loading");
            $.ajax({
                method : "POST",
                url : url_gb+"/admin/ImportToThai/"+id,
                dataType : 'json',
                data : $(form).serialize()
            }).done(function(rec){
                btn.button("reset");
                if(rec.status==1){
                    TableList.api().ajax.reload();
                    resetFormCustom(form);
                    swal(rec.title,rec.content,"success");
                    $('#ModalEdit').modal('hide');
                }else{
                    swal(rec.title,rec.content,"error");
                }
            }).fail(function(){
                swal("system.system_alert","system.system_error","error");
                btn.button("reset");
            });
        },
        invalidHandler: function (form) {

        }
    });

    $('body').on('click','.btn-delete',function(e){
        e.preventDefault();
        var btn = $(this);
        var id = btn.data('id');
        swal({
            title: "คุณต้องการลบใช่หรือไม่",
            text: "หากคุณลบจะไม่สามารถเรียกคืนข้อมูลกลับมาได้",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: "ใช่ ฉันต้องการลบ",
            cancelButtonText: "ยกเลิก",
            showLoaderOnConfirm: true,
            buttonsStyling: false
        }).then(function() {
            $.ajax({
                method : "POST",
                url : url_gb+"/admin/ImportToThai/Delete/"+id,
                data : {ID : id}
            }).done(function(rec){
                if(rec.status==1){
                    swal(rec.title,rec.content,"success");
                    TableList.api().ajax.reload();
                }else{
                    swal("ระบบมีปัญหา","กรุณาติดต่อผู้ดูแล","error");
                }
            }).fail(function(data){
                swal("ระบบมีปัญหา","กรุณาติดต่อผู้ดูแล","error");
            });
        }).catch(function(e){
            //console.log(e);
        });
    });

    $('#add_container_id').select2();
    $('#edit_container_id').select2();

</script>
@endsection
