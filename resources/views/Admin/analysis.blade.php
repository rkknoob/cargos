﻿@extends('Admin.layouts.layout')
@section('css_bottom')
<style>
.fix-scroll-dashboard {
    width: 100%;
    overflow-x: scroll;
}
</style>
@endsection
@section('body')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="content">
                        <h4 class="title">
                            {{$title_page or '' }}
                        </h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <div class="material-datatables">
                            <div class="table-responsive" style="overflow-x: scroll;">
                                <table id="TableList" class="table table-striped table-no-bordered table-hover" style="cellspacing:0">
                                    <thead>
                                        <tr>
                                            <th style='width: 500px;'>@lang('lang.number_po')</th>
                                            <th style='width: 500px;'>@lang('lang.general_customer_id')</th>
                                            <th>@lang('lang.good_received')</th>
                                            <th>@lang('lang.good_delivery')</th>
                                            <th>@lang('lang.descriptions_of_good')</th>
                                            <th>@lang('lang.container_code')</th>
                                            <th>@lang('lang.package')</th>
                                            <th>@lang('lang.pcs_rom_store_chinese')</th>
                                            <th>@lang('lang.weight_kg')</th>
                                            <th>@lang('lang.size_cm_3')</th>
                                            <th>@lang('lang.price')</th>
                                            <th>@lang('lang.status')</th>
                                            <th>@lang('lang.ship_by')</th>
                                            <th>@lang('lang.duration_days')</th>
                                            <th>@lang('lang.delivery_cost')</th>
                                            <th>@lang('lang.po_store')</th>
                                            <th>@lang('lang.product_category')</th>
                                            <th>@lang('lang.note')</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('modal')

@endsection
@section('js_bottom')
<script src="{{asset('assets/global/plugins/orakuploader/orakuploader.js')}}"></script>
<script>

    var TableList = $('#TableList').dataTable({
        "ajax": {
            "url": url_gb + "/admin/Analysis/Lists",
            "data": function ( d ) {
                //d.myKey = "myValue";
                // d.custom = $('#myInput').val();
                // etc
            }
        },
        "columns": [
            {"data" : "po_no", "name" : "import_to_chaina.po_no"},
            {"data" : "customer_general_code", "name" : "users.customer_general_code"},
            {"data" : "import_date", "searchable" : false},
            {"data" : "date_container", "searchable" : false},
            {"data" : "type_product", "searchable" : false},
            {"data" : "code_container", "searchable" : false},
            {"data" : "import_to_chaina_qty", "searchable" : false},
            {"data" : "import_to_chaina_pcs", "searchable" : false},
            {"data" : "import_to_chaina_weight_all", "searchable" : false},
            {"data" : "import_to_chaina_cm3", "searchable" : false},
            {"data" : "import_to_chaina_price", "searchable" : false},
            {"data" : "status_id", "searchable" : false},
            {"data" : "transport_type", "searchable" : false},
            {"data" : "null", "searchable" : false, "orderable" : false},
            {"data" : "null", "searchable" : false, "orderable" : false},
            {"data" : "null", "searchable" : false, "orderable" : false},
            {"data" : "null", "searchable" : false, "orderable" : false},
            {"data" : "null", "searchable" : false, "orderable" : false},
        ]
    });

    $('body').on('change', '#check_active_all', function(data){
    });

    $('#shipping_user_id').select2();


</script>
@endsection
