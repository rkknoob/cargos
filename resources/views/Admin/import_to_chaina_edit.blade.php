@extends('Admin.layouts.layout')
@section('css_bottom')
@endsection
@section('body')
<div class="content">
    <div class="col-md-12">
        <div class="container-fluid">
            <form id="FormEdit">
                <div class="row">
                    <!-- ข้อมูล PO -->
                    <div class="col-md-6">
                        <div class="card">
                            <div class="content" style="height: 477px;">
                                <h4 class="title">
                                    <p class="modal-title" id="myModalLabel">@lang('lang.create_data') {{$title_page or 'ข้อมูลใหม่'}}</p>
                                </h4>
                                <div class="material-datatables">

                                    <div class="modal-header">
                                        <h5 class="modal-title" id="myModalLabel">@lang('lang.create_data')</h5>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="add_po_number">@lang('lang.number_po')</label>
                                                    <input type="hidden" id="add_id" value="{{ $ImportToChaina->id }}">
                                                    <input type="text" class="form-control" name="import_to_chaina[po_number]" id="add_po_number" value="{{ $ImportToChaina->po_no }}" readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label for="add_import_to_chaina_date">@lang('lang.date_import')</label>
                                                <div class="input-group" data-date="{{date('Y-m-d')}}">
                                                    <input type="text" value="" readonly="readonly" class="form-control" name="import_to_chaina[import_to_chaina_date]" id="add_import_to_chaina_date"  placeholder="import_to_chaina_date">
                                                    <span class="input-group-addon remove_date_time"><i class="glyphicon glyphicon-remove icon-remove"></i></span>
                                                    <span class="input-group-addon trigger_date_time" for="add_import_to_chaina_date"><i class="glyphicon glyphicon-calendar icon-calendar"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="add_customer_id">@lang('lang.customer_id')</label>
                                                    <select name="import_to_chaina[customer_id]" class="select2 form-control" tabindex="-1" data-placeholder="@lang('lang.customer_id')" id="add_customer_id">
                                                        <option value="">@lang('lang.customer_id')</option>
                                                        @foreach($Users as $User)
                                                            @php $select_option = ''; @endphp
                                                            @if($User->id == $ImportToChaina->user_id)
                                                                @php $select_option = 'selected'; @endphp
                                                            @endif
                                                            <option value="{{$User->id}}" {{$select_option}}>{{$User->customer_general_code}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- กรอกข้อมูลสินค้า  -->
                    <div class="col-md-6">
                        <div class="card">
                            <div class="content">
                                <div class="material-datatables">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="myModalLabel">@lang('lang.product_overview')</h5>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="add_transportation_type_id">@lang('lang.travel_by')</label>
                                                    <select class="select2 form-control" tabindex="-1" id="add_transportation_type_id" >
                                                        @foreach($TransportationTypes as $TransportationType)
                                                            <option value="{{$TransportationType->id}}" data-name="{{ $TransportationType->{'name_'.$lang} }}">{{ $TransportationType->{'name_'.$lang} }} </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="add_customer_id">@lang('lang.producttype')</label>  <label style="color: red;"> (@lang('lang.assign'))</label>
                                                    <select id="add_product_type_id" class="form-control">
                                                    <option value="">@lang('lang.producttype')</option>
                                                    @foreach($ProductTypes as $ProductType)
                                                        <option value="{{$ProductType->id}}" data-name="{{ $ProductType->code }}" data-product_type="{{ $ProductType->code }}">
                                                            {{ $ProductType->name .' ('. $ProductType->code .')'}}
                                                        </option>
                                                    @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="add_product_name">@lang('lang.product_name')</label> <label style="color: red;"> (@lang('lang.assign'))</label>
                                                    <select id="add_product_name" class="form-control">
                                                    <option value="">@lang('lang.product_name')</option>
                                                    @foreach($Products as $Product)
                                                        <option value="{{$Product->name}}" data-code="{{ $Product->code }}" data-product_type="{{ $Product->product_type_id }}">
                                                            {{ $Product->name }}
                                                        </option>
                                                    @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="add_chinese_shop">@lang('lang.name_store_in_china')</label>
                                                    <input type="text" class="form-control" id="add_chinese_shop" placeholder="@lang('lang.name_store_in_china')">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="add_qty">@lang('lang.quantity')</label> <label style="color: red;"> (@lang('lang.assign'))</label>
                                                    <input type="text" class="form-control number-only" id="add_qty" min="1" placeholder="@lang('lang.quantity')">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="add_pcs">@lang("lang.number_of_pieces")</label>
                                                    <input type="text" class="form-control number-only" id="add_pcs" min="1" placeholder="จำนวน">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <!-- <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="add_rel_user_type_product_id">ราคา</label>
                                                    <select class="select2 form-control" tabindex="-1" id="add_rel_user_type_product_id" >
                                                        <option value="">เลือกราคา</option>
                                                    </select>
                                                </div>
                                            </div> -->
                                            <!-- <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="add_product_code">@lang('lang.product_id')</label>
                                                    <input type="text" class="form-control" id="add_product_code" placeholder="@lang('lang.product_id')">
                                                </div>
                                            </div> -->
                                            <div class="col-md-12">
                                                <label for="add_qty">@lang('lang.status_weigh')</label>
                                                <div class="form-group">
                                                    <input type="radio" class="" name="weigh_type" checked="checked" value="T"> @lang('lang.weigh_and_scale_later')
                                                    <input type="radio" class="" name="weigh_type" value="F"> @lang('lang.weigh_and_scale_now')
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="add_qty">@lang('lang.type_weigh')</label>
                                                <div class="form-group">
                                                    <input type="radio" class="status_weigh_format" id="weigh_format_t" name="weigh_format" checked="checked" value="S"> @lang('lang.total_weigh')
                                                    <input type="radio" class="status_weigh_format" id="weigh_format_f" name="weigh_format" value="E"> @lang('lang.split_weigh')
                                                </div>
                                            </div>

                                            <div class="col-md-6" style="text-align: right; margin-top: 15px;">
                                                <a class="btn btn-add-product btn-primary">@lang('lang.submit')</a>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="modal-footer">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- แสดงรายการสินค้า ชั่งรวมตอนนี้ -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="content">
                                <div class="material-datatables">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="myModalLabel">@lang('lang.list_product') <label style="color:red;">* @lang('lang.case_total_weigh_and_scale')</label></h5>
                                    </div>
                                    <div class="modal-body" id="addWeighFormat_T">

                                    </div>
                                    <div class="modal-footer">
                                        <!-- <button type="button" class="btn btn-insert-product-t btn-primary">เพิ่ม</button> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- แสดงรายการสินค้า ชั่งแยกตอนนี้ -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="content">
                                <div class="material-datatables">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="myModalLabel">@lang('lang.list_product') <label style="color:red;">* @lang('lang.case_split_weigh')</label></h5>
                                    </div>
                                    <div class="modal-body">
                                        <table class="table">
                                            <thead id="addWeighFormat_F_Head">
                                                <tr>
                                                    <th>@lang('lang.no')</th>
                                                    <th></th>
                                                    <!-- <th>@lang('lang.code')</th> -->
                                                    <th>@lang('lang.product')</th>
                                                    <th colspan="9" style="text-align: center;">@lang('lang.scale')</th>
                                                    <th style="text-align: center;">@lang('lang.weigh') (KG)</th>
                                                    <th style="text-align: center;">@lang('lang.detail')@lang("lang.detail_container")</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody  id="addWeighFormat_F_main">

                                            </tbody>
                                            <tbody  id="addWeighFormat_F">
                                                <tr id="data-not-found">
                                                    <td colspan="14" style="text-align: center;">@lang('lang.data_not_found')</td>
                                                </tr>
                                            </tbody>
                                        </table>

                                    </div>
                                    <div class="modal-footer">
                                        <!-- <button type="button" class="btn btn-insert-product-f btn-primary">เพิ่ม</button> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- แสดงราย การสินค้า ตาม lot ที่ชั่ง -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="content">
                                <div class="material-datatables">
                                    <!-- <form id="FormEdit"> -->
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="myModalLabel">@lang('lang.detail') {{ $ImportToChaina->po_no }}</h5>
                                    </div>
                                    <div class="modal-body">
                                        <table class="table">
                                            <thead class="text-primary">
                                                <tr>
                                                    <th>#</th>
                                                    <th>@lang('lang.producttype')</th>
                                                    <!-- <th>@lang('lang.product_id')</th> -->
                                                    <th>@lang('lang.product_name')</th>
                                                    <th>@lang('lang.scale') (CBM)</th>
                                                    <th>@lang('lang.quantity')</th>
                                                    <th>@lang('lang.weigh')</th>
                                                    <th>@lang('lang.travel_by')</th>
                                                    <th>@lang('lang.detail')</th>
                                                    <th>@lang('lang.delete')</th>
                                                </tr>
                                            </thead>
                                            <tbody id="list-products">
                                                @php
                                                    $index = 0;
                                                    $lot_weight_all = 0;
                                                @endphp
                                                @if(!empty($ProductImportToChainas) && count($ProductImportToChainas) > 0)
                                                    @foreach($ProductImportToChainas as $key1 => $ProductImportToChaina)
                                                        <tr class="list-product-sub">
                                                            <td>#</td>
                                                            <td>{{ $ProductImportToChaina->product_type_code .' ('. $ProductImportToChaina->product_type_name .')'}}</td>
                                                            <!-- <td>{{ $ProductImportToChaina->product_code }}</td> -->
                                                            <td>{{ $ProductImportToChaina->product_name }}</td>
                                                            <td>{{ number_format( $ProductImportToChaina->lot_product_cubic * (($ProductImportToChaina->product_sort_end-$ProductImportToChaina->product_sort_start)+1) , 2) }}

                                                                <input type="hidden" name="product[{{ $ProductImportToChaina->id }}][product_type_id]" value="{{ $ProductImportToChaina->product_type_id }}">
                                                                <input type="hidden" name="product[{{ $ProductImportToChaina->id }}][product_name]" value="{{ $ProductImportToChaina->product_name }}">
                                                                <input type="hidden" name="product[{{ $ProductImportToChaina->id }}][product_code]" value="{{ $ProductImportToChaina->product_code }}">
                                                                <input type="hidden" name="product[{{ $ProductImportToChaina->id }}][qty]" value="{{ $ProductImportToChaina->qty }}">
                                                                <input type="hidden" name="product[{{ $ProductImportToChaina->id }}][pcs]" value="{{ $ProductImportToChaina->pcs }}">
                                                                <input type="hidden" name="product[{{ $ProductImportToChaina->id }}][chinese_shop]" value="{{ $ProductImportToChaina->shop_chaina_name }}">
                                                                <!-- <input type="hidden" name="product[{{ $ProductImportToChaina->id }}][weigh_type]" value="{{ $ProductImportToChaina->weigh_type }}"> -->
                                                                <input type="hidden" name="product[{{ $ProductImportToChaina->id }}][weigh_format]" value="{{ $ProductImportToChaina->weigh_format }}">
                                                                <input type="hidden" name="product[{{ $ProductImportToChaina->id }}][transportation_type_id]" value="{{ $ProductImportToChaina->transport_type_id }}">
                                                                <input type="hidden" name="product[{{ $ProductImportToChaina->id }}][container_type_id]" value="{{ $ProductImportToChaina->container_type_id }}">
                                                                <?php /*
                                                                <input type="hidden" name="product[{{ $ProductImportToChaina->id }}][rel_user_type_product_id]" value="{{ $ProductImportToChaina->rel_user_type_product_id }}">
                                                                */ ?>
                                                                <input type="hidden" name="product[{{ $ProductImportToChaina->id }}][lot_product][{{ $lot_weight_all }}][product_no_min]" value="{{ $ProductImportToChaina->product_sort_start }}">
                                                                <input type="hidden" name="product[{{ $ProductImportToChaina->id }}][lot_product][{{ $lot_weight_all }}][product_no_max]" value="{{ $ProductImportToChaina->product_sort_end }}">
                                                                <input type="hidden" name="product[{{ $ProductImportToChaina->id }}][lot_product][{{ $lot_weight_all }}][product_no_max_old]" value="{{ $ProductImportToChaina->product_sort_end }}">

                                                                <input type="hidden" name="product[{{ $ProductImportToChaina->id }}][lot_product][{{ $lot_weight_all }}][decription]" value="{{ $ProductImportToChaina->lot_product_remark }}">
                                                                <input type="hidden" name="product[{{ $ProductImportToChaina->id }}][lot_product][{{ $lot_weight_all }}][size_wide]" value="{{ $ProductImportToChaina->lot_product_width }}">
                                                                <input type="hidden" name="product[{{ $ProductImportToChaina->id }}][lot_product][{{ $lot_weight_all }}][size_high]" value="{{ $ProductImportToChaina->lot_product_height }}">
                                                                <input type="hidden" name="product[{{ $ProductImportToChaina->id }}][lot_product][{{ $lot_weight_all }}][size_long]" value="{{ $ProductImportToChaina->lot_product_length }}">
                                                                <input type="hidden" name="product[{{ $ProductImportToChaina->id }}][lot_product][{{ $lot_weight_all }}][size_cubic]" value="{{ $ProductImportToChaina->lot_product_cubic }}">
                                                                <input type="hidden" name="product[{{ $ProductImportToChaina->id }}][lot_product][{{ $lot_weight_all }}][show_size_cubic]" value="{{ $ProductImportToChaina->lot_product_cubic * (($ProductImportToChaina->product_sort_start-$ProductImportToChaina->product_sort_end)+1) }}">
                                                                <input type="hidden" name="product[{{ $ProductImportToChaina->id }}][lot_product][{{ $lot_weight_all }}][size_weight_tote]" value="{{ $ProductImportToChaina->lot_product_weight_all }}">
                                                                <input type="hidden" name="product[{{ $ProductImportToChaina->id }}][lot_product][{{ $lot_weight_all }}][qty]" value="{{ ($ProductImportToChaina->product_sort_end - $ProductImportToChaina->product_sort_start) + 1 }}">
                                                            </td>
                                                            <td>{{ number_format(($ProductImportToChaina->product_sort_end - $ProductImportToChaina->product_sort_start) + 1) }}</td>
                                                            <td>{{ number_format($ProductImportToChaina->lot_product_weight_all, 2) }}</td>
                                                            <td>{{ $ProductImportToChaina->{'transport_types_name_'.$lang} }}</td>
                                                            <td>{{ $ProductImportToChaina->lot_product_remark }}</td>
                                                            <td>
                                                                <a class="btn btn-sm btn-danger btn-condensed btn-delete btn-tooltip" data-id="1" data-rel="tooltip" title="" data-original-title="Delete">
                                                                    -
                                                                </a>
                                                            </td>
                                                        </tr>
                                                        @php
                                                            $lot_weight_all++;
                                                        @endphp
                                                    @endforeach
                                                    @php
                                                        $index = $ProductImportToChaina->id + 1;
                                                    @endphp
                                                @else
                                                    <tr id="product-not-found">
                                                        <td colspan=10 style="text-align: center;">@lang('lang.data_not_found')</td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="add_remark">@lang("lang.note") :</label>
                                            <input class="form-control" name="remark" id="edit_remark" value="{{$ImportToChaina->remark}}" style="border-color: #4CAF50;"/>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-primary">@lang('lang.save')</button>
                                        <a href="{{url('admin/'.$lang.'/ImportToChaina')}}" class="btn btn-danger">ยกเลิก</a>
                                    </div>
                                    <!-- </form> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('js_bottom')
<script src="{{asset('assets/global/plugins/orakuploader/orakuploader.js')}}"></script>
<script>

    //var num_row = 0;
    //var number = 0;
    var index = parseInt('{{$index}}');
    var lot_weight_all = parseInt('{{$lot_weight_all}}');
    $("#add_import_to_chaina_date").daterangepicker(
        {
            singleDatePicker: true,
            showDropdowns: true,
            minYear: 1901,
            startDate: '{{date("Y-m-d")}}',
            // maxYear: parseInt(moment().format('YYYY'),10),
            locale: {
                format: 'YYYY-MM-DD'
            }
        }, function(start, end, label) {
            var years = moment().diff(start, 'years');
            // alert("You are " + years + " years old!");
        }
    );

    $('body').on('click', '.btn-add-product', function(){
        var product_type_id             = $('#add_product_type_id').val();
        var product_type_name           = $('#add_product_type_id').find('option:selected').attr('data-name');
        var container_type_id           = $('#add_container_type_id').val();
        var container_type_name         = $('#add_container_type_id').find('option:selected').attr('data-name');
        var product_name                = $('#add_product_name').val();
        var product_code                = $('#add_product_code').val();
        var chinese_shop                = $('#add_chinese_shop').val();
        var qty                         = $('#add_qty').val();
        var pcs                         = $('#add_pcs').val();
        var transportation_type_id      = $('#add_transportation_type_id').val();
        var transportation_type_name    = $('#add_transportation_type_id').find('option:selected').attr('data-name');
        // var rel_user_type_product_id    = $('#add_rel_user_type_product_id').val();
        var html_weigh_format_t         = '';
        var html_weigh_format_f         = '';
        var html_weigh_format_f_main        = '';
        var weigh_type = '';
        var weigh_format = '';
        var html = '';
        var index_row = ($('#list-products').find('.list-product-main').length);
        $('input[name="weigh_type"]:checked').each(function() {
            weigh_type      = this.value;
        });
        $('input[name="weigh_format"]:checked').each(function() {
            weigh_format    = this.value;
        });
        // console.log(weigh_type); && product_code != ''
        // &&  container_type_id != ''
        if(product_type_id != '' &&  transportation_type_id != '' && product_name != '' && qty != ''){ // เช็คข้อมูลที่กรอก
            if(weigh_type == 'F') { // เช็คชั่งก่อน
                if($("#weigh_format_t").is(":checked")) { // เช็ค ชั่งรวม
                    // <input type="hidden" class="form-control insert_rel_user_type_product_id" placeholder="" value="'+rel_user_type_product_id+'" readonly>\
                    html_weigh_format_t += '\
                    <div class="add_product_lists" data-key="'+index+'"><hr>\
                        <div class="row">\
                            <div class="col-md-3" style="text-align: right;">\
                                <div class="form-group">\
                                    <label><b>@lang("lang.weighing")</b></label>\
                                </div>\
                            </div>\
                            <div class="col-md-2">\
                                <div class="form-group">\
                                    <input type="text" class="form-control insert_product_no_min" placeholder="" value="1" readonly>\
                                </div>\
                            </div>\
                            <div class="col-md-2" style="text-align: center;">\
                                <div class="form-group">\
                                    <label><b>ถึง</b></label>\
                                </div>\
                            </div>\
                            <div class="col-md-2">\
                                <div class="form-group">\
                                    <input type="number" class="form-control insert_product_no_max" placeholder="" value="'+qty+'" max="'+qty+'" min="1" formenctype=""/>\
                                    <input type="hidden" class="insert_product_no_max_old" value="'+qty+'">\
                                </div>\
                            </div>\
                        </div>\
                        <div class="row">\
                            <div class="col-md-2">\
                                <div class="form-group">\
                                    <label>@lang("lang.product_category")</label>\
                                    <input type="hidden" class="form-control insert_product_type_id" placeholder="" value="'+product_type_id+'" readonly>\
                                    <input type="text" class="form-control insert_product_type_name" placeholder="" value="'+product_type_name+'" readonly>\
                                    <input type="hidden" class="form-control insert_weigh_type" placeholder="" value="'+weigh_type+'" readonly>\
                                    <input type="hidden" class="form-control insert_weigh_format" placeholder="" value="'+weigh_format+'" readonly>\
                                </div>\
                            </div>\
                            <!--<div class="col-md-2">\
                                <div class="form-group">\
                                    <label>@lang("lang.product_id")</label>\
                                    <input type="text" class="form-control insert_product_code" placeholder="" value="'+product_code+'" readonly>\
                                </div>\
                            </div>-->\
                            <div class="col-md-2">\
                                <div class="form-group">\
                                    <label>@lang("lang.product_name")</label>\
                                    <input type="text" class="form-control insert_product_name" placeholder="" value="'+product_name+'" readonly>\
                                </div>\
                            </div>\
                            <div class="col-md-2">\
                                <div class="form-group">\
                                    <label>@lang("lang.name_store_in_china")</label>\
                                    <input type="text" class="form-control insert_chinese_shop" placeholder="" value="'+chinese_shop+'" readonly>\
                                </div>\
                            </div>\
                            <div class="col-md-2">\
                                <div class="form-group">\
                                    <label>@lang("lang.travel_by")</label>\
                                    <input type="hidden" class="form-control insert_transportation_type_id" placeholder="" value="'+transportation_type_id+'" readonly>\
                                    <input type="text" class="form-control insert_transportation_type_name" placeholder="" value="'+transportation_type_name+'" readonly>\
                                </div>\
                            </div>\
                            <div class="col-md-2">\
                                <div class="form-group">\
                                    <label>@lang("lang.quantity")</label>\
                                    <input type="text" class="form-control insert_qty" placeholder="" value="'+qty+'" readonly>\
                                    <input type="hidden" class="form-control insert_pcs" placeholder="" value="'+pcs+'" readonly>\
                                </div>\
                            </div>\
                        </div>\
                        <div class="row">\
                            <div class="col-md-2">\
                                <div class="form-group">\
                                    <label>@lang("lang.tatal_weigth")</label>\
                                    <input type="text" class="form-control number-only insert_size_weight_tote" placeholder="0">\
                                </div>\
                            </div>\
                            <div class="col-md-10">\
                                <div class="form-group">\
                                    <label>@lang("lang.detail_container")</label>\
                                    <input type="text" class="form-control insert_decription" placeholder="@lang("lang.detail_container")">\
                                </div>\
                            </div>\
                        </div>\
                        <div class="row">\
                            <div class="col-md-2">\
                                <div class="form-group">\
                                    <label for="add_po_number">@lang("lang.scale")</label>\
                                </div>\
                            </div>\
                            <div class="col-md-1" style="text-align: center;">\
                                <div class="form-group">\
                                    <label for="add_po_number">@lang("lang.wide")</label>\
                                </div>\
                            </div>\
                            <div class="col-md-1">\
                                <div class="form-group">\
                                    <input type="text" class="form-control number-only insert_size_wide cal_size_wide" placeholder="0">\
                                </div>\
                            </div>\
                            <div class="col-md-1" style="text-align: center;">\
                                <div class="form-group">\
                                    <label for="add_po_number">x @lang("lang.long")</label>\
                                </div>\
                            </div>\
                            <div class="col-md-1">\
                                <div class="form-group">\
                                    <input type="text" class="form-control number-only insert_size_long cal_size_long" placeholder="0">\
                                </div>\
                            </div>\
                            <div class="col-md-1" style="text-align: center;">\
                                <div class="form-group">\
                                    <label for="add_po_number">x @lang("lang.high")</label>\
                                </div>\
                            </div>\
                            <div class="col-md-1">\
                                <div class="form-group">\
                                    <input type="text" class="form-control number-only insert_size_high cal_size_high" placeholder="0" >\
                                </div>\
                            </div>\
                            <div class="col-md-1" style="text-align: center;">\
                                <div class="form-group">\
                                    <label for="add_po_number">=</label>\
                                </div>\
                            </div>\
                            <div class="col-md-1">\
                                <div class="form-group">\
                                    <input type="hidden" class="form-control number-only insert_size_cubic cal_size_cubic" placeholder="0" readonly>\
                                    <input type="text" class="form-control number-only show_cal_size_cubic" placeholder="0" readonly>\
                                </div>\
                            </div>\
                            <div class="col-md-1" style="text-align: center;">\
                                <div class="form-group">\
                                    <label for="add_po_number">@lang("lang.queue")</label>\
                                </div>\
                            </div>\
                            <div class="col-md-1" style="text-align: center;">\
                                <a class="btn btn-insert-product-t btn-primary">@lang("lang.submit")</a>\
                            </div>\
                        </div>\
                    </div>';
                    index++;
                }
                else{ // เช็ค ชั่งแยก
                    // html_weigh_format_f    += '\
                    //     <tr class="product_weigh_format_f_main" data-key="'+index+'">\
                    //         <td>\
                    //             <input type="hidden" class="form-control insert_product_type_id" placeholder="" value="'+product_type_id+'" readonly>\
                    //             <input type="hidden" class="form-control insert_product_type_name" placeholder="" value="'+product_type_name+'" readonly>\
                    //             <input type="hidden" class="form-control insert_product_code" placeholder="" value="'+product_code+'" readonly>\
                    //             <input type="hidden" class="form-control insert_product_name" placeholder="" value="'+product_name+'" readonly>\
                    //             <input type="hidden" class="form-control insert_chinese_shop" placeholder="" value="'+chinese_shop+'" readonly>\
                    //             <input type="hidden" class="form-control insert_qty_product" placeholder="" value="'+qty+'" readonly>\
                    //             <input type="hidden" class="form-control insert_weigh_type" placeholder="" value="'+weigh_type+'" readonly>\
                    //             <input type="hidden" class="form-control insert_weigh_format" placeholder="" value="'+weigh_format+'" readonly>\
                    //             <input type="hidden" class="form-control insert_container_type_id" placeholder="" value="'+container_type_id+'" readonly>\
                    //             <input type="hidden" class="form-control insert_container_type_name" placeholder="" value="'+container_type_name+'" readonly>\
                    //             <input type="hidden" class="form-control insert_transportation_type_id" placeholder="" value="'+transportation_type_id+'" readonly>\
                    //             <input type="hidden" class="form-control insert_transportation_type_name" placeholder="" value="'+transportation_type_name+'" readonly>\
                    //         </td>\
                    //     </tr>';
                    // <input type="hidden" class="form-control insert_rel_user_type_product_id" value="'+rel_user_type_product_id+'" readonly>\
                    for(i=0; i<qty; i++){
                        html_weigh_format_f += '\
                            <tr class="product_weigh_format_f_'+index+'" data-key="'+index+'">\
                                <td style="width: 5%;">#</td>\
                                <td><a class="btn btn-primary btn-copy">Copy</a></td>\
                                <td>\
                                    <input type="hidden" class="form-control insert_product_no_min" value="'+(i+1)+'">\
                                    <input type="hidden" class="form-control insert_product_no_max" value="'+(i+1)+'"/>\
                                    <input type="hidden" class="form-control insert_product_no_max_old" value="'+(i+1)+'">\
                                    <input type="hidden" class="form-control insert_product_type_id" value="'+product_type_id+'" readonly>\
                                    <input type="hidden" class="form-control insert_product_type_name" value="'+product_type_name+'" readonly>\
                                    <input type="hidden" class="form-control insert_product_code" value="'+product_code+'" readonly>\
                                    <input type="hidden" class="form-control insert_chinese_shop" value="'+chinese_shop+'" readonly>\
                                    <input type="hidden" class="form-control insert_qty_product" value="'+qty+'" readonly>\
                                    <input type="hidden" class="form-control insert_qty" value="1" readonly>\
                                    <input type="hidden" class="form-control insert_pcs" value="'+pcs+'" readonly>\
                                    <input type="hidden" class="form-control insert_weigh_type" value="'+weigh_type+'" readonly>\
                                    <input type="hidden" class="form-control insert_weigh_format" value="'+weigh_format+'" readonly>\
                                    <input type="hidden" class="form-control insert_container_type_id" value="'+container_type_id+'" readonly>\
                                    <input type="hidden" class="form-control insert_container_type_name" value="'+container_type_name+'" readonly>\
                                    <input type="hidden" class="form-control insert_transportation_type_id" value="'+transportation_type_id+'" readonly>\
                                    <input type="hidden" class="form-control insert_transportation_type_name" value="'+transportation_type_name+'" readonly>\
                                    <input type="hidden" class="form-control insert_product_name" placeholder="" value="'+product_name+'" readonly>'+product_name+'\
                                </td>\
                                <td style="width: 5%; text-align: center;">@lang("lang.wide")</td>\
                                <td style="width: 10%;">\
                                    <input type="text" class="form-control number-only insert_size_wide cal_size_wide" placeholder="0" required>\
                                </td>\
                                <td style="width: 5%; text-align: center;"> x @lang("lang.long")</td>\
                                <td style="width: 10%;">\
                                    <input type="text" class="form-control number-only insert_size_long cal_size_long" placeholder="0" required>\
                                </td>\
                                <td style="width: 5%; text-align: center;"> x @lang("lang.high")</td>\
                                <td style="width: 10%;">\
                                    <input type="text" class="form-control number-only insert_size_high cal_size_high" placeholder="0" required>\
                                </td>\
                                <td style="width: 5%; text-align: center;"> = </td>\
                                <td style="width: 10%;">\
                                    <input type="hidden" class="form-control number-only insert_size_cubic cal_size_cubic" placeholder="0" readonly required>\
                                    <input type="text" class="form-control number-only show_cal_size_cubic" placeholder="0" readonly required>\
                                </td>\
                                <td style="width: 5%; text-align: center;">@lang("lang.queue")</td>\
                                <td style="width: 10%;">\
                                    <input type="text" class="form-control number-only insert_size_weight_tote" placeholder="0" required>\
                                </td>\
                                <td style="width: 20%;">\
                                    <input type="text" class="form-control insert_decription" placeholder="@lang("lang.detail_container")">\
                                </td>\
                                <td>\
                                    <a class="btn btn-insert-product-f btn-primary">@lang("lang.submit")</a>\
                                    <a class="btn btn-sm btn-danger btn-condensed btn-delete btn-tooltip" data-id="1" data-rel="tooltip" title="" data-original-title="Delete">\
                                        -\
                                    </a>\
                                </td>\
                            </tr>';
                    }
                    index++;
                }
                $('#data-not-found').hide();
            }
            else{ // เช็คชั่งทีหลัง
                if($("#weigh_format_t").is(":checked")) { // เช็คชั่งรวม
                    // html += '<tr class="list-product-main">\
                    //             <td conspan="8" style="border: 0;">\
                    //                 <input type="hidden" name="product['+index_row+'][product_type_id]" value="'+product_type_id+'">\
                    //                 <input type="hidden" name="product['+index_row+'][product_name]" value="'+product_name+'">\
                    //                 <input type="hidden" name="product['+index_row+'][product_code]" value="'+product_code+'">\
                    //                 <input type="hidden" name="product['+index_row+'][qty]" value="'+qty+'">\
                    //                 <input type="hidden" name="product['+index_row+'][chinese_shop]" value="'+chinese_shop+'">\
                    //                 <input type="hidden" name="product['+index_row+'][weigh_type]" value="'+weigh_type+'">\
                    //                 <input type="hidden" name="product['+index_row+'][weigh_format]" value="'+weigh_format+'">\
                    //                 <input type="hidden" name="product['+index_row+'][transportation_type_id]" value="'+transportation_type_id+'">\
                    //                 <input type="hidden" name="product['+index_row+'][container_type_id]" value="'+container_type_id+'">\
                    //             </td>\
                    //         </tr>';
                    // <input type="hidden" name="product['+index+'][rel_user_type_product_id]" value="'+rel_user_type_product_id+'">\
                    html += '<tr class="list-product-sub">\
                                <td>#</td>\
                                <td>'+product_type_name+'</td>\
                                <!--<td>'+product_code+'</td>-->\
                                <td>'+product_name+'</td>\
                                <td>\
                                    <input type="hidden" name="product['+index+'][product_type_id]" value="'+product_type_id+'">\
                                    <input type="hidden" name="product['+index+'][product_name]" value="'+product_name+'">\
                                    <input type="hidden" name="product['+index+'][product_code]" value="'+product_code+'">\
                                    <input type="hidden" name="product['+index+'][qty]" value="'+qty+'">\
                                    <input type="hidden" name="product['+index+'][pcs]" value="'+pcs+'">\
                                    <input type="hidden" name="product['+index+'][chinese_shop]" value="'+chinese_shop+'">\
                                    <input type="hidden" name="product['+index+'][weigh_type]" value="'+weigh_type+'">\
                                    <input type="hidden" name="product['+index+'][weigh_format]" value="'+weigh_format+'">\
                                    <input type="hidden" name="product['+index+'][transportation_type_id]" value="'+transportation_type_id+'">\
                                    <input type="hidden" name="product['+index+'][container_type_id]" value="'+container_type_id+'">\
                                    \
                                    <input type="hidden" name="product['+index+'][lot_product][0][product_no_min]" value="1">\
                                    <input type="hidden" name="product['+index+'][lot_product][0][product_no_max]" value="'+qty+'">\
                                    <input type="hidden" name="product['+index+'][lot_product][0][product_no_max_old]" value="'+qty+'">\
                                    <input type="hidden" name="product['+index+'][lot_product][0][decription]" value="">\
                                    <input type="hidden" name="product['+index+'][lot_product][0][size_wide]" value="">\
                                    <input type="hidden" name="product['+index+'][lot_product][0][size_high]" value="">\
                                    <input type="hidden" name="product['+index+'][lot_product][0][size_long]" value="">\
                                    <input type="hidden" name="product['+index+'][lot_product][0][size_cubic]" value="">\
                                    <input type="hidden" name="product['+index+'][lot_product][0][show_size_cubic]" value="">\
                                    <input type="hidden" name="product['+index+'][lot_product][0][size_weight_tote]" value="">\
                                    <input type="hidden" name="product['+index+'][lot_product][0][qty]" value="'+qty+'">\
                                </td>\
                                <td>'+addNumformat(qty)+'</td>\
                                <td></td>\
                                <td>'+transportation_type_name+'</td>\
                                <td></td>\
                                <td>\
                                    <a class="btn btn-sm btn-danger btn-condensed btn-delete btn-tooltip" data-id="1" data-rel="tooltip" title="" data-original-title="Delete">\
                                        -\
                                    </a>\
                                </td>\
                            </tr>';
                    index++;
                }
                else{ // เช็ค ชั่งแยก
                    // html += '<tr class="list-product-main">\
                    //             <td conspan="8" style="border: 0;">\
                    //                 <input type="hidden" name="product['+index_row+'][product_type_id]" value="'+product_type_id+'">\
                    //                 <input type="hidden" name="product['+index_row+'][product_name]" value="'+product_name+'">\
                    //                 <input type="hidden" name="product['+index_row+'][product_code]" value="'+product_code+'">\
                    //                 <input type="hidden" name="product['+index_row+'][qty]" value="'+qty+'">\
                    //                 <input type="hidden" name="product['+index_row+'][chinese_shop]" value="'+chinese_shop+'">\
                    //                 <input type="hidden" name="product['+index_row+'][weigh_type]" value="'+weigh_type+'">\
                    //                 <input type="hidden" name="product['+index_row+'][weigh_format]" value="'+weigh_format+'">\
                    //                 <input type="hidden" name="product['+index_row+'][transportation_type_id]" value="'+transportation_type_id+'">\
                    //                 <input type="hidden" name="product['+index_row+'][container_type_id]" value="'+container_type_id+'">\
                    //             </td>\
                    //         </tr>';
                    // <input type="hidden" name="product['+index+'][rel_user_type_product_id]" value="'+rel_user_type_product_id+'">\

                    for(i=0; i<qty; i++){
                        html += '<tr class="list-product-sub">\
                                    <td>#</td>\
                                    <td>'+product_type_name+'</td>\
                                    <!--<td>'+product_code+'</td>-->\
                                    <td>'+product_name+'</td>\
                                    <td>\
                                        <input type="hidden" name="product['+index+'][product_type_id]" value="'+product_type_id+'">\
                                        <input type="hidden" name="product['+index+'][product_name]" value="'+product_name+'">\
                                        <input type="hidden" name="product['+index+'][product_code]" value="'+product_code+'">\
                                        <input type="hidden" name="product['+index+'][qty]" value="'+qty+'">\
                                        <input type="hidden" name="product['+index+'][pcs]" value="'+pcs+'">\
                                        <input type="hidden" name="product['+index+'][chinese_shop]" value="'+chinese_shop+'">\
                                        <input type="hidden" name="product['+index+'][weigh_type]" value="'+weigh_type+'">\
                                        <input type="hidden" name="product['+index+'][weigh_format]" value="'+weigh_format+'">\
                                        <input type="hidden" name="product['+index+'][transportation_type_id]" value="'+transportation_type_id+'">\
                                        <input type="hidden" name="product['+index+'][container_type_id]" value="'+container_type_id+'">\
                                        \
                                        <input type="hidden" name="product['+index+'][lot_product]['+i+'][product_no_min]" value="'+(i+1)+'">\
                                        <input type="hidden" name="product['+index+'][lot_product]['+i+'][product_no_max]" value="'+(i+1)+'">\
                                        <input type="hidden" name="product['+index+'][lot_product]['+i+'][product_no_max_old]" value="'+(i+1)+'">\
                                        <input type="hidden" name="product['+index+'][lot_product]['+i+'][decription]" value="">\
                                        <input type="hidden" name="product['+index+'][lot_product]['+i+'][size_wide]" value="">\
                                        <input type="hidden" name="product['+index+'][lot_product]['+i+'][size_high]" value="">\
                                        <input type="hidden" name="product['+index+'][lot_product]['+i+'][size_long]" value="">\
                                        <input type="hidden" name="product['+index+'][lot_product]['+i+'][size_cubic]" value="">\
                                        <input type="hidden" name="product['+index+'][lot_product]['+i+'][show_size_cubic]" value="">\
                                        <input type="hidden" name="product['+index+'][lot_product]['+i+'][size_weight_tote]" value="">\
                                        <input type="hidden" name="product['+index+'][lot_product]['+i+'][qty]" value="1">\
                                    </td>\
                                    <td>1</td>\
                                    <td></td>\
                                    <td>'+transportation_type_name+'</td>\
                                    <td></td>\
                                    <td>\
                                        <a class="btn btn-sm btn-danger btn-condensed btn-delete btn-tooltip" data-id="1" data-rel="tooltip" title="" data-original-title="Delete">\
                                            -\
                                        </a>\
                                    </td>\
                                </tr>';
                    }
                    index++;
                }
                $('#product-not-found').hide();
                $('#list-products').append(html);
            }
            // set data default
            // $('#add_product_type_id').val('').trigger('change');
            $('#add_container_type_id').val('').trigger('change');
            $('#add_product_name').val('').trigger('change');
            $('#add_product_code').val('');
            $('#add_chinese_shop').val('');
            $('#add_qty').val('');
            $('#add_pcs').val('');
            $('#add_transportation_type_id').select2('');
            // $('#add_product_type_id').select2();
            // $('#add_container_type_id').select2();

            $( "#addWeighFormat_T" ).append(html_weigh_format_t);
            $( "#addWeighFormat_F_main").append(html_weigh_format_f_main);
            $( "#addWeighFormat_F").append(html_weigh_format_f);
            // $('#add_rel_user_type_product_id').html('<option value="">เลือกราคา</option>');
            // $('#add_rel_user_type_product_id').select2();
        }else{
            swal("ภาพรวมสินค้า", "กรุณากรอกข้อมูลให้ครบถ้วน", "warning");
            $(".swal2-content").css("color", "red");
        }
    });

    $('body').on('click', '.btn-insert-product-t', function(){
        lot_weight_all++;
        var ele = $(this).closest('.add_product_lists');
        var key = $(this).closest('.add_product_lists').data('key');
        // $('.add_product_lists').each(function(){
        var product_type_id             = $(ele).find('.insert_product_type_id').val();
        var product_type_name           = $(ele).find('.insert_product_type_name').val();

        var product_no_min              = $(ele).find('.insert_product_no_min').val();
        var product_no_max              = $(ele).find('.insert_product_no_max').val();
        var product_no_max_old          = $(ele).find('.insert_product_no_max_old').val();

        var container_type_id           = $(ele).find('.insert_container_type_id').val();
        var container_type_name         = $(ele).find('.insert_container_type_name').val();
        var product_name                = $(ele).find('.insert_product_name').val();
        var product_code                = $(ele).find('.insert_product_code').val();
        var chinese_shop                = $(ele).find('.insert_chinese_shop').val();
        var transportation_type_id      = $(ele).find('.insert_transportation_type_id').val();
        var transportation_type_name    = $(ele).find('.insert_transportation_type_name').val();
        // var rel_user_type_product_id    = $(ele).find('.insert_rel_user_type_product_id').val();
        var qty                         = $(ele).find('.insert_qty').val();
        var pcs                         = $(ele).find('.insert_pcs').val();
        var weigh_type                  = $(ele).find('.insert_weigh_type').val();
        var weigh_format                = $(ele).find('.insert_weigh_format').val();
        var size_weight_tote            = $(ele).find('.insert_size_weight_tote').val();
        var decription                  = $(ele).find('.insert_decription').val();
        var size_wide                   = $(ele).find('.insert_size_wide').val();
        var size_long                   = $(ele).find('.insert_size_long').val();
        var size_high                   = $(ele).find('.insert_size_high').val();
        var size_cubic                  = $(ele).find('.insert_size_cubic').val();
        var show_size_cubic             = $(ele).find('.show_cal_size_cubic').val();
        var html                        = '';
        var index_row = ($('#list-products').find('.list-product-main').length);
        // html += '<tr class="list-product-main">\
        //             <td conspan="8" style="border: 0;">\
        //                 <input type="hidden" name="product['+key+'][product_type_id]" value="'+product_type_id+'">\
        //                 <input type="hidden" name="product['+key+'][product_name]" value="'+product_name+'">\
        //                 <input type="hidden" name="product['+key+'][product_code]" value="'+product_code+'">\
        //                 <input type="hidden" name="product['+key+'][qty]" value="'+qty+'">\
        //                 <input type="hidden" name="product['+key+'][chinese_shop]" value="'+chinese_shop+'">\
        //                 <input type="hidden" name="product['+key+'][weigh_type]" value="'+weigh_type+'">\
        //                 <input type="hidden" name="product['+key+'][weigh_format]" value="'+weigh_format+'">\
        //                 <input type="hidden" name="product['+key+'][transportation_type_id]" value="'+transportation_type_id+'">\
        //                 <input type="hidden" name="product['+key+'][container_type_id]" value="'+container_type_id+'">\
        //             </td>\
        //         </tr>';
        // <input type="hidden" name="product['+key+'][rel_user_type_product_id]" value="'+rel_user_type_product_id+'">\
        html += '<tr class="list-product-sub">\
                    <td>#</td>\
                    <td>'+product_type_name+'</td>\
                    <!--<td>'+product_code+'</td>-->\
                    <td>'+product_name+'</td>\
                    <td>'+addNumformat(Math.round(show_size_cubic * 1000) / 1000)+'\
                    <input type="hidden" name="product['+key+'][product_type_id]" value="'+product_type_id+'">\
                        \
                        <input type="hidden" name="product['+key+'][product_name]" value="'+product_name+'">\
                        <input type="hidden" name="product['+key+'][product_code]" value="'+product_code+'">\
                        <input type="hidden" name="product['+key+'][qty]" value="'+qty+'">\
                        <input type="hidden" name="product['+key+'][pcs]" value="'+pcs+'">\
                        <input type="hidden" name="product['+key+'][chinese_shop]" value="'+chinese_shop+'">\
                        <input type="hidden" name="product['+key+'][weigh_type]" value="'+weigh_type+'">\
                        <input type="hidden" name="product['+key+'][weigh_format]" value="'+weigh_format+'">\
                        <input type="hidden" name="product['+key+'][transportation_type_id]" value="'+transportation_type_id+'">\
                        <input type="hidden" name="product['+key+'][container_type_id]" value="'+container_type_id+'">\
                        \
                        <input type="hidden" name="product['+key+'][lot_product]['+lot_weight_all+'][product_no_min]" value="'+product_no_min+'">\
                        <input type="hidden" name="product['+key+'][lot_product]['+lot_weight_all+'][product_no_max]" value="'+product_no_max+'">\
                        <input type="hidden" name="product['+key+'][lot_product]['+lot_weight_all+'][product_no_max_old]" value="'+product_no_max_old+'">\
                        \
                        <input type="hidden" name="product['+key+'][lot_product]['+lot_weight_all+'][decription]" value="'+decription+'">\
                        <input type="hidden" name="product['+key+'][lot_product]['+lot_weight_all+'][size_wide]" value="'+size_wide+'">\
                        <input type="hidden" name="product['+key+'][lot_product]['+lot_weight_all+'][size_high]" value="'+size_high+'">\
                        <input type="hidden" name="product['+key+'][lot_product]['+lot_weight_all+'][size_long]" value="'+size_long+'">\
                        <input type="hidden" name="product['+key+'][lot_product]['+lot_weight_all+'][size_cubic]" value="'+size_cubic+'">\
                        <input type="hidden" name="product['+key+'][lot_product]['+lot_weight_all+'][show_size_cubic]" value="'+show_size_cubic+'">\
                        <input type="hidden" name="product['+key+'][lot_product]['+lot_weight_all+'][size_weight_tote]" value="'+size_weight_tote+'">\
                        <input type="hidden" name="product['+key+'][lot_product]['+lot_weight_all+'][qty]" value="'+((parseInt(product_no_max)-parseInt(product_no_min))+1)+'">\
                    </td>\
                    <td>'+addNumformat((product_no_max-product_no_min)+1)+'</td>\
                    <td>'+addNumformat(Math.round(size_weight_tote * 1000) / 1000)+'</td>\
                    <td>'+transportation_type_name+'</td>\
                    <td>'+(decription ? decription : '')+'</td>\
                    <td>\
                        <a class="btn btn-sm btn-danger btn-condensed btn-delete btn-tooltip" data-id="1" data-rel="tooltip" title="" data-original-title="Delete">\
                            -\
                        </a>\
                    </td>\
                </tr>';
        $('#product-not-found').hide();
        $('#list-products').append(html);
        // console.log(product_no_max +' >= '+ product_no_max_old);
        if(parseInt(product_no_max) >= parseInt(product_no_max_old)){
            $(ele).remove();
            // console.log(1);
        }else{
            $(ele).find('.insert_product_no_min').val(parseInt(product_no_max)+1);
            $(ele).find('.insert_product_no_max').val(parseInt(product_no_max_old));
            $(ele).find('.insert_product_no_max').attr("min", parseInt(product_no_max)+1);
            // console.log(2);
        }
        // $('#addWeighFormat_T').html('');
        // });
    });

    $('body').on('click', '.btn-insert-product-f', function(){
        lot_weight_all++;
        var ele = $(this).closest('tr');
        var key = $(this).closest('tr').data('key');
        var html = '';
        // $('.product_weigh_format_f_main').each(function(i, val){
            // var key = $(this).attr('data-key');
            // var index_row = ($('#list-products').find('.list-product-main').length);
        var product_no_min              = $(ele).find('.insert_product_no_min').val();
        var product_no_max              = $(ele).find('.insert_product_no_max').val();
        var product_no_max_old          = $(ele).find('.insert_product_no_max_old').val();

        var product_type_id             = $(ele).find('.insert_product_type_id').val();
        var product_type_name           = $(ele).find('.insert_product_type_name').val();
        var container_type_id           = $(ele).find('.insert_container_type_id').val();
        var container_type_name         = $(ele).find('.insert_container_type_name').val();
        var product_name                = $(ele).find('.insert_product_name').val();
        var product_code                = $(ele).find('.insert_product_code').val();
        var chinese_shop                = $(ele).find('.insert_chinese_shop').val();
        var transportation_type_id      = $(ele).find('.insert_transportation_type_id').val();
        var transportation_type_name    = $(ele).find('.insert_transportation_type_name').val();
        // var rel_user_type_product_id    = $(ele).find('.insert_rel_user_type_product_id').val();
        var qty                         = $(ele).find('.insert_qty').val();
        var pcs                         = $(ele).find('.insert_pcs').val();
        var weigh_type                  = $(ele).find('.insert_weigh_type').val();
        var weigh_format                = $(ele).find('.insert_weigh_format').val();
        var size_weight_tote            = $(ele).find('.insert_size_weight_tote').val();
        var decription                  = $(ele).find('.insert_decription').val();
        var size_wide                   = $(ele).find('.insert_size_wide').val();
        var size_long                   = $(ele).find('.insert_size_long').val();
        var size_high                   = $(ele).find('.insert_size_high').val();
        var size_cubic                  = $(ele).find('.insert_size_cubic').val();
        var show_size_cubic             = $(ele).find('.show_cal_size_cubic').val();

        // html += '<tr class="list-product-main">\
        //     <td conspan="9" style="border: 0;">\
        //         <input type="hidden" name="product['+index_row+'][product_type_id]" value="'+product_type_id+'">\
        //         <input type="hidden" name="product['+index_row+'][product_name]" value="'+product_name+'">\
        //         <input type="hidden" name="product['+index_row+'][product_code]" value="'+product_code+'">\
        //         <input type="hidden" name="product['+index_row+'][qty]" value="'+qty+'">\
        //         <input type="hidden" name="product['+index_row+'][chinese_shop]" value="'+chinese_shop+'">\
        //         <input type="hidden" name="product['+index_row+'][weigh_type]" value="'+weigh_type+'">\
        //         <input type="hidden" name="product['+index_row+'][weigh_format]" value="'+weigh_format+'">\
        //         <input type="hidden" name="product['+index_row+'][transportation_type_id]" value="'+transportation_type_id+'">\
        //         <input type="hidden" name="product['+index_row+'][container_type_id]" value="'+container_type_id+'">\
        //     </td>\
        // </tr>';
        // $('.product_weigh_format_f_'+key).each(function(k, v){
        // <input type="hidden" name="product['+key+'][rel_user_type_product_id]" value="'+rel_user_type_product_id+'">\

        html += '<tr class="list-product-sub">\
            <td>#</td>\
            <td>'+product_type_name+'</td>\
            <!--<td>'+product_code+'</td>-->\
            <td>'+product_name+'</td>\
            <td>'+addNumformat(Math.round(show_size_cubic * 1000) / 1000)+'\
                \
                <input type="hidden" name="product['+key+'][product_type_id]" value="'+product_type_id+'">\
                <input type="hidden" name="product['+key+'][product_name]" value="'+product_name+'">\
                <input type="hidden" name="product['+key+'][product_code]" value="'+product_code+'">\
                <input type="hidden" name="product['+key+'][qty]" value="'+qty+'">\
                <input type="hidden" name="product['+key+'][pcs]" value="'+pcs+'">\
                <input type="hidden" name="product['+key+'][chinese_shop]" value="'+chinese_shop+'">\
                <input type="hidden" name="product['+key+'][weigh_type]" value="'+weigh_type+'">\
                <input type="hidden" name="product['+key+'][weigh_format]" value="'+weigh_format+'">\
                <input type="hidden" name="product['+key+'][transportation_type_id]" value="'+transportation_type_id+'">\
                <input type="hidden" name="product['+key+'][container_type_id]" value="'+container_type_id+'">\
                \
                <input type="hidden" name="product['+key+'][lot_product]['+lot_weight_all+'][product_no_min]" value="'+product_no_min+'">\
                <input type="hidden" name="product['+key+'][lot_product]['+lot_weight_all+'][product_no_max]" value="'+product_no_max+'">\
                <input type="hidden" name="product['+key+'][lot_product]['+lot_weight_all+'][product_no_max_old]" value="'+product_no_max_old+'">\
                \
                <input type="hidden" name="product['+key+'][lot_product]['+lot_weight_all+'][size_wide]" value="'+size_wide+'">\
                <input type="hidden" name="product['+key+'][lot_product]['+lot_weight_all+'][size_high]" value="'+size_high+'">\
                <input type="hidden" name="product['+key+'][lot_product]['+lot_weight_all+'][size_long]" value="'+size_long+'">\
                <input type="hidden" name="product['+key+'][lot_product]['+lot_weight_all+'][size_cubic]" value="'+size_cubic+'">\
                <input type="hidden" name="product['+key+'][lot_product]['+lot_weight_all+'][show_size_cubic]" value="'+show_size_cubic+'">\
                <input type="hidden" name="product['+key+'][lot_product]['+lot_weight_all+'][size_weight_tote]" value="'+size_weight_tote+'">\
                <input type="hidden" name="product['+key+'][lot_product]['+lot_weight_all+'][decription]" value="'+decription+'">\
                <input type="hidden" name="product['+key+'][lot_product]['+lot_weight_all+'][qty]" value="1">\
            </td>\
            <td>1</td>\
            <td>'+addNumformat(Math.round(size_weight_tote * 1000) / 1000)+'</td>\
            <td>'+transportation_type_name+'</td>\
            <td>'+(decription ? decription : '')+'</td>\
            <td>\
                <a class="btn btn-sm btn-danger btn-condensed btn-delete btn-tooltip" data-id="1" data-rel="tooltip" title="" data-original-title="Delete">\
                    -\
                </a>\
            </td>\
        </tr>';

        // $('#product-not-found').hide();
        $('#list-products').append(html);
        // $('#addWeighFormat_F').html('');
        // $('#addWeighFormat_F_main').html('');
        // $('#data-not-found').show();
            // });
        // });
        // console.log(html);
        $('#product-not-found').hide();
        $(ele).remove();
        // $('#data-not-found').show();
    });

    $('body').on('keyup', '.cal_size_wide, .cal_size_long, .cal_size_high, .cal_size_cubic', function(){
        var ele = $(this).closest('tr');
        var size_wide   = ele.find('.cal_size_wide').val();
        var size_long   = ele.find('.cal_size_long').val();
        var size_high   = ele.find('.cal_size_high').val();
        //var size_cubic  = ele.find('.cal_size_cubic').val();
        var cubic = (size_wide * size_long * size_high);
        ele.find('.cal_size_cubic').val(Math.round(cubic * 1000) / 1000)
        ele.find('.show_cal_size_cubic').val(Math.round(cubic * 1000) / 1000)
    });

    $('body').on('keyup', '.cal_size_wide, .cal_size_long, .cal_size_high, .cal_size_cubic, .insert_product_no_max', function(){
        var ele = $(this).closest('.add_product_lists');
        var size_wide   = $(ele).find('.cal_size_wide').val();
        var size_long   = $(ele).find('.cal_size_long').val();
        var size_high   = $(ele).find('.cal_size_high').val();
        //var size_cubic  = ele.find('.cal_size_cubic').val();
        var product_no_min   = $(ele).find('.insert_product_no_min').val();
        var product_no_max   = $(ele).find('.insert_product_no_max').val();
        var qty   = ((product_no_max-product_no_min)+1);
        var cubic = ((size_wide) * (size_long) * (size_high));
        $(ele).find('.cal_size_cubic').val(Math.round(cubic * 1000) / 1000)
        $(ele).find('.show_cal_size_cubic').val(Math.round((cubic * qty) * 1000) / 1000)
    });

    $('body').on('click', '.btn-delete', function(){
        $(this).closest('tr').remove();
    });

    $('body').on('change', '#add_product_name', function(){
        var code = $('#add_product_name').find(':selected').attr('data-code');
        var product_type = $('#add_product_name').find(':selected').attr('data-product_type');
        // $('#add_product_type_id').val(product_type);
        // $('#add_product_type_id').select2();
        $('#check_product_type_id').val(product_type);
        // $('#add_product_code').val(code);
    });

    $('body').on('click', '.btn-copy', function(){
        var ele = $(this).closest('tr');
        var pre_ele = $(this).closest('tr').prev();
        // console.log('test-copy');
        var size_wide = $(pre_ele).find('.insert_size_wide').val();
        if(typeof size_wide != 'undefined'){
            // console.log(1);
            // get data
            // var product_type_id             = $(pre_ele).find('.insert_product_type_id').val();
            // var product_type_name           = $(pre_ele).find('.insert_product_type_name').val();
            // var product_no_min              = $(pre_ele).find('.insert_product_no_min').val();
            // var product_no_max              = $(pre_ele).find('.insert_product_no_max').val();
            // var product_no_max_old          = $(pre_ele).find('.insert_product_no_max_old').val();
            // var container_type_id           = $(pre_ele).find('.insert_container_type_id').val();
            // var container_type_name         = $(pre_ele).find('.insert_container_type_name').val();
            // var product_name                = $(pre_ele).find('.insert_product_name').val();
            // var product_code                = $(pre_ele).find('.insert_product_code').val();
            // var chinese_shop                = $(pre_ele).find('.insert_chinese_shop').val();
            // var transportation_type_id      = $(pre_ele).find('.insert_transportation_type_id').val();
            // var transportation_type_name    = $(pre_ele).find('.insert_transportation_type_name').val();
            // var qty                         = $(pre_ele).find('.insert_qty').val();
            // var pcs                         = $(pre_ele).find('.insert_pcs').val();
            // var weigh_type                  = $(pre_ele).find('.insert_weigh_type').val();
            // var weigh_format                = $(pre_ele).find('.insert_weigh_format').val();
            // var size_weight_tote            = $(pre_ele).find('.insert_size_weight_tote').val();
            // var decription                  = $(pre_ele).find('.insert_decription').val();
            var size_wide                   = $(pre_ele).find('.insert_size_wide').val();
            var size_long                   = $(pre_ele).find('.insert_size_long').val();
            var size_high                   = $(pre_ele).find('.insert_size_high').val();
            var size_cubic                  = $(pre_ele).find('.insert_size_cubic').val();
            var show_size_cubic             = $(pre_ele).find('.show_cal_size_cubic').val();
            // set data
            // $(ele).find('.insert_product_type_id').val(product_type_id);
            // $(ele).find('.insert_product_type_name').val(product_type_name);
            // $(ele).find('.insert_product_no_min').val(product_no_min);
            // $(ele).find('.insert_product_no_max').val(product_no_max);
            // $(ele).find('.insert_product_no_max_old').val(product_no_max_old);
            // $(ele).find('.insert_container_type_id').val(container_type_id);
            // $(ele).find('.insert_container_type_name').val(container_type_name);
            // $(ele).find('.insert_product_name').val(product_name);
            // $(ele).find('.insert_product_code').val(product_code);
            // $(ele).find('.insert_chinese_shop').val(chinese_shop);
            // $(ele).find('.insert_transportation_type_id').val(transportation_type_id);
            // $(ele).find('.insert_transportation_type_name').val(transportation_type_name);
            // $(ele).find('.insert_qty').val(qty);
            // $(ele).find('.insert_pcs').val(pcs);
            // $(ele).find('.insert_weigh_type').val(weigh_type);
            // $(ele).find('.insert_weigh_format').val(weigh_format);
            // $(ele).find('.insert_size_weight_tote').val(size_weight_tote);
            // $(ele).find('.insert_decription').val(decription);
            $(ele).find('.insert_size_wide').val(size_wide);
            $(ele).find('.insert_size_long').val(size_long);
            $(ele).find('.insert_size_high').val(size_high);
            $(ele).find('.insert_size_cubic').val(size_cubic);
            $(ele).find('.show_cal_size_cubic').val(show_size_cubic);
        }else{
            // console.log('ไม่มีแถวก่อนหน้า');
        }
    });

    // $('body').on('change', '#add_product_type_id', function(){
    //     var check_product_type = $('#check_product_type_id').val();
    //     var add_product_type = $('#add_product_type_id').val();
    //     if(check_product_type){
    //         if(check_product_type != add_product_type){
    //             swal({
    //                 title: "คุณต้องการเปลี่ยนประเภทสินค้าใน สินค้า นี้หรือไม่",
    //                 text: "หากคุณเปลี่ยนข้อมูลก่อนหน้านี้จะถูกเปลี่ยนด้วย",
    //                 type: 'warning',
    //                 showCancelButton: true,
    //                 confirmButtonColor: '#3085d6',
    //                 cancelButtonColor: '#d33',
    //                 confirmButtonText: 'ใช่ ฉันต้องการเปลี่ยน',
    //                 confirmButtonClass: 'btn btn-success',
    //                 cancelButtonText: "ยกเลิก",
    //                 cancelButtonClass: 'btn btn-danger',
    //                 showCancelButton: true,
    //             }).then(function(result) {
    //                 if (result) {
    //                     $('#check_product_type_id').val(add_product_type);
    //                     $('#add_product_type_id').val(add_product_type);
    //                     $('#add_product_type_id').select2();
    //                 }
    //             },function(dismiss) {
    //                 if(dismiss == 'cancel') {
    //                     $('#check_product_type_id').val(check_product_type);
    //                     $('#add_product_type_id').val(check_product_type);
    //                     $('#add_product_type_id').select2();
    //                 }
    //             });
    //         }
    //     }else{
    //         $('#check_product_type_id').val(add_product_type);
    //         $('#add_product_type_id').val(add_product_type);
    //         $('#add_product_type_id').select2();
    //     }
    // });

    // var check_customer = '';
    // var check_type = '';
    // $('body').on('change', '#add_product_type_id, #add_customer_id, #add_product_name', function(){
    //     if($('#add_product_type_id').val() && $('#add_customer_id').val()){
    //         if($('#add_product_type_id').val() != check_type || $('#add_customer_id').val() != check_customer){
    //             $.ajax({
    //                 method : "POST",
    //                 url : url_gb +"/admin/{{$lang}}/ImportToChaina/GetPrice",
    //                 dataType : 'json',
    //                 data :  {
    //                     'product_type_id' : $('#add_product_type_id').val(),
    //                     'customer_id' : $('#add_customer_id').val(),
    //                 }
    //             }).done(function(rec){
    //
    //                 var html = '<option value="">เลือกราคา</option>';
    //                 $.each(rec.price, function(k, v){
    //                     html += '<option value="'+v.id+'">'+v.price+'</option>';
    //                 });
    //                 $('#add_rel_user_type_product_id').html(html);
    //                 $('#add_rel_user_type_product_id').select2();
    //
    //             }).fail(function(){
    //                 swal("system.system_alert","system.system_error","error");
    //             });
    //         }
    //     }
    //     check_customer = $('#add_customer_id').val();
    //     check_type = $('#add_product_type_id').val();
    // });

    $('#FormEdit').validate({
        errorElement: 'div',
        errorClass: 'invalid-feedback',
        focusInvalid: false,
        rules: {
            'import_to_chaina[customer_id]': {
                required: true,
            },
            // 'import_to_chaina[po_number]': {
            //     required: true,
            // },
            'import_to_chaina[import_to_chaina_date]': {
                required: true,
            },
            // product_type_id: {
            //     required: true,
            // },
            // container_type_id: {
            //     required: true,
            // },
            // add_customer_id: {
            //     required: true,
            // },
            // product_name: {
            //     required: true,
            // },
            // chinese_shop: {
            //     required: true,
            // },
        },
        messages: {

            'import_to_chaina[customer_id]': {
                required: "กรุณาระบุ",
            },
            // 'import_to_chaina[po_number]': {
            //     required: "กรุณาระบุ",
            // },
            'import_to_chaina[import_to_chaina_date]': {
                required: "กรุณาระบุ",
            },
            // product_type_id: {
            //     required: "กรุณาระบุ",
            // },
            // container_type_id: {
            //     required: "กรุณาระบุ",
            // },
            // add_customer_id: {
            //     required: "กรุณาระบุ",
            // },
            // product_name: {
            //     required: "กรุณาระบุ",
            // },
            // chinese_shop: {
            //     required: "กรุณาระบุ",
            // },
        },
        highlight: function (e) {
            validate_highlight(e);
        },
        success: function (e) {
            validate_success(e);
        },

        errorPlacement: function (error, element) {
            validate_errorplacement(error, element);
        },
        submitHandler: function (form) {
            /*
            if(CKEDITOR!==undefined){
                for ( instance in CKEDITOR.instances ){
                    CKEDITOR.instances[instance].updateElement();
                }
            }
            */
            if($('.list-product-sub').length > 0){
                var btn = $(form).find('[type="submit"]');
                var data_ar = removePriceFormat(form,$(form).serializeArray());
                var id_import = $("#add_id").val();
                btn.button("loading");
                $.ajax({
                    method : "POST",
                    url : url_gb+"/admin/{{$lang}}/ImportToChaina/edit/"+id_import,
                    dataType : 'json',
                    data : $(form).serialize()
                }).done(function(rec){
                    btn.button("reset");
                    if(rec.status==1){
                        swal(rec.title,rec.content,"success");
                        window.location.href = url_gb+"/admin/{{$lang}}/ImportToChaina";
                    }else{
                        swal(rec.title,rec.content,"error");
                    }
                }).fail(function(){
                    swal("แก้ไขข้อมูล","สินค้าอาจมีการใช้งานไปแล้วจึงไม่สามารถแก้ไขได้","error");
                    btn.button("reset");
                });
            }
        },
        invalidHandler: function (form) {

        }
    });

    $('#add_customer_id').select2();
    $('#add_product_type_id').select2();
    $('#add_product_name').select2({
        tags: true
    });
    $('#add_container_type_id').select2();
    $('#add_transportation_type_id').select2();
    // $('#add_rel_user_type_product_id').select2();

</script>
@endsection
