﻿@extends('Admin.layouts.layout')
@section('css_bottom')
@endsection
@section('body')
<div class="content">
    <div class="col-md-12">
    <div class="container-fluid">
        <form id="FormAddDelivery">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="content">
                            <h4 class="title">
                                <h4 class="modal-title" id="myModalLabel">{{$title_page or 'ข้อมูลใหม่'}}</h4>
                            </h4>
                            <div class="material-datatables">
                                <div class="modal-header">

                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-3">
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="add_date_delivery">@lang('lang.delivery_date')</label>

                                                <div class="input-group" data-date="{{date('Y-m-d')}}">
                                                    <input type="text" value="" readonly="readonly" class="form-control" name="delivery[date_delivery]" id="add_date_delivery"  placeholder="date_delivery">
                                                    <span class="input-group-addon remove_date_time"><i class="glyphicon glyphicon-remove icon-remove"></i></span>
                                                    <span class="input-group-addon trigger_date_time" for="date_delivery"><i class="glyphicon glyphicon-calendar icon-calendar"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="add_container_id">@lang('lang.select_container')</label>
                                                <select name="delivery[container_id]" class="select2 form-control" tabindex="-1" data-placeholder="@lang('lang.select_container')" id="add_container_id" >
                                                    <option value="">@lang('lang.select_container')</option>

                                                    @if(!$Containers->isEmpty())
                                                        @foreach($Containers as $Container)
                                                            <option value="{{ $Container->id }}" {{ $Container->status_id == 4 ? 'disabled' : null}}>
                                                                {{ $Container->container_code }} 
                                                                {{ $Container->status_id == 4 ? '(ออกใบส่งของไปแล้ว)' : null}}</p>
                                                            </option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary">@lang('lang.save')</button>
                                    <button type="cancel" class="btn btn-danger">@lang('lang.cancel')</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="content">
                            <div class="material-datatables">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="myModalLabel">@lang('lang.barcode_list')</h5>
                                </div>
                                <div class="modal-body">
                                    <table class="table" style="width: 100%;">
                                        <thead>
                                            <tr>
                                                <td class="text-center">#</td>
                                                <td class="text-center">@lang('lang.number_po')</td>
                                                <td class="text-center">@lang('lang.pieces_by_po')</td>
                                                <td class="text-center">@lang('lang.number_of_actual')</td>
                                            </tr>
                                        </thead>
                                        <tbody class="add-product-po">
                                            <tr>
                                                <td class="text-center" colspan="5">@lang('lang.data_not_found')</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table class="table">
                                        <thead class="text-primary">
                                            <tr>
                                                <td>#</td>
                                                <td>@lang('lang.number_po')</td>
                                                <td>@lang('lang.qrcode')</td>
                                                <td>@lang('lang.lot_id')</td>
                                                <td>@lang('lang.customer_id')</td>
                                                <td>@lang('lang.type_product')</td>
                                                <td>@lang('lang.product_name')</td>
                                                <td>@lang('lang.weigh')</td>
                                                <td>@lang('lang.queue_size')</td>
                                            </tr>
                                        </thead>
                                        <tbody id="list-qr-code">

                                        </tbody>
                                        <tfooter>
                                            <tr id="qr-code-not-found">
                                                <td colspan=10 style="text-align: center;">@lang('lang.data_not_found')</td>
                                            </tr>
                                        </tfooter>
                                    </table>
                                </div>
                                <div class="modal-footer">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
@section('js_bottom')
<script src="{{asset('assets/global/plugins/orakuploader/orakuploader.js')}}"></script>
<script>

    $("#add_date_delivery").daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        minYear: 1901,
        startDate: '{{date("Y-m-d")}}',
        // maxYear: parseInt(moment().format('YYYY'),10),
        locale: {
            format: 'YYYY-MM-DD'
        }
    }, function(start, end, label) {
        var years = moment().diff(start, 'years');
    });

    $('body').on('change', '#add_container_id', function(){
        var container_id = $(this).val();
        $.ajax({
            method : "POST",
            url : url_gb +"/admin/{{$lang}}/DeliveryContainer/GetContainerDetail/"+ container_id,
            dataType: 'json'
            // data : {ID : id}
        }).done(function(rec){
            $('#ModalContainerDetail').modal('show');
            var html = '';
            var count = rec.QrCodeProduct.length;
            $('#list-qr-code').html('');
            $('.add-product-po').html('');

            if(rec.Containers && count > 0){
                $.each(rec.QrCodeProduct, function(key, qrcode){
                    
                    html += '<tr>\
                                <td class="countRow">'+(key+1)+'</td>\
                                <td>'+qrcode.po_no+'</td>\
                                <td>'+qrcode.qr_code+'\
                                <input type="hidden" class="po_no_id" value="'+qrcode.import_to_chaina_id+'">\
                                <input type="hidden" class="po_no" value="'+qrcode.po_no+'">\
                                <input type="hidden" class="po_qty" value="'+qrcode.po_qty+'">\
                                <input type="hidden" class="lot_product_id" value="'+qrcode.lot_product_id+'">\
                                <input type="hidden" class="lot_product_qty" value="'+qrcode.lot_product_qty+'">\
                                <input type="hidden" class="qr_code_id" value="'+qrcode.id+'">\
                                <input type="hidden" class="qr_code_name"  value="'+qrcode.qr_code+'">\
                                <input type="hidden" class="weight" name="price['+qrcode.user_id+']['+qrcode.import_to_chaina_id+']['+qrcode.id+'][weight]" value="'+(qrcode.weight_per_item)+'">\
                                <input type="hidden" class="rate" name="price['+qrcode.user_id+']['+qrcode.import_to_chaina_id+']['+qrcode.id+'][rate]" value="'+(qrcode.price)+'">\
                                <input type="hidden" class="width" value="'+(qrcode.width / qrcode.lot_product_qty)+'">\
                                <input type="hidden" class="length" value="'+(qrcode.length / qrcode.lot_product_qty)+'">\
                                <input type="hidden" class="height" value="'+(qrcode.height / qrcode.lot_product_qty)+'">\
                                <input type="hidden" class="cubic" value="'+(qrcode.product_cobic / qrcode.lot_product_qty)+'">\
                                <input type="hidden" class="user_id" value="'+qrcode.user_id+'">\
                                </td>\
                                <td>'+qrcode.lot_product_id+'</td>\
                                <td>'+qrcode.customer_general_code+'</td>\
                                <td>'+qrcode.product_type_name+'</td>\
                                <td>'+qrcode.product_name+'</td>\
                                <td>'+addNumformat(Math.round(qrcode.weight_per_item * 1000) / 1000)+'</td>\
                                <td>'+addNumformat(Math.round((qrcode.product_cobic / qrcode.lot_product_qty) * 1000) / 1000)+'</td>\
                            </tr>';

                });
                $('#list-qr-code').html(html);
                $('#qr-code-not-found').hide();
                showPOandLot();
            } else {
                html =  '<tr id="qr-code-not-found">\
                            <td colspan=10 style="text-align: center;">@lang('lang.data_not_found')</td>\
                        </tr>';
                $('#qr-code-not-found').show();
                $('.add-product-po').html(html);
            }
        }).fail(function(data){
            swal("ระบบมีปัญหา","กรุณาติดต่อผู้ดูแล","error");
        });
    });

    var send_order = {};
    function showPOandLot(){
        var product_for_show = $('#list-qr-code > tr');
        var groups = {};
        send_order = {};
        var html = '';
        if (product_for_show.length > 0) {
            $.each(product_for_show, function(k, v){
                var po = $(v).find('.po_no').val();
                var lot_id = $(v).find('.lot_product_id').val();
                var qr_code_name = $(v).find('.qr_code_name').val();
                var po_qty = $(v).find('.po_qty').val();
                var lot_product_qty = $(v).find('.lot_product_qty').val();

                var po_no_id = $(v).find('.po_no_id').val();
                var user_id = $(v).find('.user_id').val();

                if (!groups[po]) {
                    groups[po] = {};
                }
                if (!groups[po][lot_id]) {
                    groups[po][lot_id] = {};
                }
                if (!groups[po][lot_id]['amount']) {
                    groups[po][lot_id]['amount'] = [];
                }
                groups[po]['po_qty'] = po_qty;
                groups[po][lot_id]['lot_product_qty'] = lot_product_qty;
                groups[po][lot_id]['amount'].push(qr_code_name);

                if (!send_order[user_id]) {
                    send_order[user_id] = {};
                }
                if (!send_order[user_id][po_no_id]) {
                    send_order[user_id][po_no_id] = {};
                }
                send_order[user_id][po_no_id]['user_id'] = user_id;
                send_order[user_id][po_no_id]['import_to_chaina_id'] = po_no_id;
            });

            var No_Po = 1;
            var style = '';
            var style_lot = '';
            $.each(groups, function(k1, v1){
                var sumallproduct = 0;
                $.each(v1, function(k2, v2){
                    if (k2 != 'po_qty') {
                        sumallproduct += v2.amount.length
                    }
                });
                if(v1.po_qty > sumallproduct){
                    style = 'rgba(255, 0, 0, 0.33)';
                }else if(v1.po_qty < sumallproduct){
                    style = 'rgba(255, 235, 59, 0.5)';
                }else{
                    style = 'rgba(76, 175, 80, 0.42)';
                }
                html += '\
                    <tr style="background-color: '+style+'">\
                        <td>'+No_Po+'</td>\
                        <td>'+k1+'</td>\
                        <td class="text-center">'+addNumformat(v1.po_qty)+'</td>\
                        <td class="text-center">'+addNumformat(sumallproduct)+'</td>\
                    </tr>';
                $.each(v1, function(k2, v2){
                    if (k2 != 'po_qty') {
                        if(v2.lot_product_qty > v2.amount.length){
                            style_lot = 'rgba(255, 0, 0, 0.33)';
                        }else if(v2.lot_product_qty < v2.amount.length){
                            style_lot = 'rgba(255, 235, 59, 0.5)';
                        }else{
                            style_lot = 'rgba(76, 175, 80, 0.42)';
                        }
                        html += '\
                            <tr style="background-color: '+style+'">\
                                <td></td>\
                                <td class="text-right">Lot '+k2+'</td>\
                                <td class="text-center">'+addNumformat(v2.lot_product_qty)+'</td>\
                                <td class="text-center">'+addNumformat(v2.amount.length)+'</td>\
                            </tr>';
                    }
                });
                No_Po++;
            });
        }else{
            html += '\
                <tr>\
                    <td colspan="5" class="text-center">@lang('lang.data_not_found')</td>\
                </tr>';
        }

        $('.add-product-po').html(html)
    }

    $('#FormAddDelivery').validate({
        errorElement: 'div',
        errorClass: 'invalid-feedback',
        focusInvalid: false,
        rules: {
            'delivery[container_id]': {
                required: true,
            },
        },
        messages: {
            'delivery[container_id]': {
                required: '@lang("lang.please_specify")',
            },
        },
        highlight: function (e) {
            validate_highlight(e);
        },
        success: function (e) {
            validate_success(e);
        },

        errorPlacement: function (error, element) {
            validate_errorplacement(error, element);
        },
        submitHandler: function (form) {
            /*
            if(CKEDITOR!==undefined){
                for ( instance in CKEDITOR.instances ){
                    CKEDITOR.instances[instance].updateElement();
                }
            }
            */
            var btn = $(form).find('[type="submit"]');
            var data_ar = removePriceFormat(form,$(form).serializeArray());
            btn.button("loading");
            $.ajax({
                method : "POST",
                url : url_gb+"/admin/{{$lang}}/DeliveryContainer",
                dataType : 'json',
                data : $(form).serialize()+'&send_order='+JSON.stringify(send_order)
            }).done(function(rec){
                btn.button("reset");
                if(rec.status==1){
                    swal(rec.title,rec.content,"success");
                    window.location.href = url_gb+"/admin/{{$lang}}/DeliveryContainer";
                }else{
                    swal(rec.title,rec.content,"error");
                }
            }).fail(function(){
                swal("system.system_alert","@lang('lang.system_system_error')","error");
                btn.button("reset");
            });
        },
        invalidHandler: function (form) {

        }
    });

    $('#add_container_id').select2();


</script>
@endsection
