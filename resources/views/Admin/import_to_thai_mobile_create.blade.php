﻿@extends('Admin.layouts.layout')
@section('css_bottom')
<style>
    .fix-scroll-dashboard {
        width: 100%;
        overflow-x: scroll;
    }
</style>
@endsection
@section('body')
<div class="content">
    <div class="col-md-12">
    <div class="container-fluid">
        <form id="FormGetQRCode">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="content">
                            <h4 class="title">
                                {{$title_page or '' }}
                                <a href="{{ url('/admin/'.$lang.'/ImportToThai/Create') }}" class="btn btn-success btn-add pull-right" >
                                    + <i class="fa fa-desktop" style="font-size: 20px;"></i>
                                </a>
                            </h4>
                            <h4 class="title">
                                <a href="{{ url('/admin/'.$lang.'/ImportToThai/Mobile/Create') }}" class="btn btn-success btn-add pull-right" >
                                    + <i class="fa fa-mobile" style="font-size: 25px;"></i>&nbsp&nbsp
                                </a>
                            </h4>
                            <div class="material-datatables">
                                <div class="modal-header">

                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-3">
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="add_container_id">@lang('lang.select_container')</label>
                                                <select name="container_id" class="select2 form-control" tabindex="-1" data-placeholder="@lang('lang.select_container')" id="add_container_id" >
                                                    <option value="">@lang('lang.select_container')</option>
                                                    @if(!$Containers->isEmpty())
                                                        @foreach($Containers as $Container)
                                                            <option value="{{ $Container->id }}">{{ $Container->container_code }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="add_qrcode">@lang('lang.barcode')</label>
                                                <input type="text" class="form-control" name="qr_code" id="add_qrcode" value="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12" style="text-align: center;">
                                            <button type="submit" class="btn btn-get-qrcode btn-primary">@lang('lang.save')</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <form id="FormAddImportToThai">
            <input type="hidden" name="container_id" id="show_container_id">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="content">
                            <div class="material-datatables">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="myModalLabel">@lang('lang.table_of_products_scanned_by_po')</h5>
                                </div>
                                <div class="modal-body fix-scroll-dashboard">
                                    <table class="table">
                                        <thead class="text-primary">
                                            <tr>
                                                <th>#</th>
                                                <th>@lang('lang.number_po')</th>
                                                <th>@lang('lang.number_of_po')</th>
                                                <th>@lang('lang.number_piece_close')</th>
                                                <th>@lang('lang.actual_number')</th>
                                            </tr>
                                        </thead>
                                        <tbody id="list-products">

                                        </tbody>
                                    <tfooter>
                                        <tr id="products-not-found">
                                            <td colspan=10 style="text-align: center;">@lang('lang.data_not_found')</td>
                                        </tr>
                                    </tfooter>
                                    </table>
                                    <!-- <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="add_customer_id">หมายเหตุ</label>
                                            <input type="text" class="form-control"  id="add_remark" value="">
                                        </div>
                                    </div> -->
                                </div>

                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary">@lang('lang.save')</button>
                                    <button type="cancel" class="btn btn-danger">@lang('lang.cancel')</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="card">
                        <div class="content">
                            <div class="material-datatables">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="myModalLabel">@lang('lang.list_po')</h5>
                                </div>
                                <div class="modal-body fix-scroll-dashboard">
                                    <table class="table">
                                        <thead class="text-primary">
                                            <tr>
                                                <th>#</th>
                                                <th>@lang('lang.customer_id')</th>
                                                <th>@lang('lang.number_po')</th>
                                                <th>@lang('lang.delivery_dates')</th>
                                                <th>@lang('lang.number_of_po')</th>
                                                <th>@lang('lang.weigh')</th>
                                                <th>@lang('lang.queue_size')</th>
                                            </tr>
                                        </thead>
                                        <tbody id="list-po">

                                        </tbody>
                                        <tfooter>
                                            <tr id="op-not-found">
                                                <td colspan=10 style="text-align: center;">@lang('lang.data_not_found')</td>
                                            </tr>
                                        </tfooter>
                                    </table>
                                </div>
                                <div class="modal-footer">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="card">
                        <div class="content">
                            <div class="material-datatables">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="myModalLabel">@lang('lang.barcode_list')</h5>
                                </div>
                                <div class="modal-body fix-scroll-dashboard">
                                    <table class="table">
                                        <thead class="text-primary">
                                            <tr>
                                                <th>@lang('lang.product_id')</th>
                                                <th>@lang('lang.number_po')</th>
                                                <th>@lang('lang.qrcode')</th>
                                                <th>@lang('lang.lot_id')</th>
                                                <th>@lang('lang.customer_id')</th>
                                                <th>@lang('lang.type_product')</th>
                                                <th>@lang('lang.product_name')</th>
                                            </tr>
                                        </thead>
                                        <tbody id="list-qr-code">

                                        </tbody>
                                        <tfooter>
                                            <tr id="qr-code-not-found">
                                                <td colspan=10 style="text-align: center;">@lang('lang.data_not_found')</td>
                                            </tr>
                                        </tfooter>
                                    </table>
                                </div>
                                <div class="modal-footer">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
@section('js_bottom')
<script src="{{asset('assets/global/plugins/orakuploader/orakuploader.js')}}"></script>
<script>

    $('body').on('change', '#add_container_id', function(){
        var container_id = $(this).val();
        $('#show_container_id').val(container_id);
        $('#add_qrcode').val('');
        $('#add_remark').val('');
        $('#list-po').html('');
        $('#op-not-found').show();
        $('#list-qr-code').html('');
        $('#qr-code-not-found').show();
        $('#list-products').html('');
        $('#products-not-found').show();
        $('#add_qrcode').focus();
    });

    $('#FormAddImportToThai').validate({
        errorElement: 'div',
        errorClass: 'invalid-feedback',
        focusInvalid: false,
        rules: {
            contauner_id: {
                required: true,
            },
        },
        messages: {
            contauner_id: {
                required: "@lang('lang.please_specify')",
            },
        },
        highlight: function (e) {
            validate_highlight(e);
        },
        success: function (e) {
            validate_success(e);
        },
        errorPlacement: function (error, element) {
            validate_errorplacement(error, element);
        },
        submitHandler: function (form) {
            /*
            if(CKEDITOR!==undefined){
                for ( instance in CKEDITOR.instances ){
                    CKEDITOR.instances[instance].updateElement();
                }
            }
            */
            var btn = $(form).find('[type="submit"]');
            var data_ar = removePriceFormat(form,$(form).serializeArray());
            btn.button("loading");
            $.ajax({
                method : "POST",
                url : url_gb+"/admin/{{$lang}}/ImportToThai",
                dataType : 'json',
                data : $(form).serialize()
            }).done(function(rec){
                btn.button("reset");
                if(rec.status==1){
                    swal(rec.title,rec.content,"success");
                    window.location.href = url_gb+"/admin/{{$lang}}/ImportToThai";
                }else{
                    swal(rec.title,rec.content,"error");
                }
            }).fail(function(){
                swal("system.system_alert","system.system_error","error");
                btn.button("reset");
            });
        },
        invalidHandler: function (form) {

        }
    });

    $('#FormGetQRCode').validate({
        errorElement: 'div',
        errorClass: 'invalid-feedback',
        focusInvalid: false,
        rules: {
            container_id: {
                required: true,
            },
            qr_code: {
                required: true,
            },

        },
        messages: {

            container_id: {
                required: "@lang('lang.please_specify')",
            },
            qr_code: {
                required: "@lang('lang.please_specify')",
            },

        },
        highlight: function (e) {
            validate_highlight(e);
        },
        success: function (e) {
            validate_success(e);
        },

        errorPlacement: function (error, element) {
            validate_errorplacement(error, element);
        },
        submitHandler: function (form) {
            /*
            if(CKEDITOR!==undefined){
                for ( instance in CKEDITOR.instances ){
                    CKEDITOR.instances[instance].updateElement();
                }
            }
            */
            var btn = $(form).find('[type="submit"]');
            btn.button("loading");
            $.ajax({
                method : "POST",
                url : url_gb+"/admin/{{$lang}}/ImportToThai/CheckQrCode",
                dataType : 'json',
                data : $(form).serialize()
            }).done(function(rec){
                btn.button("reset");
                var html_list_qr_code = "";
                var html_list_po = "";
                var html_list_product = "";
                var html_lot_product = "";

                if(rec.status==1){
                    var number_po = $('#po_id'+rec.ImportToChaina.id).val();
                    var qr_code_id = $('#qr_code_id'+rec.QrCodeProduct.id).val();
                    var import_to_chaina_id = $('#import_to_chaina_id'+rec.QrCodeProduct.import_to_chaina_id).val();
                    var lot_product_id = $('#lot_product_id'+rec.LotProduct.id).val();
                    var amount_po = $('#amount_po'+rec.QrCodeProduct.import_to_chaina_id).val();
                    var amount_lot = $('#amount_lot'+rec.QrCodeProduct.import_to_chaina_id+'_'+rec.LotProduct.id).val();


                    if(rec.ImportToChaina.po_no != number_po){
                        html_list_po += '<tr>\
                            <td>#</td>\
                            <td>'+rec.QrCodeProduct.import_to_chaina.user.customer_general_code+'</td>\
                            <td>'+rec.ImportToChaina.po_no+'\
                                <input type="hidden" id="po_id'+rec.ImportToChaina.id+'" value="'+rec.ImportToChaina.po_no+'">\
                            </td>\
                            <td>'+rec.ImportToChaina.import_date+'</td>\
                            <td>'+addNumformat(rec.qty_po)+'</td>\
                            <td>'+addNumformat(rec.weight_all)+'</td>\
                            <td>'+addNumformat(rec.cubic)+'</td>\
                        </tr>';

                        $('#op-not-found').hide();
                        $('#list-po').append(html_list_po);
                    }
                    var num_po = 1;
                    var num_lot = 1;
                    if(rec.QrCodeProduct.id != qr_code_id){

                        if(rec.QrCodeProduct.import_to_chaina_id == import_to_chaina_id){
                             num_po = (parseInt(num_po) + parseInt(amount_po));
                             $('#amount_po'+rec.QrCodeProduct.import_to_chaina_id).val(num_po);
                             $('#amount_po_html'+rec.QrCodeProduct.import_to_chaina_id).html(num_po);

                             if(rec.LotProduct.id == lot_product_id){
                                 num_lot = (parseInt(num_lot) + parseInt(amount_lot));
                                 $('#amount_lot'+rec.QrCodeProduct.import_to_chaina_id+'_'+rec.LotProduct.id).val(num_lot);
                                 $('#amount_lot_html'+rec.QrCodeProduct.import_to_chaina_id+'_'+rec.LotProduct.id).html(num_lot);
                             }else{
                                  html_list_product += '<tr class="list_po'+rec.ImportToChaina.id+'_list_lot'+rec.LotProduct.id+'">\
                                      <td>\
                                          <input type="hidden" id="lot_product_id'+rec.LotProduct.id+'" value="'+rec.LotProduct.id+'">\
                                      </td>\
                                      <td>Lot'+rec.LotProduct.id+'</td>\
                                      <td><input type="hidden" id="qty_lot_chaina'+rec.QrCodeProduct.import_to_chaina_id+'_'+rec.LotProduct.id+'" value="'+rec.qty_lot_product+'">\
                                      '+addNumformat(rec.qty_lot_product)+'</td>\
                                      <td><input type="hidden" id="qty_lot_container'+rec.QrCodeProduct.import_to_chaina_id+'_'+rec.LotProduct.id+'" value="'+rec.qty_lot_product+'">\
                                      '+addNumformat(rec.qty_lot_product)+'</td>\
                                      <td><input type="hidden" id="amount_lot'+rec.QrCodeProduct.import_to_chaina_id+'_'+rec.LotProduct.id+'" value="'+num_lot+'">\
                                      <label id="amount_lot_html'+rec.QrCodeProduct.import_to_chaina_id+'_'+rec.LotProduct.id+'" >'+addNumformat(num_lot)+'</label></td>\
                                  </tr>';

                                  $('#list-products').append(html_list_product);
                             }
                        }else{
                            html_list_product += '<tr class="list_po'+rec.ImportToChaina.id+'">\
                                                        <td>#</td>\
                                                        <td>'+rec.ImportToChaina.po_no+'\
                                                            <input type="hidden" id="import_to_chaina_id'+rec.QrCodeProduct.import_to_chaina_id+'" value="'+rec.QrCodeProduct.import_to_chaina_id+'">\
                                                        </td>\
                                                        <td><input type="hidden" id="qty_po_chaina'+rec.QrCodeProduct.import_to_chaina_id+'" value="'+rec.qty_po+'">\
                                                        '+addNumformat(rec.qty_po)+'</td>\
                                                        <td><input type="hidden" id="qty_po_container'+rec.QrCodeProduct.import_to_chaina_id+'" value="'+rec.qty_container+'">\
                                                        '+addNumformat(rec.qty_container)+'</td>\
                                                        <td><input type="hidden" id="amount_po'+rec.QrCodeProduct.import_to_chaina_id+'" value="'+num_po+'">\
                                                        <label id="amount_po_html'+rec.QrCodeProduct.import_to_chaina_id+'" >'+addNumformat(num_po)+'</label></td>\
                                                    </tr>\
                                                    <tr class="list_po'+rec.ImportToChaina.id+'_list_lot'+rec.LotProduct.id+'">\
                                                        <td>\
                                                            <input type="hidden" id="lot_product_id'+rec.LotProduct.id+'" value="'+rec.LotProduct.id+'">\
                                                        </td>\
                                                        <td>Lot'+rec.LotProduct.id+'</td>\
                                                        <td><input type="hidden" id="qty_lot_chaina'+rec.QrCodeProduct.import_to_chaina_id+'_'+rec.LotProduct.id+'" value="'+rec.qty_lot_product+'">\
                                                        '+addNumformat(rec.qty_lot_product)+'</td>\
                                                        <td><input type="hidden" id="qty_lot_container'+rec.QrCodeProduct.import_to_chaina_id+'_'+rec.LotProduct.id+'" value="'+rec.qty_lot_product+'">\
                                                        '+addNumformat(rec.qty_lot_product)+'</td>\
                                                        <td><input type="hidden" id="amount_lot'+rec.QrCodeProduct.import_to_chaina_id+'_'+rec.LotProduct.id+'" value="'+num_lot+'">\
                                                        <label id="amount_lot_html'+rec.QrCodeProduct.import_to_chaina_id+'_'+rec.LotProduct.id+'" >'+addNumformat(num_lot)+'</label></td>\
                                                    </tr>';

                            $('#products-not-found').hide();
                            $('#list-products').append(html_list_product);
                        }

                        html_list_qr_code += '<tr>\
                            <td>#</td>\
                            <td>'+rec.ImportToChaina.po_no+'</td>\
                            <td>'+rec.QrCodeProduct.qr_code+'\
                                <input type="hidden" name="import_to_chaina_id['+rec.ImportToChaina.id+'][qr_code_product_id]['+rec.QrCodeProduct.id+']" value="'+rec.QrCodeProduct.id+'">\
                            </td>\
                            <td>Lot'+rec.LotProduct.id+'</td>\
                            <td>'+rec.QrCodeProduct.import_to_chaina.user.customer_general_code+'\
                                <input type="hidden" id="qr_code_id'+rec.QrCodeProduct.id+'" value="'+rec.QrCodeProduct.id+'">\
                            </td>\
                            <td>'+rec.QrCodeProduct.product_import_to_chaina.product.product_type.name+'</td>\
                            <td>'+rec.QrCodeProduct.product_import_to_chaina.product.name+'</td>\
                        </tr>';

                        $('#qr-code-not-found').hide();
                        $('#add_qrcode').val('');
                        $('#list-qr-code').append(html_list_qr_code);

                        var check_qty_po_chaina = $('#qty_po_chaina'+rec.QrCodeProduct.import_to_chaina_id).val();
                        var check_qty_po_container = $('#qty_po_container'+rec.QrCodeProduct.import_to_chaina_id).val();
                        var check_amount_po = $('#amount_po'+rec.QrCodeProduct.import_to_chaina_id).val();

                        var check_qty_lot_chaina = $('#qty_lot_chaina'+rec.QrCodeProduct.import_to_chaina_id+'_'+rec.LotProduct.id).val();
                        var check_qty_lot_container = $('#qty_lot_container'+rec.QrCodeProduct.import_to_chaina_id+'_'+rec.LotProduct.id).val();
                        var check_amount_lot = $('#amount_lot'+rec.QrCodeProduct.import_to_chaina_id+'_'+rec.LotProduct.id).val();

                        if(check_amount_po == check_qty_po_container && check_qty_po_container == check_qty_po_chaina){
                            $('#list-products').find('.list_po'+rec.QrCodeProduct.import_to_chaina_id).css("background-color", "rgba(76, 175, 80, 0.42);");
                        }else if(check_amount_po == check_qty_po_container && check_qty_po_container != check_qty_po_chaina){
                            $('#list-products').find('.list_po'+rec.QrCodeProduct.import_to_chaina_id).css("background-color", "rgba(255, 235, 59, 0.5);");
                        }else{
                            $('#list-products').find('.list_po'+rec.QrCodeProduct.import_to_chaina_id).css("background-color", "rgba(255, 0, 0, 0.33);");
                        }

                        if(check_amount_lot == check_qty_lot_container && check_qty_lot_container == check_qty_lot_chaina){
                            $('.list_po'+rec.QrCodeProduct.import_to_chaina_id+'_list_lot'+rec.LotProduct.id).css("background-color", "rgba(76, 175, 80, 0.42);");
                        }else if(check_amount_lot == check_qty_lot_container && check_qty_lot_container != check_qty_lot_chaina){
                            $('.list_po'+rec.QrCodeProduct.import_to_chaina_id+'_list_lot'+rec.LotProduct.id).css("background-color", "rgba(255, 235, 59, 0.5);");
                        }else{
                            $('.list_po'+rec.QrCodeProduct.import_to_chaina_id+'_list_lot'+rec.LotProduct.id).css("background-color", "rgba(255, 0, 0, 0.33);");
                        }

                    }else{
                        $('#add_qrcode').val('');
                        swal("ระบบแจ้งเตือน","รายการถูกคีย์เข้าระบบแล้ว กรุณาลองใหม่", "error");
                    }


                }else{
                     $('#add_qrcode').val('');
                    swal(rec.title,rec.content,"error");
                }
                beep();
            }).fail(function(){
                swal("system.system_alert","system.system_error","error");
                btn.button("reset");
            });
        },
        invalidHandler: function (form) {

        }
    });

    //$('#add_container_id').select2();


</script>
@endsection
