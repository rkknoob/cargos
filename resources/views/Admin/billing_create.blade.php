﻿@extends('Admin.layouts.layout')
@section('css_bottom')
@endsection
@section('body')
<div class="content">
    <div class="col-md-12">
    <div class="container-fluid">
        <form id="FormAddBilling">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="content">
                                <h4 class="title">
                                    <h4 class="modal-title" id="myModalLabel">@lang('lang.billing_number')</h4>
                                </h4>
                                <div class="material-datatables">
                                    <div class="modal-header">
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="add_billing_no">@lang('lang.billing_number')</label>
                                                    <input type="text" class="form-control" name="billing[billing_no]" value="{{ $BillingNo }}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="add_delivery_slip_date">@lang('lang.issued_date_billing')</label>
                                                    <div class="input-group" data-date="{{date('Y-m-d')}}">
                                                        <input type="text" value="" readonly="readonly" class="form-control" name="billing[billing_date]" id="add_billing_date"  placeholder="import_to_chaina_date">
                                                        <span class="input-group-addon remove_date_time"><i class="glyphicon glyphicon-remove icon-remove"></i></span>
                                                        <span class="input-group-addon trigger_date_time" for="add_delivery_slip_date"><i class="glyphicon glyphicon-calendar icon-calendar"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="add_user_id">@lang('lang.customer')</label>
                                                    <select name="billing[user_id]" class="select2 form-control" tabindex="-1" data-placeholder="@lang('lang.customer')" id="add_user_id">
                                                        <option value="">@lang('lang.customer')</option>
                                                        @if(!$Users->isEmpty())
                                                            @foreach($Users as $User)
                                                                <option value="{{ $User->id }}">{{ $User->customer_general_code }} </option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="add_billing_no">@lang('lang.note')</label>
                                                    <input type="text" class="form-control" name="billing[remark]">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="margin-top: 40px;">
                                            <div class="col-md-12">
                                                <h5>@lang('lang.po_items_shipped')</h5>
                                                <div class="fix-scroll-dashboard">
                                                    <table class="table">
                                                        <thead>
                                                            <tr>
                                                                <th>
                                                                    <div class="form-check">
                                                                        <label for="edit_active" class="checkbox form-check-label">
                                                                            <span class="icons">
                                                                                <span class="first-icon fa fa-square"></span>
                                                                                <span class="second-icon fa fa-check-square "></span>
                                                                            </span><input type="checkbox" class="form-check-input" data-toggle="checkbox" id="check_delivery_active_all" required="" aria-required="true">
                                                                        </label>
                                                                    </div>
                                                                </th>
                                                                <th>@lang('lang.customer_id')</th>
                                                                <th>@lang('lang.delivery_date')</th>
                                                                <th>@lang('lang.invoice_no')</th>
                                                                <th>@lang('lang.number_po')</th>
                                                                <th>@lang('lang.number_piece_close')</th>
                                                                <th>@lang('lang.weigh')</th>
                                                                <th>@lang('lang.queue_size')</th>
                                                                <th>@lang('lang.price')</th>
                                                                <th>@lang('lang.import_warehouse_china')</th>
                                                                <th>@lang('lang.status')</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="list-delivery-po-products">

                                                        </tbody>
                                                        <tfooter>
                                                            <tr id="list-delivery-po-products-not-found">
                                                                <td colspan="12" style="text-align: center;">@lang('lang.data_not_found')</td>
                                                            </tr>

                                                        </tfooter>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row" style="margin-top: 40px;">
                                            <div class="col-md-12">
                                                <h5>@lang('lang.po_item_not_shipped')</h5>
                                                <div class="fix-scroll-dashboard">
                                                    <table class="table">
                                                        <thead>
                                                            <tr>
                                                                <th>
                                                                    <!-- <div class="form-check">
                                                                        <label for="edit_active" class="checkbox form-check-label">
                                                                            <span class="icons">
                                                                                <span class="first-icon fa fa-square"></span>
                                                                                <span class="second-icon fa fa-check-square "></span>
                                                                            </span><input type="checkbox" class="form-check-input" data-toggle="checkbox" id="check_active_all" required="" aria-required="true">
                                                                        </label>
                                                                    </div> -->
                                                                </th>
                                                                <th>@lang('lang.customer_id')</th>
                                                                <th>@lang('lang.delivery_date')</th>
                                                                <th>@lang('lang.invoice_no')</th>
                                                                <th>@lang('lang.number_po')</th>
                                                                <th>@lang('lang.number_piece_close')</th>
                                                                <th>@lang('lang.weigh')</th>
                                                                <th>@lang('lang.queue_size')</th>
                                                                <th>@lang('lang.price')</th>
                                                                <th>@lang('lang.date_of_entry_to_the_warehouse_china')</th>
                                                                <th>@lang('lang.status')</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="list-po-products">

                                                        </tbody>
                                                        <tfooter>
                                                            <tr id="list-po-products-not-found">
                                                                <td colspan="12" style="text-align: center;">@lang('lang.data_not_found')</td>
                                                            </tr>

                                                        </tfooter>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-primary">@lang('lang.save')</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </form>
    </div>
</div>
@endsection
@section('js_bottom')
<script src="{{asset('assets/global/plugins/orakuploader/orakuploader.js')}}"></script>
<script>
    $("#add_billing_date").daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        minYear: 1901,
        startDate: '{{date("Y-m-d")}}',
        // maxYear: parseInt(moment().format('YYYY'),10),
        locale: {
            format: 'YYYY-MM-DD'
        }
    }, function(start, end, label) {
            var years = moment().diff(start, 'years');
            // alert("You are " + years + " years old!");
    });

    $('body').on('change','#add_user_id',function(data){
        $('#list-delivery-po-products').html('');
        $('#list-po-products').html('');
        var user_id = $(this).val();
        var delivery_id = $('#edit_id').val();
        $.ajax({
            method : "GET",
            url : url_gb+"/admin/{{$lang}}/Billing/GetUserPo/"+user_id,
            dataType : 'json',
            data: {
                delivery_id: delivery_id,
            }
        }).done(function(rec){
            var html_delivery = '';
            var html = '';
            var status = '';
            var delivery_charge = '';
            $.each(rec.DeliverySlips, function( k, v ) {
                if(v.delivery_slip_status.id == 1){
                    status = '<span class="badge badge-warning">'+v.delivery_slip_status.name+'</span>';
                }else{
                    status = '<span class="badge badge-success">'+v.delivery_slip_status.name+'</span>';
                }

                if(v.delivery_charge_price > 0){
                    delivery_charge = addNumformat(v.delivery_charge_price.toFixed(2))
                }else{
                    delivery_charge = 'ฟรีค่าส่ง';
                }
                html += '<tr>\
                            <td>\
                            <div class="form-check">\
                                <label for="add_active" class="checkbox form-check-label label-check-po">\
                                    <span class="icons">\
                                        <span class="first-icon fa fa-square"></span>\
                                        <span class="second-icon fa fa-check-square"></span>\
                                    </span>\
                                    <input type="checkbox" class="form-check-input input-check-po" data-toggle="checkbox" name="billing_list[]" required="" value="'+v.id+'" aria-required="true">\
                                </label>\
                            </div>\
                            </td>\
                            <td>'+v.delivery_slip_no+'</td>\
                            <td>'+v.user.firstname+' '+v.user.lastname+'</td>\
                            <td>'+v.delivery.date_delivery+'</td>\
                            <td>'+addNumformat(v.total_qty)+'</td>\
                            <td>'+addNumformat(v.total_weight.toFixed(2))+'</td>\
                            <td>'+addNumformat(v.price.toFixed(2))+'</td>\
                            <td>'+delivery_charge+'</td>\
                            <td>'+addNumformat(v.total_price.toFixed(2))+'</td>\
                            <td>'+status+'</td>\
                        </tr>';

            });

            $.each(rec.DeliveryPos, function( k, v ) {
                if(v.status_id == 2){
                    status = '<span class="badge badge-warning" style="background-color: #364150;">'+v.status_name+'</span>';
                }else if(v.status_id == 3){
                    status = '<span class="badge badge-warning" stylebackground-color: #5bc0de;>'+v.status_name+'</span>';
                }else if(v.status_id == 4){
                    status = '<span class="badge badge-warning" stylebackground-color: #9C27B0;>'+v.status_name+'</span>';
                }else if(v.status_id == 5){
                    status = '<span class="badge badge-warning">'+v.status_name+'</span>';
                }else if(v.status_id == 6){
                    status = '<span class="badge badge-success">'+v.status_name+'</span>';
                }
                html_delivery += '<tr>\
                            <td>\
                            <div class="form-check">\
                                <label for="add_active" class="checkbox form-check-label label-check-delivery-po">\
                                    <span class="icons">\
                                        <span class="first-icon fa fa-square"></span>\
                                        <span class="second-icon fa fa-check-square"></span>\
                                    </span>\
                                    <input type="checkbox" class="form-check-input input-check-delivery-po" data-toggle="checkbox" name="billing_list[]" required="" value="'+v.delivery_pos_id+'" aria-required="true">\
                                </label>\
                            </div>\
                            </td>\
                            <td>'+v.customer_general_code+'</td>\
                            <td>'+v.delivery_slip_date+'</td>\
                            <td>'+v.delivery_no+'</td>\
                            <td>'+v.po_no+'</td>\
                            <td>'+addNumformat(v.qty)+'</td>\
                            <td>'+addNumformat(v.weight != null ? v.weight.toFixed(2) : 0)+'</td>\
                            <td>'+addNumformat(v.cubic != null ? addNumformat(v.cubic.toFixed(2)) : 0)+'</td>\
                            <td>'+addNumformat(v.price != null ? addNumformat(v.price.toFixed(2)) : '0.00')+'</td>\
                            <td>'+v.import_date +'</td>\
                            <td>'+status+'</td>\
                        </tr>';
            });
            if(rec.DeliveryPos.length > 0){
                $('#list-delivery-po-products').append(html_delivery);
                $('#list-delivery-po-products-not-found').hide();
            }else{
                $('#list-delivery-po-products-not-found').show();
            }

            $.each(rec.ImportToChainas, function( k, v ) {

                if(v.status_id == 2){
                    status = '<span class="badge badge-warning" style="background-color: #364150;">'+v.status_name+'</span>';
                }else if(v.status_id == 3){
                    status = '<span class="badge badge-warning" stylebackground-color: #5bc0de;>'+v.status_name+'</span>';
                }else if(v.status_id == 4){
                    status = '<span class="badge badge-warning" stylebackground-color: #9C27B0;>'+v.status_name+'</span>';
                }else if(v.status_id == 5){
                    status = '<span class="badge badge-warning">'+v.status_name+'</span>';
                }else if(v.status_id == 6){
                    status = '<span class="badge badge-success">'+v.status_name+'</span>';
                }

                // $check = '<div class="form-check">\
                //                     <label for="add_active" class="checkbox form-check-label label-check-po">\
                //                         <span class="icons">\
                //                             <span class="first-icon fa fa-square"></span>\
                //                             <span class="second-icon fa fa-check-square"></span>\
                //                         </span>\
                //                         <input type="checkbox" class="form-check-input input-check-po" data-toggle="checkbox" name="billing_list['+k+'][import_to_chaina_id]"  required="" value="'+v.import_to_chaina_id+'" aria-required="true">\
                //                     </label>\
                //                 </div>';

                html += '<tr>\
                            <td></td>\
                            <td>\
                                '+v.customer_general_code+'\
                            </td>\
                            <td style="text-align: center;">-</td>\
                            <td style="text-align: center;">-</td>\
                            <td>'+v.po_no+'</td>\
                            <td>'+addNumformat(v.qty_container)+'</td>\
                            <td>'+addNumformat(v.weight != null ? v.weight.toFixed(2) : 0)+'</td>\
                            <td>'+addNumformat(v.cubic != null ? addNumformat(v.cubic.toFixed(2)) : 0)+'</td>\
                            <td>'+addNumformat(v.price != null ? addNumformat(v.price.toFixed(2)) : '0.00')+'</td>\
                            <td>'+v.import_date +'</td>\
                            <td>'+status+'</td>\
                        </tr>';
            });
            if(rec.ImportToChainas.length > 0){
                $('#list-po-products').append(html);
                $('#list-po-products-not-found').hide();
            }else{
                $('#list-po-products-not-found').show();
            }

        }).fail(function(){
            swal("system.system_alert","@lang('lang.system_system_error')","error");
        });
    });

    $('body').on('change','#check_active_all',function(data){
        var active = $('#check_active_all').closest("label.checked");
        var input_check = $('#list-po-products').find('label.label-check-po');
        if(active.length > 0){
            $('.label-check-po').addClass('checked');
            $(".input-check-po").prop( "checked", true );
        }else{
            $('.label-check-po').removeClass('checked');
            $(".input-check-po").prop( "checked", false );
        }
    });

    $('body').on('change','#check_delivery_active_all',function(data){
        var active = $('#check_delivery_active_all').closest("label.checked");
        var input_check = $('#list-delivery-po-products').find('label.label-check-delivery-po');
        if(active.length > 0){
            $('.label-check-delivery-po').addClass('checked');
            $(".input-check-delivery-po").prop( "checked", true );
        }else{
            $('.label-check-delivery-po').removeClass('checked');
            $(".input-check-delivery-po").prop( "checked", false );
        }
    });

    $('#FormAddBilling').validate({
        errorElement: 'div',
        errorClass: 'invalid-feedback',
        focusInvalid: false,
        rules: {
            'billing[user_id]': {
                required: true,
            },

        },
        messages: {
            'billing[user_id]': {
                required: '@lang("lang.please_specify")',
            },

        },
        highlight: function (e) {
            validate_highlight(e);
        },
        success: function (e) {
            validate_success(e);
        },

        errorPlacement: function (error, element) {
            validate_errorplacement(error, element);
        },
        submitHandler: function (form) {
            /*
            if(CKEDITOR!==undefined){
                for ( instance in CKEDITOR.instances ){
                    CKEDITOR.instances[instance].updateElement();
                }
            }
            */
            var btn = $(form).find('[type="submit"]');
            var delivery_id = $('#edit_id').val();
            btn.button("loading");
            $.ajax({
                method : "post",
                url : url_gb+"/admin/{{$lang}}/Billing",
                dataType : 'json',
                data : $(form).serialize()
            }).done(function(rec){
                btn.button("reset");
                if(rec.status==1){
                    resetFormCustom(form);
                    swal(rec.title,rec.content,"success");
                    location.reload();
                   // window.location.href = url_gb+"/admin/"+$lang+"/Billing";
                    //$('#ModalEdit').modal('hide');
                }else{
                    swal(rec.title,rec.content,"error");
                }
            }).fail(function(){
                swal("system.system_alert","@lang('lang.system_system_error')","error");
                btn.button("reset");
            });
        },
        invalidHandler: function (form) {

        }
    });

    $('#add_user_id').select2();
    $('#add_user_address_id').select2();
</script>
@endsection
