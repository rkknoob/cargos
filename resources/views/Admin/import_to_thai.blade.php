﻿@extends('Admin.layouts.layout')
@section('css_bottom')
<style>
    .fix-scroll-dashboard {
        width: 100%;
        overflow-x: scroll;
    }   
</style>
@endsection
@section('body')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="content">
                        <h4 class="title">
                            {{$title_page or '' }}
                            <a href="{{ url('/admin/'.$lang.'/ImportToThai/Create') }}" class="btn btn-success btn-add pull-right" >
                                + <i class="fa fa-desktop" style="font-size: 20px;"></i> @lang('lang.import_product_thai')
                            </a>
                        </h4>
                        <h4 class="title">
                            <a href="{{ url('/admin/'.$lang.'/ImportToThai/Mobile/Create') }}" class="btn btn-success btn-add pull-right" >
                                + <i class="fa fa-mobile" style="font-size: 25px;"></i> @lang('lang.import_product_thai')&nbsp&nbsp
                            </a>
                        </h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <div class="material-datatables fix-scroll-dashboard">
                            <div class="table-responsive">
                                <table id="TableList" class="table table-striped table-no-bordered table-hover" style="width:100%;cellspacing:0">
                                    <thead>
                                        <!-- <tr>
                                            <th>เลขตู้</th>
                                            <th>ประเภทการขนส่ง</th>
                                            <th>ประเภทตู้</th>
                                            <th>รหัสตู้จริง</th>
                                            <th>น้ำหนักรวม</th>
                                            <th>จำนวนคิว</th>
                                            <th>วันที่บันทึก</th>
                                            <th>สถานะ</th>
                                            <th>หมายเหตุ</th>
                                            <th>จำนวนตาม PO</th>
                                            <th>จำนวนตามจริง</th>
                                            <th>จำนวนที่ขาด</th>
                                            <th>เครื่องมือ</th>
                                        </tr> -->
                                        <tr>
                                            <th>@lang('lang.container_no')</th>
                                            <th>@lang('lang.tatal_weigth')</th>
                                            <th>@lang('lang.total_size')</th>
                                            <th>@lang('lang.total_queue')</th>
                                            <th>@lang('lang.date_of_container_export_china')</th>
                                            <th>@lang('lang.delivery_deadline')</th>
                                            <th>@lang('lang.status')</th>
                                            <th>@lang('lang.number_of_po')</th>
                                            <th>@lang('lang.number_of_container')</th>
                                            <th>@lang('lang.number_of_warehouse_in_thai')</th>
                                            <th>@lang('lang.missing_from_cabinet_warehouse_thailand')</th>
                                            <!-- <th>หมายเหตุ</th> -->
                                            <th style="width: 100px;">@lang('lang.tool')</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('modal')
<div class="modal" id="ModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form id="FormAdd">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">เพิ่ม {{$title_page or 'ข้อมูลใหม่'}}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">

                <div class="form-group">
                    <label for="add_container_id">@lang('lang.container_id')</label>
                    <select name="container_id" class="select2 form-control" tabindex="-1" data-placeholder="Select container_id" id="add_container_id" required="" >
                        <option value="">Select container_id</option>
                        @foreach($Containers as $Container)
                        <option value="{{$Container->id}}">{{$Container->container_code}}</option>
                        @endforeach
                    </select>
                </div>

                <label for="add_date_import">date_import</label>
                <div class="input-group" data-date="{{date('Y-m-d')}}">
                    <input type="text" value="" readonly="readonly" class="form-control form_date_time" name="date_import" id="add_date_import" required="" placeholder="date_import">
                    <span class="input-group-addon remove_date_time"><i class="glyphicon glyphicon-remove icon-remove"></i></span>
                    <span class="input-group-addon trigger_date_time" for="add_date_import"><i class="glyphicon glyphicon-calendar icon-calendar"></i></span>
                </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">บันทึก</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" id="ModalDetail" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <input type="hidden" name="edit_id" id="edit_id">
            <!-- <form id="FormEdit"> -->
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">รายละเอียดข้อมูล {{$title_page or 'ข้อมูลใหม่'}}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="show_po_no">เลข PO</label>
                            <input type="text" class="form-control" id="show_po_no">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="show_customer_code">รหัสลูกค้า</label>
                            <input type="text" class="form-control" id="show_customer_code">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="show_customer_name">ชื่อลูกค้า</label>
                            <input type="text" class="form-control" id="show_customer_name">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <table class="table table-bordered table-lg mt-lg mb-0">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>ชื่อร้านค้าจีน</th>
                                <th>ประเภท</th>
                                <th>รหัสสินค้า</th>
                                <th>ชื่อสินค้า</th>
                                <th>จำนวน</th>
                                <th>น้ำหนัก</th>
                                <th>เดินทางโดย</th>
                                <th>รายละเอียด</th>
                            </tr>
                        </thead>
                        <tbody id="list-po-products">

                        </tbody>
                    </table>
                </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">บันทึก</button>
                </div>
            <!-- </form> -->
        </div>
    </div>
</div>
@endsection
@section('js_bottom')
<script src="{{asset('assets/global/plugins/orakuploader/orakuploader.js')}}"></script>
<script>

     var TableList = $('#TableList').dataTable({
        "ajax": {
            "url": url_gb+"/admin/{{$lang}}/ImportToThai/Lists",
            "data": function ( d ) {
                //d.myKey = "myValue";
                // d.custom = $('#myInput').val();
                // etc
            }
        },
        "order": [[ 4, "desc" ]],
        "columns": [
            {"data" : "container_code"},
            {"data" : "weight"},
            {"data" : "size", "className":"action text-center", "searchable" : false , "orderable" : false},
            {"data" : "cubic"},
            {"data" : "created_at"},
            {"data" : "created_at"},
            {"data" : "status", "className":"action text-center", "searchable" : false , "orderable" : false},
            {"data" : "qty_po", "searchable" : false},
            {"data" : "rel_container_product_count", "className":"action text-center", "searchable" : false , "orderable" : false},
            {"data" : "qty_product_real", "searchable" : false},
            {"data" : "amount_missing", "searchable" : false, "orderable" : false},
            // {"data" : "remark"},
            { "data": "action","className":"action text-center", "searchable" : false , "orderable" : false }
        ]
    });
    $('body').on('click','.btn-add',function(data){
        //ShowModal('ModalAdd');
    });

    // $('body').on('click','.btn-detail',function(data){
    //     var btn = $(this);
    //     btn.button('loading');
    //     var id = $(this).data('id');
    //     $('#edit_id').val(id);
    //     $.ajax({
    //         method : "GET",
    //         url : url_gb+"/admin/ImportToThai/"+id,
    //         dataType : 'json'
    //     }).done(function(rec){
    //         console.log(rec);
    //         var html = '';
    //         $('#show_po_no').val(rec.import_to_chaina.po_no);
    //         $('#show_customer_code').val(rec.import_to_chaina.user.customer_general_code);
    //         $('#show_customer_name').val(rec.import_to_chaina.user.firstname+' '+rec.import_to_chaina.user.lastname);
    //
    //         $.each(rec.product_import_to_thai, function(k, v) {
    //             html += '<tr>\
    //                 <td>'+(k+1)+'</td>\
    //                 <td>'+v.qr_code_product.product_import_to_chaina.shop_chaina_name+'</td>\
    //                 <td>'+v.qr_code_product.product_import_to_chaina.product.product_type.code+'</td>\
    //                 <td>'+v.qr_code_product.product_import_to_chaina.product.code+'</td>\
    //                 <td>'+v.qr_code_product.product_import_to_chaina.product.name+'</td>\
    //                 <td>'+addNumformat(v.qr_code_product.product_import_to_chaina.qty)+'</td>\
    //                 <td>'+addNumformat(v.qr_code_product.product_import_to_chaina.weight_all)+'</td>\
    //                 <td>'+v.qr_code_product.product_import_to_chaina.transport_type.name+'</td>\
    //                 <td>'+v.qr_code_product.product_import_to_chaina.detail+'</td>\
    //             </tr>';
    //         });
    //
    //         $('#list-po-products').append(html);
    //
    //         btn.button("reset");
    //         ShowModal('ModalDetail');
    //     }).fail(function(){
    //         swal("system.system_alert","system.system_error","error");
    //         btn.button("reset");
    //     });
    // });

    $('#FormAdd').validate({
        errorElement: 'div',
        errorClass: 'invalid-feedback',
        focusInvalid: false,
        rules: {

            container_id: {
                required: true,
            },
            date_import: {
                required: true,
            },
        },
        messages: {

            container_id: {
                required: "กรุณาระบุ",
            },
            date_import: {
                required: "กรุณาระบุ",
            },
        },
        highlight: function (e) {
            validate_highlight(e);
        },
        success: function (e) {
            validate_success(e);
        },

        errorPlacement: function (error, element) {
            validate_errorplacement(error, element);
        },
        submitHandler: function (form) {
            /*
            if(CKEDITOR!==undefined){
                for ( instance in CKEDITOR.instances ){
                    CKEDITOR.instances[instance].updateElement();
                }
            }
            */
            var btn = $(form).find('[type="submit"]');
            var data_ar = removePriceFormat(form,$(form).serializeArray());
            btn.button("loading");
            $.ajax({
                method : "POST",
                url : url_gb+"/admin/ImportToThai",
                dataType : 'json',
                data : $(form).serialize()
            }).done(function(rec){
                btn.button("reset");
                if(rec.status==1){
                    TableList.api().ajax.reload();
                    resetFormCustom(form);
                    swal(rec.title,rec.content,"success");
                    $('#ModalAdd').modal('hide');
                }else{
                    swal(rec.title,rec.content,"error");
                }
            }).fail(function(){
                swal("system.system_alert","system.system_error","error");
                btn.button("reset");
            });
        },
        invalidHandler: function (form) {

        }
    });

    $('#FormEdit').validate({
        errorElement: 'div',
        errorClass: 'invalid-feedback',
        focusInvalid: false,
        rules: {

            container_id: {
                required: true,
            },
            date_import: {
                required: true,
            },
        },
        messages: {

            container_id: {
                required: "กรุณาระบุ",
            },
            date_import: {
                required: "กรุณาระบุ",
            },
        },
        highlight: function (e) {
            validate_highlight(e);
        },
        success: function (e) {
            validate_success(e);
        },

        errorPlacement: function (error, element) {
            validate_errorplacement(error, element);
        },
        submitHandler: function (form) {
            /*
            if(CKEDITOR!==undefined){
                for ( instance in CKEDITOR.instances ){
                    CKEDITOR.instances[instance].updateElement();
                }
            }
            */
            var btn = $(form).find('[type="submit"]');
            var id = $('#edit_id').val();
            btn.button("loading");
            $.ajax({
                method : "POST",
                url : url_gb+"/admin/ImportToThai/"+id,
                dataType : 'json',
                data : $(form).serialize()
            }).done(function(rec){
                btn.button("reset");
                if(rec.status==1){
                    TableList.api().ajax.reload();
                    resetFormCustom(form);
                    swal(rec.title,rec.content,"success");
                    $('#ModalEdit').modal('hide');
                }else{
                    swal(rec.title,rec.content,"error");
                }
            }).fail(function(){
                swal("system.system_alert","system.system_error","error");
                btn.button("reset");
            });
        },
        invalidHandler: function (form) {

        }
    });

    $('body').on('click','.btn-delete',function(e){
        e.preventDefault();
        var btn = $(this);
        var id = btn.data('id');
        swal({
            title: "คุณต้องการลบใช่หรือไม่",
            text: "หากคุณลบจะไม่สามารถเรียกคืนข้อมูลกลับมาได้",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: "ใช่ ฉันต้องการลบ",
            cancelButtonText: "ยกเลิก",
            showLoaderOnConfirm: true,
            buttonsStyling: false
        }).then(function() {
            $.ajax({
                method : "POST",
                url : url_gb+"/admin/ImportToThai/Delete/"+id,
                data : {ID : id}
            }).done(function(rec){
                if(rec.status==1){
                    swal(rec.title,rec.content,"success");
                    TableList.api().ajax.reload();
                }else{
                    swal("ระบบมีปัญหา","กรุณาติดต่อผู้ดูแล","error");
                }
            }).fail(function(data){
                swal("ระบบมีปัญหา","กรุณาติดต่อผู้ดูแล","error");
            });
        }).catch(function(e){
            //console.log(e);
        });
    });

    $('#add_container_id').select2();
$('#edit_container_id').select2();

</script>
@endsection
