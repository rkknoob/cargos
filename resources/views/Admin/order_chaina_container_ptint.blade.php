        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>ใบ Order / รายการสินค้า</title>
        <style>
            @font-face {
                font-family: 'SentyTang';
                font-style: normal;
                font-weight: normal;
                src: url("{{ asset('fonts/SentyTang.ttf') }}") format('truetype');
            }

            body {
                font-family: 'SentyTang';
            }
            .table_css {
                height: 25px;
                font-size: 14px;
                width: 800px;
            }
            .table_css td {
                height: 25px;
                font-size: 14px;
            }
            .table_css th {
                font-weight: normal;
                font-size: 14px;
            }

        </style>
    </head>
    <body lang="th">
        <htmlpageheader name="page-header">

        </htmlpageheader>

        <htmlpagebody>
            <div class="row">
                <div style="color: black; text-align: left; font-size: 13px; ">

                </div>
            </div>
            <div class="row" style="padding-top: 20px;">
                <div style="color: black; text-align: center; font-size: 16px; ">
                    <b>ใบ Order</b>
                </div>
            </div>
            <div class="row" style="padding-top: 20px;">
                <table class="table_css">
                    <tr>
                        <td colspan="3" style="text-align: right; width: 700px;"><b>รหัสตู้: {{ $Container->container_code }}</b></td>
                    </tr>
                    <tr>
                        <td colspan="3" style="text-align: right;"><b>วันที่ปิดตู้: {{ $Container->created_at }}</b></td>
                    </tr>
                </table>
            </div>

            <div class="row" style="padding-top: 20px;">
                <table class="table_css" border="1" cellspacing="0">
                    <tr>
                        <th>รหัสลูกค้า件数</th>
                        <th>จำนวน</th>
                        <th>KG รวม</th>
                        <th>CBM รวม</th>
                        <th>จำนวน  PO</th>
                        <th>จำนวนรวม</th>
                    </tr>
                    @if(! $ImportToChainas->isEmpty())
                        @php

                        @endphp
                        @foreach($ImportToChainas as $key => $ImportToChaina)
                            @php
                                $cubil = 0;
                                if(!empty($ImportToChaina->product_amount_real)){
                                    $cubil = $ImportToChaina->product_cubic * $ImportToChaina->product_amount_real;
                                }
                            @endphp
                            <tr>
                                <td>{{  $ImportToChaina->customer_general_code }}</td>
                                <td style="text-align: right;">{{ number_format($ImportToChaina->product_amount) }}</td>
                                <td style="text-align: right;">{{ number_format($ImportToChaina->product_KG,1) }}</td>
                                <td style="text-align: right;">{{ number_format($cubil, 3) }}</td>
                                <td style="text-align: right;">{{ number_format($ImportToChaina->po_amount) }}</td>
                                <td style="text-align: right;">{{ number_format($ImportToChaina->product_amount_real) }}/{{ number_format($ImportToChaina->product_amount) }}</td>
                            </tr>
                        @endforeach
                    @endif
                </table>
                <br>
                <table class="table_css" border="1" cellspacing="0">
                    <tr>
                        <th>ลำดับ</th>
                        <th>รหัสลูกค้า</th>
                        <th>เลขที่ PO</th>
                        <th>รายการ</th>
                        <th>จำนวน</th>
                        <th>KG</th>
                        <th>CBM</th>
                    </tr>
                    @if(! $QrCodeProducts->isEmpty())
                        @php
                            $total_qty_container = 0;
                            $total_weight_all = 0;
                            $total_cubic = 0;
                            $amount = 0;
                            $subtotal = 0;
                            $total = 0;
                        @endphp
                        @foreach($QrCodeProducts as $key => $QrCodeProduct)
                            @php
                                $cubil = 0;
                                if(!empty($QrCodeProduct->product_amount)){
                                    $cubil = $QrCodeProduct->cubic_item * $QrCodeProduct->product_amount;
                                }
                                $subtotal = $QrCodeProduct->rate_price * $cubil;
                                $amount = $amount + $QrCodeProduct->product_amount;
                                $total_cubic += $cubil;
                                $total_weight_all += $QrCodeProduct->product_weight_per_item;
                                $total += $subtotal;
                                $rate_type_price = '';
                                if($QrCodeProduct->rel_user_type_product_id_old != null){
                                    $rate_type_price = \App\Models\RelUserTypeProduct::where('id', $QrCodeProduct->rel_user_type_product_id_old)->first();
                                }

                            @endphp
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ $QrCodeProduct->customer_general_code }}</td>
                                <td>{{ $QrCodeProduct->po_no }}</td>
                                <td>{{ $QrCodeProduct->product_name }}</td>
                                <td style="text-align: right;">{{ number_format($QrCodeProduct->product_amount) }}</td>
                                <td style="text-align: right;">{{ number_format($QrCodeProduct->product_weight_per_item,1) }}</td>
                                <td style="text-align: right;">{{ number_format($cubil,3) }}</td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="4" style="text-align: center;"><b>Total</b></td>
                            <td style="text-align: right;">{{ number_format($amount) }}</td>
                            <td style="text-align: right;">{{ number_format($total_weight_all,1) }}</td>
                            <td style="text-align: right;">{{ number_format($total_cubic,3) }}</td>
                        </tr>
                    @endif
                </table>
            </div>



        </htmlpagebody>

        <htmlpagefooter name="page-footer">
            <div style="padding-bottom: 40px;text-align: center;">

            </div>
        </htmlpagefooter>
