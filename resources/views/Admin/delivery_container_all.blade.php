﻿@extends('Admin.layouts.layout')
@section('css_bottom')
<style>
.fix-scroll-dashboard {
    width: 100%;
    overflow-x: scroll;
}
</style>
@endsection
@section('body')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="content">
                        <h4 class="title">
                            {{$title_page or '' }}
                            <a href="{{ url('/admin/'.$lang.'/DeliveryContainer/Create') }}" class="btn btn-success btn-add pull-right" >
                                + @lang('lang.create_data')
                            </a>
                        </h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <div class="material-datatables fix-scroll-dashboard">
                            <div class="table-responsive">
                                <table id="TableList" class="table table-striped table-no-bordered table-hover" style="width:100%;cellspacing:0">
                                    <thead>
                                        <tr>
                                            <th>@lang('lang.container_no')</th>
                                            <th>@lang('lang.delivery_date')</th>
                                            <th>@lang('lang.tool')</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('modal')
<div class="modal" id="ModalDelivery" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <input type="hidden" id="edit_dalivery_by_container_id">
            <form id="FormAddressShipping">
                <div class="modal-header"><h4>@lang('lang.manage_address')</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="content" id="show-address-shipping" style="text-align: center;">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-check">
                        <label for="edit_user_address_id">เลือกที่อยู่จัดส่ง</label>
                        <select name="user_address_id" class="select2 form-control" tabindex="-1" data-placeholder="เลือกที่อยู่จัดส่ง" id="edit_user_address_id" >

                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">@lang('lang.save')</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('js_bottom')
<script src="{{asset('assets/global/plugins/orakuploader/orakuploader.js')}}"></script>
<script>

    var TableList = $('#TableList').dataTable({
        "ajax": {
            "url": url_gb+"/admin/{{$lang}}/DeliveryContainer/Lists",
            "data": function ( d ) {
                //d.myKey = "myValue";
                // d.custom = $('#myInput').val();
                // etc
            }
        },
        //"order": [[ 3, "desc" ]],
        "columns": [
            {"data" : "container_code", "name": "containers.container_code"},
            {"data" : "date_delivery"},
            { "data": "action","className":"action text-center","searchable" : false , "orderable" : false }
        ]
    });

    $('body').on('click','.btn-delivery',function(data){
        var id = $(this).data('id');
        $('#edit_dalivery_by_container_id').val(id);
        ShowModal('ModalDelivery');
    });

</script>
@endsection
