<table>
    <tr>
        <td colspan="3"><b>บิลเลขที่:</b></td>
        <td><b>{{ $DaliverySheet->delivery_no }}</b></td>
    </tr>
    <tr>
        <td colspan="3"><b>Date:</b></td>
        <td><b>{{ $DaliverySheet->delivery_slip_date }}</b></td>
    </tr>
    <tr>
        <td colspan="3"><b>ID:</b></td>
        <td><b>{{ $DaliverySheet->customer_general_code }}</b></td>
    </tr>
    <tr>
        <td><b>Customer Name:</b></td>
        <td><b>{{ $DaliverySheet->firstname }} {{ $DaliverySheet->lastname }}</b></td>
        <td><b>Mobile:</b></td>
        <td><b>{{ $DaliverySheet->main_mobile }}</b></td>
    </tr>
    <tr>
        <td><b>Address:</b></td>
        <td colspan="3">
            <b>
                {{ $DaliverySheet->address }}
                {{ $DaliverySheet->amphure_name }}
                {{ $DaliverySheet->province_name }}
                {{ $DaliverySheet->zipcode }}
            </b>
        </td>
    </tr>
    <tr>
        <td><b>สถานะ:</b></td>
        <td colspan="3">
            <b>
                {{ $DaliverySheet->status_payment == 'T' ? 'ชำระเสร็จเรียบร้อย' : 'ยังไม่มีการชำระ' }}
            </b>
        </td>
    </tr>
</table>

<table border="1" cellspacing="0">
    <tr>
        <td colspan="5"><b>รายการสินค้า</b></td>
        <td colspan="6"><b>ค่าขนส่ง (บาท)</b></td>
    </tr>
    <tr>
        <td rowspan="2"><b>No.<br>ลำดับ</b></td>
        <td rowspan="2"><b>ID<br>รหัสลูกค้า</b></td>
        <td rowspan="2"><b>PO No.<br>เลขที่บิล</b></td>
        <td rowspan="2"><b>Item<br>เลขกล่อง</b></td>
        <td rowspan="2"><b>Description<br>รายการ</b></td>
        <td colspan="2"><b>Weight</b></td>
        <td><b></b></td>
        <td rowspan="2"><b>Rate<br>เรท (THB)</b></td>
        <td rowspan="2"><b>Amount<br>รวม</b></td>
        <td rowspan="2"><b>จำนวน </b></td>
    </tr>
    <tr>
        <td><b>KG</b></td>
        <td><b>CBM</b></td>
        <td><b>เรทอื่น</b></td>
    </tr>
    @php
        $price = 0;
        $lot_id = '';
    @endphp
    @if(!$ProductLists->isEmpty())
        @foreach($ProductLists as $key => $ProductList)
            @php
                $weight_all = 0;
                if($lot_id != $ProductList->lot_id){
                    $weight_all = $ProductList->weight_all;
                    $lot_id = $ProductList->lot_id;
                }

                $cubic = ($ProductList->cubic / $ProductList->lot_product_qty);

                if($ProductList->rel_user_type_product_id_old != null){
                    $RelUserTypeProduct = \App\Models\RelUserTypeProduct::find($ProductList->rel_user_type_product_id_old);
                    $rate_price_old = $RelUserTypeProduct->price;
                }else{
                    $rate_price_old = 0;
                }

                if($ProductList->rel_user_type_product_id != null){
                    $RelUserTypeProduct = \App\Models\RelUserTypeProduct::find($ProductList->rel_user_type_product_id);
                    $rate_price = $RelUserTypeProduct->price;
                }else{
                    $rate_price = 0;
                }

                $price = ($weight_all * $rate_price);

                if($weight_all != 0){
                    if($rate_price != 0){
                        $show_rate = number_format($rate_price,2);
                    }else{
                        $show_rate = 'NO';
                    }
                }else{
                    $show_rate = '';
                }

            @endphp
            <tr>
                <td>{{ number_format($key+1) }}</td>
                <td>{{ $ProductList->customer_general_code }}</td>
                <td>{{ $ProductList->po_no }}</td>
                <td>{{ number_format($ProductList->item) }}</td>
                <td>{{ $ProductList->product_name }}</td>
                <td>{{ $weight_all != 0 ? number_format($weight_all,2) : null }}</td>
                <td>{{ $weight_all != 0 ? number_format($cubic) : null }}</td>
                <td>{{ $rate_price_old != 0 ? $rate_price_old : 'NO' }}</td>
                <td>{{ $show_rate }}</td>
                <td>{{ $weight_all != 0 ? number_format($price,2) : null }}</td>
                <td>1</td>
            </tr>
        @endforeach

        <tr>
            <td></td>
            <td><b>รวม</b></td>
            <td></td>
            <td></td>
            <td></td>
            <td>{{ number_format($DaliverySheet->total_weight) }}</td>
            <td>{{ number_format($DaliverySheet->total_cubic) }}</td>
            <td></td>
            <td></td>
            <td>{{ number_format($DaliverySheet->total_price,2) }}</td>
            <td>{{ number_format($DaliverySheet->total_qty) }}</td>
        </tr>
    @endif
</table>
<table border="0">
    <tr>
        <td colspan="3">ค่าใช้จ่ายอื่นๆ: ไม่มี</td>
    </tr>
    <tr>
        <td><b>รวม:</b></td>
        <td><b><u>{{ number_format($DaliverySheet->total_price, 2) }}</u></b></td>
        <td></td>
    </tr>
    <tr>
        <td><b>ค่าจัดส่ง:</b></td>
        <td><b><u></u></b></td>
        <td></td>
    </tr>
    <tr>
        <td><b>รวมสุทธิ:</b></td>
        <td><b><u>{{ number_format($DaliverySheet->total_price , 2) }}</u></b></td>
        <td></td>
    </tr>
</table>
<table border="0">
    <tr>
        <td colspan="2">หมายเหตุ</td>
    </tr>
    <tr>
        <td></td>
        <td>1. ทางบริษัทขอสงวนสิทธ์คิดค่าขนส่ง หากขนาดหรือน้ำหนักของสินค้าราคาค่าขนส่งต่ำกว่า 5,000 บาท/ครั้ง<br>ทางบริษัทจะคิดว่าค่าบริการจัดส่ง 500 บาท</td>
    </tr>
    <tr>
        <td></td>
        <td>2. ถ้าหากลูกค้ายินยอมให้สะสมสินค้าครบ 5,000 บาท บริษัทจะบริการจัดส่งฟรี</td>
    </tr>
    <tr>
        <td></td>
        <td>3. หากยอดรวมไม่ถึง 1,500 บาท บริษัทขอสงวนสิทธิ์ในการคิดค่าบริการขั้นต่ำที่ 1,500 บาท</td>
    </tr>
    <tr>
        <td></td>
        <td>4. หากสินค้ามีปัญหา กรุณาแจ้งบริษัท ภายใน 3 วัน มิฉะนั้นทางบริษัทจะไม่รับผิดชอบ</td>
    </tr>
</table>
<table border="0">
    <tr>
        <td>ลงชื่อ.......................................................ผู้ส่งสินค้า</td>
        <td>ลงชื่อ.......................................................ผู้รับสินค้า</td>
    </tr>
    <tr>
        <td>วันที่ ........../........../..........</td>
        <td>วันที่ ........../........../..........</td>
    </tr>
</table>