﻿@extends('Admin.layouts.layout')
@section('css_bottom')
<style>
.fix-scroll-dashboard {
    width: 100%;
    overflow-x: scroll;
}
</style>
@endsection
@section('body')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="content">
                        <h4 class="title">
                        </h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <div class="material-datatables fix-scroll-dashboard">
                            <div class="table-responsive">
                                <table id="tableSummarizeContainer" class="table table-striped table-hover" style="width:100%;">
                                    <thead>
                                        <tr>
                                            <th>@lang('lang.type_delivery')</th>
                                            <th>@lang('lang.product_number')</th>
                                            <th>@lang('lang.tatal_weigth')</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($SummarizeContainer as $sum_container)
                                            <tr>
                                                <td>{{$sum_container->{'transport_type_name_'.$lang} }}</td>
                                                <td>{{$sum_container->amount_product}}</td>
                                                <td>{{$sum_container->weight_product}}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="content">
                        <h4 class="title">
                            {{$title_page or '' }}
                            <a href="{{url('admin/'.$lang.'/ImportToChaina/create')}}" class="btn btn-success btn-add pull-right">
                                + @lang('lang.create_data')
                            </a>
                        </h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <div class="material-datatables">
                            <div class="table-responsive" style="overflow-x: scroll;">
                                <table id="TableList" class="table table-striped table-hover" style="width:100%;">
                                    <thead>
                                        <tr>
                                            <th>@lang('lang.no')</th>
                                            <th>@lang('lang.customer_id')</th>
                                            <th>@lang('lang.number_po')</th>
                                            <th>@lang('lang.date_and_time_import')</th>
                                            <th>@lang('lang.quantity')</th>
                                            <th>@lang('lang.weight_kg')</th>
                                            <th>@lang('lang.cbm')</th>
                                            <th>@lang('lang.status_weigh')</th>
                                            <!-- <th>@lang('lang.importtochinastatus')</th> -->
                                            <th>@lang('lang.type_delivery')</th>
                                            <th>@lang('lang.type_delivery')</th>
                                            <th>@lang('lang.type_delivery')</th>
                                            <th>@lang('lang.type_delivery')</th>
                                            <th>@lang('lang.note')</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('modal')
<div class="modal" id="ModalDetail" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">@lang('lang.detail')</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>@lang('lang.type_product')</th>
                            <!-- <th>@lang('lang.product_id')</th> -->
                            <th>@lang('lang.product_name')</th>
                            <th>@lang('lang.queue_size')</th>
                            <th>@lang('lang.quantity')</th>
                            <th>@lang('lang.weigh')</th>
                            <th>@lang('lang.travel_by')</th>
                            <th>@lang('lang.detail')</th>
                        </tr>
                    </thead>
                    <tbody id="list-products">

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger" class="close" data-dismiss="modal" aria-label="Close">@lang('lang.close')</button>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js_bottom')
<script src="{{asset('assets/global/plugins/orakuploader/orakuploader.js')}}"></script>
<script>

    var TableList = $('#TableList').dataTable({
        "ajax": {
            "type": "POST",
            "url": url_gb+"/admin/{{$lang}}/ImportToChaina/Lists",
            "data": function ( d ) {
                //d.myKey = "myValue";
                // d.custom = $('#myInput').val();
                // etc
            }
        },
        "order": [[ 3, "desc" ]],
        "columns": [
            {"data": "DT_Row_Index", "className": "text-center", "searchable": false, "orderable": false},
            {"data": "customer_general_code", "name": "users.customer_general_code"},
            {"data": "po_no"},
            {"data": "import_date", "searchable" : false},
            {"data": "qty_all_PO", "className":"action text-center", "searchable" : false},
            {"data": "weight_all_PO", "className":"action text-right", "searchable" : false},
            {"data": "cubic_all_PO", "className":"action text-right", "searchable" : false},
            {"data": "status_weight", "searchable" : false},
            // {"data": "status_id", "searchable" : false},
            {"data": "type_delivery", "name": "transport_types.name_th", "searchable": false},
            {"data": "type_delivery", "name": "transport_types.name_th", "visible" : false},
            {"data": "type_delivery", "name": "transport_types.name_en", "visible" : false},
            {"data": "type_delivery", "name": "transport_types.name_ch", "visible" : false},
            {"data": "remark", "searchable" : false},
            {"data": "action", "className":"action text-center", "searchable" : false, "orderable" : false}
        ],
        "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
            $('td', nRow).css('background-color', 'rgb(255, 255, 255)');
            var import_date = new Date(aData.import_date);
            var date_now = new Date('{{date("Y-m-d H:i:s")}}');
            var hour_24 = 24*(3600*1000);
            var hour_36 = 36*(3600*1000);
            var hour = date_now.getTime() - import_date.getTime();
            if (hour >=  hour_36){
                $('td', nRow).css('background-color', 'rgb(255, 210, 210)');
            }else if ( hour >=  hour_24){
                $('td', nRow).css('background-color', 'rgb(210, 210, 255)');
            }
        },
    });

    $('body').on('click','.btn-delete',function(e){
        e.preventDefault();
        var btn = $(this);
        var id = btn.data('id');
        swal({
            title: "@lang('lang.do_you_want_to_delete')",
            text: "@lang('lang.if_you_delete_you_will_not_be_able_to_restore_it')้",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: "@lang('lang.yes_i_want_to_delete')",
            cancelButtonText: "@lang('lang.cancel')",
            showLoaderOnConfirm: true,
            buttonsStyling: false
        }).then(function() {
            $.ajax({
                method : "POST",
                url : url_gb+"/admin/ImportToChaina/Delete/"+id,
                data : {ID : id}
            }).done(function(rec){
                if(rec.status==1){
                    swal(rec.title,rec.content,"success");
                    TableList.api().ajax.reload();
                }else{
                    swal("ไม่สามารถลบได้","กรุณาติดต่อผู้ดูแล","error");
                }
            }).fail(function(data){
                swal("ไม่สามารถลบได้","กรุณาติดต่อผู้ดูแล","error");
            });
        }).catch(function(e){
            //console.log(e);
        });
    });

    $('body').on('click', '.btn-show_detail', function(){
        var id = $(this).data('id');
        $.ajax({
            method : "POST",
            url : url_gb+"/admin/ImportToChaina/showDetail",
            dataType: "json",
            data : {po_id : id}
        }).done(function(rec){
            console.log(rec);
            var html = '';
            if(rec.ProductImportToChainas){
                $.each(rec.ProductImportToChainas, function(k,v){
                    html += '\
                        <tr>\
                            <td>'+(k+1)+'</td>\
                            <td class="text-center">'+v.product_type_name+'</td>\
                            <!--<td class="text-center">'+( v.product_code ? v.product_code : '' )+'</td>-->\
                            <td class="text-center">'+v.product_name+'</td>\
                            <td>'+( v.lot_product_cubic ? parseFloat(v.lot_product_cubic).toFixed(2) : '' )+'</td>\
                            <td class="text-center">'+parseInt((parseInt(v.product_sort_end) - parseInt(v.product_sort_start)) + 1)+'</td>\
                            <td>'+( v.lot_product_weight_all ? parseFloat(v.lot_product_weight_all).toFixed(2) : '' )+'</td>\
                            <td class="text-center">'+v.transport_types_name_{{$lang}}+'</td>\
                            <td>'+( v.lot_product_remark ? v.lot_product_remark : '' )+'</td>\
                        </tr>\
                    ';
                });
            }else{
                html += '\
                    <tr>\
                        <td colspan="9" class="text-center">ไม่มีสินค้า</td>\
                    </tr>\
                ';
            }
            $('#list-products').html(html);
            $('#ModalDetail').modal('show');
        }).fail(function(data){
            swal("ไม่สามารถโชว์รายละเอียดได้","กรุณาติดต่อผู้ดูแล","error");
        });
    });
</script>
@endsection
