<table border="1" cellspacing="0" cellpadding="0"> 
    <tr>
        <th colspan="19">
            <h3>นิวคาร์โก้</h3>
         </th>
    </tr>
    <tr>
        <th colspan="19">
            <h5>104/8 แขวงศาลาธรรมสพน์ เขตทวีวัฒนา กรุงเทพมหานคร 10170</h5>
         </th>
    </tr>
    <tr>
        <th colspan="19">
            <h3>ใบวางบิล</h3>
         </th>
    </tr>
    <tr>
        <th>รหัสลูกค้า:</th>
        <th>{{ $Billing->customer_general_code }}</th>
        <th>เลขที่ใบวางบิล</th>
        <th>{{ $Billing->billing_no }}</th>
    </tr>
    <tr>
        <th>คุณ:</th>
        <th>{{ $Billing->firstname.' '.$Billing->lastname }}</th>
        <th>วันที่</th>
        <th>{{ $Billing->billing_date }}</th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th>เงื่อนไขการชำระ</th>
        <th></th>
    </tr>
</table>
    <table border="1" cellspacing="0">
        <tr>
            <th>No.</th>
            <th>เลขที่ใบส่งของ</th>
            <th>วันที่จัดส่ง</th>
            <th>เลข PO</th>
            <th>จำนวน</th>
            <th>น้ำหนัก</th>
            <th>ขนาดคิว</th>
            <th>สถานะ</th>
            <th>จำนวนเงิน</th>
        </tr>
        @php
            $accrued = 0;
        @endphp
        @if(! $BillingLists->isEmpty())\
            @foreach($BillingLists as $key => $BillingList)
                <tr>
                    <td>{{ $key+1 }}</td>
                    <td>{{ $BillingList->delivery_no }}</td>
                    <td>{{ $BillingList->delivery_slip_date }}</td>
                    <td>{{ $BillingList->po_no }}</td>
                    <td>{{ number_format($BillingList->qty) }}</td>
                    <td>{{ number_format($BillingList->weight,2) }}</td>
                    <td>{{ number_format($BillingList->cubic,1) }}</td>
                    <td>{{ $BillingList->status == 'T' ? 'ชำระแล้ว' : 'ยังไม่มีชำระ' }}</td>
                    <td>{{ number_format($BillingList->subtotal,2) }}</td>
                </tr>
                    @php
                        $accrued += $BillingList->subtotal;
                    @endphp
            @endforeach
            <tr>
                <th colspan="2">รวมเงินทั้งสิ้น</th>
                <th colspan="6">({{ m2t($accrued) }})</th>
                <th>{{ number_format($accrued, 2) }}</th>
            </tr>
        @endif
    </table>
    <table border="0" cellspacing="0">
        <tr>
            <th colspan="4">หมายเหตุ: {!! $Billing->remark !!}</th>
        </tr>
        <br><br><br><br>
        <tr>
            <th>ชื่อผู้รับวางบิล</th>
            <th>_________________________</th>
            <th colspan="2">ในนาม นิวคาร์โก้</th>
        </tr>
        <tr>
            <th>วันที่รับ</th>
            <th>_____/_____/_____</th>
            <th></th>
            <th></th>
        </tr>
        <tr>
            <th>วันที่นัดรับเช็ค</th>
            <th>_____/_____/_____</th>
            <th>ชื่อผู้วางบิล</th>
            <th>_________________________</th>
        </tr>
    </table>
                
            