﻿@extends('Admin.layouts.layout')
@section('css_bottom')
@endsection
@section('body')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="content">
                        <h4 class="title">
                            {{$title_page or '' }}
                            <button class="btn btn-success btn-add pull-right" >
                                + @lang('lang.create_data')
                            </button>
                        </h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <div class="material-datatables">
                            <div class="table-responsive">
                                <table id="TableList" class="table table-striped table-no-bordered table-hover" style="width:100%;cellspacing:0">
                                    <thead>
                                        <tr>
                                        <th>@lang('lang.name')</th>
                                        <th>@lang('lang.active')</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('modal')
<div class="modal" id="ModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="FormAdd">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">@lang('lang.create') {{$title_page or 'ข้อมูลใหม่'}}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">

                <div class="form-group">
                    <label for="add_name">@lang('lang.name') (@lang("lang.th"))</label>
                    <input type="text" class="form-control" name="name_th" id="add_name_th" required="" placeholder="@lang('lang.name')">
                </div>

                <div class="form-group">
                    <label for="add_name">@lang('lang.name') (@lang("lang.en"))</label>
                    <input type="text" class="form-control" name="name_en" id="add_name_en" required="" placeholder="@lang('lang.name')">
                </div>

                <div class="form-group">
                    <label for="add_name">@lang('lang.name') (@lang("lang.ch"))</label>
                    <input type="text" class="form-control" name="name_ch" id="add_name_ch" required="" placeholder="@lang('lang.name')">
                </div>

                <div class="form-check">
                    <label for="add_active" class="checkbox form-check-label">
                        <input type="checkbox" class="form-check-input" data-toggle="checkbox" name="active" id="add_active" required="" value="T" checked="checked"> @lang('lang.active')
                    </label>
                </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">@lang('lang.save')</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" id="ModalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <input type="hidden" name="edit_id" id="edit_id">
            <form id="FormEdit">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">@lang('lang.edit_data') {{$title_page or 'ข้อมูลใหม่'}}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">

                <div class="form-group">
                    <label for="edit_name">@lang('lang.name') (@lang("lang.th"))</label>
                    <input type="text" class="form-control" name="name_th" id="edit_name_th" required="" placeholder="name">
                </div>
                <div class="form-group">
                    <label for="edit_name">@lang('lang.name') (@lang("lang.en"))</label>
                    <input type="text" class="form-control" name="name_en" id="edit_name_en" required="" placeholder="name">
                </div>
                <div class="form-group">
                    <label for="edit_name">@lang('lang.name') (@lang("lang.ch"))</label>
                    <input type="text" class="form-control" name="name_ch" id="edit_name_ch" required="" placeholder="name">
                </div>

                <div class="form-check">
                    <label for="edit_active" class="checkbox form-check-label">
                        <input type="checkbox" class="form-check-input" data-toggle="checkbox" name="active" id="edit_active" required="" value="T"> @lang('lang.active')
                    </label>
                </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">@lang('lang.save')</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('js_bottom')
<script src="{{asset('assets/global/plugins/orakuploader/orakuploader.js')}}"></script>
<script>

     var TableList = $('#TableList').dataTable({
        "ajax": {
            "url": url_gb+"/admin/{{$lang}}/ContainerType/Lists",
            "data": function ( d ) {
                //d.myKey = "myValue";
                // d.custom = $('#myInput').val();
                // etc
            }
        },
        "columns": [
            {"data" : "name_{{$lang}}"},
            {"data" : "active"},
            { "data": "action","className":"action text-center","searchable" : false , "orderable" : false }
        ]
    });
    $('body').on('click','.btn-add',function(data){
        ShowModal('ModalAdd');
    });
    $('body').on('click','.btn-edit',function(data){
        var btn = $(this);
        btn.button('loading');
        var id = $(this).data('id');
        $('#edit_id').val(id);
        $.ajax({
            method : "GET",
            url : url_gb+"/admin/ContainerType/"+id,
            dataType : 'json'
        }).done(function(rec){
            $('#edit_name_th').val(rec.name_th);
            $('#edit_name_en').val(rec.name_en);
            $('#edit_name_ch').val(rec.name_ch);
            if(rec.active=='T'){
                $('#edit_active').prop('checked','checked').closest('label').addClass('checked');
            }else{
                $('#edit_active').removeAttr('checked').closest('label').removeClass('checked');
            }

            btn.button("reset");
            ShowModal('ModalEdit');
        }).fail(function(){
            swal("system.system_alert","system.system_error","error");
            btn.button("reset");
        });
    });

    $('#FormAdd').validate({
        errorElement: 'div',
        errorClass: 'invalid-feedback',
        focusInvalid: false,
        rules: {

            name_th: {
                required: true,
            },
            name_en: {
                required: true,
            },
            name_ch: {
                required: true,
            },
            
        },
        messages: {

            name_th: {
                required: "@lang('lang.please_specify')",
            },
            name_en: {
                required: "@lang('lang.please_specify')",
            },
            name_ch: {
                required: "@lang('lang.please_specify')",
            },
            
        },
        highlight: function (e) {
            validate_highlight(e);
        },
        success: function (e) {
            validate_success(e);
        },

        errorPlacement: function (error, element) {
            validate_errorplacement(error, element);
        },
        submitHandler: function (form) {
            /*
            if(CKEDITOR!==undefined){
                for ( instance in CKEDITOR.instances ){
                    CKEDITOR.instances[instance].updateElement();
                }
            }
            */
            var btn = $(form).find('[type="submit"]');
            var data_ar = removePriceFormat(form,$(form).serializeArray());
            btn.button("loading");
            $.ajax({
                method : "POST",
                url : url_gb+"/admin/ContainerType",
                dataType : 'json',
                data : $(form).serialize()
            }).done(function(rec){
                btn.button("reset");
                if(rec.status==1){
                    TableList.api().ajax.reload();
                    resetFormCustom(form);
                    swal(rec.title,rec.content,"success");
                    $('#ModalAdd').modal('hide');
                }else{
                    swal(rec.title,rec.content,"error");
                }
            }).fail(function(){
                swal("system.system_alert","system.system_error","error");
                btn.button("reset");
            });
        },
        invalidHandler: function (form) {

        }
    });

    $('#FormEdit').validate({
        errorElement: 'div',
        errorClass: 'invalid-feedback',
        focusInvalid: false,
        rules: {

            name_th: {
                required: true,
            },
            name_en: {
                required: true,
            },
            name_ch: {
                required: true,
            },
            
        },
        messages: {

            name_th: {
                required: "@lang('lang.please_specify')",
            },
            name_en: {
                required: "@lang('lang.please_specify')",
            },
            name_ch: {
                required: "@lang('lang.please_specify')",
            },
            
        },
        highlight: function (e) {
            validate_highlight(e);
        },
        success: function (e) {
            validate_success(e);
        },

        errorPlacement: function (error, element) {
            validate_errorplacement(error, element);
        },
        submitHandler: function (form) {
            /*
            if(CKEDITOR!==undefined){
                for ( instance in CKEDITOR.instances ){
                    CKEDITOR.instances[instance].updateElement();
                }
            }
            */
            var btn = $(form).find('[type="submit"]');
            var id = $('#edit_id').val();
            btn.button("loading");
            $.ajax({
                method : "POST",
                url : url_gb+"/admin/ContainerType/"+id,
                dataType : 'json',
                data : $(form).serialize()
            }).done(function(rec){
                btn.button("reset");
                if(rec.status==1){
                    TableList.api().ajax.reload();
                    resetFormCustom(form);
                    swal(rec.title,rec.content,"success");
                    $('#ModalEdit').modal('hide');
                }else{
                    swal(rec.title,rec.content,"error");
                }
            }).fail(function(){
                swal("system.system_alert","system.system_error","error");
                btn.button("reset");
            });
        },
        invalidHandler: function (form) {

        }
    });

    $('body').on('click','.btn-delete',function(e){
        e.preventDefault();
        var btn = $(this);
        var id = btn.data('id');
        swal({
            title: "@lang('lang.do_you_want_to_delete')",
            text: "@lang('lang.if_you_delete_you_will_not_be_able_to_restore_it')",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: "@lang('lang.yes_i_want_to_delete')",
            cancelButtonText: "@lang('lang.cancel')",
            showLoaderOnConfirm: true,
            buttonsStyling: false
        }).then(function() {
            $.ajax({
                method : "POST",
                url : url_gb+"/admin/ContainerType/Delete/"+id,
                data : {ID : id}
            }).done(function(rec){
                if(rec.status==1){
                    swal(rec.title,rec.content,"success");
                    TableList.api().ajax.reload();
                }else{
                    swal("@lang('lang.problematic_system')","@lang('lang.please_contact_the_administrator')","error");
                }
            }).fail(function(data){
                swal("@lang('lang.problematic_system')","@lang('lang.please_contact_the_administrator')","error");
            });
        }).catch(function(e){
            //console.log(e);
        });
    });


</script>
@endsection
