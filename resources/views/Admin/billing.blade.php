﻿@extends('Admin.layouts.layout')
@section('css_bottom')
<style>
.fix-scroll-dashboard {
    width: 100%;
    overflow-x: scroll;
}
</style>
@endsection
@section('body')
<div class="content">
    <input type="hidden" id="lang" value="{{ $lang }}">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="content">
                        <h4 class="title">
                            {{$title_page or '' }}
                            <a href="{{ url('/admin/'.$lang.'/Billing/Create') }}" class="btn btn-success btn-add pull-right" >
                                + @lang('lang.create_data')
                            </a>
                        </h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <div class="material-datatables fix-scroll-dashboard">
                            <div class="table-responsive">
                                <table id="TableList" class="table table-striped table-no-bordered table-hover" style="width:100%;cellspacing:0">
                                    <thead>
                                        <tr>
                                            <th>@lang('lang.customer_id')</th>
                                            <th>@lang('lang.customer')</th>
                                            <th>@lang('lang.firstname')</th>
                                            <th>@lang('lang.lastname')</th>
                                            <th>@lang('lang.billing_number')</th>
                                            <th>@lang('lang.issued_date_billing')</th>
                                            <th>@lang('lang.total')</th>
                                            <!-- <th>@lang('lang.status')</th> -->
                                            <th>@lang('lang.status')</th>
                                            <th>@lang('lang.tool')</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('modal')
<div class="modal" id="ModalStatus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="FormShippingStatus">
                <input type="hidden" id="edit_billing_id">
                <div class="modal-header"><h4>@lang('lang.status') @lang('lang.billing')</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="form-check">
                        <label for="add_container_id">@lang('lang.select_status_payment')</label>
                        <select name="payment_status" class="select2 form-control" tabindex="-1" data-placeholder="@lang('lang.select_status_payment')" id="edit_status_payment" >
                            <option value="F">@lang('lang.payment_not_complete')</option>
                            <option value="T">@lang('lang.payment_complete')</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">@lang('lang.save')</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('js_bottom')
<script src="{{asset('assets/global/plugins/orakuploader/orakuploader.js')}}"></script>
<script>

     var TableList = $('#TableList').dataTable({
        "ajax": {
            "url": url_gb+"/admin/{{$lang}}/Billing/Lists",
            "data": function ( d ) {
                d.lang = $('#lang').val();
                // d.custom = $('#myInput').val();
                // etc
            }
        },
        "order": [[ 2, "desc" ]],
        "columns": [
            {"data" : "customer_general_code", "name": "users.customer_general_code"},
            {"data" : "user", "searchable" : false},
            {"data" : "firstname", name:"users.firstname", "visible" : false},
            {"data" : "lastname", name:"users.lastname", "visible" : false},
            {"data" : "billing_no", name: "billings.billing_no"},
            {"data" : "billing_date", name: "billings.billing_date"},
            {"data" : "total_price", name: "billings.total_price"},
            // {"data" : "status", "searchable" : false},
            {"data" : "payment_status", "searchable" : false},
            { "data": "action","className":"action text-center","searchable" : false , "orderable" : false }
        ]
    });

     $('body').on('click','.btn-ststus',function(data){
        var id = $(this).data('id');
        $('#edit_billing_id').val(id);
        ShowModal('ModalStatus');

    });

     $('#FormShippingStatus').validate({
        errorElement: 'div',
        errorClass: 'invalid-feedback',
        focusInvalid: false,
        rules: {

        },
        messages: {
        },
        highlight: function (e) {
            validate_highlight(e);
        },
        success: function (e) {
            validate_success(e);
        },

        errorPlacement: function (error, element) {
            validate_errorplacement(error, element);
        },
        submitHandler: function (form) {
            /*
            if(CKEDITOR!==undefined){
                for ( instance in CKEDITOR.instances ){
                    CKEDITOR.instances[instance].updateElement();
                }
            }
            */
            var btn = $(form).find('[type="submit"]');
            var id = $('#edit_billing_id').val();
            btn.button("loading");
            $.ajax({
                method : "POST",
                url : url_gb+"/admin/Billing/PaymentStatus/"+id,
                dataType : 'json',
                data : $(form).serialize()
            }).done(function(rec){
                if(rec.status==1){
                    TableList.api().ajax.reload();
                    swal(rec.title,rec.content,"success");
                    $('#ModalStatus').modal('hide');
                }else{
                    $('#ModalStatus').modal('hide');
                    swal(rec.title,rec.content,"error");
                }
            }).fail(function(){
                swal("system.system_alert","system.system_error","error");
                btn.button("reset");
            });
        },
        invalidHandler: function (form) {

        }
    });

</script>
@endsection
