﻿@extends('Admin.layouts.layout')
@section('css_bottom')
<style>
.fix-scroll-dashboard {
    width: 100%;
    overflow-x: scroll;
}
</style>
@endsection
@section('body')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="content">
                        <h4 class="title">
                            {{$title_page or '' }}
                            <a href="{{ url('/admin/Delivery/Create') }}" class="btn btn-success btn-add pull-right" >
                                + เพิ่มข้อมูล
                            </a>
                        </h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <div class="material-datatables">
                            <div class="table-responsive">
                                <table id="TableList" class="table table-striped table-no-bordered table-hover" style="width:100%;cellspacing:0">
                                    <thead>
                                        <tr>
                                        <th>ทะเบียนรถขนส่ง</th>
                                        <th>วันที่จัดส่ง</th>
                                        <th>สถานะ</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('modal')
<div class="modal" id="ModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="FormAdd">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">เพิ่ม {{$title_page or 'ข้อมูลใหม่'}}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">

                <div class="form-group">
                    <label for="add_plate_no">plate_no</label>
                    <input type="text" class="form-control" name="plate_no" id="add_plate_no" required="" placeholder="plate_no">
                </div>

                <label for="add_date_delivery">date_delivery</label>
                <div class="input-group" data-date="{{date('Y-m-d')}}">
                    <input type="text" value="" readonly="readonly" class="form-control form_date" name="date_delivery" id="add_date_delivery" required="" placeholder="date_delivery">
                    <span class="input-group-addon remove_date_time"><i class="glyphicon glyphicon-remove icon-remove"></i></span>
                    <span class="input-group-addon trigger_date_time" for="add_date_delivery"><i class="glyphicon glyphicon-calendar icon-calendar"></i></span>
                </div>

                <div class="form-group">
                    <label for="add_status_id">status_id</label>
                    <input type="text" class="form-control" name="status_id" id="add_status_id" required="" placeholder="status_id">
                </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">บันทึก</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" id="ModalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <input type="hidden" name="edit_id" id="edit_id">
            <form id="FormEdit">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">แก้ไขข้อมูล {{$title_page or 'ข้อมูลใหม่'}}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">

                <div class="form-group">
                    <label for="edit_plate_no">plate_no</label>
                    <input type="text" class="form-control" name="plate_no" id="edit_plate_no" required="" placeholder="plate_no">
                </div>

                <label for="edit_date_delivery">date_delivery</label>
                <div class="input-group" data-date="{{date('Y-m-d')}}">
                    <input type="text" value="" readonly="readonly" class="form-control form_date" name="date_delivery" id="edit_date_delivery" required="" placeholder="date_delivery">
                    <span class="input-group-addon remove_date_time"><i class="glyphicon glyphicon-remove icon-remove"></i></span>
                    <span class="input-group-addon trigger_date_time" for="edit_date_delivery"><i class="glyphicon glyphicon-calendar icon-calendar"></i></span>
                </div>

                <div class="form-group">
                    <label for="edit_status_id">status_id</label>
                    <input type="text" class="form-control" name="status_id" id="edit_status_id" required="" placeholder="status_id">
                </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">บันทึก</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- <div class="modal" id="ModalShipping" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <input type="hidden" name="edit_id" id="edit_id">
            <form id="FormShipping">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">ออกใบส่งสินค้า</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="form-group" style="margin-bottom: 50PX;">
                        <label for="shipping_user_id">เลือกลูกค้า</label>
                        <select name="user_id" class="select2 form-control" tabindex="-1" data-placeholder="เลือกลูกค้า" id="shipping_user_id" >
                            <option value="">เลือกลูกค้า</option>
                            @if(!$Users->isEmpty())
                                @foreach($Users as $User)
                                    <option value="{{ $User->id }}">{{ $User->firstname }} {{ $User->lastname }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>

                    <div class="fix-scroll-dashboard">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>
                                        <div class="form-check">
                                            <label for="edit_active" class="checkbox form-check-label">
                                                <span class="icons">
                                                    <span class="first-icon fa fa-square"></span>
                                                    <span class="second-icon fa fa-check-square "></span>
                                                </span><input type="checkbox" class="form-check-input" data-toggle="checkbox" id="check_active_all" required="" aria-required="true">
                                            </label>
                                        </div>
                                    </th>
                                    <th>เลข PO</th>
                                    <th>จำนวนชิ้นตาม PO</th>
                                    <th>จำนวนชิ้นที่ปิดตู้มา</th>
                                    <th>จำนวนชิ้นตามจริง</th>
                                    <th>น้ำหนักรวม</th>
                                    <th>ขนาดคิว</th>
                                    <th>จัดส่งภายในวันที่</th>
                                    <th>วันที่รับเข้าโกดังไทย</th>
                                    <th>วันที่รับของเข้าที่จีน</th>
                                    <th>หมายเหตุ</th>
                                </tr>
                            </thead>
                            <tbody id="list-po-products">

                            </tbody>
                            <tfooter>
                                <tr id="list-po-products-not-found">
                                    <td colspan="10" style="text-align: center;">ไม่พบข้อมูล</td>
                                </tr>

                            </tfooter>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">บันทึก</button>
                </div>
            </form>
        </div>
    </div>
</div> -->

<div class="modal" id="ModalStatusDeliverySlip" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <input type="hidden" name="edit_id" id="edit_id">
            <form id="FormSttusDelivery">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">เปลี่ยนสถานะการจัดส่งของใบส่งสินค้า</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="fix-scroll-dashboard">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>
                                        <div class="form-check">
                                            <label for="edit_active" class="checkbox form-check-label">
                                                <span class="icons">
                                                    <span class="first-icon fa fa-square"></span>
                                                    <span class="second-icon fa fa-check-square "></span>
                                                </span><input type="checkbox" class="form-check-input" data-toggle="checkbox" id="check_active_all" required="" aria-required="true">
                                            </label>
                                        </div>
                                    </th>
                                    <th>เลขที่ใบส่งของ</th>
                                    <th>ลูกค้า</th>
                                    <th>วันที่ออกบิล</th>
                                    <th>จำนวน</th>
                                    <th>น้ำหนัก</th>
                                    <th>รวม</th>
                                    <th>ค่าจัดส่ง</th>
                                    <th>รวมสุทธิ</th>
                                    <th>สถานะ</th>
                                    <th></th>
                                </tr>

                            </thead>
                            <tbody id="list-delivery-slip">

                            </tbody>
                            <tfooter>
                                <tr id="list-delivery-slip-not-found">
                                    <td colspan="10" style="text-align: center;">ไม่พบข้อมูล</td>
                                </tr>

                            </tfooter>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">บันทึก</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" id="ModalStatus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="FormShippingStatus">
                <input type="hidden" id="edit_delivery_id">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">เปลี่ยนสถานะการจัดส่ง</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="form-check">
                        <label for="edit_active" class="checkbox form-check-label edit_active">
                            <span class="icons"><span class="first-icon fa fa-square"></span><span class="second-icon fa fa-check-square "></span></span><input type="checkbox" class="form-check-input" data-toggle="checkbox" name="active" id="edit_active" required="" value="T" aria-required="true"> ดำเนินการจัดส่งเรียบร้อย
                        </label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">บันทึก</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('js_bottom')
<script src="{{asset('assets/global/plugins/orakuploader/orakuploader.js')}}"></script>
<script>

    var TableList = $('#TableList').dataTable({
        "ajax": {
            "url": url_gb+"/admin/Delivery/Lists",
            "data": function ( d ) {
                //d.myKey = "myValue";
                // d.custom = $('#myInput').val();
                // etc
            }
        },
        "columns": [
            {"data" : "plate_no"},
            {"data" : "date_delivery"},
            {"data" : "status_id"},
            { "data": "action","className":"action text-center","searchable" : false , "orderable" : false }
        ]
    });

    $('body').on('change','#check_active_all',function(data){
        var active = $('#check_active_all').closest("label.checked");
        var input_check = $('#list-po-products').find('label.label-check-po');
        if(active.length > 0){
            $('.label-check-po').addClass('checked');
            $(".input-check-po").prop( "checked", true );
        }else{
            $('.label-check-po').removeClass('checked');
            $(".input-check-po").prop( "checked", false );
        }
    });

    $('body').on('click','.btn-ststus',function(data){
        var id = $(this).data('id');
        $('#edit_delivery_id').val(id);
        ShowModal('ModalStatus');
    });

    // $('body').on('change','#shipping_user_id',function(data){
    //     $('#list-po-products').html('');
    //     var user_id = $(this).val();
    //     var delivery_id = $('#edit_id').val();
    //     $.ajax({
    //         method : "GET",
    //         url : url_gb+"/admin/Delivery/Shipping/GetUserPo/"+user_id,
    //         dataType : 'json',
    //         data: {
    //             delivery_id: delivery_id,
    //         }
    //     }).done(function(rec){
    //         var html = '';
    //         $.each(rec.ImportToChainas, function( k, v ) {
    //             var qty_po = 0;
    //             var weight_all_po = 0;
    //             var cubic_po = 0;
    //             html += '<tr>\
    //                         <td>\
    //                             <div class="form-check">\
    //                                 <label for="add_active" class="checkbox form-check-label label-check-po">\
    //                                     <span class="icons">\
    //                                         <span class="first-icon fa fa-square"></span>\
    //                                         <span class="second-icon fa fa-check-square"></span>\
    //                                     </span>\
    //                                     <input type="checkbox" class="form-check-input input-check-po" data-toggle="checkbox" name="import_to_chaina_id[]" required="" value="'+v.import_to_chaina_id+'" aria-required="true">\
    //                                 </label>\
    //                             </div>\
    //                         </td>\
    //                         <td>'+v.po_no+'</td>\
    //                         <td>'+addNumformat(v.qty_chaina)+'</td>\
    //                         <td>'+addNumformat(v.qty_container)+'</td>\
    //                         <td>'+addNumformat(v.qty_thai)+'</td>\
    //                         <td>'+addNumformat(v.weight_all)+'</td>\
    //                         <td>'+addNumformat(v.cubic)+'</td>\
    //                         <td>'+v.delivery_date+'</td>\
    //                         <td>'+v.import_to_thai_date+'</td>\
    //                         <td>'+v.import_to_chaina_date+'</td>\
    //                         <td><input type="text" class="form-control" id="" value=""></td>\
    //                     </tr>';
    //         });
    //
    //         if(rec.ImportToChainas.length > 0){
    //             $('#list-po-products').append(html);
    //             $('#list-po-products-not-found').hide();
    //         }else{
    //             $('#list-po-products-not-found').show();
    //         }
    //
    //         ShowModal('ModalShipping');
    //     }).fail(function(){
    //         swal("system.system_alert","system.system_error","error");
    //     });
    // });

    $('body').on('click','.btn-add',function(data){
        //ShowModal('ModalAdd');
    });

    $('body').on('click','.btn-edit',function(data){
        var btn = $(this);
        btn.button('loading');
        var id = $(this).data('id');
        $('#edit_id').val(id);
        $.ajax({
            method : "GET",
            url : url_gb+"/admin/Delivery/"+id,
            dataType : 'json'
        }).done(function(rec){
            $('#edit_plate_no').val(rec.plate_no);
            $('#edit_date_delivery').val(rec.date_delivery);
            $('#edit_status_id').val(rec.status_id);

            btn.button("reset");
            ShowModal('ModalEdit');
        }).fail(function(){
            swal("system.system_alert","system.system_error","error");
            btn.button("reset");
        });
    });

    // $('body').on('click','.btn-shipping',function(data){
    //     $("#shipping_user_id").val('').trigger('change');
    //     $('#shipping_user_id').select2('val','');
    //     var btn = $(this);
    //     btn.button('loading');
    //     var id = $(this).data('id');
    //     $('#edit_id').val(id);
    //     $.ajax({
    //         method : "GET",
    //         url : url_gb+"/admin/Delivery/Shipping/"+id,
    //         dataType : 'json'
    //     }).done(function(rec){
    //         btn.button("reset");
    //         ShowModal('ModalShipping');
    //     }).fail(function(){
    //         swal("system.system_alert","system.system_error","error");
    //         btn.button("reset");
    //     });
    // });

    $('body').on('click','.btn-change-delivery-slip',function(data){
        //$("#shipping_user_id").val('').trigger('change');
        $('#shipping_user_id').select2('val','');
        $('.form-check-label').removeClass('checked');
        var btn = $(this);
        btn.button('loading');
        var id = $(this).data('id');
        $('#edit_id').val(id);
        $.ajax({
            method : "GET",
            url : url_gb+"/admin/Delivery/ShowDeliverySlip/"+id,
            dataType : 'json'
        }).done(function(rec){
            $('#list-delivery-slip').html('');
            var html = '';
            var ststus = '';
            var html_input = '';
            $.each(rec.DeliverySlips, function( k, v ) {
                if(v.status_id == 1){
                    ststus = '<span class="badge badge-warning">'+v.delivery_slip_status_name+'</span>';
                    html_input = '<div class="form-check">\
                                    <label for="add_active" class="checkbox form-check-label label-check-po">\
                                        <span class="icons">\
                                            <span class="first-icon fa fa-square"></span>\
                                            <span class="second-icon fa fa-check-square"></span>\
                                        </span>\
                                        <input type="checkbox" class="form-check-input input-check-po" data-toggle="checkbox" name="delivery_slip_id[]" required="" value="'+v.id+'" aria-required="true">\
                                    </label>\
                                </div>';
                }else{
                    ststus = '<span class="badge badge-success">'+v.delivery_slip_status_name+'</span>';
                    html_input = '';
                }
                html += '<tr>\
                            <td>\
                                '+html_input+'\
                            </td>\
                            <td>'+v.delivery_slip_no+'</td>\
                            <td>'+v.firstname+' '+v.lastname+'</td>\
                            <td>'+v.delivery_slip_date+'</td>\
                            <td>'+addNumformat(v.total_qty)+'</td>\
                            <td>'+addNumformat(v.total_weight)+'</td>\
                            <td>'+addNumformat(v.price.toFixed(2))+'</td>\
                            <td>'+addNumformat(v.delivery_charge_price.toFixed(2))+'</td>\
                            <td>'+addNumformat(v.total_price.toFixed(2))+'</td>\
                            <td>'+ststus+'</td>\
                            <td></td>\
                        </tr>';
            });
            $('#list-delivery-slip').append(html);
            $('#list-delivery-slip-not-found').hide();
            ShowModal('ModalStatusDeliverySlip');
        }).fail(function(){
            swal("system.system_alert","system.system_error","error");
            btn.button("reset");
        });
    });

    $('#FormAdd').validate({
        errorElement: 'div',
        errorClass: 'invalid-feedback',
        focusInvalid: false,
        rules: {

            plate_no: {
                required: true,
            },
            date_delivery: {
                required: true,
            },
            status_id: {
                required: true,
            },
        },
        messages: {

            plate_no: {
                required: "กรุณาระบุ",
            },
            date_delivery: {
                required: "กรุณาระบุ",
            },
            status_id: {
                required: "กรุณาระบุ",
            },
        },
        highlight: function (e) {
            validate_highlight(e);
        },
        success: function (e) {
            validate_success(e);
        },

        errorPlacement: function (error, element) {
            validate_errorplacement(error, element);
        },
        submitHandler: function (form) {
            /*
            if(CKEDITOR!==undefined){
                for ( instance in CKEDITOR.instances ){
                    CKEDITOR.instances[instance].updateElement();
                }
            }
            */
            var btn = $(form).find('[type="submit"]');
            var data_ar = removePriceFormat(form,$(form).serializeArray());
            btn.button("loading");
            $.ajax({
                method : "POST",
                url : url_gb+"/admin/Delivery",
                dataType : 'json',
                data : $(form).serialize()
            }).done(function(rec){
                btn.button("reset");
                if(rec.status==1){
                    TableList.api().ajax.reload();
                    resetFormCustom(form);
                    swal(rec.title,rec.content,"success");
                    $('#ModalShipping').modal('hide');
                }else{
                    swal(rec.title,rec.content,"error");
                }
            }).fail(function(){
                swal("system.system_alert","system.system_error","error");
                btn.button("reset");
            });
        },
        invalidHandler: function (form) {

        }
    });

    $('#FormShippingStatus').validate({
        errorElement: 'div',
        errorClass: 'invalid-feedback',
        focusInvalid: false,
        rules: {

        },
        messages: {
        },
        highlight: function (e) {
            validate_highlight(e);
        },
        success: function (e) {
            validate_success(e);
        },

        errorPlacement: function (error, element) {
            validate_errorplacement(error, element);
        },
        submitHandler: function (form) {
            /*
            if(CKEDITOR!==undefined){
                for ( instance in CKEDITOR.instances ){
                    CKEDITOR.instances[instance].updateElement();
                }
            }
            */
            var btn = $(form).find('[type="submit"]');
            var id = $('#edit_delivery_id').val();
            btn.button("loading");
            $.ajax({
                method : "POST",
                url : url_gb+"/admin/Delivery/GetStatus/"+id,
                dataType : 'json',
                data : $(form).serialize()
            }).done(function(rec){
                if(rec.status==1){
                    TableList.api().ajax.reload();
                    swal(rec.title,rec.content,"success");
                    $('#ModalStatus').modal('hide');
                }else{
                    swal(rec.title,rec.content,"error");
                }
            }).fail(function(){
                swal("system.system_alert","system.system_error","error");
                btn.button("reset");
            });
        },
        invalidHandler: function (form) {

        }
    });

    $('#FormEdit').validate({
        errorElement: 'div',
        errorClass: 'invalid-feedback',
        focusInvalid: false,
        rules: {

            plate_no: {
                required: true,
            },
            date_delivery: {
                required: true,
            },
            status_id: {
                required: true,
            },
        },
        messages: {

            plate_no: {
                required: "กรุณาระบุ",
            },
            date_delivery: {
                required: "กรุณาระบุ",
            },
            status_id: {
                required: "กรุณาระบุ",
            },
        },
        highlight: function (e) {
            validate_highlight(e);
        },
        success: function (e) {
            validate_success(e);
        },

        errorPlacement: function (error, element) {
            validate_errorplacement(error, element);
        },
        submitHandler: function (form) {
            /*
            if(CKEDITOR!==undefined){
                for ( instance in CKEDITOR.instances ){
                    CKEDITOR.instances[instance].updateElement();
                }
            }
            */
            var btn = $(form).find('[type="submit"]');
            var id = $('#edit_id').val();
            btn.button("loading");
            $.ajax({
                method : "POST",
                url : url_gb+"/admin/Delivery/"+id,
                dataType : 'json',
                data : $(form).serialize()
            }).done(function(rec){
                btn.button("reset");
                if(rec.status==1){
                    TableList.api().ajax.reload();
                    resetFormCustom(form);
                    swal(rec.title,rec.content,"success");
                    $('#ModalEdit').modal('hide');
                }else{
                    swal(rec.title,rec.content,"error");
                }
            }).fail(function(){
                swal("system.system_alert","system.system_error","error");
                btn.button("reset");
            });
        },
        invalidHandler: function (form) {

        }
    });

    $('#FormSttusDelivery').validate({
        errorElement: 'div',
        errorClass: 'invalid-feedback',
        focusInvalid: false,
        rules: {

        },
        messages: {

        },
        highlight: function (e) {
            validate_highlight(e);
        },
        success: function (e) {
            validate_success(e);
        },

        errorPlacement: function (error, element) {
            validate_errorplacement(error, element);
        },
        submitHandler: function (form) {
            /*
            if(CKEDITOR!==undefined){
                for ( instance in CKEDITOR.instances ){
                    CKEDITOR.instances[instance].updateElement();
                }
            }
            */
            var btn = $(form).find('[type="submit"]');
            btn.button("loading");
            $.ajax({
                method : "POST",
                url : url_gb+"/admin/Delivery/GetStatusDeliverySlips",
                dataType : 'json',
                data : $(form).serialize()
            }).done(function(rec){
                btn.button("reset");
                if(rec.status==1){
                    TableList.api().ajax.reload();
                    resetFormCustom(form);
                    swal(rec.title,rec.content,"success");
                    $('#ModalStatusDeliverySlip').modal('hide');
                }else{
                    swal(rec.title,rec.content,"error");
                }
            }).fail(function(){
                swal("system.system_alert","system.system_error","error");
                btn.button("reset");
            });
        },
        invalidHandler: function (form) {

        }
    });

    $('body').on('click','.btn-delete',function(e){
        e.preventDefault();
        var btn = $(this);
        var id = btn.data('id');
        swal({
            title: "คุณต้องการลบใช่หรือไม่",
            text: "หากคุณลบจะไม่สามารถเรียกคืนข้อมูลกลับมาได้",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: "ใช่ ฉันต้องการลบ",
            cancelButtonText: "ยกเลิก",
            showLoaderOnConfirm: true,
            buttonsStyling: false
        }).then(function() {
            $.ajax({
                method : "POST",
                url : url_gb+"/admin/Delivery/Delete/"+id,
                data : {ID : id}
            }).done(function(rec){
                if(rec.status==1){
                    swal(rec.title,rec.content,"success");
                    TableList.api().ajax.reload();
                }else{
                    swal("ระบบมีปัญหา","กรุณาติดต่อผู้ดูแล","error");
                }
            }).fail(function(data){
                swal("ระบบมีปัญหา","กรุณาติดต่อผู้ดูแล","error");
            });
        }).catch(function(e){
            //console.log(e);
        });
    });

    $('#shipping_user_id').select2();


</script>
@endsection
