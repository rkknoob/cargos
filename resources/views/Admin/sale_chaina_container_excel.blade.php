
<table>
    <tr>
        <td colspan="3"><h3>ใบ Sale</h3></td>
    </tr>
    <tr>
        <td colspan="3"><b>รหัสตู้: {{ $Container->container_code }}</b></td>
    </tr>
    <tr>
        <td colspan="3"><b>วันที่ปิดตู้: {{ $Container->created_at }}</b></td>
    </tr>
</table>

<table class="table_css" border="1" cellspacing="0">
    <tr>
        <th>รหัสลูกค้า</th>
        <th>จำนวน</th>
        <th>KG รวม</th>
        <th>CBM รวม</th>
        <th>จำนวน  PO</th>
        <th>จำนวนรวม</th>
    </tr>
    @if(! $ImportToChainas->isEmpty())
        @php

        @endphp
        @foreach($ImportToChainas as $key => $ImportToChaina)
            @php
                $cubil = 0;
                if(!empty($ImportToChaina->product_amount_real) && !empty($ImportToChaina->product_cubic)){
                    $cubil = $ImportToChaina->product_cubic * $ImportToChaina->product_amount_real;
                }
            @endphp
            <tr>
                <td>{{  $ImportToChaina->customer_general_code }}</td>
                <td>{{ number_format($ImportToChaina->product_amount) }}</td>
                <td>{{ number_format($ImportToChaina->product_KG) }}</td>
                <td>{{ number_format($cubil, 3) }}</td>
                <td>{{ number_format($ImportToChaina->po_amount) }}</td>
                <td>{{ number_format($ImportToChaina->product_amount_real) }}/{{ number_format($ImportToChaina->product_amount) }}</td>
            </tr>
        @endforeach
    @endif
</table>

<table border="1" cellspacing="0">
    <tr>
        <th>ลำดับ</th>
        <th>รหัสลูกค้า</th>
        <th>เลขที่ PO</th>
        <th>รายการ</th>
        <th>จำนวน</th>
        <th>KG</th>
        <th>CBM</th>
        <th>เรทราคา</th>
        <th>รวม</th>
        <th>ควรคิดเป็น</th>
        <th>ปัจจุบันคิดเป็น</th>
        <th>ตรงกันหรือไม่</th>
    </tr>
    @if(! $QrCodeProducts->isEmpty())
        @php
            $total_qty_container = 0;
            $total_weight_all = 0;
            $total_cubic = 0;
            $amount = 0;
            $total = 0;
        @endphp
        @foreach($QrCodeProducts as $key => $QrCodeProduct)
            @php
                $cubil = 0;
                $subtotal = 0;
                $should_rate = '-';
                $kg_cbm = 0;

                /* วิธีคำนวนน้ำหนักต่อคิว = กิโล/คิว
                เช่นหนัก 400 กิโล 2 คิว
                ตกคิวละ 400/2 = 200 กิโล
                ถ้าหนัก 250 กิโลต่อคิวขึ้นไป = ควรคิดเป็นกิโล
                ถ้าหนัก ต่ำกว่า 250 กิโล = ควรคิดเป็นคิว
                ถ้าไม่ใส่คิวมา ขึ้นเป็น n/a */

                $amount += $QrCodeProduct->product_amount;
                $total_weight_all += $QrCodeProduct->product_weight_per_item;
                if(!empty($QrCodeProduct->cubic_item) && !empty($QrCodeProduct->product_amount)){
                    $cubil = $QrCodeProduct->cubic_item * $QrCodeProduct->product_amount;
                }

                $weight_product = $QrCodeProduct->product_weight_per_item;

                // set สิ่งที่ควณนำมาคำนวณ
                $check_price = 250;
                //$check_price = ($QrCodeProduct->rate_price * $weight_product) / $cubil;

                if($cubil > 0 && $weight_product > 0){
                    if(($cubil/$weight_product) >= $check_price ){
                        $should_rate = 'KG';
                    }else if( ($cubil/$weight_product) < $check_price ){
                        $should_rate = 'CBM';
                    }
                }

                // set เงินที่คำนวน
                if( $QrCodeProduct->type_rate == 'KG' ){
                    $subtotal = $QrCodeProduct->rate_price * $weight_product;
                }else if( $QrCodeProduct->type_rate == 'CBM' ){
                    if( $QrCodeProduct->cubic_item > 0 ){
                        $subtotal = $QrCodeProduct->rate_price * $cubil;
                    }else{
                        $subtotal = 0;
                    }
                }
                $subtotal = $subtotal;

                $total_cubic += $cubil;
                $total += $subtotal;
                $rate_type_price = '';
                if($QrCodeProduct->rel_user_type_product_id_old != null){
                    $rate_type_price = \App\Models\RelUserTypeProduct::where('id', $QrCodeProduct->rel_user_type_product_id_old)->first();
                }

                $q = 0;
                if($QrCodeProduct->product_weight_per_item > 0 && $cubil > 0){
                    $q = number_format($cubil,2,".","");
                    if($QrCodeProduct->product_weight_per_item > 0 && $q > 0){
                        $kg_cbm = ($QrCodeProduct->product_weight_per_item / $q);
                    }
                }
            @endphp
            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ $QrCodeProduct->customer_general_code }}</td>
                <td>{{ $QrCodeProduct->po_no }}</td>
                <td>{{ $QrCodeProduct->product_name_en != null ? $QrCodeProduct->product_name_en : $QrCodeProduct->product_name_th }}</td>
                <td>{{ number_format($QrCodeProduct->product_amount) }}</td>
                <td>{{ number_format($QrCodeProduct->product_weight_per_item) }}</td>
                <td>{{ number_format($cubil,3) }}</td>
                <td>{{ number_format($QrCodeProduct->rate_price, 2) }}</td>
                <td>{{ number_format($subtotal, 2) }}</td>
                <td>{{ $should_rate }}</td>
                <td>{{ $QrCodeProduct->type_rate }}</td>
                <td>
                    {{ $should_rate == $QrCodeProduct->type_rate ? 'ตรงกัน' : 'ไม่ตรงกัน' }}
                </td>

            </tr>
        @endforeach
        <tr>
            <td colspan="4"><b>Total</b></td>
            <td>{{ number_format($amount) }}</td>
            <td>{{ number_format($total_weight_all) }}</td>
            <td>{{ number_format($total_cubic,3) }}</td>
            <td></td>
            <td>{{ number_format($total,2) }}</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    @endif

</table>
