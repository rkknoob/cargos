
 <style>
    @page{
        header: page-header;
        footer: page-footer;
        margin-top: 0;
        /*margin-bottom: 150px;*/
    }
    .page-break {
        page-break-after: always;
    }
    /*body{
        font-family: Arial;
    }
    td {
        /height: 25px;
    }
    th {
        font-weight: normal;
    }*/
</style>


    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>ใบส่งสินค้า / รายการสินค้า</title>
        <style>
            @font-face {
                font-family: 'SentyTang';
                font-style: normal;
                font-weight: normal;
                src: url("{{ asset('fonts/SentyTang.ttf') }}") format('truetype');
            }

            body {
                font-family: 'SentyTang';
            }
            .table_css {
                height: 15px;
                font-size: 12px;
                width: 800px;
            }
            .table_css td {
                height: 15px;
                font-size: 12px;
            }
            .table_css th {
                font-weight: normal;
                font-size: 12px;
            }
        </style>
    </head>
    <body lang="th">
        <htmlpageheader name="page-header">

        </htmlpageheader>

        <htmlpagebody>
            <div class="row">
                <div style="color: black; text-align: left; font-size: 13px; ">

                </div>
            </div>
            <div class="row" style="padding-top: 20px;">
                <div style="color: black; text-align: center; font-size: 16px; ">
                    <b>NEW CARGO</b>
                </div>
            </div>
            <div class="row" style="padding-top: 20px;">
                <table class="table_css">
                    <tr>
                        <td colspan="3" style="text-align: right; width: 700px;"><b>บิลเลขที่:</b></td>
                        <td style="text-align: right;  width: 100px;"><b>{{ $DaliverySheet->delivery_no }}</b></td>
                    </tr>
                    <tr>
                        <td colspan="3" style="text-align: right;"><b>Date:</b></td>
                        <td style="text-align: right;"><b>{{ $DaliverySheet->delivery_slip_date }}</b></td>
                    </tr>
                    <tr>
                        <td colspan="3" style="text-align: right;"><b>ID:</b></td>
                        <td style="text-align: right;"><b>{{ $DaliverySheet->customer_general_code }}</b></td>
                    </tr>
                    <tr>
                        <td style="width: 150px;"><b>Customer Name:</b></td>
                        <td style="width: 300px;"><b>{{ $DaliverySheet->firstname }} {{ $DaliverySheet->lastname }}</b></td>
                        <td style="text-align: right;  width: 300px;"><b>Mobile:</b></td>
                        <td style="text-align: right;  width: 100px;"><b>{{ $DaliverySheet->main_mobile }}</b></td>
                    </tr>
                    <tr>
                        <td style="width: 150px;"><b>Address:</b></td>
                        <td colspan="3" style="width: 300px;">
                            <b>
                                {{ $DaliverySheet->address }}
                                {{ $DaliverySheet->amphure_name }}
                                {{ $DaliverySheet->province_name }}
                                {{ $DaliverySheet->zipcode }}
                            </b>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px;"><b>สถานะ:</b></td>
                        <td colspan="3" style="width: 300px;">
                            <b>
                                {{ $DaliverySheet->status_payment == 'T' ? 'ชำระเสร็จเรียบร้อย' : 'ยังไม่มีการชำระ' }}
                            </b>
                        </td>
                    </tr>

                </table>
            </div>

            <div class="row" style="padding-top: 20px;">
                <table class="table_css" border="1" cellspacing="0">
                    <tr>
                        <td colspan="5" style="text-align: center; width: 400px;"><b>รายการสินค้า</b></td>
                        <td colspan="6" style="text-align: center; width: 400px;"><b>ค่าขนส่ง (บาท)</b></td>
                    </tr>
                    <tr>
                        <td rowspan="2" style="text-align: center; width: 20px;"><b>No.<br>ลำดับ</b></td>
                        <td rowspan="2" style="text-align: center; width: 50px;"><b>ID<br>รหัสลูกค้า</b></td>
                        <td rowspan="2" style="text-align: center; width: 100px;"><b>PO No.<br>เลขที่บิล</b></td>
                        <td rowspan="2" style="text-align: center; width: 50px;"><b>Item<br>เลขกล่อง</b></td>
                        <td rowspan="2" style="text-align: center; width: 200px;"><b>Description<br>รายการ</b></td>
                        <td colspan="2" style="text-align: center;"><b>Weight</b></td>
                        <td style="text-align: center;"><b></b></td>
                        <td rowspan="2" style="text-align: center;"><b>Rate<br>เรท (THB)</b></td>
                        <td rowspan="2" style="text-align: center; width: 50px;"><b>Amount<br>รวม</b></td>
                        <td rowspan="2" style="text-align: center; width: 50px;"><b>จำนวน </b></td>
                    </tr>
                    <tr>
                        <td style="text-align: center;"><b>KG</b></td>
                        <td style="text-align: center;"><b>CBM</b></td>
                        <td style="text-align: center;"><b>เรทอื่น</b></td>
                    </tr>
                    @php
                        $cubic_all = 0;
                        $total_weight = 0;
                        $total_price = 0;
                        $price = 0;
                        $lot_id = '';
                    @endphp
                    @if(!$ProductLists->isEmpty())
                        @foreach($ProductLists as $key => $ProductList)
                            @php
                                $weight_all = 0;
                                if($lot_id != $ProductList->lot_id){
                                    $weight_all = $ProductList->weight_all;
                                    $lot_id = $ProductList->lot_id;
                                }

                                $cubic = ($ProductList->cubic / $ProductList->lot_product_qty);
                                $cubic_all += $cubic;
                                if($ProductList->rel_user_type_product_id_old != null){
                                    $RelUserTypeProduct = \App\Models\RelUserTypeProduct::find($ProductList->rel_user_type_product_id_old);
                                    $rate_price_old = $RelUserTypeProduct->price;
                                }else{
                                    $rate_price_old = 0;
                                }

                                if($ProductList->rel_user_type_product_id != null){
                                    $RelUserTypeProduct = \App\Models\RelUserTypeProduct::find($ProductList->rel_user_type_product_id);
                                    $rate_price = $RelUserTypeProduct->price;
                                }else{
                                    $rate_price = 0;
                                }

                                $price = ($weight_all * $rate_price);
                                $total_price += ($price);
                                if($weight_all != 0){
                                    if($rate_price != 0){
                                        $show_rate = number_format($rate_price,2);
                                    }else{
                                        $show_rate = 'NO';
                                    }
                                }else{
                                    $show_rate = '';
                                }
                                $total_weight += $weight_all;
                            @endphp
                            <tr>
                                <td style="text-align: center;">{{ number_format($key+1) }}</td>
                                <td style="text-align: center;">{{ $ProductList->customer_general_code }}</td>
                                <td style="text-align: center;">{{ $ProductList->po_no }}</td>
                                <td style="text-align: center;">{{ number_format($ProductList->item) }}</td>
                                <td style="text-align: center;">{{ $ProductList->product_name }}</td>
                                <td style="text-align: center;">{{ $weight_all != 0 ? number_format($weight_all, 2) : null }}</td>
                                <td style="text-align: center;">{{ $weight_all != 0 ? number_format($cubic, 2) : null }}</td>
                                <td style="text-align: center;">{{ $rate_price_old != 0 ? $rate_price_old : 'NO' }}</td>
                                <td style="text-align: center;">{{ $show_rate }}</td>
                                <td style="text-align: center;">{{ $weight_all != 0 ? number_format($price, 2) : null }}</td>
                                <td style="text-align: center;">1</td>
                            </tr>
                        @endforeach

                        <tr>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"><b>รวม</b></td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;">{{ number_format($total_weight, 2) }}</td>\
                            <td style="text-align: center;">{{ number_format($cubic_all, 2) }}</td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;">{{ number_format($total_price,2) }}</td>
                            <td style="text-align: center;">{{ number_format($DaliverySheet->total_qty) }}</td>
                        </tr>
                        @endif
                </table>
            </div>

            <div class="row" style="padding-top: 20px;">
                <table class="table_css" border="0">
                    <tr>
                        <td colspan="3" style="width: 800px;">ค่าใช้จ่ายอื่นๆ: ไม่มี</td>
                    </tr>
                    <tr>
                        <td style="width: 600px;"><b>รวม:</b></td>
                        <td style="text-align: center; width: 80px;"><b><u>{{ number_format($DaliverySheet->total_price, 2) }}</u></b></td>
                        <td style="width: 80px;"></td>
                    </tr>
                    <tr>
                        <td style="width: 600px;"><b>ค่าจัดส่ง:</b></td>
                        <td style="text-align: center; width: 80px;"><b><u></u></b></td>
                        <td style="width: 80px;"></td>
                    </tr>
                    <tr>
                        <td style="width: 600px;"><b>รวมสุทธิ:</b></td>
                        <td style="text-align: center; width: 80px;"><b><u>{{ number_format($DaliverySheet->total_price , 2) }}</u></b></td>
                        <td style="width: 80px;"></td>
                    </tr>
                </table>
            </div>

            <div class="row" style="padding-top: 20px;">
                <table class="table_css" border="0">
                    <tr>
                        <td colspan="2" style="width: 800px;">หมายเหตุ</td>
                    </tr>
                    <tr>
                        <td style="width: 50px;"></td>
                        <td style="width: 700px;">1. ทางบริษัทขอสงวนสิทธ์คิดค่าขนส่ง หากขนาดหรือน้ำหนักของสินค้าราคาค่าขนส่งต่ำกว่า 5,000 บาท/ครั้ง<br>ทางบริษัทจะคิดว่าค่าบริการจัดส่ง 500 บาท</td>
                    </tr>
                    <tr>
                        <td style="width: 50px;"></td>
                        <td style="width: 700px;">2. ถ้าหากลูกค้ายินยอมให้สะสมสินค้าครบ 5,000 บาท บริษัทจะบริการจัดส่งฟรี</td>
                    </tr>
                    <tr>
                        <td style="width: 50px;"></td>
                        <td style="width: 700px;">3. หากยอดรวมไม่ถึง 1,500 บาท บริษัทขอสงวนสิทธิ์ในการคิดค่าบริการขั้นต่ำที่ 1,500 บาท</td>
                    </tr>
                    <tr>
                        <td style="width: 50px;"></td>
                        <td style="width: 700px;">4. หากสินค้ามีปัญหา กรุณาแจ้งบริษัท ภายใน 3 วัน มิฉะนั้นทางบริษัทจะไม่รับผิดชอบ</td>
                    </tr>

                </table>
            </div>

            <div class="row" style="padding-top: 20px;">
                <table class="table_css" border="0">
                    <tr>
                        <td style="text-align: center; width: 400px;">ลงชื่อ.......................................................ผู้ส่งสินค้า</td>
                        <td style="text-align: center; width: 400px;">ลงชื่อ.......................................................ผู้รับสินค้า</td>

                    </tr>
                    <tr>
                        <td style="text-align: center; width: 400px;">วันที่ ........../........../..........</td>
                        <td style="text-align: center; width: 400px;">วันที่ ........../........../..........</td>
                    </tr>

                </table>
            </div>

        </htmlpagebody>

        <htmlpagefooter name="page-footer">
            <div style="padding-bottom: 40px;text-align: center;">

            </div>
        </htmlpagefooter>
    </body>
