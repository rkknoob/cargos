﻿@extends('Admin.layouts.layout')
@section('css_bottom')
<style>
.fix-scroll-dashboard {
    width: 100%;
    overflow-x: scroll;
}
</style>
@endsection
@section('body')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="content">
                        <!-- <h4 class="title">
                            {{$title_page or '' }}
                            <a href="{{ url('/admin/'.$lang.'/ImportToChainaContainer/create') }}" class="btn btn-success btn-add pull-right" >
                                + <i class="fa fa-desktop" style="font-size: 25px;"></i> @lang('lang.import_container_china')&nbsp&nbsp
                            </a>
                        </h4>
                        <h4 class="title">
                            <a href="{{ url('/admin/'.$lang.'/ImportToChainaContainer/MobileCreate') }}" class="btn btn-success btn-add pull-right" >
                                + <i class="fa fa-mobile" style="font-size: 25px;"></i> @lang('lang.import_container_china')&nbsp&nbsp
                            </a>
                        </h4> -->
                        <h4 class="title">
                            {{$title_page or '' }}
                            <a href="{{ url('/admin/'.$lang.'/ImportToChainaContainer/create') }}" class="btn btn-success btn-add pull-right" >
                                + <i class="fa fa-desktop" style="font-size: 20px;"></i> @lang('lang.import_container_china')
                            </a>
                        </h4>
                        <h4 class="title">
                            <a href="{{ url('/admin/'.$lang.'/ImportToChainaContainer/MobileCreate') }}" class="btn btn-success btn-add pull-right" >
                                + <i class="fa fa-mobile" style="font-size: 25px;"></i> @lang('lang.import_container_china')&nbsp&nbsp
                            </a>
                        </h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <div class="material-datatables fix-scroll-dashboard">
                            <div class="table-responsive">
                                <table id="TableList" class="table table-striped table-no-bordered table-hover" style="width:100%;cellspacing:0">
                                    <thead>
                                        <tr>
                                            <th>@lang('lang.container_no')</th>
                                            <th>@lang('lang.type_delivery')</th>
                                            <th>@lang('lang.type_container')</th>
                                            <th>@lang('lang.tatal_weigth')</th>
                                            <th>@lang('lang.total_size')</th>
                                            <th>@lang('lang.total_queue')</th>
                                            <th>@lang('lang.date_of_container_export_china')</th>
                                            <th>@lang('lang.delivery_deadline')</th>
                                            <th>@lang('lang.status')</th>
                                            <!-- <th>@lang('lang.sometime')จำนวนตาม PO</th> -->
                                            <th>@lang('lang.actual_number')</th>
                                            <!-- <th>@lang('lang.sometime')จำนวนที่ขาด</th> -->
                                            <th>@lang('lang.note')</th>
                                            <th style="width: 200px;">@lang('lang.tool')</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('modal')
<div class="modal" id="ModalContainerDetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-maximize" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">@lang('lang.detail_container')</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row table-data-product">
                    <!-- table scan -->
                    <div class="col-md-12">
                        <table class="table">
                            <thead>
                                <tr>
                                    <td>#</td>
                                    <td>@lang('lang.number_po')</td>
                                    <td>@lang('lang.qrcode')</td>
                                    <td>@lang('lang.lot_id')</td>
                                    <td>@lang('lang.customer_id')</td>
                                    <td>@lang('lang.type_product')</td>
                                    <td>@lang('lang.product_name')</td>
                                    <td>@lang('lang.weigh')</td>
                                    <td>@lang('lang.queue_size') (@lang('lang.m'))</td>
                                </tr>
                            </thead>
                            <tbody id="list-qr-code">
                            </tbody>
                        </table>
                    </div>
                    <!-- table summarize -->
                    <div class="col-md-12">
                        <table class="table" style="width: 100%;">
                            <thead>
                                <tr>
                                    <td class="text-center">#</td>
                                    <td class="text-center">@lang('lang.number_po')</td>
                                    <td class="text-center">@lang('lang.number_of_po')</td>
                                    <td class="text-center">@lang('lang.actual_number')</td>
                                </tr>
                            </thead>
                            <tbody class="add-product-po">
                                <tr>
                                    <td class="text-center" colspan="5">@lang('lang.data_not_found')</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">@lang('lang.close')</button>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js_bottom')
<script src="{{asset('assets/global/plugins/orakuploader/orakuploader.js')}}"></script>
<script>

    var TableList = $('#TableList').dataTable({
        "ajax": {
            "type": 'POST',
            "url": url_gb+"/admin/{{$lang}}/ImportToChainaContainer/Lists",
            "data": function ( d ) {
                //d.myKey = "myValue";
                // d.custom = $('#myInput').val();
                // etc
            }
        },
        "order": [[ 4, "desc" ]],
        "columns": [

            {"data": "container_code"},
            {"data": "transport_type_name_{{$lang}}", 'name': "transport_types.name_{{$lang}}"},
            {"data": "container_type_name_{{$lang}}", 'name': "container_types.name_{{$lang}}"},
            {"data": "weight"},
            {"data": "size", "searchable": false, "orderable" : false},
            {"data": "cubic"},
            {"data": "created_at"},
            {"data": "created_at"},
            {"data": "status_id", "className": "action text-center", "searchable": false},
            // {"data": "qty_po", "className": "text-center", "searchable": false },
            {"data": "qty_product_real", "className": "text-center", "searchable": false },
            // {"data": "null"},
            {"data": "remark"},
            {"data": "action","className":"action text-center","searchable" : false , "orderable" : false }
        ]
    });

    $('body').on('click', '.btn-delete', function(e){
        e.preventDefault();
        var btn = $(this);
        var id = btn.data('id');
        swal({
            title: "@lang('lang.do_you_want_to_delete')",
            text: "@lang('lang.if_you_delete_you_will_not_be_able_to_restore_it')้",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: "@lang('lang.yes_i_want_to_delete')",
            cancelButtonText: "@lang('lang.cancel')",
            showLoaderOnConfirm: true,
            buttonsStyling: false
        }).then(function() {
            $.ajax({
                method : "POST",
                url : url_gb+"/admin/ImportToChainaContainer/Delete/"+id,
                data : {ID : id}
            }).done(function(rec){
                if(rec.status==1){
                    swal(rec.title,rec.content,"success");
                    TableList.api().ajax.reload();
                }else{
                    swal("@lang('lang.system_system_error')","กรุณาติดต่อผู้ดูแล","error");
                }
            }).fail(function(data){
                swal("@lang('lang.system_system_error')","กรุณาติดต่อผู้ดูแล","error");
            });
        }).catch(function(e){
            //console.log(e);
        });
    });

    $('body').on('click', '.btn-container-detail', function(){
        var container_id = $(this).data('id');
        $.ajax({
            method : "POST",
            url : url_gb +"/admin/ImportToChainaContainer/GetContainerDetail/"+ container_id,
            dataType: 'json'
            // data : {ID : id}
        }).done(function(rec){
            $('#ModalContainerDetail').modal('show');
            var html = '';
            if(rec.Containers){
                $.each(rec.QrCodeProduct, function(key, qrcode){
                    html += '<tr>\
                                <td class="countRow">'+(key+1)+'</td>\
                                <td>'+qrcode.po_no+'</td>\
                                <td>'+qrcode.qr_code+'\
                                <input type="hidden" class="po_no" name="qrcode['+key+'][po_no]" value="'+qrcode.po_no+'">\
                                <input type="hidden" class="po_qty" name="qrcode['+key+'][po_qty]" value="'+qrcode.po_qty+'">\
                                <input type="hidden" class="lot_product_id" name="qrcode['+key+'][lot_product_id]" value="'+qrcode.lot_product_id+'">\
                                <input type="hidden" class="lot_product_qty" name="qrcode['+key+'][lot_product_qty]" value="'+qrcode.lot_product_qty+'">\
                                <input type="hidden" class="qr_code_id" name="qrcode['+key+'][qr_code_id]" value="'+qrcode.id+'">\
                                <input type="hidden" class="qr_code_name" name="qrcode['+key+'][qr_code_name]" value="'+qrcode.qr_code+'">\
                                <input type="hidden" class="weight" name="qrcode['+key+'][weight]" value="'+(qrcode.weight_per_item)+'">\
                                <input type="hidden" class="width" name="qrcode['+key+'][width]" value="'+(qrcode.width / qrcode.lot_product_qty)+'">\
                                <input type="hidden" class="length" name="qrcode['+key+'][length]" value="'+(qrcode.length / qrcode.lot_product_qty)+'">\
                                <input type="hidden" class="height" name="qrcode['+key+'][height]" value="'+(qrcode.height / qrcode.lot_product_qty)+'">\
                                <input type="hidden" class="cubic" name="qrcode['+key+'][cubic]" value="'+(qrcode.product_cobic / qrcode.lot_product_qty)+'">\
                                </td>\
                                <td>'+qrcode.lot_product_id+'</td>\
                                <td>'+qrcode.customer_general_code+'</td>\
                                <td>'+qrcode.product_type_name+'</td>\
                                <td>'+qrcode.product_name+'</td>\
                                <td>'+addNumformat((Math.round(qrcode.weight_per_item * 1000) / 1000).toFixed(2))+'</td>\
                                <td>'+addNumformat((Math.round((qrcode.product_cobic).toFixed(2)) * 1000) / 1000)+'</td>\
                            </tr>';
                            // / qrcode.lot_product_qty
                });
                $('#list-qr-code').html(html);
                showPOandLot();
            } else {
                html =  '<tr id="qr-code-not-found">\
                            <td colspan=10 style="text-align: center;">@lang('lang.data_not_found')</td>\
                        </tr>';
            }
        }).fail(function(data){
            swal("@lang('lang.system_system_error')","กรุณาติดต่อผู้ดูแล","error");
        });
    });

    function showPOandLot(){
        var product_for_show = $('#list-qr-code > tr');
        var groups = {};
        var html = '';
        if (product_for_show.length > 0) {
            $.each(product_for_show, function(k, v){
                var po = $(v).find('.po_no').val();
                var lot_id = $(v).find('.lot_product_id').val();
                var qr_code_name = $(v).find('.qr_code_name').val();
                var po_qty = $(v).find('.po_qty').val();
                var lot_product_qty = $(v).find('.lot_product_qty').val();
                if (!groups[po]) {
                    groups[po] = {};
                }
                if (!groups[po][lot_id]) {
                    groups[po][lot_id] = {};
                }
                if (!groups[po][lot_id]['amount']) {
                    groups[po][lot_id]['amount'] = [];
                }
                groups[po]['po_qty'] = po_qty;
                groups[po][lot_id]['lot_product_qty'] = lot_product_qty;
                groups[po][lot_id]['amount'].push(qr_code_name);
            });
            var No_Po = 1;
            var style = '';
            var style_lot = '';
            $.each(groups, function(k1, v1){
                var sumallproduct = 0;
                $.each(v1, function(k2, v2){
                    if (k2 != 'po_qty') {
                        sumallproduct += v2.amount.length
                    }
                });
                if(v1.po_qty > sumallproduct){
                    style = 'rgba(255, 0, 0, 0.33)';
                }else if(v1.po_qty < sumallproduct){
                    style = 'rgba(255, 235, 59, 0.5)';
                }else{
                    style = 'rgba(76, 175, 80, 0.42)';
                }
                html += '\
                    <tr style="background-color: '+style+'">\
                        <td>'+No_Po+'</td>\
                        <td>'+k1+'</td>\
                        <td class="text-center">'+addNumformat(v1.po_qty)+'</td>\
                        <td class="text-center">'+addNumformat(sumallproduct)+'</td>\
                    </tr>';
                $.each(v1, function(k2, v2){
                    if (k2 != 'po_qty') {
                        if(v2.lot_product_qty > v2.amount.length){
                            style_lot = 'rgba(255, 0, 0, 0.33)';
                        }else if(v2.lot_product_qty < v2.amount.length){
                            style_lot = 'rgba(255, 235, 59, 0.5)';
                        }else{
                            style_lot = 'rgba(76, 175, 80, 0.42)';
                        }
                        html += '\
                            <tr style="background-color: '+style+'">\
                                <td></td>\
                                <td class="text-right">Lot '+k2+'</td>\
                                <td class="text-center">'+addNumformat(v2.lot_product_qty)+'</td>\
                                <td class="text-center">'+addNumformat(v2.amount.length)+'</td>\
                            </tr>';
                    }
                });
                No_Po++;
            });
        }else{
            html += '\
                <tr>\
                    <td colspan="5" class="text-center">@lang('lang.data_not_found')</td>\
                </tr>';
        }

        $('.add-product-po').html(html)
    }

    $('#add_container_id').select2();
    $('#edit_container_id').select2();

</script>
@endsection
