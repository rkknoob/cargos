﻿@extends('Admin.layouts.layout')
@section('css_bottom')
@endsection
@section('body')
<div class="content">
    <div class="col-md-12">
    <div class="container-fluid">
        <form id="FormShipping">
                <input type="hidden" name="id" id="edit_id" value="{{ $Delivery->id }}">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="content">
                                <h4 class="title">
                                    <h4 class="modal-title" id="myModalLabel">ออกใบส่งสินค้า (ทะเบียนรถ {{ $Delivery->plate_no }})</h4>
                                </h4>
                                <div class="material-datatables">
                                    <div class="modal-header">
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="add_delivery_slip_no">บิลเลขที่</label>
                                                    <input type="text" class="form-control" name="delivery_slip[delivery_slip_no]" value="{{ $DeliverySlipNo }}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="add_delivery_slip_date">วันที่รับเข้า</label>
                                                    <div class="input-group" data-date="{{date('Y-m-d')}}">
                                                        <input type="text" value="" readonly="readonly" class="form-control" name="delivery_slip[delivery_slip_date]" id="add_delivery_slip_date"  placeholder="import_to_chaina_date">
                                                        <span class="input-group-addon remove_date_time"><i class="glyphicon glyphicon-remove icon-remove"></i></span>
                                                        <span class="input-group-addon trigger_date_time" for="add_delivery_slip_date"><i class="glyphicon glyphicon-calendar icon-calendar"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="add_user_id">เลือกลูกค้า</label>
                                                    <select name="delivery_slip[user_id]" class="select2 form-control" tabindex="-1" data-placeholder="เลือกลูกค้า" id="add_user_id">
                                                        <option value="">เลือกลูกค้า</option>
                                                        @if(!$Users->isEmpty())
                                                            @foreach($Users as $User)
                                                                <option value="{{ $User->id }}">{{ $User->firstname }} {{ $User->lastname }}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="shipping_user_address_id">เลือกที่อยู่จัดส่ง</label>
                                                    <select name="delivery_slip[user_address_id]" class="select2 form-control" tabindex="-1" data-placeholder="กรุณาเลือกลูกค้าก่อน" id="add_user_address_id" >
                                                        <option value="">กรุณาเลือกลูกค้าก่อน</option>

                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row" style="margin-top: 40px;">
                                            <div class="col-md-12">
                                                <div class="fix-scroll-dashboard">
                                                    <table class="table">
                                                        <thead>
                                                            <tr>
                                                                <th>
                                                                    <div class="form-check">
                                                                        <label for="edit_active" class="checkbox form-check-label">
                                                                            <span class="icons">
                                                                                <span class="first-icon fa fa-square"></span>
                                                                                <span class="second-icon fa fa-check-square "></span>
                                                                            </span><input type="checkbox" class="form-check-input" data-toggle="checkbox" id="check_active_all" required="" aria-required="true">
                                                                        </label>
                                                                    </div>
                                                                </th>
                                                                <th>เลข PO</th>
                                                                <th>จำนวนชิ้นตาม PO</th>
                                                                <th>จำนวนชิ้นที่ปิดตู้มา</th>
                                                                <th>จำนวนชิ้นตามจริง</th>
                                                                <th>น้ำหนักรวม</th>
                                                                <th>ขนาดคิว</th>
                                                                <th>จัดส่งภายในวันที่</th>
                                                                <th>วันที่รับเข้าโกดังไทย</th>
                                                                <th>วันที่รับของเข้าที่จีน</th>
                                                                <th>หมายเหตุ</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="list-po-products">

                                                        </tbody>
                                                        <tfooter>
                                                            <tr id="list-po-products-not-found">
                                                                <td colspan="10" style="text-align: center;">ไม่พบข้อมูล</td>
                                                            </tr>

                                                        </tfooter>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-primary">พิมพ์</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </form>
    </div>
</div>
@endsection
@section('js_bottom')
<script src="{{asset('assets/global/plugins/orakuploader/orakuploader.js')}}"></script>
<script>
    $("#add_delivery_slip_date").daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        minYear: 1901,
        startDate: '{{date("Y-m-d")}}',
        // maxYear: parseInt(moment().format('YYYY'),10),
        locale: {
            format: 'YYYY-MM-DD'
        }
    }, function(start, end, label) {
            var years = moment().diff(start, 'years');
            // alert("You are " + years + " years old!");
    });

    $('body').on('change','#add_user_id',function(data){
        $('#list-po-products').html('');
        var user_id = $(this).val();
        var delivery_id = $('#edit_id').val();
        $.ajax({
            method : "GET",
            url : url_gb+"/admin/Shipping/GetUserPo/"+user_id,
            dataType : 'json',
            data: {
                delivery_id: delivery_id,
            }
        }).done(function(rec){
            var html = '';
            $.each(rec.ImportToChainas, function( k, v ) {
                var qty_po = 0;
                var weight_all_po = 0;
                var cubic_po = 0;
                html += '<tr>\
                            <td>\
                                <div class="form-check">\
                                    <label for="add_active" class="checkbox form-check-label label-check-po">\
                                        <span class="icons">\
                                            <span class="first-icon fa fa-square"></span>\
                                            <span class="second-icon fa fa-check-square"></span>\
                                        </span>\
                                        <input type="checkbox" class="form-check-input input-check-po" data-toggle="checkbox" name="delivery_to_user_id[]" required="" value="'+v.delivery_to_user_id+'" aria-required="true">\
                                    </label>\
                                </div>\
                            </td>\
                            <td>'+v.po_no+'</td>\
                            <td>'+addNumformat(v.qty_chaina)+'</td>\
                            <td>'+addNumformat(v.qty_container)+'</td>\
                            <td>'+addNumformat(v.qty_thai)+'</td>\
                            <td>'+addNumformat(v.weight_all)+'</td>\
                            <td>'+addNumformat(v.cubic)+'</td>\
                            <td>'+v.delivery_date+'</td>\
                            <td>'+v.import_to_thai_date+'</td>\
                            <td>'+v.import_to_chaina_date+'</td>\
                            <td><input type="text" class="form-control" id="" value=""></td>\
                        </tr>';
            });

            if(rec.ImportToChainas.length > 0){
                $('#list-po-products').append(html);
                $('#list-po-products-not-found').hide();
            }else{
                $('#list-po-products-not-found').show();
            }

            ShowModal('ModalShipping');
        }).fail(function(){
            swal("system.system_alert","system.system_error","error");
        });
    });

    $('body').on('change','#check_active_all',function(data){
        var active = $('#check_active_all').closest("label.checked");
        var input_check = $('#list-po-products').find('label.label-check-po');
        if(active.length > 0){
            $('.label-check-po').addClass('checked');
            $(".input-check-po").prop( "checked", true );
        }else{
            $('.label-check-po').removeClass('checked');
            $(".input-check-po").prop( "checked", false );
        }
    });

    $('#FormShipping').validate({
        errorElement: 'div',
        errorClass: 'invalid-feedback',
        focusInvalid: false,
        rules: {
            'delivery_slip[user_id]': {
                required: true,
            },
            'delivery_slip[user_address_id]': {
                required: true,
            },
        },
        messages: {
            'delivery_slip[user_id]': {
                required: "กรุณาระบุ",
            },
            'delivery_slip[user_address_id]': {
                required: "กรุณาระบุ",
            },
        },
        highlight: function (e) {
            validate_highlight(e);
        },
        success: function (e) {
            validate_success(e);
        },

        errorPlacement: function (error, element) {
            validate_errorplacement(error, element);
        },
        submitHandler: function (form) {
            /*
            if(CKEDITOR!==undefined){
                for ( instance in CKEDITOR.instances ){
                    CKEDITOR.instances[instance].updateElement();
                }
            }
            */
            var btn = $(form).find('[type="submit"]');
            var delivery_id = $('#edit_id').val();
            btn.button("loading");
            $.ajax({
                method : "post",
                url : url_gb+"/admin/Shipping",
                dataType : 'json',
                data : $(form).serialize()
            }).done(function(rec){
                btn.button("reset");
                if(rec.status==1){
                    console.log('success');
                    resetFormCustom(form);
                    swal(rec.title,rec.content,"success");
                    window.location.href = url_gb+"/admin/Shipping/"+delivery_id;
                    //$('#ModalEdit').modal('hide');
                }else{
                    swal(rec.title,rec.content,"error");
                }
            }).fail(function(){
                swal("system.system_alert","system.system_error","error");
                btn.button("reset");
            });
        },
        invalidHandler: function (form) {

        }
    });


    $('body').on('change','#add_user_id',function(){
       var user_id = $('#add_user_id').val();
       $("#add_user_address_id").html('');
       $.ajax({
           method : "GET",
           url: url_gb + "/admin/User/GetAddress/"+user_id,
           dataType : 'json',
       }).done(function(res){
           $('#add_user_address_id').append('<option value="">กรุณาเลือกที่อยู่จัดส่ง</option>');
           $.each(res,function(k,v){
               $('#add_user_address_id').append('<option value="'+v.id+'" >(ชื่อ '+v.name+') ที่อยู่'+v.address+' อำเภอ'+v.amphure.amphure_name+' จังหวัด'+v.province.province_name+' '+v.amphure.zipcode+'</option>');
           });
       }).fail(function(){

       });
    });


    $('#add_user_id').select2();
    $('#add_user_address_id').select2();
</script>
@endsection
