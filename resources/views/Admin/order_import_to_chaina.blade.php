﻿@extends('Admin.layouts.layout')
@section('css_bottom')
<style>
.fix-scroll-dashboard {
    width: 100%;
    overflow-x: scroll;
}
</style>
@endsection
@section('body')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="content">
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                            <h4 class="title">
                            {{$title_page or '' }}

                        </h4>
                        </div>
                        <div class="material-datatables fix-scroll-dashboard">
                            <div class="table-responsive">
                                <table id="TableList" class="table table-striped table-no-bordered table-hover" style="width:100%;cellspacing:0">
                                    <thead>
                                        <tr>
                                            <th>@lang('lang.no')</th>
                                            <th>@lang('lang.customer_id')</th>
                                            <th>@lang('lang.number_po')</th>
                                            <th>@lang('lang.quantity')</th>
                                            <th>@lang('lang.weight_kg')</th>
                                            <th>@lang('lang.price')</th>
                                            <th>@lang('lang.producttype')</th>
                                            <!-- <th>@lang('lang.total_queue')</th> -->
                                            <!-- <th>@lang('lang.status')</th> -->
                                            <th>CBS</th>
                                            <th>@lang('lang.import_date')</th>
                                            <th>@lang('lang.status_rate')</th>
                                            <th>@lang('lang.tool')</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js_bottom')
<script src="{{asset('assets/global/plugins/orakuploader/orakuploader.js')}}"></script>
<script>
     var TableList = $('#TableList').dataTable({
        "ajax": {
            "type": "POST",
            "url": url_gb+"/admin/{{$lang}}/OrderImportToChaina/Lists",
            "data": function ( d ) {
                //d.myKey = "myValue";
                // d.custom = $('#myInput').val();
                // etc
            }
        },
        "order": [[ 9, "desc" ]],
        "columns": [
            {"data": "DT_Row_Index", "className": "text-center", "searchable": false, "orderable": false  },
            {"data": "customer_general_code", "name": "users.customer_general_code"},
            {"data": "po_no"},
            {"data": "qty_all_PO", "className":"action text-center", "searchable" : false},
            {"data": "weight_all_PO", "className":"action text-right", "searchable" : false},
            {"data": "price", "className":"action text-right", "searchable" : false, "orderable": false  },
            {"data": "product_type_name", "className":"action text-right", "searchable" : false},
            {"data": "qty_all_cubic", "className":"action text-right", "searchable" : false},
            // {"data": "status_id", "searchable" : false},
            {"data": "import_date"},
            {"data": "status_rate"},
            {"data": "action","className":"action text-center","searchable" : false, "orderable" : false }
        ]
    });
</script>
@endsection
