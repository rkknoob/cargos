﻿@extends('Admin.layouts.layout')
@section('css_bottom')
@endsection
@section('body')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="content">
                        <div class="row" style="padding-bottom: 15px;">
                            <div class="col"></div>
                            <div class="col-6">
                                @if(!empty($Container))
                                    <a href="{{ url('/admin/'.$lang.'/ImportToChainaContainer/edit/'.$Container->id) }}" class="btn btn-success btn-add pull-right" >
                                        + <i class="fa fa-desktop" style="font-size: 20px;"></i>
                                    </a>
                                    <a href="{{ url('/admin/'.$lang.'/ImportToChainaContainer/MobileEdit/'.$Container->id) }}" class="btn btn-success btn-add pull-right" >
                                        + <i class="fa fa-mobile" style="font-size: 25px;"></i>&nbsp&nbsp
                                    </a>
                                @else
                                    <a href="{{ url('/admin/'.$lang.'/ImportToChainaContainer/create') }}" class="btn btn-success btn-add pull-right" >
                                        + <i class="fa fa-desktop" style="font-size: 20px;"></i>
                                    </a>
                                    <a href="{{ url('/admin/'.$lang.'/ImportToChainaContainer/MobileCreate') }}" class="btn btn-success btn-add pull-right" >
                                        + <i class="fa fa-mobile" style="font-size: 25px;"></i>&nbsp&nbsp
                                    </a>
                                @endif
                            </div>
                        </div>
                        <!-- Nav tabs -->
                        <ul class="nav nav-pills nav-fill col-12">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#container">@lang('lang.data_container')</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#barcode">@lang('lang.scan_product')</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#summarize">@lang('lang.conclude')</a>
                            </li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div id="container" class="container tab-pane active"><br>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-md-4 text-right">
                                                @lang('lang.type_delivery')
                                            </div>
                                            <div class="col-md-8">
                                                @foreach($TransportTypes as $key => $TransportType)
                                                    @if(!empty($Container->transport_type_id))
                                                        <label class="radio-inline">
                                                            <input {{$Container->transport_type_id == $TransportType->id ? 'checked' : ''}} type="radio" class="transport_types" name="transport_types" data-nameshow="{{$TransportType->{'name_'.$lang} }}" value="{{$TransportType->id}}"> {{$TransportType->{'name_'.$lang} }}
                                                        </label>
                                                    @else
                                                        <label class="radio-inline">
                                                            <input {{$key == 0 ? 'checked' : ''}} type="radio" class="transport_types" name="transport_types" data-nameshow="{{$TransportType->{'name_'.$lang} }}" value="{{$TransportType->id}}"> {{$TransportType->{'name_'.$lang} }}
                                                        </label>
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-md-4 text-right">
                                                @lang('lang.type_container')
                                            </div>
                                            <div class="col-md-8">
                                                @foreach($ContainerTypes as  $key => $ContainerType)
                                                    @if(!empty($Container->container_type_id))
                                                        <label class="radio-inline">
                                                            <input {{$Container->container_type_id == $ContainerType->id ? 'checked' : ''}} type="radio" class="container_types" name="container_types" data-nameshow="{{$ContainerType->{'name_'.$lang} }}" value="{{$ContainerType->id}}"> {{$ContainerType->{'name_'.$lang} }}
                                                        </label>
                                                    @else
                                                        <label class="radio-inline">
                                                            <input {{$key == 0 ? 'checked' : ''}} type="radio" class="container_types" name="container_types" data-nameshow="{{$ContainerType->{'name_'.$lang} }}" value="{{$ContainerType->id}}"> {{$ContainerType->{'name_'.$lang} }}
                                                        </label>
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-md-4 text-right">
                                                @lang('lang.size_container')
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control container_sizes" name="container_sizes">
                                                    <option value="">--@lang('lang.size_container')--</option>
                                                    @foreach($ContainerSizes as $ContainerSize)
                                                        @if(!empty($Container->container_size_id) && $Container->container_size_id == $ContainerSize->id)
                                                            <option selected="" data-nameshow="{{$ContainerSize->name}}" value="{{$ContainerSize->id}}">{{$ContainerSize->name}}</option>
                                                        @else
                                                            <option data-nameshow="{{$ContainerSize->name}}" value="{{$ContainerSize->id}}">{{$ContainerSize->name}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <div class="col-md-4 text-right">
                                                @lang('lang.container_id')
                                            </div>
                                            <div class="col-md-8">
                                                <input class="form-control" type="text" name="container_code" value="{{$ContainerCode}}" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <a class="pull-right next-tab btn btn-success" data-numtab="1">@lang('lang.save')</a>
                            </div>
                            <div id="barcode" class="container tab-pane fade"><br>
                                <div class="row">
                                    <form class="scan_barcode col-md-6">
                                        <div class="input-group">
                                            @lang('lang.barcode') &nbsp;
                                            <input type="text" class="form-control scan-barcode" value="">
                                            <button type="submit" class="btn btn-success enter-qrcode">
                                                @lang('lang.scan_product')
                                            </button>
                                        </div>
                                    </form>
                                </div>
                                <div class="row table-data-product">
                                    <!-- table scan -->
                                    <div class="col-md-12">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <td>#</td>
                                                    <td>@lang('lang.number_po')</td>
                                                    <td>
                                                        @lang('lang.qrcode')
                                                        @if(isset($Container))
                                                        <a href="{{ url('/admin/'.$lang.'/ImportToChainaContainer/edit/'.$Container->id) }}"><i class="fa fa-sort-down"></i></a>
                                                        @else
                                                            <a id="url-container"><i class="fa fa-sort-down"></i></a>
                                                        @endif
                                                    </td>
                                                    <td>@lang('lang.lot_id')</td>
                                                    <td>@lang('lang.customer_id')</td>
                                                    <td>@lang('lang.type_product')</td>
                                                    <td>@lang('lang.product_name')</td>
                                                    <td>@lang('lang.weigh')</td>
                                                    <td>@lang('lang.queue_size')</td>
                                                    <td></td>
                                                </tr>
                                            </thead>
                                            <tbody id="list-qr-code">
                                                @if(!empty($RelProductContainer) && count($RelProductContainer) > 0)
                                                    @foreach($RelProductContainer as $key => $value)
                                                    <tr>
                                                        <td class="countRow">{{ $key+1 }}</td>
                                                        <td>{{ $value->po_no }}</td>
                                                        <td>{{ $value->qr_code }}
                                                            <!-- name="qrcode[{{ $value->id }}][rel_container_product_id]"
                                                            name="qrcode[{{ $value->id }}][po_no]"
                                                            name="qrcode[{{ $value->id }}][po_qty]"
                                                            name="qrcode[{{ $value->id }}][lot_product_id]"
                                                            name="qrcode[{{ $value->id }}][lot_product_qty]"
                                                            name="qrcode[{{ $value->id }}][qr_code_id]"
                                                            name="qrcode[{{ $value->id }}][qr_code_name]"
                                                            name="qrcode[{{ $value->id }}][weight]"
                                                            name="qrcode[{{ $value->id }}][width]"
                                                            name="qrcode[{{ $value->id }}][length]"
                                                            name="qrcode[{{ $value->id }}][height]"
                                                            name="qrcode[{{ $value->id }}][cubic]" -->
                                                            <input type="hidden" class="rel_container_product_id" value="{{ $value->rel_container_product_id }}">
                                                            <input type="hidden" class="po_no" value="{{ $value->po_no }}">
                                                            <input type="hidden" class="customer_general_code" value="{{ $value->customer_general_code }}">
                                                            <input type="hidden" class="po_qty" value="{{ $value->po_qty }}">
                                                            <input type="hidden" class="lot_product_id" value="{{ $value->lot_product_id }}">
                                                            <input type="hidden" class="lot_product_qty" value="{{ $value->lot_product_qty }}">
                                                            <input type="hidden" class="qr_code_id" value="{{ $value->id }}">
                                                            <input type="hidden" class="qr_code_name" value="{{ $value->qr_code }}">
                                                            <input type="hidden" class="weight" value="{{ ($value->weight_per_item) }}">
                                                            <input type="hidden" class="width" value="{{ ($value->width / $value->lot_product_qty) }}">
                                                            <input type="hidden" class="length" value="{{ ($value->length / $value->lot_product_qty) }}">
                                                            <input type="hidden" class="height" value="{{ ($value->height / $value->lot_product_qty) }}">
                                                            <input type="hidden" class="cubic" value="{{ ($value->product_cobic / $value->lot_product_qty) }}">
                                                        </td>
                                                        <td>{{ $value->lot_product_id }}</td>
                                                        <td>{{ $value->customer_general_code }}</td>
                                                        <td>{{ $value->product_type_name }}</td>
                                                        <td>{{ $value->product_name }}</td>
                                                        <td>{{ number_format(($value->weight_per_item * 1000) / 1000, 2) }}</td>
                                                        <td>{{ number_format((($value->product_cobic / $value->lot_product_qty)* 1000) / 1000, 2) }}</td>
                                                        <td><a class="btn btn-danger btn-delete">-</a></td>
                                                    </tr>
                                                    @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- table summarize -->
                                    <div class="col-md-12">
                                        <table class="table" style="width: 100%;">
                                            <thead>
                                                <tr>
                                                    <td class="text-center">-</td>
                                                    <td class="text-center">#</td>
                                                    <td class="text-center">@lang('lang.number_po')</td>
                                                    <td class="text-center">@lang('lang.customer_id')</td>
                                                    <td class="text-center"></td>
                                                    <td class="text-center">@lang('lang.number_of_po')</td>
                                                    <td class="text-center">@lang('lang.actual_number')</td>
                                                </tr>
                                            </thead>
                                            <tbody class="add-product-po">
                                                <tr>
                                                    <td class="text-center" colspan="7">@lang('lang.data_not_found')</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <a class="pull-right next-tab btn btn-success" data-numtab="2">@lang('lang.next')</a>
                            </div>
                            <div id="summarize" class="container tab-pane fade"><br>
                                <form id="FormAddContainer" method="post">
                                    <input type="hidden" class="container_id" name="container_id" value="{{$Container->id or ''}}">
                                    <div class="row">
                                        <!-- head summarize -->
                                        <div class="row col-md-6">
                                            <div class="col-md-12 text-right">
                                                @lang('lang.type_delivery') &nbsp;
                                                <input type="hidden" class="transport_types_send" name="transport_types" value="">
                                                <span id="show-transport_types"></span>
                                            </div>
                                        </div>
                                        <div class="col"></div>
                                    </div>
                                    <div class="row">
                                        <div class="row col-md-6">
                                            <div class="col-md-12 text-right">
                                                @lang('lang.type_container') &nbsp;
                                                <input type="hidden" class="container_types_send" name="container_types" value="">
                                                <span id="show-container_types"></span>
                                            </div>
                                        </div>
                                        <div class="col"></div>
                                    </div>
                                    <div class="row">
                                        <div class="row col-md-6">
                                            <div class="col-md-12 text-right">
                                                @lang('lang.container_id') &nbsp;
                                                <input type="hidden" class="container_code_send" name="container_code" value="{{$ContainerCode}}">
                                                <span id="show-container_code">{{$ContainerCode}}</span>
                                            </div>
                                        </div>
                                        <div class="col"></div>
                                    </div>
                                    <div class="row">
                                        <div class="row col-md-6">
                                            <div class="col-md-12 text-right">
                                                @lang('lang.tatal_weigth') &nbsp;
                                                <input type="hidden" class="container_sizes_send" name="container_sizes" value="">
                                                <span id="show-weight"></span>
                                            </div>
                                        </div>
                                        <div class="col"></div>
                                    </div><br>
                                        <!-- table summarize -->
                                    <div class="row table-data-product">
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="container_remark">
                                                    @lang('lang.note') :
                                                </label>
                                                    <textarea class="form-control" id="container_remark" name="container_remark" rows="2">{{$Container->remark or ''}}</textarea>
                                            </div>
                                            <div class="form-group">
                                                <label for="container_tag">
                                                    @lang('lang.tag_close_container') :
                                                </label>
                                                <input class="form-control" id="container_tag" name="container_tag" value="{{$Container->tag or ''}}">
                                            </div>
                                            <div class="form-group">
                                                <label for="container_tag">
                                                    @lang("lang.container_no") :
                                                </label>
                                                <input class="form-control" id="container_no" name="container_no" value="{{$Container->container_no or ''}}">
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-sm-12">
                                                    <label for="add_before_close_container_img">@lang('lang.before_picture_close_container')</label>
                                                    <div id="orak_add_before_close_container_img">
                                                        <div id="add_before_close_container_img" orakuploader="on"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-sm-12">
                                                    <label for="add_after_close_container_img">@lang('lang.After_picture_close_container')</label>
                                                    <div id="orak_add_after_close_container_img">
                                                        <div id="add_after_close_container_img" orakuploader="on"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-sm-12">
                                                    <label for="add_tag_container_img">@lang('lang.picture_tag_close_container')</label>
                                                    <div id="orak_add_tag_container_img">
                                                        <div id="add_tag_container_img" orakuploader="on"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <button type="submit" class="btn btn-success pull-right">@lang('lang.save')</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('modal')
@endsection
@section('js_bottom')
<script src="{{asset('assets/global/plugins/orakuploader/orakuploader.js')}}"></script>
<script>
    $(function(){
        showPOandLot();
    });

    $('body').on('click', '.next-tab', function(){
        var next_tab = $(this).attr('data-numtab');
        var nav_bar = $('.nav > .nav-item > .nav-link');
        var tab_pane = $('.tab-pane');

        var container_id = $('.container_id').val();
        var transport_types = $('.transport_types:checked').val();
        var container_types = $('.container_types:checked').val();
        var container_sizes = $('.container_sizes').val();
        if(next_tab == 1){
            if(!container_id){
                if(transport_types && container_types && container_sizes){
                    $.ajax({
                        method : "POST",
                        url : url_gb+"/admin/ImportToChainaContainer/ContainerSave",
                        dataType : 'json',
                        data : {
                            'transport_types' : transport_types,
                            'container_types' : container_types,
                            'container_sizes' : container_sizes,
                        }
                    }).done(function(rec){
                        if(rec.status==1){
                            swal(rec.title, rec.content, "success");
                            $('.container_id').val(rec.id_container);
                            $('#url-container').attr('href', url_gb+"/admin/{{$lang}}/ImportToChainaContainer/edit/"+rec.id_container);
                            $(nav_bar).removeClass("active");
                            $(nav_bar[next_tab]).addClass("active");
                            $.each(tab_pane, function(k, v){
                                $(v).removeClass("active");
                                $(v).removeClass("fade");
                                if(k == next_tab){
                                    $(v).addClass("active");
                                }else{
                                    $(v).addClass("fade");
                                }
                            });
                        }else{
                            swal(rec.title,rec.content,"warning");
                        }
                    }).fail(function(){
                        swal("system.system_alert","@lang('lang.system_system_error')","error");
                    });
                }
                else{
                    swal('สร้างตู้','กรุณากรอกข้อมูลให้ครบถ้วน',"warning");
                }
            }
            else{
                $(nav_bar).removeClass("active");
                $(nav_bar[next_tab]).addClass("active");
                $.each(tab_pane, function(k, v){
                    $(v).removeClass("active");
                    $(v).removeClass("fade");
                    if(k == next_tab){
                        $(v).addClass("active");
                    }else{
                        $(v).addClass("fade");
                    }
                });
            }
        }
        else{
            $(nav_bar).removeClass("active");
            $(nav_bar[next_tab]).addClass("active");
            $.each(tab_pane, function(k, v){
                $(v).removeClass("active");
                $(v).removeClass("fade");
                if(k == next_tab){
                    $(v).addClass("active");
                }else{
                    $(v).addClass("fade");
                }
            });
        }
    });

    $('body').on('change', '.container_sizes', function(){
        var tag_name = '#show-'+ $(this).attr("name");
        var name_show = $(this).find(':selected').data('nameshow');
        $(tag_name).html(name_show);
    });

    $('.scan_barcode').submit(function(e){
        var container_id = $('.container_id').val();
        e.preventDefault();
        var qrcode = $('.scan-barcode').val();
        if(container_id){
            $.ajax({
                method : "POST",
                url : url_gb + "/admin/ImportToChainaContainer/GetQRCode",
                dataType : 'json',
                data : {
                    'container_id' : container_id,
                    'qrcode' : qrcode
                }
            }).done(function(rec){
                var CheckQrCode = $('body').find('#barcode').find(".qr_code_name[value='"+qrcode+"']").length;
                if (CheckQrCode <= 0) {
                    if(rec.QrCodeProduct){
                        // name="qrcode['+rec.QrCodeProduct.id+'][rel_container_product_id]"
                        // name="qrcode['+rec.QrCodeProduct.id+'][po_no]"
                        // name="qrcode['+rec.QrCodeProduct.id+'][po_qty]"
                        // name="qrcode['+rec.QrCodeProduct.id+'][lot_product_id]"
                        // name="qrcode['+rec.QrCodeProduct.id+'][lot_product_qty]"
                        // name="qrcode['+rec.QrCodeProduct.id+'][qr_code_id]"
                        // name="qrcode['+rec.QrCodeProduct.id+'][qr_code_name]"
                        // name="qrcode['+rec.QrCodeProduct.id+'][weight]"
                        // name="qrcode['+rec.QrCodeProduct.id+'][width]"
                        // name="qrcode['+rec.QrCodeProduct.id+'][length]"
                        // name="qrcode['+rec.QrCodeProduct.id+'][height]"
                        // name="qrcode['+rec.QrCodeProduct.id+'][cubic]"
                        var html = '<tr>\
                                        <td class="countRow"></td>\
                                        <td>'+rec.QrCodeProduct.po_no+'</td>\
                                        <td>'+rec.QrCodeProduct.qr_code+'\
                                            <input type="hidden" class="rel_container_product_id" value="'+rec.rel_container_product_id+'">\
                                            <input type="hidden" class="po_no" value="'+rec.QrCodeProduct.po_no+'">\
                                            <input type="hidden" class="customer_general_code" value="'+rec.QrCodeProduct.customer_general_code+'">\
                                            <input type="hidden" class="po_qty" value="'+rec.QrCodeProduct.po_qty+'">\
                                            <input type="hidden" class="lot_product_id" value="'+rec.QrCodeProduct.lot_product_id+'">\
                                            <input type="hidden" class="lot_product_qty" value="'+rec.QrCodeProduct.lot_product_qty+'">\
                                            <input type="hidden" class="qr_code_id" value="'+rec.QrCodeProduct.id+'">\
                                            <input type="hidden" class="qr_code_name" value="'+rec.QrCodeProduct.qr_code+'">\
                                            <input type="hidden" class="weight" value="'+(rec.QrCodeProduct.weight_per_item)+'">\
                                            <input type="hidden" class="width" value="'+(rec.QrCodeProduct.width / rec.QrCodeProduct.lot_product_qty)+'">\
                                            <input type="hidden" class="length" value="'+(rec.QrCodeProduct.length / rec.QrCodeProduct.lot_product_qty)+'">\
                                            <input type="hidden" class="height" value="'+(rec.QrCodeProduct.height / rec.QrCodeProduct.lot_product_qty)+'">\
                                            <input type="hidden" class="cubic" value="'+(rec.QrCodeProduct.product_cobic / rec.QrCodeProduct.lot_product_qty)+'">\
                                        </td>\
                                        <td>'+rec.QrCodeProduct.lot_product_id+'</td>\
                                        <td>'+rec.QrCodeProduct.customer_general_code+'</td>\
                                        <td>'+rec.QrCodeProduct.product_type_name+'</td>\
                                        <td>'+rec.QrCodeProduct.product_name+'</td>\
                                        <td>'+addNumformat(Math.round(rec.QrCodeProduct.weight_per_item * 1000) / 1000)+'</td>\
                                        <td>'+addNumformat(Math.round((rec.QrCodeProduct.product_cobic / rec.QrCodeProduct.lot_product_qty)* 1000) / 1000)+'</td>\
                                        <td><a class="btn btn-danger btn-delete">-</a></td>\
                                    </tr>';
                        $('#list-qr-code').append(html)
                        $('.scan-barcode').val('');
                        countRow();
                        showPOandLot();
                    } else {
                        swal("ScanQRcode","ไม่พบ QR Code นี้","warning");
                        $('.scan-barcode').val('');
                    }
                } else {
                    swal("ScanQRcode","QR Code นี้ Scan ไปแล้ว","warning");
                    $('.scan-barcode').val('');
                }

            }).fail(function(){
                swal("system.system_alert","@lang('lang.system_system_error')","error");
            });
        }else{
            swal("ScanQRcode", "กรุณากดบันทึกตู้", "warning");
        }
    });

    $('body').on('click', '.btn-delete', function(){
        var id_product = $(this).closest('tr').find('.rel_container_product_id').val();
        $(this).closest('tr').remove();
        $.ajax({
            method : "POST",
            url : url_gb+"/admin/ImportToChainaContainer/destroyContainerProduct",
            dataType : 'json',
            data : {
                'id_product' : id_product,
            }
        }).done(function(rec){
            if(rec.status==1){
                countRow();
                showPOandLot();
                // swal(rec.title, rec.content, "success");
                // $('.container_id').val(rec.id_container);
                // $(nav_bar).removeClass("active");
                // $(nav_bar[next_tab]).addClass("active");
                // $.each(tab_pane, function(k, v){
                //     $(v).removeClass("active");
                //     $(v).removeClass("fade");
                //     if(k == next_tab){
                //         $(v).addClass("active");
                //     }else{
                //         $(v).addClass("fade");
                //     }
                // });
            }else{
                // swal(rec.title,rec.content,"warning");
            }
        }).fail(function(){
            swal("system.system_alert","@lang('lang.system_system_error')","error");
        });
    });

    $('body').on('click', '.btn-delete-po', function(){
        var po = $(this).attr('data-po');
        $('.po_no[value="'+po+'"]').closest('tr').remove();
        // $.each( $('.po_no') , function(k, v){
        //     if($(v).val() == po)
        //     $(this).closest('tr').remove();
        // });
        countRow();
        showPOandLot();
    });

    $('body').on('click', '.nav-link, .next-tab', function(){
        // set show data
        var radio_transport_types = $('#container').find('input[name="transport_types"]:checked');
        var radio_container_types = $('#container').find('input[name="container_types"]:checked');
        var transport_types_show = $(radio_transport_types).attr('data-nameshow');
        var container_types_show = $(radio_container_types).attr('data-nameshow');
        $('#show-transport_types').html(transport_types_show);
        $('#show-container_types').html(container_types_show);
        // set show weight
        var sum_weight = 0;
        var sum_cubic = 0;
        var weight = $('#barcode').find('.weight');
        var cubic = $('#barcode').find('.cubic');
        $.each($(weight), function(k, v){
            sum_weight += parseFloat($(v).val());
            sum_cubic += parseFloat($(cubic[k]).val());
        });
        $('#show-weight').html( ((Math.round(sum_weight * 1000))/1000) +' kg @lang("lang.total_size") '+ ((Math.round(sum_cubic * 1000))/1000) +' @lang("lang.queue")');
        // set input data
        var transport_types = $(radio_transport_types).val();
        var container_types = $(radio_container_types).val();
        var container_sizes = $('.container_sizes').val();
        $('.transport_types_send').val(transport_types);
        $('.container_types_send').val(container_types);
        $('.container_sizes_send').val(container_sizes);
        // set data all product to scan
        var data_product = $('#barcode').find('.table-data-product').html();
        $('#summarize').find('.table-data-product').html(data_product);
        $('#summarize').find('.btn-danger').remove();

    });

    $('#FormAddContainer').validate({
        errorElement: 'div',
        errorClass: 'invalid-feedback',
        focusInvalid: false,
        rules: {
            container_tag: {
                required: true,
            },
            // "before_close_container_img[]": {
            //     required: true,
            // },
            // "after_close_container_img[]": {
            //     required: true,
            // },
            // "tag_container_img[]": {
            //     required: true,
            // },
        },
        messages: {
            container_tag: {
                required: "@lang('lang.please_specify')",
            },
            // "before_close_container_img[]": {
            //     required: "@lang('lang.please_specify')",
            // },
            // "after_close_container_img[]": {
            //     required: "@lang('lang.please_specify')",
            // },
            // "tag_container_img[]": {
            //     required: "@lang('lang.please_specify')",
            // },
        },
        highlight: function (e) {
            validate_highlight(e);
        },
        success: function (e) {
            validate_success(e);
        },

        errorPlacement: function (error, element) {
            validate_errorplacement(error, element);
        },
        submitHandler: function (form) {
            /*
            if(CKEDITOR!==undefined){
                for ( instance in CKEDITOR.instances ){
                    CKEDITOR.instances[instance].updateElement();
                }
            }
            */
            if ($('.countRow').length > 0) {
                var btn = $(form).find('[type="submit"]');
                var data_ar = removePriceFormat(form,$(form).serializeArray());
                btn.button("loading");
                $.ajax({
                    method : "POST",
                    url : url_gb+"/admin/ImportToChainaContainer/create",
                    dataType : 'json',
                    data : $(form).serialize()
                }).done(function(rec){
                    if(rec.status==1){
                        swal(rec.title,rec.content,"success");
                        window.location.href = url_gb + '/admin/{{$lang}}/ImportToChainaContainer'
                    }else{
                        swal(rec.title,rec.content,"warning");
                    }
                }).fail(function(){
                    swal("system.system_alert","@lang('lang.system_system_error')","error");
                });
            } else {
                swal("รับเข้าตู้สินค้า","ไม่มีสินค้า","warning");
            }
        },
        invalidHandler: function (form) {

        }
    });

    function showPOandLot(){
        var product_for_show = $('body').find('#barcode').find('#list-qr-code > tr');
        delete groups;
        var groups = {};
        // console.log(product_for_show);
        // console.log(product_for_show);
        var html = '';
        if (product_for_show.length > 0) {
            $.each(product_for_show, function(k, v){
                var po = $(v).find('.po_no').val();
                var lot_id = $(v).find('.lot_product_id').val();
                var qr_code_name = $(v).find('.qr_code_name').val();
                var po_qty = $(v).find('.po_qty').val();
                var customer_general_code = $(v).find('.customer_general_code').val();
                var lot_product_qty = $(v).find('.lot_product_qty').val();
                if (!groups[po]) {
                    groups[po] = {};
                }
                if (!groups[po][lot_id]) {
                    groups[po][lot_id] = {};
                }
                if (!groups[po][lot_id]['amount']) {
                    groups[po][lot_id]['amount'] = [];
                }
                groups[po]['po_qty'] = po_qty;
                groups[po][lot_id]['customer_general_code'] = customer_general_code;
                groups[po][lot_id]['lot_product_qty'] = lot_product_qty;
                groups[po][lot_id]['amount'].push(qr_code_name);

            });
            var No_Po = 1;
            var style = '';
            var style_lot = '';
            $.each(groups, function(k1, v1){
                var sumallproduct = 0;
                $.each(v1, function(k2, v2){
                    if (k2 != 'po_qty') {
                        sumallproduct += v2.amount.length;
                    }
                });
                if(v1.po_qty > sumallproduct){
                    style = 'rgba(255, 0, 0, 0.33)';
                }else if(v1.po_qty < sumallproduct){
                    style = 'rgba(255, 235, 59, 0.5)';
                }else{
                    style = 'rgba(76, 175, 80, 0.42)';
                }
                
                html += '\
                    <tr style="background-color: '+style+'">\
                        <td><a class="btn-delete-po btn btn-danger" data-po="'+k1+'">-</a></td>\
                        <td>'+No_Po+'</td>\
                        <td>'+k1+'</td>\
                        <td></td>\
                        <td></td>\
                        <td class="text-center">'+addNumformat(v1.po_qty)+'</td>\
                        <td class="text-center">'+addNumformat(sumallproduct)+'</td>\
                    </tr>';
                $.each(v1, function(k2, v2){
                    if (k2 != 'po_qty') {
                        if(v2.lot_product_qty > v2.amount.length){
                            style_lot = 'rgba(255, 0, 0, 0.33)';
                        }else if(v2.lot_product_qty < v2.amount.length){
                            style_lot = 'rgba(255, 235, 59, 0.5)';
                        }else{
                            style_lot = 'rgba(76, 175, 80, 0.42)';
                        }
                        html += '\
                            <tr style="background-color: '+style_lot+'">\
                                <td></td>\
                                <td></td>\
                                <td></td>\
                                <td>'+v2.customer_general_code+'</td>\
                                <td class="text-right">Lot '+k2+'</td>\
                                <td class="text-center">'+addNumformat(v2.lot_product_qty)+'</td>\
                                <td class="text-center">'+addNumformat(v2.amount.length)+'</td>\
                            </tr>';
                    }
                });
                No_Po++;
            });
        }else{
            html += '\
                <tr>\
                    <td colspan="7" class="text-center">@lang('lang.data_not_found')</td>\
                </tr>';
        }

        $('.add-product-po').html(html)
    }

    function countRow(){
        if ($('.countRow').length > 0) {
            $('#qr-code-not-found').hide();
            $.each($('.countRow'), function(k, v){
                $(v).html(k+1)
            });
        } else {
            $('#qr-code-not-found').show();
        }
    }
    // set OrakUploader
    var photo_type_1 = '{{$photo_type_container_1 or ""}}';
    var photo_type_2 = '{{$photo_type_container_2 or ""}}';
    var photo_type_3 = '{{$photo_type_container_3 or ""}}';
    console.log(photo_type_1);
    console.log(photo_type_2);
    console.log(photo_type_3);
    // type 1
    if (photo_type_1) {
        var max_file_1 = 0;
        var file_1 = [];
        file_1[0] = photo_type_1;
        var photo_1 = photo_type_1;
    } else {
        var max_file_1 = 1;
        var file_1 = [];
        var photo_1 = photo_type_1;
    }
    
    
    $('#add_before_close_container_img').orakuploader({
        orakuploader_path: url_gb + '/',
        orakuploader_ckeditor: false,
        orakuploader_use_dragndrop: true,
        orakuploader_main_path: 'uploads/temp',
        orakuploader_thumbnail_path: 'uploads/temp',
        orakuploader_thumbnail_real_path: asset_gb + 'uploads/temp',
        orakuploader_add_image: asset_gb + 'images/add.png',
        orakuploader_loader_image: asset_gb + 'images/loader.gif',
        orakuploader_no_image: asset_gb + 'images/no-image.jpg',
        orakuploader_add_label: '@lang("lang.select_image")',
        orakuploader_use_rotation: false,
        orakuploader_maximum_uploads: max_file_1,
        orakuploader_hide_on_exceed: true,
        orakuploader_attach_images: file_1,
        orakuploader_field_name: 'before_close_container_img',
        orakuploader_thumbnail_size    : 150,
        orakuploader_finished: function () {

        }
    });

    // type 2
    if (photo_type_2) {
        var max_file_2 = 0;
        var file_2 = [];
        file_2[0] = photo_type_2;
        var photo_2 = photo_type_2;
    } else {
        var max_file_2 = 1;
        var file_2 = [];
        var photo_2 = photo_type_2;
    }
    $('#add_after_close_container_img').orakuploader({
        orakuploader_path: url_gb + '/',
        orakuploader_ckeditor: false,
        orakuploader_use_dragndrop: true,
        orakuploader_main_path: 'uploads/temp',
        orakuploader_thumbnail_path: 'uploads/temp',
        orakuploader_thumbnail_real_path: asset_gb + 'uploads/temp',
        orakuploader_add_image: asset_gb + 'images/add.png',
        orakuploader_loader_image: asset_gb + 'images/loader.gif',
        orakuploader_no_image: asset_gb + 'images/no-image.jpg',
        orakuploader_add_label: '@lang("lang.select_image")',
        orakuploader_use_rotation: false,
        orakuploader_maximum_uploads: max_file_2,
        orakuploader_hide_on_exceed: true,
        orakuploader_attach_images: file_2,
        orakuploader_field_name: 'after_close_container_img',
        orakuploader_thumbnail_size    : 150,
        orakuploader_finished: function () {

        }
    });
    // type 3
    if (photo_type_3) {
        var max_file_3 = 0;
        var file_3 = [];
        file_3[0] = photo_type_3;
        var photo_3 = photo_type_3;
    } else {
        var max_file_3 = 1;
        var file_3 = [];
        var photo_3 = photo_type_3;
    }
    $('#add_tag_container_img').orakuploader({
        orakuploader_path: url_gb + '/',
        orakuploader_ckeditor: false,
        orakuploader_use_dragndrop: true,
        orakuploader_main_path: 'uploads/temp',
        orakuploader_thumbnail_path: 'uploads/temp',
        orakuploader_thumbnail_real_path: asset_gb + 'uploads/temp',
        orakuploader_add_image: asset_gb + 'images/add.png',
        orakuploader_loader_image: asset_gb + 'images/loader.gif',
        orakuploader_no_image: asset_gb + 'images/no-image.jpg',
        orakuploader_add_label: '@lang("lang.select_image")',
        orakuploader_use_rotation: false,
        orakuploader_maximum_uploads: max_file_3,
        orakuploader_hide_on_exceed: true,
        orakuploader_attach_images: file_3,
        orakuploader_field_name: 'tag_container_img',
        orakuploader_thumbnail_size    : 150,
        orakuploader_finished: function () {

        }
    });

    $('#add_container_id').select2();
    $('#edit_container_id').select2();
</script>
@endsection
