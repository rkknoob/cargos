﻿@extends('Admin.layouts.layout')
@section('css_bottom')
@endsection
@section('body')
<div class="content">
    <div class="col-md-12">
    <div class="container-fluid">
        <form id="FormAddDelivery">
            @if(isset($Delivery))
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="content">
                                <h4 class="title">
                                    <h4 class="modal-title" id="myModalLabel">{{$title_page or 'ข้อมูลใหม่'}}</h4>
                                </h4>
                                <div class="material-datatables">
                                    <div class="modal-header">

                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-3">
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="add_plate_no">ทะเบียนรถขนส่ง</label>
                                                    <input type="text" class="form-control" name="delivery[plate_no]" id="add_plate_no" required="" placeholder="ทะเบียนรถขนส่ง" value="{{ $Delivery->plate_no }}" readonly>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="add_date_delivery">วันที่ส่งของ</label>

                                                        <input type="text" class="form-control" name="delivery[date_delivery]" id="add_date_delivery" required="" value="{{ $Delivery->date_delivery }}" readonly>

                                                    <!-- <div class="input-group" data-date="{{date('Y-m-d')}}">
                                                        <input type="text" value="" readonly="readonly" class="form-control" name="delivery[date_delivery]" id="add_date_delivery"  placeholder="date_delivery" >
                                                        <span class="input-group-addon remove_date_time"><i class="glyphicon glyphicon-remove icon-remove"></i></span>
                                                        <span class="input-group-addon trigger_date_time" for="date_delivery"><i class="glyphicon glyphicon-calendar icon-calendar"></i></span>
                                                    </div> -->
                                                </div>
                                            </div>
                                        </div>

                                        <!-- <div class="row">
                                            <div class="col-md-3">
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="add_qrcode">Barcode สินค้า</label>
                                                    <input type="text" class="form-control" id="add_qrcode" value="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12" style="text-align: center;">
                                                <button type="button" class="btn btn-get-qrcode btn-primary">ตกลง</button>
                                            </div>
                                        </div> -->

                                    </div>
                                    <div class="modal-footer">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="content">
                                <div class="material-datatables">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="myModalLabel">ตารางรายการสินค้าตาม Barcode</h5>
                                    </div>
                                        <div class="modal-body">
                                            <table class="table">
                                                <thead class="text-primary">
                                                    <tr>
                                                        <!-- <th>ID Product</th> -->
                                                        <th>เลข PO</th>
                                                        <th>QR Code</th>
                                                        <th>รหัสลูกค้า</th>
                                                        <th>ประเภทสินค้า</th>
                                                        <th>ชื่อสินค้า</th>
                                                        <th>วันที่รับสินค้าเข้าโกดัง (ไทย)</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="list-qr-products">
                                                    @if(!$DeliveryProducts->iSEmpty())
                                                        @foreach($DeliveryProducts as $DeliveryProduct)
                                                            <tr>
                                                                <!-- <td>{{ $DeliveryProduct->product_id }}</td> -->
                                                                <td>{{ $DeliveryProduct->po_no }}</td>
                                                                <td>{{ $DeliveryProduct->qr_code }}</td>
                                                                <td>{{ $DeliveryProduct->customer_general_code }}</td>
                                                                <td>{{ $DeliveryProduct->product_type_name }}</td>
                                                                <td>{{ $DeliveryProduct->product_name }}</td>
                                                                <td>{{ $DeliveryProduct->date_import }}</td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr id="qr-code-not-found">
                                                            <td colspan=10 style="text-align: center;">ไม่พบข้อมูล</td>
                                                        </tr>
                                                    @endif
                                                </tbody>
                                                <tfooter>

                                                </tfooter>
                                            </table>
                                        </div>
                                        <div class="modal-footer">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="content">
                                <div class="material-datatables">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="myModalLabel">ตารางรายการสินค้า</h5>
                                    </div>
                                        <div class="modal-body">
                                            <table class="table">
                                                <thead class="text-primary">
                                                    <tr>
                                                        <!-- <th>ID Product</th> -->
                                                        <th>รหัสลูกค้า</th>
                                                        <th>ประเภทสินค้า</th>
                                                        <th>ชื่อสินค้า</th>
                                                        <th>เลข PO</th>
                                                        <th>วันที่รับสินค้าเข้าโกดัง (ไทย)</th>
                                                        <th>จำนวนชิ้นของสินค้า</th>
                                                        <th>จำนวนชิ้นที่จำนวนชิ้นที่สแกนออก</th>
                                                        <th>น้ำหนัก</th>
                                                        <th>ขนาดคิว</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="list-products">
                                                    @if(!$ProductImportToChainas->isEmpty())
                                                        @foreach($ProductImportToChainas as $ProductImportToChaina)
                                                            <tr style="background-color: {{ $ProductImportToChaina->product_qty == $ProductImportToChaina->qty_delivery ? 'rgba(76, 175, 80, 0.42)' : 'rgba(255, 0, 0, 0.33)' }}">
                                                                <!-- <td>{{ $ProductImportToChaina->product_id }}</td> -->
                                                                <td>{{ $ProductImportToChaina->customer_general_code }}</td>
                                                                <td>{{ $ProductImportToChaina->product_type_name }}</td>
                                                                <td>{{ $ProductImportToChaina->product_name }}</td>
                                                                <td>{{ $ProductImportToChaina->po_no }}</td>
                                                                <td>{{ $ProductImportToChaina->import_date }}</td>
                                                                <td>{{ number_format($ProductImportToChaina->product_qty) }}</td>
                                                                <td>{{ number_format($ProductImportToChaina->qty_delivery) }}</td>
                                                                <td>{{ number_format($ProductImportToChaina->weight_per_item * $ProductImportToChaina->qty_delivery, 3) }}</td>
                                                                <td>{{ number_format(($ProductImportToChaina->cubic / $ProductImportToChaina->product_qty) * $ProductImportToChaina->qty_delivery, 3) }}</td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr id="products-not-founds">
                                                            <td colspan=10 style="text-align: center;">ไม่พบข้อมูล</td>
                                                        </tr>
                                                    @endif
                                                </tbody>
                                                <tfooter>

                                                </tfooter>
                                            </table>
                                        </div>
                                        <div class="modal-footer">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="content">
                                    <div class="material-datatables">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="myModalLabel">ตารางรายการ PO</h5>
                                        </div>
                                        <div class="modal-body">
                                            <table class="table">
                                                <thead class="text-primary">
                                                    <tr>
                                                        <th>#</th>
                                                        <th>เลข PO</th>
                                                        <th>จำนวนชิ้นตาม PO</th>
                                                        <th>จำนวนชิ้นในโกดังไทย</th>
                                                        <th>จำนวนชิ้นที่สแกนออกแล้ว</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="list-po-products">
                                                    @if(! $ImportToChainas->isEmpty())
                                                        @foreach($ImportToChainas as $ImportToChaina)

                                                            @if($ImportToChaina->qty_chaina == $ImportToChaina->qty_thai && $ImportToChaina->qty_thai == $ImportToChaina->qty_delivery)
                                                                @php $style = 'rgba(76, 175, 80, 0.42)'; @endphp
                                                            @elseif($ImportToChaina->qty_chaina == $ImportToChaina->qty_thai && $ImportToChaina->qty_thai != $ImportToChaina->qty_delivery)
                                                                @php  $style = 'rgba(255, 235, 59, 0.5)'; @endphp
                                                            @else
                                                                @php  $style = 'rgba(255, 0, 0, 0.33)'; @endphp
                                                            @endif
                                                            <tr style="background-color: {{ $style }}">
                                                                <td>#</td>
                                                                <td>{{ $ImportToChaina->po_no }}</td>
                                                                <td>{{ number_format($ImportToChaina->qty_chaina) }}</td>
                                                                <td>{{ number_format($ImportToChaina->qty_thai) }}</td>
                                                                <td>{{ number_format($ImportToChaina->qty_delivery) }}</td>
                                                                <td></td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr id="po-not-found">
                                                            <td colspan=10 style="text-align: center;">ไม่พบข้อมูล</td>
                                                        </tr>
                                                    @endif
                                                </tbody>
                                            <tfooter>

                                            </tfooter>
                                            </table>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="add_customer_id">หมายเหตุ</label>
                                                    <input type="text" class="form-control"  id="add_remark" value="" readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <!-- <button type="submit" class="btn btn-primary">บันทึก</button>
                                            <button type="cancel" class="btn btn-danger">ยกเลิก</button> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            @endif
        </form>
    </div>
</div>
@endsection
@section('js_bottom')
<script src="{{asset('assets/global/plugins/orakuploader/orakuploader.js')}}"></script>
<!-- <script>

    $("#add_date_delivery").daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        minYear: 1901,
        startDate: '{{date("Y-m-d")}}',
        // maxYear: parseInt(moment().format('YYYY'),10),
        locale: {
            format: 'YYYY-MM-DD'
        }
    }, function(start, end, label) {
        var years = moment().diff(start, 'years');
        // alert("You are " + years + " years old!");
    });

    $('body').on('click', '.btn-get-qrcode', function(){
        var qr_code = $('#add_qrcode').val();
        if(qr_code != ''){
            $.ajax({
                method : "POST",
                url : url_gb+"/admin/Delivery/CheckQrCode",
                dataType : 'json',
                data : {
                    qr_code: qr_code,
                }
            }).done(function(rec){
                var html_list_po = "";
                var html_list_products = "";
                var html_list_qr_products = "";

                if(rec.status==1){
                    var num_po = 1;
                    var num_product = 1;
                    var import_to_chaina_id = $('#ImportToChaina'+rec.QrCodeProduct.import_to_chaina_id).val();
                    var amount_po = $('#amount_po'+rec.QrCodeProduct.import_to_chaina_id).val();
                    var amount_product = $('#amount_product'+rec.QrCodeProduct.product_import_to_chaina_id).val();
                    var qr_code_product = $('#QrCodeProduct'+rec.QrCodeProduct.qr_code).val();
                    var product_import_to_chaina = $('#ProductImportToChaina'+rec.QrCodeProduct.product_import_to_chaina_id).val();

                    if(rec.QrCodeProduct.qr_code != qr_code_product){
                        if(rec.QrCodeProduct.product_import_to_chaina_id == product_import_to_chaina){
                            if(import_to_chaina_id == rec.QrCodeProduct.import_to_chaina_id){

                                num_po = (parseInt(num_po) + parseInt(amount_po));
                                num_product = (parseInt(num_product) + parseInt(amount_product));
                                $('#amount_po'+rec.QrCodeProduct.import_to_chaina_id).val(num_po);
                                $('#amount_po_html'+rec.QrCodeProduct.import_to_chaina_id).html(num_po);
                                $('#amount_product'+rec.QrCodeProduct.product_import_to_chaina_id).val(num_product);
                                $('#amount_product_html'+rec.QrCodeProduct.product_import_to_chaina_id).html(num_product);
                            }else{
                                html_list_po = '<tr>\
                                    <td>#\
                                        <input type="hidden" id="ImportToChaina'+rec.QrCodeProduct.import_to_chaina_id+'" value="'+rec.QrCodeProduct.import_to_chaina_id+'">\
                                        <input type="hidden" name="delivery[delivery_to_user][user_id]" value="'+rec.QrCodeProduct.import_to_chaina.user.id+'">\
                                        <input type="hidden" name="delivery[delivery_to_user][import_to_chaina_id]" value="'+rec.QrCodeProduct.import_to_chaina_id+'">\
                                    </td>\
                                    <td>'+rec.QrCodeProduct.import_to_chaina.po_no+'</td>\
                                    <td>'+addNumformat(rec.qty_po_chaina)+'</td>\
                                    <td>'+addNumformat(rec.qty_po_thai)+'</td>\
                                    <td><input type="hidden" id="amount_po'+rec.QrCodeProduct.import_to_chaina_id+'" value="'+num_po+'">\
                                    <label id="amount_po_html'+rec.QrCodeProduct.import_to_chaina_id+'" >'+addNumformat(num_po)+'</label></td>\
                                </tr>';

                                $('#po-not-found').hide();
                                $('#list-po-products').append(html_list_po);
                            }

                        }else{

                            if(import_to_chaina_id != rec.QrCodeProduct.import_to_chaina_id){
                                html_list_po = '<tr>\
                                    <td>#\
                                        <input type="hidden" id="ImportToChaina'+rec.QrCodeProduct.import_to_chaina_id+'" value="'+rec.QrCodeProduct.import_to_chaina_id+'">\
                                        <input type="hidden" name="delivery_to_user['+rec.QrCodeProduct.import_to_chaina_id+'][user_id]" value="'+rec.QrCodeProduct.import_to_chaina.user.id+'">\
                                        <input type="hidden" name="delivery_to_user['+rec.QrCodeProduct.import_to_chaina_id+'][import_to_chaina_id]" value="'+rec.QrCodeProduct.import_to_chaina_id+'">\
                                    </td>\
                                    <td>'+rec.QrCodeProduct.import_to_chaina.po_no+'</td>\
                                    <td>'+addNumformat(rec.qty_po_chaina)+'</td>\
                                    <td>'+addNumformat(rec.qty_po_thai)+'</td>\
                                    <td><input type="hidden" id="amount_po'+rec.QrCodeProduct.import_to_chaina_id+'" value="'+num_po+'">\
                                    <label id="amount_po_html'+rec.QrCodeProduct.import_to_chaina_id+'" >'+addNumformat(num_po)+'</label></td>\
                                </tr>';

                                $('#po-not-found').hide();
                                $('#list-po-products').append(html_list_po);
                            }else{

                                num_po = (parseInt(num_po) + parseInt(amount_po));
                                $('#amount_po'+rec.QrCodeProduct.import_to_chaina_id).val(num_po);
                                $('#amount_po_html'+rec.QrCodeProduct.import_to_chaina_id).html(num_po);
                                $('#amount_product'+rec.QrCodeProduct.product_import_to_chaina_id).val(num_product);
                                $('#amount_product_html'+rec.QrCodeProduct.product_import_to_chaina_id).html(num_product);
                            }

                            html_list_products = '<tr>\
                                    <td>'+rec.QrCodeProduct.product_import_to_chaina_id+'\
                                        <input type="hidden" id="ProductImportToChaina'+rec.QrCodeProduct.product_import_to_chaina_id+'" value="'+rec.QrCodeProduct.product_import_to_chaina_id+'">\
                                    </td>\
                                    <td>'+rec.QrCodeProduct.import_to_chaina.user.customer_general_code+'</td>\
                                    <td>'+rec.QrCodeProduct.product_import_to_chaina.product.product_type.code+'</td>\
                                    <td>'+rec.QrCodeProduct.product_import_to_chaina.product.name+'</td>\
                                    <td>'+rec.QrCodeProduct.import_to_chaina.po_no+'</td>\
                                    <td>'+rec.ProductImportToThai.created_at+'</td>\
                                    <td>'+addNumformat(rec.QrCodeProduct.product_import_to_chaina.qty)+'</td>\
                                    <td><input type="hidden" id="amount_product'+rec.QrCodeProduct.product_import_to_chaina_id+'" value="'+num_product+'">\
                                    <label id="amount_product_html'+rec.QrCodeProduct.product_import_to_chaina_id+'" >'+addNumformat(num_product)+'</label></td>\
                                    <td>'+addNumformat(rec.QrCodeProduct.product_import_to_chaina.weight_all)+'</td>\
                                    <td>'+addNumformat(rec.QrCodeProduct.product_import_to_chaina.cubic)+'</td>\
                                </tr>';

                            $('#products-not-founds').hide();
                            $('#list-products').append(html_list_products);

                        }

                        html_list_qr_products = '<tr>\
                                <td>'+rec.QrCodeProduct.product_import_to_chaina_id+'</td>\
                                <td>'+rec.QrCodeProduct.import_to_chaina.po_no+'</td>\
                                <td>'+rec.QrCodeProduct.qr_code+'\
                                    <input type="hidden" id="QrCodeProduct'+rec.QrCodeProduct.qr_code+'" value="'+rec.QrCodeProduct.qr_code+'">\
                                    <input type="hidden" name="delivery_to_user['+rec.QrCodeProduct.import_to_chaina_id+'][qr_code_product_id]['+rec.QrCodeProduct.id+']" value="'+rec.QrCodeProduct.id+'">\
                                </td>\
                                <td>'+rec.QrCodeProduct.import_to_chaina.user.customer_general_code+'</td>\
                                <td>'+rec.QrCodeProduct.product_import_to_chaina.product.product_type.code+'</td>\
                                <td>'+rec.QrCodeProduct.product_import_to_chaina.product.name+'</td>\
                                <td>'+rec.ProductImportToThai.created_at+'</td>\
                            </tr>';

                        $('#qr-code-not-found').hide();
                        $('#list-qr-products').append(html_list_qr_products);

                    }else{
                        swal("ระบบแจ้งเตือน","รายการถูกคีย์เข้าระบบแล้ว กรุณาลองใหม่", "error");
                    }

                }else{
                    swal(rec.title,rec.content,"error");
                }
            }).fail(function(){
                swal("system.system_alert","system.system_error","error");
                btn.button("reset");
            });
        }else{
            swal("ระบบแจ้งเตือน","กรุณากรอก Barcode สินค้าก่อนกดบันทึก", "error");
        }
    });

    $('#FormAddDelivery').validate({
        errorElement: 'div',
        errorClass: 'invalid-feedback',
        focusInvalid: false,
        rules: {
            'delivery[plate_no]': {
                required: true,
            },
        },
        messages: {
            'delivery[plate_no]': {
                required: 'กรุณาระบุ',
            },
        },
        highlight: function (e) {
            validate_highlight(e);
        },
        success: function (e) {
            validate_success(e);
        },

        errorPlacement: function (error, element) {
            validate_errorplacement(error, element);
        },
        submitHandler: function (form) {
            /*
            if(CKEDITOR!==undefined){
                for ( instance in CKEDITOR.instances ){
                    CKEDITOR.instances[instance].updateElement();
                }
            }
            */
            var btn = $(form).find('[type="submit"]');
            var data_ar = removePriceFormat(form,$(form).serializeArray());
            btn.button("loading");
            $.ajax({
                method : "POST",
                url : url_gb+"/admin/Delivery",
                dataType : 'json',
                data : $(form).serialize()
            }).done(function(rec){
                btn.button("reset");
                if(rec.status==1){
                    swal(rec.title,rec.content,"success");
                    window.location.href = url_gb+"/admin/Delivery";
                }else{
                    swal(rec.title,rec.content,"error");
                }
            }).fail(function(){
                swal("system.system_alert","system.system_error","error");
                btn.button("reset");
            });
        },
        invalidHandler: function (form) {

        }
    });

    // $('#FormGetQRCode').validate({
    //     errorElement: 'div',
    //     errorClass: 'invalid-feedback',
    //     focusInvalid: false,
    //     rules: {
    //
    //         qr_code: {
    //             required: true,
    //         },
    //
    //     },
    //     messages: {
    //
    //         qr_code: {
    //             required: "กรุณาระบุ",
    //         },
    //
    //     },
    //     highlight: function (e) {
    //         validate_highlight(e);
    //     },
    //     success: function (e) {
    //         validate_success(e);
    //     },
    //
    //     errorPlacement: function (error, element) {
    //         validate_errorplacement(error, element);
    //     },
    //     submitHandler: function (form) {
    //         /*
    //         if(CKEDITOR!==undefined){
    //             for ( instance in CKEDITOR.instances ){
    //                 CKEDITOR.instances[instance].updateElement();
    //             }
    //         }
    //         */
    //         var btn = $(form).find('[type="submit"]');
    //         btn.button("loading");
    //         $.ajax({
    //             method : "POST",
    //             url : url_gb+"/admin/Delivery/CheckQrCode",
    //             dataType : 'json',
    //             data : $(form).serialize()
    //         }).done(function(rec){
    //             btn.button("reset");
    //
    //             var html_list_po = "";
    //             var html_list_products = "";
    //             var html_list_qr_products = "";
    //
    //
    //             if(rec.status==1){
    //                 var num_po = 1;
    //                 var num_product = 1;
    //                 var import_to_chaina_id = $('#ImportToChaina'+rec.QrCodeProduct.import_to_chaina_id).val();
    //                 var amount_po = $('#amount_po'+rec.QrCodeProduct.import_to_chaina_id).val();
    //                 var amount_product = $('#amount_product'+rec.QrCodeProduct.product_import_to_chaina_id).val();
    //                 var qr_code_product = $('#QrCodeProduct'+rec.QrCodeProduct.qr_code).val();
    //                 var product_import_to_chaina = $('#ProductImportToChaina'+rec.QrCodeProduct.product_import_to_chaina_id).val();
    //
    //                 if(rec.QrCodeProduct.qr_code != qr_code_product){
    //                     if(rec.QrCodeProduct.product_import_to_chaina_id == product_import_to_chaina){
    //                         if(import_to_chaina_id == rec.QrCodeProduct.import_to_chaina_id){
    //
    //                             num_po = (parseInt(num_po) + parseInt(amount_po));
    //                             num_product = (parseInt(num_product) + parseInt(amount_product));
    //                             $('#amount_po'+rec.QrCodeProduct.import_to_chaina_id).val(num_po);
    //                             $('#amount_po_html'+rec.QrCodeProduct.import_to_chaina_id).html(num_po);
    //                             $('#amount_product'+rec.QrCodeProduct.product_import_to_chaina_id).val(num_product);
    //                             $('#amount_product_html'+rec.QrCodeProduct.product_import_to_chaina_id).html(num_product);
    //                         }else{
    //                             html_list_po = '<tr>\
    //                                 <td>#\
    //                                     <input type="hidden" id="ImportToChaina'+rec.QrCodeProduct.import_to_chaina_id+'" value="'+rec.QrCodeProduct.import_to_chaina_id+'">\
    //                                 </td>\
    //                                 <td>'+rec.QrCodeProduct.import_to_chaina.po_no+'</td>\
    //                                 <td>'+addNumformat(rec.qty_po_chaina)+'</td>\
    //                                 <td>'+addNumformat(rec.qty_po_thai)+'</td>\
    //                                 <td><input type="hidden" id="amount_po'+rec.QrCodeProduct.import_to_chaina_id+'" value="'+num_po+'">\
    //                                 <label id="amount_po_html'+rec.QrCodeProduct.import_to_chaina_id+'" >'+addNumformat(num_po)+'</label></td>\
    //                             </tr>';
    //
    //                             $('#po-not-found').hide();
    //                             $('#list-po-products').append(html_list_po);
    //                         }
    //
    //                     }else{
    //
    //                         if(import_to_chaina_id != rec.QrCodeProduct.import_to_chaina_id){
    //                             html_list_po = '<tr>\
    //                                 <td>#\
    //                                     <input type="hidden" id="ImportToChaina'+rec.QrCodeProduct.import_to_chaina_id+'" value="'+rec.QrCodeProduct.import_to_chaina_id+'">\
    //                                 </td>\
    //                                 <td>'+rec.QrCodeProduct.import_to_chaina.po_no+'</td>\
    //                                 <td>'+addNumformat(rec.qty_po_chaina)+'</td>\
    //                                 <td>'+addNumformat(rec.qty_po_thai)+'</td>\
    //                                 <td><input type="hidden" id="amount_po'+rec.QrCodeProduct.import_to_chaina_id+'" value="'+num_po+'">\
    //                                 <label id="amount_po_html'+rec.QrCodeProduct.import_to_chaina_id+'" >'+addNumformat(num_po)+'</label></td>\
    //                             </tr>';
    //
    //                             $('#po-not-found').hide();
    //                             $('#list-po-products').append(html_list_po);
    //                         }else{
    //
    //                             num_po = (parseInt(num_po) + parseInt(amount_po));
    //                             $('#amount_po'+rec.QrCodeProduct.import_to_chaina_id).val(num_po);
    //                             $('#amount_po_html'+rec.QrCodeProduct.import_to_chaina_id).html(num_po);
    //                             $('#amount_product'+rec.QrCodeProduct.product_import_to_chaina_id).val(num_product);
    //                             $('#amount_product_html'+rec.QrCodeProduct.product_import_to_chaina_id).html(num_product);
    //                         }
    //
    //                         html_list_products = '<tr>\
    //                                 <td>'+rec.QrCodeProduct.product_import_to_chaina_id+'\
    //                                     <input type="hidden" id="ProductImportToChaina'+rec.QrCodeProduct.product_import_to_chaina_id+'" value="'+rec.QrCodeProduct.product_import_to_chaina_id+'">\
    //                                 </td>\
    //                                 <td>'+rec.QrCodeProduct.import_to_chaina.user.customer_general_code+'</td>\
    //                                 <td>'+rec.QrCodeProduct.product_import_to_chaina.product.product_type.code+'</td>\
    //                                 <td>'+rec.QrCodeProduct.product_import_to_chaina.product.name+'</td>\
    //                                 <td>'+rec.QrCodeProduct.import_to_chaina.po_no+'</td>\
    //                                 <td>'+rec.ProductImportToThai.created_at+'</td>\
    //                                 <td>'+addNumformat(rec.QrCodeProduct.product_import_to_chaina.qty)+'</td>\
    //                                 <td><input type="hidden" id="amount_product'+rec.QrCodeProduct.product_import_to_chaina_id+'" value="'+num_product+'">\
    //                                 <label id="amount_product_html'+rec.QrCodeProduct.product_import_to_chaina_id+'" >'+addNumformat(num_product)+'</label></td>\
    //                                 <td>'+addNumformat(rec.QrCodeProduct.product_import_to_chaina.weight_all)+'</td>\
    //                                 <td>'+addNumformat(rec.QrCodeProduct.product_import_to_chaina.cubic)+'</td>\
    //                             </tr>';
    //
    //                         $('#products-not-founds').hide();
    //                         $('#list-products').append(html_list_products);
    //
    //                     }
    //
    //                     html_list_qr_products = '<tr>\
    //                             <td>'+rec.QrCodeProduct.product_import_to_chaina_id+'</td>\
    //                             <td>'+rec.QrCodeProduct.import_to_chaina.po_no+'</td>\
    //                             <td>'+rec.QrCodeProduct.qr_code+'\
    //                                 <input type="hidden" id="QrCodeProduct'+rec.QrCodeProduct.qr_code+'" value="'+rec.QrCodeProduct.qr_code+'">\
    //                             </td>\
    //                             <td>'+rec.QrCodeProduct.import_to_chaina.user.customer_general_code+'</td>\
    //                             <td>'+rec.QrCodeProduct.product_import_to_chaina.product.product_type.code+'</td>\
    //                             <td>'+rec.QrCodeProduct.product_import_to_chaina.product.name+'</td>\
    //                             <td>'+rec.ProductImportToThai.created_at+'</td>\
    //                         </tr>';
    //
    //                     $('#qr-code-not-found').hide();
    //                     $('#list-qr-products').append(html_list_qr_products);
    //
    //                 }else{
    //                     swal("ระบบแจ้งเตือน","รายการถูกคีย์เข้าระบบแล้ว กรุณาลองใหม่", "error");
    //                 }
    //
    //             }else{
    //                 swal(rec.title,rec.content,"error");
    //             }
    //         }).fail(function(){
    //             swal("system.system_alert","system.system_error","error");
    //             btn.button("reset");
    //         });
    //     },
    //     invalidHandler: function (form) {
    //
    //     }
    // });

    $('#add_container_id').select2();


</script> -->
@endsection
