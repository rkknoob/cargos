@extends('Admin.layouts.layout')
@section('css_bottom')
@endsection
@section('body')
<div class="content">
    <div class="col-md-12">
    <div class="container-fluid">
        <form id="FormGetQRCode">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="content">
                            <h4 class="title">
                                <h4 class="modal-title" id="myModalLabel">{{$title_page or 'ข้อมูลใหม่'}}</h4>
                            </h4>
                            <div class="material-datatables">
                                <div class="modal-header">

                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-3">
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="add_container_id">เลือกตู้</label>
                                                <select name="container_id" class="select2 form-control" tabindex="-1" data-placeholder="เลือกตู้" id="add_container_id" >
                                                    <option value="{{ $Container->id }}" selected>{{ $Container->container_code }}</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="add_qrcode">Barcode สินค้า</label>
                                                <input type="text" class="form-control" name="qr_code" id="add_qrcode" value="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12" style="text-align: center;">
                                            <button type="submit" class="btn btn-get-qrcode btn-primary">ตกลง</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <form id="FormAddImportToThai">
            <input type="hidden" name="container_id" id="show_container_id">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="content">
                            <div class="material-datatables">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="myModalLabel">รายการ PO</h5>
                                </div>
                                    <div class="modal-body">
                                        <table class="table">
                                            <thead class="text-primary">
                                                <tr>
                                                    <th>#</th>
                                                    <th>รหัสลูกค้า</th>
                                                    <th>เลข PO</th>
                                                    <th>วันที่รับสินค้า</th>
                                                    <th>จำนวนขึ้นตาม PO</th>
                                                    <th>น้ำหนัก</th>
                                                    <th>ขนาดคิว</th>
                                                </tr>
                                            </thead>
                                            <tbody id="list-po">
                                                @if(! $ImportToChainas->isEmpty())
                                                    @foreach($ImportToChainas as $ImportToChaina)
                                                        <tr>
                                                            <td>#</td>
                                                            <td>{{ $ImportToChaina->User ? $ImportToChaina->User->customer_general_code : null }}</td>
                                                            <td>{{ $ImportToChaina->po_no }}</td>
                                                            <td>{{ $ImportToChaina->import_to_thai_date }}</td>
                                                            <td>{{ number_format($ImportToChaina->qty_chaina) }}</td>
                                                            <td>{{ number_format($ImportToChaina->weight_all) }}</td>
                                                            <td>{{ number_format($ImportToChaina->cubic) }}</td>
                                                        </tr>
                                                    @endforeach
                                                @else
                                                    <tr id="op-not-found">
                                                        <td colspan=10 style="text-align: center;">ไม่พบข้อมูล</td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                            <tfooter>

                                            </tfooter>
                                        </table>
                                    </div>
                                    <div class="modal-footer">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="content">
                                <div class="material-datatables">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="myModalLabel">รายการสินค้าที่รับเข้าโกดังไทย</h5>
                                    </div>
                                        <div class="modal-body">
                                            <table class="table">
                                                <thead class="text-primary">
                                                    <tr>
                                                        <th>#</th>
                                                        <th>เลข PO</th>
                                                        <th>QR Code</th>
                                                        <th>Lot ID</th>
                                                        <th>รหัสลูกค้า</th>
                                                        <th>ประเภทสินค้า</th>
                                                        <th>ชื่อสินค้า</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="list-qr-code">

                                                    @if(! $ImportToChainas->isEmpty())
                                                        @foreach($ImportToChainas as $ImportToChaina)
                                                            @if(!$ImportToChaina->ImportToThai->isEmpty())
                                                                @foreach($ImportToChaina->ImportToThai as $ImportToThai)
                                                                    @if(!$ImportToThai->ProductImportToThai->isEmpty())
                                                                        @foreach($ImportToThai->ProductImportToThai as $ProductImportToThai)
                                                                            <tr>
                                                                                <td>#
                                                                                    <input type="hidden" class="po_no" value="{{$ImportToChaina->po_no}}" />
                                                                                    <input type="hidden" class="qr_code_name" value="{{ $ProductImportToThai->QrCodeProduct ? $ProductImportToThai->QrCodeProduct->qr_code : null }}" />
                                                                                    <input type="hidden" class="po_no" value="{{ $ProductImportToThai->QrCodeProduct ? $ProductImportToThai->QrCodeProduct->qr_code : null }}" />
                                                                                </td>
                                                                                <td>{{ $ImportToChaina->po_no }}</td>
                                                                                <td>{{ $ProductImportToThai->QrCodeProduct ? $ProductImportToThai->QrCodeProduct->qr_code : null }}</td>
                                                                                <td>Lot ID</td>
                                                                                <td>{{ $ImportToChaina->User ? $ImportToChaina->User->customer_general_code : null }}</td>
                                                                                <td>{{ $ProductImportToThai->QrCodeProduct && $ProductImportToThai->QrCodeProduct->ProductImportToChaina && $ProductImportToThai->QrCodeProduct->ProductImportToChaina->Product && $ProductImportToThai->QrCodeProduct->ProductImportToChaina->Product->ProductType ? $ProductImportToThai->QrCodeProduct->ProductImportToChaina->Product->ProductType->name : null }}</td>
                                                                                <td>{{ $ProductImportToThai->QrCodeProduct && $ProductImportToThai->QrCodeProduct->ProductImportToChaina && $ProductImportToThai->QrCodeProduct->ProductImportToChaina->Product ? $ProductImportToThai->QrCodeProduct->ProductImportToChaina->Product->name : null }}</td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        @endforeach
                                                    @else
                                                        <tr id="qr-code-not-found">
                                                            <td colspan=10 style="text-align: center;">ไม่พบข้อมูล</td>
                                                        </tr>
                                                    @endif
                                                </tbody>
                                                <tfooter>

                                                </tfooter>
                                            </table>
                                        </div>
                                        <div class="modal-footer">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="content">
                                <div class="material-datatables">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="myModalLabel">รายการ PO ภายในตู้</h5>
                                    </div>
                                    <div class="modal-body">
                                        <table class="table">
                                            <thead class="text-primary">
                                                <tr>
                                                    <th>#</th>
                                                    <th>เลข PO</th>
                                                    <th>จำนวนชิ้นตาม PO</th>
                                                    <th>จำนวนที่ปิดตู้มา</th>
                                                    <th>จำนวนขึ้นตามจริง</th>
                                                </tr>
                                            </thead>
                                            <tbody id="list-products">
                                                @if(! $ImportToChainas->isEmpty())
                                                    @foreach($ImportToChainas as $ImportToChaina)
                                                        <tr>
                                                            <td>#</td>
                                                            <td>{{ $ImportToChaina->po_no }}</td>
                                                            <td>{{ number_format($ImportToChaina->qty_chaina) }}</td>
                                                            <td>{{ number_format($ImportToChaina->qty_container) }}</td>
                                                            <td>{{ number_format($ImportToChaina->qty_thai) }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td></td>
                                                            <td>Lot ID</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                        </tr>
                                                    @endforeach
                                                @else
                                                    <tr id="products-not-found">
                                                        <td colspan=10 style="text-align: center;">ไม่พบข้อมูล</td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                        <tfooter>

                                        </tfooter>
                                        </table>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="add_customer_id">หมายเหตุ</label>
                                                <input type="text" class="form-control"  id="add_remark" value="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-primary">บันทึก</button>
                                        <button type="cancel" class="btn btn-danger">ยกเลิก</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </form>
    </div>
</div>
@endsection
@section('js_bottom')
<script src="{{asset('assets/global/plugins/orakuploader/orakuploader.js')}}"></script>
<script>

    $('body').on('change', '#add_container_id', function(){
        var container_id = $(this).val();
        $('#show_container_id').val(container_id);
        $('#add_qrcode').val('');
        $('#add_remark').val('');
        $('#list-po').html('');
        $('#op-not-found').show();
        $('#list-qr-code').html('');
        $('#qr-code-not-found').show();
        $('#list-products').html('');
        $('#products-not-found').show();
    });

    $('#FormAddImportToThai').validate({
        errorElement: 'div',
        errorClass: 'invalid-feedback',
        focusInvalid: false,
        rules: {
            contauner_id: {
                required: true,
            },

        },
        messages: {

            contauner_id: {
                required: "กรุณาระบุ",
            },

        },
        highlight: function (e) {
            validate_highlight(e);
        },
        success: function (e) {
            validate_success(e);
        },

        errorPlacement: function (error, element) {
            validate_errorplacement(error, element);
        },
        submitHandler: function (form) {
            /*
            if(CKEDITOR!==undefined){
                for ( instance in CKEDITOR.instances ){
                    CKEDITOR.instances[instance].updateElement();
                }
            }
            */
            var btn = $(form).find('[type="submit"]');
            var data_ar = removePriceFormat(form,$(form).serializeArray());
            btn.button("loading");
            $.ajax({
                method : "POST",
                url : url_gb+"/admin/ImportToThai",
                dataType : 'json',
                data : $(form).serialize()
            }).done(function(rec){
                btn.button("reset");
                if(rec.status==1){
                    swal(rec.title,rec.content,"success");
                    window.location.href = url_gb+"/admin/ImportToThai";
                }else{
                    swal(rec.title,rec.content,"error");
                }
            }).fail(function(){
                swal("system.system_alert","system.system_error","error");
                btn.button("reset");
            });
        },
        invalidHandler: function (form) {

        }
    });

    $('#FormGetQRCode').validate({
        errorElement: 'div',
        errorClass: 'invalid-feedback',
        focusInvalid: false,
        rules: {
            container_id: {
                required: true,
            },
            qr_code: {
                required: true,
            },

        },
        messages: {

            container_id: {
                required: "กรุณาระบุ",
            },
            qr_code: {
                required: "กรุณาระบุ",
            },

        },
        highlight: function (e) {
            validate_highlight(e);
        },
        success: function (e) {
            validate_success(e);
        },

        errorPlacement: function (error, element) {
            validate_errorplacement(error, element);
        },
        submitHandler: function (form) {
            /*
            if(CKEDITOR!==undefined){
                for ( instance in CKEDITOR.instances ){
                    CKEDITOR.instances[instance].updateElement();
                }
            }
            */
            var btn = $(form).find('[type="submit"]');
            btn.button("loading");
            $.ajax({
                method : "POST",
                url : url_gb+"/admin/ImportToThai/CheckQrCode",
                dataType : 'json',
                data : $(form).serialize()
            }).done(function(rec){
                btn.button("reset");
                var html_list_qr_code = "";
                var html_list_po = "";
                var html_list_product = "";
                var html_lot_product = "";


                if(rec.status==1){
                    var number_po = $('#po_id'+rec.ImportToChaina.id).val();
                    var qr_code_id = $('#qr_code_id'+rec.QrCodeProduct.id).val();
                    var import_to_chaina_id = $('#import_to_chaina_id'+rec.QrCodeProduct.import_to_chaina_id).val();
                    var lot_product_id = $('#lot_product_id'+rec.LotProduct.id).val();
                    var amount_po = $('#amount_po'+rec.QrCodeProduct.import_to_chaina_id).val();
                    var amount_lot = $('#amount_lot'+rec.QrCodeProduct.import_to_chaina_id+'_'+rec.LotProduct.id).val();


                    if(rec.ImportToChaina.po_no != number_po){
                        html_list_po += '<tr>\
                            <td>#</td>\
                            <td>'+rec.QrCodeProduct.import_to_chaina.user.customer_general_code+'</td>\
                            <td>'+rec.ImportToChaina.po_no+'\
                                <input type="hidden" id="po_id'+rec.ImportToChaina.id+'" value="'+rec.ImportToChaina.po_no+'">\
                            </td>\
                            <td>'+rec.ImportToChaina.import_date+'</td>\
                            <td>'+addNumformat(rec.qty_po)+'</td>\
                            <td>'+addNumformat(rec.weight_all)+'</td>\
                            <td>'+addNumformat(rec.cubic)+'</td>\
                        </tr>';

                        $('#op-not-found').hide();
                        $('#list-po').append(html_list_po);
                    }
                    var num_po = 1;
                    var num_lot = 1;
                    if(rec.QrCodeProduct.id != qr_code_id){

                        if(rec.QrCodeProduct.import_to_chaina_id == import_to_chaina_id){
                             num_po = (parseInt(num_po) + parseInt(amount_po));
                             $('#amount_po'+rec.QrCodeProduct.import_to_chaina_id).val(num_po);
                             $('#amount_po_html'+rec.QrCodeProduct.import_to_chaina_id).html(num_po);

                             if(rec.LotProduct.id == lot_product_id){
                                 num_lot = (parseInt(num_lot) + parseInt(amount_lot));
                                 $('#amount_lot'+rec.QrCodeProduct.import_to_chaina_id+'_'+rec.LotProduct.id).val(num_lot);
                                 $('#amount_lot_html'+rec.QrCodeProduct.import_to_chaina_id+'_'+rec.LotProduct.id).html(num_lot);
                             }else{
                                  html_list_product += '<tr class="list_po'+rec.ImportToChaina.id+'_list_lot'+rec.LotProduct.id+'">\
                                      <td>\
                                          <input type="hidden" id="lot_product_id'+rec.LotProduct.id+'" value="'+rec.LotProduct.id+'">\
                                      </td>\
                                      <td>Lot'+rec.LotProduct.id+'</td>\
                                      <td>'+addNumformat(rec.qty_lot_product)+'</td>\
                                      <td>'+addNumformat(rec.qty_lot_product)+'</td>\
                                      <td><input type="hidden" id="amount_lot'+rec.QrCodeProduct.import_to_chaina_id+'_'+rec.LotProduct.id+'" value="'+num_lot+'">\
                                      <label id="amount_lot_html'+rec.QrCodeProduct.import_to_chaina_id+'_'+rec.LotProduct.id+'" >'+addNumformat(num_lot)+'</label></td>\
                                  </tr>';

                                  $('#list-products').append(html_list_product);
                             }
                        }else{
                            html_list_product += '<tr class="list_po'+rec.ImportToChaina.id+'">\
                                                        <td>#</td>\
                                                        <td>'+rec.ImportToChaina.po_no+'\
                                                            <input type="hidden" id="import_to_chaina_id'+rec.QrCodeProduct.import_to_chaina_id+'" value="'+rec.QrCodeProduct.import_to_chaina_id+'">\
                                                        </td>\
                                                        <td>'+addNumformat(rec.qty_po)+'</td>\
                                                        <td>'+addNumformat(rec.qty_container)+'</td>\
                                                        <td><input type="hidden" id="amount_po'+rec.QrCodeProduct.import_to_chaina_id+'" value="'+num_po+'">\
                                                        <label id="amount_po_html'+rec.QrCodeProduct.import_to_chaina_id+'" >'+addNumformat(num_po)+'</label></td>\
                                                    </tr><tr class="list_po'+rec.ImportToChaina.id+'_list_lot'+rec.LotProduct.id+'">\
                                                        <td>\
                                                            <input type="hidden" id="lot_product_id'+rec.LotProduct.id+'" value="'+rec.LotProduct.id+'">\
                                                        </td>\
                                                        <td>Lot'+rec.LotProduct.id+'</td>\
                                                        <td>'+addNumformat(rec.qty_lot_product)+'</td>\
                                                        <td>'+addNumformat(rec.qty_lot_product)+'</td>\
                                                        <td><input type="hidden" id="amount_lot'+rec.QrCodeProduct.import_to_chaina_id+'_'+rec.LotProduct.id+'" value="'+num_lot+'">\
                                                        <label id="amount_lot_html'+rec.QrCodeProduct.import_to_chaina_id+'_'+rec.LotProduct.id+'" >'+addNumformat(num_lot)+'</label></td>\
                                                    </tr>';

                            $('#products-not-found').hide();
                            $('#list-products').append(html_list_product);
                        }

                        html_list_qr_code += '<tr>\
                            <td>#</td>\
                            <td>'+rec.ImportToChaina.po_no+'</td>\
                            <td>'+rec.QrCodeProduct.qr_code+'\
                                <input type="hidden" name="import_to_chaina_id['+rec.ImportToChaina.id+'][qr_code_product_id]['+rec.QrCodeProduct.id+']" value="'+rec.QrCodeProduct.id+'">\
                            </td>\
                            <td>Lot'+rec.LotProduct.id+'</td>\
                            <td>'+rec.QrCodeProduct.import_to_chaina.user.customer_general_code+'\
                                <input type="hidden" id="qr_code_id'+rec.QrCodeProduct.id+'" value="'+rec.QrCodeProduct.id+'">\
                            </td>\
                            <td>'+rec.QrCodeProduct.product_import_to_chaina.product.product_type.name+'</td>\
                            <td>'+rec.QrCodeProduct.product_import_to_chaina.product.name+'</td>\
                        </tr>';

                        $('#qr-code-not-found').hide();
                        $('#list-qr-code').append(html_list_qr_code);

                    }else{
                        swal("ระบบแจ้งเตือน","รายการถูกคีย์เข้าระบบแล้ว กรุณาลองใหม่", "error");
                    }


                }else{
                    swal(rec.title,rec.content,"error");
                }
            }).fail(function(){
                swal("system.system_alert","system.system_error","error");
                btn.button("reset");
            });
        },
        invalidHandler: function (form) {

        }
    });

    $('#add_container_id').select2();

    function showPOandLot(){
        var product_for_show = $('body').find('#barcode').find('#list-qr-code > tr');
        delete groups;
        var groups = {};
        console.log(groups);
        console.log(product_for_show);
        var html = '';
        if (product_for_show.length > 0) {
            $.each(product_for_show, function(k, v){
                var po = $(v).find('.po_no').val();
                var lot_id = $(v).find('.lot_product_id').val();
                var qr_code_name = $(v).find('.qr_code_name').val();
                var po_qty = $(v).find('.po_qty').val();
                var lot_product_qty = $(v).find('.lot_product_qty').val();
                if (!groups[po]) {
                    groups[po] = {};
                }
                if (!groups[po][lot_id]) {
                    groups[po][lot_id] = {};
                }
                if (!groups[po][lot_id]['amount']) {
                    groups[po][lot_id]['amount'] = [];
                }
                groups[po]['po_qty'] = po_qty;
                groups[po][lot_id]['lot_product_qty'] = lot_product_qty;
                groups[po][lot_id]['amount'].push(qr_code_name);
            });
            var No_Po = 1;
            var style = '';
            var style_lot = '';
            $.each(groups, function(k1, v1){
                var sumallproduct = 0;
                $.each(v1, function(k2, v2){
                    if (k2 != 'po_qty') {
                        sumallproduct += v2.amount.length;
                    }
                });
                if(v1.po_qty > sumallproduct){
                    style = 'rgba(255, 0, 0, 0.33)';
                }else if(v1.po_qty < sumallproduct){
                    style = 'rgba(255, 235, 59, 0.5)';
                }else{
                    style = 'rgba(76, 175, 80, 0.42)';
                }
                html += '\
                    <tr style="background-color: '+style+'">\
                        <td><a class="btn-delete-po btn btn-danger" data-po="'+k1+'">-</a></td>\
                        <td>'+No_Po+'</td>\
                        <td>'+k1+'</td>\
                        <td class="text-center">'+addNumformat(v1.po_qty)+'</td>\
                        <td class="text-center">'+addNumformat(sumallproduct)+'</td>\
                    </tr>';
                $.each(v1, function(k2, v2){
                    if (k2 != 'po_qty') {
                        if(v2.lot_product_qty > v2.amount.length){
                            style_lot = 'rgba(255, 0, 0, 0.33)';
                        }else if(v2.lot_product_qty < v2.amount.length){
                            style_lot = 'rgba(255, 235, 59, 0.5)';
                        }else{
                            style_lot = 'rgba(76, 175, 80, 0.42)';
                        }
                        html += '\
                            <tr style="background-color: '+style_lot+'">\
                                <td></td>\
                                <td></td>\
                                <td class="text-right">Lot '+k2+'</td>\
                                <td class="text-center">'+addNumformat(v2.lot_product_qty)+'</td>\
                                <td class="text-center">'+addNumformat(v2.amount.length)+'</td>\
                            </tr>';
                    }
                });
                No_Po++;
            });
        }else{
            html += '\
                <tr>\
                    <td colspan="5" class="text-center">ไม่พบข้อมูล</td>\
                </tr>';
        }

        $('.add-product-po').html(html)
    }

</script>
@endsection
