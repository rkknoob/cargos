﻿@extends('Admin.layouts.layout')
@section('css_bottom')
@endsection
@section('body')
<div class="content">
    <div class="col-md-12">
    <div class="container-fluid">
                <input type="hidden" id="edit_id" value="{{ $Billing->id }}">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="content">
                                <h4 class="title">
                                    <h4 class="modal-title" id="myModalLabel">@lang('lang.export_billing')</h4>
                                </h4>
                                <div class="material-datatables">
                                    <div class="modal-header">
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="add_billing_no">@lang('lang.billing_number')</label>
                                                    <input type="text" class="form-control" value="{{ $Billing->billing_no }}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="add_delivery_slip_date">@lang('lang.issued_date_billing')</label>
                                                    <input type="text" class="form-control" value="{{ $Billing->billing_date }}" readonly>
                                                    <!-- <div class="input-group" data-date="{{date('Y-m-d')}}">
                                                        <input type="text" value="" readonly="readonly" class="form-control" name="billing[billing_date]" id="add_billing_date" value="{{ $Billing->billing_date }}"  placeholder="import_to_chaina_date">
                                                        <span class="input-group-addon remove_date_time"><i class="glyphicon glyphicon-remove icon-remove"></i></span>
                                                        <span class="input-group-addon trigger_date_time" for="add_delivery_slip_date"><i class="glyphicon glyphicon-calendar icon-calendar"></i></span>
                                                    </div> -->
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="add_user_id">@lang('lang.customer')</label>
                                                    <select class="select2 form-control" tabindex="-1" data-placeholder="@lang('lang.customer')" id="add_user_id" disabled>
                                                        <option value="">เลือกลูกค้า</option>
                                                        @if(!$Users->isEmpty())
                                                            @foreach($Users as $User)
                                                                <option value="{{ $User->id }}" {{ $Billing->user_id == $User->id ? 'selected' : null }} >{{ $User->firstname }} {{ $User->lastname }}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="add_user_id">@lang('lang.status')</label>
                                                    <input type="text" class="form-control" value="{{ $Billing->status == 'T' ? 'ปิดบิลเรียบร้อย' : 'ยังไม่ปิดบิล' }}" readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="add_billing_no">@lang('lang.note')</label>
                                                    <input type="text" class="form-control" value="{{ $Billing->remark }}" readonly>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row" style="margin-top: 40px;">
                                            <div class="col-md-12">
                                                <h5>@lang('lang.item_has_been_paid_successfully')</h5>
                                                <div class="fix-scroll-dashboard">
                                                    <table class="table">
                                                        <thead>
                                                            <tr>
                                                                <!-- <th>
                                                                    <div class="form-check">
                                                                        <label for="edit_active" class="checkbox form-check-label">
                                                                            <span class="icons">
                                                                                <span class="first-icon fa fa-square"></span>
                                                                                <span class="second-icon fa fa-check-square "></span>
                                                                            </span><input type="checkbox" class="form-check-input" data-toggle="checkbox" id="check_active_all" required="" aria-required="true">
                                                                        </label>
                                                                    </div>
                                                                </th> -->
                                                                <!-- <th>@lang('lang.no')</th> -->
                                                                <th>@lang('lang.customer_id')</th>
                                                                <th>@lang('lang.delivery_date')</th>
                                                                <th>@lang('lang.invoice_no')</th>
                                                                <th>@lang('lang.number_po')</th>
                                                                <th>@lang('lang.quantity')</th>
                                                                <th>@lang('lang.weigh')</th>
                                                                <th>@lang('lang.queue_size')</th>
                                                                <th>@lang('lang.price')</th>
                                                                <th>@lang('lang.date_of_entry_to_the_warehouse_china')</th>
                                                                <th>@lang('lang.status')</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="list-po-products">
                                                            @if(! $BillingListsT->isEmpty())
                                                                @php
                                                                    $total_qty = 0;
                                                                    $total_weight = 0;
                                                                    $total_cubic = 0;
                                                                    $total_subtotal = 0;
                                                                @endphp
                                                                @foreach($BillingListsT as $k => $BillingListT)
                                                                    <tr>
                                                                        <!-- <td>
                                                                            <div class="form-check">
                                                                                <label for="edit_active" class="checkbox form-check-label label-check-po">
                                                                                    <span class="icons">
                                                                                        <span class="first-icon fa fa-square"></span>
                                                                                        <span class="second-icon fa fa-check-square "></span>
                                                                                    </span>
                                                                                    <input type="checkbox" class="form-check-input input-check-po" data-toggle="checkbox" required="" aria-required="true" name="delivery_slip_id[]" value="{{ $BillingListT->billing_list_id }}">
                                                                                </label>
                                                                            </div>
                                                                        </td> -->
                                                                        <!-- <td>{{ $k+1 }}</td> -->
                                                                        <td>{{ $BillingListT->customer_general_code }}</td>
                                                                        <td>{{ $BillingListT->delivery_slip_date }}</td>
                                                                        <td>{{ $BillingListT->delivery_no }}</td>
                                                                        <td>{{ $BillingListT->po_no }}</td>
                                                                        <td>{{ number_format($BillingListT->qty) }}</td>
                                                                        <td>{{ number_format($BillingListT->weight) }}</td>
                                                                        <td>{{ number_format($BillingListT->cubic) }}</td>
                                                                        <td>{{ number_format($BillingListT->subtotal,2) }}</td>
                                                                        <td>{{ $BillingListT->import_date }}</td>
                                                                        <td>
                                                                            @if($BillingListT->status == 'T')
                                                                                <span class="badge badge-success">@lang('lang.payment_complete')</span>
                                                                            @else
                                                                                <span class="badge badge-danger">@lang('lang.payment_not_complete')</span>
                                                                            @endif
                                                                        </td>
                                                                    </tr>

                                                                    @php
                                                                        $total_qty          += $BillingListT->qty;
                                                                        $total_weight       += $BillingListT->weight;
                                                                        $total_cubic        += $BillingListT->cubic;
                                                                        $total_subtotal     += $BillingListT->subtotal;
                                                                    @endphp
                                                                @endforeach
                                                                <tr>
                                                                    <th colspan="5">@lang('lang.total')</th>
                                                                    <td>{{ number_format($total_qty) }}</td>
                                                                    <td>{{ number_format($total_weight) }}</td>
                                                                    <td>{{ number_format($total_cubic) }}</td>
                                                                    <td>{{ number_format($total_subtotal,2) }}</td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                            @else
                                                                <tr id="list-po-products-not-found">
                                                                    <td colspan="12" style="text-align: center;">@lang('lang.data_not_found')</td>
                                                                </tr>
                                                            @endif
                                                        </tbody>
                                                        <tfooter>

                                                        </tfooter>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row" style="margin-top: 40px;">
                                            <div class="col-md-12">
                                                <h5>@lang('lang.unpaid_item')</h5>

                                                <label for="edit_active" class="checkbox form-check-label" style="color: red;">
                                                    <span class="icons">
                                                        <span class="first-icon fa fa-square"></span>
                                                        <span class="second-icon fa fa-check-square "></span>
                                                    </span>
                                                    <input type="checkbox" class="form-check-input" data-toggle="checkbox" required="" aria-required="true" disabled> @lang('lang.click_to_change_status_of_payment_successfully')
                                                </label>
                                                <div class="fix-scroll-dashboard">
                                                    <table class="table">
                                                        <thead>
                                                            <tr>
                                                                <th>
                                                                    <div class="form-check">
                                                                        <label for="edit_active" class="checkbox form-check-label">
                                                                            <span class="icons">
                                                                                <span class="first-icon fa fa-square"></span>
                                                                                <span class="second-icon fa fa-check-square "></span>
                                                                            </span><input type="checkbox" class="form-check-input" data-toggle="checkbox" id="check_active_all" required="" aria-required="true">
                                                                        </label>
                                                                    </div>
                                                                </th>
                                                                <th>@lang('lang.customer_id')</th>
                                                                <th>@lang('lang.delivery_date')</th>
                                                                <th>@lang('lang.invoice_no')</th>
                                                                <th>@lang('lang.number_po')</th>
                                                                <th>@lang('lang.quantity')</th>
                                                                <th>@lang('lang.weigh')</th>
                                                                <th>@lang('lang.queue_size')</th>
                                                                <th>@lang('lang.price')</th>
                                                                <th>@lang('lang.date_of_entry_to_the_warehouse_china')</th>
                                                                <th>@lang('lang.status')</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="list-po-products">
                                                            @if(! $BillingListsF->isEmpty())
                                                                @php
                                                                    $total_qty = 0;
                                                                    $total_weight = 0;
                                                                    $total_cubic = 0;
                                                                    $total_subtotal = 0;
                                                                @endphp
                                                                @foreach($BillingListsF as $BillingListF)
                                                                    <tr>
                                                                        <td>
                                                                            <div class="form-check">
                                                                                <label for="edit_active" class="checkbox form-check-label label-check-po">
                                                                                    <span class="icons">
                                                                                        <span class="first-icon fa fa-square"></span>
                                                                                        <span class="second-icon fa fa-check-square "></span>
                                                                                    </span>
                                                                                    <input type="checkbox" class="form-check-input input-check-po" data-toggle="checkbox" required="" aria-required="true" name="billing_list_id[]" value="{{ $BillingListF->billing_list_id }}">
                                                                                </label>
                                                                            </div>
                                                                        </td>
                                                                        <td>{{ $BillingListF->customer_general_code }}</td>
                                                                        <td>{{ $BillingListF->delivery_slip_date }}</td>
                                                                        <td>{{ $BillingListF->delivery_no }}</td>
                                                                        <td>{{ $BillingListF->po_no }}</td>
                                                                        <td>{{ number_format($BillingListF->qty) }}</td>
                                                                        <td>{{ number_format($BillingListF->weight) }}</td>
                                                                        <td>{{ number_format($BillingListF->cubic) }}</td>
                                                                        <td>{{ number_format($BillingListF->subtotal,2) }}</td>
                                                                        <td>{{ $BillingListF->import_date }}</td>
                                                                        <td>
                                                                            @if($BillingListF->status == 'T')
                                                                                <span class="badge badge-success">@lang('lang.payment_complete')</span>
                                                                            @else
                                                                                <span class="badge badge-danger">@lang('lang.payment_not_complete')</span>
                                                                            @endif
                                                                        </td>
                                                                    </tr>

                                                                    @php
                                                                        $total_qty          += $BillingListF->qty;
                                                                        $total_weight       += $BillingListF->weight;
                                                                        $total_cubic        += $BillingListF->cubic;
                                                                        $total_subtotal     += $BillingListF->subtotal;
                                                                    @endphp
                                                                @endforeach
                                                                <tr>
                                                                    <th colspan="5">@lang('lang.total')</th>
                                                                    <td>{{ number_format($total_qty) }}</td>
                                                                    <td>{{ number_format($total_weight) }}</td>
                                                                    <td>{{ number_format($total_cubic) }}</td>
                                                                    <td>{{ number_format($total_subtotal,2) }}</td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                            @else
                                                                <tr id="list-po-products-not-found">
                                                                    <td colspan="12" style="text-align: center;">@lang('lang.data_not_found')</td>
                                                                </tr>
                                                            @endif
                                                        </tbody>
                                                        <tfooter>

                                                        </tfooter>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary confirm-status" {{ $Billing->status == 'T' ? 'disabled' : null }}>@lang('lang.change_status_of_payment')</button>
                                        <!-- <button type="button" class="btn btn-warning" id="ConfirmBilling" {{ $Billing->status == 'T' ? 'disabled' : null }} >@lang('lang.close_bill')</button> -->
                                        <a href="{{ url('/admin/'.$lang.'/Billing') }}">
                                            <button type="button" class="btn btn-danger">@lang('lang.cancel')</button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    </div>
</div>
@endsection
@section('js_bottom')
<script src="{{asset('assets/global/plugins/orakuploader/orakuploader.js')}}"></script>
<script>
    

    $('body').on('change','#check_active_all',function(data){
        var active = $('#check_active_all').closest("label.checked");
        var input_check = $('#list-po-products').find('label.label-check-po');
        if(active.length > 0){
            $('.label-check-po').addClass('checked');
            $(".input-check-po").prop( "checked", true );
        }else{
            $('.label-check-po').removeClass('checked');
            $(".input-check-po").prop( "checked", false );
        }
    });


    $('body').on('click','.confirm-status',function(e){
        var id = $('#edit_id').val();
        var data_select = {};
        $("input[name='billing_list_id[]']:checked").each( function (k,v) {
            data_select[k] = $(this).val();
        });

        swal({
            title: "@lang('lang.do_you_want_to_do_this')",
            text: "@lang('lang.change_status_of_payment') / @lang('lang.close_bill')",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: "ใช่ ฉันต้องการยืนยัน",
            cancelButtonText: "@lang('lang.cancel')",
            showLoaderOnConfirm: true,
            buttonsStyling: false
        }).then(function() {
            $.ajax({
                method : "POST",
                url : url_gb+"/admin/{{$lang}}/Billing/ChangeStatus/"+id,
                data : {
                    billing_list_id : data_select,
                }
            }).done(function(rec){
                console.log(rec.status);
                if(rec.status==1){
                    swal(rec.title,rec.content,"success");
                    location.reload();
                }else{
                    swal(rec.title,rec.content,"error");
                }
            }).fail(function(data){
                swal("@lang('lang.system_system_error')","@lang('lang.please_contact_the_administrator')","error");
            });
        }).catch(function(e){
            //console.log(e);
        });

    });

    $('body').on('click','#ConfirmBilling',function(e){
        e.preventDefault();
        var btn = $(this);
        var id = $('#edit_id').val();;
        swal({
            title: "คุณต้องการทำการปิดบิลใช่หรือไม่",
            text: "หากคุณำการปิดบิลจะไม่สามารถยกเลิกการทำรายการได้อีก",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: "ใช่ ฉันต้องการปิดบิล",
            cancelButtonText: "@lang('lang.cancel')",
            showLoaderOnConfirm: true,
            buttonsStyling: false
        }).then(function() {
            $.ajax({
                method : "POST",
                url : url_gb+"/admin/{{$lang}}/Billing/ConfirmBilling/"+id,
                data : {ID : id}
            }).done(function(rec){
                if(rec.status==1){
                    swal(rec.title,rec.content,"success");
                    location.reload();
                    //window.location.href = url_gb+"/admin/{{$lang}}/Billing";
                    //TableList.api().ajax.reload();
                }else{
                    swal("@lang('lang.system_system_error')","กรุณาติดต่อผู้ดูแล","error");
                }
            }).fail(function(data){
                swal("@lang('lang.system_system_error')","กรุณาติดต่อผู้ดูแล","error");
            });
        }).catch(function(e){
            //console.log(e);
        });
    });

</script>
@endsection
