@extends('Admin.layouts.layout')
@section('css_bottom')
<style>
    .fix-scroll-dashboard {
        width: 100%;
        overflow-x: scroll;
    }
</style>
@endsection
@section('body')
<div class="content">
    <div class="col-md-12">
    <div class="container-fluid">
        <form id="FormGetQRCode">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="content">
                            <h4 class="title">
                                {{$title_page or '' }}
                                <a href="{{ url('/admin/'.$lang.'/ImportToThai/Edit/'.$Container->id) }}" class="btn btn-success btn-add pull-right" >
                                    + <i class="fa fa-desktop" style="font-size: 20px;"></i>
                                </a>
                            </h4>
                            <h4 class="title">
                                <a href="{{ url('/admin/'.$lang.'/ImportToThai/Mobile/Edit/'.$Container->id) }}" class="btn btn-success btn-add pull-right" >
                                    + <i class="fa fa-mobile" style="font-size: 25px;"></i>&nbsp&nbsp
                                </a>
                            </h4>
                            <div class="material-datatables">
                                <div class="modal-header">

                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-3">
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="add_container_id">@lang('lang.select_container')</label>
                                                <select name="container_id" class="select2 form-control" tabindex="-1" data-placeholder="@lang('lang.select_container')" id="add_container_id" >
                                                    <option value="{{ $Container->id }}" selected>{{ $Container->container_code }}</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="add_qrcode">@lang('lang.barcode')</label>
                                                <input type="text" class="form-control" name="qr_code" id="add_qrcode" value="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12" style="text-align: center;">
                                            <button type="submit" class="btn btn-get-qrcode btn-primary">@lang('lang.save')</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <form id="FormAddImportToThai">
            <input type="hidden" name="container_id" id="show_container_id">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="content">
                            <div class="material-datatables fix-scroll-dashboard">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="myModalLabel">@lang('lang.table_of_products_scanned_by_po')</h5>
                                </div>
                                <div class="modal-body">
                                    <table class="table">
                                        <thead class="text-primary">
                                            <tr>
                                                <th>#</th>
                                                <th>@lang('lang.number_po')</th>
                                                <th>@lang('lang.number_of_po')</th>
                                                <th>@lang('lang.number_piece_close')</th>
                                                <th>@lang('lang.actual_number')</th>
                                            </tr>
                                        </thead>
                                        <tbody id="list-products">
                                        </tbody>
                                    <tfooter>

                                    </tfooter>
                                    </table>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="add_customer_id">@lang('lang.note')</label>
                                            <input type="text" class="form-control" name="remark" id="add_remark" value='{{ !empty($remark) ? $remark->remark : '' }}'>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary">@lang('lang.save')</button>
                                    <button type="cancel" class="btn btn-danger">@lang('lang.cancel')</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="card">
                        <div class="content">
                            <div class="material-datatables fix-scroll-dashboard">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="myModalLabel">@lang('lang.list_po')</h5>
                                </div>
                                <div class="modal-body">
                                    <table class="table">
                                        <thead class="text-primary">
                                            <tr>
                                                <th>#</th>
                                                <th>@lang('lang.customer_id')</th>
                                                <th>@lang('lang.number_po')</th>
                                                <th>@lang('lang.delivery_dates')</th>
                                                <th>@lang('lang.number_of_po')</th>
                                                <th>@lang('lang.weigh')</th>
                                                <th>@lang('lang.queue_size')</th>
                                            </tr>
                                        </thead>
                                        <tbody id="list-po">
                                                <tr id="op-not-found">
                                                    <td colspan=10 style="text-align: center;">@lang('lang.data_not_found')</td>
                                                </tr>
                                        </tbody>
                                        <tfooter>

                                        </tfooter>
                                    </table>
                                </div>
                                <div class="modal-footer">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="card">
                        <div class="content">
                            <div class="material-datatables fix-scroll-dashboard">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="myModalLabel">@lang('lang.barcode_list')</h5>
                                </div>
                                <div class="modal-body">
                                    <table class="table">
                                        <thead class="text-primary">
                                            <tr>
                                                <th>#</th>
                                                <th>@lang('lang.number_po')</th>
                                                <th>@lang('lang.qrcode')</th>
                                                <th>@lang('lang.lot_id')</th>
                                                <th>@lang('lang.customer_id')</th>
                                                <th>@lang('lang.producttype')</th>
                                                <th>@lang('lang.product_name')</th>
                                            </tr>
                                        </thead>
                                        <tbody id="list-qr-code">
                                            @if(!empty($QrCodeProducts))
                                                @foreach($QrCodeProducts as $QrCodeProduct)
                                                    <tr>
                                                        <td>#
                                                            <input type="hidden" class="po_id" value="{{$QrCodeProduct->import_to_chaina_id}}" />
                                                            <input type="hidden" class="po_no" value="{{$QrCodeProduct->po_no}}" />
                                                            <input type="hidden" class="qr_code_name" value="{{ $QrCodeProduct->qr_code }}" />
                                                            <input type="hidden" class="lot_product_id" value="{{ $QrCodeProduct->lot_product_id }}" />
                                                            <input type="hidden" class="po_qty" value="{{ $QrCodeProduct->po_qty }}" />
                                                            <input type="hidden" class="lot_product_qty" value="{{ $QrCodeProduct->lot_product_qty }}" />
                                                            <input type="hidden" class="qr_code_container_qty" value="{{ $QrCodeProduct->qr_code_container_qty }}" />
                                                            <input type="hidden" class="customer_general_code" value="{{ $QrCodeProduct->customer_general_code }}" />
                                                            <input type="hidden" class="weight_all" value="{{ $QrCodeProduct->weight_all }}" />
                                                            <input type="hidden" class="cubic_all" value="{{ $QrCodeProduct->cubic_all }}" />
                                                            <input type="hidden" class="date_import" value="{{ $QrCodeProduct->date_import }}" />
                                                        </td>
                                                        <td>{{ $QrCodeProduct->po_no }}</td>
                                                        <td>{{ $QrCodeProduct->qr_code }}</td>
                                                        <td>{{ $QrCodeProduct->lot_product_id }}</td>
                                                        <td>{{ $QrCodeProduct->customer_general_code }}</td>
                                                        <td>{{ $QrCodeProduct->product_type_name }}</td>
                                                        <td>{{ $QrCodeProduct->product_name }}</td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                        </tbody>
                                        <tfooter>

                                        </tfooter>
                                    </table>
                                </div>
                                <div class="modal-footer">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
@section('js_bottom')
<script src="{{asset('assets/global/plugins/orakuploader/orakuploader.js')}}"></script>
<script>

    $(function(){
        showPOandLot();
        $('#show_container_id').val( $('#add_container_id').val() );
        $('#add_qrcode').focus();
    });

    $('body').on('change', '#add_container_id', function(){
        var container_id = $(this).val();
        $('#show_container_id').val(container_id);
        $('#add_qrcode').val('');
        $('#add_remark').val('');
        $('#list-po').html('');
        $('#op-not-found').show();
        $('#list-qr-code').html('');
        $('#qr-code-not-found').show();
        $('#list-products').html('');
        $('#products-not-found').show();
        $('#add_qrcode').focus();
    });

    $('#FormAddImportToThai').validate({
        errorElement: 'div',
        errorClass: 'invalid-feedback',
        focusInvalid: false,
        rules: {
            contauner_id: {
                required: true,
            },

        },
        messages: {

            contauner_id: {
                required: "@lang('lang.please_specify')",
            },

        },
        highlight: function (e) {
            validate_highlight(e);
        },
        success: function (e) {
            validate_success(e);
        },

        errorPlacement: function (error, element) {
            validate_errorplacement(error, element);
        },
        submitHandler: function (form) {
            /*
            if(CKEDITOR!==undefined){
                for ( instance in CKEDITOR.instances ){
                    CKEDITOR.instances[instance].updateElement();
                }
            }
            */
            var btn = $(form).find('[type="submit"]');
            btn.button("loading");
            $.ajax({
                method : "POST",
                url : url_gb+"/admin/{{$lang}}/ImportToThai/update",
                dataType : 'json',
                data : $(form).serialize()+'&po='+JSON.stringify(showPo)
            }).done(function(rec){
                btn.button("reset");
                if(rec.status==1){
                    swal(rec.title,rec.content,"success");
                    window.location.href = url_gb+"/admin/{{$lang}}/ImportToThai";
                }else{
                    swal(rec.title,rec.content,"error");
                }
            }).fail(function(){
                swal("system.system_alert","@lang('lang.system_system_error')","error");
                btn.button("reset");
            });
        },
        invalidHandler: function (form) {

        }
    });

    $('#FormGetQRCode').validate({
        errorElement: 'div',
        errorClass: 'invalid-feedback',
        focusInvalid: false,
        rules: {
            container_id: {
                required: true,
            },
            qr_code: {
                required: true,
            },

        },
        messages: {

            container_id: {
                required: "@lang('lang.please_specify')",
            },
            qr_code: {
                required: "@lang('lang.please_specify')",
            },

        },
        highlight: function (e) {
            validate_highlight(e);
        },
        success: function (e) {
            validate_success(e);
        },

        errorPlacement: function (error, element) {
            validate_errorplacement(error, element);
        },
        submitHandler: function (form) {
            /*
            if(CKEDITOR!==undefined){
                for ( instance in CKEDITOR.instances ){
                    CKEDITOR.instances[instance].updateElement();
                }
            }
            */
            var btn = $(form).find('[type="submit"]');
            btn.button("loading");
            $.ajax({
                method : "POST",
                url : url_gb+"/admin/{{$lang}}/ImportToThai/CheckQrCodeEdit",
                dataType : 'json',
                data : $(form).serialize()
            }).done(function(rec){
                
                btn.button("reset");
                var html_list_qr_code = "";
                if(rec.status==1){
                    // if(rec.QrCodeProduct.id != qr_code_id){
                        html_list_qr_code += '\
                            <tr>\
                                <td>#\
                                    <input type="hidden" class="po_id" value="'+rec.ImportToChaina.id+'" />\
                                    <input type="hidden" class="po_no" value="'+rec.ImportToChaina.po_no+'" />\
                                    <input type="hidden" class="qr_code_name" value="'+rec.QrCodeProduct.qr_code+'" />\
                                    <input type="hidden" class="lot_product_id" value="'+rec.LotProduct.id+'" />\
                                    <input type="hidden" class="po_qty" value="'+rec.qty_po+'" />\
                                    <input type="hidden" class="lot_product_qty" value="'+rec.qty_lot_product+'" />\
                                    <input type="hidden" class="qr_code_container_qty" value="'+rec.qty_container+'" />\
                                    <input type="hidden" class="customer_general_code" value="'+rec.QrCodeProduct.import_to_chaina.user.customer_general_code+'" />\
                                    <input type="hidden" class="weight_all" value="'+rec.weight_all+'" />\
                                    <input type="hidden" class="cubic_all" value="'+rec.cubic+'" />\
                                    <input type="hidden" class="date_import" value="'+rec.date_import+'" />\
                                </td>\
                                <td>'+rec.ImportToChaina.po_no+'</td>\
                                <td>'+rec.QrCodeProduct.qr_code+'</td>\
                                <td>'+rec.LotProduct.id+'</td>\
                                <td>'+rec.QrCodeProduct.import_to_chaina.user.customer_general_code+'</td>\
                                <td>'+rec.QrCodeProduct.product_import_to_chaina.product_type.name+'</td>\
                                <td>'+rec.QrCodeProduct.product_import_to_chaina.product.name+'</td>\
                            </tr>\
                        ';

                        $('#qr-code-not-found').hide();
                        $('#list-qr-code').append(html_list_qr_code);
                        showPOandLot();
                        beep();
                    // }else{
                    //     swal("ระบบแจ้งเตือน","รายการถูกคีย์เข้าระบบแล้ว กรุณาลองใหม่", "error");
                    // }
                }else{
                    swal(rec.title,rec.content,"error");
                }
                $('#add_qrcode').val('');
            }).fail(function(){
                swal("system.system_alert","@lang('lang.system_system_error')","error");
                btn.button("reset");
            });
        },
        invalidHandler: function (form) {

        }
    });

    //$('#add_container_id').select2();

    var groups = {};
    var showPo = {};
    function showPOandLot(){ // function show Po and Lot
        var product_for_show = $('body').find('#list-qr-code > tr');
        // console.log(product_for_show);
        // delete groups;
        groups = {};
        showPo = {};
        var html = '';
        if (product_for_show.length > 0) {
            // set data in variable
            $.each(product_for_show, function(k, v){
                var po_id = $(v).find('.po_id').val();
                var po = $(v).find('.po_no').val();
                var lot_id = $(v).find('.lot_product_id').val();
                var qr_code_name = $(v).find('.qr_code_name').val();
                var po_qty = $(v).find('.po_qty').val();
                var lot_product_qty = $(v).find('.lot_product_qty').val();
                var qr_code_container_qty = $(v).find('.qr_code_container_qty').val();
                var customer_general_code = $(v).find('.customer_general_code').val();
                var weight_all = $(v).find('.weight_all').val();
                var cubic_all = $(v).find('.cubic_all').val();
                var date_import = $(v).find('.date_import').val();
                // set po
                if (!showPo[po_id]) {
                    showPo[po_id] = {};
                }
                showPo[po_id]['customer_general_code'] = customer_general_code;
                showPo[po_id]['po_qty'] = po_qty;
                showPo[po_id]['po_no'] = po;
                showPo[po_id]['po_id'] = po_id;
                showPo[po_id]['date_import'] = date_import;
                showPo[po_id]['weight_all'] = weight_all;
                showPo[po_id]['cubic_all'] = cubic_all;
                // end set po
                if (!groups[po]) {
                    groups[po] = {};
                }
                if (!groups[po][lot_id]) {
                    groups[po][lot_id] = {};
                }
                if (!groups[po][lot_id]['amount']) {
                    groups[po][lot_id]['amount'] = [];
                }
                groups[po]['po_qty'] = po_qty;
                groups[po]['qr_code_container_qty'] = qr_code_container_qty;
                groups[po][lot_id]['lot_product_qty'] = lot_product_qty;
                groups[po][lot_id]['amount'].push(qr_code_name);
            });
            // end set data in variable
            // set html in table
            var No_Po = 1;
            var No_po_show = 1;
            var style = '';
            var style_lot = '';
            var show_po_html = '';
            $.each(showPo, function(key_po, po_show){
                show_po_html += '\
                    <tr>\
                        <td>'+(No_po_show)+'</td>\
                        <td>'+po_show.customer_general_code+'</td>\
                        <td>'+po_show.po_no+'</td>\
                        <td>'+po_show.date_import+'</td>\
                        <td>'+po_show.po_qty+'</td>\
                        <td>'+po_show.weight_all+'</td>\
                        <td>'+po_show.cubic_all+'</td>\
                    </tr>';
                No_po_show++;
            });
            $('#list-po').html(show_po_html);
            $.each(groups, function(k1, v1){
                var sumallproduct = 0;
                $.each(v1, function(k2, v2){
                    if (k2 != 'po_qty' && k2 != 'qr_code_container_qty') {
                        sumallproduct += v2.amount.length;
                    }
                });
                if(v1.qr_code_container_qty > sumallproduct){
                    style = 'rgba(255, 0, 0, 0.33)';
                }else if(v1.po_qty < sumallproduct){
                    style = 'rgba(255, 235, 59, 0.5)';
                }else{
                    style = 'rgba(76, 175, 80, 0.42)';
                }
                html += '\
                    <tr style="background-color: '+style+'">\
                        <td>'+No_Po+'</td>\
                        <td>'+k1+'</td>\
                        <td class="text-center">'+addNumformat(v1.po_qty)+'</td>\
                        <td class="text-center">'+addNumformat(v1.qr_code_container_qty)+'</td>\
                        <td class="text-center">'+addNumformat(sumallproduct)+'</td>\
                    </tr>';
                $.each(v1, function(k2, v2){
                    if (k2 != 'po_qty' && k2 != 'qr_code_container_qty') {
                        if(v2.lot_product_qty > v2.amount.length){
                            style_lot = 'rgba(255, 0, 0, 0.33)';
                        }else if(v2.lot_product_qty < v2.amount.length){
                            style_lot = 'rgba(255, 235, 59, 0.5)';
                        }else{
                            style_lot = 'rgba(76, 175, 80, 0.42)';
                        }
                        html += '\
                            <tr style="background-color: '+style_lot+'">\
                                <td></td>\
                                <td></td>\
                                <td class="text-right">Lot '+k2+'</td>\
                                <td class="text-center">'+addNumformat(v2.lot_product_qty)+'</td>\
                                <td class="text-center">'+addNumformat(v2.amount.length)+'</td>\
                            </tr>';
                    }
                });
                No_Po++;
            });
        }else{
            html += '\
                <tr>\
                    <td colspan="6" class="text-center">@lang('lang.data_not_found')</td>\
                </tr>';
        }
        $('#list-products').html(html);
        // end set html in table
    }

</script>
@endsection
