<!--
 <style>
    @page{
        header: page-header;
        footer: page-footer;
        margin-top: 50px;
        margin-bottom: 150px;
    }
    .page-break {
        page-break-after: always;
    }
    body{
        font-family: Arial;
    }
    td {
        /height: 25px;
    }
    th {
        font-weight: normal;
    }
</style>
-->

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>ใบสั่งซื้อสินค้า / รายการสินค้า</title>
        <style>
            @font-face {
                font-family: 'THSarabunNew';
                font-style: normal;
                font-weight: normal;
                src: url("{{ asset('fonts/THSarabunNew.ttf') }}") format('truetype');
            }

            body {
                font-family: "THSarabunNew";
                /* background-image: url('images/po/form-po.jpg');
                background-repeat: no-repeat;
                background-size: 100% 100%; */

            }
            .bg-img {
                    background-image: url('images/po/form-po.jpg');
                    background-repeat: no-repeat;
                    background-size: 100% 100%;
                    height: 100%;
            }
            .table_css tr {
                font-weight: normal;
                font-family: "THSarabunNew";
                font-size: 16px;
            }
            .table_css td {
                height: 25px;
                font-family: "THSarabunNew";
                font-size: 16px;
            }
            .table_css th {
                font-weight: normal;
                font-family: "THSarabunNew";
                font-size: 16px;
            }
             .table_list td {
                height: 25px;
                font-family: "THSarabunNew";
                font-size: 16px;
            }
            .table_list th {
                font-weight: normal;
                font-family: "THSarabunNew";
                font-size: 16px;
            }
        </style>
    </head>
    <body lang="th">
        <div class="bg-img">

        <!-- <img src="{{asset('images/po/form-po.jpg')}}" alt=""> -->
        <htmlpageheader name="page-header">

        </htmlpageheader>

        <htmlpagebody>
            <div class="row">
                <div style="color: black; text-align: left; font-size: 13px; ">

                </div>
            </div>
            <div class="row" style="padding-top: 20px;">
                <div style="color: black; text-align: center; font-size: 16px; ">
                </div>
            </div>

            <div class="row" style="padding-top: 20px;">
                
            </div>

        </htmlpagebody>

        <htmlpagefooter name="page-footer">
            <div style="padding-bottom: 40px;text-align: center;">

            </div>
        </htmlpagefooter>
        </div>
    </body>
