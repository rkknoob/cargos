
<table border="1" cellspacing="0">
    <tr>
        <td colspan="13">NEW CARGO (EK){{date('d', strtotime($QrCodeProduct[0]->container_created_at))}}(2)/{{date('m/Y', strtotime($QrCodeProduct[0]->container_created_at))}}</td>
    </tr>
    <tr>
        <td>Receipt date</td>
        <td>No. Container</td>
        <td>Customer Code</td>
        <td>Po Number</td>
        <td>Item</td>
        <td>Descriptions Of Good</td>
        <td>PCS</td>
        <td>Package</td>
        <td>Brand</td>
        <td>KG</td>
        <td>Cubic Meter</td>
        <td>Remark</td>
        <td>Remark</td>
    </tr>

    @php
        $check_lot_id = '';
    @endphp
    @foreach($QrCodeProduct as $product)
        @php
            $user_product_type_id = '';
            if($product->user_product_type_id == 5){
                $user_product_type_id = 'NO';
            }else if($product->user_product_type_id == 6){
                $user_product_type_id = 'YES';
            }
        @endphp
        <tr>
            <td>{{date('Y/m/d', strtotime($product->product_created_at))}}</td>
            <td>{{$product->container_code}}</td>
            <td>{{$product->customer_general_code}}</td>
            <td>{{$product->po_no}}</td>
            <td>{{number_format($product->sort_id)}}</td>
            <td>{{$product->product_name}}</td>
            <td>{{$product->product_pcs}}</td>
            @if($check_lot_id != $product->lot_product_id)
                <td>{{$product->qr_code_container_qty}}</td>
                <td>{{$user_product_type_id}}</td>
            @else
                <td></td>
                <td></td>
            @endif
            @if($check_lot_id != $product->lot_product_id)
                @php $check_lot_id = $product->lot_product_id @endphp
                <td>{{number_format($product->weight_per_item, 3)}}</td>
                <td>{{number_format(($product->product_cobic/$product->lot_product_qty)/100, 3)}}</td>
            @else
                <td></td>
                <td></td>
            @endif
            <td>{{$product->transport_type_name}}</td>
            <td></td>
        </tr>
    @endforeach
    
</table>

