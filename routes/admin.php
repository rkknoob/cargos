<?php

/*
  |--------------------------------------------------------------------------
  | Admin Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

// Route::get('/admin/login', 'Admin\AuthController@login');
// Route::post('/admin/CheckLogin', 'Admin\AuthController@CheckLogin');
// Route::get('/admin/debug', function() {
//     return view('debug');
// });


Route::prefix('admin')->group(function () {
    $langs = ['', 'th', 'en', 'ch'];
    foreach ($langs as $lang) {
        Route::prefix($lang)->group(function () {
            Route::get('/login', 'Admin\AuthController@login');
            Route::post('/CheckLogin', 'Admin\AuthController@CheckLogin');
            Route::get('/debug', function() {
                return view('debug');
            });
        });
    }
});


Route::group(['middleware' => 'admin_auth', 'prefix' => 'admin'], function() {
    $langs = ['', 'th', 'en', 'ch'];
    foreach ($langs as $lang) {
        Route::prefix($lang)->group(function () {
            Route::get('/', 'Admin\HomeController@index');
            Route::get('/logout', 'Admin\AuthController@logout');
            Route::get('/dashboard', 'Admin\HomeController@index');

            Route::post('/upload_file', 'Admin\UploadFileController@index');

            //User
            Route::get('/change_password', 'Admin\UserController@change_password');
                Route::get('/profile', 'Admin\UserController@profile');
                Route::get('/user/ListUser', 'Admin\UserController@ListUser');
                Route::post('/user/checkedit/{id}', 'Admin\UserController@update');
                Route::post('/user/delete/{id}', 'Admin\UserController@destroy');
                Route::resource('/user', 'Admin\UserController');
                // Route::get('/user', 'Admin\UserController@index');
                Route::get('/logout', 'Admin\AuthController@logout');

            //ManageMenu
            Route::get('/ManageMenu', 'Admin\MenuController@index');
                Route::get('/menu/ListMenu', 'Admin\MenuController@ListMenu');
                Route::post('/menu', 'Admin\MenuController@store');
                Route::get('/menu/{id}', 'Admin\MenuController@edit');
                Route::post('/menu/checkedit/{id}', 'Admin\MenuController@update');
                Route::post('/menu/delete/{id}', 'Admin\MenuController@destroy');

            //SetPermission
            Route::post('/SetPermission', 'Admin\MenuController@SetPermission');
                Route::post('/GetPermission/{id}', 'Admin\MenuController@GetPermission');

            Route::get('/Install', 'Admin\InstallController@index');
                Route::get('/Install/DefaultView', 'Admin\InstallController@DefaultView');
                Route::post('/Install/GetFieldDropDown', 'Admin\InstallController@GetFieldDropDown');
                Route::post('/Install/GetField/{table}', 'Admin\InstallController@GetField');
                Route::post('/Install', 'Admin\InstallController@Install');

            Route::get('/Menu', 'Admin\MenuController@index');
                Route::get('/Menu/Lists', 'Admin\MenuController@Lists');
                Route::post('/Menu', 'Admin\MenuController@store');
                Route::get('/Menu/{id}', 'Admin\MenuController@show');
                Route::post('/Menu/{id}', 'Admin\MenuController@update');
                Route::post('/Menu/Delete/{id}', 'Admin\MenuController@destroy');

            Route::get('/AdminUser', 'Admin\AdminUserController@index');
                Route::get('/AdminUser/Lists', 'Admin\AdminUserController@Lists');
                Route::post('/AdminUser', 'Admin\AdminUserController@store');
                Route::post('/AdminUser/change_password', 'Admin\AdminUserController@change_password');
                Route::get('/AdminUser/{id}', 'Admin\AdminUserController@show');
                Route::post('/AdminUser/{id}', 'Admin\AdminUserController@update');
                Route::post('/AdminUser/Delete/{id}', 'Admin\AdminUserController@destroy');

            Route::get('/Test', 'Admin\TestController@index');
                Route::get('/Test/Lists', 'Admin\TestController@Lists');
                Route::post('/Test', 'Admin\TestController@store');
                Route::get('/Test/{id}', 'Admin\TestController@show');
                Route::post('/Test/{id}', 'Admin\TestController@update');
                Route::post('/Test/Delete/{id}', 'Admin\TestController@destroy');

            Route::get('/User', 'Admin\UserController@index');
                Route::get('/User/Lists', 'Admin\UserController@Lists');
                Route::get('/User/ListsAddress', 'Admin\UserController@ListsAddress');
                Route::get('/User/ListPrice', 'Admin\UserController@ListsPrice');
                Route::post('/User', 'Admin\UserController@store');
                Route::post('/User/AddAddress', 'Admin\UserController@AddAddress');
                Route::post('/User/AddPrice', 'Admin\UserController@AddPrice');
                Route::get('/User/EditAddress/{id}', 'Admin\UserController@EditAddress');
                Route::get('/User/EditPrice/{id}', 'Admin\UserController@EditPrice');
                Route::get('/User/{id}', 'Admin\UserController@show');
                Route::post('/User/{id}', 'Admin\UserController@update');
                Route::post('/User/Delete/{id}', 'Admin\UserController@destroy');
                Route::post('/User/AddressDelete/{id}', 'Admin\UserController@AddressDelete');
                Route::post('/User/PriceDelete/{id}', 'Admin\UserController@PriceDelete');
                Route::get('/User/GetAddress/{id}', 'Admin\UserController@GetAddress');
                Route::post('/User/change_password/{id}', 'Admin\UserController@change_password');

            Route::get('/ContainerType', 'Admin\ContainerTypeController@index');
                Route::get('/ContainerType/Lists', 'Admin\ContainerTypeController@Lists');
                Route::post('/ContainerType', 'Admin\ContainerTypeController@store');
                Route::get('/ContainerType/{id}', 'Admin\ContainerTypeController@show');
                Route::post('/ContainerType/{id}', 'Admin\ContainerTypeController@update');
                Route::post('/ContainerType/Delete/{id}', 'Admin\ContainerTypeController@destroy');

              Route::get('/ContainerSize', 'Admin\ContainerSizeController@index');
                Route::get('/ContainerSize/Lists', 'Admin\ContainerSizeController@Lists');
                Route::post('/ContainerSize', 'Admin\ContainerSizeController@store');
                Route::get('/ContainerSize/{id}', 'Admin\ContainerSizeController@show');
                Route::post('/ContainerSize/{id}', 'Admin\ContainerSizeController@update');
                Route::post('/ContainerSize/Delete/{id}', 'Admin\ContainerSizeController@destroy');

              Route::get('/ContainerStatus', 'Admin\ContainerStatusController@index');
                Route::get('/ContainerStatus/Lists', 'Admin\ContainerStatusController@Lists');
                Route::post('/ContainerStatus', 'Admin\ContainerStatusController@store');
                Route::get('/ContainerStatus/{id}', 'Admin\ContainerStatusController@show');
                Route::post('/ContainerStatus/{id}', 'Admin\ContainerStatusController@update');
                Route::post('/ContainerStatus/Delete/{id}', 'Admin\ContainerStatusController@destroy');

              Route::get('/ContainerPhotoType', 'Admin\ContainerPhotoTypeController@index');
                Route::get('/ContainerPhotoType/Lists', 'Admin\ContainerPhotoTypeController@Lists');
                Route::post('/ContainerPhotoType', 'Admin\ContainerPhotoTypeController@store');
                Route::get('/ContainerPhotoType/{id}', 'Admin\ContainerPhotoTypeController@show');
                Route::post('/ContainerPhotoType/{id}', 'Admin\ContainerPhotoTypeController@update');
                Route::post('/ContainerPhotoType/Delete/{id}', 'Admin\ContainerPhotoTypeController@destroy');

              Route::get('/TransportType', 'Admin\TransportTypeController@index');
                Route::get('/TransportType/Lists', 'Admin\TransportTypeController@Lists');
                Route::post('/TransportType', 'Admin\TransportTypeController@store');
                Route::get('/TransportType/{id}', 'Admin\TransportTypeController@show');
                Route::post('/TransportType/{id}', 'Admin\TransportTypeController@update');
                Route::post('/TransportType/Delete/{id}', 'Admin\TransportTypeController@destroy');

              Route::get('/ProductType', 'Admin\ProductTypeController@index');
                Route::get('/ProductType/Lists', 'Admin\ProductTypeController@Lists');
                Route::post('/ProductType', 'Admin\ProductTypeController@store');
                Route::get('/ProductType/{id}', 'Admin\ProductTypeController@show');
                Route::post('/ProductType/DeleteAll', 'Admin\ProductTypeController@DeleteAll');
                Route::post('/ProductType/{id}', 'Admin\ProductTypeController@update');
                Route::post('/ProductType/Delete/{id}', 'Admin\ProductTypeController@destroy');

              Route::get('/Perfix', 'Admin\PerfixController@index');
                Route::get('/Perfix/Lists', 'Admin\PerfixController@Lists');
                Route::post('/Perfix', 'Admin\PerfixController@store');
                Route::get('/Perfix/{id}', 'Admin\PerfixController@show');
                Route::post('/Perfix/{id}', 'Admin\PerfixController@update');
                Route::post('/Perfix/Delete/{id}', 'Admin\PerfixController@destroy');

              Route::get('/Province', 'Admin\ProvinceController@index');
                Route::get('/Province/Lists', 'Admin\ProvinceController@Lists');
                Route::post('/Province', 'Admin\ProvinceController@store');
                Route::get('/Province/{id}', 'Admin\ProvinceController@show');
                Route::post('/Province/{id}', 'Admin\ProvinceController@update');
                Route::post('/Province/Delete/{id}', 'Admin\ProvinceController@destroy');

              Route::get('/Amphure', 'Admin\AmphureController@index');
                Route::get('/Amphure/Lists', 'Admin\AmphureController@Lists');
                Route::post('/Amphure', 'Admin\AmphureController@store');
                Route::get('/Amphure/{id}', 'Admin\AmphureController@show');
                Route::post('/Amphure/{id}', 'Admin\AmphureController@update');
                Route::post('/Amphure/Delete/{id}', 'Admin\AmphureController@destroy');
                Route::get('/Amphure/GetAmphur/{id}', 'Admin\AmphureController@GetAmphur');

            Route::get('/ImportToChaina', 'Admin\ImportToChainaController@index');
                Route::post('/ImportToChaina/Lists', 'Admin\ImportToChainaController@Lists');
                Route::get('/ImportToChaina/create', 'Admin\ImportToChainaController@create');
                Route::post('/ImportToChaina/create', 'Admin\ImportToChainaController@store');
                Route::post('/ImportToChaina/showDetail', 'Admin\ImportToChainaController@show');
                Route::post('/ImportToChaina/GetPrice', 'Admin\ImportToChainaController@GetPrice');
                Route::get('/ImportToChaina/QRCode/{id}', 'Admin\ImportToChainaController@QRCode');
                Route::get('/ImportToChaina/edit/{id}', 'Admin\ImportToChainaController@edit');
                Route::post('/ImportToChaina/edit/{id}', 'Admin\ImportToChainaController@update');
                Route::post('/ImportToChaina/Delete/{id}', 'Admin\ImportToChainaController@destroy');
                Route::get('/ImportToChaina/WeighLot/{id}', 'Admin\ImportToChainaController@WeighLot');
                Route::post('/ImportToChaina/WeighLot/{id}', 'Admin\ImportToChainaController@WeighLotUpdate');

            Route::get('/ImportToChainaContainer', 'Admin\ImportToChainaContainerController@index');
                Route::post('/ImportToChainaContainer/Lists', 'Admin\ImportToChainaContainerController@Lists');
                Route::post('/ImportToChainaContainer/ContainerSave', 'Admin\ImportToChainaContainerController@ContainerSave');
                Route::post('/ImportToChainaContainer/destroyContainerProduct', 'Admin\ImportToChainaContainerController@destroyContainerProduct');
                Route::post('/ImportToChainaContainer/GetQRCode', 'Admin\ImportToChainaContainerController@show');
                Route::post('/ImportToChainaContainer/GetContainerDetail/{id}', 'Admin\ImportToChainaContainerController@GetContainerDetail');
                Route::get('/ImportToChainaContainer/create', 'Admin\ImportToChainaContainerController@create');
                Route::get('/ImportToChainaContainer/MobileCreate', 'Admin\ImportToChainaContainerController@MobileCreate');
                Route::post('/ImportToChainaContainer/create', 'Admin\ImportToChainaContainerController@store');
                Route::get('/ImportToChainaContainer/edit/{id}', 'Admin\ImportToChainaContainerController@edit');
                Route::get('/ImportToChainaContainer/MobileEdit/{id}', 'Admin\ImportToChainaContainerController@MobileEdit');
                Route::post('/ImportToChainaContainer/edit/{id}', 'Admin\ImportToChainaContainerController@update');
                Route::post('/ImportToChainaContainer/Delete/{id}', 'Admin\ImportToChainaContainerController@destroy');
                Route::get('/ImportToChainaContainer/PDF_Container/{id}', 'Admin\ImportToChainaContainerController@PDF_Container');
                Route::get('/ImportToChainaContainer/PDF_ContainerSendStation/{id}', 'Admin\ImportToChainaContainerController@PDF_ContainerSendStation');
                Route::get('/ImportToChainaContainer/Excel_Container/{id}', 'Admin\ImportToChainaContainerController@Excel_Container');
                Route::get('/ImportToChainaContainer/Excel_ContainerSendStation/{id}', 'Admin\ImportToChainaContainerController@Excel_ContainerSendStation');

            Route::get('/ImportToThai', 'Admin\ImportToThaiController@index');
                Route::get('/ImportToThai/Lists', 'Admin\ImportToThaiController@Lists');
                Route::get('/ImportToThai/Create', 'Admin\ImportToThaiController@Create');
                Route::get('/ImportToThai/Mobile/Create', 'Admin\ImportToThaiController@MobileCreate');
                Route::post('/ImportToThai', 'Admin\ImportToThaiController@store');
                Route::post('/ImportToThai/CheckQrCode', 'Admin\ImportToThaiController@CheckQrCode');
                Route::post('/ImportToThai/CheckQrCodeEdit', 'Admin\ImportToThaiController@CheckQrCodeEdit');
                Route::post('/ImportToThai/update', 'Admin\ImportToThaiController@update');
                Route::get('/ImportToThai/{id}', 'Admin\ImportToThaiController@show');
                Route::post('/ImportToThai/Delete/{id}', 'Admin\ImportToThaiController@destroy');
                Route::get('/ImportToThai/PrintPo/{id}', 'Admin\ImportToThaiController@PrintPo');
                Route::get('/ImportToThai/Edit/{id}', 'Admin\ImportToThaiController@Edit');
                Route::get('/ImportToThai/Mobile/Edit/{id}', 'Admin\ImportToThaiController@MobileEdit');
                Route::get('/ImportToThai/Detail/{id}', 'Admin\ImportToThaiController@Detail');

            Route::get('/OrderChainaContainer', 'Admin\OrderChainaContainerController@index');
                Route::post('/OrderChainaContainer/Lists', 'Admin\OrderChainaContainerController@Lists');
                Route::post('/OrderChainaContainer/GetContainerDetail/{id}', 'Admin\OrderChainaContainerController@GetContainerDetail');
                Route::get('/OrderChainaContainer/PrintOrder/{id}', 'Admin\OrderChainaContainerController@PrintOrder');
                Route::get('/OrderChainaContainer/PrintSale/{id}', 'Admin\OrderChainaContainerController@PrintSale');
                Route::get('/OrderChainaContainer/ExcelOrder/{id}', 'Admin\OrderChainaContainerController@ExcelOrder');
                Route::get('/OrderChainaContainer/ExcelSale/{id}', 'Admin\OrderChainaContainerController@ExcelSale');

            Route::get('/OrderImportToChaina', 'Admin\OrderImportToChainaController@index');
                Route::post('/OrderImportToChaina/Lists', 'Admin\OrderImportToChainaController@Lists');
                Route::get('/OrderImportToChaina/EditPrice/{id}', 'Admin\OrderImportToChainaController@EditPrice');
                Route::post('/OrderImportToChaina/EditPriceLists/{id}', 'Admin\OrderImportToChainaController@EditPriceLists');
                Route::get('/OrderImportToChaina/ShowEditPrice/{id}', 'Admin\OrderImportToChainaController@ShowEditPrice');
                Route::post('/OrderImportToChaina/EditPrice/{id}', 'Admin\OrderImportToChainaController@EditPriceUpdate');
                Route::post('/OrderImportToChaina/Prints/{id}', 'Admin\OrderImportToChainaController@Prints');

            Route::get('/ProductToThai', 'Admin\ProductToThaiController@index');
                Route::get('/ProductToThai/Lists', 'Admin\ProductToThaiController@Lists');
                Route::get('/ProductToThai/DetailLists', 'Admin\ProductToThaiController@DetailLists');
                Route::get('/ProductToThai/{id}', 'Admin\ProductToThaiController@show');
                Route::get('/ProductToThai/PrintPo/{id}', 'Admin\ProductToThaiController@PrintPo');

            Route::get('/Delivery', 'Admin\DeliveryController@index');
                Route::get('/Delivery/Lists', 'Admin\DeliveryController@Lists');
                Route::get('/Delivery/Create', 'Admin\DeliveryController@Create');
                Route::post('/Delivery', 'Admin\DeliveryController@store');
                Route::get('/Delivery/Detail/{id}', 'Admin\DeliveryController@Detail');
                Route::post('/Delivery/CheckQrCode', 'Admin\DeliveryController@CheckQrCode');
                Route::get('/Delivery/{id}', 'Admin\DeliveryController@show');
                Route::post('/Delivery/GetStatusDeliverySlips', 'Admin\DeliveryController@GetStatusDeliverySlips');
                Route::post('/Delivery/{id}', 'Admin\DeliveryController@update');
                Route::post('/Delivery/Delete/{id}', 'Admin\DeliveryController@destroy');
                Route::post('/Delivery/GetStatus/{id}', 'Admin\DeliveryController@GetStatus');
                Route::get('/Delivery/ShowDeliverySlip/{id}', 'Admin\DeliveryController@ShowDeliverySlip');

            Route::get('/DeliveryContainer', 'Admin\DeliveryContainerController@index');
                Route::get('/DeliveryContainerUser/{id}', 'Admin\DeliveryContainerController@DeliveryContainerUser');
                Route::get('/DeliveryContainerUserLists', 'Admin\DeliveryContainerController@DeliveryContainerUserLists');
                Route::get('/DeliveryContainer/Lists', 'Admin\DeliveryContainerController@Lists');
                Route::get('/DeliveryContainer/Create', 'Admin\DeliveryContainerController@Create');
                Route::post('/DeliveryContainer', 'Admin\DeliveryContainerController@store');
                Route::get('/DeliveryContainer/{id}', 'Admin\DeliveryContainerController@show');
                Route::post('/DeliveryContainer/{id}', 'Admin\DeliveryContainerController@update');
                Route::post('/DeliveryContainer/Delete/{id}', 'Admin\DeliveryContainerController@destroy');
                Route::post('/DeliveryContainer/GetContainerDetail/{id}', 'Admin\DeliveryContainerController@GetContainerDetail');
                Route::get('/DeliveryContainer/Prints/{id}', 'Admin\DeliveryContainerController@Prints');
                Route::get('/DeliveryContainer/Excel/{id}', 'Admin\DeliveryContainerController@Excel');
                Route::get('/DeliveryContainer/PrintsAll/{id}', 'Admin\DeliveryContainerController@PrintsAll');
                Route::get('/DeliveryContainer/ExcelAll/{id}', 'Admin\DeliveryContainerController@ExcelAll');
                Route::get('/DeliveryContainer/GetStatusShipping/{id}', 'Admin\DeliveryContainerController@GetStatusShipping');
                Route::get('/DeliveryContainer/GetStatusShippingUserPo/{id}', 'Admin\DeliveryContainerController@GetStatusShippingUserPo');
                Route::post('/DeliveryContainer/StatusShipping/{id}', 'Admin\DeliveryContainerController@StatusShipping');
                Route::get('/DeliveryContainer/GetUserAddress/{id}', 'Admin\DeliveryContainerController@GetUserAddress');
                Route::post('/DeliveryContainer/UserAddress/{id}', 'Admin\DeliveryContainerController@UserAddress');

                Route::get('/DeliveryContainer/DeliveryContainerUserPo/{id}', 'Admin\DeliveryContainerController@DeliveryContainerUserPo');
                Route::get('/DeliveryContainerUserPoLists', 'Admin\DeliveryContainerController@DeliveryContainerUserPoLists');
                Route::post('/DeliveryContainerUserPo/StatusShippingUserPo/{id}', 'Admin\DeliveryContainerController@StatusShippingUserPo');
                Route::get('/DeliveryContainerUserPo/PrintsUserPo/{id}', 'Admin\DeliveryContainerController@PrintsUserPo');
                Route::get('/DeliveryContainerUserPo/ExcelUserPo/{id}', 'Admin\DeliveryContainerController@ExcelUserPo');

            Route::get('/Shipping/{id}', 'Admin\ShippingController@index');
                Route::get('/Shippings/Lists', 'Admin\ShippingController@Lists');
                Route::get('/Shipping/Create/{id}', 'Admin\ShippingController@Create');
                Route::get('/Shipping/Prints/{id}', 'Admin\ShippingController@Prints');
                Route::get('/Shipping/GetUserPo/{id}', 'Admin\ShippingController@GetUserPo');
                Route::post('/Shipping', 'Admin\ShippingController@store');
                Route::post('/Shipping/Delete/{id}', 'Admin\ShippingController@destroy');
                Route::post('/Shipping/GetStatus/{id}', 'Admin\ShippingController@GetStatus');

            Route::get('/DeliveryStatus', 'Admin\DeliveryStatusController@index');
                Route::get('/DeliveryStatus/Lists', 'Admin\DeliveryStatusController@Lists');
                Route::post('/DeliveryStatus', 'Admin\DeliveryStatusController@store');
                Route::get('/DeliveryStatus/{id}', 'Admin\DeliveryStatusController@show');
                Route::post('/DeliveryStatus/{id}', 'Admin\DeliveryStatusController@update');
                Route::post('/DeliveryStatus/Delete/{id}', 'Admin\DeliveryStatusController@destroy');

            Route::get('/DeliverySlipStatus', 'Admin\DeliverySlipStatusController@index');
                Route::get('/DeliverySlipStatus/Lists', 'Admin\DeliverySlipStatusController@Lists');
                Route::post('/DeliverySlipStatus', 'Admin\DeliverySlipStatusController@store');
                Route::get('/DeliverySlipStatus/{id}', 'Admin\DeliverySlipStatusController@show');
                Route::post('/DeliverySlipStatus/{id}', 'Admin\DeliverySlipStatusController@update');
                Route::post('/DeliverySlipStatus/Delete/{id}', 'Admin\DeliverySlipStatusController@destroy');

            Route::get('/Billing', 'Admin\BillingController@index');
                Route::get('/Billing/Lists', 'Admin\BillingController@Lists');
                Route::get('/Billing/Create', 'Admin\BillingController@Create');
                Route::get('/Billing/Edit/{id}', 'Admin\BillingController@edit');
                Route::get('/Billing/Prints/{id}', 'Admin\BillingController@Prints');
                Route::get('/Billing/Excel/{id}', 'Admin\BillingController@Excel');
                Route::post('/Billing', 'Admin\BillingController@store');
                Route::get('/Billing/{id}', 'Admin\BillingController@show');
                Route::post('/Billing/{id}', 'Admin\BillingController@update');
                Route::post('/Billing/Delete/{id}', 'Admin\BillingController@destroy');
                Route::get('/Billing/GetUserPo/{id}', 'Admin\BillingController@GetUserPo');
                Route::post('/Billing/ChangeStatus/{id}', 'Admin\BillingController@ChangeStatus');
                Route::post('/Billing/ConfirmBilling/{id}', 'Admin\BillingController@ConfirmBilling');
                Route::post('/Billing/PaymentStatus/{id}', 'Admin\BillingController@PaymentStatus');

            Route::get('/ImportToChainaStatus', 'Admin\ImportToChainaStatusController@index');
                Route::get('/ImportToChainaStatus/Lists', 'Admin\ImportToChainaStatusController@Lists');
                Route::post('/ImportToChainaStatus', 'Admin\ImportToChainaStatusController@store');
                Route::get('/ImportToChainaStatus/{id}', 'Admin\ImportToChainaStatusController@show');
                Route::post('/ImportToChainaStatus/{id}', 'Admin\ImportToChainaStatusController@update');
                Route::post('/ImportToChainaStatus/Delete/{id}', 'Admin\ImportToChainaStatusController@destroy');

            Route::get('/DeliveryCharge', 'Admin\DeliveryChargeController@index');
                Route::get('/DeliveryCharge/Lists', 'Admin\DeliveryChargeController@Lists');
                Route::post('/DeliveryCharge', 'Admin\DeliveryChargeController@store');
                Route::get('/DeliveryCharge/{id}', 'Admin\DeliveryChargeController@show');
                Route::post('/DeliveryCharge/{id}', 'Admin\DeliveryChargeController@update');
                Route::post('/DeliveryCharge/Delete/{id}', 'Admin\DeliveryChargeController@destroy');

            Route::get('/News', 'Admin\NewsController@index');
                Route::get('/News/Lists', 'Admin\NewsController@Lists');
                Route::post('/News', 'Admin\NewsController@store');
                Route::get('/News/{id}', 'Admin\NewsController@show');
                Route::post('/News/{id}', 'Admin\NewsController@update');
                Route::post('/News/Delete/{id}', 'Admin\NewsController@destroy');

            Route::get('/Analysis', 'Admin\AnalysisController@index');
                Route::get('/Analysis/Lists', 'Admin\AnalysisController@Lists');

            Route::get('/Product', 'Admin\ProductController@index');
            Route::get('/Product/Lists', 'Admin\ProductController@Lists');
            Route::post('/Product', 'Admin\ProductController@store');
            Route::get('/Product/{id}', 'Admin\ProductController@show');
            Route::post('/Product/{id}', 'Admin\ProductController@update');
            Route::post('/Product/Delete/{id}', 'Admin\ProductController@destroy');


            // Route::get('/getDataCSV', 'Admin\TestController@getDataCSV'); // set tb_users
            // Route::get('/SendSMS', 'Admin\ManageSmsController@SendSMS');
      ##ROUTEFORINSTALL##
        });
    }
});
///OrakUploader
Route::any('/upload_file', 'OrakController@upload_file');

?>
