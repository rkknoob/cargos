<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('Frontend/index');
// });

Route::get('/', 'HomeController@index');
Route::get('/index', 'HomeController@index');
Route::get('/login', 'HomeController@index');
Route::get('/about', 'HomeController@about');
Route::get('/news', 'NewsController@news');
Route::get('/news_detail/{id}', 'NewsController@news_detail');
Route::get('/contact', 'HomeController@contact');
Route::get('/register', 'RegisterController@register');
Route::post('/register', 'RegisterController@AddRegister');
Route::post('/CheckLogin', 'AuthController@CheckLogin');
Route::get('/resetpassword', 'ResetPasswordController@index');
Route::post('/resetpassword', 'ResetPasswordController@store');
Route::get('/resetpassword/{token}', 'ResetPasswordController@resetpassword');
Route::post('/resetpassword/update', 'ResetPasswordController@update');
Route::post('/GetAmphure/{province}', 'RegisterController@GetAmphure');

Route::group(['middleware' => 'user'], function(){
	Route::get('/profile', 'ProfileController@profile');
	Route::get('/logout', 'AuthController@logout');
	Route::get('/list_product', 'ProductController@list_product');
	Route::get('/list_product_detail/{id}', 'ProductController@list_product_detail');
	Route::get('/history', 'ProductController@history');
	Route::get('/history_detail/{id}', 'ProductController@history_detail');
	Route::post('/CheckPassword', 'ProfileController@CheckPassword');
	Route::post('/EditProfile', 'ProfileController@EditProfile');
	Route::post('/AddAddress', 'ProfileController@AddAddress');
	Route::post('/AddressDelete/{id}', 'ProfileController@AddressDelete');
	Route::get('/ListsAddress', 'ProfileController@ListsAddress');
	Route::get('/EditAddress/{id}', 'ProfileController@EditAddress');


});

