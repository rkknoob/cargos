<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class WebMultiLanguage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $locale = $request->segment(2);
        
        if ( ! in_array($locale, config('app.locales'))) {
            $locale = 'en';
            config(['app.locales' => $locale]);
            config(['app.locale' => $locale]);
            view()->share('lang_url', '');
        }else{
            config(['app.locales' => $locale]);
            config(['app.locale' => $locale]);
            view()->share('lang_url', config('app.locales').'/');
        }
        view()->share('lang', config('app.locales'));
		\App::setLocale($locale);
        return $next($request);
    }
}
