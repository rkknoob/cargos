<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Storage;
use Hash;
use Mail;
use Illuminate\Support\Facades\Auth;

class ResetPasswordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Frontend.resetpassword');
    }

    public function store(Request $request) 
    {
        $member = \App\Models\User::where('email',$request->input('email'))->first();
        if(isset($member) && $member->email!='' && $member->email!=null){
            $bytes = openssl_random_pseudo_bytes(16 * 2);
            $new_token = substr(str_replace(array('/', '+', '='), '', base64_encode($bytes)), 0, 16);
            $new_token .= '_'.base64_encode(date('Y-m-d H:i:s'));

            $update = array(
                'reset_password_token' => $new_token
            );
            \App\Models\User::where('id',$member->id)->update($update);

            $data = array(
                'email' => $member->email,
                'name' => $member->firstname.' '.$member->lastname,
                'gen_token' => $new_token
            );

            \Mail::send('Frontend.Email.form_reset_password', $data, function($message) use ($data) {
                $message->from(env('MAIL_USERNAME'), 'New Cargo');
                $message->to($data['email'], '')->subject('คำขอรีเซ็ตรหัสผ่าน');
            });

            $return['status'] = 1;
            $return['content'] = 'สำเร็จ กรุณาตรวจสอบที่อีเมลของท่านภายใน 24 ชั่วโมง';
            $return['token'] = $new_token;
        

        }else{
            $return['status'] = 0;
            $return['content'] = 'ไม่สำเร็จ กรุณาตรวจสอบอีเมลของท่านอีกครั้ง';
        }
        $return['title'] = 'ลืมรหัสผ่าน';
        return $return;
    }

    public function resetpassword($token)
    {
        $data['User'] = \App\Models\User::where('reset_password_token', $token)->first();

        return view('Frontend.form_change_password', $data);
    }

    public function update(Request $request)
    {
        $User = \App\Models\User::where('email', $request->input('email'))->first();
        
        $data['password'] = \Hash::make($request->new_password);
        $data['reset_password_token'] = null;
        \DB::beginTransaction();
        try {
            \App\Models\User::where('id',$User['id'])->update($data);
            \DB::commit();
            $return['status'] = 1;
            $return['content'] = 'สำเร็จ';
        } catch (Exception $e) {
            \DB::rollBack();
            $return['status'] = 0;
            $return['content'] = 'ไม่สำเร็จ'.$e->getMessage();
        }
        $return['title'] = 'รีเซ็ตรหัสผ่าน';
        return $return;
    }

}
