<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Socialite;
use Auth;
use Hash;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{

    public function CheckLogin(Request $request){
    	$email = $request->input('email');
    	$password = $request->input('password');
    	if (\Auth::attempt(['email' => $email, 'password' => $password])) {
            return 1;
        }else{
        	return 0;
        }
    }

    public function logout()
    {
        \Auth::logout();
        return redirect('/');
    }

    // public function facebookAuthRedirect() {
    //     return \Socialite::driver('facebook')->redirect();
    // }

    // public function facebookSuccess()
    // {
    //     $user = \Socialite::driver('facebook')->setHttpClient(new \GuzzleHttp\Client(['verify' => false]))->user();

    //     $email = $user->getEmail();

    //     $name  = $user->getName();

    //     $password = substr($user->token,0,10);
    //     $facebook_id = $user->getId();



    //     if($email == null){ // case permission is not email public.
    //         $user = $this->checkExistUserByFacebookId($facebook_id);
    //         if($user == null){
    //             $email = $facebook_id;
    //         }
    //     }else{
    //         $user = $this->checkExistUserByEmail($email);
    //     }

    //     if($user != null){ // Auth exist account.
    //         Auth::loginUsingId($user->id);
    //     }else{ // new Account.
    //         $user = $this->registerUser($email,$name,$password,$facebook_id);
    //         Auth::loginUsingId($user);
    //     }

    //     return redirect('/');

    // }

    // private function checkExistUserByEmail($email)
    // {
    //     $user = \App\User::where('email','=',$email)->first();
    //     return $user;
    // } 

    // private function checkExistUserByFacebookId($facebook_id)
    // {
    //     $user = \App\User::where('email','=',$facebook_id)->first();

    //     return $user;
    // }

    // private function registerUser($email,$name,$password,$facebook_id){

    //     $input_all['firstname'] = $name;
    //     $input_all['email'] = $email;
    //     $input_all['facebook_id'] = $facebook_id;
    //     $input_all['password'] = bcrypt($password);
    //     $input_all['created_at'] = date('Y-m-d H:i:s');
    //     $input_all['updated_at'] = date('Y-m-d H:i:s');


    //     $data_insert = $input_all;
    //     $user_id = \App\User::insertGetId($data_insert);
    //     return $user_id;
    // }


}
