<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductController extends Controller
{
	public function list_product(Request $request)
	{
        $data['search'] = $request->has('search') ? $request->input('search') : null;

		$user_id = Auth()->user()->id;
		$data['ImportToChainas'] = \App\Models\ImportToChaina::select(
							'import_to_chaina.id as id'
                            ,'import_to_chaina.remark'
                            ,'import_to_chaina.status_id'
                            ,'import_to_chaina.user_id as user_id'
                            ,'import_to_chaina.created_at as import_to_chaina_date'
                            ,'import_to_thai.created_at as import_to_thai_date'
                            ,'import_to_chaina.po_no as po_no'
                            ,'users.customer_general_code'
                            ,'product_types.name as product_type_name'
                            ,'import_to_chaina_statuses.name as import_to_chaina_status'
                            ,'import_to_chaina_statuses.id as import_to_chaina_status_id'
                            ,'dalivery_sheets.delivery_slip_date as delivery_slip_date'
                            ,'delivery_pos.created_at as delivery_pos_date'
                            ,\DB::raw('(select sum(qty) from tb_product_import_to_chaina
                                            where tb_product_import_to_chaina.import_to_chaina_id = tb_import_to_chaina.id
                                        ) as qty_chaina')
                            ,\DB::raw('(select count(*) from tb_product_import_to_chaina
                                            join tb_lot_products on tb_lot_products.product_import_to_chaina_id = tb_product_import_to_chaina.id
                                            where tb_product_import_to_chaina.import_to_chaina_id = tb_import_to_chaina.id
                                        ) as qty_lot')
                            ,\DB::raw('(select count(*) from tb_import_to_thai
                                            join tb_product_import_to_thai on tb_product_import_to_thai.import_to_thai_id = tb_import_to_thai.id
                                            where tb_import_to_thai.import_to_chaina_id = tb_import_to_chaina.id
                                        ) as qty_thai')
                            ,\DB::raw('(select count(*) from tb_qr_code_products
                                            join tb_rel_container_products on tb_rel_container_products.qr_code_product_id = tb_qr_code_products.id
                                            where tb_qr_code_products.import_to_chaina_id = tb_import_to_chaina.id
                                        ) as qty_container')
                            ,\DB::raw('(select count(*) from tb_delivery_to_users
                                            join tb_delivery_products on tb_delivery_products.delivery_to_user_id = tb_delivery_to_users.id
                                            where tb_delivery_to_users.import_to_chaina_id = tb_import_to_chaina.id
                                        ) as qty_delivery')
							,\DB::raw('( select tb_containers.date_chaina
											from tb_qr_code_products
                                            join tb_rel_container_products on tb_rel_container_products.qr_code_product_id = tb_qr_code_products.id
                                            join tb_containers on tb_containers.id = tb_rel_container_products.container_id
                                            where tb_qr_code_products.import_to_chaina_id = tb_import_to_chaina.id
											and tb_containers.date_chaina is not null
											order by tb_containers.date_chaina asc
											limit 1
                                        ) as date_container') // เวลาตู้ที่ออกก่อน
							)
                        ->leftjoin('delivery_pos', 'delivery_pos.import_to_chaina_id','=' , 'import_to_chaina.id')
                        ->leftjoin('dalivery_sheets', 'dalivery_sheets.id','=' , 'delivery_pos.delivery_sheet_id')
                        ->leftjoin('import_to_thai', 'import_to_thai.import_to_chaina_id', '=', 'import_to_chaina.id')
                        ->join('users', 'users.id', '=', 'import_to_chaina.user_id')
                        ->join('product_types', 'product_types.id', '=', 'users.product_type_id')
                        ->join('import_to_chaina_statuses', 'import_to_chaina_statuses.id', '=', 'import_to_chaina.status_id')
                        //->join('qr_code_products', 'qr_code_products.import_to_chaina_id', '=', 'import_to_chaina.id')
                        ->Where('import_to_chaina.po_no', 'like', '%' .$data['search']. '%')
                        ->whereNotIn('import_to_chaina.status_id', [6])
						->where('import_to_chaina.user_id', $user_id)
                        ->orderBy('import_to_chaina.id', 'desc')
                        //->get();
                        ->paginate(5);

        //dd($data);
		return view('Frontend.list_product', $data);
	}

	public function list_product_detail($id)
	{
        $data['ImportToChaina'] = \App\Models\ImportToChaina::select(
                            'import_to_chaina.id as id'
                            ,'import_to_chaina.remark'
                            ,'import_to_chaina.status_id'
                            ,'import_to_chaina.user_id as user_id'
                            ,'import_to_chaina.created_at as import_to_chaina_date'
                            ,'import_to_thai.created_at as import_to_thai_date'
                            ,'import_to_chaina.po_no as po_no'
                            ,'users.customer_general_code'
                            ,'users.main_mobile'
                            ,'product_types.name as product_type_name'
                            ,'import_to_chaina_statuses.name as import_to_chaina_status'
                            ,'import_to_chaina_statuses.id as import_to_chaina_status_id'
                            ,'dalivery_sheets.delivery_slip_date as delivery_slip_date'
                            ,'dalivery_sheets.address'
                            ,'dalivery_sheets.zipcode'
                            ,'amphures.amphure_name'
                            ,'provinces.province_name'
                            ,'delivery_pos.created_at as delivery_pos_date'
                            ,'delivery_pos.updated_at as delivery_pos_success_date'
                            ,\DB::raw('(select sum(qty) from tb_product_import_to_chaina
                                            where tb_product_import_to_chaina.import_to_chaina_id = tb_import_to_chaina.id
                                        ) as qty_chaina')
                            ,\DB::raw('(select count(*) from tb_product_import_to_chaina
                                            join tb_lot_products on tb_lot_products.product_import_to_chaina_id = tb_product_import_to_chaina.id
                                            where tb_product_import_to_chaina.import_to_chaina_id = tb_import_to_chaina.id
                                        ) as qty_lot')
                            ,\DB::raw('(select count(*) from tb_import_to_thai
                                            join tb_product_import_to_thai on tb_product_import_to_thai.import_to_thai_id = tb_import_to_thai.id
                                            where tb_import_to_thai.import_to_chaina_id = tb_import_to_chaina.id
                                        ) as qty_thai')
                            ,\DB::raw('(select count(*) from tb_qr_code_products
                                            join tb_rel_container_products on tb_rel_container_products.qr_code_product_id = tb_qr_code_products.id
                                            where tb_qr_code_products.import_to_chaina_id = tb_import_to_chaina.id
                                        ) as qty_container')
                            ,\DB::raw('(select count(*) from tb_delivery_to_users
                                            join tb_delivery_products on tb_delivery_products.delivery_to_user_id = tb_delivery_to_users.id
                                            where tb_delivery_to_users.import_to_chaina_id = tb_import_to_chaina.id
                                        ) as qty_delivery')
							,\DB::raw('( 	select tb_containers.date_chaina
											from tb_qr_code_products
											join tb_rel_container_products on tb_rel_container_products.qr_code_product_id = tb_qr_code_products.id
											join tb_containers on tb_containers.id = tb_rel_container_products.container_id
											where tb_qr_code_products.import_to_chaina_id = tb_import_to_chaina.id
											and tb_containers.date_chaina is not null
											order by tb_containers.date_chaina asc
											limit 1
										) as date_container') // เวลาตู้ที่ออกก่อน
                            )
                        ->leftjoin('delivery_pos', 'delivery_pos.import_to_chaina_id','=' , 'import_to_chaina.id')
                        ->leftjoin('dalivery_sheets', 'dalivery_sheets.id','=' , 'delivery_pos.delivery_sheet_id')
                        ->leftjoin('amphures', 'amphures.id','=' , 'dalivery_sheets.amphur_id')
                        ->leftjoin('provinces', 'provinces.id','=' , 'dalivery_sheets.province_id')
                        ->leftjoin('import_to_thai', 'import_to_thai.import_to_chaina_id', '=', 'import_to_chaina.id')
                        ->join('users', 'users.id', '=', 'import_to_chaina.user_id')
                        ->join('product_types', 'product_types.id', '=', 'users.product_type_id')
                        ->join('import_to_chaina_statuses', 'import_to_chaina_statuses.id', '=', 'import_to_chaina.status_id')
                        ->leftjoin('delivery_slip_lists', 'delivery_slip_lists.import_to_chaina_id', '=', 'import_to_chaina.id')
                        ->leftjoin('delivery_slips', 'delivery_slips.id', '=', 'delivery_slip_lists.delivery_slip_id')
                        ->leftjoin('user_addresses', 'user_addresses.id', '=', 'delivery_slips.user_address_id')
                        ->where('import_to_chaina.id', $id)
                        ->first();

        $data['ProductImportToChainas'] = \App\Models\ProductImportToChaina::select(
                            'products.name as product_name'
                            ,'product_import_to_chaina.qty as qty_chaina'
                            ,'product_import_to_chaina.pcs as pcs'
                            ,'transport_types.name_th as transport_type_name'
                            ,\DB::raw('(select sum(weight_all) from tb_lot_products
                                            where tb_lot_products.product_import_to_chaina_id = tb_product_import_to_chaina.id
                                        ) as weight_all')
                            ,\DB::raw('(select sum(cubic) from tb_lot_products
                                            where tb_lot_products.product_import_to_chaina_id = tb_product_import_to_chaina.id
                                        ) as cubic')
                            ,\DB::raw('(select count(*) from tb_lot_products
                                            where tb_lot_products.product_import_to_chaina_id = tb_product_import_to_chaina.id
                                        ) as lot_qty')
                        )
                        ->join('products', 'products.id', '=', 'product_import_to_chaina.product_id')
                        ->join('transport_types', 'transport_types.id', '=', 'product_import_to_chaina.transport_type_id')
                        ->where('product_import_to_chaina.import_to_chaina_id', $id)
                        ->get();
        
		return view('Frontend.list_product_detail', $data);
	}

	public function history(Request $request)
	{
        $data['search'] = $request->has('search') ? $request->input('search') : null;
        $user_id = Auth()->user()->id;
        $data['ImportToChainas'] = \App\Models\ImportToChaina::select(
                            'import_to_chaina.id as id'
                            ,'import_to_chaina.user_id as user_id'
                            ,'import_to_chaina.created_at as import_to_chaina_date'
                            ,'import_to_thai.created_at as import_to_thai_date'
                            ,'import_to_chaina.po_no as po_no'
                            ,'users.customer_general_code'
                            ,'product_types.name as product_type_name'
                            ,'import_to_chaina_statuses.name as import_to_chaina_status'
                            ,'import_to_chaina_statuses.id as import_to_chaina_status_id'
                            ,'dalivery_sheets.delivery_slip_date as delivery_slip_date'
                            ,\DB::raw('(select sum(qty) from tb_product_import_to_chaina
                                            where tb_product_import_to_chaina.import_to_chaina_id = tb_import_to_chaina.id
                                        ) as qty_chaina')
                            ,\DB::raw('(select count(*) from tb_product_import_to_chaina
                                            join tb_lot_products on tb_lot_products.product_import_to_chaina_id = tb_product_import_to_chaina.id
                                            where tb_product_import_to_chaina.import_to_chaina_id = tb_import_to_chaina.id
                                        ) as qty_lot')
                            ,\DB::raw('(select count(*) from tb_import_to_thai
                                            join tb_product_import_to_thai on tb_product_import_to_thai.import_to_thai_id = tb_import_to_thai.id
                                            where tb_import_to_thai.import_to_chaina_id = tb_import_to_chaina.id
                                        ) as qty_thai')
                            ,\DB::raw('(select count(*) from tb_qr_code_products
                                            join tb_rel_container_products on tb_rel_container_products.qr_code_product_id = tb_qr_code_products.id
                                            where tb_qr_code_products.import_to_chaina_id = tb_import_to_chaina.id
                                        ) as qty_container')
                            ,\DB::raw('(select count(*) from tb_delivery_to_users
                                            join tb_delivery_products on tb_delivery_products.delivery_to_user_id = tb_delivery_to_users.id
                                            where tb_delivery_to_users.import_to_chaina_id = tb_import_to_chaina.id
                                        ) as qty_delivery')
							,\DB::raw('( 	select tb_containers.date_chaina
											from tb_qr_code_products
											join tb_rel_container_products on tb_rel_container_products.qr_code_product_id = tb_qr_code_products.id
											join tb_containers on tb_containers.id = tb_rel_container_products.container_id
											where tb_qr_code_products.import_to_chaina_id = tb_import_to_chaina.id
											and tb_containers.date_chaina is not null
											order by tb_containers.date_chaina asc
											limit 1
										) as date_container') // เวลาตู้ที่ออกก่อน
                            )
                        ->leftjoin('delivery_pos', 'delivery_pos.import_to_chaina_id','=' , 'import_to_chaina.id')
                        ->leftjoin('dalivery_sheets', 'dalivery_sheets.id','=' , 'delivery_pos.delivery_sheet_id')
                        ->leftjoin('import_to_thai', 'import_to_thai.import_to_chaina_id', '=', 'import_to_chaina.id')
                        ->join('users', 'users.id', '=', 'import_to_chaina.user_id')
                        ->join('product_types', 'product_types.id', '=', 'users.product_type_id')
                        ->join('import_to_chaina_statuses', 'import_to_chaina_statuses.id', '=', 'import_to_chaina.status_id')
                        ->Where('import_to_chaina.po_no', 'like', '%' .$data['search']. '%')
                        ->where('import_to_chaina.status_id', 6)
                        ->where('import_to_chaina.user_id', $user_id)
                        ->paginate(5);

		return view('Frontend.history', $data);
	}

	public function history_detail($id)
	{
        $data['ImportToChaina'] = \App\Models\ImportToChaina::select(
                            'import_to_chaina.id as  id'
                            ,'import_to_chaina.status_id'
                            ,'import_to_chaina.user_id as user_id'
                            ,'import_to_chaina.created_at as import_to_chaina_date'
                            ,'import_to_thai.created_at as import_to_thai_date'
                            ,'import_to_chaina.po_no as po_no'
                            ,'users.customer_general_code'
                            ,'users.main_mobile'
                            ,'product_types.name as product_type_name'
                            ,'import_to_chaina_statuses.name as import_to_chaina_status'
                            ,'import_to_chaina_statuses.id as import_to_chaina_status_id'
                            ,'dalivery_sheets.delivery_slip_date as delivery_slip_date'
                            ,'dalivery_sheets.address'
                            ,'dalivery_sheets.zipcode'
                            ,'amphures.amphure_name'
                            ,'provinces.province_name'
                            ,'delivery_pos.created_at as delivery_pos_date'
                            ,'delivery_pos.updated_at as delivery_pos_success_date'
                            ,\DB::raw('(select sum(qty) from tb_product_import_to_chaina
                                            where tb_product_import_to_chaina.import_to_chaina_id = tb_import_to_chaina.id
                                        ) as qty_chaina')
                            ,\DB::raw('(select count(*) from tb_product_import_to_chaina
                                            join tb_lot_products on tb_lot_products.product_import_to_chaina_id = tb_product_import_to_chaina.id
                                            where tb_product_import_to_chaina.import_to_chaina_id = tb_import_to_chaina.id
                                        ) as qty_lot')
                            // ,\DB::raw('(select sum(weight_all) from tb_product_import_to_chaina
                            //                 where tb_product_import_to_chaina.import_to_chaina_id = tb_import_to_chaina.id
                            //             ) as weight_all')
                            // ,\DB::raw('(select sum(cubic) from tb_product_import_to_chaina
                            //                 where tb_product_import_to_chaina.import_to_chaina_id = tb_import_to_chaina.id
                            //             ) as cubic')
                            ,\DB::raw('(select count(*) from tb_import_to_thai
                                            join tb_product_import_to_thai on tb_product_import_to_thai.import_to_thai_id = tb_import_to_thai.id
                                            where tb_import_to_thai.import_to_chaina_id = tb_import_to_chaina.id
                                        ) as qty_thai')
                            ,\DB::raw('(select count(*) from tb_qr_code_products
                                            join tb_rel_container_products on tb_rel_container_products.qr_code_product_id = tb_qr_code_products.id
                                            where tb_qr_code_products.import_to_chaina_id = tb_import_to_chaina.id
                                        ) as qty_container')
                            ,\DB::raw('(select count(*) from tb_delivery_to_users
                                            join tb_delivery_products on tb_delivery_products.delivery_to_user_id = tb_delivery_to_users.id
                                            where tb_delivery_to_users.import_to_chaina_id = tb_import_to_chaina.id
                                        ) as qty_delivery')
							,\DB::raw('( 	select tb_containers.date_chaina
											from tb_qr_code_products
											join tb_rel_container_products on tb_rel_container_products.qr_code_product_id = tb_qr_code_products.id
											join tb_containers on tb_containers.id = tb_rel_container_products.container_id
											where tb_qr_code_products.import_to_chaina_id = tb_import_to_chaina.id
											and tb_containers.date_chaina is not null
											order by tb_containers.date_chaina asc
											limit 1
										) as date_container') // เวลาตู้ที่ออกก่อน
                            )
                        ->leftjoin('delivery_pos', 'delivery_pos.import_to_chaina_id','=' , 'import_to_chaina.id')
                        ->leftjoin('dalivery_sheets', 'dalivery_sheets.id','=' , 'delivery_pos.delivery_sheet_id')
                        ->leftjoin('amphures', 'amphures.id','=' , 'dalivery_sheets.amphur_id')
                        ->leftjoin('provinces', 'provinces.id','=' , 'dalivery_sheets.province_id')
                        ->leftjoin('import_to_thai', 'import_to_thai.import_to_chaina_id', '=', 'import_to_chaina.id')
                        ->join('users', 'users.id', '=', 'import_to_chaina.user_id')
                        ->join('product_types', 'product_types.id', '=', 'users.product_type_id')
                        ->join('import_to_chaina_statuses', 'import_to_chaina_statuses.id', '=', 'import_to_chaina.status_id')
                        ->leftjoin('delivery_slip_lists', 'delivery_slip_lists.import_to_chaina_id', '=', 'import_to_chaina.id')
                        ->leftjoin('delivery_slips', 'delivery_slips.id', '=', 'delivery_slip_lists.delivery_slip_id')
                        ->leftjoin('user_addresses', 'user_addresses.id', '=', 'delivery_slips.user_address_id')
                        ->where('import_to_chaina.id', $id)
                        ->first();

        $data['ProductImportToChainas'] = \App\Models\ProductImportToChaina::select(
                            'products.name as product_name'
                            ,'product_import_to_chaina.qty as qty_chaina'
                            ,'product_import_to_chaina.pcs as pcs'
                            ,'transport_types.name_th as transport_type_name'
                            ,\DB::raw('(select sum(weight_all) from tb_lot_products
                                            where tb_lot_products.product_import_to_chaina_id = tb_product_import_to_chaina.id
                                        ) as weight_all')
                            ,\DB::raw('(select sum(cubic) from tb_lot_products
                                            where tb_lot_products.product_import_to_chaina_id = tb_product_import_to_chaina.id
                                        ) as cubic')
                            ,\DB::raw('(select count(*) from tb_lot_products
                                            where tb_lot_products.product_import_to_chaina_id = tb_product_import_to_chaina.id
                                        ) as lot_qty')
                        )
                        ->join('products', 'products.id', '=', 'product_import_to_chaina.product_id')
                        ->join('transport_types', 'transport_types.id', '=', 'product_import_to_chaina.transport_type_id')
                        ->where('product_import_to_chaina.import_to_chaina_id', $id)
                        ->get();


		return view('Frontend.history_detail', $data);
	}

}
