<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
	public function index()
	{
		$data['News'] = \App\Models\News::where('active', 'T')->take(3)->get();

		return view('Frontend.index', $data);
	}

	public function about()
	{
		return view('Frontend.about');
	}

	public function contact()
	{
		return view('Frontend.contact');
	}

}