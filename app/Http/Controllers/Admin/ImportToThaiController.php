<?php

namespace App\Http\Controllers\Admin;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \Mpdf\Mpdf;
use Storage;

class ImportToThaiController extends Controller
{

    public function index()
    {
        $data['main_menu'] = 'ImportToThai';
        $data['sub_menu'] = 'ImportToThai';
        $data['title_page'] = trans('lang.import_product_thai');
        $data['menus'] = \App\Models\AdminMenu::ActiveMenu()->get();
        $data['Containers'] = \App\Models\Container::get();
        return view('Admin.import_to_thai',$data);
    }

    public function create()
    {
        $data['main_menu'] = 'ImportToThai';
        $data['sub_menu'] = 'ImportToThai';
        $data['title_page'] = trans('lang.import_product_thai');
        $data['menus'] = \App\Models\AdminMenu::ActiveMenu()->get();
        $data['Containers'] = \App\Models\Container::where('status_id', 1)->get();

        return view('Admin.import_to_thai_create', $data);
    }

    public function MobileCreate()
    {
        $data['main_menu'] = 'ImportToThai';
        $data['sub_menu'] = 'ImportToThai';
        $data['title_page'] = 'รับสินค้าเข้าโกดัง (ไทย)';
        $data['menus'] = \App\Models\AdminMenu::ActiveMenu()->get();
        $data['Containers'] = \App\Models\Container::where('status_id', 1)->get();

        return view('Admin.import_to_thai_mobile_create', $data);
    }

    public function store(Request $request)
    {
        $input_all = $request->all();
        $input_all1['container_id'] = $input_all['container_id'];

        $input_all2['created_at'] = date('Y-m-d H:i:s');
        $input_all2['updated_at'] = date('Y-m-d H:i:s');
        $input_all2['date_import'] = date('Y-m-d H:i:s');

        $input_all3['created_at'] = date('Y-m-d H:i:s');
        $input_all3['updated_at'] = date('Y-m-d H:i:s');

        $validator = Validator::make($request->all(), [
            'container_id' => 'required',
        ]);

        if (!$validator->fails()) {
            \DB::beginTransaction();
            try {
                // $data_insert1 = $input_all1;
                // \App\Models\Container::where('id', $data_insert1['container_id'])->update(['status_id' => 2]);

                // if(isset($input_all['import_to_chaina_id'])){
                //     foreach($input_all['import_to_chaina_id'] as $import_to_chaina_id => $import_to_chainas){
                //         $input_all2['container_id']         = $data_insert1['container_id'];
                //         $input_all2['import_to_chaina_id']  = $import_to_chaina_id;
                //
                //         $data_insert2 = $input_all2;
                //         $import_to_thai_id = \App\Models\ImportToThai::insertGetId($data_insert2);
                //         \App\Models\ImportToChaina::where('id', $import_to_chaina_id)->update(['status_id' => 4]);
                //
                //         if(isset($import_to_chainas)){
                //             foreach($import_to_chainas['qr_code_product_id'] as $qr_code_product_id){
                //                 $input_all3['import_to_thai_id']  = $import_to_thai_id;
                //                 $input_all3['qr_code_product_id']   = $qr_code_product_id;
                //
                //                 $data_insert3 = $input_all3;
                //                 \App\Models\ProductImportToThai::insert($data_insert3);
                //             }
                //         }
                //     }
                // }

                \DB::commit();
                $return['status'] = 1;
                $return['content'] = 'สำเร็จ';
            } catch (Exception $e) {
                \DB::rollBack();
                $return['status'] = 0;
                $return['content'] = trans('lang.unsuccessful').$e->getMessage();
            }
        }else{
            $return['status'] = 0;
        }
        $return['title'] = trans('lang.create_data');
        return json_encode($return);
    }

    public function show($id)
    {
        $result = \App\Models\ImportToThai::with('ProductImportToThai.QrCodeProduct.ProductImportToChaina.Product.ProductType'
                                                ,'ProductImportToThai.QrCodeProduct.ProductImportToChaina.TransportType'
                                                ,'ImportToChaina.User'
                                                ,'Container.ContainerStatus')->where('id', $id)->first();

        return json_encode($result);
    }

    public function edit($container_id)
    {
        $data['main_menu'] = 'ImportToThai';
        $data['sub_menu'] = 'ImportToThai';
        $data['title_page'] = trans('lang.import_product_thai');
        $data['menus'] = \App\Models\AdminMenu::ActiveMenu()->get();
        $data['Containers'] = \App\Models\Container::get();
        $data['Container'] = \App\Models\Container::with('ImportToThai.ImportToChaina')->where('id', $container_id)->first();
        $data['QrCodeProducts'] = \App\Models\QrCodeProduct::select(
                'qr_code_products.*'
                , 'import_to_chaina.po_no'
                , 'import_to_thai.container_id'
                , 'import_to_thai.date_import'
                , 'users.customer_general_code'
                , 'lot_products.id as lot_product_id'
                , 'product_types.name as product_type_name'
                , 'products.name as product_name'
                // , 'import_to_thai.container_id as testtestsestsetsete'
                // , \DB::raw('(`tb_lot_products`.`product_sort_end` - `tb_lot_products`.`product_sort_start`) + 1 as lot_product_qty')
                ,\DB::raw('
                    (select count(*) from tb_qr_code_products
                        join tb_rel_container_products on tb_rel_container_products.qr_code_product_id = tb_qr_code_products.id
                        join `tb_lot_products` as tlp ON `tlp`.`product_import_to_chaina_id` = `tb_qr_code_products`.`product_import_to_chaina_id`
                        where `tb_qr_code_products`.`sort_id` BETWEEN `tlp`.`product_sort_start` and `tlp`.`product_sort_end`
                        and tb_qr_code_products.import_to_chaina_id = tb_import_to_chaina.id
                        and tb_lot_products.id = tlp.id
                    ) as lot_product_qty
                ')
                // , \DB::raw('
                //     (   select count(*)
                //         from tb_rel_container_products rcp
                //         where tb_import_to_thai.container_id = rcp.container_id
                //     ) as qr_code_container_qty
                // ')
                ,\DB::raw('
                    (select count(*) from tb_qr_code_products
                        join tb_rel_container_products on tb_rel_container_products.qr_code_product_id = tb_qr_code_products.id
                        where tb_qr_code_products.import_to_chaina_id = tb_import_to_chaina.id
                    ) as qr_code_container_qty
                ')
                , \DB::raw('
                    (   select sum(qty)
                        from tb_product_import_to_chaina
                        where tb_product_import_to_chaina.import_to_chaina_id = tb_import_to_chaina.id
                    ) as po_qty
                ')
                , \DB::raw('
                    (   select sum(weight_all)
                        from tb_product_import_to_chaina
                        where tb_product_import_to_chaina.import_to_chaina_id = tb_import_to_chaina.id
                    ) as weight_all
                ')
                , \DB::raw('
                    (   select sum(cubic)
                        from tb_product_import_to_chaina
                        where tb_product_import_to_chaina.import_to_chaina_id = tb_import_to_chaina.id
                    ) as cubic_all
                ')
            )
            ->join('product_import_to_thai', 'product_import_to_thai.qr_code_product_id', '=', 'qr_code_products.id')
            ->join('import_to_thai', 'import_to_thai.id', '=', 'product_import_to_thai.import_to_thai_id')
            ->join('import_to_chaina', 'import_to_chaina.id', '=', 'qr_code_products.import_to_chaina_id')
            ->join('users', 'users.id', '=', 'import_to_chaina.user_id')
            ->join('product_import_to_chaina', 'product_import_to_chaina.id', '=', 'qr_code_products.product_import_to_chaina_id')
            ->join('products', 'products.id', '=', 'product_import_to_chaina.product_id')
            ->join('product_types', 'product_types.id', '=', 'product_import_to_chaina.product_type_id')
            // join lot product
            ->join('lot_products', 'lot_products.product_import_to_chaina_id', '=', 'qr_code_products.product_import_to_chaina_id')
            ->whereRaw('`tb_qr_code_products`.`sort_id` BETWEEN `tb_lot_products`.`product_sort_start` AND `tb_lot_products`.`product_sort_end`')
            //end join lot product
            ->where('import_to_thai.container_id', $container_id)
            ->get();

        $data['remark'] = \App\Models\ImportToThai::select('remark')->where('container_id', $container_id)->first();
        return view('Admin.import_to_thai_edit', $data);
    }

    public function MobileEdit($container_id)
    {
        $data['main_menu'] = 'ImportToThai';
        $data['sub_menu'] = 'ImportToThai';
        $data['title_page'] = trans('lang.import_product_thai');
        $data['menus'] = \App\Models\AdminMenu::ActiveMenu()->get();
        $data['Containers'] = \App\Models\Container::get();
        $data['Container'] = \App\Models\Container::with('ImportToThai.ImportToChaina')->where('id', $container_id)->first();
        $data['QrCodeProducts'] = \App\Models\QrCodeProduct::select(
                'qr_code_products.*'
                , 'import_to_chaina.po_no'
                , 'import_to_thai.container_id'
                , 'import_to_thai.date_import'
                , 'users.customer_general_code'
                , 'lot_products.id as lot_product_id'
                , 'product_types.name as product_type_name'
                , 'products.name as product_name'
                // , 'import_to_thai.container_id as testtestsestsetsete'
                // , \DB::raw('(`tb_lot_products`.`product_sort_end` - `tb_lot_products`.`product_sort_start`) + 1 as lot_product_qty')
                ,\DB::raw('
                    (select count(*) from tb_qr_code_products
                        join tb_rel_container_products on tb_rel_container_products.qr_code_product_id = tb_qr_code_products.id
                        join `tb_lot_products` as tlp ON `tlp`.`product_import_to_chaina_id` = `tb_qr_code_products`.`product_import_to_chaina_id`
                        where `tb_qr_code_products`.`sort_id` BETWEEN `tlp`.`product_sort_start` and `tlp`.`product_sort_end`
                        and tb_qr_code_products.import_to_chaina_id = tb_import_to_chaina.id
                        and tb_lot_products.id = tlp.id
                    ) as lot_product_qty
                ')
                // , \DB::raw('
                //     (   select count(*)
                //         from tb_rel_container_products rcp
                //         where tb_import_to_thai.container_id = rcp.container_id
                //     ) as qr_code_container_qty
                // ')
                ,\DB::raw('
                    (select count(*) from tb_qr_code_products
                        join tb_rel_container_products on tb_rel_container_products.qr_code_product_id = tb_qr_code_products.id
                        where tb_qr_code_products.import_to_chaina_id = tb_import_to_chaina.id
                    ) as qr_code_container_qty
                ')
                , \DB::raw('
                    (   select sum(qty)
                        from tb_product_import_to_chaina
                        where tb_product_import_to_chaina.import_to_chaina_id = tb_import_to_chaina.id
                    ) as po_qty
                ')
                , \DB::raw('
                    (   select sum(weight_all)
                        from tb_product_import_to_chaina
                        where tb_product_import_to_chaina.import_to_chaina_id = tb_import_to_chaina.id
                    ) as weight_all
                ')
                , \DB::raw('
                    (   select sum(cubic)
                        from tb_product_import_to_chaina
                        where tb_product_import_to_chaina.import_to_chaina_id = tb_import_to_chaina.id
                    ) as cubic_all
                ')
            )
            ->join('product_import_to_thai', 'product_import_to_thai.qr_code_product_id', '=', 'qr_code_products.id')
            ->join('import_to_thai', 'import_to_thai.id', '=', 'product_import_to_thai.import_to_thai_id')
            ->join('import_to_chaina', 'import_to_chaina.id', '=', 'qr_code_products.import_to_chaina_id')
            ->join('users', 'users.id', '=', 'import_to_chaina.user_id')
            ->join('product_import_to_chaina', 'product_import_to_chaina.id', '=', 'qr_code_products.product_import_to_chaina_id')
            ->join('products', 'products.id', '=', 'product_import_to_chaina.product_id')
            ->join('product_types', 'product_types.id', '=', 'product_import_to_chaina.product_type_id')
            // join lot product
            ->join('lot_products', 'lot_products.product_import_to_chaina_id', '=', 'qr_code_products.product_import_to_chaina_id')
            ->whereRaw('`tb_qr_code_products`.`sort_id` BETWEEN `tb_lot_products`.`product_sort_start` AND `tb_lot_products`.`product_sort_end`')
            //end join lot product
            ->where('import_to_thai.container_id', $container_id)
            ->get();

        $data['remark'] = \App\Models\ImportToThai::select('remark')->where('container_id', $container_id)->first();
        return view('Admin.import_to_thai_mobile_edit', $data);
    }

    public function update(Request $request)
    {
        $input_all = $request->all();
        $input_all['po'] = json_decode($input_all['po']);
        $input_all['updated_at'] = date('Y-m-d H:i:s');
        $validator = Validator::make($request->all(), [
            'container_id' => 'required',
        ]);
        if (!$validator->fails()) {
            \DB::beginTransaction();
            try {
                $data_insert = [
                    'remark' => $input_all['remark']
                ];
                foreach ($input_all['po'] as $key => $value) {
                    \App\Models\ImportToThai::where('import_to_chaina_id', $value->po_id)->where('container_id', $input_all['container_id'])->update($data_insert);
                }
                \DB::commit();
                $return['status'] = 1;
                $return['content'] = 'สำเร็จ';
            } catch (Exception $e) {
                \DB::rollBack();
                $return['status'] = 0;
                $return['content'] = trans('lang.unsuccessful').$e->getMessage();
            }
        }else{
            $return['status'] = 0;
        }
        $return['title'] = trans('lang.create_data');
        return json_encode($return);
    }

    public function destroy($id)
    {
        \DB::beginTransaction();
        try {
            \App\Models\ImportToThai::where('id',$id)->delete();
            \DB::commit();
            $return['status'] = 1;
            $return['content'] = 'สำเร็จ';
        } catch (Exception $e) {
            \DB::rollBack();
            $return['status'] = 0;
            $return['content'] = trans('lang.unsuccessful').$e->getMessage();
        }
        $return['title'] = 'ลบข้อมูล';
        return $return;
    }

    public function Lists()
    {
        $result = \App\Models\Container::select(
                'containers.*'
                ,'transport_types.name_th as transport_type_name_th'
                ,'transport_types.name_en as transport_type_name_en'
                ,'transport_types.name_ch as transport_type_name_ch'
                ,'container_types.name_th as container_type_name_th'
                ,'container_types.name_en as container_type_name_en'
                ,'container_types.name_ch as container_type_name_ch'
                ,'container_sizes.name as container_size_name'
                ,\DB::raw('
                    (select sum(`tb_product_import_to_chaina`.`qty`)
                    from `tb_import_to_thai`
                    join `tb_import_to_chaina` on  `tb_import_to_chaina`.`id` = `tb_import_to_thai`.`import_to_chaina_id`
                    join `tb_product_import_to_chaina` on  `tb_product_import_to_chaina`.`import_to_chaina_id` = `tb_import_to_chaina`.`id`
                    where `tb_import_to_thai`.`container_id` = `tb_containers`.`id`)
                    as qty_po
                ')
                ,\DB::raw('
                    (select count(`tb_product_import_to_thai`.`id`)
                    from `tb_import_to_thai`
                    join `tb_product_import_to_thai` on  `tb_product_import_to_thai`.`import_to_thai_id` = `tb_import_to_thai`.`id`
                    where `tb_import_to_thai`.`container_id` = `tb_containers`.`id`)
                    as qty_product_real
                ')
            )
            ->withCount('RelContainerProduct')
            ->Join('transport_types','transport_types.id','=','containers.transport_type_id')
            ->Join('container_types','container_types.id','=','containers.container_type_id')
            ->Join('container_sizes','container_sizes.id','=','containers.container_size_id')
            // ->Join('import_to_thai','import_to_thai.container_id','=','containers.id')
            // ->leftJoin('product_import_to_thai','product_import_to_thai.import_to_thai_id','=','import_to_thai.id')
            // ->leftJoin('product_import_to_thai','product_import_to_thai.qr_code_product_id','=','qr_code_products.id')
            //->leftJoin('qr_code_products','qr_code_products.product_import_to_chaina_id','=','product_import_to_chaina.id')
            ->where('status_id', 2)
            ;
            // ->get();
            // dd($result);
        //$result = \App\Models\ImportToThai::with('Container', 'ImportToChaina.User', 'ImportToChaina.ProductImportToChaina')->select('*','import_to_thai.id as import_id');

        return \Datatables::of($result)

        ->editColumn('updated_at', function($rec){
            return $rec->updated_at;
        })
        ->editColumn('qty_po', function($rec){
            $qty_po = number_format($rec->qty_po);
            return $qty_po;
        })
        ->editColumn('rel_container_product_count', function($rec){
            $rel_container_product_count = number_format($rec->rel_container_product_count);
            return $rel_container_product_count;
        })
        ->editColumn('qty_product_real', function($rec){
            $qty_product_real = number_format($rec->qty_product_real);
            return $qty_product_real;
        })
        ->addColumn('amount_missing', function($rec){
            $amount_missing = number_format($rec->rel_container_product_count - $rec->qty_product_real);
            return $amount_missing;
        })
        ->editColumn('size', function($rec){
            $weight = number_format(pow($rec->cubic, 1/3), 3) .'x'. number_format(pow($rec->cubic, 1/3), 3) .'x'. number_format(pow($rec->cubic, 1/3), 3);
            return $weight;
        })
        ->editColumn('weight', function($rec){
            $weight = number_format($rec->weight, 2);
            return $weight;
        })
        ->editColumn('cubic', function($rec){
            $cubic =  number_format($rec->cubic, 3);
            return $cubic;
        })
        ->editColumn('status', function($rec){
            if($rec->status_id == 1){
                return '<label class="label label-danger">'.trans('lang.box_to_thailand').'</span>';
            }elseif($rec->status_id == 2){
                return '<label class="label label-success">'.trans('lang.goods_imported_to_thailand').'</span>';
            }
        })
        ->addColumn('action',function($rec){
            $lang = \Config::get('app.locale');
            $str='
                <a href="'.url("/admin/".$lang."/ImportToThai/Detail/".$rec->id).'" data-loading-text="<i class=\'fa fa-refresh fa-spin\'></i>" class="btn btn-xs btn-warning btn-condensed btn-detail btn-tooltip" data-rel="tooltip" data-id="'.$rec->id.'" title="'.trans('lang.view_detail').'">
                    <i class="ace-icon fa fa-search bigger-120"></i>
                </a>
                <a href="'.url("/admin/".$lang."/ImportToThai/Edit/".$rec->id).'" data-loading-text="<i class=\'fa fa-refresh fa-spin\'></i>" class="btn btn-xs btn-warning btn-condensed btn-detail btn-tooltip" data-rel="tooltip" data-id="'.$rec->id.'" title="'.trans('lang.ddd_product').'">
                    <i class="ace-icon fa fa-pencil bigger-120"></i>
                </a>
            ';
            return $str;
        })
        ->rawColumns(['status', 'action'])
        ->make(true);
    }

    public function CheckQrCode(Request $request)
    {
        $input_all      = $request->all();

        $container_id   = $input_all['container_id'];
        $qr_code        = $input_all['qr_code'];

        $QrCodeProduct = \App\Models\QrCodeProduct::with('ImportToChaina.User', 'ProductImportToChaina.Product.ProductType')->where('qr_code', $qr_code)->first();
        if(isset($QrCodeProduct)){
            $RelContainerProducts = \App\Models\RelContainerProduct::where('rel_container_products.container_id', $container_id)->where('qr_code_product_id', $QrCodeProduct->id)->first();
            $ProductImportToThai = \App\Models\ProductImportToThai::where('qr_code_product_id', $QrCodeProduct->id)->first();
        }

        if(!empty($QrCodeProduct) && !empty($RelContainerProducts)){
            if(empty($ProductImportToThai)){
                $return['ImportToChaina']   = \App\Models\ImportToChaina::where('id', $QrCodeProduct->import_to_chaina_id)->first();
                $return['LotProduct']       = \App\Models\LotProduct::where('product_import_to_chaina_id', $QrCodeProduct->product_import_to_chaina_id)
                                                ->where('product_sort_start', '<=', $QrCodeProduct->sort_id)
                                                ->where('product_sort_end', '>=', $QrCodeProduct->sort_id)
                                                ->first();
                $return['QrCodeProduct']    = $QrCodeProduct;
                $ProductImportToChainas     = \App\Models\ProductImportToChaina::where('import_to_chaina_id', $return['ImportToChaina']->id)->get();
                $return['qty_po']           = $ProductImportToChainas->sum('qty');
                $return['weight_all']       = $ProductImportToChainas->sum('weight_all');
                $return['cubic']            = $ProductImportToChainas->sum('cubic');
                $return['qty_container']    = $RelContainerProducts = \App\Models\QrCodeProduct::where('import_to_chaina_id', $return['ImportToChaina']->id)->with('RelContainerProduct')->count();
                $return['qty_lot_product']  = ($return['LotProduct']->product_sort_end - $return['LotProduct']->product_sort_start) + 1;
                // insert product import to thai and import to thai
                $check_import_to_thai = \App\Models\ImportToThai::where('container_id', $container_id)
                    ->where('import_to_chaina_id', $QrCodeProduct->import_to_chaina_id)
                    ->first();
                if(!empty($check_import_to_thai)){
                    $data_insert_product = [
                        'import_to_thai_id' => $check_import_to_thai->id,
                        'qr_code_product_id' => $QrCodeProduct->id,
                        'created_at' => date('Y-m-d H:i:s'),
                    ];
                    $id_product_to_thai = \App\Models\ProductImportToThai::insertGetId($data_insert_product);
                }
                else{
                    // insert import to thai
                    $data_insert_po_thai = [
                        'container_id' => $container_id,
                        'import_to_chaina_id' => $QrCodeProduct->import_to_chaina_id,
                        'date_import' => date('Y-m-d H:i:s'),
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                    ];
                    $id_po_thai = \App\Models\ImportToThai::insertGetId($data_insert_po_thai);
                    \App\Models\Container::where('id', $container_id)->update(['status_id' => 2]);
                    \App\Models\ImportToChaina::where('id', $QrCodeProduct->import_to_chaina_id)->update(['status_id' => 4]);
                    // insert product import to chaina
                    $data_insert_product = [
                        'import_to_thai_id' => $id_po_thai,
                        'qr_code_product_id' => $QrCodeProduct->id,
                        'created_at' => date('Y-m-d H:i:s'),
                    ];
                    $id_product_to_thai = \App\Models\ProductImportToThai::insertGetId($data_insert_product);
                }
                // insert product import to thai and import to thai
                $return['status'] = 1;
            }else{
                $return['status'] = 0;
                $return['content'] = 'Barcode สินค้าซ้ำกรุณาลองใหม่';
                $return['title'] = trans('lang.barcode');
            }

        }else{
            $return['status'] = 0;
            $return['content'] = 'Barcode สินค้าไม่ถูกต้องกรุณาลองใหม่';
            $return['title'] = trans('lang.barcode');
        }
        return $return;
    }

    public function CheckQrCodeEdit(Request $request)
    {
        $input_all      = $request->all();
        $container_id   = $input_all['container_id'];
        $qr_code        = $input_all['qr_code'];
        $QrCodeProduct = \App\Models\QrCodeProduct::with('ImportToChaina.User', 'ProductImportToChaina.Product', 'ProductImportToChaina.ProductType')->where('qr_code', $qr_code)->first();

        if(isset($QrCodeProduct)){
            $RelContainerProducts = \App\Models\RelContainerProduct::where('rel_container_products.container_id', $container_id)->where('qr_code_product_id', $QrCodeProduct->id)->first();
            $ProductImportToThai = \App\Models\ProductImportToThai::where('qr_code_product_id', $QrCodeProduct->id)->first();
        }

        if(!empty($QrCodeProduct) && !empty($RelContainerProducts)){
            if(empty($ProductImportToThai)){
                $return['ImportToChaina']   = \App\Models\ImportToChaina::where('id', $QrCodeProduct->import_to_chaina_id)->first();
                $return['LotProduct']       = \App\Models\LotProduct::where('product_import_to_chaina_id', $QrCodeProduct->product_import_to_chaina_id)
                                                ->where('product_sort_start', '<=', $QrCodeProduct->sort_id)
                                                ->where('product_sort_end', '>=', $QrCodeProduct->sort_id)
                                                ->first();
                $return['QrCodeProduct']    = $QrCodeProduct;
                $ProductImportToChainas     = \App\Models\ProductImportToChaina::select(
                        \DB::raw('sum(qty) as qty')
                        , \DB::raw('sum(weight_all) as weight_all')
                        , \DB::raw('sum(cubic) as cubic')
                    )
                    ->where('import_to_chaina_id', $return['ImportToChaina']->id)
                    ->first();
                $return['qty_po']           = $ProductImportToChainas->qty;
                $return['weight_all']       = $ProductImportToChainas->weight_all;
                $return['cubic']            = $ProductImportToChainas->cubic;
                $return['qty_container']    = $RelContainerProducts = \App\Models\QrCodeProduct::where('import_to_chaina_id', $return['ImportToChaina']->id)->join('rel_container_products', 'rel_container_products.qr_code_product_id', '=', 'qr_code_products.id')->count();
                $return['qty_lot_product']  = ($return['LotProduct']->product_sort_end - $return['LotProduct']->product_sort_start) + 1;
                // insert product import to thai and import to thai
                $check_import_to_thai = \App\Models\ImportToThai::where('container_id', $container_id)
                    ->where('import_to_chaina_id', $QrCodeProduct->import_to_chaina_id)
                    ->first();
                if(!empty($check_import_to_thai)){
                    $data_insert_product = [
                        'import_to_thai_id' => $check_import_to_thai->id,
                        'qr_code_product_id' => $QrCodeProduct->id,
                        'created_at' => date('Y-m-d H:i:s'),
                    ];
                    $id_product_to_thai = \App\Models\ProductImportToThai::insertGetId($data_insert_product);
                }
                else{
                    // insert import to thai
                    $data_insert_po_thai = [
                        'container_id' => $container_id,
                        'import_to_chaina_id' => $QrCodeProduct->import_to_chaina_id,
                        'date_import' => date('Y-m-d H:i:s'),
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                    ];
                    $id_po_thai = \App\Models\ImportToThai::insertGetId($data_insert_po_thai);
                    \App\Models\Container::where('id', $container_id)->update(['status_id' => 2]);
                    \App\Models\ImportToChaina::where('id', $QrCodeProduct->import_to_chaina_id)->update(['status_id' => 4]);
                    // insert product import to chaina
                    $data_insert_product = [
                        'import_to_thai_id' => $id_po_thai,
                        'qr_code_product_id' => $QrCodeProduct->id,
                        'created_at' => date('Y-m-d H:i:s'),
                    ];
                    $id_product_to_thai = \App\Models\ProductImportToThai::insertGetId($data_insert_product);
                }
                // insert product import to thai and import to thai
                $return['date_import'] = $check_import_to_thai ? $check_import_to_thai->date_import : date('Y-m-d H:i:s') ;
                $return['status'] = 1;
            }else{
                $return['status'] = 0;
                $return['content'] = 'Barcode สินค้าซ้ำกรุณาลองใหม่';
                $return['title'] = trans('lang.barcode');
            }

        }else{
            $return['status'] = 0;
            $return['content'] = 'Barcode สินค้าไม่ถูกต้องกรุณาลองใหม่';
            $return['title'] = trans('lang.barcode');
        }
        return $return;
    }

    public function PrintPo($id)
    {
        $data['ImportToChaina'] = \App\Models\ImportToChaina::where('id', $id)->first();

        $mpdf = new Mpdf(['autoLangToFont' => true]);
        $data2 = view('Admin.import_to_thai_print', $data);
        $mpdf->WriteHTML($data2);
        $mpdf->Output();
    }

    public function Detail($id)
    {
        $data['main_menu'] = 'ImportToThai';
        $data['sub_menu'] = 'ImportToThai';
        $data['title_page'] = trans('lang.import_product_thai');
        $data['menus'] = \App\Models\AdminMenu::ActiveMenu()->get();
        $data['Containers'] = \App\Models\Container::get();
        $data['Container'] = \App\Models\Container::with('ImportToThai.ImportToChaina')->where('id', $id)->first();
        $data['ImportToChainas'] = \App\Models\ImportToChaina::select(
                                            'import_to_chaina.id as id'
                                            ,'import_to_chaina.user_id as user_id'
                                            ,'import_to_chaina.created_at as import_to_chaina_date'
                                            ,'import_to_thai.created_at as import_to_thai_date'
                                            ,'import_to_chaina.po_no as po_no'
                                            ,\DB::raw('(select sum(qty) from tb_product_import_to_chaina
                                                            where tb_product_import_to_chaina.import_to_chaina_id = tb_import_to_chaina.id
                                                        ) as qty_chaina')
                                            ,\DB::raw('(select sum(weight_all) from tb_product_import_to_chaina
                                                            where tb_product_import_to_chaina.import_to_chaina_id = tb_import_to_chaina.id
                                                        ) as weight_all')
                                            ,\DB::raw('(select sum(cubic) from tb_product_import_to_chaina
                                                            where tb_product_import_to_chaina.import_to_chaina_id = tb_import_to_chaina.id
                                                        ) as cubic')
                                            ,\DB::raw('(select count(*) from tb_import_to_thai
                                                            join tb_product_import_to_thai on tb_product_import_to_thai.import_to_thai_id = tb_import_to_thai.id
                                                            where tb_import_to_thai.import_to_chaina_id = tb_import_to_chaina.id
                                                        ) as qty_thai')
                                            ,\DB::raw('(select count(*) from tb_qr_code_products
                                                            join tb_rel_container_products on tb_rel_container_products.qr_code_product_id = tb_qr_code_products.id
                                                            where tb_qr_code_products.import_to_chaina_id = tb_import_to_chaina.id
                                                        ) as qty_container')
                                            )
                                        ->join('import_to_thai', 'import_to_thai.import_to_chaina_id', '=', 'import_to_chaina.id')
                                        //->join('product_import_to_chaina', 'product_import_to_chaina.import_to_chaina_id', '=', 'import_to_chaina.id')
                                        //->join('lot_products', 'lot_products.product_import_to_chaina_id', '=', 'product_import_to_chaina.id')
                                        //->join('qr_code_products', 'qr_code_products.product_import_to_chaina_id', '<=', 'product_import_to_chaina.id')
                                        //->join('qr_code_products', 'qr_code_products.sort_id', '>=', 'lot_products.product_sort_end')
                                        ->groupBy('import_to_chaina.id')
                                        ->groupBy('import_to_chaina.user_id')
                                        ->groupBy('import_to_chaina.created_at')
                                        ->groupBy('import_to_chaina.po_no')
                                        ->groupBy('import_to_thai.created_at')
                                        //->with('User' ,'ImportToThai.ProductImportToThai.QrCodeProduct.ProductImportToChaina.Product.ProductType')
                                        //->where('lot_products.product_sort_start', '<=', 'qr_code_products.sort_id')
                                        //->where('lot_products.product_sort_end', '>=', 'qr_code_products.sort_id')
                                        ->where('import_to_thai.container_id', '=', $id)->get();
        $data['QrCodeProducts'] = \App\Models\ProductImportToThai::select('import_to_thai.container_id as container_id'
                                                                ,'import_to_chaina.po_no as po_no'
                                                                ,'qr_code_products.qr_code as qr_code'
                                                                ,'qr_code_products.sort_id as sort_id'
                                                                ,'users.customer_general_code as customer_general_code'
                                                                ,'lot_products.id as lot_id'
                                                                ,'products.name as product_name'
                                                                ,'product_types.name as product_type_name'
                                                                , \DB::raw('
                                                                        (select sum(qty)
                                                                        from tb_product_import_to_chaina
                                                                        where tb_product_import_to_chaina.import_to_chaina_id = tb_import_to_chaina.id
                                                                        ) as po_qty_chaina
                                                                    ')
                                                                , \DB::raw('
                                                                        (select count(*)
                                                                        from tb_product_import_to_thai
                                                                        where tb_product_import_to_thai.import_to_thai_id = tb_import_to_thai.id
                                                                        ) as po_qty_thai
                                                                    ')
                                                                , \DB::raw('
                                                                        (select count(*)
                                                                        from tb_rel_container_products
                                                                        join tb_qr_code_products on tb_qr_code_products.id = tb_rel_container_products.qr_code_product_id
                                                                        where tb_qr_code_products.import_to_chaina_id = tb_import_to_chaina.id
                                                                        ) as po_qty_container
                                                                    ')
                                                                // , \DB::raw('(`tb_lot_products`.`product_sort_end` - `tb_lot_products`.`product_sort_start`) + 1 as lot_product_qty')
                                                                ,\DB::raw('
                                                                    (select count(*) from tb_qr_code_products
                                                                        join tb_rel_container_products on tb_rel_container_products.qr_code_product_id = tb_qr_code_products.id
                                                                        join `tb_lot_products` as tlp ON `tlp`.`product_import_to_chaina_id` = `tb_qr_code_products`.`product_import_to_chaina_id`
                                                                        where `tb_qr_code_products`.`sort_id` BETWEEN `tlp`.`product_sort_start` and `tlp`.`product_sort_end`
                                                                        and tb_qr_code_products.import_to_chaina_id = tb_import_to_chaina.id
                                                                        and tb_lot_products.id = tlp.id
                                                                    ) as lot_product_qty
                                                                ')
                                                            )
                                                        ->join('import_to_thai', 'import_to_thai.id', '=', 'product_import_to_thai.import_to_thai_id')
                                                        ->join('import_to_chaina', 'import_to_chaina.id', '=', 'import_to_thai.import_to_chaina_id')
                                                        ->join('qr_code_products', 'qr_code_products.id', '=', 'product_import_to_thai.qr_code_product_id')
                                                        ->join('users', 'users.id', '=', 'import_to_chaina.user_id')
                                                        ->join('product_import_to_chaina', 'product_import_to_chaina.id', '=', 'qr_code_products.product_import_to_chaina_id')
                                                        ->join('products', 'products.id', '=', 'product_import_to_chaina.product_id')
                                                        ->join('product_types', 'product_types.id', '=', 'products.product_type_id')
                                                        ->join('lot_products', 'lot_products.product_import_to_chaina_id', '=', 'product_import_to_chaina.id')
                                                        // ->groupBy('import_to_thai.container_id')
                                                        // ->groupBy('import_to_chaina.po_no')
                                                        // ->groupBy('qr_code_products.qr_code')
                                                        // ->groupBy('qr_code_products.sort_id')
                                                        // ->groupBy('users.customer_general_code')
                                                        // ->groupBy('products.name')
                                                        // ->groupBy('product_types.name')
                                                        // ->groupBy('lot_products.id')
                                                        ->whereRaw('tb_lot_products.product_sort_start <= tb_qr_code_products.sort_id')
                                                        ->whereRaw('tb_lot_products.product_sort_end >= tb_qr_code_products.sort_id')
                                                        ->where('import_to_thai.container_id', $id)
                                                        ->get();

        //dd($data);
        return view('Admin.import_to_thai_detail', $data);
    }

}
