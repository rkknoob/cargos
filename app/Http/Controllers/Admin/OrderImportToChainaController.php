<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Storage;
use PDF;
use DB;
use \Mpdf\Mpdf;
use App\Providers\Admin\PermissionCRUDProvider as Permission;

class OrderImportToChainaController extends Controller
{

    public function index()
    {
    	$data['main_menu'] = 'OrderImportToChaina';
    	$data['sub_menu'] = '';
    	$data['title'] = trans('lang.adjust_value_of_money');
    	$data['title_page'] = trans('lang.adjust_value_of_money');
    	$data['permission'] = Permission::CheckPermission($data['main_menu'],$data['sub_menu']);
    	$data['menus'] = \App\Models\AdminMenu::ActiveMenu()->get();
    	return view('Admin.order_import_to_chaina',$data);
    }

    public function Lists(Request $request)
    {
        $result = \App\Models\ImportToChaina::select(
                'import_to_chaina.*'
                , 'import_to_chaina_statuses.color as status_color'
                , 'import_to_chaina_statuses.name as status_name'
                , 'import_to_chaina_statuses.number as status_number'
                , 'users.customer_general_code as customer_general_code'
                , \DB::raw('sum(tb_product_import_to_chaina.weight_all) weight_all_PO')
                , \DB::raw('sum(tb_product_import_to_chaina.qty) qty_all_PO')
                , \DB::raw('sum(tb_product_import_to_chaina.cubic) qty_all_cubic')
                // , \DB::raw('
                //     (
                //         select tb_rel_user_type_products.price
                //         from tb_product_import_to_chaina
                //         join tb_qr_code_products on tb_qr_code_products.product_import_to_chaina_id = tb_product_import_to_chaina.id
                //         join tb_rel_user_type_products on tb_rel_user_type_products.id = tb_qr_code_products.rel_user_type_product_id
                //         where tb_product_import_to_chaina.import_to_chaina_id = tb_import_to_chaina.id
                //         order by tb_rel_user_type_products.price asc
                //         limit 1
                //     ) as price
                // ')
                // , \DB::raw('
                //     (
                //         select tb_product_types.name
                //         from tb_product_import_to_chaina
                //         join tb_product_types on tb_product_types.id = tb_product_import_to_chaina.product_type_id
                //         where tb_product_import_to_chaina.import_to_chaina_id = tb_import_to_chaina.id
                //         order by tb_product_import_to_chaina.product_type_id asc
                //         limit 1
                //     ) as product_type_name
                // ')
            )
            ->with('ProductImportToChaina.QrCodeProductPrice.RelUserTypeProduct')
            ->with('ProductImportToChaina.Product')
            ->with('ProductImportToChaina.ProductType')
            ->join('users', 'users.id', '=', 'import_to_chaina.user_id')
            ->join('import_to_chaina_statuses', 'import_to_chaina_statuses.id', '=', 'import_to_chaina.status_id')
            ->join('product_import_to_chaina', 'product_import_to_chaina.import_to_chaina_id', '=', 'import_to_chaina.id')
            ->groupBy('import_to_chaina.id')
            ->groupBy('import_to_chaina.payment_status')
            ->groupBy('import_to_chaina_statuses.color')
            ->groupBy('import_to_chaina_statuses.name')
            ->groupBy('import_to_chaina_statuses.number')
            ->groupBy('users.customer_general_code')
            ->groupBy('import_to_chaina.user_id')
            ->groupBy('import_to_chaina.po_no')
            ->groupBy('import_to_chaina.import_date')
            ->groupBy('import_to_chaina.status_id')
            ->groupBy('import_to_chaina.created_at')
            ->groupBy('import_to_chaina.updated_at')
            // ->get()
            ;
        // return($result[0]->ProductImportToChaina);
        return \Datatables::of($result)

        ->addIndexColumn()
        ->editColumn('status_id', function($rec){
            //(x/y) AAAAA mm:ss-DM'
            $number = $rec->status_number;
            $name   = $rec->status_name;
            // $check_date = 'created_at';
            $date_status = $rec->created_at;
            if(!empty($rec->updated_at)){
                $date_status = $rec->updated_at;
                // $check_date = 'updated_at';
            }
            $time   = date_format($date_status, "H:i");
            $day    = date_format($date_status, "d");
            $month  = showM()[date_format($date_status, "n")];

            $color_status = (!empty($rec->status_color) ? $rec->status_color : '#000');
            return '<label class="label" style="background-color: '. $color_status .';">('.$number.'/5)'.$name.' '.$time.' - '.$day.' '.$month.'</label>';
        })
        ->editColumn('weight_all_PO', function($rec){
            $weight_all_PO = (!empty($rec->weight_all_PO) ? number_format($rec->weight_all_PO, 2) : trans('lang.Have_not_weighed'));
            return $weight_all_PO;
        })
        ->editColumn('qty_all_cubic', function($rec){
            $weight_all_cubic = number_format($rec->qty_all_cubic, 2);
            // $weight_all_cubic = (!empty($rec->qty_all_cubic) ? number_format($rec->qty_all_cubic, 2) : '0.00');
            return $weight_all_cubic;
        })
        ->editColumn('qty_all_PO', function($rec){
            $qty_all_PO = number_format($rec->qty_all_PO);
            return $qty_all_PO;
        })
        ->editColumn('price', function($rec){
            $price = '';
            if( !empty($rec->ProductImportToChaina) ){
                // foreach ($rec->ProductImportToChaina as $key => $ProductImportToChaina) {
                //     $price .= $ProductImportToChaina->Product->name .': '. number_format($ProductImportToChaina->QrCodeProductPrice->RelUserTypeProduct['price'], 2) .'<br>';
                // }
                foreach ($rec->ProductImportToChaina as $key => $ProductImportToChaina) {
                    $price .= number_format($ProductImportToChaina->QrCodeProductPrice->RelUserTypeProduct['price'], 2);
                }
            }
            return $price;
        })
        ->editColumn('product_type_name', function($rec){
            $typeproduct = '';
            if( !empty($rec->ProductImportToChaina) ){
                foreach ($rec->ProductImportToChaina as $key => $ProductImportToChaina) {
                    $typeproduct .= $ProductImportToChaina->Product->name .': '. $ProductImportToChaina->ProductType->name .'<br>';
                }
            }
            return $typeproduct;
        })
        ->editColumn('status_rate',function($rec){
            $str = ($rec->status_rate == 'T') ? "<font color=\"green\">".trans('lang.status_rate_true')."</font>" : "<font color=\"red\">".trans('lang.status_rate_false')."</font>";
            return $str;
        })
        ->addColumn('action',function($rec){
            $lang = \Config::get('app.locale');
            $str='
                <a href="'. url('admin/'.$lang.'/OrderImportToChaina/EditPrice/'. $rec->id) .'" data-loading-text="<i class=\'fa fa-refresh fa-spin\'></i>" class="btn btn-xs btn-warning btn-condensed btn-edit btn-tooltip" data-rel="tooltip" data-id="'. $rec->id .'" title="'.trans('lang.adjust_value_of_money').'">
                    <i class="ace-icon fa fa-cog bigger-120"></i>
                </a>
            ';
            return $str;
        })
        ->rawColumns(['status_id', 'action', 'price', 'product_type_name', 'status_rate'])
        ->make(true);
    }

    public function EditPrice($id)
    {
    	$data['main_menu'] = 'OrderImportToChaina';
    	$data['sub_menu'] = '';
    	$data['title'] = trans('lang.adjust_value_of_money');
    	$data['title_page'] = trans('lang.adjust_value_of_money');
    	$data['permission'] = Permission::CheckPermission($data['main_menu'],$data['sub_menu']);
    	$data['menus'] = \App\Models\AdminMenu::ActiveMenu()->get();
    	$data['id_po'] = $id;
    	return view('Admin.order_import_to_chaina_edit_price',$data);
    }

    public function EditPriceLists($id, Request $request)
    {
        $result = \App\Models\ProductImportToChaina::select(
                'product_import_to_chaina.*'
                , 'import_to_chaina.po_no'
                , 'import_to_chaina.import_date'
                , 'product_types.name as product_type_name'
                ,'transport_types.name_th as transport_type_name_th'
                ,'transport_types.name_en as transport_type_name_en'
                ,'transport_types.name_ch as transport_type_name_ch'
                // , 'container_types.name as container_type_name'
                , 'products.name as product_name'
                , 'users.customer_general_code'
                , 'rel_user_type_products.price'
            )
            ->join('transport_types', 'transport_types.id', '=', 'product_import_to_chaina.transport_type_id')
            // ->join('container_types', 'container_types.id', '=', 'product_import_to_chaina.container_type_id')
            ->join('products', 'products.id', '=', 'product_import_to_chaina.product_id')
            ->join('product_types', 'product_types.id', '=', 'product_import_to_chaina.product_type_id')
            ->join('import_to_chaina', 'import_to_chaina.id', '=', 'product_import_to_chaina.import_to_chaina_id')
            ->join('users', 'users.id', '=', 'import_to_chaina.user_id')
            ->leftjoin('qr_code_products', 'qr_code_products.product_import_to_chaina_id', '=', 'product_import_to_chaina.id')
            ->leftjoin('rel_user_type_products', 'rel_user_type_products.id', '=', 'qr_code_products.rel_user_type_product_id')
            ->groupBy('rel_user_type_products.price')
            ->where('product_import_to_chaina.import_to_chaina_id', $id);
        return \Datatables::of($result)
            ->addIndexColumn()
            ->editColumn('qty',function($rec){
                $str=number_format($rec->qty);
                return $str;
            })
            ->editColumn('weight_all',function($rec){
                $str=number_format($rec->weight_all, 2);
                return $str;
            })
            ->editColumn('cubic',function($rec){
                $str=number_format($rec->cubic/100, 2);
                return $str;
            })
            ->editColumn('status_rate',function($rec){
                // $str = ($rec->status_rate == 'T') ? trans('lang.status_rate_true') : trans('lang.status_rate_false');
                $str = ($rec->status_rate == 'T') ? "<font color=\"green\">".trans('lang.status_rate_true')."</font>" : "<font color=\"red\">".trans('lang.status_rate_false')."</font>";
                return $str;
            })
            ->addColumn('price',function($rec){
                return number_format($rec->price,2);
            })
            ->addColumn('action',function($rec){
                $str='
                    <button data-loading-text="<i class=\'fa fa-refresh fa-spin\'></i>" class="btn btn-xs btn-info btn-condensed btn-edit btn-tooltip" data-rel="tooltip" data-id="'. $rec->id .'" title="'.trans('lang.adjust_value_of_money').'">
                        <i class="ace-icon fa fa-btc bigger-120"></i>
                    </button>
                ';
                return $str;
            })
            //->rawColumns(['status_rate'])
             ->rawColumns(['action', 'status_rate'])
            ->make(true);
    }

    public function ShowEditPrice($id)
    {
        $data['ProductToChaina'] = \App\Models\ProductImportToChaina::select()
            ->with(
                'Product.ProductType'
                , 'TransportType'
                , 'ImportToChaina.User'
                , 'QrCodeProductPrice.RelUserTypeProduct'
                , 'QrCodeProductPrice.RelUserTypeProductOld'
            )
            ->where('product_import_to_chaina.id', $id)
            ->first();
        // หาราคาล่าสดที่ถูกใช้งาน ใน บุคคลและประเภทสินค้านี้
        $data['ProductToChainaOldPrice'] = \App\Models\QrCodeProduct::select(
                'qr_code_products.rel_user_type_product_id',
                'rel_user_type_products.price',
                'rel_user_type_products.type_rate',
                'import_to_chaina.user_id',
                'products.product_type_id'
            )
            ->join('product_import_to_chaina', 'qr_code_products.product_import_to_chaina_id', '=', 'product_import_to_chaina.id')
            ->join('products', 'products.id', '=', 'product_import_to_chaina.product_id')
            ->join('import_to_chaina', 'import_to_chaina.id', '=', 'qr_code_products.import_to_chaina_id')
            ->join('rel_user_type_products', 'rel_user_type_products.id', '=', 'qr_code_products.rel_user_type_product_id')
            ->where('products.product_type_id', $data['ProductToChaina']->Product->product_type_id)
            ->where('import_to_chaina.user_id', $data['ProductToChaina']->ImportToChaina->user_id)
            ->orderBy('product_import_to_chaina.id', 'desc')
            ->first();
        // dd($data);
        $data['price'] = \App\Models\RelUserTypeProduct::select()
            ->where('product_type_id', $data['ProductToChaina']->Product->product_type_id)
            ->where('user_id', $data['ProductToChaina']->ImportToChaina->user_id)
            ->get();
    	return json_encode($data);
    }

    public function EditPriceUpdate($id, Request $request)
    {
        $data['product_import_id'] = \App\Models\ProductImportToChaina::where('id', $id)
                                        ->select('product_import_to_chaina.import_to_chaina_id')->first();
        $data['product_imports'] = \App\Models\ProductImportToChaina::where('import_to_chaina_id', $data['product_import_id']->import_to_chaina_id)
                                        ->select('product_import_to_chaina.status_rate')
                                        ->where('product_import_to_chaina.id', '!=', $id)
                                        ->get();
        $status_default = 'T';
        foreach ($data['product_imports'] as $key => $product_import_id) {
            if ($product_import_id->status_rate != 'T' && empty($product_import_id->status_rate)){
                $status_default = 'F';
            } 
        }
        $input_all = $request->all();
        // dd($input_all);
        $validator = Validator::make($request->all(), [
            // 'name' => 'required',
        ]);
        if (!$validator->fails()) {
            \DB::beginTransaction();
            try {
                $checkrate = \App\Models\RelUserTypeProduct::where('product_type_id', $input_all['product_type_id'])
                    ->where('user_id', $input_all['user_id'])
                    ->where('price', $input_all['price'])
                    ->first();
                if($checkrate){
                    $rel_user_type_product = $checkrate->id;
                }else{
                    $data_insert_rel_user_type_product = [
                        'product_type_id' => $input_all['product_type_id'],
                        'user_id' => $input_all['user_id'],
                        'price' => $input_all['price'],
                        'type_rate' => $input_all['type_rate'],
                        'created_at' => date('Y-m-d H:i:s')
                    ];
                    $rel_user_type_product = \App\Models\RelUserTypeProduct::insertGetId($data_insert_rel_user_type_product);
                }
                $data_update = [
                    'rel_user_type_product_id' => $rel_user_type_product,
                    'rel_user_type_product_id_old' => $input_all['rel_user_type_product_id_old'],
                    'type_rate' => $input_all['type_rate']
                ];
                \App\Models\QrCodeProduct::where('product_import_to_chaina_id', $id)->where('delivery_status', 'F')->update($data_update);
                \App\Models\ProductImportToChaina::where('id', $id)->update(['status_rate' => 'T']);
               \App\Models\ImportToChaina::where('id', $data['product_import_id']->import_to_chaina_id)
               ->update(['status_rate' => $status_default]);

                \DB::commit();
                $return['status'] = 1;
                $return['content'] = 'สำเร็จ';
            } catch (Exception $e) {
                \DB::rollBack();
                $return['status'] = 0;
                $return['content'] = trans('lang.unsuccessful').$e->getMessage();
            }
        }else{
            $return['status'] = 0;
        }
        $return['title'] = trans('lang.edit_data');
        return json_encode($return);
    }

}
