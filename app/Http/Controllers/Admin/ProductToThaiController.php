<?php

namespace App\Http\Controllers\Admin;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \Mpdf\Mpdf;
use Storage;

class ProductToThaiController extends Controller
{

    public function index()
    {
        $data['main_menu'] = 'ProductToThai';
        $data['sub_menu'] = 'ProductToThai';
        $data['title_page'] = trans('lang.product_warehouse_thai');
        $data['menus'] = \App\Models\AdminMenu::ActiveMenu()->get();
        $data['Containers'] = \App\Models\Container::get();
        return view('Admin.product_to_thai',$data);
    }

    public function create()
    {
    }

    public function store(Request $request)
    {
        $input_all = $request->all();

        $input_all1['container_id'] = $input_all['container_id'];

        $input_all2['created_at'] = date('Y-m-d H:i:s');
        $input_all2['updated_at'] = date('Y-m-d H:i:s');
        $input_all2['date_import'] = date('Y-m-d H:i:s');

        $input_all3['created_at'] = date('Y-m-d H:i:s');
        $input_all3['updated_at'] = date('Y-m-d H:i:s');

        $validator = Validator::make($request->all(), [
            'container_id' => 'required',
        ]);

        if (!$validator->fails()) {
            \DB::beginTransaction();
            try {
                $data_insert1 = $input_all1;
                \App\Models\Container::where('id', $data_insert1['container_id'])->update(['status_id' => 2]);

                if(isset($input_all['import_to_chaina_id'])){
                    foreach($input_all['import_to_chaina_id'] as $import_to_chaina_id => $import_to_chainas){
                        $input_all2['container_id']         = $data_insert1['container_id'];
                        $input_all2['import_to_chaina_id']  = $import_to_chaina_id;

                        $data_insert2 = $input_all2;
                        $import_to_thai_id = \App\Models\ImportToThai::insertGetId($data_insert2);

                        if(isset($import_to_chainas)){
                            foreach($import_to_chainas['qr_code_product_id'] as $qr_code_product_id){
                                $input_all3['import_to_thai_id']  = $import_to_thai_id;
                                $input_all3['qr_code_product_id']   = $qr_code_product_id;

                                $data_insert3 = $input_all3;
                                \App\Models\ProductImportToThai::insert($data_insert3);
                            }
                        }
                    }
                }

                \DB::commit();
                $return['status'] = 1;
                $return['content'] = 'สำเร็จ';
            } catch (Exception $e) {
                \DB::rollBack();
                $return['status'] = 0;
                $return['content'] = 'ไม่สำเร็จ'.$e->getMessage();
            }
        }else{
            $return['status'] = 0;
        }
        $return['title'] = 'เพิ่มข้อมูล';
        return json_encode($return);
    }

    public function show($id)
    {
        $result['ImportToThai']     = \App\Models\ImportToThai::with('ProductImportToThai.QrCodeProduct.ProductImportToChaina.Product.ProductType'
                                                ,'ProductImportToThai.QrCodeProduct.ProductImportToChaina.TransportType'
                                                ,'ImportToChaina.User'
                                                ,'ImportToChaina.ProductImportToChaina'
                                                ,'ImportToChaina.ImportToChainaStatus'
                                                ,'Container.ContainerStatus'
                                                ,'Container.RelContainerProduct')
                                            ->where('id', $id)->first();
        $result['DeliveryToUser']   = \App\Models\DeliveryToUser::with('Delivery.DeliveryStatus')
                                            ->where('import_to_chaina_id', $result['ImportToThai']->ImportToChaina->id)
                                            ->first();
        $result['DeliverySlipList'] = \App\Models\DeliverySlipList::with('DeliverySlip.UserAddress.Province', 'DeliverySlip.UserAddress.Amphure')
                                            ->where('import_to_chaina_id', $result['ImportToThai']->ImportToChaina->id)
                                            ->first();

        return json_encode($result);
    }

    public function edit($id)
    {

    }

    public function update(Request $request, $id)
    {
        $input_all = $request->all();

        $input_all['updated_at'] = date('Y-m-d H:i:s');

        $validator = Validator::make($request->all(), [
            'container_id' => 'required',
             'date_import' => 'required',

        ]);
        if (!$validator->fails()) {
            \DB::beginTransaction();
            try {
                $data_insert = $input_all;
                \App\Models\ImportToThai::where('id',$id)->update($data_insert);
                \DB::commit();
                $return['status'] = 1;
                $return['content'] = 'สำเร็จ';
            } catch (Exception $e) {
                \DB::rollBack();
                $return['status'] = 0;
                $return['content'] = 'ไม่สำเร็จ'.$e->getMessage();
            }
        }else{
            $return['status'] = 0;
        }
        $return['title'] = 'เพิ่มข้อมูล';
        return json_encode($return);
    }

    public function destroy($id)
    {
        \DB::beginTransaction();
        try {
            \App\Models\ImportToThai::where('id',$id)->delete();
            \DB::commit();
            $return['status'] = 1;
            $return['content'] = 'สำเร็จ';
        } catch (Exception $e) {
            \DB::rollBack();
            $return['status'] = 0;
            $return['content'] = 'ไม่สำเร็จ'.$e->getMessage();
        }
        $return['title'] = 'ลบข้อมูล';
        return $return;
    }

    public function Lists()
    {
        $result = \App\Models\ImportToThai::select(
            'import_to_thai.id as import_to_thai_id'
            ,'users.customer_general_code as customer_general_code'
            ,'users.firstname as firstname'
            ,'users.lastname as lastname'
            ,'import_to_chaina.po_no as po_no'
            ,'import_to_thai.created_at as date_import'
            ,'containers.container_code as container_code'
            ,'import_to_chaina.updated_at as updated_at'
            ,'import_to_chaina_statuses.number as import_to_chaina_ststus_number'
            ,'import_to_chaina_statuses.name as import_to_chaina_ststus_name'
            ,\DB::raw('(select sum(qty) from tb_import_to_chaina
                    join tb_product_import_to_chaina on tb_product_import_to_chaina.import_to_chaina_id = tb_import_to_chaina.id
                    where tb_import_to_thai.import_to_chaina_id = tb_import_to_chaina.id
                ) as qty_chaina')
            ,\DB::raw('(select count(*) from `tb_product_import_to_thai`
                    join `tb_rel_container_products` on `tb_rel_container_products`.`qr_code_product_id` = `tb_product_import_to_thai`.`qr_code_product_id`
                    where `tb_product_import_to_thai`.`import_to_thai_id` = `tb_import_to_thai`.`id`
                    and `tb_rel_container_products`.`container_id` = `tb_containers`.`id`
                ) as qty_thai')
            ,\DB::raw('(select count(*) from tb_qr_code_products
                    join tb_rel_container_products on tb_rel_container_products.qr_code_product_id = tb_qr_code_products.id
                    where tb_qr_code_products.import_to_chaina_id = tb_import_to_chaina.id
                    and `tb_rel_container_products`.`container_id` = `tb_containers`.`id`
                ) as qty_container')
            ,\DB::raw('(select sum(`tb_lot_products`.`weight_per_item`)
                    from `tb_qr_code_products`
                    join `tb_rel_container_products` on `tb_rel_container_products`.`qr_code_product_id` = `tb_qr_code_products`.`id`
                    join `tb_product_import_to_thai` on `tb_product_import_to_thai`.`qr_code_product_id` = `tb_qr_code_products`.`id`
                    join `tb_lot_products` ON `tb_lot_products`.`product_import_to_chaina_id` = `tb_qr_code_products`.`product_import_to_chaina_id`
                    where `tb_qr_code_products`.`sort_id` BETWEEN `tb_lot_products`.`product_sort_start` and `tb_lot_products`.`product_sort_end`
                    and `tb_qr_code_products`.`import_to_chaina_id` = `tb_import_to_chaina`.`id`
                    and `tb_rel_container_products`.`container_id` = `tb_containers`.`id`
                ) as weight_all')
                 // ((`tb_lot_products`.`product_sort_end` - `tb_lot_products`.`product_sort_start`)+1)
            ,\DB::raw('(select sum( `tb_lot_products`.`cubic`  )
                    from `tb_qr_code_products`
                    join `tb_rel_container_products` on `tb_rel_container_products`.`qr_code_product_id` = `tb_qr_code_products`.`id`
                    join `tb_product_import_to_thai` on `tb_product_import_to_thai`.`qr_code_product_id` = `tb_qr_code_products`.`id`
                    join `tb_lot_products` ON `tb_lot_products`.`product_import_to_chaina_id` = `tb_qr_code_products`.`product_import_to_chaina_id`
                    where `tb_qr_code_products`.`sort_id` BETWEEN `tb_lot_products`.`product_sort_start` and `tb_lot_products`.`product_sort_end`
                    and `tb_qr_code_products`.`import_to_chaina_id` = `tb_import_to_chaina`.`id`
                    and `tb_rel_container_products`.`container_id` = `tb_containers`.`id`
                ) as cubic_all')
        )
        ->join('import_to_chaina','import_to_chaina.id','=','import_to_thai.import_to_chaina_id')
        ->join('import_to_chaina_statuses','import_to_chaina_statuses.id','=','import_to_chaina.status_id')
        ->join('containers','containers.id','=','import_to_thai.container_id')
        ->join('users','users.id','=','import_to_chaina.user_id');

        return \Datatables::of($result)
        ->addColumn('customer', function($rec){
            return $rec->firstname.' '.$rec->lastname;
        })
        ->addColumn('qty_chaina', function($rec){
            return number_format($rec->qty_chaina);
        })
        ->addColumn('qty_thai', function($rec){
            return number_format($rec->qty_thai);
        })
        ->addColumn('qty_container', function($rec){
            return number_format($rec->qty_container);
        })
        ->addColumn('weight_all', function($rec){
            return number_format($rec->weight_all);
        })
        ->addColumn('cubic_all', function($rec){
            return number_format($rec->cubic_all);
        })
        ->addColumn('status', function($rec){
            //(x/y) AAAAA mm:ss-DM'
            $number = $rec->import_to_chaina_ststus_number;
            $name   = $rec->import_to_chaina_ststus_name;
            $time   = date_format($rec->updated_at, "H:i");
            $day    = date_format($rec->updated_at, "d");
            $month  = showM()[date_format($rec->updated_at, "n")];

            $date_import        =  date_create($rec->date_import);
            $date_import_day    = date_format($date_import, "Y-m-d");

            $date_now   = date('Y-m-d'); //วันที่ปัจจุบัน
            $date_5_day = date('Y-m-d', strtotime($date_import_day.'+5 days')); //5วัน เปลี่ยนสีน้ำเงินนับจากวันที่รับสินค้าจากจีน
            $date_8_day = date('Y-m-d', strtotime($date_import_day.'+8 days')); //8วัน เปลี่ยนสีน้ำเงินนับจากวันที่รับสินค้าจากจีน

            $chk_now    = strtotime('now');
            $chk_5_day  = strtotime($date_import_day.'+5 days');
            $chk_8_day  = strtotime($date_import_day.'+8 days');

            if($chk_now > $chk_5_day && $chk_now < $chk_8_day){
                $status_color = 'style="color: blue;"'; //เกิน 5วัน เปลี่ยนสีน้ำเงินนับจากวันที่รับสินค้าจากจีน
            }elseif($chk_now > $chk_8_day){
                $status_color = 'style="color: red;"'; //เกิน 8วัน เปลี่ยนสีน้ำเงินนับจากวันที่รับสินค้าจากจีน
            }else{
                $status_color = '';
            }

            return '<p '.$status_color.'>('.$number.'/5)'.$name.' '.$time.' - '.$day.' '.$month.'</p>';
        })
        ->addColumn('action',function($rec){
            // <a href="'.url("/admin/ProductToThai/PrintPo/".$rec->import_to_thai_id).'" target="_blank" class="btn btn-xs btn-info btn-condensed btn-print btn-tooltip" data-id="'.$rec->import_to_thai_id.'" data-rel="tooltip" title="ปริ้นใบส่งของ">
            //     <i class="ace-icon fa fa-print bigger-120"></i>
            // </a>
            $str='
                <button data-loading-text="<i class=\'fa fa-refresh fa-spin\'></i>" class="btn btn-xs btn-warning btn-condensed btn-detail btn-tooltip" data-rel="tooltip" data-id="'.$rec->import_to_thai_id.'" title="'.trans('lang.view_detail').'">
                    <i class="ace-icon fa fa-search bigger-120"></i>
                </button>
            ';
            return $str;
        })->rawColumns(['status', 'action'])->make(true);
    }

    public function DetailLists(Request $request)
    {
        $import_to_thai_id = $request->input('id');
        $result = \App\Models\ProductImportToThai::select(
                    'product_import_to_chaina.shop_chaina_name as shop_chaina_name',
                    'product_import_to_chaina.qty as qty',
                    'product_import_to_chaina.weight_all as weight_all',
                    'products.code as product_code',
                    'products.name as product_name',
                    'product_types.code as product_type_code',
                    'product_types.name as product_type_name'
                    ,'transport_types.name_th as transport_type_name_th'
                    ,'transport_types.name_en as transport_type_name_en'
                    ,'transport_types.name_ch as transport_type_name_ch')
                ->join('import_to_thai','import_to_thai.id','=','product_import_to_thai.import_to_thai_id')
                ->join('qr_code_products','qr_code_products.id','=','product_import_to_thai.qr_code_product_id')
                ->join('product_import_to_chaina','product_import_to_chaina.id','=','qr_code_products.product_import_to_chaina_id')
                ->join('products','products.id','=','product_import_to_chaina.product_id')
                ->join('product_types','product_types.id','=','products.product_type_id')
                ->join('transport_types','transport_types.id','=','product_import_to_chaina.transport_type_id')

                ->where('product_import_to_thai.import_to_thai_id', $import_to_thai_id);

        return \Datatables::of($result)

        ->editcolumn('qty', function($rec){
            return number_format($rec->qty / $rec->qty);
        })

        ->editcolumn('weight_all', function($rec){
            return number_format($rec->weight_all / $rec->qty);
        })

        ->addColumn('action',function($rec){
            $str='
                <button data-loading-text="<i class=\'fa fa-refresh fa-spin\'></i>" class="btn btn-xs btn-warning btn-condensed btn-detail btn-tooltip" data-rel="tooltip" data-id="'.$rec->import_to_thai_id.'" title="ดูรายละเอียด">
                    <i class="ace-icon fa fa-search bigger-120"></i>
                </button>
                <a href="'.url("/admin/ProductToThai/PrintPo/".$rec->import_to_thai_id).'" target="_blank" class="btn btn-xs btn-info btn-condensed btn-print btn-tooltip" data-id="'.$rec->import_to_thai_id.'" data-rel="tooltip" title="ปริ้นใบส่งของ">
                    <i class="ace-icon fa fa-print bigger-120"></i>
                </a>
            ';
            return $str;
        })->addIndexColumn()->make(true);
    }

    public function PrintPo($id)
    {
        $data['ImportToThai'] = \App\Models\ImportToThai::with('ImportToChaina.User')->where('id', $id)->first();
        $data['LotProducts'] = \App\Models\LotProduct::select('lot_products.remark as remark'
                                                ,'lot_products.id as id'
                                                ,'products.code as product_code'
                                                ,'products.name as product_name'
                                                ,'product_import_to_chaina.qty as product_qty'
                                                // ,\DB::raw('(select count(*) from `tb_lot_products`
                                                //         join `tb_rel_container_products` on `tb_rel_container_products`.`qr_code_product_id` = `tb_product_import_to_thai`.`qr_code_product_id`
                                                //         where `tb_product_import_to_thai`.`import_to_thai_id` = `tb_import_to_thai`.`id`
                                                //         and `tb_rel_container_products`.`container_id` = `tb_containers`.`id`
                                                //     ) as qty_thai')
                                                )
                ->join('product_import_to_chaina', 'product_import_to_chaina.id', '=', 'lot_products.product_import_to_chaina_id')
                ->join('import_to_chaina', 'import_to_chaina.id', '=', 'product_import_to_chaina.import_to_chaina_id')
                ->join('import_to_thai', 'import_to_thai.import_to_chaina_id', '=', 'import_to_chaina.id')
                ->join('product_import_to_thai', 'product_import_to_thai.import_to_thai_id', '=', 'import_to_thai.id')
                ->join('products', 'products.id', '=', 'product_import_to_chaina.product_id')
                ->groupBy('lot_products.id')
                ->groupBy('lot_products.remark')
                ->groupBy('products.code')
                ->groupBy('products.name')
                ->groupBy('product_import_to_chaina.qty')
                ->where('product_import_to_thai.import_to_thai_id', $id)->get();

        $mpdf = new Mpdf(['autoLangToFont' => true]);
        $data2 = view('Admin.import_to_thai_print', $data);
        $mpdf->WriteHTML($data2);
        $mpdf->Output();
    }

}
