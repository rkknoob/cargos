<?php

namespace App\Http\Controllers\Admin;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \Mpdf\Mpdf;
use Storage;

class AnalysisController extends Controller
{
    public function index()
    {
        $data['main_menu'] = 'Analysis';
        $data['sub_menu'] = 'Analysis';
        $data['title_page'] = trans('lang.analysis');
        $data['menus'] = \App\Models\AdminMenu::ActiveMenu()->get();
        $data['Users'] = \App\Models\User::get();

        return view('Admin.analysis',$data);
    }

    public function Lists(){
        $lang_code = \Config::get('app.locale');
        $result = \App\Models\ImportToChaina::select(
                'import_to_chaina.*'
                , 'import_to_chaina_statuses.color as status_color'
                , 'import_to_chaina_statuses.name as status_name'
                , 'import_to_chaina_statuses.number as status_number'
                , 'users.customer_general_code'
                , \DB::raw('
                    (   select tb_containers.date_chaina
                        from tb_rel_container_products
                        join tb_qr_code_products on tb_qr_code_products.id = tb_rel_container_products.qr_code_product_id
                        join tb_containers on tb_containers.id = tb_rel_container_products.container_id
                        where tb_qr_code_products.import_to_chaina_id = tb_import_to_chaina.id
                        order by tb_containers.date_chaina asc
                        limit 1
                    ) as date_container
                ')  // วันรับเข้าตู้
                , \DB::raw('
                    (   select tb_product_types.name
                        from tb_product_import_to_chaina
                        join tb_product_types on tb_product_types.id = tb_product_import_to_chaina.product_type_id
                        where tb_product_import_to_chaina.import_to_chaina_id = tb_import_to_chaina.id
                        order by tb_product_import_to_chaina.product_type_id asc
                        limit 1
                    ) as type_product
                ') // ประเภทสินค้า
                , \DB::raw('
                    (   select tb_containers.container_code
                        from tb_rel_container_products
                        join tb_qr_code_products on tb_qr_code_products.id = tb_rel_container_products.qr_code_product_id
                        join tb_containers on tb_containers.id = tb_rel_container_products.container_id
                        where tb_qr_code_products.import_to_chaina_id = tb_import_to_chaina.id
                        order by tb_containers.date_chaina asc
                        limit 1
                    ) as code_container
                ') // เลขตู้
                , \DB::raw('
                    (   select sum(tb_product_import_to_chaina.qty)
                        from tb_product_import_to_chaina
                        where tb_product_import_to_chaina.import_to_chaina_id = tb_import_to_chaina.id
                    ) as import_to_chaina_qty
                ') // จำนวนสินค้าใน PO
                , \DB::raw('
                    (   select sum(tb_product_import_to_chaina.pcs)
                        from tb_product_import_to_chaina
                        where tb_product_import_to_chaina.import_to_chaina_id = tb_import_to_chaina.id
                    ) as import_to_chaina_pcs
                ') // จำนวนสินค้าใน packkage
                , \DB::raw('
                    (   select sum(tb_product_import_to_chaina.weight_all)
                        from tb_product_import_to_chaina
                        where tb_product_import_to_chaina.import_to_chaina_id = tb_import_to_chaina.id
                    ) as import_to_chaina_weight_all
                ') // น้ำหนักรวมใน PO
                , \DB::raw('
                    (   select sum(tb_lot_products.cubic)
                        from tb_product_import_to_chaina
                        join tb_lot_products on tb_lot_products.product_import_to_chaina_id = tb_product_import_to_chaina.id
                        where tb_product_import_to_chaina.import_to_chaina_id = tb_import_to_chaina.id
                    ) as import_to_chaina_cm3
                ') // ลูกบาศ รวมใน PO
                , \DB::raw('
                    (
                        select sum( (tb_rel_user_type_products.price * tb_lot_products.weight_per_item) / (((tb_lot_products.product_sort_end - tb_lot_products.product_sort_start) + 1) / tb_lot_products.cubic) )
                        from tb_qr_code_products
                        join tb_lot_products on tb_lot_products.product_import_to_chaina_id = tb_qr_code_products.product_import_to_chaina_id
                        join tb_rel_user_type_products on tb_rel_user_type_products.id = tb_qr_code_products.rel_user_type_product_id
                        where tb_qr_code_products.import_to_chaina_id = tb_import_to_chaina.id
                        and tb_qr_code_products.sort_id BETWEEN tb_lot_products.product_sort_start AND tb_lot_products.product_sort_end
                    ) as import_to_chaina_price
                ') // ราคา รวมใน PO สูตร (rate_price * product_weight_per_item) / $cubil
                , \DB::raw('
                    (   select tb_transport_types.name_'.$lang_code.'
                        from tb_product_import_to_chaina
                        join tb_transport_types on tb_transport_types.id = tb_product_import_to_chaina.transport_type_id
                        where tb_product_import_to_chaina.import_to_chaina_id = tb_import_to_chaina.id
                        order by tb_product_import_to_chaina.product_type_id asc
                        limit 1
                    ) as transport_type
                ') // เดินทางโดย
            )
            ->join('users', 'users.id', '=', 'import_to_chaina.user_id')
            ->join('import_to_chaina_statuses', 'import_to_chaina_statuses.id', '=', 'import_to_chaina.status_id')
            // ->get()
        ;
        // dd($result);
        return \Datatables::of($result)
        ->editColumn('import_to_chaina_price', function($rec){
            return number_format($rec->import_to_chaina_price, 2);
        })
        ->editColumn('status_id', function($rec){
            //(x/y) AAAAA mm:ss-DM'
            $number = $rec->status_number;
            $name   = $rec->status_name;
            // $check_date = 'created_at';
            $date_status = $rec->created_at;
            if(!empty($rec->updated_at)){
                $date_status = $rec->updated_at;
                // $check_date = 'updated_at';
            }
            $time   = date_format($date_status, "H:i");
            $day    = date_format($date_status, "d");
            $month  = showM()[date_format($date_status, "n")];

            $color_status = (!empty($rec->status_color) ? $rec->status_color : '#000');
            return '<label class="label" style="background-color: '. $color_status .';">('.$number.'/5)'.$name.' '.$time.' - '.$day.' '.$month.'</label>';
        })
        ->addColumn('action',function($rec){
            $str='
                <a href="'.url('/admin/Shipping/'.$rec->id).'" data-loading-text="<i class=\'fa fa-refresh fa-spin\'></i>" class="btn btn-xs btn-info btn-condensed btn-tooltip" data-rel="tooltip" data-id="'.$rec->id.'" title="ออกใบส่งสินค้า">
                    <i class="ace-icon fa fa-list bigger-120"></i>
                </a>
                <a href="'.url('/admin/Delivery/Detail/'.$rec->id).'" data-loading-text="<i class=\'fa fa-refresh fa-spin\'></i>" class="btn btn-xs btn-warning btn-condensed btn-tooltip" data-rel="tooltip" data-id="'.$rec->id.'" title="ดูรายละเอียด">
                    <i class="ace-icon fa fa-search bigger-120"></i>
                </a>
            ';
            return $str;
        })
        ->addColumn('null',function($rec){
            $str='';
            return $str;
        })
        ->rawColumns(['status_id', 'action'])
        ->make(true);
    }
}
