<?php

namespace App\Http\Controllers\Admin;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Storage;
class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['main_menu'] = 'Website';
        $data['sub_menu'] = 'News';
        $data['title_page'] = trans('lang.news');
        $data['menus'] = \App\Models\AdminMenu::ActiveMenu()->get();
        
        return view('Admin.news',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input_all = $request->all();
        $input_all['active'] = $request->input('active','F');
            if(isset($input_all['image'])&&isset($input_all['image'][0])){
                $input_all['image'] = $input_all['image'][0];
                if(Storage::disk("uploads")->exists("temp/".$input_all['image'])&&!Storage::disk("uploads")->exists("News/".$input_all['image'])){
                    Storage::disk("uploads")->copy("temp/".$input_all['image'],"News/".$input_all['image']);
                    Storage::disk("uploads")->delete("temp/".$input_all['image']);
                }
            }
        
        $input_all['admin_id'] = auth()->guard('admin')->user()->id;
        $input_all['created_at'] = date('Y-m-d H:i:s');
        $input_all['updated_at'] = date('Y-m-d H:i:s');

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'title' => 'required',
            'news_date' => 'required',
            'active' => 'required',
            'image' => 'required',
             
        ]);
        if (!$validator->fails()) {
            \DB::beginTransaction();
            try {
                $data_insert = $input_all;
                \App\Models\News::insert($data_insert);
                \DB::commit();
                $return['status'] = 1;
                $return['content'] = 'สำเร็จ';
            } catch (Exception $e) {
                \DB::rollBack();
                $return['status'] = 0;
                $return['content'] = trans('lang.unsuccessful').$e->getMessage();
            }
        }else{
            $return['status'] = 0;
        }
        $return['title'] = trans('lang.create_data');
        return json_encode($return);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = \App\Models\News::find($id);
        
            if($result){
                if($result->image){
                    if(Storage::disk("uploads")->exists("News/".$result->image)){
                        if(Storage::disk("uploads")->exists("temp/".$result->image)){
                            Storage::disk("uploads")->delete("temp/".$result->image);
                        }
                        Storage::disk("uploads")->copy("News/".$result->image,"temp/".$result->image);
                    }
                }
            }
        
        return json_encode($result);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input_all = $request->all();
        $input_all['active'] = $request->input('active','F');
        if(isset($input_all['image'])&&isset($input_all['image'][0])){
            $input_all['image'] = $input_all['image'][0];
            unset($input_all['org_image']);
            if(Storage::disk("uploads")->exists("temp/".$input_all['image'])){
                if(Storage::disk("uploads")->exists("News/".$input_all['image'])){
                    Storage::disk("uploads")->delete("News/".$input_all['image']);
                }
                Storage::disk("uploads")->copy("temp/".$input_all['image'],"News/".$input_all['image']);

            }
        }
        if(isset($input_all['org_image'])){
            Storage::disk("uploads")->delete("temp/".$input_all['org_image']);
        }
        unset($input_all['org_image']);
        
        $input_all['updated_at'] = date('Y-m-d H:i:s');

        $validator = Validator::make($request->all(), [
            'name' => 'required',
             'title' => 'required',
             'news_date' => 'required',
             'active' => 'required',
             'image' => 'required',
             
        ]);

        if (!$validator->fails()) {

            \DB::beginTransaction();
            try {
                $data_insert = $input_all;
                
                \App\Models\News::where('id',$id)->update($data_insert);
                \DB::commit();
                $return['status'] = 1;
                $return['content'] = 'สำเร็จ';
            } catch (Exception $e) {
                \DB::rollBack();
                $return['status'] = 0;
                $return['content'] = trans('lang.unsuccessful').$e->getMessage();
            }
        }else{
            $return['status'] = 0;
        }
        $return['title'] = trans('lang.create_data');
        return json_encode($return);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();
        try {
            \App\Models\News::where('id',$id)->delete();
            \DB::commit();
            $return['status'] = 1;
            $return['content'] = 'สำเร็จ';
        } catch (Exception $e) {
            \DB::rollBack();
            $return['status'] = 0;
            $return['content'] = trans('lang.unsuccessful').$e->getMessage();
        }
        $return['title'] = 'ลบข้อมูล';
        return $return;
    }

    public function Lists(){
        $result = \App\Models\News::select();
        return \Datatables::of($result)

        ->editColumn('active', function($rec){
            if($rec->active == "T"){
                return '<span class="badge badge-success">'.trans('lang.status_open').'</span>';
            }else{
                return '<span class="badge badge-dunger">'.trans('lang.status_close').'</span>';
            }
        })
        
        ->addColumn('action',function($rec){
            $str='
                <button data-loading-text="<i class=\'fa fa-refresh fa-spin\'></i>" class="btn btn-xs btn-warning btn-condensed btn-edit btn-tooltip" data-rel="tooltip" data-id="'.$rec->id.'" title="'.trans('lang.edit').'">
                    <i class="ace-icon fa fa-edit bigger-120"></i>
                </button>
                <button  class="btn btn-xs btn-danger btn-condensed btn-delete btn-tooltip" data-id="'.$rec->id.'" data-rel="tooltip" title="'.trans('lang.delete').'">
                    <i class="ace-icon fa fa-trash bigger-120"></i>
                </button>
            ';
            return $str;
        })->rawColumns(['active', 'action'])->make(true);
    }

}
