<?php

namespace App\Http\Controllers\Admin;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Storage;

class ProductController extends Controller
{
    public function index()
    {
        $data['main_menu'] = 'Product';
        $data['sub_menu'] = 'Product';
        $data['title_page'] = trans('lang.product');
        $data['menus'] = \App\Models\AdminMenu::ActiveMenu()->get();
        $data['Containers'] = \App\Models\Container::get();
        $data['ProductTypes'] = \App\Models\ProductType::get();
        // return($data);
        return view('Admin.product', $data);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $input_all = $request->all();
        // $input_all['active'] = $request->input('active','F');
        $input_all['created_at'] = date('Y-m-d H:i:s');
        $input_all['updated_at'] = date('Y-m-d H:i:s');

        $validator = Validator::make($request->all(), [
            'name' => 'required',

        ]);
        if (!$validator->fails()) {
            \DB::beginTransaction();
            try {
                $data_insert = $input_all;
                \App\Models\Product::insert($data_insert);
                \DB::commit();
                $return['status'] = 1;
                $return['content'] = 'สำเร็จ';
            } catch (Exception $e) {
                \DB::rollBack();
                $return['status'] = 0;
                $return['content'] = trans('lang.unsuccessful').$e->getMessage();
            }
        }else{
            $return['status'] = 0;
        }
        $return['title'] = trans('lang.create_data');
        return json_encode($return);
    }

    public function show($id)
    {
       $result = \App\Models\Product::find($id);
       return json_encode($result);
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $input_all = $request->all();
        // $input_all['active'] = $request->input('active','F');
        $input_all['updated_at'] = date('Y-m-d H:i:s');

        $validator = Validator::make($request->all(), [
            'name' => 'required',

        ]);
        if (!$validator->fails()) {
            \DB::beginTransaction();
            try {
                $data_insert = $input_all;
                \App\Models\Product::where('id',$id)->update($data_insert);
                \DB::commit();
                $return['status'] = 1;
                $return['content'] = 'สำเร็จ';
            } catch (Exception $e) {
                \DB::rollBack();
                $return['status'] = 0;
                $return['content'] = trans('lang.unsuccessful').$e->getMessage();
            }
        }else{
            $return['status'] = 0;
        }
        $return['title'] = trans('lang.create_data');
        return json_encode($return);
    }

    public function destroy($id)
    {
        // \DB::beginTransaction();
        // try {
        //     \App\Models\product::where('id',$id)->delete();
        //     \DB::commit();
        //     $return['status'] = 1;
        //     $return['content'] = 'สำเร็จ';
        // } catch (Exception $e) {
        //     \DB::rollBack();
        //     $return['status'] = 0;
        //     $return['content'] = trans('lang.unsuccessful').$e->getMessage();
        // }
        // $return['title'] = 'ลบข้อมูล';
        // return $return;
    }

    public function Lists(Request $request)
    {
        $result = \App\Models\Product::select(
                'products.*'
                , 'product_types.name as product_type_name'
            )
            ->leftjoin('product_types', 'product_types.id', '=', 'products.product_type_id');
        return \Datatables::of($result)
            ->addColumn('action',function($rec){
                $str='
                    <button data-loading-text="<i class=\'fa fa-refresh fa-spin\'></i>" class="btn btn-xs btn-warning btn-condensed btn-edit btn-tooltip" data-rel="tooltip" data-id="'.$rec->id.'" title="'.trans('lang.edit').'">
                        <i class="ace-icon fa fa-edit bigger-120"></i>
                    </button>
                ';
                // <button  class="btn btn-xs btn-danger btn-condensed btn-delete btn-tooltip" data-id="'.$rec->id.'" data-rel="tooltip" title="'.trans('lang.delete').'">
                //     <i class="ace-icon fa fa-trash bigger-120"></i>
                // </button>
                return $str;
            })
            ->make(true);
    }
}
