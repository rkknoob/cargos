<?php

namespace App\Http\Controllers\Admin;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Storage;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['main_menu']      = 'User';
        $data['sub_menu']       = 'User';
        $data['title_page']     = trans('lang.user');
        $data['menus']          = \App\Models\AdminMenu::ActiveMenu()->get();
        $data['Perfixs']        = \App\Models\Perfix::get();
        $data['Provinces']      = \App\Models\Province::get();
        $data['ProductTypes']   = \App\Models\ProductType::whereIn('id', [5,6])->where('active', 'T')->get();

        return view('Admin.user',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input_all = $request->all();
        $check_email = \App\Models\User::where('email', $input_all['email'])->first();
        $check_customer_general_code = \App\Models\User::where('customer_general_code', $input_all['customer_general_code'])->first();

        if($check_email){
            $return['status'] = 0;
            $return['content'] = 'อีเมลนี้มีผู้ใช้งานแล้ว กรุณาตรวจสอบข้อมูลใหม่';
            $return['title'] = trans('lang.create_data');
            return $return;
        }

        if($check_customer_general_code){
            $return['status'] = 0;
            $return['content'] = 'รหัสลูกค้าทั่วไปซ้ำ กรุณาตรวจสอบข้อมูลใหม่';
            $return['title'] = trans('lang.create_data');
            return $return;
        }

        if($input_all['product_type_id'] == 6){
            $input_all['customer_general_code'] = $input_all['customer_general_code'].'B';
        }

        $input_all['password'] = \Hash::make($input_all['password']);
        $input_all['created_at'] = date('Y-m-d H:i:s');
        $input_all['updated_at'] = date('Y-m-d H:i:s');

        unset($input_all['password_confirmation']);

        $validator = Validator::make($request->all(), [

        ]);

        if (!$validator->fails()) {
            \DB::beginTransaction();
            try {
                $data_insert = $input_all;
                \App\Models\User::insert($data_insert);
                \DB::commit();
                $return['status'] = 1;
                $return['content'] = 'สำเร็จ';
            } catch (Exception $e) {
                \DB::rollBack();
                $return['status'] = 0;
                $return['content'] = trans('lang.unsuccessful').$e->getMessage();
            }
        }else{
            $return['status'] = 0;
        }
        $return['title'] = trans('lang.create_data');
        return json_encode($return);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = \App\Models\User::find($id);

        return json_encode($result);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input_all = $request->all();
        
        $check_email = \App\Models\User::where('email', $input_all['email'])->whereNotIn('id', [$id])->first();
        $check_customer_general_code = \App\Models\User::where('customer_general_code', $input_all['customer_general_code'])->whereNotIn('id', [$id])->first();

        if($check_email){
            $return['status'] = 0;
            $return['content'] = 'อีเมลนี้มีผู้ใช้งานแล้ว กรุณาตรวจสอบข้อมูลใหม่';
            $return['title'] = trans('lang.edit_data');
            return $return;
        }

        if($check_customer_general_code){
            $return['status'] = 0;
            $return['content'] = 'รหัสลูกค้าทั่วไปซ้ำ กรุณาตรวจสอบข้อมูลใหม่';
            $return['title'] = trans('lang.edit_data');
            return $return;
        }

        if($input_all['product_type_id'] == 6){
            $input_all['customer_general_code'] = $input_all['customer_general_code'].'B';
        }

        $input_all['updated_at'] = date('Y-m-d H:i:s');

        $validator = Validator::make($request->all(), [

        ]);

        if (!$validator->fails()) {
            \DB::beginTransaction();
            try {
                $data_insert = $input_all;
                \App\Models\User::where('id',$id)->update($data_insert);
                \DB::commit();
                $return['status'] = 1;
                $return['content'] = 'สำเร็จ';
            } catch (Exception $e) {
                \DB::rollBack();
                $return['status'] = 0;
                $return['content'] = trans('lang.unsuccessful').$e->getMessage();
            }
        }else{
            $return['status'] = 0;
        }
        $return['title'] = trans('lang.edit_data');
        return json_encode($return);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();
        try {
            \App\Models\User::where('id',$id)->delete();
            \DB::commit();
            $return['status'] = 1;
            $return['content'] = 'สำเร็จ';
        } catch (Exception $e) {
            \DB::rollBack();
            $return['status'] = 0;
            $return['content'] = trans('lang.unsuccessful').$e->getMessage();
        }
        $return['title'] = 'ลบข้อมูล';
        return $return;
    }

    public function Lists(){
        $result = \App\Models\User::select(
                'users.*'
                ,'product_types.name as customer_type'
            )
            ->leftjoin('product_types', 'product_types.id', '=', 'users.product_type_id');

        return \Datatables::of($result)

        ->addColumn('customer_name', function($rec){
            return $rec->firstname.' '.$rec->lastname;
        })

        ->addColumn('action',function($rec){
            $str='
                <button data-loading-text="<i class=\'fa fa-refresh fa-spin\'></i>" class="btn btn-xs btn-success btn-condensed btn-price btn-tooltip" data-rel="tooltip" data-id="'.$rec->id.'" title="'.trans('lang.manage_price').'">
                    <i class="ace-icon fa fa-cog bigger-120"></i>
                </button>
                <button data-loading-text="<i class=\'fa fa-refresh fa-spin\'></i>" class="btn btn-xs btn-info btn-condensed btn-address btn-tooltip" data-rel="tooltip" data-id="'.$rec->id.'" title="'.trans('lang.manage_address').'">
                    <i class="ace-icon fa fa-home bigger-120"></i>
                </button>
                <button data-loading-text="<i class=\'fa fa-refresh fa-spin\'></i>" class="btn btn-xs btn-default btn-condensed btn-change-password btn-tooltip" data-rel="tooltip" data-id="'.$rec->id.'" title="'.trans('lang.change_password').'">
                    <i class="ace-icon fa fa-lock bigger-120"></i>
                </button>
                <button data-loading-text="<i class=\'fa fa-refresh fa-spin\'></i>" class="btn btn-xs btn-warning btn-condensed btn-edit btn-tooltip" data-rel="tooltip" data-id="'.$rec->id.'" title="'.trans('lang.edit').'">
                    <i class="ace-icon fa fa-edit bigger-120"></i>
                </button>
                <button  class="btn btn-xs btn-danger btn-condensed btn-delete btn-tooltip" data-id="'.$rec->id.'" data-rel="tooltip" title="'.trans('lang.delete').'">
                    <i class="ace-icon fa fa-trash bigger-120"></i>
                </button>
            ';
            return $str;
        })->make(true);
    }

    public function ListsAddress(Request $request)
    {
        $id = $request->input('id');
        $result = \App\Models\UserAddress::with('Province', 'Amphure')->where('user_id', $id)->select();
        $result = \App\Models\UserAddress::select(
                'user_addresses.*'
                , 'amphures.amphure_name'
                , 'provinces.province_name'
            )
            ->leftjoin('amphures', 'amphures.id', '=', 'user_addresses.amphur_id')
            ->leftjoin('provinces', 'provinces.id', '=', 'user_addresses.province_id')
            ->where('user_addresses.user_id', $id);
        return \Datatables::of($result)

        ->addColumn('action',function($rec){
            $str='
                <button data-loading-text="<i class=\'fa fa-refresh fa-spin\'></i>" class="btn btn-xs btn-warning btn-condensed btn-address-edit btn-tooltip" data-rel="tooltip"
                    data-id="'.$rec->id.'"title="'.trans('lang.edit').'"><i class="ace-icon fa fa-edit bigger-120"></i>
                </button>
                <button  class="btn btn-xs btn-danger btn-condensed btn-address-delete btn-tooltip" data-id="'.$rec->id.'" data-rel="tooltip" title="'.trans('lang.delete').'">
                    <i class="ace-icon fa fa-trash bigger-120"></i>
                </button>
            ';
            return $str;
        })->rawColumns(['action'])->addIndexColumn()->make(true);
    }

    public function AddAddress(Request $request)
    {
        $input_all = $request->all();
        // dd($input_all);
        $input_all['created_at'] = date('Y-m-d H:i:s');
        $input_all['updated_at'] = date('Y-m-d H:i:s');

        $validator = Validator::make($request->all(), [

        ]);
        if($input_all['id']  == null){
            unset($input_all['id']);
        }
        if (!$validator->fails()) {
            \DB::beginTransaction();
            try {
                $data_insert = $input_all;
                if(isset($data_insert['id'])){
                    \App\Models\UserAddress::where('id',$data_insert['id'])->update($data_insert);
                }else{
                    \App\Models\UserAddress::insert($data_insert);
                }

                \DB::commit();
                $return['status'] = 1;
                $return['content'] = 'สำเร็จ';
            } catch (Exception $e) {
                \DB::rollBack();
                $return['status'] = 0;
                $return['content'] = trans('lang.unsuccessful').$e->getMessage();
            }
        }else{
            $return['status'] = 0;
        }
        $return['title'] = ''.trans('lang.create_data').'';
        return json_encode($return);
    }

    public function EditAddress($id)
    {
        $data['UserAddress'] = \App\Models\UserAddress::find($id);
        $data['Amphures']    = \App\Models\Amphure::where('province_id', $data['UserAddress']->province_id)->get();

        return $data;
    }

    public function AddressDelete($id)
    {
        \DB::beginTransaction();
        try {
            \App\Models\UserAddress::where('id',$id)->delete();
            \DB::commit();
            $return['status'] = 1;
            $return['content'] = 'สำเร็จ';
        } catch (Exception $e) {
            \DB::rollBack();
            $return['status'] = 0;
            $return['content'] = trans('lang.unsuccessful').$e->getMessage();
        }
        $return['title'] = 'ลบข้อมูล';
        return $return;
    }

    public function GetAddress($id)
    {
        $UserAddresses = \App\Models\UserAddress::with('Province', 'Amphure')->where('user_id', $id)->get();

        return $UserAddresses;
    }

    public function AddPrice(Request $request)
    {
        $input_all = $request->all();

        $input_all['created_at'] = date('Y-m-d H:i:s');
        $input_all['updated_at'] = date('Y-m-d H:i:s');

        $validator = Validator::make($request->all(), [

        ]);

        if($input_all['id']  == null){
            unset($input_all['id']);
        }
        if (!$validator->fails()) {
            \DB::beginTransaction();
            try {
                $data_insert = $input_all;
                if(isset($data_insert['id'])){
                    \App\Models\RelUserTypeProduct::where('id',$data_insert['id'])->update($data_insert);
                }else{
                    \App\Models\RelUserTypeProduct::insert($data_insert);
                }

                \DB::commit();
                $return['status'] = 1;
                $return['content'] = 'สำเร็จ';
            } catch (Exception $e) {
                \DB::rollBack();
                $return['status'] = 0;
                $return['content'] = trans('lang.unsuccessful').$e->getMessage();
            }
        }else{
            $return['status'] = 0;
        }
        $return['title'] = ''.trans('lang.create_data').'';
        return json_encode($return);
    }

    public function ListsPrice(Request $request)
    {
        $id = $request->input('id');
        $result = \App\Models\RelUserTypeProduct::select('product_types.name as product_type_name'
                                                        ,'rel_user_type_products.id as id'
                                                        ,'rel_user_type_products.price as price')
                                                    ->join('product_types', 'product_types.id', '=', 'rel_user_type_products.product_type_id')
                                                    ->where('user_id', $id);
        return \Datatables::of($result)

        ->editColumn('price', function($rec){
            return number_format($rec->price, 2);
        })

        ->addColumn('action',function($rec){
            $str='
                <button data-loading-text="<i class=\'fa fa-refresh fa-spin\'></i>" class="btn btn-xs btn-warning btn-condensed btn-price-edit btn-tooltip" data-rel="tooltip"
                    data-id="'.$rec->id.'"title="แก้ไข"><i class="ace-icon fa fa-edit bigger-120"></i>
                </button>
                <button  class="btn btn-xs btn-danger btn-condensed btn-price-delete btn-tooltip" data-id="'.$rec->id.'" data-rel="tooltip" title="ลบ">
                    <i class="ace-icon fa fa-trash bigger-120"></i>
                </button>
            ';
            return $str;
        })->rawColumns(['action'])->addIndexColumn()->make(true);
    }

    public function EditPrice($id)
    {
        $data['RelUserTypeProduct'] = \App\Models\RelUserTypeProduct::find($id);

        return $data;
    }

    public function PriceDelete($id)
    {
        \DB::beginTransaction();
        try {
            \App\Models\RelUserTypeProduct::where('id',$id)->delete();
            \DB::commit();
            $return['status'] = 1;
            $return['content'] = 'สำเร็จ';
        } catch (Exception $e) {
            \DB::rollBack();
            $return['status'] = 0;
            $return['content'] = trans('lang.unsuccessful').$e->getMessage();
        }
        $return['title'] = 'ลบข้อมูล';
        return $return;
    }

    public function change_password(Request $request, $id)
    {
        $password = $request->input('password');
        $new_password = \Hash::make($password);

        $validator = Validator::make($request->all(), [
            'password' => 'required'
        ]);
        if (!$validator->fails()) {
            \DB::beginTransaction();
            try {
                $data_update = [
                    'password'=>$new_password
                ];
                \App\Models\User::where('id',$id)->update($data_update);
                \DB::commit();
                $return['status'] = 1;
                $return['content'] = 'Finish';
            } catch (Exception $e) {
                \DB::rollBack();
                $return['status'] = 0;
                $return['content'] = 'Fail'.$e->getMessage();
            }
        }else{
            $return['status'] = 0;
        }
        $return['title'] = ''.trans('lang.change_password').'';
        return json_encode($return);
    }

}
