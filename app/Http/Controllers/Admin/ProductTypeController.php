<?php

namespace App\Http\Controllers\Admin;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Storage;
class ProductTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['main_menu'] = 'Master';
        $data['sub_menu'] = 'ProductType';
        $data['title_page'] = trans('lang.product_category');
        $data['menus'] = \App\Models\AdminMenu::ActiveMenu()->get();

        return view('Admin.product_type',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input_all = $request->all();
        $input_all['active'] = $request->input('active','F');
        $input_all['created_at'] = date('Y-m-d H:i:s');
        $input_all['updated_at'] = date('Y-m-d H:i:s');

        $validator = Validator::make($request->all(), [
            'code' => 'required',
             'name' => 'required',

        ]);
        if (!$validator->fails()) {
            \DB::beginTransaction();
            try {
                $data_insert = $input_all;
                \App\Models\ProductType::insert($data_insert);
                \DB::commit();
                $return['status'] = 1;
                $return['content'] = trans('lang.successfully_added');
            } catch (Exception $e) {
                \DB::rollBack();
                $return['status'] = 0;
                $return['content'] = trans('lang.unsuccessful').$e->getMessage();
            }
        }else{
            $return['status'] = 0;
        }
        $return['title'] = trans('lang.create_data');
        return json_encode($return);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = \App\Models\ProductType::find($id);

        return json_encode($result);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input_all = $request->all();
        $input_all['active'] = $request->input('active','F');
        $input_all['updated_at'] = date('Y-m-d H:i:s');

        $validator = Validator::make($request->all(), [
            'code' => 'required',
             'name' => 'required',

        ]);
        if (!$validator->fails()) {
            \DB::beginTransaction();
            try {
                $data_insert = $input_all;
                \App\Models\ProductType::where('id',$id)->update($data_insert);
                \DB::commit();
                $return['status'] = 1;
                $return['content'] = 'สำเร็จ';
            } catch (Exception $e) {
                \DB::rollBack();
                $return['status'] = 0;
                $return['content'] = trans('lang.unsuccessful').$e->getMessage();
            }
        }else{
            $return['status'] = 0;
        }
        $return['title'] = trans('lang.edit_data');
        return json_encode($return);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();
        try {
            \App\Models\ProductType::where('id',$id)->delete();
            \DB::commit();
            $return['status'] = 1;
            $return['content'] = 'สำเร็จ';
        } catch (Exception $e) {
            \DB::rollBack();
            $return['status'] = 0;
            $return['content'] = trans('lang.unsuccessful').$e->getMessage();
        }
        $return['title'] = trans('lang.delete');
        return $return;
    }

    public function Lists(){
        $result = \App\Models\ProductType::whereNotIn('id', [5,6])->select();
        return \Datatables::of($result)

        ->editColumn('active', function($rec){
            if($rec->active == "T"){
                return '<span class="badge badge-success">'.trans('lang.status_open').'</span>';
            }else{
                return '<span class="badge badge-dunger">'.trans('lang.status_close').'</span>';
            }
        })

        ->addColumn('id', function($rec){
            return '<input type="checkbox" class="checkbox-product-type" name="product_type_id[]" value="'.$rec->id.'" />';
        })

        ->addColumn('action', function($rec){
            $str='
                <button data-loading-text="<i class=\'fa fa-refresh fa-spin\'></i>" class="btn btn-xs btn-warning btn-condensed btn-edit btn-tooltip" data-rel="tooltip" data-id="'.$rec->id.'" title="'.trans('lang.edit').'">
                    <i class="ace-icon fa fa-edit bigger-120"></i>
                </button>
                <button  class="btn btn-xs btn-danger btn-condensed btn-delete btn-tooltip" data-id="'.$rec->id.'" data-rel="tooltip" title="'.trans('lang.delete').'">
                    <i class="ace-icon fa fa-trash bigger-120"></i>
                </button>
            ';
            return $str;
        })->rawColumns(['active', 'action', 'id'])->make(true);
    }

    public function DeleteAll(Request $request)
    {
        $id = $request->input('id');
        if($request->has('status')){
            \DB::beginTransaction();
            try {
                \App\Models\ProductType::whereIn('id',$id)->delete();
                \DB::commit();
                $return['status'] = 1;
                $return['content'] = 'สำเร็จ';
            } catch (Exception $e) {
                \DB::rollBack();
                $return['status'] = 0;
                $return['content'] = trans('lang.unsuccessful').$e->getMessage();
            }
            $return['title'] = trans('lang.delete');
        }else{
            $return['status'] = 0;
            $return['content'] = 'ไม่สำเร็จ กรุณาเลือกการดำเนินการก่อน';
            $return['title'] = trans('lang.delete');
        }

        return $return;
    }

}
