<?php

namespace App\Http\Controllers\Admin;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \Mpdf\Mpdf;
use Storage;
class DeliveryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['main_menu'] = 'Delivery';
        $data['sub_menu'] = 'Delivery';
        $data['title_page'] = 'ออกรายการส่งของ (ไทย)';
        $data['menus'] = \App\Models\AdminMenu::ActiveMenu()->get();
        $data['Users']    = \App\Models\User::get();

        return view('Admin.delivery',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['main_menu'] = 'Delivery';
        $data['sub_menu'] = 'Delivery';
        $data['title_page'] = 'ออกรายการส่งของ (ไทย)';
        $data['menus'] = \App\Models\AdminMenu::ActiveMenu()->get();

        return view('Admin.delivery_create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input_all = $request->all();

        $input_all['delivery']['status_id']  = 1;
        $input_all['delivery']['created_at'] = date('Y-m-d H:i:s');
        $input_all['delivery']['updated_at'] = date('Y-m-d H:i:s');

        $validator = Validator::make($request->all(), [

        ]);

        if (!$validator->fails()) {
            \DB::beginTransaction();
            try {
                $data_insert1 = $input_all['delivery'];
                dd($data_insert1);
                $delivery_id = \App\Models\Delivery::insertGetId($data_insert1);
                if(isset($input_all['delivery_to_user'])){
                    foreach($input_all['delivery_to_user'] as $delivery_to_user){
                        $input_all2['user_id']              = $delivery_to_user['user_id'];
                        $input_all2['import_to_chaina_id']  = $delivery_to_user['import_to_chaina_id'];
                        $input_all2['delivery_id']          = $delivery_id;
                        $input_all2['doc_no']               = null;
                        $input_all2['recive_date']          = date('Y-m-d');
                        $input_all2['created_at']           = date('Y-m-d H:i:s');
                        $input_all2['updated_at']           = date('Y-m-d H:i:s');

                        $data_insert2 = $input_all2;
                        $delivery_to_user_id = \App\Models\DeliveryToUser::insertGetId($data_insert2);
                        \App\Models\ImportToChaina::where('id', $delivery_to_user['import_to_chaina_id'])->update(['status_id' => 5]);

                        if(isset($delivery_to_user['qr_code_product_id'])){
                            foreach($delivery_to_user['qr_code_product_id'] as $delivery_product){
                                $input_all3['delivery_to_user_id']  = $delivery_to_user_id;
                                $input_all3['qr_code_product_id']   = $delivery_product;
                                $input_all3['created_at']           = date('Y-m-d H:i:s');
                                $input_all3['updated_at']           = date('Y-m-d H:i:s');

                                $data_insert3 = $input_all3;
                                \App\Models\DeliveryProduct::insert($data_insert3);
                            }
                        }
                    }
                }else{
                    $return['status'] = 0;
                }
                \DB::commit();
                $return['status'] = 1;
                $return['content'] = 'สำเร็จ';
            } catch (Exception $e) {
                \DB::rollBack();
                $return['status'] = 0;
                $return['content'] = 'ไม่สำเร็จ'.$e->getMessage();
            }
        }else{
            $return['status'] = 0;
        }
        $return['title'] = 'เพิ่มข้อมูล';
        return json_encode($return);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = \App\Models\Delivery::find($id);

        return json_encode($result);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input_all = $request->all();

        $input_all['updated_at'] = date('Y-m-d H:i:s');

        $validator = Validator::make($request->all(), [
            'plate_no' => 'required',
             'date_delivery' => 'required',
             'status_id' => 'required',

        ]);
        if (!$validator->fails()) {
            \DB::beginTransaction();
            try {
                $data_insert = $input_all;
                \App\Models\Delivery::where('id',$id)->update($data_insert);
                \DB::commit();
                $return['status'] = 1;
                $return['content'] = 'สำเร็จ';
            } catch (Exception $e) {
                \DB::rollBack();
                $return['status'] = 0;
                $return['content'] = 'ไม่สำเร็จ'.$e->getMessage();
            }
        }else{
            $return['status'] = 0;
        }
        $return['title'] = 'เพิ่มข้อมูล';
        return json_encode($return);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();
        try {
            \App\Models\Delivery::where('id',$id)->delete();
            \DB::commit();
            $return['status'] = 1;
            $return['content'] = 'สำเร็จ';
        } catch (Exception $e) {
            \DB::rollBack();
            $return['status'] = 0;
            $return['content'] = 'ไม่สำเร็จ'.$e->getMessage();
        }
        $return['title'] = 'ลบข้อมูล';
        return $return;
    }

    public function Lists(){
        $result = \App\Models\Delivery::with('DeliveryStatus')->select();
        return \Datatables::of($result)

        ->editColumn('status_id', function($rec){
            return $rec->DeliveryStatus ? $rec->DeliveryStatus->name : null;
        })

        ->addColumn('action',function($rec){
            // <button  class="btn btn-xs btn-danger btn-condensed btn-delete btn-tooltip" data-id="'.$rec->id.'" data-rel="tooltip" title="ลบ">
            //     <i class="ace-icon fa fa-trash bigger-120"></i>
            // </button>
            if($rec->status_id == 2){
                $disabled = 'disabled';
            }else{
                $disabled = '';
            }
            $str='
                <button style="border-color: #364150;" class="btn btn-xs btn-condensed btn-change-delivery-slip btn-tooltip" data-id="'.$rec->id.'" data-rel="tooltip" title="เปลี่ยนสถานะการจัดส่งของใบส่งสินค้า" '.$disabled.'>
                    <i class="ace-icon fa fa-cog bigger-120"></i>
                </button>
                <button  class="btn btn-xs btn-success btn-condensed btn-ststus btn-tooltip" data-id="'.$rec->id.'" data-rel="tooltip" title="เปลี่ยนสถานะการจัดส่งของรถ" '.$disabled.'>
                    <i class="ace-icon fa fa-truck bigger-120"></i>
                </button>
                <a href="'.url('/admin/Shipping/'.$rec->id).'" data-loading-text="<i class=\'fa fa-refresh fa-spin\'></i>" class="btn btn-xs btn-info btn-condensed btn-tooltip" data-rel="tooltip" data-id="'.$rec->id.'" title="ออกใบส่งสินค้า">
                    <i class="ace-icon fa fa-list bigger-120"></i>
                </a>
                <a href="'.url('/admin/Delivery/Detail/'.$rec->id).'" data-loading-text="<i class=\'fa fa-refresh fa-spin\'></i>" class="btn btn-xs btn-warning btn-condensed btn-tooltip" data-rel="tooltip" data-id="'.$rec->id.'" title="ดูรายละเอียด">
                    <i class="ace-icon fa fa-search bigger-120"></i>
                </a>
            ';
            return $str;
        })->make(true);
    }

    public function Detail($id)
    {
        $data['main_menu'] = 'Delivery';
        $data['sub_menu'] = 'Delivery';
        $data['title_page'] = 'ออกรายการส่งของ (ไทย)';
        $data['menus'] = \App\Models\AdminMenu::ActiveMenu()->get();

        $data['Delivery'] = \App\Models\Delivery::find($id);
        $data['ImportToChainas'] = \App\Models\ImportToChaina::select(
                                            'import_to_chaina.id as id'
                                            ,'import_to_chaina.user_id as user_id'
                                            ,'import_to_chaina.created_at as import_to_chaina_date'
                                            ,'import_to_thai.created_at as import_to_thai_date'
                                            ,'import_to_chaina.po_no as po_no'
                                            ,'delivery_to_users.created_at as delivery_date'
                                            ,\DB::raw('(select sum(qty) from tb_product_import_to_chaina
                                                            where tb_product_import_to_chaina.import_to_chaina_id = tb_import_to_chaina.id
                                                        ) as qty_chaina')
                                            ,\DB::raw('(select sum(weight_all) from tb_product_import_to_chaina
                                                            where tb_product_import_to_chaina.import_to_chaina_id = tb_import_to_chaina.id
                                                        ) as weight_all')
                                            ,\DB::raw('(select sum(cubic) from tb_product_import_to_chaina
                                                            where tb_product_import_to_chaina.import_to_chaina_id = tb_import_to_chaina.id
                                                        ) as cubic')
                                            ,\DB::raw('(select count(*) from tb_import_to_thai
                                                            join tb_product_import_to_thai on tb_product_import_to_thai.import_to_thai_id = tb_import_to_thai.id
                                                            where tb_import_to_thai.import_to_chaina_id = tb_import_to_chaina.id
                                                        ) as qty_thai')
                                            ,\DB::raw('(select count(*) from tb_qr_code_products
                                                            join tb_rel_container_products on tb_rel_container_products.qr_code_product_id = tb_qr_code_products.id
                                                            where tb_qr_code_products.import_to_chaina_id = tb_import_to_chaina.id
                                                        ) as qty_container')
                                            ,\DB::raw('(select count(*) from tb_delivery_to_users
                                                            join tb_delivery_products on tb_delivery_products.delivery_to_user_id = tb_delivery_to_users.id
                                                            where tb_delivery_to_users.import_to_chaina_id = tb_import_to_chaina.id
                                                        ) as qty_delivery')
                                            )
                                        ->join('delivery_to_users', 'delivery_to_users.import_to_chaina_id','=' , 'import_to_chaina.id')
                                        ->join('import_to_thai', 'import_to_thai.import_to_chaina_id', '=', 'import_to_chaina.id')
                                        ->groupBy('import_to_chaina.id')
                                        ->groupBy('import_to_chaina.user_id')
                                        ->groupBy('import_to_chaina.created_at')
                                        ->groupBy('import_to_chaina.po_no')
                                        ->groupBy('import_to_thai.created_at')
                                        ->groupBy('delivery_to_users.created_at')
                                        ->groupBy('delivery_to_users.created_at')
                                        ->where('delivery_to_users.delivery_id', '=', $id)->get();

        $data['ProductImportToChainas'] = \App\Models\ProductImportToChaina::select(
                                            'product_import_to_chaina.id as product_id'
                                            ,'users.customer_general_code as customer_general_code'
                                            ,'product_types.name as product_type_name'
                                            ,'products.name as product_name'
                                            ,'product_import_to_chaina.qty as product_qty'
                                            ,'product_import_to_chaina.cubic as cubic'
                                            ,'product_import_to_chaina.weight_per_item as weight_per_item'
                                            ,'import_to_chaina.po_no as po_no'
                                            ,'import_to_thai.date_import as import_date'
                                            ,\DB::raw('(select count(*) from tb_qr_code_products
                                                            join tb_delivery_products on tb_delivery_products.qr_code_product_id = tb_qr_code_products.id
                                                            where tb_qr_code_products.product_import_to_chaina_id = tb_product_import_to_chaina.id
                                                        ) as qty_delivery')
                                            )
                                        ->join('qr_code_products', 'qr_code_products.product_import_to_chaina_id','=' , 'product_import_to_chaina.id')
                                        ->join('delivery_products', 'delivery_products.qr_code_product_id', '=', 'qr_code_products.id')
                                        ->join('delivery_to_users', 'delivery_to_users.id', '=', 'delivery_products.delivery_to_user_id')
                                        ->join('users', 'users.id', '=', 'delivery_to_users.user_id')
                                        ->join('products', 'products.id', '=', 'product_import_to_chaina.product_id')
                                        ->join('product_types', 'product_types.id', '=', 'products.product_type_id')
                                        ->join('import_to_chaina', 'import_to_chaina.id', '=', 'product_import_to_chaina.import_to_chaina_id')
                                        ->join('import_to_thai', 'import_to_thai.import_to_chaina_id','=' , 'import_to_chaina.id')
                                        ->groupBy('product_import_to_chaina.id')
                                        ->groupBy('users.customer_general_code')
                                        ->groupBy('product_types.name')
                                        ->groupBy('products.name')
                                        ->groupBy('product_import_to_chaina.qty')
                                        ->groupBy('import_to_chaina.po_no')
                                        ->groupBy('import_to_thai.date_import')
                                        ->groupBy('product_import_to_chaina.cubic')
                                        ->groupBy('product_import_to_chaina.weight_per_item')
                                        ->where('delivery_to_users.delivery_id', '=', $id)->get();

        $data['DeliveryProducts'] = \App\Models\DeliveryProduct::select(
                                            'product_import_to_chaina.id as product_id'
                                            ,'import_to_chaina.po_no as po_no'
                                            ,'qr_code_products.qr_code as qr_code'
                                            ,'users.customer_general_code as customer_general_code'
                                            ,'product_types.name as product_type_name'
                                            ,'products.name as product_name'
                                            ,'import_to_thai.date_import as date_import'
                                             )
                                        ->join('delivery_to_users', 'delivery_to_users.id','=' , 'delivery_products.delivery_to_user_id')
                                        ->join('qr_code_products', 'qr_code_products.id','=' , 'delivery_products.qr_code_product_id')
                                        ->join('product_import_to_chaina', 'product_import_to_chaina.id','=' , 'qr_code_products.product_import_to_chaina_id')
                                        ->join('import_to_chaina', 'import_to_chaina.id','=' , 'product_import_to_chaina.import_to_chaina_id')
                                        ->join('users', 'users.id','=' , 'delivery_to_users.user_id')
                                        ->join('products', 'products.id','=' , 'product_import_to_chaina.product_id')
                                        ->join('product_types', 'product_types.id','=' , 'products.product_type_id')
                                        ->join('import_to_thai', 'import_to_thai.import_to_chaina_id','=' , 'import_to_chaina.id')
                                        ->where('delivery_to_users.delivery_id', '=', $id)->get();

        //dd($data);
        return view('Admin.delivery_detail',$data);
    }

    public function CheckQrCode(Request $request)
    {
        $input_all      = $request->all();
        $qr_code        = $input_all['qr_code'];

        $QrCodeProduct = \App\Models\QrCodeProduct::where('qr_code', $qr_code)->first();

        if($QrCodeProduct){
            $DeliveryProduct = \App\Models\DeliveryProduct::where('qr_code_product_id', $QrCodeProduct->id)->first();
            if($DeliveryProduct == null){
                $return['ProductImportToThai'] = \App\Models\ProductImportToThai::where('qr_code_product_id', $QrCodeProduct->id)->first();
                if($return['ProductImportToThai']){
                    $return['QrCodeProduct'] = \App\Models\QrCodeProduct::with('ImportToChaina.User', 'ProductImportToChaina.LotProduct', 'ProductImportToChaina.Product.ProductType')->where('id', $return['ProductImportToThai']->qr_code_product_id)->where('qr_code', $qr_code)->first();
                    if($return['QrCodeProduct']){
                        $ProductImportToChainas     = \App\Models\ProductImportToChaina::where('import_to_chaina_id', $return['QrCodeProduct']->import_to_chaina_id)->get();
                        $ImportToThais = \App\Models\ImportToThai::where('import_to_chaina_id', $return['QrCodeProduct']->import_to_chaina_id)->get();
                        $ImportToThaiId = [];
                        foreach($ImportToThais as $ImportToThai){
                            $ImportToThaiId[] = $ImportToThai->id;
                        }
                        $ProductImportToThais = \App\Models\ProductImportToThai::whereIn('import_to_thai_id', $ImportToThaiId)->count();
                        $return['qty_po_chaina']           = $ProductImportToChainas->sum('qty');
                        $return['qty_po_thai']              = $ProductImportToThais;
                        $return['status'] = 1;
                        return $return;
                    }else{
                        $return['status'] = 0;
                        $return['content'] = 'Barcode สินค้าไม่ถูกต้องกรุณาลองใหม่';
                        $return['title'] = 'Barcode สินค้า';
                        return $return;
                    }
                }else{
                    $return['status'] = 0;
                    $return['content'] = 'Barcode สินค้าไม่ถูกต้องกรุณาลองใหม่';
                    $return['title'] = 'Barcode สินค้า';
                    return $return;
                }
            }
        }

        $return['status'] = 0;
        $return['content'] = 'Barcode สินค้าไม่ถูกต้องกรุณาลองใหม่';
        $return['title'] = 'Barcode สินค้า';

        return $return;
    }

    public function GetStatus(Request $request, $id)
    {
        \DB::beginTransaction();
        try {
            if($request->has('active')){
                \App\Models\Delivery::where('id', $id)->update(['status_id' => 2]);
                \DB::commit();
                $return['status'] = 1;
                $return['content'] = 'สำเร็จ';
            }else{
                $return['status'] = 0;
                $return['content'] = 'ไม่สำเร็จ';
            }

        } catch (Exception $e) {
            \DB::rollBack();
            $return['status'] = 0;
            $return['content'] = 'ไม่สำเร็จ'.$e->getMessage();
        }
        $return['title'] = 'สถานะจัดส่งสินค้า';
        return $return;
    }

    public function ShowDeliverySlip($id)
    {
        $return['DeliverySlips'] = \App\Models\DeliverySlip::select(
                                                    'delivery_slips.id as id'
                                                    ,'delivery_slips.delivery_slip_no as delivery_slip_no'
                                                    ,'delivery_slips.delivery_slip_date as delivery_slip_date'
                                                    ,'delivery_slips.total_qty as total_qty'
                                                    ,'delivery_slips.total_weight as total_weight'
                                                    ,'delivery_slips.price as price'
                                                    ,'delivery_slips.delivery_charge_price as delivery_charge_price'
                                                    ,'delivery_slips.total_price as total_price'
                                                    ,'delivery_slips.status_id as status_id'
                                                    ,'users.firstname as firstname'
                                                    ,'users.lastname as lastname'
                                                    ,'delivery_slip_statuses.name as delivery_slip_status_name')
                                    ->join('users', 'users.id', '=', 'delivery_slips.user_id')
                                    ->join('delivery_slip_statuses', 'delivery_slip_statuses.id', '=', 'delivery_slips.status_id')
                                    ->where('delivery_id', $id)
                                    ->get();

        return $return;
    }

    public function GetStatusDeliverySlips(Request $request)
    {
        $input_all = $request->all();

        if(isset($input_all['delivery_slip_id'])){
            \App\Models\DeliverySlip::whereIn('id', $input_all['delivery_slip_id'])->update(['status_id' => 2]);
            $return['status'] = 1;
            $return['content'] = 'สำเร็จ';
        }else{
            $return['status'] = 0;
            $return['content'] = 'ไม่สำเร็จ';
        }

        $return['title'] = 'สถานะจัดส่งสินค้าเรียบร้อย';
        return $return;
    }
}
