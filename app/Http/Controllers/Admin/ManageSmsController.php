<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ManageSmsController extends Controller
{

    public function SendSMS($po_no = NULL, $status = NULL, $amount = NULL, $msisdn = NULL)
    {
        return 1;
        $sms = app('App\Http\Controllers\Admin\SmsController'); // controller ส่งข้อความ
        // สิ่งที่ต้องส่งมามี  เลข po สถานะ จำนวน ถ้ามี
        // $po_no = 'PO-32165252';
        // $status = 5;
        // $amount = $request->input('amount', 30);
        // set data for send sms
        $username = '0858178800';
        $password = '655557';
        $msisdn = $msisdn;
        $message = '';
        $sender = "NewCargo";
        $ScheduledDelivery = "";
        $force = "premium";
        switch ($status) {
            case 1:
                $message = 'ทาง NewCargothai.com ได้รับสินค้าของท่าน(PO:'.$po_no.')เข้าโกดังจีนจำนวน '.$amount.' แพ็คเกจ ท่านสามารถดูรายละเอียดเพิ่มเติมได้ที่เว็ปไซด์ด้านบนค่ะ';
                break;
            case 2:
                $message = 'สินค้าของท่าน (PO:'.$po_no.'/'.$amount.'แพ็คเกจ)ออกจากโกดังจีนแล้ว กำลังเดินทางมาไทยค่ะ หากสินค้าถึงโกดังไทยแล้วจะมีพนักงานโทรไปนัดวันเวลาส่งของภายใน24ชม.ค่ะ';
                break;
            case 3:
                $message = '';
                break;
            case 4:
                $message = 'New Cargo กำลังจัดส่งสินค้า(PO:'.$po_no.')ไปตามที่อยู่ที่ท่านแจ้งค่ะ';
                break;
            case 5:
                $message = '(PO:'.$po_no.') จัดส่งเรียบร้อย เพื่อเป็นการพัฒนาการบริการในอนาคต รบกวนลูกค้าให้คะแนนความพึงพอใจบริการของบริษัทที่นี่ค่ะ(https://goo.gl/forms/tIPtEZvuWd5CZ3PF2)';
                break;
        }
        // return $message;
        // end set data for send sms
        // return $sms->check_credit($username,$password,$credit_type = "credit_remain_premium");
        if( !empty($po_no) && !empty($status) && !empty($msisdn) ){
            if( !empty($message) ){
                return $sms->send_sms($username, $password, $msisdn, $message, $sender, $ScheduledDelivery, $force);
            }
        }
    }

}
