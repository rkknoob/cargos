<?php

namespace App\Http\Controllers\Admin;

use Validator;
use DB;
use \Mpdf\Mpdf;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DeliveryContainerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['main_menu'] = 'DeliveryContainer';
        $data['sub_menu'] = 'DeliveryContainer';
        $data['title_page'] = trans('lang.list_invoice');
        $data['menus'] = \App\Models\AdminMenu::ActiveMenu()->get();
        $data['Users']    = \App\Models\User::get();

        return view('Admin.delivery_container_all',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['main_menu'] = 'DeliveryContainer';
        $data['sub_menu'] = 'DeliveryContainer';
        $data['title_page'] = trans('lang.list_invoice');
        $data['menus'] = \App\Models\AdminMenu::ActiveMenu()->get();
        $data['Containers'] = \App\Models\Container::whereNotIn('status_id', [4])->get();

        return view('Admin.delivery_container_create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input_all = $request->all();

        $input_all['delivery']['created_at'] = date('Y-m-d H:i:s');
        $input_all['delivery']['updated_at'] = date('Y-m-d H:i:s');

        $container_id   = $input_all['delivery']['container_id'];
        $GetIdUsers     = json_decode($input_all['send_order']);
        $user_prices    = $input_all['price'];
        unset($input_all['send_order']);
        unset($input_all['price']);

        $validator = Validator::make($request->all(), [

        ]);

        if (!$validator->fails()) {
            \DB::beginTransaction();
            try {
                $data_insert = $input_all['delivery'];
                $dalivery_by_container_id = \App\Models\DaliveryByContainer::insertGetId($data_insert);

                if(isset($user_prices)){

                    foreach($user_prices as $key => $import_to_chaina_prices){
                        $user = \App\Models\User::with('Amphure')->where('id', $key)->first();

                        $input_all1['delivery_no']                  = $this->runCode();
                        $input_all1['delivery_slip_date']           = date('Y-m-d');
                        $input_all1['user_id']                      = $user['id'];
                        $input_all1['dalivery_by_containers_id']    = $dalivery_by_container_id;
                        $input_all1['address']                      = $user['address'];
                        $input_all1['amphur_id']                    = $user['amphur_id'];
                        $input_all1['province_id']                  = $user['province_id'];
                        $input_all1['zipcode']                      = $user->Amphure ? $user->Amphure->zipcode : null;
                        $input_all1['email']                        = $user['email'];
                        $input_all1['main_mobile']                  = $user['main_mobile'];
                        $input_all1['total_price']                  = 0;
                        $input_all1['total_qty']                    = 0;
                        $input_all1['total_cubic']                  = 0;
                        $input_all1['total_weight']                 = 0;
                        $input_all1['created_at']                   = date('Y-m-d H:i:s');
                        $input_all1['updated_at']                   = date('Y-m-d H:i:s');

                        $data_insert1 = $input_all1;
                        $dalivery_sheet_id = \App\Models\DaliverySheet::insertGetId($data_insert1);

                        if(isset($import_to_chaina_prices)){
                            $total_all_price = 0;
                            $total_qty = 0;
                            $total_weight = 0;
                            $total_cubic  = 0;
                            foreach ($import_to_chaina_prices as $i => $rate_prices) {
                                $QrCodeProduct = \App\Models\QrCodeProduct::select(
                                            DB::raw('count(`tb_qr_code_products`.`id`) as amount_product')
                                            ,DB::raw('sum(`tb_lot_products`.`weight_per_item`) as weight_product')
                                            ,DB::raw('sum(
                                                    if(`tb_rel_user_type_products`.`price`,`tb_rel_user_type_products`.`price`,0)
                                                ) as price')
                                            ,DB::raw('sum(
                                                    if(`tb_lot_products`.`cubic`,`tb_lot_products`.`cubic`,0)
                                                ) as cubic_product')
                                        )
                                        ->join('rel_container_products', 'rel_container_products.qr_code_product_id', '=', 'qr_code_products.id')
                                        ->leftjoin('rel_user_type_products', 'rel_user_type_products.id', '=', 'qr_code_products.rel_user_type_product_id')
                                        ->join('product_import_to_chaina', 'product_import_to_chaina.id', '=', 'qr_code_products.product_import_to_chaina_id')
                                        ->join('lot_products', 'lot_products.product_import_to_chaina_id', '=', 'qr_code_products.product_import_to_chaina_id')
                                        ->whereRaw('`tb_qr_code_products`.`sort_id` BETWEEN `tb_lot_products`.`product_sort_start` AND `tb_lot_products`.`product_sort_end`')
                                        ->where('qr_code_products.import_to_chaina_id', $i)
                                        ->first();

                                $input_all2['import_to_chaina_id']          = intval($i);
                                $input_all2['delivery_sheet_id']            = $dalivery_sheet_id;
                                $input_all2['qty']                          = $QrCodeProduct['amount_product'];
                                $input_all2['weight']                       = $QrCodeProduct['weight_product'];
                                $input_all2['cubic']                        = $QrCodeProduct['cubic_product'];
                                $input_all2['created_at']                   = date('Y-m-d H:i:s');
                                $input_all2['updated_at']                   = date('Y-m-d H:i:s');

                                $total_qty      += $QrCodeProduct['amount_product'];
                                $total_weight   += $QrCodeProduct['weight_product'];
                                $total_cubic    += $QrCodeProduct['cubic_product'];

                                $data_insert2 = $input_all2;
                                $delivery_pos_id = \App\Models\DeliveryPos::insertGetId($data_insert2);
                                // \App\Models\ImportToChaina::where('id',$input_all2['import_to_chaina_id'])->update(['status_id' => 4]);


                                if(isset($rate_prices)){
                                    $total_price = 0;
                                    foreach($rate_prices as $qrcode_product_id => $price){
                                        if($price['rate'] == 'null'){
                                            $return['status'] = 0;
                                            $return['content'] = 'กรุณาใส่ Rate ราคาของสินค้าให้ครบ';
                                            $return['title'] = trans('lang.create_data').' '.trans('lang.unsuccessful');

                                            return $return;
                                        }else{
                                            $weight      = $price['weight'] != 'null' ? $price['weight'] : 0;
                                            $rate        = $price['rate'] != 'null' ? $price['rate'] : 0;
                                            $total       = $weight * $rate;
                                            $total_price += $total;

                                            \App\Models\QrCodeProduct::where('id', $qrcode_product_id)->update(['delivery_status' => 'T']);
                                        }
                                    }

                                    \App\Models\DeliveryPos::where('id', $delivery_pos_id)->update(['price' => $total_price]);
                                }

                                $total_all_price += $total_price;

                            }

                            $input_all3['total_price']  = $total_all_price;
                            $input_all3['total_qty']    = $total_qty;
                            $input_all3['total_cubic']  = $total_cubic;
                            $input_all3['total_weight'] = $total_weight;
                            $data_update = $input_all3;

                            \App\Models\DaliverySheet::where('id',$dalivery_sheet_id)->update($data_update);
                        }
                    }
                }

                \App\Models\Container::where('id',$container_id)->update(['status_id' => 4]);
                \DB::commit();
                $return['status'] = 1;
                $return['content'] = 'สำเร็จ';
            } catch (Exception $e) {
                \DB::rollBack();
                $return['status'] = 0;
                $return['content'] = trans('lang.unsuccessful').$e->getMessage();
            }
        }else{
            $return['status'] = 0;
        }
        $return['title'] = trans('lang.create_data');
        return json_encode($return);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = \App\Models\Delivery::find($id);

        return json_encode($result);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input_all = $request->all();

        $input_all['updated_at'] = date('Y-m-d H:i:s');

        $validator = Validator::make($request->all(), [
            'plate_no' => 'required',
             'date_delivery' => 'required',
             'status_id' => 'required',

        ]);
        if (!$validator->fails()) {
            \DB::beginTransaction();
            try {
                $data_insert = $input_all;
                \App\Models\Delivery::where('id',$id)->update($data_insert);
                \DB::commit();
                $return['status'] = 1;
                $return['content'] = 'สำเร็จ';
            } catch (Exception $e) {
                \DB::rollBack();
                $return['status'] = 0;
                $return['content'] = trans('lang.unsuccessful').$e->getMessage();
            }
        }else{
            $return['status'] = 0;
        }
        $return['title'] = trans('lang.create_data');
        return json_encode($return);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();
        try {
            \App\Models\Delivery::where('id',$id)->delete();
            \DB::commit();
            $return['status'] = 1;
            $return['content'] = 'สำเร็จ';
        } catch (Exception $e) {
            \DB::rollBack();
            $return['status'] = 0;
            $return['content'] = trans('lang.unsuccessful').$e->getMessage();
        }
        $return['title'] = 'ลบข้อมูล';
        return $return;
    }

    public function Lists()
    {
        $result = \App\Models\DaliveryByContainer::select(
                    'dalivery_by_containers.id'
                    ,'dalivery_by_containers.date_delivery'
                    ,'containers.container_code'
                )
                ->leftjoin('containers', 'containers.id', '=', 'dalivery_by_containers.container_id')
                ->orderBy('dalivery_by_containers.id', 'desc');
        return \Datatables::of($result)

        ->addColumn('action',function($rec){
            // <button class="btn btn-xs btn-condensed btn-success btn-delivery btn-tooltip" data-id="'.$rec->id.'" data-rel="tooltip" title="เปลี่ยนสถานะการจัดส่งของใบส่งสินค้า">
            //     <i class="ace-icon fa fa-cog bigger-120"></i>
            // </button>

            $lang = \Config::get('app.locale');
            $str='
                <a href="'.url('/admin/'.$lang.'/DeliveryContainerUser/'.$rec->id).'" class="btn btn-xs btn-warning btn-condensed btn-tooltip" data-id="'.$rec->id.'" title="'.trans('lang.view_detail').'">
                    <i class="ace-icon fa fa-search bigger-120"></i>
                </a>
                <a href="'.url('/admin/DeliveryContainer/PrintsAll/'.$rec->id).'" target="_blank" class="btn btn-xs btn-info btn-condensed btn-tooltip" data-id="'.$rec->id.'" title="'.trans('lang.issued_invoice').'">
                    <i class="ace-icon fa fa-print bigger-120"></i>
                </a>
                <a href="'.url('/admin/DeliveryContainer/ExcelAll/'.$rec->id).'" class="btn btn-xs btn-success btn-condensed btn-tooltip" data-id="'.$rec->id.'" title="'.trans('lang.export_excel').'">
                    <i class="ace-icon fa fa-list bigger-120"></i>
                </a>

            ';
            return $str;
        })->rawColumns(['action'])->make(true);
    }

    public function DeliveryContainerUser($id)
    {
        $data['main_menu'] = 'DeliveryContainer';
        $data['sub_menu'] = 'DeliveryContainer';
        $data['title_page'] = trans('lang.list_invoice');
        $data['menus'] = \App\Models\AdminMenu::ActiveMenu()->get();
        $data['Users']    = \App\Models\User::get();
        $data['DaliveryByContainer'] = \App\Models\DaliveryByContainer::find($id);

        return view('Admin.delivery_container',$data);
    }

    public function DeliveryContainerUserLists(Request $request)
    {
        $id = $request->input('id');
        $result = \App\Models\DaliverySheet::select(
                    'dalivery_sheets.id'
                    ,'dalivery_sheets.delivery_no'
                    ,'dalivery_sheets.delivery_slip_date'
                    ,'dalivery_sheets.status_payment'
                    ,'dalivery_sheets.status_delivery'
                    ,'dalivery_sheets.total_price'
                    ,'containers.container_code'
                    ,'users.id as user_id'
                    ,'users.customer_general_code'
                )
                ->join('users', 'users.id', '=', 'dalivery_sheets.user_id')
                ->join('dalivery_by_containers', 'dalivery_by_containers.id', '=', 'dalivery_sheets.dalivery_by_containers_id')
                ->join('containers', 'containers.id', '=', 'dalivery_by_containers.container_id')
                ->where('dalivery_sheets.dalivery_by_containers_id', $id)
                ->orderBy('dalivery_sheets.id', 'desc');
        return \Datatables::of($result)

        ->editColumn('total_price', function ($rec){
            return number_format($rec->total_price,2);
        })

        ->addColumn('status', function($rec){
            if($rec->status_payment == 'T'){
                return '<span class="badge badge-success">ชำระเสร็จเรียบร้อย</span>';
            }else{
                return '<span class="badge badge-danger">ยังไม่มีการชำระ</span>';
            }
        })

        ->addColumn('status_delivery', function($rec){
            if($rec->status_delivery == 'S'){
                return '<span class="badge badge-success">จัดส่งเรียบร้อย</span>';
            }elseif($rec->status_delivery == 'W'){
                return '<span class="badge badge-warning">กำลังจัดส่งสินค้า</span>';
            }else{
                return '<span class="badge badge-default">รอดำเนินการ</span>';
            }
        })

        ->addColumn('action',function($rec){
            $lang = \Config::get('app.locale');
            if($rec->status_payment == 'T'){
                $disabled = 'disabled';
            }else{
                $disabled = '';
            }
            if($rec->status_delivery == 'S'){
                $disabled_delivery = 'disabled';
            }else{
                $disabled_delivery = '';
            }

            $str='
                <button  class="btn btn-xs btn-danger btn-condensed btn-change-delivery-slip btn-tooltip" data-id="'.$rec->id.'" data-user-id="'.$rec->user_id.'" '.$disabled.' '.$disabled_delivery.' data-rel="tooltip" title="'.trans('lang.change_status_delivery').'">
                    <i class="ace-icon fa fa-truck bigger-120"></i>
                </button>
                <button style="border-color: #364150;" class="btn btn-xs btn-condensed btn-ststus btn-tooltip" data-id="'.$rec->id.'" '.$disabled_delivery.' data-rel="tooltip" title="เปลี่ยนสถานะ">
                    <i class="ace-icon fa fa-cog bigger-120"></i>
                </button>
                <a href="'.url('/admin/'.$lang.'/DeliveryContainer/DeliveryContainerUserPo/'.$rec->id).'" class="btn btn-xs btn-warning btn-condensed btn-tooltip" data-id="'.$rec->id.'" title="'.trans('lang.view_detail').'">
                    <i class="ace-icon fa fa-search bigger-120"></i>
                </a>
                <a href="'.url('/admin/'.$lang.'/DeliveryContainer/Prints/'.$rec->id).'" target="_blank" class="btn btn-xs btn-info btn-condensed btn-tooltip" data-id="'.$rec->id.'" title="'.trans('lang.issued_invoice').'">
                    <i class="ace-icon fa fa-print bigger-120"></i>
                </a>
                <a href="'.url('/admin/'.$lang.'/DeliveryContainer/Excel/'.$rec->id).'" class="btn btn-xs btn-success btn-condensed btn-tooltip" data-id="'.$rec->id.'" title="'.trans('lang.export_excel').'">
                    <i class="ace-icon fa fa-list bigger-120"></i>
                </a>

            ';
            return $str;
        })->rawColumns(['status', 'status_delivery', 'action'])->make(true);
    }

    public function GetContainerDetail($id)
    {
        $data['container_id'] = $id;
        $data['Containers'] = \App\Models\Container::
            with('TransportType', 'ContainerType', 'ContainerSize')
            ->where('containers.id', $id)
            ->first();
         $data['QrCodeProduct'] = \App\Models\QrCodeProduct::select(
                'qr_code_products.*'
                , 'import_to_chaina.id as import_to_chaina_id'
                , 'import_to_chaina.po_no'
                , 'lot_products.id as lot_product_id'
                , 'users.id as user_id'
                , 'users.customer_general_code'
                , 'product_types.name as product_type_name'
                , 'products.name as product_name'
                , 'lot_products.width'
                , 'lot_products.height'
                , 'lot_products.length'
                , 'lot_products.weight_per_item'
                , 'lot_products.cubic as product_cobic'
                ,'rel_user_type_products.price'
                , \DB::raw('
                        (select sum(qty)
                        from tb_product_import_to_chaina
                        where tb_product_import_to_chaina.import_to_chaina_id = tb_import_to_chaina.id
                        ) as po_qty
                    ')
                , \DB::raw('(`tb_lot_products`.`product_sort_end` - `tb_lot_products`.`product_sort_start`) + 1 as lot_product_qty')
            )
            ->join('rel_container_products', 'rel_container_products.qr_code_product_id', '=', 'qr_code_products.id')
            ->join('import_to_chaina', 'import_to_chaina.id', '=', 'qr_code_products.import_to_chaina_id')
            ->join('users', 'users.id', '=', 'import_to_chaina.user_id')
            ->join('product_import_to_chaina', 'product_import_to_chaina.id', '=', 'qr_code_products.product_import_to_chaina_id')
            ->join('products', 'products.id', '=', 'product_import_to_chaina.product_id')
            ->join('product_types', 'product_types.id', '=', 'products.product_type_id')
            // join lot product
            ->join('lot_products', 'lot_products.product_import_to_chaina_id', '=', 'qr_code_products.product_import_to_chaina_id')
            ->leftjoin('rel_user_type_products', 'rel_user_type_products.id', '=', 'qr_code_products.rel_user_type_product_id')
            ->whereRaw('`tb_qr_code_products`.`sort_id` BETWEEN `tb_lot_products`.`product_sort_start` AND `tb_lot_products`.`product_sort_end`')
            //end join lot product
            ->where('rel_container_products.container_id', $id)
            ->get();

        $data['Users'] = \App\Models\QrCodeProduct::select(
                'qr_code_products.*'
                , 'import_to_chaina.id as import_to_chaina_id'
                , 'import_to_chaina.po_no'
                , 'lot_products.id as lot_product_id'
                , 'users.id as user_id'
                , 'users.customer_general_code'
                , 'product_types.name as product_type_name'
                , 'products.name as product_name'
                , 'lot_products.width'
                , 'lot_products.height'
                , 'lot_products.length'
                , 'lot_products.weight_per_item'
                , 'lot_products.cubic as product_cobic'
                , \DB::raw('
                        (select sum(qty)
                        from tb_product_import_to_chaina
                        where tb_product_import_to_chaina.import_to_chaina_id = tb_import_to_chaina.id
                        ) as po_qty
                    ')
                , \DB::raw('(`tb_lot_products`.`product_sort_end` - `tb_lot_products`.`product_sort_start`) + 1 as lot_product_qty')
            )
            ->join('rel_container_products', 'rel_container_products.qr_code_product_id', '=', 'qr_code_products.id')
            ->join('import_to_chaina', 'import_to_chaina.id', '=', 'qr_code_products.import_to_chaina_id')
            ->join('users', 'users.id', '=', 'import_to_chaina.user_id')
            ->join('product_import_to_chaina', 'product_import_to_chaina.id', '=', 'qr_code_products.product_import_to_chaina_id')
            ->join('products', 'products.id', '=', 'product_import_to_chaina.product_id')
            ->join('product_types', 'product_types.id', '=', 'products.product_type_id')
            // join lot product
            ->join('lot_products', 'lot_products.product_import_to_chaina_id', '=', 'qr_code_products.product_import_to_chaina_id')
            ->whereRaw('`tb_qr_code_products`.`sort_id` BETWEEN `tb_lot_products`.`product_sort_start` AND `tb_lot_products`.`product_sort_end`')
            //end join lot product
            ->where('rel_container_products.container_id', $id)
            //->groupBy('users.id')
            ->get();


        return json_encode($data);
    }

    public function runCode()
    {
        $year = date('ym', strtotime('now'));
        $month = date('m', strtotime('now'));
        $num = 0;
        $cnt = 0;
        $rs = \App\Models\DaliverySheet::orderBy('id', 'DESC')->first();

        if($rs){
            $delivery_no = $rs->delivery_no;  // E-170600001
            $ckmonth2 = substr($delivery_no, 4, 2);//06
            $num = substr($delivery_no, 7, 4);
            if($month == $ckmonth2){
                $cnt = $num+1;
                $shownum = sprintf("%05d", $cnt);
            }
            else{
                $cnt = 0;
                $cnt++;
                $shownum = sprintf("%05d", $cnt);
            }
        }
        else{
            $cnt = 0;
            $cnt++;
            $shownum = sprintf("%05d", $cnt);
        }
         return 'E-'.$year.$shownum;
    }

    public function GetStatusShipping(Request $request, $id)
    {
        $result  = \App\Models\DaliverySheet::find($id);

        return $result;
    }

    public function StatusShipping(Request $request, $id)
    {
        $input_all = $request->all();

        $status_delivery = $input_all['status_delivery'];
        $input_all['updated_at'] = date('Y-m-d H:i:s');

        $validator = Validator::make($request->all(), [

        ]);
        if (!$validator->fails()) {
            \DB::beginTransaction();
            try {
                $data_insert = $input_all;
                \App\Models\DaliverySheet::where('id',$id)->update($data_insert);
                $DeliveryPos = \App\Models\DeliveryPos::where('delivery_sheet_id', $id)->get();

                if(isset($DeliveryPos) && $status_delivery == 'W'){
                    foreach($DeliveryPos as $value){
                        $import_to_chaina_id = $value['import_to_chaina_id'];
                        \App\Models\ImportToChaina::where('id', $import_to_chaina_id)->update(['status_id' => 5]);
                        \App\Models\DeliveryPos::where('id', $value['id'])->update(['status' => 'W']);

                        // send sms
                        $po_status_sms = \App\Models\ImportToChaina::find($import_to_chaina_id);
                        $check_po_status_sms = \App\Models\ImportToChaina::select('import_to_chaina.*')
                            ->join('send_sms', 'send_sms.import_to_chaina_id', '=', 'import_to_chaina.id')
                            ->where('import_to_chaina_id', $import_to_chaina_id)
                            ->where('send_sms.type_status', 4)
                            ->first();
                        // user
                        $User = \App\Models\User::find($po_status_sms->user_id);
                        if(empty($check_po_status_sms) && !empty($po_status_sms) && !empty($User) && $User->main_mobile != null){
                            app('App\Http\Controllers\Admin\ManageSmsController')->SendSMS($po_status_sms->po_no, 4, $amount = NULL, $msisdn = $User->main_mobile);
                            \App\Models\SendSMS::insert([
                                'import_to_chaina_id'=> $import_to_chaina_id
                                , 'type_status'=> 4
                                , 'created_at'=> date('Y-m-d H:i:s')
                            ]);
                        }
                        //end send sms
                    }
                }

                if(isset($DeliveryPos) && $status_delivery == 'S'){
                    foreach($DeliveryPos as $value){
                        $import_to_chaina_id = $value['import_to_chaina_id'];
                        \App\Models\ImportToChaina::where('id', $import_to_chaina_id)->update(['status_id' => 6]);
                        \App\Models\DeliveryPos::where('id', $value['id'])->update(['status' => 'S']);
                        // send sms
                        $po_status_sms = \App\Models\ImportToChaina::where(['id'=> $import_to_chaina_id])->first();
                        $check_po_status_sms = \App\Models\ImportToChaina::select('import_to_chaina.*')
                            ->join('send_sms', 'send_sms.import_to_chaina_id', '=', 'import_to_chaina.id')
                            ->where('import_to_chaina_id', $import_to_chaina_id)
                            ->where('send_sms.type_status', 5)
                            ->first();
                        // user
                        $User = \App\Models\User::find($po_status_sms->user_id);
                        if(empty($check_po_status_sms) && !empty($po_status_sms) && !empty($User) && $User->main_mobile != null){
                            app('App\Http\Controllers\Admin\ManageSmsController')->SendSMS($po_status_sms->po_no, 5, $amount = NULL, $msisdn = $User->main_mobile);
                            \App\Models\SendSMS::insert([
                                'import_to_chaina_id'=> $import_to_chaina_id
                                , 'type_status'=> 5
                                , 'created_at'=> date('Y-m-d H:i:s')
                            ]);
                        }
                        //end send sms
                    }
                }

                \DB::commit();
                $return['status'] = 1;
                $return['content'] = 'สำเร็จ';
            } catch (Exception $e) {
                \DB::rollBack();
                $return['status'] = 0;
                $return['content'] = trans('lang.unsuccessful').$e->getMessage();
            }
        }else{
            $return['status'] = 0;
        }
        $return['title'] = trans('lang.create_data');
        return json_encode($return);
    }

    public function GetUserAddress($id)
    {
        $data['DaliverySheet'] = \App\Models\DaliverySheet::with('Province', 'Amphure')->where('id', $id)->first();
        $data['UserAddresses'] = \App\Models\UserAddress::with('Province', 'Amphure')->where('user_id', $data['DaliverySheet']->user_id)->get();

        return $data;
    }

    public function UserAddress(Request $request, $id)
    {
        $input_all = $request->all();

        $user_address_id = $input_all['user_address_id'];
        $input_all['updated_at'] = date('Y-m-d H:i:s');

        unset($input_all['user_address_id']);

        $validator = Validator::make($request->all(), [

        ]);
        if (!$validator->fails()) {
            \DB::beginTransaction();
            try {
                $UserAddress = \App\Models\UserAddress::with('Province', 'Amphure')->where('id', $user_address_id)->first();

                $input_all['address']       = $UserAddress['address'];
                $input_all['amphur_id']     = $UserAddress['amphur_id'];
                $input_all['province_id']   = $UserAddress['province_id'];
                $input_all['zipcode']       = $UserAddress['zipcode'];

                $data_insert = $input_all;
                \App\Models\DaliverySheet::where('id',$id)->update($data_insert);
                \DB::commit();

                $DaliverySheet = \App\Models\DaliverySheet::find($id);
                $return['DaliverySheet'] = $DaliverySheet;
                $return['status'] = 1;
                $return['content'] = 'สำเร็จ';
            } catch (Exception $e) {
                \DB::rollBack();
                $return['status'] = 0;
                $return['content'] = trans('lang.unsuccessful').$e->getMessage();
            }
        }else{
            $return['status'] = 0;
        }
        $return['title'] = trans('lang.create_data');
        return json_encode($return);
    }

    public function Prints($id)
    {
        $data['DaliverySheet'] = \App\Models\DaliverySheet::select(
                                    'dalivery_sheets.id'
                                    ,'dalivery_sheets.delivery_no'
                                    ,'dalivery_sheets.delivery_slip_date'
                                    ,'dalivery_sheets.address'
                                    ,'dalivery_sheets.zipcode'
                                    ,'dalivery_sheets.status_payment'
                                    ,'dalivery_sheets.total_price'
                                    ,'dalivery_sheets.total_qty'
                                    ,'dalivery_sheets.total_cubic'
                                    ,'dalivery_sheets.total_weight'
                                    ,'users.customer_general_code'
                                    ,'users.firstname'
                                    ,'users.lastname'
                                    ,'users.main_mobile'
                                    ,'amphures.amphure_name'
                                    ,'amphures.zipcode'
                                    ,'provinces.province_name'

                                )
                                ->leftjoin('users', 'users.id', 'dalivery_sheets.user_id')
                                ->leftjoin('amphures', 'amphures.id', '=', 'dalivery_sheets.amphur_id')
                                ->leftjoin('provinces', 'provinces.id', '=', 'dalivery_sheets.province_id')
                                ->where('dalivery_sheets.id', $id)
                                ->first();
        $data['ProductLists']  = \App\Models\DeliveryPos::select(
                                    'delivery_pos.id'
                                    ,'users.customer_general_code'
                                    ,'import_to_chaina.po_no'
                                    ,'qr_code_products.id as qr_code_product_id'
                                    ,'qr_code_products.sort_id as item'
                                    ,'qr_code_products.rel_user_type_product_id'
                                    ,'qr_code_products.rel_user_type_product_id_old'
                                    ,'products.id as product_id'
                                    ,'products.name_en as product_name'
                                    ,'lot_products.id as lot_id'
                                    ,'lot_products.weight_per_item'
                                    ,'lot_products.weight_all'
                                    ,'lot_products.cubic'
                                    ,'qr_code_products.type_rate'
                                    ,\DB::raw('(`tb_lot_products`.`product_sort_end` - `tb_lot_products`.`product_sort_start`) + 1 as lot_product_qty')
                                )
                                ->leftjoin('dalivery_sheets', 'dalivery_sheets.id', '=', 'delivery_pos.delivery_sheet_id')
                                ->leftjoin('users', 'users.id', '=', 'dalivery_sheets.user_id')
                                ->leftjoin('import_to_chaina', 'import_to_chaina.id', '=', 'delivery_pos.import_to_chaina_id')
                                ->leftjoin('qr_code_products', 'qr_code_products.import_to_chaina_id', 'import_to_chaina.id')
                                ->leftjoin('product_import_to_chaina', 'product_import_to_chaina.id', '=', 'qr_code_products.product_import_to_chaina_id')
                                ->leftjoin('products', 'products.id', '=', 'product_import_to_chaina.product_id')
                                ->leftjoin('rel_user_type_products', 'rel_user_type_products.id', '=', 'qr_code_products.rel_user_type_product_id')
                                ->join('lot_products', 'lot_products.product_import_to_chaina_id', '=', 'qr_code_products.product_import_to_chaina_id')
                                ->join('rel_container_products', 'rel_container_products.qr_code_product_id', '=', 'qr_code_products.id')
                                ->whereRaw('`tb_qr_code_products`.`sort_id` BETWEEN `tb_lot_products`.`product_sort_start` AND `tb_lot_products`.`product_sort_end`')
                                ->where('delivery_pos.delivery_sheet_id', $data['DaliverySheet']->id)
                                ->get();

        $data2 = view('Admin.delivery_slip_print', $data);
        $mpdf = new Mpdf([
            'autoLangToFont' => true,
            'autoScriptToLang' => true,
            'format' => 'letter',
            'mode' => 'utf-8',
        ]);
        
        $mpdf->setHtmlHeader('<div style="text-align: right; width: 100%;">{PAGENO}</div>');
        $mpdf->WriteHTML($data2);
        $mpdf->Output();
    }

    public function PrintsAll($id)
    {
        $data['DaliverySheets'] = \App\Models\DaliverySheet::select(
                                    'dalivery_sheets.id'
                                    ,'dalivery_sheets.delivery_no'
                                    ,'dalivery_sheets.delivery_slip_date'
                                    ,'dalivery_sheets.address'
                                    ,'dalivery_sheets.zipcode'
                                    ,'dalivery_sheets.status_payment'
                                    ,'dalivery_sheets.total_price'
                                    ,'dalivery_sheets.total_qty'
                                    ,'dalivery_sheets.total_cubic'
                                    ,'dalivery_sheets.total_weight'
                                    ,'users.customer_general_code'
                                    ,'users.firstname'
                                    ,'users.lastname'
                                    ,'users.main_mobile'
                                    ,'amphures.amphure_name'
                                    ,'provinces.province_name'
                                )
                                ->leftjoin('users', 'users.id', 'dalivery_sheets.user_id')
                                ->leftjoin('amphures', 'amphures.id', '=', 'dalivery_sheets.amphur_id')
                                ->leftjoin('provinces', 'provinces.id', '=', 'dalivery_sheets.province_id')
                                ->where('dalivery_sheets.dalivery_by_containers_id', $id)
                                ->orderBy('users.customer_general_code')
                                ->get();

        if(!$data['DaliverySheets']->isEmpty()){
            foreach($data['DaliverySheets'] as $DaliverySheet){
                $data['DeliveryPos'][$DaliverySheet['id']] = \App\Models\DeliveryPos::select(
                                    'delivery_pos.id'
                                    ,'users.customer_general_code'
                                    ,'import_to_chaina.po_no'
                                    ,'qr_code_products.id as qr_code_product_id'
                                    ,'qr_code_products.sort_id as item'
                                    ,'qr_code_products.rel_user_type_product_id'
                                    ,'qr_code_products.rel_user_type_product_id_old'
                                    ,'products.id as product_id'
                                    ,'products.name_en as product_name'
                                    ,'lot_products.id as lot_id'
                                    ,'lot_products.weight_per_item'
                                    ,'lot_products.weight_all'
                                    ,'lot_products.cubic'
                                    ,'qr_code_products.type_rate'
                                    ,\DB::raw('(`tb_lot_products`.`product_sort_end` - `tb_lot_products`.`product_sort_start`) + 1 as lot_product_qty')
                                )
                                ->leftjoin('dalivery_sheets', 'dalivery_sheets.id', '=', 'delivery_pos.delivery_sheet_id')
                                ->leftjoin('users', 'users.id', '=', 'dalivery_sheets.user_id')
                                ->leftjoin('import_to_chaina', 'import_to_chaina.id', '=', 'delivery_pos.import_to_chaina_id')
                                ->leftjoin('qr_code_products', 'qr_code_products.import_to_chaina_id', 'import_to_chaina.id')
                                ->leftjoin('product_import_to_chaina', 'product_import_to_chaina.id', '=', 'qr_code_products.product_import_to_chaina_id')
                                ->leftjoin('products', 'products.id', '=', 'product_import_to_chaina.product_id')
                                ->leftjoin('rel_user_type_products', 'rel_user_type_products.id', '=', 'qr_code_products.rel_user_type_product_id')
                                ->join('lot_products', 'lot_products.product_import_to_chaina_id', '=', 'qr_code_products.product_import_to_chaina_id')
                                ->join('rel_container_products', 'rel_container_products.qr_code_product_id', '=', 'qr_code_products.id')
                                ->whereRaw('`tb_qr_code_products`.`sort_id` BETWEEN `tb_lot_products`.`product_sort_start` AND `tb_lot_products`.`product_sort_end`')
                                ->where('delivery_pos.delivery_sheet_id', $DaliverySheet['id'])
                                ->get();
            }
        }

        $data2 = view('Admin.delivery_slip_all_print', $data);
        $mpdf = new Mpdf([
            'autoLangToFont' => true,
            'autoScriptToLang' => true,
            'format' => 'letter',
            'mode' => 'utf-8',
        ]);
        
        $mpdf->setHtmlHeader('<div style="text-align: right; width: 100%;">{PAGENO}</div>');
        $mpdf->WriteHTML($data2);
        $mpdf->Output();
    }

    public function Excel($id)
    {
        $date = date('d-m-Y');
        $data['DaliverySheet'] = \App\Models\DaliverySheet::select(
                                    'dalivery_sheets.id'
                                    ,'dalivery_sheets.delivery_no'
                                    ,'dalivery_sheets.delivery_slip_date'
                                    ,'dalivery_sheets.address'
                                    ,'dalivery_sheets.zipcode'
                                    ,'dalivery_sheets.status_payment'
                                    ,'dalivery_sheets.total_price'
                                    ,'dalivery_sheets.total_qty'
                                    ,'dalivery_sheets.total_cubic'
                                    ,'dalivery_sheets.total_weight'
                                    ,'users.customer_general_code'
                                    ,'users.firstname'
                                    ,'users.lastname'
                                    ,'users.main_mobile'
                                    ,'amphures.amphure_name'
                                    ,'provinces.province_name'

                                )
                                ->leftjoin('users', 'users.id', 'dalivery_sheets.user_id')
                                ->leftjoin('amphures', 'amphures.id', '=', 'dalivery_sheets.amphur_id')
                                ->leftjoin('provinces', 'provinces.id', '=', 'dalivery_sheets.province_id')
                                ->where('dalivery_sheets.id', $id)
                                ->first();
        $data['ProductLists']  = \App\Models\DeliveryPos::select(
                                    'delivery_pos.id'
                                    ,'users.customer_general_code'
                                    ,'import_to_chaina.po_no'
                                    ,'qr_code_products.id as qr_code_product_id'
                                    ,'qr_code_products.sort_id as item'
                                    ,'qr_code_products.rel_user_type_product_id'
                                    ,'qr_code_products.rel_user_type_product_id_old'
                                    ,'products.id as product_id'
                                    ,'products.name_en as product_name'
                                    ,'lot_products.id as lot_id'
                                    ,'lot_products.weight_per_item'
                                    ,'lot_products.weight_all'
                                    ,'lot_products.cubic'
                                    ,'qr_code_products.type_rate'
                                    ,\DB::raw('(`tb_lot_products`.`product_sort_end` - `tb_lot_products`.`product_sort_start`) + 1 as lot_product_qty')
                                )
                                ->leftjoin('dalivery_sheets', 'dalivery_sheets.id', '=', 'delivery_pos.delivery_sheet_id')
                                ->leftjoin('users', 'users.id', '=', 'dalivery_sheets.user_id')
                                ->leftjoin('import_to_chaina', 'import_to_chaina.id', '=', 'delivery_pos.import_to_chaina_id')
                                ->leftjoin('qr_code_products', 'qr_code_products.import_to_chaina_id', 'import_to_chaina.id')
                                ->leftjoin('product_import_to_chaina', 'product_import_to_chaina.id', '=', 'qr_code_products.product_import_to_chaina_id')
                                ->leftjoin('products', 'products.id', '=', 'product_import_to_chaina.product_id')
                                ->leftjoin('rel_user_type_products', 'rel_user_type_products.id', '=', 'qr_code_products.rel_user_type_product_id')
                                ->join('lot_products', 'lot_products.product_import_to_chaina_id', '=', 'qr_code_products.product_import_to_chaina_id')
                                ->join('rel_container_products', 'rel_container_products.qr_code_product_id', '=', 'qr_code_products.id')
                                ->whereRaw('`tb_qr_code_products`.`sort_id` BETWEEN `tb_lot_products`.`product_sort_start` AND `tb_lot_products`.`product_sort_end`')
                                ->where('delivery_pos.delivery_sheet_id', $data['DaliverySheet']->id)
                                ->get();

        \Excel::create('Delivery-Slip-'.$date, function($excel) use ($data) {
           $excel->sheet('Delivery-Slip', function($sheet) use ($data) {
               $sheet->loadView('Admin.delivery_slip_excel', $data);
           });
       })->download('xls');
    }

    public function ExcelAll($id)
    {
        $date = date('d-m-Y');

        $data['DaliverySheets'] = \App\Models\DaliverySheet::select(
                                    'dalivery_sheets.id'
                                    ,'dalivery_sheets.delivery_no'
                                    ,'dalivery_sheets.delivery_slip_date'
                                    ,'dalivery_sheets.address'
                                    ,'dalivery_sheets.zipcode'
                                    ,'dalivery_sheets.status_payment'
                                    ,'dalivery_sheets.total_price'
                                    ,'dalivery_sheets.total_qty'
                                    ,'dalivery_sheets.total_cubic'
                                    ,'dalivery_sheets.total_weight'
                                    ,'users.customer_general_code'
                                    ,'users.firstname'
                                    ,'users.lastname'
                                    ,'users.main_mobile'
                                    ,'amphures.amphure_name'
                                    ,'provinces.province_name'
                                )
                                ->leftjoin('users', 'users.id', 'dalivery_sheets.user_id')
                                ->leftjoin('amphures', 'amphures.id', '=', 'dalivery_sheets.amphur_id')
                                ->leftjoin('provinces', 'provinces.id', '=', 'dalivery_sheets.province_id')
                                ->where('dalivery_sheets.dalivery_by_containers_id', $id)
                                ->get();

        if(!$data['DaliverySheets']->isEmpty()){
            foreach($data['DaliverySheets'] as $DaliverySheet){
                $data['DeliveryPos'][$DaliverySheet['id']] = \App\Models\DeliveryPos::select(
                                    'delivery_pos.id'
                                    ,'users.customer_general_code'
                                    ,'import_to_chaina.po_no'
                                    ,'qr_code_products.id as qr_code_product_id'
                                    ,'qr_code_products.sort_id as item'
                                    ,'qr_code_products.rel_user_type_product_id'
                                    ,'qr_code_products.rel_user_type_product_id_old'
                                    ,'products.id as product_id'
                                    ,'products.name_en as product_name'
                                    ,'lot_products.id as lot_id'
                                    ,'lot_products.weight_per_item'
                                    ,'lot_products.weight_all'
                                    ,'lot_products.cubic'
                                    ,'qr_code_products.type_rate'
                                    ,\DB::raw('(`tb_lot_products`.`product_sort_end` - `tb_lot_products`.`product_sort_start`) + 1 as lot_product_qty')
                                )
                                ->leftjoin('dalivery_sheets', 'dalivery_sheets.id', '=', 'delivery_pos.delivery_sheet_id')
                                ->leftjoin('users', 'users.id', '=', 'dalivery_sheets.user_id')
                                ->leftjoin('import_to_chaina', 'import_to_chaina.id', '=', 'delivery_pos.import_to_chaina_id')
                                ->leftjoin('qr_code_products', 'qr_code_products.import_to_chaina_id', 'import_to_chaina.id')
                                ->leftjoin('product_import_to_chaina', 'product_import_to_chaina.id', '=', 'qr_code_products.product_import_to_chaina_id')
                                ->leftjoin('products', 'products.id', '=', 'product_import_to_chaina.product_id')
                                ->leftjoin('rel_user_type_products', 'rel_user_type_products.id', '=', 'qr_code_products.rel_user_type_product_id')
                                ->join('lot_products', 'lot_products.product_import_to_chaina_id', '=', 'qr_code_products.product_import_to_chaina_id')
                                ->join('rel_container_products', 'rel_container_products.qr_code_product_id', '=', 'qr_code_products.id')
                                ->whereRaw('`tb_qr_code_products`.`sort_id` BETWEEN `tb_lot_products`.`product_sort_start` AND `tb_lot_products`.`product_sort_end`')
                                ->where('delivery_pos.delivery_sheet_id', $DaliverySheet['id'])
                                ->get();
            }
        }

        \Excel::create('Delivery-Slip-All'.$date, function($excel) use ($data) {
           $excel->sheet('Delivery-Slip_All', function($sheet) use ($data) {
               $sheet->loadView('Admin.delivery_slip_all_excel', $data);
           });
       })->download('xls');
    }

    public function DeliveryContainerUserPo($id)
    {
        $data['main_menu'] = 'DeliveryContainer';
        $data['sub_menu'] = 'DeliveryContainer';
        $data['title_page'] = trans('lang.list_invoice');
        $data['menus'] = \App\Models\AdminMenu::ActiveMenu()->get();
        $data['Users']    = \App\Models\User::get();
        $data['delivery_sheet_id'] = $id;

        return view('Admin.delivery_container_user_po',$data);
    }

    public function DeliveryContainerUserPoLists(Request $request)
    {
        $id = $request->input('id');
        $result = \App\Models\DeliveryPos::select(
                    'delivery_pos.id'
                    ,'dalivery_sheets.id as dalivery_sheet_id'
                    ,'dalivery_sheets.delivery_no'
                    ,'dalivery_sheets.delivery_slip_date'
                    ,'delivery_pos.payment_status as status_payment'
                    ,'delivery_pos.status as status_delivery'
                    ,'delivery_pos.price as total_price'
                    ,'containers.container_code'
                    ,'users.id as user_id'
                    ,'users.customer_general_code'
                    ,'import_to_chaina.po_no'
                )
                ->leftjoin('dalivery_sheets', 'dalivery_sheets.id', '=', 'delivery_pos.delivery_sheet_id')
                ->leftjoin('import_to_chaina', 'import_to_chaina.id', '=', 'delivery_pos.import_to_chaina_id')
                ->leftjoin('users', 'users.id', '=', 'dalivery_sheets.user_id')
                ->leftjoin('dalivery_by_containers', 'dalivery_by_containers.id', '=', 'dalivery_sheets.dalivery_by_containers_id')
                ->leftjoin('containers', 'containers.id', '=', 'dalivery_by_containers.container_id')
                ->where('delivery_pos.delivery_sheet_id', $id)
                ->orderBy('delivery_pos.id', 'desc');
        return \Datatables::of($result)

        ->editColumn('total_price', function ($rec){
            return number_format($rec->total_price,2);
        })

        ->addColumn('status', function($rec){
            if($rec->status_payment == 'T'){
                return '<span class="badge badge-success">ชำระเสร็จเรียบร้อย</span>';
            }else{
                return '<span class="badge badge-danger">ยังไม่มีการชำระ</span>';
            }
        })

        ->addColumn('status_delivery', function($rec){
            if($rec->status_delivery == 'S'){
                return '<span class="badge badge-success">จัดส่งเรียบร้อย</span>';
            }elseif($rec->status_delivery == 'W'){
                return '<span class="badge badge-warning">กำลังจัดส่งสินค้า</span>';
            }else{
                return '<span class="badge badge-default">รอดำเนินการ</span>';
            }
        })

        ->addColumn('action',function($rec){
            $lang = \Config::get('app.locale');
            if($rec->status_payment == 'T'){
                $disabled = 'disabled';
            }else{
                $disabled = '';
            }
            if($rec->status_delivery == 'S'){
                $disabled_delivery = 'disabled';
            }else{
                $disabled_delivery = '';
            }

            $str='
                <button  class="btn btn-xs btn-danger btn-condensed btn-change-delivery-slip btn-tooltip" data-id="'.$rec->dalivery_sheet_id.'" data-user-id="'.$rec->user_id.'" data-rel="tooltip" title="'.trans('lang.address_delivery').'">
                    <i class="ace-icon fa fa-truck bigger-120"></i>
                </button>
                <button style="border-color: #364150;" class="btn btn-xs btn-condensed btn-ststus btn-tooltip" data-id="'.$rec->id.'" '.$disabled_delivery.' data-rel="tooltip" title="เปลี่ยนสถานะ">
                    <i class="ace-icon fa fa-cog bigger-120"></i>
                </button>
                <a href="'.url('/admin/'.$lang.'/DeliveryContainerUserPo/PrintsUserPo/'.$rec->id).'" target="_blank" class="btn btn-xs btn-info btn-condensed btn-tooltip" data-id="'.$rec->id.'" title="'.trans('lang.issued_invoice').'">
                    <i class="ace-icon fa fa-print bigger-120"></i>
                </a>
                <a href="'.url('/admin/'.$lang.'/DeliveryContainerUserPo/ExcelUserPo/'.$rec->id).'" class="btn btn-xs btn-success btn-condensed btn-tooltip" data-id="'.$rec->id.'" title="'.trans('lang.export_excel').'">
                    <i class="ace-icon fa fa-list bigger-120"></i>
                </a>

            ';
            return $str;
        })->rawColumns(['status', 'status_delivery', 'action'])->make(true);
    }

    public function GetStatusShippingUserPo(Request $request, $id)
    {
        $result  = \App\Models\DeliveryPos::find($id);

        return $result;
    }

    public function StatusShippingUserPo(Request $request, $id)
    {
        $input_all = $request->all();
        $input_all['status'] = $input_all['status_delivery'];
        $input_all['updated_at'] = date('Y-m-d H:i:s');

        unset($input_all['status_delivery']);
        $validator = Validator::make($request->all(), [
            'status_delivery' => 'required',
        ]);
        if (!$validator->fails()) {
            \DB::beginTransaction();
            try {
                $data_insert = $input_all;
                \App\Models\DeliveryPos::where('id',$id)->update($data_insert);

                \DB::commit();
                $return['status'] = 1;
                $return['content'] = 'สำเร็จ';
            } catch (Exception $e) {
                \DB::rollBack();
                $return['status'] = 0;
                $return['content'] = trans('lang.unsuccessful').$e->getMessage();
            }
        }else{
            $return['status'] = 0;
        }
        $return['title'] = trans('lang.create_data');
        return json_encode($return);
    }


    public function PrintsUserPo($id)
    {
        $data['DeliveryPos'] = \App\Models\DeliveryPos::select(
                'delivery_pos.*'
                ,'dalivery_sheets.delivery_no'
                ,'dalivery_sheets.delivery_slip_date'
                ,'users.customer_general_code'
                ,'users.firstname'
                ,'users.lastname'
                ,'users.main_mobile'
                ,'users.address'
                ,'amphures.amphure_name'
                ,'amphures.zipcode'
                ,'provinces.province_name'
                ,'import_to_chaina.po_no'
            )
            ->leftjoin('dalivery_sheets', 'dalivery_sheets.id', '=', 'delivery_pos.delivery_sheet_id')
            ->leftjoin('import_to_chaina', 'import_to_chaina.id', '=', 'delivery_pos.import_to_chaina_id')
            ->leftjoin('users', 'users.id', '=', 'dalivery_sheets.user_id')
            ->leftjoin('amphures', 'amphures.id', '=', 'dalivery_sheets.amphur_id')
            ->leftjoin('provinces', 'provinces.id', '=', 'dalivery_sheets.province_id')
            ->where('delivery_pos.id', $id)
            ->first();
        $data['ProductLists']  = \App\Models\DeliveryPos::select(
                                    'delivery_pos.id'
                                    ,'users.customer_general_code'
                                    ,'import_to_chaina.po_no'
                                    ,'qr_code_products.id as qr_code_product_id'
                                    ,'qr_code_products.sort_id as item'
                                    ,'qr_code_products.rel_user_type_product_id'
                                    ,'qr_code_products.rel_user_type_product_id_old'
                                    ,'products.id as product_id'
                                    ,'products.name_en as product_name'
                                    ,'lot_products.id as lot_id'
                                    ,'lot_products.weight_per_item'
                                    ,'lot_products.weight_all'
                                    ,'lot_products.cubic'
                                    ,'qr_code_products.type_rate'
                                    ,\DB::raw('(`tb_lot_products`.`product_sort_end` - `tb_lot_products`.`product_sort_start`) + 1 as lot_product_qty')
                                )
                                ->leftjoin('dalivery_sheets', 'dalivery_sheets.id', '=', 'delivery_pos.delivery_sheet_id')
                                ->leftjoin('users', 'users.id', '=', 'dalivery_sheets.user_id')
                                ->leftjoin('import_to_chaina', 'import_to_chaina.id', '=', 'delivery_pos.import_to_chaina_id')
                                ->leftjoin('qr_code_products', 'qr_code_products.import_to_chaina_id', 'import_to_chaina.id')
                                ->leftjoin('product_import_to_chaina', 'product_import_to_chaina.id', '=', 'qr_code_products.product_import_to_chaina_id')
                                ->leftjoin('products', 'products.id', '=', 'product_import_to_chaina.product_id')
                                ->leftjoin('rel_user_type_products', 'rel_user_type_products.id', '=', 'qr_code_products.rel_user_type_product_id')
                                ->join('lot_products', 'lot_products.product_import_to_chaina_id', '=', 'qr_code_products.product_import_to_chaina_id')
                                ->join('rel_container_products', 'rel_container_products.qr_code_product_id', '=', 'qr_code_products.id')
                                ->whereRaw('`tb_qr_code_products`.`sort_id` BETWEEN `tb_lot_products`.`product_sort_start` AND `tb_lot_products`.`product_sort_end`')
                                ->where('delivery_pos.id', $id)
                                ->get();

        $data2 = view('Admin.delivery_slip_user_po_print', $data);
        $mpdf = new Mpdf([
            'autoLangToFont' => true,
            'autoScriptToLang' => true,
            'format' => 'letter',
            'mode' => 'utf-8',
        ]);
        
        $mpdf->setHtmlHeader('<div style="text-align: right; width: 100%;">{PAGENO}</div>');
        $mpdf->WriteHTML($data2);
        $mpdf->Output();
    }

    public function ExcelUserPo($id)
    {
        $date = date('d-m-Y');
        $data['DeliveryPos'] = \App\Models\DeliveryPos::select(
                'delivery_pos.*'
                ,'dalivery_sheets.delivery_no'
                ,'dalivery_sheets.delivery_slip_date'
                ,'users.customer_general_code'
                ,'users.firstname'
                ,'users.lastname'
                ,'users.main_mobile'
                ,'users.address'
                ,'amphures.amphure_name'
                ,'amphures.zipcode'
                ,'provinces.province_name'
                ,'import_to_chaina.po_no'
            )
            ->leftjoin('dalivery_sheets', 'dalivery_sheets.id', '=', 'delivery_pos.delivery_sheet_id')
            ->leftjoin('import_to_chaina', 'import_to_chaina.id', '=', 'delivery_pos.import_to_chaina_id')
            ->leftjoin('users', 'users.id', '=', 'dalivery_sheets.user_id')
            ->leftjoin('amphures', 'amphures.id', '=', 'dalivery_sheets.amphur_id')
            ->leftjoin('provinces', 'provinces.id', '=', 'dalivery_sheets.province_id')
            ->where('delivery_pos.id', $id)
            ->first();
        $data['ProductLists']  = \App\Models\DeliveryPos::select(
                                    'delivery_pos.id'
                                    ,'users.customer_general_code'
                                    ,'import_to_chaina.po_no'
                                    ,'qr_code_products.id as qr_code_product_id'
                                    ,'qr_code_products.sort_id as item'
                                    ,'qr_code_products.rel_user_type_product_id'
                                    ,'qr_code_products.rel_user_type_product_id_old'
                                    ,'products.id as product_id'
                                    ,'products.name_en as product_name'
                                    ,'lot_products.id as lot_id'
                                    ,'lot_products.weight_per_item'
                                    ,'lot_products.weight_all'
                                    ,'lot_products.cubic'
                                    ,'qr_code_products.type_rate'
                                    ,\DB::raw('(`tb_lot_products`.`product_sort_end` - `tb_lot_products`.`product_sort_start`) + 1 as lot_product_qty')
                                )
                                ->leftjoin('dalivery_sheets', 'dalivery_sheets.id', '=', 'delivery_pos.delivery_sheet_id')
                                ->leftjoin('users', 'users.id', '=', 'dalivery_sheets.user_id')
                                ->leftjoin('import_to_chaina', 'import_to_chaina.id', '=', 'delivery_pos.import_to_chaina_id')
                                ->leftjoin('qr_code_products', 'qr_code_products.import_to_chaina_id', 'import_to_chaina.id')
                                ->leftjoin('product_import_to_chaina', 'product_import_to_chaina.id', '=', 'qr_code_products.product_import_to_chaina_id')
                                ->leftjoin('products', 'products.id', '=', 'product_import_to_chaina.product_id')
                                ->leftjoin('rel_user_type_products', 'rel_user_type_products.id', '=', 'qr_code_products.rel_user_type_product_id')
                                ->join('lot_products', 'lot_products.product_import_to_chaina_id', '=', 'qr_code_products.product_import_to_chaina_id')
                                ->join('rel_container_products', 'rel_container_products.qr_code_product_id', '=', 'qr_code_products.id')
                                ->whereRaw('`tb_qr_code_products`.`sort_id` BETWEEN `tb_lot_products`.`product_sort_start` AND `tb_lot_products`.`product_sort_end`')
                                ->where('delivery_pos.id', $id)
                                ->get();

        \Excel::create('Delivery-Slip-PO'.$date, function($excel) use ($data) {
           $excel->sheet('Delivery-Slip-Po', function($sheet) use ($data) {
               $sheet->loadView('Admin.delivery_slip_user_po_excel', $data);
           });
       })->download('xls');
    }
}
