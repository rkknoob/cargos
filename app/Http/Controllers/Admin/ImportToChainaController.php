<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Storage;
use PDF;
use DB;
use \Mpdf\Mpdf;
use App\Providers\Admin\PermissionCRUDProvider as Permission;

class ImportToChainaController extends Controller
{

    public function index()
    {
    	$data['main_menu'] = 'ImportToChaina';
    	$data['sub_menu'] = '';
    	$data['title'] = trans('lang.import_warehouse_china');
    	$data['title_page'] = trans('lang.import_warehouse_china');
    	$data['permission'] = Permission::CheckPermission($data['main_menu'],$data['sub_menu']);
    	$data['menus'] = \App\Models\AdminMenu::ActiveMenu()->get();
        $data['SummarizeContainer'] = \App\Models\QrCodeProduct::select(
                DB::raw('count(`tb_qr_code_products`.`id`) as amount_product')
                ,DB::raw('sum(`tb_lot_products`.`weight_per_item`) as weight_product')
                ,'transport_types.name_th as transport_type_name_th'
                ,'transport_types.name_en as transport_type_name_en'
                ,'transport_types.name_ch as transport_type_name_ch'
            )
            ->leftjoin('rel_container_products', 'rel_container_products.qr_code_product_id', '=', 'qr_code_products.id')
            ->join('product_import_to_chaina', 'product_import_to_chaina.id', '=', 'qr_code_products.product_import_to_chaina_id')
            ->join('transport_types', 'transport_types.id', '=', 'product_import_to_chaina.transport_type_id')
            // join lot product
            ->join('lot_products', 'lot_products.product_import_to_chaina_id', '=', 'qr_code_products.product_import_to_chaina_id')
            ->whereRaw('`tb_qr_code_products`.`sort_id` BETWEEN `tb_lot_products`.`product_sort_start` AND `tb_lot_products`.`product_sort_end`')
            //end join lot product
            ->whereNull('rel_container_products.id')
            ->groupBy('transport_types.id')
            ->groupBy('transport_types.name_th')
            ->groupBy('transport_types.name_en')
            ->groupBy('transport_types.name_ch')
            ->get();
        // dd($data['SummarizeContainer']);
    	return view('Admin.import_to_chaina',$data);
    }

    public function create()
    {
    	$data['main_menu'] = 'ImportToChaina';
    	$data['sub_menu'] = 'ImportToChaina';
    	$data['title'] = trans('lang.increase_of_import_chinese');
    	$data['title_page'] = trans('lang.increase_of_import_chinese');
    	$data['permission'] = Permission::CheckPermission($data['main_menu'],$data['sub_menu']);
    	$data['menus'] = \App\Models\AdminMenu::ActiveMenu()->get();
        $data['Users'] = \App\Models\User::whereNotNull('customer_general_code')->get();
        $data['ProductTypes'] = \App\Models\ProductType::whereNotIn('id', [5,6])->get();
        $data['Products'] = \App\Models\Product::get();
        $data['ContainerTypes'] = \App\Models\ContainerType::get();
        $data['TransportationTypes'] = \App\Models\TransportType::get();
        $data['PO'] = \App\Models\ImportToChaina::select('po_no')->where('status_id', 1)->get();
        $data['po_number'] = '';

        return view('Admin.import_to_chaina_create',$data);
    }

    public function store(Request $request)
    {
        $input_all = $request->all();
        $amount = 0;
        $user = \App\Models\User::select('customer_general_code')->find($input_all['import_to_chaina']['customer_id']);
        // $input_all['import_to_chaina']['po_number'] = $this->runCode();
        $input_all['import_to_chaina']['created_by'] = auth()->guard('admin')->user()->id;
        // $input_all['import_to_chaina']['updated_by'] = auth()->guard('admin')->user()->id;
        $input_all['import_to_chaina']['created_at'] = date('Y-m-d H:i:s');
        // $input_all['import_to_chaina']['updated_at'] = date('Y-m-d H:i:s');
        $input_all['import_to_chaina']['import_to_chaina_date'] = $input_all['import_to_chaina']['import_to_chaina_date'].' '.date('H:i:s');

        unset($input_all['weigh_type']);
        unset($input_all['weigh_format']);

        $validator = Validator::make($request->all(), [
            // 'import_to_chaina[po_number]'   => 'required',
            // 'import_to_chaina[customer_id]'   => 'required',
        ]);

        $check_po_use = \App\Models\ImportToChaina::select(
                'import_to_chaina.id as po_id'
            )
            // ->whereNotIn('status_id', [1])
            ->where('po_no', '=', $input_all['import_to_chaina']['po_number'])
            ->first();
        if(empty($check_po_use)){
            if (!$validator->fails()) {
                \DB::beginTransaction();
                try {
                    $status_default = 'T';
                    $data_insert_import_to_chaina = [
                        'po_no' => $input_all['import_to_chaina']['po_number'],
                        'import_date' => $input_all['import_to_chaina']['import_to_chaina_date'],
                        'user_id' => $input_all['import_to_chaina']['customer_id'],
                        'remark' => (isset($input_all['remark'])) ? $input_all['remark'] : null,
                        'status_id' => 1,
                        'created_at' => date('Y-m-d H:i:s'),
                    ];
                    // dd($input_all['import_to_chaina']['po_number']);
                    $check_po = \App\Models\ImportToChaina::select(
                            'import_to_chaina.id as po_id'
                        )
                        ->where('po_no', '=', $input_all['import_to_chaina']['po_number'])
                        ->first();
                    // dd($check_po);
                    if(!empty($check_po)){
                        unset($data_insert_import_to_chaina['created_at']);
                        $import_to_chaina_id = $check_po->po_id;
                        \App\Models\ImportToChaina::where('id', $import_to_chaina_id)->update($data_insert_import_to_chaina);
                    }else{
                        $import_to_chaina_id = \App\Models\ImportToChaina::insertGetId($data_insert_import_to_chaina);
                    }
                    // $import_to_chaina    = \App\Models\ReceiversChin::find($import_to_chaina_id);

                    foreach($input_all['product'] as $key1 => $value1){
                        $no = 1;
                        // set price in product
                        $product_old_price = \App\Models\QrCodeProduct::select(
                                'qr_code_products.rel_user_type_product_id',
                                'rel_user_type_products.type_rate'
                            )
                            ->join('product_import_to_chaina', 'qr_code_products.product_import_to_chaina_id', '=', 'product_import_to_chaina.id')
                            ->join('products', 'products.id', '=', 'product_import_to_chaina.product_id')
                            ->join('import_to_chaina', 'import_to_chaina.id', '=', 'qr_code_products.import_to_chaina_id')
                            ->join('rel_user_type_products', 'rel_user_type_products.id', '=', 'qr_code_products.rel_user_type_product_id')
                            ->where('products.product_type_id', $value1['product_type_id'])
                            ->where('import_to_chaina.user_id', $input_all['import_to_chaina']['customer_id'])
                            ->orderBy('product_import_to_chaina.id', 'desc')
                            ->first();
                        // end set price in product
                        // $start_no = 1;
                        // $end_no = 0;
                        if(!empty($value1['lot_product']) && count($value1['lot_product']) > 0){
                            // set data product
                            if (!empty($value1['product_name'])) {
                                $checkProduct = \App\Models\Product::select('id', 'product_type_id')
                                    ->where('name', $value1['product_name'])
                                    ->where('code', $value1['product_code'])
                                    ->first();
                                if(!empty($checkProduct)){
                                    $product_id = $checkProduct->id;
                                    if($checkProduct->product_type_id != $value1['product_type_id']){
                                        $data_update_product= [
                                            'product_type_id' => $value1['product_type_id'],
                                        ];
                                        \App\Models\Product::where('id', $product_id)->update($data_update_product);
                                    }
                                } else {
                                    $data_insert_product= [
                                        'code' => $value1['product_code'],
                                        'product_type_id' => $value1['product_type_id'],
                                        'name' => $value1['product_name'],
                                        'created_at' => date('Y-m-d H:i:s'),
                                    ];
                                    $product_id = \App\Models\Product::insertGetId($data_insert_product);
                                }
                            }
                            // end set data product
                            $size_wide = $size_high = $size_long = $size_weight_tote = $size_cubic = $qty_product = 0;

                            foreach($value1['lot_product'] as $sum_size){

                                $size_wide += $sum_size['size_wide'];
                                $size_high += $sum_size['size_high'];
                                $size_long += $sum_size['size_long'];
                                $size_weight_tote += $sum_size['size_weight_tote'];
                                $size_cubic += $sum_size['size_cubic'];
                                $qty_product += ($sum_size['product_no_max']-$sum_size['product_no_min'])+1;
                            }
                            $data_insert_all['product'][$key1] = $data_insert_product = [
                                'import_to_chaina_id' => $import_to_chaina_id,
                                'product_id' => $product_id,
                                'product_type_id' => $value1['product_type_id'],
                                'transport_type_id' => $value1['transportation_type_id'],
                                'shop_chaina_name' => $value1['chinese_shop'],
                                // 'container_type_id' => $value1['container_type_id'],
                                'detail' => $input_all['import_to_chaina']['customer_id'],
                                'qty' => $qty_product,
                                'pcs' => $value1['pcs'],
                                'width' => $size_wide,
                                'height' => $size_high,
                                'length' => $size_long,
                                'weight_per_item' => ($size_weight_tote / $qty_product),
                                'weight_all' => $size_weight_tote,
                                'cubic' => $size_cubic,
                                'weigh_format' => $value1['weigh_format'],
                                'created_at' => date('Y-m-d H:i:s'),
                            ];
                            if(!empty($product_old_price)){
                                $data_insert_product['status_rate'] = 'T';
                            }else{
                                $data_insert_product['status_rate'] = 'F';
                                $status_default = 'F';
                            }
                            $product_import_to_chaina_id = \App\Models\ProductImportToChaina::insertGetId($data_insert_product);

                            // \App\Models\ImportToChaina::where('stauts_rate', 'T')->select('id')

                            foreach($value1['lot_product'] as $key2 => $value2){
                                // $end_no += $value2['qty'];
                                $data_insert_all['product'][$key1]['lot'][$key2] = $data_insert_lot = [
                                    'product_import_to_chaina_id' => $product_import_to_chaina_id,
                                    'product_sort_start' => $value2['product_no_min'],
                                    'product_sort_end' => $value2['product_no_max'],
                                    'width' => $value2['size_wide'],
                                    'height' => $value2['size_high'],
                                    'length' => $value2['size_long'],
                                    'weight_per_item' => ($value2['size_weight_tote'] / $value2['qty']),
                                    'weight_all' => $value2['size_weight_tote'],
                                    'cubic' => $value2['size_cubic'],
                                    'remark' => $value2['decription'],
                                    'created_at' => date('Y-m-d H:i:s'),
                                ];

                                $lot_product_id = \App\Models\LotProduct::insertGetId($data_insert_lot);
                                $num = intval($value2['qty']);
                                $amount += $num;
                                for($i=0; $i<$num; $i++){
                                    $data_insert_all['product'][$key1]['lot'][$key2]['qr_code'][$i] = $data_insert_qrcode = [
                                        // 'lot_product_id' => $lot_product_id,
                                        'import_to_chaina_id' => $import_to_chaina_id,
                                        // 'rel_user_type_product_id' => $value1['rel_user_type_product_id'],
                                        'product_import_to_chaina_id' => $product_import_to_chaina_id,
                                        // format qr_code = po-product-sort_id/all_qty_product
                                        'qr_code' => $input_all['import_to_chaina']['po_number'].'-'.$product_import_to_chaina_id.'-'.$no.'/'.$qty_product,
                                        'sort_id' => $no,
                                        'created_at' => date('Y-m-d H:i:s'),
                                    ];
                                    if(!empty($product_old_price)){
                                         $data_insert_qrcode['rel_user_type_product_id'] = $product_old_price->rel_user_type_product_id;
                                         $data_insert_qrcode['rel_user_type_product_id_old'] = $product_old_price->rel_user_type_product_id;
                                         $data_insert_qrcode['type_rate'] = $product_old_price->type_rate;
                                    }
                                    \App\Models\QrCodeProduct::insert($data_insert_qrcode);
                                    $no++;
                                }
                                // $start_no = $end_no + 1;
                            }
                        }
                    }
                    // send sms
                    // dd([$data_insert_import_to_chaina['po_no'], 1, $amount]);
                    // user
                    $ImportToChaina = \App\Models\ImportToChaina::find($import_to_chaina_id);
                    \App\Models\ImportToChaina::where('id', $import_to_chaina_id)->update(['status_rate' => $status_default]);
                    \App\Models\ImportToChaina::find($import_to_chaina_id);
                    
                    if(!empty($ImportToChaina)){
                        $User = \App\Models\User::find($ImportToChaina->user_id);
                        if(empty($check_po) && !empty($User) && $User->main_mobile != null){
                            app('App\Http\Controllers\Admin\ManageSmsController')->SendSMS($data_insert_import_to_chaina['po_no'], 1, $amount, $msisdn = $User->main_mobile);
                            \App\Models\SendSMS::insert([
                                'import_to_chaina_id'=> $import_to_chaina_id
                                , 'type_status'=> 1
                                , 'created_at'=> date('Y-m-d H:i:s')
                            ]);
                        }
                    }


                    \DB::commit();
                    // $return['import_to_chaina_id'] = $import_to_chaina_id;
                    $return['status'] = 1;
                    $return['content'] = trans('lang.success');
                } catch (Exception $e) {
                    \DB::rollBack();
                    $return['status'] = 0;
                    $return['content'] = trans('lang.unsuccessful').$e->getMessage();;
                }
            }else{
                $return['status'] = 0;
            }
        }else{
            $return['status'] = 0;
            $return['content'] = trans('lang.this_po_number_can_not_be_added');
        }
        $return['title'] = trans('lang.create_data');
        // dd($data_insert_all);
        return json_encode($return);
    	// return 1;
    }

    public function show(Request $request)
    {
        $id = $request->po_id;
        $data['ProductImportToChainas'] = \App\Models\ProductImportToChaina::select(
            'product_import_to_chaina.*'
            , 'product_types.id as product_type_id'
            , 'products.name as product_name', 'products.code as product_code'
            , 'product_types.name as product_type_name', 'product_types.code as product_type_code'
            , 'transport_types.name_th as transport_types_name_th'
            , 'transport_types.name_en as transport_types_name_en'
            , 'transport_types.name_ch as transport_types_name_ch'
            , 'lot_products.product_sort_start', 'lot_products.product_sort_end', 'lot_products.width as lot_product_width'
            , 'lot_products.height as lot_product_height', 'lot_products.length as lot_product_length', 'lot_products.weight_per_item as lot_product_weight_per_item'
            , 'lot_products.weight_all as lot_product_weight_all', 'lot_products.cubic as lot_product_cubic', 'lot_products.remark as lot_product_remark'
            , \DB::raw('
                (select rel_user_type_product_id
                from tb_qr_code_products
                where tb_lot_products.product_import_to_chaina_id = tb_qr_code_products.product_import_to_chaina_id
                and tb_qr_code_products.sort_id BETWEEN tb_lot_products.product_sort_start AND tb_lot_products.product_sort_end
                order by tb_qr_code_products.id asc
                limit 1
                ) as rel_user_type_product_id
            ')
        )
        ->where('import_to_chaina_id', $id)
        ->join('products','products.id','=','product_import_to_chaina.product_id')
        ->join('product_types','product_types.id','=','product_import_to_chaina.product_type_id')
        ->join('transport_types','transport_types.id','=','product_import_to_chaina.transport_type_id')
        ->join('lot_products','lot_products.product_import_to_chaina_id','=','product_import_to_chaina.id')
        ->orderBy('product_import_to_chaina_id', 'asc')
        // ->with('LotProduct')
        ->get();
        return json_encode($data);
    }

    public function edit($id)
    {
    	$data['main_menu'] = 'ImportToChaina';
    	$data['sub_menu'] = '';
    	$data['title'] = 'แก้ไขรับของเข้า (โกดังจีน)';
    	$data['title_page'] = 'แก้ไขรับของเข้า (โกดังจีน)';
    	$data['permission'] = Permission::CheckPermission($data['main_menu'],$data['sub_menu']);
    	$data['menus'] = \App\Models\AdminMenu::ActiveMenu()->get();
        $data['Users'] = \App\Models\User::whereNotNull('customer_general_code')->get();
        $data['ProductTypes'] = \App\Models\ProductType::whereNotIn('id', [5,6])->get();
        $data['Products'] = \App\Models\Product::get();
        $data['ProductImportToChainas'] = \App\Models\ProductImportToChaina::select(
                'product_import_to_chaina.*'
                , 'product_types.id as product_type_id'
                , 'products.name as product_name', 'products.code as product_code'
                , 'product_types.name as product_type_name', 'product_types.code as product_type_code'
                , 'transport_types.name_th as transport_types_name_th'
                , 'transport_types.name_en as transport_types_name_en'
                , 'transport_types.name_ch as transport_types_name_ch'
                , 'lot_products.product_sort_start', 'lot_products.product_sort_end', 'lot_products.width as lot_product_width'
                , 'lot_products.height as lot_product_height', 'lot_products.length as lot_product_length', 'lot_products.weight_per_item as lot_product_weight_per_item'
                , 'lot_products.weight_all as lot_product_weight_all', 'lot_products.cubic as lot_product_cubic', 'lot_products.remark as lot_product_remark'
                , \DB::raw('
                    (select rel_user_type_product_id
                    from tb_qr_code_products
                    where tb_lot_products.product_import_to_chaina_id = tb_qr_code_products.product_import_to_chaina_id
                    and tb_qr_code_products.sort_id BETWEEN tb_lot_products.product_sort_start AND tb_lot_products.product_sort_end
                    order by tb_qr_code_products.id asc
                    limit 1
                    ) as rel_user_type_product_id
                ')
            )
            ->where('import_to_chaina_id', $id)
            ->join('products','products.id','=','product_import_to_chaina.product_id')
            ->join('product_types','product_types.id','=','product_import_to_chaina.product_type_id')
            ->join('transport_types','transport_types.id','=','product_import_to_chaina.transport_type_id')
            ->join('lot_products','lot_products.product_import_to_chaina_id','=','product_import_to_chaina.id')
            ->orderBy('product_import_to_chaina_id', 'asc')
            // ->with('LotProduct')
            ->get();
        $data['ContainerTypes'] = \App\Models\ContainerType::get();
        $data['TransportationTypes'] = \App\Models\TransportType::get();
        $data['ImportToChaina'] = \App\Models\ImportToChaina::find($id);
        // dd($data['ImportToChaina']);
        return view('Admin.import_to_chaina_edit',$data);
    	// return 3;
    }

    public function update($id, Request $request)
    {
        $input_all = $request->all();
        $import_to_chaina_id = $id;
        // dd($id, $input_all);
        $user = \App\Models\User::select('customer_general_code')->find($input_all['import_to_chaina']['customer_id']);
        // $input_all['import_to_chaina']['po_number'] = $this->runCode();
        // $input_all['import_to_chaina']['created_by'] = auth()->guard('admin')->user()->id;
        $input_all['import_to_chaina']['updated_by'] = auth()->guard('admin')->user()->id;
        // $input_all['import_to_chaina']['created_at'] = date('Y-m-d H:i:s');
        $input_all['import_to_chaina']['updated_at'] = date('Y-m-d H:i:s');
        $input_all['import_to_chaina']['import_to_chaina_date'] = $input_all['import_to_chaina']['import_to_chaina_date'].' '.date('H:i:s');

        unset($input_all['weigh_type']);
        unset($input_all['weigh_format']);

        $validator = Validator::make($request->all(), [
            // 'import_to_chaina[po_number]'   => 'required',
            // 'import_to_chaina[customer_id]'   => 'required',
        ]);
        if (!$validator->fails()) {
            \DB::beginTransaction();
            try {
                $status_default = 'T';
                // delete data before update
                \App\Models\QrCodeProduct::where('import_to_chaina_id', $import_to_chaina_id)->delete();
                $products = \App\Models\ProductImportToChaina::where('import_to_chaina_id', $import_to_chaina_id)->get();
                foreach($products as $product){
                    \App\Models\LotProduct::where('product_import_to_chaina_id', $product->id)->delete();
                }
                \App\Models\ProductImportToChaina::where('import_to_chaina_id', $import_to_chaina_id)->delete();
                // end delete data before update

                $data_insert_import_to_chaina = [
                    // 'po_no' => $input_all['import_to_chaina']['po_number'],
                    'import_date' => $input_all['import_to_chaina']['import_to_chaina_date'],
                    'user_id' => $input_all['import_to_chaina']['customer_id'],
                    'remark' => $input_all['remark'],
                    // 'status_id' => 1,
                    'updated_at' => date('Y-m-d H:i:s'),
                ];
                \App\Models\ImportToChaina::where('id', $import_to_chaina_id)->update($data_insert_import_to_chaina);
                // $import_to_chaina    = \App\Models\ReceiversChin::find($import_to_chaina_id);
                foreach($input_all['product'] as $key1 => $value1){
                    $no = 1;
                    // $start_no = 1;
                    // $end_no = 0;
                    if(!empty($value1['lot_product']) && count($value1['lot_product']) > 0){
                        // set data product
                        if (!empty($value1['product_name'])) {
                            $checkProduct = \App\Models\Product::select('id', 'product_type_id')
                                ->where('name', $value1['product_name'])
                                ->where('code', $value1['product_code'])
                                ->first();
                            if(!empty($checkProduct)){
                                $product_id = $checkProduct->id;
                                $data_insert_all['check_product_type_id'] = $checkProduct->product_type_id .' = '. $value1['product_type_id'];
                                if($checkProduct->product_type_id != $value1['product_type_id']){
                                    $data_update_product= [
                                        'product_type_id' => $value1['product_type_id'],
                                    ];
                                    \App\Models\Product::where('id', $product_id)->update($data_update_product);
                                }
                            } else {
                                $data_insert_product= [
                                    'code' => $value1['product_code'],
                                    'product_type_id' => $value1['product_type_id'],
                                    'name' => $value1['product_name'],
                                    'created_at' => date('Y-m-d H:i:s'),
                                ];
                                $product_id = \App\Models\Product::insertGetId($data_insert_product);
                            }
                        }
                        // end set data product
                        // set price in product
                        $product_old_price = \App\Models\QrCodeProduct::select(
                                'qr_code_products.rel_user_type_product_id',
                                'rel_user_type_products.type_rate'
                            )
                            ->join('product_import_to_chaina', 'qr_code_products.product_import_to_chaina_id', '=', 'product_import_to_chaina.id')
                            ->join('products', 'products.id', '=', 'product_import_to_chaina.product_id')
                            ->join('import_to_chaina', 'import_to_chaina.id', '=', 'qr_code_products.import_to_chaina_id')
                            ->join('rel_user_type_products', 'rel_user_type_products.id', '=', 'qr_code_products.rel_user_type_product_id')
                            ->where('products.product_type_id', $value1['product_type_id'])
                            ->where('import_to_chaina.user_id', $input_all['import_to_chaina']['customer_id'])
                            ->orderBy('product_import_to_chaina.id', 'desc')
                            ->first();
                        // end set price in product
                        $size_wide = $size_high = $size_long = $size_weight_tote = $size_cubic = $qty_product = 0;
                        foreach($value1['lot_product'] as $sum_size){
                            $size_wide += $sum_size['size_wide'];
                            $size_high += $sum_size['size_high'];
                            $size_long += $sum_size['size_long'];
                            $size_weight_tote += $sum_size['size_weight_tote'];
                            $size_cubic += $sum_size['size_cubic'];
                            $qty_product += ($sum_size['product_no_max']-$sum_size['product_no_min'])+1;

                        }
                        $data_insert_all['product'][$key1] = $data_insert_product = [
                            'import_to_chaina_id' => $import_to_chaina_id,
                            'product_id' => $product_id,
                            'product_type_id' => $value1['product_type_id'],
                            'transport_type_id' => $value1['transportation_type_id'],
                            'shop_chaina_name' => $value1['chinese_shop'],
                            // 'container_type_id' => $value1['container_type_id'],
                            'detail' => $input_all['import_to_chaina']['customer_id'],
                            'qty' => $qty_product,
                            'pcs' => $value1['pcs'],
                            'width' => $size_wide,
                            'height' => $size_high,
                            'length' => $size_long,
                            'weight_per_item' => ($size_weight_tote / $qty_product),
                            'weight_all' => $size_weight_tote,
                            'cubic' => $size_cubic,
                            'weigh_format' => $value1['weigh_format'],
                            'created_at' => date('Y-m-d H:i:s'),
                        ];

                        if(!empty($product_old_price)){
                            $data_insert_product['status_rate'] = 'T';
                        }else{
                            $data_insert_product['status_rate'] = 'F';
                            $status_default = 'F';
                        }

                        $product_import_to_chaina_id = \App\Models\ProductImportToChaina::insertGetId($data_insert_product);

                        foreach($value1['lot_product'] as $key2 => $value2){
                            // $end_no += $value2['qty'];
                            $data_insert_all['product'][$key1]['lot'][$key2] = $data_insert_lot = [
                                'product_import_to_chaina_id' => $product_import_to_chaina_id,
                                'product_sort_start' => $value2['product_no_min'],
                                'product_sort_end' => $value2['product_no_max'],
                                'width' => $value2['size_wide'],
                                'height' => $value2['size_high'],
                                'length' => $value2['size_long'],
                                'weight_per_item' => ($value2['size_weight_tote'] / $value2['qty']),
                                'weight_all' => $value2['size_weight_tote'],
                                'cubic' => $value2['size_cubic'],
                                'remark' => $value2['decription'],
                                'created_at' => date('Y-m-d H:i:s'),
                            ];
                            $lot_product_id = \App\Models\LotProduct::insertGetId($data_insert_lot);
                            $num = intval($value2['qty']);
                            for($i=0; $i<$num; $i++){
                                $data_insert_all['product'][$key1]['lot'][$key2]['qr_code'][$i] = $data_insert_qrcode = [
                                    // 'lot_product_id' => $lot_product_id,
                                    'import_to_chaina_id' => $import_to_chaina_id,
                                    // 'rel_user_type_product_id' => $value1['rel_user_type_product_id'],
                                    'product_import_to_chaina_id' => $product_import_to_chaina_id,
                                    'qr_code' => $input_all['import_to_chaina']['po_number'].'-'.$product_import_to_chaina_id.'-'.$no.'/'.$num,
                                    'sort_id' => $no,
                                    'created_at' => date('Y-m-d H:i:s'),
                                ];
                                if(!empty($product_old_price)){
                                     $data_insert_qrcode['rel_user_type_product_id'] = $product_old_price->rel_user_type_product_id;
                                     $data_insert_qrcode['rel_user_type_product_id_old'] = $product_old_price->rel_user_type_product_id;
                                     $data_insert_qrcode['type_rate'] = $product_old_price->type_rate;
                                }
                                \App\Models\QrCodeProduct::insert($data_insert_qrcode);
                                $no++;
                            }
                            // $start_no = $end_no + 1;
                        }
                    }
                }
                \App\Models\ImportToChaina::where('id', $import_to_chaina_id)->update(['status_rate' => $status_default]);
                \DB::commit();
                // $return['import_to_chaina_id'] = $import_to_chaina_id;
                $return['status'] = 1;
                $return['content'] = trans('lang.success');
            } catch (Exception $e) {
                \DB::rollBack();
                $return['status'] = 0;
                $return['content'] = trans('lang.the_product_may_already_be_used_and_can_not_be_repaired');
            }
        }else{
            $return['status'] = 0;
        }
        $return['title'] = trans('lang.edit_data');
        // dd($data_insert_all);
        return json_encode($return);
    	// return 1;
    }

    public function Lists(Request $request)
    {
        $result = \App\Models\ImportToChaina::select(
                'import_to_chaina.*'
                , 'import_to_chaina_statuses.color as status_color'
                , 'import_to_chaina_statuses.name as status_name'
                , 'import_to_chaina_statuses.number as status_number'
                , 'users.customer_general_code as customer_general_code'
                , 'transport_types.name_th as transport_type_name_th'
                , 'transport_types.name_en as transport_type_name_en'
                , 'transport_types.name_ch as transport_type_name_ch'
                , \DB::raw('sum(tb_product_import_to_chaina.cubic) cubic_all_PO')
                , \DB::raw('sum(tb_product_import_to_chaina.weight_all) weight_all_PO')
                , \DB::raw('sum(tb_product_import_to_chaina.qty) qty_all_PO')
                , \DB::raw('
                    (   select lot2.id
                        from tb_product_import_to_chaina product2
                        join tb_lot_products lot2 on product2.id = lot2.product_import_to_chaina_id
                        where lot2.weight_all IS NULL
                        and product2.import_to_chaina_id = tb_import_to_chaina.id
                        limit 1
                    ) as status_weight
                ')
            )
            ->join('users', 'users.id', '=', 'import_to_chaina.user_id')
            ->join('import_to_chaina_statuses', 'import_to_chaina_statuses.id', '=', 'import_to_chaina.status_id')
            ->join('product_import_to_chaina', 'product_import_to_chaina.import_to_chaina_id', '=', 'import_to_chaina.id')
            ->leftjoin('transport_types', 'transport_types.id', '=', 'product_import_to_chaina.transport_type_id')
            ->where('import_to_chaina.status_id', '1')
            ->groupBy('import_to_chaina.id')
            ->groupBy('import_to_chaina_statuses.color')
            ->groupBy('import_to_chaina_statuses.name')
            ->groupBy('import_to_chaina_statuses.number')
            ->groupBy('users.customer_general_code')
            ->groupBy('import_to_chaina.user_id')
            ->groupBy('import_to_chaina.payment_status')
            ->groupBy('import_to_chaina.po_no')
            ->groupBy('import_to_chaina.import_date')
            ->groupBy('import_to_chaina.status_id')
            ->groupBy('import_to_chaina.created_at')
            ->groupBy('import_to_chaina.updated_at');
        return \Datatables::of($result)
        ->addIndexColumn()
        ->editColumn('status_id', function($rec){
            //(x/y) AAAAA mm:ss-DM'
            $number = $rec->status_number;
            $name   = $rec->status_name;
            // $check_date = 'created_at';
            $date_status = $rec->created_at;
            if(!empty($rec->updated_at)){
                $date_status = $rec->updated_at;
                // $check_date = 'updated_at';
            }
            $time   = date_format($date_status, "H:i");
            $day    = date_format($date_status, "d");
            $month  = showM()[date_format($date_status, "n")];

            $color_status = (!empty($rec->status_color) ? $rec->status_color : '#000');
            return '<label class="label" style="background-color: '. $color_status .';">('.$number.'/5)'.$name.' '.$time.' - '.$day.' '.$month.'</label>';
        })
        ->editColumn('weight_all_PO', function($rec){
            $weight_all_PO = (!empty($rec->weight_all_PO) ? number_format($rec->weight_all_PO, 2) : trans('lang.Have_not_weighed'));
            return $weight_all_PO;
        })
        ->editColumn('qty_all_PO', function($rec){
            $qty_all_PO = number_format($rec->qty_all_PO);
            return $qty_all_PO;
        })
        ->editColumn('status_weight',function($rec){
            $str = '<label class="label" style="background-color: green;">'.trans('lang.weigh_complete').'</label>';
            if(!empty($rec->status_weight)){
                $str = '<label class="label" style="background-color: orange;">'.trans('lang.weigh_not_complete').'</label>';
            }
            return $str;
        })
        ->addColumn('type_delivery', function($rec){
            $lang = \Config::get('app.locale');
            $type_delivery = '';
            if($lang == 'th'){
                $type_delivery = $rec->transport_type_name_th;
            }elseif($lang == 'ch'){
                $type_delivery = $rec->transport_type_name_ch;
            }else{
                $type_delivery = $rec->transport_type_name_en;
            }

            return $type_delivery;
        })
        ->addColumn('action', function($rec){
            $lang = \Config::get('app.locale');

            $str='
                <a data-loading-text="<i class=\'fa fa-refresh fa-spin\'></i>" class="btn btn-xs btn-success btn-condensed btn-show_detail btn-tooltip" data-rel="tooltip" data-id="'. $rec->id .'" title="'.trans('lang.view_detail').'">
                    <i class="ace-icon fa fa-search bigger-120"></i>
                </a>
                <a href="'. url('admin/'.$lang.'/ImportToChaina/edit/'. $rec->id) .'" data-loading-text="<i class=\'fa fa-refresh fa-spin\'></i>" class="btn btn-xs btn-warning btn-condensed btn-edit btn-tooltip" data-rel="tooltip" data-id="'. $rec->id .'" title="'.trans('lang.edit').'">
                    <i class="ace-icon fa fa-edit bigger-120"></i>
                </a>
                <a href="'. url('admin/'.$lang.'/ImportToChaina/WeighLot/'. $rec->id) .'" data-loading-text="<i class=\'fa fa-refresh fa-spin\'></i>" class="btn btn-xs btn-warning btn-condensed btn-weighlot btn-tooltip" data-rel="tooltip" data-id="'. $rec->id .'" title="'.trans('lang.weigh_lot').'">
                    <i class="ace-icon fa fa-sitemap bigger-120"></i>
                </a>
                <a href="'. url('admin/'.$lang.'/ImportToChaina/QRCode/'. $rec->id) .'" target="_blank" data-loading-text="<i class=\'fa fa-refresh fa-spin\'></i>" class="btn btn-xs btn-warning btn-condensed btn-edit btn-tooltip" data-rel="tooltip" data-id="'. $rec->id .'" title="'.trans('lang.print_qrcode').'">
                    <i class="fa fa-qrcode"></i>
                </a>
                <button  class="btn btn-xs btn-danger btn-condensed btn-delete btn-tooltip" data-id="'. $rec->id .'" data-rel="tooltip" title="'.trans('lang.delete').'">
                    <i class="ace-icon fa fa-trash bigger-120"></i>
                </button>
            ';
            return $str;
        })
        ->rawColumns(['status_id', 'action', 'status_weight'])
        ->make(true);
    }

    public function destroy($id)
    {
        \DB::beginTransaction();
        try {
            \App\Models\QrCodeProduct::where('import_to_chaina_id', $id)->delete();
            $products = \App\Models\ProductImportToChaina::where('import_to_chaina_id', $id)->get();
            foreach($products as $product){
                \App\Models\LotProduct::where('product_import_to_chaina_id', $product->id)->delete();
            }
            \App\Models\ProductImportToChaina::where('import_to_chaina_id', $id)->delete();
            \App\Models\ImportToChaina::where('id', $id)->delete();
            \DB::commit();
            $return['status'] = 1;
            $return['content'] = trans('lang.success');
        } catch (Exception $e) {
            \DB::rollBack();
            $return['status'] = 0;
            $return['content'] = trans('lang.unsuccessful').$e->getMessage();;
        }
        $return['title'] = trans('lang.delete_data');
        return $return;
    }

    public function QRCode($id)
    {
        $data['QRCodes'] = \App\Models\QrCodeProduct::select(
                'qr_code_products.*'
                , 'lot_products.id as lot_product_id'
                , 'lot_products.id as lot_product_id'
                , 'transport_types.name_th as transport_type_name_th'
                , 'transport_types.name_en as transport_type_name_en'
                , 'transport_types.name_ch as transport_type_name_ch'
                , \DB::raw('(`tb_lot_products`.`product_sort_end` - `tb_lot_products`.`product_sort_start`) + 1 as lot_product_qty')
            )
            // join lot product
            ->join('lot_products', 'lot_products.product_import_to_chaina_id', '=', 'qr_code_products.product_import_to_chaina_id')
            ->join('product_import_to_chaina', 'product_import_to_chaina.id', '=', 'qr_code_products.product_import_to_chaina_id')
            ->join('transport_types', 'transport_types.id', '=', 'product_import_to_chaina.transport_type_id')
            ->whereRaw('`tb_qr_code_products`.`sort_id` BETWEEN `tb_lot_products`.`product_sort_start` AND `tb_lot_products`.`product_sort_end`')
            //end join lot product
            ->where('qr_code_products.import_to_chaina_id', $id)
            ->orderBy('lot_products.id')
            ->orderBy('qr_code_products.sort_id')
            ->get();
        $data['PO'] = \App\Models\ImportToChaina::select(
                'import_to_chaina.*'
                , 'users.customer_general_code'
            )
            ->join('users','users.id','=','import_to_chaina.user_id')
            ->withCount('QrCodeProduct')
            ->where('import_to_chaina.id', $id)
            ->first();
        $data2 = view('Admin.pdf_qrcode_import_chaina', $data);

        $mpdf = new Mpdf([
            'autoLangToFont' => true,
            'mode' => 'utf-8',
            'format' => [101.6, 152.4],
            'margin_top' => 0,
            'margin_left' => 0,
            'margin_right' => 0,
            'margin_bottom' => 0,
        ]);
        // $mpdf->setHtmlHeader('<div style="text-align: right; width: 100%;">{PAGENO}</div>');
        $mpdf->WriteHTML($data2);
        $mpdf->Output('QrCode_'. $id .'_'. date('Y_m_d') .'.pdf', 'I');
    }

    public function runCode()
    {
        $year = date('ym', strtotime('now'));
        $month = date('m', strtotime('now'));
        $num = 0;
        $cnt = 0;
        $rs = \App\Models\ImportToChaina::orderBy('id', 'DESC')->first();

        if($rs){
            $po_no	= $rs->po_no;  // PO-170600001
            $ckmonth2 = substr($po_no, 5, 2);//06
            $num = substr($po_no, 8, 5);
            if($month == $ckmonth2){
                $cnt = $num+1;
                $shownum = sprintf("%05d", $cnt);
            }
            else{
                $cnt = 0;
				$cnt++;
                $shownum = sprintf("%05d", $cnt);
            }
        }
        else{
            $cnt = 0;
			$cnt++;
            $shownum = sprintf("%05d", $cnt);
        }
         return 'PO-'.$year.$shownum;
    }
    // ชั่งน้ำหนัก สินค้า
    public function WeighLot($id)
    {
        $data['ImportToChaina'] = \App\Models\ImportToChaina::with('User')
            ->where('id', $id)
            ->first();
        $data['ProductImportChaina'] = \App\Models\ProductImportToChaina::select(
                'product_import_to_chaina.*'
                , 'product_types.id as product_type_id'
                , 'products.name as product_name', 'products.code as product_code'
                , 'product_types.name as product_type_name', 'product_types.code as product_type_code'
                , 'transport_types.name_th as transport_types_name_th'
                , 'transport_types.name_en as transport_types_name_en'
                , 'transport_types.name_ch as transport_types_name_ch'
                // table tb_lot_products
                , 'lot_products.id as lot_product_id', 'lot_products.product_sort_start', 'lot_products.product_sort_end'
                , 'lot_products.width as lot_product_width', 'lot_products.height as lot_product_height', 'lot_products.length as lot_product_length'
                , 'lot_products.weight_per_item as lot_product_weight_per_item', 'lot_products.weight_all as lot_product_weight_all', 'lot_products.cubic as lot_product_cubic'
                , 'lot_products.remark as lot_product_remark'
                // table container_types
                // , 'container_types.name as container_type_name'
            )
            // ->with('LotProduct')
            ->join('lot_products','lot_products.product_import_to_chaina_id','=','product_import_to_chaina.id')
            ->join('products','products.id','=','product_import_to_chaina.product_id')
            ->join('product_types','product_types.id','=','product_import_to_chaina.product_type_id')
            ->join('transport_types','transport_types.id','=','product_import_to_chaina.transport_type_id')
            // ->join('container_types','container_types.id','=','product_import_to_chaina.container_type_id')
            ->where('import_to_chaina_id', $id)
            ->get();
        $data['main_menu'] = 'ImportToChaina';
        $data['sub_menu'] = 'WeighingLot';
        $data['title_page'] = trans('lang.weighing_lot');
        $data['menus'] = \App\Models\AdminMenu::ActiveMenu()->get();
        $data['Users'] = \App\Models\User::whereNotNull('customer_general_code')->get();
        // $data['ProductTypes'] = \App\Models\ProductType::get();
        // $data['ContainerTypes'] = \App\Models\ContainerType::get();
        // $data['TransportTypes'] = \App\Models\TransportType::get();
        // $product_min = [];
        // $product_max = [];
        // foreach($data['ProductImportChaina'] as $ProductImportChaina_first){
        //     foreach($ProductImportChaina_first->LotProduct as $LotProduct_first){
        //         if(empty($LotProduct_first->weight_all)){
        //             $product_min[$ProductImportChaina_first->id] = (!empty($LotProduct_first->product_sort_start) ? $LotProduct_first->product_sort_start : 0 );
        //             $product_max[$ProductImportChaina_first->id] = (!empty($LotProduct_first->product_sort_end) ? $LotProduct_first->product_sort_end : 0 );
        //             $setfirst = 0;
        //         }
        //     }
        // }
        //
        // $data['rel_lot_product_min'] = $product_min;
        // $data['rel_lot_product_max'] = $product_max;
        // dd($data);
        return view('Admin.weighing_lot', $data);
    }

    public function WeighLotUpdate($id, Request $request)
    {
        $input_all = $request->all();
        // dd($input_all);
        $import_to_chaina_id = $id;
        $validator = Validator::make($request->all(), [
            // 'import_to_chaina[po_number]'   => 'required',
            // 'import_to_chaina[customer_id]'   => 'required',
        ]);
        if (!$validator->fails()) {
            \DB::beginTransaction();
            try {
                if(isset($input_all['product'])){
                    foreach($input_all['product'] as $key1 => $value1){
                        $no = 1;
                        $start_no = 1;
                        $end_no = 0;
                        if(!empty($value1['lot_product']) && count($value1['lot_product']) > 0){
                            $size_wide = $size_high = $size_long = $size_weight_tote = $size_cubic = $qty_product = 0;
                            foreach($value1['lot_product'] as $sum_size){
                                $size_wide += $sum_size['size_wide'];
                                $size_high += $sum_size['size_high'];
                                $size_long += $sum_size['size_long'];
                                $size_weight_tote += $sum_size['size_weight_tote'];
                                $size_cubic += $sum_size['size_cubic'];
                                $qty_product += ($sum_size['product_no_max']-$sum_size['product_no_min'])+1;
                            }
                            $data_update_all['product'][$key1] = $data_update_product = [
                                'width' => $size_wide,
                                'height' => $size_high,
                                'length' => $size_long,
                                'weight_per_item' => ($size_weight_tote / $qty_product),
                                'weight_all' => $size_weight_tote,
                                'cubic' => $size_cubic,
                                'updated_at' => date('Y-m-d H:i:s'),
                            ];
                            \App\Models\ProductImportToChaina::where('id', $value1['product_id'])->update($data_update_product);
                            foreach($value1['lot_product'] as $key2 => $value2){
                                // $end_no += $value2['qty'];
                                $data_update_all['product'][$key1]['lot'][$key2] = $data_update_lot = [
                                    'product_sort_start' => $value2['product_no_min'],
                                    'product_sort_end' => $value2['product_no_max'],
                                    'width' => $value2['size_wide'],
                                    'height' => $value2['size_high'],
                                    'length' => $value2['size_long'],
                                    'weight_per_item' => ($value2['size_weight_tote'] / (($sum_size['product_no_max']-$sum_size['product_no_min'])+1)),
                                    'weight_all' => $value2['size_weight_tote'],
                                    'cubic' => $value2['size_cubic'],
                                    'remark' => $value2['decription'],
                                    'updated_at' => date('Y-m-d H:i:s'),
                                ];
                                if($value2['product_no_max_old'] == $value2['product_no_max']){
                                    \App\Models\LotProduct::where('id', $value2['lot_product_id'])->update($data_update_lot);
                                }else{
                                    $data_update_all['product'][$key1]['lot'][$key2] = $data_update_lot = [
                                        'product_import_to_chaina_id' => $value1['product_id'],
                                        'product_sort_start' => $value2['product_no_min'],
                                        'product_sort_end' => $value2['product_no_max'],
                                        'width' => $value2['size_wide'],
                                        'height' => $value2['size_high'],
                                        'length' => $value2['size_long'],
                                        'weight_per_item' => ($value2['size_weight_tote'] / (($sum_size['product_no_max']-$sum_size['product_no_min'])+1)),
                                        'weight_all' => $value2['size_weight_tote'],
                                        'cubic' => $value2['size_cubic'],
                                        'remark' => $value2['decription'],
                                        'created_at' => date('Y-m-d H:i:s'),
                                        'updated_at' => date('Y-m-d H:i:s'),
                                    ];
                                    \App\Models\LotProduct::insert($data_update_lot);
                                }
                                // $start_no = $end_no + 1;
                            }
                        }
                    }
                }else{
                    $return['title'] = trans('lang.weighing_data');
                    $return['content'] = trans('lang.unsuccessful');
                    $return['status'] = 0;

                    return $return;
                }

                \DB::commit();
                // $return['import_to_chaina_id'] = $import_to_chaina_id;
                $return['status'] = 1;
                $return['content'] = trans('lang.success');
            } catch (Exception $e) {
                \DB::rollBack();
                $return['status'] = 0;
                $return['content'] = trans('lang.unsuccessful').$e->getMessage();;
            }
        }else{
            $return['status'] = 0;
        }
        $return['title'] = trans('lang.weighing_data');

        return json_encode($return);
    }

    public function GetPrice(Request $request)
    {
        $data['price'] = \App\Models\RelUserTypeProduct::select('price', 'id')
            ->where('product_type_id', $request->input('product_type_id'))
            ->where('user_id', $request->input('customer_id'))
            ->get();
        // $data['price'] = $request->all();
        return json_encode($data);
    }

}
