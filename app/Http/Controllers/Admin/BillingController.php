<?php

namespace App\Http\Controllers\Admin;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Storage;
use \Mpdf\Mpdf;
use DB;

class BillingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['main_menu'] = 'Billing';
        $data['sub_menu'] = 'Billing';
        $data['title_page'] = trans('lang.export_billing');
        $data['menus'] = \App\Models\AdminMenu::ActiveMenu()->get();
        $data['Users'] = \App\Models\User::where('customer_general_code', '!=', null)->get();

        return view('Admin.billing',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['main_menu'] = 'Billing';
        $data['sub_menu'] = 'Billing';
        $data['title_page'] = trans('lang.export_billing');
        $data['menus'] = \App\Models\AdminMenu::ActiveMenu()->get();
        $data['Users'] = \App\Models\User::where('customer_general_code', '!=', null)->get();
        $data['BillingNo'] = $this->runCode();

        return view('Admin.billing_create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!$request->has('billing_list')){
            $return['status'] = 0;
            $return['title'] = trans('lang.unsuccessful');
            $return['content'] = trans('lang.please_select_items_that_po_ship_to_complete');
            return $return;

        }

        $input_all = $request->all();
        $input_all['billing']['created_at'] = date('Y-m-d H:i:s');
        $input_all['billing']['updated_at'] = date('Y-m-d H:i:s');

        $validator = Validator::make($request->all(), [

        ]);


        if (!$validator->fails()) {
            \DB::beginTransaction();
            try {

                $data_insert = $input_all['billing'];
                $billing_id = \App\Models\Billing::insertGetId($data_insert);
                if(isset($input_all['billing_list'])){
                    $total_sub_price                = 0; //รวมราคาสินค้าทั้งหมด
                    $total_price                    = 0; //ราคาสินค้าทั้งหมด + ค่าจัดส่งทั้งหมด
                    $total_delivery_charge_price    = 0; //รวมราคาค่าจัดส่งทั้งหมด
                    $total_qty                      = 0; //รวมจำนวน
                    $total_cubic                    = 0; //รวมขนาดคิว
                    $total_weight                   = 0; //รวมน้ำหนัก
                    foreach($input_all['billing_list'] as $delivery_pos_id){
                        $delivery_pos = \App\Models\DeliveryPos::find($delivery_pos_id);
                        $input_all2['billing_id']           = $billing_id;
                        $input_all2['delivery_pos_id']      = $delivery_pos_id;
                        $input_all2['import_to_chaina_id']  = $delivery_pos['import_to_chaina_id'];
                        $input_all2['qty']                  = $delivery_pos['qty'];
                        $input_all2['cubic']                = $delivery_pos['cubic'];
                        $input_all2['weight']               = $delivery_pos['weight'];
                        $input_all2['subtotal']             = $delivery_pos['price'];

                        $total_sub_price    += $delivery_pos['price'];
                        $total_qty          += $delivery_pos['qty'];
                        $total_cubic        += $delivery_pos['cubic'];
                        $total_weight       += $delivery_pos['weight'];

                        $data_insert2 = $input_all2;
                        \App\Models\BillingList::insert($data_insert2);
                        \App\Models\DeliveryPos::where('id',$delivery_pos['id'])->update(['billing_status'=>'T']);
                    }
                }

                $total_price = $total_sub_price + $total_delivery_charge_price;
                $input_all3['total_sub_price']              = $total_sub_price;
                $input_all3['total_price']                  = $total_price;
                $input_all3['total_delivery_charge_price']  = $total_delivery_charge_price;
                $input_all3['total_qty']                    = $total_qty;
                $input_all3['total_cubic']                  = $total_cubic;
                $input_all3['total_weight']                 = $total_weight;

                $update_all = $input_all3;
                \App\Models\Billing::where('id',$billing_id)->update($update_all);
                
                \DB::commit();
                $return['status'] = 1;
                $return['content'] = trans('lang.success');
            } catch (Exception $e) {
                \DB::rollBack();
                $return['status'] = 0;
                $return['content'] = trans('lang.unsuccessful').$e->getMessage();
            }
        }else{
            $return['status'] = 0;
        }
        $return['title'] = trans('lang.create_data');
        return json_encode($return);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = \App\Models\Billing::find($id);

        return json_encode($result);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['main_menu'] = 'Billing';
        $data['sub_menu'] = 'Billing';
        $data['title_page'] = trans('lang.export_billing');
        $data['menus'] = \App\Models\AdminMenu::ActiveMenu()->get();
        $data['Users'] = \App\Models\User::get();
        $data['Billing'] = \App\Models\Billing::find($id);

        $data['BillingListsT'] = \App\Models\BillingList::select(
                        'users.customer_general_code'
                        ,'dalivery_sheets.delivery_no'
                        ,'dalivery_sheets.delivery_slip_date'
                        ,'import_to_chaina.po_no'
                        ,'import_to_chaina.import_date'
                        ,'billing_lists.id as billing_list_id'
                        ,'billing_lists.qty'
                        ,'billing_lists.weight'
                        ,'billing_lists.cubic'
                        ,'billing_lists.subtotal'
                        ,'billing_lists.status'
                        ,'import_to_chaina_statuses.name'
                    )
                    ->leftjoin('import_to_chaina', 'import_to_chaina.id', '=', 'billing_lists.import_to_chaina_id')
                    ->leftjoin('import_to_chaina_statuses', 'import_to_chaina_statuses.id', '=', 'import_to_chaina.status_id')
                    ->leftjoin('delivery_pos', 'delivery_pos.id', '=','billing_lists.delivery_pos_id')
                    ->leftjoin('dalivery_sheets' ,'dalivery_sheets.id', '=','delivery_pos.delivery_sheet_id')
                    ->leftjoin('users', 'users.id', '=', 'dalivery_sheets.user_id')
                    ->where('billing_lists.status', 'T')
                    ->where('billing_lists.billing_id', $id)
                    ->get();

        $data['BillingListsF'] = \App\Models\BillingList::select(
                        'users.customer_general_code'
                        ,'dalivery_sheets.delivery_no'
                        ,'dalivery_sheets.delivery_slip_date'
                        ,'import_to_chaina.po_no'
                        ,'import_to_chaina.import_date'
                        ,'billing_lists.id as billing_list_id'
                        ,'billing_lists.qty'
                        ,'billing_lists.weight'
                        ,'billing_lists.cubic'
                        ,'billing_lists.subtotal'
                        ,'billing_lists.status'
                        ,'import_to_chaina_statuses.name'
                    )
                    ->leftjoin('import_to_chaina', 'import_to_chaina.id', '=', 'billing_lists.import_to_chaina_id')
                    ->leftjoin('import_to_chaina_statuses', 'import_to_chaina_statuses.id', '=', 'import_to_chaina.status_id')
                    ->leftjoin('delivery_pos', 'delivery_pos.id', '=','billing_lists.delivery_pos_id')
                    ->leftjoin('dalivery_sheets' ,'dalivery_sheets.id', '=','delivery_pos.delivery_sheet_id')
                    ->leftjoin('users', 'users.id', '=', 'dalivery_sheets.user_id')
                    ->where('billing_lists.status', 'F')
                    ->where('billing_lists.billing_id', $id)
                    ->get();

        return view('Admin.billing_edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input_all = $request->all();

            if(isset($input_all['total'])){
                $input_all['total'] = str_replace(',', '', $input_all['total']);
            }

        $input_all['updated_at'] = date('Y-m-d H:i:s');

        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
             'billing_no' => 'required',
             'billing_date' => 'required',
             'total' => 'required',

        ]);
        if (!$validator->fails()) {
            \DB::beginTransaction();
            try {
                $data_insert = $input_all;
                \App\Models\Billing::where('id',$id)->update($data_insert);
                \DB::commit();
                $return['status'] = 1;
                $return['content'] = trans('lang.success');
            } catch (Exception $e) {
                \DB::rollBack();
                $return['status'] = 0;
                $return['content'] = trans('lang.unsuccessful').$e->getMessage();
            }
        }else{
            $return['status'] = 0;
        }
        $return['title'] = trans('lang.create_data');
        return json_encode($return);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();
        try {
            \App\Models\BillingList::where('billing_id',$id)->delete();
            \App\Models\Billing::where('id',$id)->delete();
            \DB::commit();
            $return['status'] = 1;
            $return['content'] = trans('lang.success');
        } catch (Exception $e) {
            \DB::rollBack();
            $return['status'] = 0;
            $return['content'] = trans('lang.unsuccessful').$e->getMessage();
        }
        $return['title'] = 'ลบข้อมูล';
        return $return;
    }

    public function Lists(Request $request)
    {
        $text_lang = $request->input('lang');

        $result = \App\Models\Billing::select('users.firstname'
                                            ,'users.lastname'
                                            ,'users.customer_general_code'
                                            ,'billings.billing_no'
                                            ,'billings.id'
                                            ,'billings.status'
                                            ,'billings.payment_status'
                                            ,'billings.total_price'
                                            ,'billings.billing_date')
                                        ->join('users', 'users.id', '=', 'billings.user_id');

        return \Datatables::of($result)

        ->editColumn('total_price', function($rec){
            return number_format($rec->total_price, 2);
        })

        ->addColumn('status', function($rec){
            if($rec->status == 'T'){
                return '<span class="badge badge-success">'.trans('lang.close_bill_complete').'</span>';
            }else{
                return '<span class="badge badge-danger">'.trans('lang.not_close_bill').'</span>';
            }
        })

        ->addColumn('payment_status', function($rec){
            if($rec->payment_status == 'T'){
                return '<span class="badge badge-success">'.trans('lang.payment_complete').'</span>';
            }else{
                return '<span class="badge badge-danger">'.trans('lang.payment_not_complete').'</span>';
            }
        })

        ->addColumn('user', function($rec){
            return $rec->firstname.' '.$rec->lastname;
        })

        ->addColumn('action',function($rec){
            $lang = \Config::get('app.locale');
            if($rec->status == 'T'){
                $disabled = 'disabled';
            }else{
                $disabled = '';
            }

            if($rec->payment_status == 'T'){
                $disabled_payment_status = 'disabled';
            }else{
                $disabled_payment_status = '';
            }
            $str='
                <a href="'.url('/admin/'.$lang.'/Billing/Edit/'.$rec->id).'" data-loading-text="<i class=\'fa fa-refresh fa-spin\'></i>" class="btn btn-xs btn-warning btn-condensed btn-tooltip" data-rel="tooltip" data-id="'.$rec->id.'" title="'.trans('lang.edit').'">
                    <i class="ace-icon fa fa-edit bigger-120"></i>
                </a>
                <button style="border-color: #364150;" class="btn btn-xs btn-condensed btn-ststus btn-tooltip" data-id="'.$rec->id.'" data-rel="tooltip" title="'.trans('lang.change_status').'" '.$disabled_payment_status.'>
                    <i class="ace-icon fa fa-cog bigger-120"></i>
                </button>
                <a href="'.url('/admin/Billing/Prints/'.$rec->id).'" target="_blank" class="btn btn-xs btn-info btn-condensed btn-print btn-tooltip" data-id="12" data-rel="tooltip" title="" data-original-title="'.trans('lang.print_billing').'">
                    <i class="ace-icon fa fa-print bigger-120"></i>
                </a>
                <a href="'.url('/admin/Billing/Excel/'.$rec->id).'" class="btn btn-xs btn-success btn-condensed btn-print btn-tooltip" data-id="12" data-rel="tooltip" title="" data-original-title="'.trans('lang.export_excel').'">
                    <i class="ace-icon fa fa-list bigger-120"></i>
                </a>

            ';
            return $str;
        })->rawColumns(['status', 'payment_status', 'action'])->make(true);
    }

    public function GetUserPo($id)
    {
        $return['DeliverySlips'] = \App\Models\DeliverySlip::with(
                                        'Delivery'
                                        ,'User'
                                        ,'DeliverySlipStatus'
                                    )
                                    ->where('user_id', $id)
                                    ->where('status_id', 2)
                                    ->where('status_payment', 'F')
                                    ->get();

        $return['DeliveryPos'] = \App\Models\DeliveryPos::select(
                                    'delivery_pos.id as delivery_pos_id'
                                    ,'delivery_pos.qty'
                                    ,'delivery_pos.cubic'
                                    ,'delivery_pos.weight'
                                    ,'delivery_pos.price'
                                    ,'dalivery_sheets.delivery_slip_date'
                                    ,'dalivery_sheets.delivery_no'
                                    ,'import_to_chaina.id as import_to_chaina_id'
                                    ,'import_to_chaina.po_no'
                                    ,'import_to_chaina.import_date'
                                    ,'import_to_chaina.status_id'
                                    ,'import_to_chaina_statuses.name as status_name'
                                    ,'users.customer_general_code'
                                )
                                ->leftjoin('dalivery_sheets', 'dalivery_sheets.id', '=', 'delivery_pos.delivery_sheet_id')
                                ->leftjoin('import_to_chaina', 'import_to_chaina.id', '=', 'delivery_pos.import_to_chaina_id')
                                ->leftjoin('import_to_chaina_statuses', 'import_to_chaina_statuses.id', '=', 'import_to_chaina.status_id')
                                ->leftjoin('users', 'users.id', '=', 'dalivery_sheets.user_id')
                                ->where('dalivery_sheets.user_id', $id)
                                ->where('delivery_pos.payment_status', 'F')
                                ->where('delivery_pos.billing_status', 'F')
                                ->get();

        $return['ImportToChainas'] = \App\Models\ImportToThai::select(
            'import_to_chaina.id as import_to_chaina_id'
            ,'import_to_chaina.import_date'
            ,'import_to_chaina.id as import_to_chaina_id'
            ,'users.customer_general_code'
            ,'import_to_chaina.user_id'
            ,'import_to_chaina.po_no'
            ,'import_to_chaina.status_id'
            ,'import_to_chaina.updated_at as updated_at'
            ,'import_to_chaina_statuses.number as import_to_chaina_ststus_number'
            ,'import_to_chaina_statuses.name as status_name'
            ,\DB::raw('(select count(*) from tb_qr_code_products
                    join tb_rel_container_products on tb_rel_container_products.qr_code_product_id = tb_qr_code_products.id
                    where tb_qr_code_products.import_to_chaina_id = tb_import_to_chaina.id
                ) as qty_container')
            ,\DB::raw('(select sum(`tb_lot_products`.`weight_per_item`)
                    from `tb_qr_code_products`
                    join `tb_rel_container_products` on `tb_rel_container_products`.`qr_code_product_id` = `tb_qr_code_products`.`id`
                    join `tb_lot_products` ON `tb_lot_products`.`product_import_to_chaina_id` = `tb_qr_code_products`.`product_import_to_chaina_id`
                    where `tb_qr_code_products`.`sort_id` BETWEEN `tb_lot_products`.`product_sort_start` and `tb_lot_products`.`product_sort_end`
                    and `tb_qr_code_products`.`import_to_chaina_id` = `tb_import_to_chaina`.`id`
                ) as weight')
            ,\DB::raw('(select sum( `tb_lot_products`.`cubic` / ((`tb_lot_products`.`product_sort_end` - `tb_lot_products`.`product_sort_start`)+1) )
                    from `tb_qr_code_products`
                    join `tb_rel_container_products` on `tb_rel_container_products`.`qr_code_product_id` = `tb_qr_code_products`.`id`
                    join `tb_lot_products` ON `tb_lot_products`.`product_import_to_chaina_id` = `tb_qr_code_products`.`product_import_to_chaina_id`
                    where `tb_qr_code_products`.`sort_id` BETWEEN `tb_lot_products`.`product_sort_start` and `tb_lot_products`.`product_sort_end`
                    and `tb_qr_code_products`.`import_to_chaina_id` = `tb_import_to_chaina`.`id`
                ) as cubic')
            ,\DB::raw('(SELECT SUM(`tb_rel_user_type_products`.`price` * `tb_lot_products`.`weight_per_item`)
                    FROM `tb_qr_code_products`
                    JOIN `tb_rel_container_products` ON `tb_rel_container_products`.`qr_code_product_id` = `tb_qr_code_products`.`id`
                    JOIN `tb_rel_user_type_products` ON `tb_rel_user_type_products`.`id` = `tb_qr_code_products`.`rel_user_type_product_id`
                    JOIN `tb_lot_products` ON `tb_lot_products`.`product_import_to_chaina_id` = `tb_qr_code_products`.`product_import_to_chaina_id`
                    WHERE `tb_qr_code_products`.`sort_id` BETWEEN `tb_lot_products`.`product_sort_start` AND `tb_lot_products`.`product_sort_end`
                    AND `tb_qr_code_products`.`import_to_chaina_id` = `tb_import_to_chaina`.`id`
                ) as price')
        )

        ->leftjoin('import_to_chaina','import_to_chaina.id','=','import_to_thai.import_to_chaina_id')
        ->leftjoin('import_to_chaina_statuses','import_to_chaina_statuses.id','=','import_to_chaina.status_id')
        ->leftjoin('containers','containers.id','=','import_to_thai.container_id')
        ->leftjoin('users','users.id','=','import_to_chaina.user_id')
        ->whereIn('import_to_chaina.status_id', [2,3,4]) //2=ปิดตู้, 3=จัดส่งเข้าไทย, 4=รับสินค้าเข้าโกดัง (ไทย)
        ->where('import_to_chaina.user_id', $id)
        ->get();

        return $return;
    }

    public function Prints($id)
    {
        $data['Billing'] = \App\Models\Billing::select(
                                    'billings.billing_no as billing_no'
                                    ,'billings.billing_date as billing_date'
                                    ,'billings.total_qty as total_qty'
                                    ,'billings.total_weight as total_weight'
                                    ,'billings.total_price as total_price'
                                    ,'billings.remark as remark'
                                    ,'users.firstname as firstname'
                                    ,'users.lastname as lastname'
                                    ,'users.customer_general_code as customer_general_code'
                                    )
                                ->join('users', 'users.id', 'billings.user_id')
                                ->where('billings.id', $id)
                                ->first();
        $data['BillingLists'] = \App\Models\BillingList::select(
                                    'users.customer_general_code'
                                    ,'import_to_chaina.po_no'
                                    ,'import_to_chaina.import_date'
                                    ,'billing_lists.id as billing_list_id'
                                    ,'billing_lists.qty'
                                    ,'billing_lists.qty'
                                    ,'billing_lists.weight'
                                    ,'billing_lists.cubic'
                                    ,'billing_lists.rating_price'
                                    ,'billing_lists.subtotal'
                                    ,'billing_lists.status'
                                    ,'dalivery_sheets.delivery_no'
                                    ,'dalivery_sheets.delivery_slip_date'
                                )
                                ->join('import_to_chaina', 'import_to_chaina.id', '=', 'billing_lists.import_to_chaina_id')
                                ->join('billings', 'billings.id', '=', 'billing_lists.billing_id')
                                ->join('delivery_pos', 'delivery_pos.id', '=', 'billing_lists.delivery_pos_id')
                                ->join('dalivery_sheets', 'dalivery_sheets.id', '=', 'delivery_pos.delivery_sheet_id')
                                ->join('users', 'users.id', '=', 'billings.user_id')
                                ->where('billing_lists.billing_id', $id)
                                ->get();

        $mpdf = new Mpdf(['autoLangToFont' => true]);
        $data2 = view('Admin.billing_print', $data);
        $mpdf->WriteHTML($data2);
        $mpdf->Output();
    }

    public function ChangeStatus(Request $request, $id)
    {
        if(! $request->has('billing_list_id')){
            $return['status'] = 0;
            $return['content'] = trans('lang.change_status_of_payment').' '.trans('lang.unsuccessful');
            $return['title'] = trans('lang.please_select_items_that_po_ship_to_complete');
            return $return;
        }
        $input_all = $request->all();

        $validator = Validator::make($request->all(), [

        ]);
        if (!$validator->fails()) {
            \DB::beginTransaction();
            try {
                if(isset($input_all['billing_list_id'])){
                    foreach($input_all['billing_list_id'] as $billing_list_id){
                        $BillingList = \App\Models\BillingList::find($billing_list_id);

                        \App\Models\ImportToChaina::where('id', $BillingList->import_to_chaina_id)->update(['payment_status' => 'T']);
                        \App\Models\BillingList::where('id',$billing_list_id)->update(['status' => 'T']);
                        \App\Models\DeliveryPos::where('id', $BillingList['delivery_pos_id'])->update(['payment_status' => 'T']);
                    }
                }

                \App\Models\Billing::where('id',$id)->update(['status' => 'T']);
                \DB::commit();
                $return['status'] = 1;
                $return['content'] = trans('lang.success');
            } catch (Exception $e) {
                \DB::rollBack();
                $return['status'] = 0;
                $return['content'] = trans('lang.unsuccessful').$e->getMessage();
            }
        }else{
            $return['status'] = 0;
        }
        $return['title'] = trans('lang.change_status_of_payment');
        return $return;

    }

    public function ConfirmBilling($id)
    {
        \DB::beginTransaction();
        try {
            \App\Models\Billing::where('id',$id)->update(['status' => 'T']);

            $BillingLists = \App\Models\BillingList::where('billing_id', $id)->get();
            if(isset($BillingLists)){
                foreach($BillingLists as $BillingList){
                    \App\Models\BillingList::where('id', $BillingList['id'])->update(['status'=>'T']);
                }
            }

            \DB::commit();
            $return['status'] = 1;
            $return['content'] = trans('lang.success');
        } catch (Exception $e) {
            \DB::rollBack();
            $return['status'] = 0;
            $return['content'] = trans('lang.unsuccessful').$e->getMessage();
        }
        $return['title'] = 'ทำรายการปิดบิล';
        return $return;
    }

    public function PaymentStatus(Request $request,$id)
    {
        $input_all = $request->all();
        \DB::beginTransaction();
        try {
            $BillingLists = \App\Models\BillingList::where('billing_id',$id)->where('status', 'F')->get();
            
            if(!$BillingLists->isEmpty() && isset($BillingLists)){
                $return['status'] = 0;
                $return['title'] = trans('lang.change_status');
                $return['content'] = trans('lang.unsuccessful');
                return $return;
            }else{
                \App\Models\Billing::where('id',$id)->update(['payment_status' => $input_all['payment_status']]);
            }
                        
            \DB::commit();
            $return['status'] = 1;
            $return['content'] = trans('lang.success');
        } catch (Exception $e) {
            \DB::rollBack();
            $return['status'] = 0;
            $return['content'] = trans('lang.unsuccessful').$e->getMessage();
        }
        $return['title'] = trans('lang.change_status');
        return $return;
    }

    public function runCode()
    {
        $year = date('ym', strtotime('now'));
        $month = date('m', strtotime('now'));
        $num = 0;
        $cnt = 0;
        $rs = \App\Models\Billing::orderBy('id', 'DESC')->first();

        if($rs){
            $billing_no	= $rs->billing_no;  // dT-170600001
            $ckmonth2 = substr($billing_no, 5, 2);//06
            $num = substr($billing_no, 8, 5);
            if($month == $ckmonth2){
                $cnt = $num+1;
                $shownum = sprintf("%05d", $cnt);
            }
            else{
                $cnt = 0;
				$cnt++;
                $shownum = sprintf("%05d", $cnt);
            }
        }
        else{
            $cnt = 0;
			$cnt++;
            $shownum = sprintf("%05d", $cnt);
        }
         return 'BI-'.$year.$shownum;
    }

    public function Excel($id)
    {
        $date = date('d-m-Y');
        $data['Billing'] = \App\Models\Billing::select(
                                    'billings.billing_no as billing_no'
                                    ,'billings.billing_date as billing_date'
                                    ,'billings.total_qty as total_qty'
                                    ,'billings.total_weight as total_weight'
                                    ,'billings.total_price as total_price'
                                    ,'billings.remark as remark'
                                    ,'users.firstname as firstname'
                                    ,'users.lastname as lastname'
                                    ,'users.customer_general_code as customer_general_code'
                                )
                                ->join('users', 'users.id', 'billings.user_id')
                                ->where('billings.id', $id)
                                ->first();
        $data['BillingLists'] = \App\Models\BillingList::select(
                                    'users.customer_general_code'
                                    ,'import_to_chaina.po_no'
                                    ,'import_to_chaina.import_date'
                                    ,'billing_lists.id as billing_list_id'
                                    ,'billing_lists.qty'
                                    ,'billing_lists.qty'
                                    ,'billing_lists.weight'
                                    ,'billing_lists.cubic'
                                    ,'billing_lists.rating_price'
                                    ,'billing_lists.subtotal'
                                    ,'billing_lists.status'
                                    ,'dalivery_sheets.delivery_no'
                                    ,'dalivery_sheets.delivery_slip_date'
                                )
                                ->join('import_to_chaina', 'import_to_chaina.id', '=', 'billing_lists.import_to_chaina_id')
                                ->join('billings', 'billings.id', '=', 'billing_lists.billing_id')
                                ->join('delivery_pos', 'delivery_pos.id', '=', 'billing_lists.delivery_pos_id')
                                ->join('dalivery_sheets', 'dalivery_sheets.id', '=', 'delivery_pos.delivery_sheet_id')
                                ->join('users', 'users.id', '=', 'billings.user_id')
                                ->where('billing_lists.billing_id', $id)
                                ->get();
        // return $data;
        //return view('Admin.billing_excel', $data);

       \Excel::create('Billing-'.$date, function($excel) use ($data) {
           $excel->sheet('Billing', function($sheet) use ($data) {
               $sheet->loadView('Admin.billing_excel', $data);
           });
       })->download('xls');
    }

}
