<?php

namespace App\Http\Controllers\Admin;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \Mpdf\Mpdf;
use Storage;
class ShippingController extends Controller
{

    public function index($id)
    {
        $data['main_menu'] = 'Delivery';
        $data['sub_menu'] = 'Delivery';
        $data['title_page'] = 'รายการออกใบส่งสินค้า';
        $data['menus'] = \App\Models\AdminMenu::ActiveMenu()->get();
        $data['Delivery'] = \App\Models\Delivery::find($id);
        $data['Users']    = \App\Models\User::get();
        $data['DeliverySlipNo'] = $this->runCode();

        return view('Admin.delivery_shipping',$data);
    }

    public function create($id)
    {
        $return['DeliverySlipNo'] = $this->runCode();

        return $return;
    }

    public function store(Request $request)
    {
        $input_all = $request->all();

        $input_all['delivery_slip']['status_id']  = 1;
        $input_all['delivery_slip']['delivery_id']  = $input_all['id'];
        $input_all['delivery_slip']['created_at'] = date('Y-m-d H:i:s');
        $input_all['delivery_slip']['updated_at'] = date('Y-m-d H:i:s');

        $validator = Validator::make($request->all(), [

        ]);

        if (!$validator->fails()) {
            \DB::beginTransaction();
            try {
                $data_insert1 = $input_all['delivery_slip'];
                $delivery_slip_id = \App\Models\DeliverySlip::insertGetId($data_insert1);

                if(isset($input_all['delivery_slip_list'])){
                    foreach($input_all['delivery_slip_list'] as $value){

                        $DeliveryToUser = \App\Models\DeliveryToUser::select('qr_code_products.qr_code as qr_code'
                                                                            ,'rel_user_type_products.price as price'
                                                                            ,'lot_products.weight_per_item as weight_per_item'
                                                                            ,\DB::raw('(`tb_lot_products`.`product_sort_end` - `tb_lot_products`.`product_sort_start`) + 1 as lot_product_qty ')
                                                                            ,\DB::raw('`tb_lot_products`.`product_sort_end` / ((`tb_lot_products`.`product_sort_end` - `tb_lot_products`.`product_sort_start`) + 1) as cubic_per_item')
                                                                            )
                                                                    ->join('delivery_products', 'delivery_products.delivery_to_user_id', '=', 'delivery_to_users.id')
                                                                    ->join('qr_code_products', 'qr_code_products.id', '=', 'delivery_products.qr_code_product_id')
                                                                    ->join('rel_user_type_products', 'rel_user_type_products.id', '=', 'qr_code_products.rel_user_type_product_id')
                                                                    ->join('product_import_to_chaina', 'product_import_to_chaina.id', '=', 'qr_code_products.product_import_to_chaina_id')
                                                                    ->join('lot_products', 'lot_products.product_import_to_chaina_id', '=', 'product_import_to_chaina.id')
                                                                    ->whereRaw('tb_qr_code_products.sort_id between tb_lot_products.product_sort_start and tb_lot_products.product_sort_end')
                                                                    ->where('delivery_to_users.import_to_chaina_id', $value['import_to_chaina_id'])
                                                                    ->get();

                        $total_price = 0;
                        $total_cubic = 0;
                        $total_weight = 0;
                        $total_qty = 0;

                        if(isset($DeliveryToUser)){
                            foreach($DeliveryToUser as $DeliveryToUse){
                                $total_price += $DeliveryToUse->price * $DeliveryToUse->cubic_per_item;
                                $total_cubic += $DeliveryToUse->cubic_per_item;
                                $total_weight += $DeliveryToUse->weight_per_item;
                                $total_qty = $total_qty + 1;
                            }
                        }
                        $input_all2['delivery_slip_id']     = $delivery_slip_id;
                        $input_all2['delivery_to_user_id']  = $value['delivery_to_user_id'];
                        $input_all2['import_to_chaina_id']  = $value['import_to_chaina_id'];
                        $input_all2['total_price']          = $total_price;
                        $input_all2['total_cubic']          = $total_cubic;
                        $input_all2['total_weight']         = $total_weight;
                        $input_all2['total_qty']            = $total_qty;
                        $input_all2['remark']               = $value['remark'];
                        $input_all2['created_at']           = date('Y-m-d H:i:s');
                        $input_all2['updated_at']           = date('Y-m-d H:i:s');

                        $data_insert2 = $input_all2;

                        \App\Models\DeliverySlipList::insert($data_insert2);
                        //\App\Models\ImportToChaina::where('id', $value['import_to_chaina_id'])->update(['status_id' => 6]);
                    }

                    $DeliverySlipList = \App\Models\DeliverySlipList::where('delivery_slip_id', $delivery_slip_id)->get();
                    $price          = $DeliverySlipList->sum('total_price');
                    $total_price    = $DeliverySlipList->sum('total_price');
                    $total_qty      = $DeliverySlipList->sum('total_qty');
                    $total_cubic    = $DeliverySlipList->sum('total_cubic');
                    $total_weight   = $DeliverySlipList->sum('total_weight');

                    $delivery_charge_price  = \App\Models\DeliveryCharge::find(1);
                    $delivery_buy_low_price  = \App\Models\DeliveryCharge::find(2);
                    $delivery_charge = 0;

                    if($total_price < $delivery_buy_low_price->price){
                        $total_price = $total_price + $delivery_charge_price->price;
                        $delivery_charge = $delivery_charge_price->price;
                    }

                    \App\Models\DeliverySlip::where('id', $delivery_slip_id)->update(['price' => $price
                                                                                ,'delivery_charge_price' => $delivery_charge
                                                                                ,'total_price' => $total_price
                                                                                ,'total_qty' => $total_qty
                                                                                ,'total_cubic' => $total_cubic
                                                                                ,'total_weight' => $total_weight]);

                }else{
                    $return['status'] = 0;
                    $return['content'] = 'ไม่สำเร็จ';
                }
                \DB::commit();
                $return['status'] = 1;
                $return['content'] = 'สำเร็จ';
            } catch (Exception $e) {
                \DB::rollBack();
                $return['status'] = 0;
                $return['content'] = 'ไม่สำเร็จ'.$e->getMessage();
            }
        }else{
            $return['status'] = 0;
        }
        $return['title'] = 'เพิ่มข้อมูล';
        return json_encode($return);
    }

    public function show($id)
    {
        $result = \App\Models\Delivery::find($id);

        return json_encode($result);
    }

    public function edit($id)
    {

    }

    public function update(Request $request, $id)
    {
        $input_all = $request->all();

        $input_all['updated_at'] = date('Y-m-d H:i:s');

        $validator = Validator::make($request->all(), [
             'date_delivery' => 'required',
             'status_id' => 'required',

        ]);
        if (!$validator->fails()) {
            \DB::beginTransaction();
            try {
                $data_insert = $input_all;
                \App\Models\Delivery::where('id',$id)->update($data_insert);
                \DB::commit();
                $return['status'] = 1;
                $return['content'] = 'สำเร็จ';
            } catch (Exception $e) {
                \DB::rollBack();
                $return['status'] = 0;
                $return['content'] = 'ไม่สำเร็จ'.$e->getMessage();
            }
        }else{
            $return['status'] = 0;
        }
        $return['title'] = 'เพิ่มข้อมูล';
        return json_encode($return);
    }

    public function destroy($id)
    {
        \DB::beginTransaction();
        try {
            \App\Models\DeliverySlipList::where('delivery_slip_id',$id)->delete();
            \App\Models\DeliverySlip::where('id',$id)->delete();
            \DB::commit();
            $return['status'] = 1;
            $return['content'] = 'สำเร็จ';
        } catch (Exception $e) {
            \DB::rollBack();
            $return['status'] = 0;
            $return['content'] = 'ไม่สำเร็จ'.$e->getMessage();
        }
        $return['title'] = 'ลบข้อมูล';
        return $return;
    }

    public function Lists(Request $request)
    {
        $delivery_id = $request->input('id');
        //$result = \App\Models\DeliverySlip::where('delivery_id', $delivery_id)->select();
        $result = \App\Models\DeliverySlip::select(
                                    'delivery_slips.delivery_slip_no as delivery_slip_no'
                                    ,'delivery_slips.delivery_slip_date as delivery_slip_date'
                                    ,'delivery_slips.status_id as status_id'
                                    ,'delivery_slips.id as id'
                                    ,'delivery_slips.total_qty as total_qty'
                                    ,'delivery_slips.total_weight as total_weight'
                                    ,'delivery_slips.total_price as total_price'
                                    ,'delivery_slips.price as price'
                                    ,'delivery_slips.delivery_charge_price as delivery_charge_price'
                                    ,'delivery_slips.status_id as status_id'
                                    ,'delivery_slip_statuses.name as status_name'
                                    ,'users.firstname as firstname'
                                    ,'users.lastname as lastname'
                                    ,'users.customer_general_code as customer_general_code'
                                    ,'deliveries.plate_no as plate_no'
                                )
                                ->join('users', 'users.id', '=', 'delivery_slips.user_id')
                                ->join('delivery_slip_statuses', 'delivery_slip_statuses.id', '=', 'delivery_slips.status_id')
                                ->join('deliveries', 'deliveries.id', '=', 'delivery_slips.delivery_id')
                                ->where('delivery_slips.delivery_id', $delivery_id) ;

        return \Datatables::of($result)

        ->editColumn('total_qty', function($rec){
            return number_format($rec->total_qty);
        })

        ->editColumn('total_weight', function($rec){
            return number_format($rec->total_weight, 2);
        })

        ->editColumn('total_price', function($rec){
            return number_format($rec->total_price, 2);
        })

        ->editColumn('price', function($rec){
            return number_format($rec->price, 2);
        })

        ->editColumn('delivery_charge_price', function($rec){
            return number_format($rec->delivery_charge_price, 2);
        })

        ->editColumn('status_name', function($rec){
            if($rec->status_id == 1){
                return '<span class="badge badge-warning">'.$rec->status_name.'</span>';
            }else{
                return '<span class="badge badge-success">'.$rec->status_name.'</span>';
            }
        })

        ->addColumn('user', function($rec){
            return $rec->firstname.' '.$rec->lastname;
        })

        ->addColumn('customer_general_code', function($rec){
            return $rec->customer_general_code;
        })

        ->addColumn('action',function($rec){
            // <button data-loading-text="<i class=\'fa fa-refresh fa-spin\'></i>" class="btn btn-xs btn-warning btn-condensed btn-tooltip btn-edit"  data-rel="tooltip" data-id="'.$rec->id.'" title="ดูรายละเอียด">
            //     <i class="ace-icon fa fa-edit bigger-120"></i>
            // </button>
            if($rec->status_id == 2){
                $disabled = 'disabled';
            }else{
                $disabled = '';
            }
            $str='
                <button  class="btn btn-xs btn-condensed btn-status btn-tooltip" data-id="'.$rec->id.'" data-rel="tooltip" title="เปลี่ยนสถานะการจัดส่งของแต่ละใบ" '.$disabled.'>
                    <i class="ace-icon fa fa-cog bigger-120"></i>
                </button>
                <a href="'.url('/admin/Shipping/Prints/'.$rec->id).'" target="_blank" data-loading-text="<i class=\'fa fa-refresh fa-spin\'></i>" class="btn btn-xs btn-info btn-condensed btn-tooltip" data-rel="tooltip" data-id="'.$rec->id.'"title="พิมพ์ใบส่งสินค้า">
                    <i class="ace-icon fa fa-print bigger-120"></i>
                </a>
                <button  class="btn btn-xs btn-danger btn-condensed btn-delete btn-tooltip" data-id="'.$rec->id.'" data-rel="tooltip" title="ลบ" '.$disabled.'>
                    <i class="ace-icon fa fa-trash bigger-120"></i>
                </button>
            ';
            return $str;
        })->rawColumns(['status_name', 'action'])->make(true);
    }

    public function Prints($id)
    {
        $data['DeliverySlip']   = \App\Models\DeliverySlip::with('User'
                        ,'UserAddress.Province'
                        ,'UserAddress.Amphure'
                        ,'DeliverySlipStatus')
                    ->where('id', $id)->first();
        $data['RelUserTypeProduct'] =   \App\Models\RelUserTypeProduct::orderBy('id', 'desc')
                    ->where('user_id', $data['DeliverySlip']->User->id)
                    ->take(2)
                    ->get();
        $data['DeliverySlips']  = \App\Models\DeliverySlip::select(
                                        'delivery_slips.delivery_slip_no as delivery_slip_no'
                                        ,'delivery_slips.delivery_slip_date as delivery_slip_date'
                                        ,'users.customer_general_code as customer_general_code'
                                        ,'import_to_chaina.po_no as po_no'
                                        ,'qr_code_products.qr_code as qr_code'
                                        ,'qr_code_products.sort_id as sort_id'
                                        ,'qr_code_products.rel_user_type_product_id_old as rel_user_type_product_id_old'
                                        //,'qr_code_products.rel_user_type_product_id as rel_user_type_product_id'
                                        ,'qr_code_products.id as qr_code_product_id'
                                        ,'products.name as product_name'
                                        ,'lot_products.weight_per_item as weight_per_item'
                                        ,'lot_products.product_sort_start as product_sort_start'
                                        ,'lot_products.product_sort_end as product_sort_end'
                                        ,'lot_products.cubic as cubic'
                                        ,'rel_user_type_products.price as price'
                                        //,'rel_user_type_products.price as price_old'
                                        ,'rel_user_type_products.type_rate as type_rate'
                                        , \DB::raw('(`tb_lot_products`.`product_sort_end` - `tb_lot_products`.`product_sort_start`) + 1 as lot_product_qty ')
                                        , \DB::raw('`tb_lot_products`.`product_sort_end` / ((`tb_lot_products`.`product_sort_end` - `tb_lot_products`.`product_sort_start`) + 1) as cubic_per_item')
                                        )
                                    ->join('delivery_slip_lists', 'delivery_slip_lists.delivery_slip_id', '=', 'delivery_slips.id')
                                    ->join('import_to_chaina', 'import_to_chaina.id', '=', 'delivery_slip_lists.import_to_chaina_id')
                                    ->join('users', 'users.id', '=', 'delivery_slips.user_id')
                                    ->join('qr_code_products', 'qr_code_products.import_to_chaina_id', '=', 'import_to_chaina.id')
                                    ->leftjoin('rel_user_type_products', 'rel_user_type_products.id', '=', 'qr_code_products.rel_user_type_product_id')
                                    //->leftjoin('rel_user_type_products', 'rel_user_type_products.id', '=', 'qr_code_products.rel_user_type_product_id_old')
                                    ->join('delivery_products', 'delivery_products.qr_code_product_id', '=', 'qr_code_products.id')
                                    ->join('product_import_to_chaina', 'product_import_to_chaina.id', '=', 'qr_code_products.product_import_to_chaina_id')
                                    ->join('products', 'products.id', '=', 'product_import_to_chaina.product_id')
                                    ->join('lot_products', 'lot_products.product_import_to_chaina_id', '=', 'product_import_to_chaina.id')
                                    ->whereRaw('tb_qr_code_products.sort_id between tb_lot_products.product_sort_start and tb_lot_products.product_sort_end')
                                    ->where('delivery_slips.id', $id)
                                    ->get();

        $mpdf = new Mpdf(['autoLangToFont' => true]);
        $data2 = view('Admin.delivery_slip_print', $data);
        $mpdf->WriteHTML($data2);
        $mpdf->Output();
    }

    public function GetUserPo(Request $request, $id)
    {
        $delivery_id = $request->input('delivery_id');
        $return['ImportToChainas'] = \App\Models\ImportToChaina::select(
                'import_to_chaina.id as import_to_chaina_id'
                ,'import_to_chaina.created_at as import_to_chaina_date'
                // ,'import_to_thai.created_at as import_to_thai_date'
                ,'import_to_chaina.po_no as po_no'
                ,'delivery_to_users.created_at as delivery_date'
                ,'delivery_to_users.id as delivery_to_user_id'
                //, \DB::raw('count(distinct tb_product_import_to_chaina.id) as count')
                // , \DB::raw('sum(tb_product_import_to_chaina.qty) as qty_po')
                ,\DB::raw(' (   select `tb_import_to_thai`.`created_at` from `tb_import_to_thai`
                                where `tb_import_to_thai`.`import_to_chaina_id` = `tb_import_to_chaina`.`id`
                                order by `tb_import_to_thai`.`created_at` desc limit 1
                            ) as import_to_thai_date')
                ,\DB::raw(' (   select sum(qty) from tb_product_import_to_chaina
                                where tb_product_import_to_chaina.import_to_chaina_id = tb_import_to_chaina.id
                            ) as qty_chaina')
                ,\DB::raw(' (   select count(*) from tb_qr_code_products
                                join tb_rel_container_products on tb_rel_container_products.qr_code_product_id = tb_qr_code_products.id
                                where tb_qr_code_products.import_to_chaina_id = tb_import_to_chaina.id
                            ) as qty_container')
                ,\DB::raw(' (   select count(*) from tb_delivery_products
                                join tb_qr_code_products  on tb_qr_code_products.id = tb_delivery_products.qr_code_product_id
                                where tb_delivery_products.delivery_to_user_id = tb_delivery_to_users.id
                                and tb_qr_code_products.import_to_chaina_id = tb_import_to_chaina.id
                            ) as qty_delivery_product')
                ,\DB::raw(' (   select sum(`tb_lot_products`.`weight_per_item`)
                                from `tb_qr_code_products`
                                join `tb_delivery_products` on `tb_delivery_products`.`qr_code_product_id` = `tb_qr_code_products`.`id`
                                join `tb_lot_products` ON `tb_lot_products`.`product_import_to_chaina_id` = `tb_qr_code_products`.`product_import_to_chaina_id`
                                where `tb_qr_code_products`.`sort_id` BETWEEN `tb_lot_products`.`product_sort_start` and `tb_lot_products`.`product_sort_end`
                                and `tb_qr_code_products`.`import_to_chaina_id` = `tb_import_to_chaina`.`id`
                            ) as weight_all')
                ,\DB::raw(' (   select sum( `tb_lot_products`.`cubic` / ((`tb_lot_products`.`product_sort_end` - `tb_lot_products`.`product_sort_start`)+1) )
                                from `tb_qr_code_products`
                                join `tb_delivery_products` on `tb_delivery_products`.`qr_code_product_id` = `tb_qr_code_products`.`id`
                                join `tb_lot_products` ON `tb_lot_products`.`product_import_to_chaina_id` = `tb_qr_code_products`.`product_import_to_chaina_id`
                                where `tb_qr_code_products`.`sort_id` BETWEEN `tb_lot_products`.`product_sort_start` and `tb_lot_products`.`product_sort_end`
                                and `tb_qr_code_products`.`import_to_chaina_id` = `tb_import_to_chaina`.`id`
                            ) as cubic')
            )
            ->join('delivery_to_users', 'delivery_to_users.import_to_chaina_id','=' , 'import_to_chaina.id')
            ->leftjoin('delivery_slip_lists', 'delivery_slip_lists.import_to_chaina_id', '=', 'import_to_chaina.id')
            // ->join('import_to_thai', 'import_to_thai.import_to_chaina_id', '=', 'import_to_chaina.id')
            // ->groupBy('import_to_chaina.id')
            // ->groupBy('import_to_chaina.created_at')
            // ->groupBy('import_to_chaina.po_no')
            // ->groupBy('import_to_thai.created_at')
            // ->groupBy('delivery_to_users.created_at')
            // ->groupBy('delivery_to_users.id')
            ->where('delivery_to_users.user_id', '=', $id)
            ->where('delivery_to_users.delivery_id', '=', $delivery_id)
            ->whereNull('delivery_slip_lists.id')
            ->get();
        return $return;
    }

    public function GetStatus(Request $request, $id)
    {
        $DeliverySlipLists = \App\Models\DeliverySlipList::where('delivery_slip_id', $id)->get();
        if(! $DeliverySlipLists->isEmpty()){
            $impurt_to_chaina_id = [];
            foreach($DeliverySlipLists as $DeliverySlipList){
                $impurt_to_chaina_id[] = $DeliverySlipList->import_to_chaina_id;
            }
        }
        \DB::beginTransaction();
        try {
            if($request->has('active')){
                \App\Models\ImportToChaina::whereIn('id', $impurt_to_chaina_id)->update(['status_id' => 6]);
                \App\Models\DeliverySlip::where('id', $id)->update(['status_id' => 2]);
                \DB::commit();
                $return['status'] = 1;
                $return['content'] = 'สำเร็จ';
            }else{
                $return['status'] = 0;
                $return['content'] = 'ไม่สำเร็จ';
            }

        } catch (Exception $e) {
            \DB::rollBack();
            $return['status'] = 0;
            $return['content'] = 'ไม่สำเร็จ'.$e->getMessage();
        }
        $return['title'] = 'สถานะจัดส่งสินค้า';
        return $return;
    }

    public function runCode()
    {
        $year = date('ym', strtotime('now'));
        $month = date('m', strtotime('now'));
        $num = 0;
        $cnt = 0;
        $rs = \App\Models\DeliverySlip::orderBy('id', 'DESC')->first();

        if($rs){
            $delivery_slip_no	= $rs->delivery_slip_no;  // dT-170600001
            $ckmonth2 = substr($delivery_slip_no, 4, 2);//06
            $num = substr($delivery_slip_no, 7, 4);
            if($month == $ckmonth2){
                $cnt = $num+1;
                $shownum = sprintf("%05d", $cnt);
            }
            else{
                $cnt = 0;
				$cnt++;
                $shownum = sprintf("%05d", $cnt);
            }
        }
        else{
            $cnt = 0;
			$cnt++;
            $shownum = sprintf("%05d", $cnt);
        }
         return 'E-'.$year.$shownum;
    }


}
