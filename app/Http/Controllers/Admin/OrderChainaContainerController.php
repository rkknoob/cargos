<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Storage;
use \Mpdf\Mpdf;
use App\Providers\Admin\PermissionCRUDProvider as Permission;

class OrderChainaContainerController extends Controller
{
    public function index()
    {
    	$data['main_menu'] = 'OrderChainaContainer';
    	$data['sub_menu'] = '';
    	$data['title'] = trans('lang.close_container_china');
    	$data['title_page'] = trans('lang.close_container_china');
    	$data['permission'] = Permission::CheckPermission($data['main_menu'],$data['sub_menu']);
    	$data['menus'] = \App\Models\AdminMenu::ActiveMenu()->get();
    	return view('Admin.order_chaina_container', $data);
    }

    public function GetContainerDetail($id)
    {
        $data['container_id'] = $id;
        $data['Containers'] = \App\Models\Container::
            with('TransportType', 'ContainerType', 'ContainerSize')
            ->where('containers.id', $id)
            ->first();
         $data['QrCodeProduct'] = \App\Models\QrCodeProduct::select(
                'qr_code_products.*'
                , 'import_to_chaina.po_no'
                , 'lot_products.id as lot_product_id'
                , 'users.customer_general_code'
                , 'product_types.name as product_type_name'
                , 'products.name as product_name'
                , 'lot_products.width'
                , 'lot_products.height'
                , 'lot_products.length'
                , 'lot_products.weight_per_item'
                , 'lot_products.cubic as product_cobic'
                , \DB::raw('
                        (select sum(qty)
                        from tb_product_import_to_chaina
                        where tb_product_import_to_chaina.import_to_chaina_id = tb_import_to_chaina.id
                        ) as po_qty
                    ')
                , \DB::raw('(`tb_lot_products`.`product_sort_end` - `tb_lot_products`.`product_sort_start`) + 1 as lot_product_qty')
            )
            ->join('rel_container_products', 'rel_container_products.qr_code_product_id', '=', 'qr_code_products.id')
            ->join('import_to_chaina', 'import_to_chaina.id', '=', 'qr_code_products.import_to_chaina_id')
            ->join('users', 'users.id', '=', 'import_to_chaina.user_id')
            ->join('product_import_to_chaina', 'product_import_to_chaina.id', '=', 'qr_code_products.product_import_to_chaina_id')
            ->join('products', 'products.id', '=', 'product_import_to_chaina.product_id')
            ->join('product_types', 'product_types.id', '=', 'product_import_to_chaina.product_type_id')
            // join lot product
            ->join('lot_products', 'lot_products.product_import_to_chaina_id', '=', 'qr_code_products.product_import_to_chaina_id')
            ->whereRaw('`tb_qr_code_products`.`sort_id` BETWEEN `tb_lot_products`.`product_sort_start` AND `tb_lot_products`.`product_sort_end`')
            //end join lot product
            ->where('rel_container_products.container_id', $id)
            ->get();
        return json_encode($data);
    }

    public function Lists(Request $request)
    {
        $result = \App\Models\Container::select(
                'containers.*'
                ,'transport_types.name_th as transport_type_name_th'
                ,'transport_types.name_en as transport_type_name_en'
                ,'transport_types.name_ch as transport_type_name_ch'
                ,'container_types.name_th as container_type_name_th'
                ,'container_types.name_en as container_type_name_en'
                ,'container_types.name_ch as container_type_name_ch'
                ,'container_sizes.name as container_size_name'
                ,'container_statuses.color as container_status_color'
                ,'container_statuses.name as container_status_name'
                ,\DB::raw('
                    (select count(`tb_rel_container_products`.`id`)
                    from `tb_rel_container_products`
                    where `tb_rel_container_products`.`container_id` = `tb_containers`.`id`)
                    as qty_product_real
                ')
            )
            ->leftJoin('transport_types','transport_types.id','=','containers.transport_type_id')
            ->leftJoin('container_types','container_types.id','=','containers.container_type_id')
            ->leftJoin('container_sizes','container_sizes.id','=','containers.container_size_id')
            ->leftJoin('container_statuses','container_statuses.id','=','containers.status_id')
            //->where('status_id', '>=',  1)
            ;
        return \Datatables::of($result)
        ->addIndexColumn()
        ->editColumn('updated_at', function($rec){
            return $rec->updated_at;
        })
        ->editColumn('size', function($rec){
            $weight = number_format(pow($rec->cubic, 1/3), 3) .'x'. number_format(pow($rec->cubic, 1/3), 3) .'x'. number_format(pow($rec->cubic, 1/3), 3);
            return $weight;
        })
        ->editColumn('weight', function($rec){
            $weight = number_format($rec->weight, 3);
            return $weight;
        })
        ->editColumn('qty_product_real', function($rec){
            $qty_product_real = number_format($rec->qty_product_real);

            return $qty_product_real;
        })
        ->editColumn('cubic', function($rec){
            $cubic =  number_format($rec->cubic, 3);
            return $cubic;
        })
        ->editColumn('status_id', function($rec){
            $color_status = (!empty($rec->container_status_color) ? $rec->container_status_color : '#000');
            return '<label class="label" style="background-color: '. $color_status .';">'. $rec->container_status_name .'</label>';
        })
        ->addColumn('null',function($rec){
            $str='';
            return $str;
        })
        ->addColumn('action',function($rec){
            $str='
                <button  class="btn btn-xs btn-warning btn-condensed btn-container-detail btn-tooltip" data-id="'.$rec->id.'" data-rel="tooltip" title="'.trans('lang.detail_container').'">
                    <i class="ace-icon fa fa-info bigger-120"></i>
                </button>
                <a href="'.url('admin/OrderChainaContainer/PrintOrder/'.$rec->id).'" target="_blank" class="btn btn-xs btn-info btn-condensed btn-tooltip" data-id="'.$rec->id.'" data-rel="tooltip" title="'.trans('lang.print_order').'">
                    <i class="ace-icon fa fa-print bigger-120"></i>
                </a>
                <a href="'.url('admin/OrderChainaContainer/ExcelOrder/'.$rec->id).'" class="btn btn-xs btn-success btn-condensed btn-tooltip" data-id="'.$rec->id.'" data-rel="tooltip" title="'.trans('lang.export_excel').' Order">
                    <i class="ace-icon fa fa-list bigger-120"></i>
                </a>
                <a href="'.url('admin/OrderChainaContainer/PrintSale/'.$rec->id).'" target="_blank" class="btn btn-xs btn-info btn-condensed btn-tooltip" data-id="'.$rec->id.'" data-rel="tooltip" title="'.trans('lang.print_sale').'">
                    <i class="ace-icon fa fa-print bigger-120"></i>
                </a>
                <a href="'.url('admin/OrderChainaContainer/ExcelSale/'.$rec->id).'" class="btn btn-xs btn-success btn-condensed btn-tooltip" data-id="'.$rec->id.'" data-rel="tooltip" title="'.trans('lang.export_excel').' Sale">
                    <i class="ace-icon fa fa-list bigger-120"></i>
                </a>
            ';
            return $str;
        })
        ->rawColumns(['status_id', 'action'])
        ->make(true);
    }

    public function PrintOrder($id)
    {
        $data['Container'] = \App\Models\Container::find($id);
        $data['QrCodeProducts'] = \App\Models\QrCodeProduct::select(
                'qr_code_products.product_import_to_chaina_id'
                , 'qr_code_products.rel_user_type_product_id_old'
                , 'product_import_to_chaina.id as product_import_to_chaina_id'
                , 'product_import_to_chaina.qty as product_import_to_chaina_qty'
                , 'product_import_to_chaina.cubic as product_import_to_chaina_cubic'
                , 'product_import_to_chaina.width as width'
                , 'product_import_to_chaina.height as height'
                , 'product_import_to_chaina.length as length'
                , 'import_to_chaina.id'
                , 'import_to_chaina.po_no as po_no'
                , 'users.customer_general_code'
                , 'product_types.name as product_type_name'
                , 'products.name as product_name'
                , 'rel_user_type_products.price as rate_price'
                , 'rel_user_type_products.type_rate as type_rate'
                , 'product_import_to_chaina.id as product_import_to_chaina_id'
                , \DB::raw('
                        (count(tb_rel_container_products.id)) as product_amount
                    ')
                , \DB::raw('
                        (sum(tb_product_import_to_chaina.weight_per_item)) as product_weight_per_item
                    ')
                , \DB::raw('tb_product_import_to_chaina.cubic as cubic_item')
            )
            ->leftjoin('rel_container_products', 'rel_container_products.qr_code_product_id', '=', 'qr_code_products.id')
            ->leftjoin('rel_user_type_products', 'rel_user_type_products.id', '=', 'qr_code_products.rel_user_type_product_id')
            ->join('import_to_chaina', 'import_to_chaina.id', '=', 'qr_code_products.import_to_chaina_id')
            ->join('users', 'users.id', '=', 'import_to_chaina.user_id')
            ->join('product_import_to_chaina', 'product_import_to_chaina.id', '=', 'qr_code_products.product_import_to_chaina_id')
            ->join('products', 'products.id', '=', 'product_import_to_chaina.product_id')
            ->join('product_types', 'product_types.id', '=', 'product_import_to_chaina.product_type_id')
            ->groupBy('qr_code_products.product_import_to_chaina_id')
            ->groupBy('qr_code_products.rel_user_type_product_id_old')
            ->groupBy('product_import_to_chaina.id')
            ->groupBy('import_to_chaina.id')
            ->groupBy('import_to_chaina.po_no')
            ->groupBy('users.customer_general_code')
            ->groupBy('product_types.name')
            ->groupBy('products.name')
            ->groupBy('rel_user_type_products.price')
            ->groupBy('rel_user_type_products.type_rate')
            ->groupBy('product_import_to_chaina.qty')
            ->groupBy('product_import_to_chaina.cubic')
            ->groupBy('product_import_to_chaina.width')
            ->groupBy('product_import_to_chaina.height')
            ->groupBy('product_import_to_chaina.length')
            ->where('rel_container_products.container_id', $id)
            ->get();

        $data['ImportToChainas'] = \App\Models\ImportToChaina::select(
                'users.customer_general_code'
                // sub select get amount product all
                , \DB::raw('
                    (   select count(`tb_qr_code_products`.`id`)
                        from `tb_qr_code_products`
                        where `tb_qr_code_products`.`import_to_chaina_id` = `tb_import_to_chaina`.`id`
                    ) as product_amount
                ')
                // sub select get weight product all
                , \DB::raw('
                    (   select sum(`tb_product_import_to_chaina`.`weight_all`)
                        from `tb_product_import_to_chaina`
                        where `tb_product_import_to_chaina`.`import_to_chaina_id` = `tb_import_to_chaina`.`id`
                    ) as product_KG
                ')
                // sub select get cubic product all
                , \DB::raw('
                    (   select sum(`tb_product_import_to_chaina`.`cubic`)
                        from `tb_product_import_to_chaina`
                        where `tb_product_import_to_chaina`.`import_to_chaina_id` = `tb_import_to_chaina`.`id`
                    ) as product_cubic
                ')
                // get po amount
                , \DB::raw('count(`tb_import_to_chaina`.`id`) as po_amount')
                // sub select get cubic product all real
                , \DB::raw('
                    (   select count(`tb_qr_code_products`.`id`)
                        from `tb_qr_code_products`
                        join `tb_rel_container_products` on `tb_rel_container_products`.`qr_code_product_id` = `tb_qr_code_products`.`id`
                        where `tb_qr_code_products`.`import_to_chaina_id` = `tb_import_to_chaina`.`id`
                    ) as product_amount_real
                ')
                // get p
            )
            ->join('users', 'users.id', '=', 'import_to_chaina.user_id')
            // sub select where
            ->whereRaw('
                (   select count(`tb_qr_code_products`.`id`)
                    from `tb_qr_code_products`
                    join `tb_rel_container_products` on `tb_rel_container_products`.`qr_code_product_id` = `tb_qr_code_products`.`id`
                    where `tb_qr_code_products`.`import_to_chaina_id` = `tb_import_to_chaina`.`id`
                    and `tb_rel_container_products`.`container_id` = '.$id.'
                ) > 0
            ')
            ->groupBy('users.id')
            ->get();

        $data2 = view('Admin.order_chaina_container_ptint', $data);
        $mpdf = new Mpdf([
            'autoLangToFont' => true,
            'autoScriptToLang' => true,
            'mode' => 'utf-8',
        ]);

        $mpdf->setHtmlHeader('<div style="text-align: right; width: 100%;">{PAGENO}</div>');
        $mpdf->WriteHTML($data2);
        $mpdf->Output();
    }

    public function PrintSale($id)
    {
        $data['Container'] = \App\Models\Container::find($id);
        $data['QrCodeProducts'] = \App\Models\QrCodeProduct::select(
                'qr_code_products.product_import_to_chaina_id'
                , 'qr_code_products.rel_user_type_product_id_old'
                , 'product_import_to_chaina.id as product_import_to_chaina_id'
                , 'product_import_to_chaina.qty as product_import_to_chaina_qty'
                , 'product_import_to_chaina.cubic as product_import_to_chaina_cubic'
                , 'product_import_to_chaina.width as width'
                , 'product_import_to_chaina.height as height'
                , 'product_import_to_chaina.length as length'
                , 'import_to_chaina.id'
                , 'import_to_chaina.po_no as po_no'
                , 'users.customer_general_code'
                , 'product_types.name as product_type_name'
                , 'products.name as product_name_th'
                , 'products.name_en as product_name_en'
                , 'rel_user_type_products.price as rate_price'
                , 'rel_user_type_products.type_rate as type_rate'
                , \DB::raw('
                        (count(tb_rel_container_products.id)) as product_amount
                    ')
                , \DB::raw('
                        (sum(tb_product_import_to_chaina.weight_per_item)) as product_weight_per_item
                    ')
                , \DB::raw('tb_product_import_to_chaina.cubic as cubic_item')
                // / tb_product_import_to_chaina.qty
            )
            ->leftjoin('rel_container_products', 'rel_container_products.qr_code_product_id', '=', 'qr_code_products.id')
            ->leftjoin('rel_user_type_products', 'rel_user_type_products.id', '=', 'qr_code_products.rel_user_type_product_id')
            ->join('import_to_chaina', 'import_to_chaina.id', '=', 'qr_code_products.import_to_chaina_id')
            ->join('users', 'users.id', '=', 'import_to_chaina.user_id')
            ->join('product_import_to_chaina', 'product_import_to_chaina.id', '=', 'qr_code_products.product_import_to_chaina_id')
            ->join('products', 'products.id', '=', 'product_import_to_chaina.product_id')
            ->join('product_types', 'product_types.id', '=', 'product_import_to_chaina.product_type_id')
            ->groupBy('qr_code_products.product_import_to_chaina_id')
            ->groupBy('qr_code_products.rel_user_type_product_id_old')
            ->groupBy('product_import_to_chaina.id')
            ->groupBy('import_to_chaina.id')
            ->groupBy('import_to_chaina.po_no')
            ->groupBy('users.customer_general_code')
            ->groupBy('product_types.name')
            ->groupBy('products.name')
            ->groupBy('rel_user_type_products.price')
            ->groupBy('rel_user_type_products.type_rate')
            ->groupBy('product_import_to_chaina.qty')
            ->groupBy('product_import_to_chaina.cubic')
            ->groupBy('product_import_to_chaina.width')
            ->groupBy('product_import_to_chaina.height')
            ->groupBy('product_import_to_chaina.length')
            ->where('rel_container_products.container_id', $id)
            ->get();
            // dd();
        $data['ImportToChainas'] = \App\Models\ImportToChaina::select(
                'users.customer_general_code'
                // sub select get amount product all
                , \DB::raw('
                    (   select count(`tb_qr_code_products`.`id`)
                        from `tb_qr_code_products`
                        where `tb_qr_code_products`.`import_to_chaina_id` = `tb_import_to_chaina`.`id`
                    ) as product_amount
                ')
                // sub select get weight product all
                , \DB::raw('
                    (   select sum(`tb_product_import_to_chaina`.`weight_all`)
                        from `tb_product_import_to_chaina`
                        where `tb_product_import_to_chaina`.`import_to_chaina_id` = `tb_import_to_chaina`.`id`
                    ) as product_KG
                ')
                // sub select get cubic product all
                , \DB::raw('
                    (   select sum(`tb_product_import_to_chaina`.`cubic`)
                        from `tb_product_import_to_chaina`
                        where `tb_product_import_to_chaina`.`import_to_chaina_id` = `tb_import_to_chaina`.`id`
                    ) as product_cubic
                ')
                // get po amount
                , \DB::raw('count(`tb_import_to_chaina`.`id`) as po_amount')
                // sub select get cubic product all real
                , \DB::raw('
                    (   select count(`tb_qr_code_products`.`id`)
                        from `tb_qr_code_products`
                        join `tb_rel_container_products` on `tb_rel_container_products`.`qr_code_product_id` = `tb_qr_code_products`.`id`
                        where `tb_qr_code_products`.`import_to_chaina_id` = `tb_import_to_chaina`.`id`
                    ) as product_amount_real
                ')
                // get p
            )
            ->join('users', 'users.id', '=', 'import_to_chaina.user_id')
            // sub select where
            ->whereRaw('
                (   select count(`tb_qr_code_products`.`id`)
                    from `tb_qr_code_products`
                    join `tb_rel_container_products` on `tb_rel_container_products`.`qr_code_product_id` = `tb_qr_code_products`.`id`
                    where `tb_qr_code_products`.`import_to_chaina_id` = `tb_import_to_chaina`.`id`
                    and `tb_rel_container_products`.`container_id` = '.$id.'
                ) > 0
            ')
            ->groupBy('users.id')
            ->get();

        $data2 = view('Admin.sale_chaina_container_ptint', $data);
        $mpdf = new Mpdf([
            'autoLangToFont' => true,
            'autoScriptToLang' => true,
            'mode' => 'utf-8',
        ]);

        $mpdf->setHtmlHeader('<div style="text-align: right; width: 100%;">{PAGENO}</div>');
        $mpdf->WriteHTML($data2);
        $mpdf->Output();
    }

    public function ExcelOrder($id)
    {
        $date = date('d-m-Y');

        $data['Container'] = \App\Models\Container::find($id);
        $data['QrCodeProducts'] = \App\Models\QrCodeProduct::select(
                'qr_code_products.product_import_to_chaina_id'
                ,'qr_code_products.rel_user_type_product_id_old'
                ,'product_import_to_chaina.id as product_import_to_chaina_id'
                ,'product_import_to_chaina.qty as product_import_to_chaina_qty'
                ,'product_import_to_chaina.cubic as product_import_to_chaina_cubic'
                ,'product_import_to_chaina.width as width'
                ,'product_import_to_chaina.height as height'
                ,'product_import_to_chaina.length as length'
                ,'import_to_chaina.id'
                ,'import_to_chaina.po_no as po_no'
                ,'users.customer_general_code'
                ,'product_types.name as product_type_name'
                ,'products.name as product_name'
                ,'rel_user_type_products.price as rate_price'
                ,'rel_user_type_products.type_rate as type_rate'
                ,\DB::raw('
                        (count(tb_rel_container_products.id)) as product_amount
                    ')
                ,\DB::raw('
                        (sum(tb_product_import_to_chaina.weight_per_item)) as product_weight_per_item
                    ')
                ,\DB::raw('tb_product_import_to_chaina.cubic as cubic_item')
            )
            ->join('rel_container_products', 'rel_container_products.qr_code_product_id', '=', 'qr_code_products.id')
            ->join('rel_user_type_products', 'rel_user_type_products.id', '=', 'qr_code_products.rel_user_type_product_id')
            ->join('import_to_chaina', 'import_to_chaina.id', '=', 'qr_code_products.import_to_chaina_id')
            ->join('users', 'users.id', '=', 'import_to_chaina.user_id')
            ->join('product_import_to_chaina', 'product_import_to_chaina.id', '=', 'qr_code_products.product_import_to_chaina_id')
            ->join('products', 'products.id', '=', 'product_import_to_chaina.product_id')
            ->join('product_types', 'product_types.id', '=', 'product_import_to_chaina.product_type_id')
            ->groupBy('qr_code_products.product_import_to_chaina_id')
            ->groupBy('qr_code_products.rel_user_type_product_id_old')
            ->groupBy('product_import_to_chaina.id')
            ->groupBy('import_to_chaina.id')
            ->groupBy('import_to_chaina.po_no')
            ->groupBy('users.customer_general_code')
            ->groupBy('product_types.name')
            ->groupBy('products.name')
            ->groupBy('rel_user_type_products.price')
            ->groupBy('rel_user_type_products.type_rate')
            ->groupBy('product_import_to_chaina.qty')
            ->groupBy('product_import_to_chaina.cubic')
            ->groupBy('product_import_to_chaina.width')
            ->groupBy('product_import_to_chaina.height')
            ->groupBy('product_import_to_chaina.length')
            ->where('rel_container_products.container_id', $id)
            ->get();

        $data['ImportToChainas'] = \App\Models\ImportToChaina::select(
                'users.customer_general_code'
                // sub select get amount product all
                , \DB::raw('
                    (   select count(`tb_qr_code_products`.`id`)
                        from `tb_qr_code_products`
                        where `tb_qr_code_products`.`import_to_chaina_id` = `tb_import_to_chaina`.`id`
                    ) as product_amount
                ')
                // sub select get weight product all
                , \DB::raw('
                    (   select sum(`tb_product_import_to_chaina`.`weight_all`)
                        from `tb_product_import_to_chaina`
                        where `tb_product_import_to_chaina`.`import_to_chaina_id` = `tb_import_to_chaina`.`id`
                    ) as product_KG
                ')
                // sub select get cubic product all
                , \DB::raw('
                    (   select sum(`tb_product_import_to_chaina`.`cubic`)
                        from `tb_product_import_to_chaina`
                        where `tb_product_import_to_chaina`.`import_to_chaina_id` = `tb_import_to_chaina`.`id`
                    ) as product_cubic
                ')
                // get po amount
                , \DB::raw('count(`tb_import_to_chaina`.`id`) as po_amount')
                // sub select get cubic product all real
                , \DB::raw('
                    (   select count(`tb_qr_code_products`.`id`)
                        from `tb_qr_code_products`
                        join `tb_rel_container_products` on `tb_rel_container_products`.`qr_code_product_id` = `tb_qr_code_products`.`id`
                        where `tb_qr_code_products`.`import_to_chaina_id` = `tb_import_to_chaina`.`id`
                    ) as product_amount_real
                ')
                // get p
            )
            ->join('users', 'users.id', '=', 'import_to_chaina.user_id')
            // sub select where
            ->whereRaw('
                (   select count(`tb_qr_code_products`.`id`)
                    from `tb_qr_code_products`
                    join `tb_rel_container_products` on `tb_rel_container_products`.`qr_code_product_id` = `tb_qr_code_products`.`id`
                    where `tb_qr_code_products`.`import_to_chaina_id` = `tb_import_to_chaina`.`id`
                    and `tb_rel_container_products`.`container_id` = '.$id.'
                ) > 0
            ')
            ->groupBy('users.id')
            ->get();
        \Excel::create('Order-Chaina-Container-'.$date, function($excel) use ($data) {
           $excel->sheet('Order-Chaina-Container', function($sheet) use ($data) {
               $sheet->loadView('Admin.order_chaina_container_excel', $data);
           });
       })->download('xls');
    }

    public function ExcelSale($id)
    {
        $date = date('d-m-Y');
        $data['Container'] = \App\Models\Container::find($id);
        $data['QrCodeProducts'] = \App\Models\QrCodeProduct::select(
                'qr_code_products.product_import_to_chaina_id'
                , 'qr_code_products.rel_user_type_product_id_old'
                , 'product_import_to_chaina.id as product_import_to_chaina_id'
                , 'product_import_to_chaina.qty as product_import_to_chaina_qty'
                , 'product_import_to_chaina.cubic as product_import_to_chaina_cubic'
                , 'product_import_to_chaina.width as width'
                , 'product_import_to_chaina.height as height'
                , 'product_import_to_chaina.length as length'
                , 'import_to_chaina.id'
                , 'import_to_chaina.po_no as po_no'
                , 'users.customer_general_code'
                , 'product_types.name as product_type_name'
                , 'products.name as product_name_th'
                , 'products.name_en as product_name_en'
                , 'rel_user_type_products.price as rate_price'
                , 'rel_user_type_products.type_rate as type_rate'
                , \DB::raw('
                        (count(tb_rel_container_products.id)) as product_amount
                    ')
                , \DB::raw('
                        (sum(tb_product_import_to_chaina.weight_per_item)) as product_weight_per_item
                    ')
                , \DB::raw('tb_product_import_to_chaina.cubic as cubic_item')
            )
            ->leftjoin('rel_container_products', 'rel_container_products.qr_code_product_id', '=', 'qr_code_products.id')
            ->leftjoin('rel_user_type_products', 'rel_user_type_products.id', '=', 'qr_code_products.rel_user_type_product_id')
            ->join('import_to_chaina', 'import_to_chaina.id', '=', 'qr_code_products.import_to_chaina_id')
            ->join('users', 'users.id', '=', 'import_to_chaina.user_id')
            ->join('product_import_to_chaina', 'product_import_to_chaina.id', '=', 'qr_code_products.product_import_to_chaina_id')
            ->join('products', 'products.id', '=', 'product_import_to_chaina.product_id')
            ->join('product_types', 'product_types.id', '=', 'product_import_to_chaina.product_type_id')
            ->groupBy('qr_code_products.product_import_to_chaina_id')
            ->groupBy('qr_code_products.rel_user_type_product_id_old')
            ->groupBy('product_import_to_chaina.id')
            ->groupBy('import_to_chaina.id')
            ->groupBy('import_to_chaina.po_no')
            ->groupBy('users.customer_general_code')
            ->groupBy('product_types.name')
            ->groupBy('products.name')
            ->groupBy('rel_user_type_products.price')
            ->groupBy('rel_user_type_products.type_rate')
            ->groupBy('product_import_to_chaina.qty')
            ->groupBy('product_import_to_chaina.cubic')
            ->groupBy('product_import_to_chaina.width')
            ->groupBy('product_import_to_chaina.height')
            ->groupBy('product_import_to_chaina.length')
            ->where('rel_container_products.container_id', $id)
            ->get();
            // dd();
        $data['ImportToChainas'] = \App\Models\ImportToChaina::select(
                'users.customer_general_code'
                // sub select get amount product all
                , \DB::raw('
                    (   select count(`tb_qr_code_products`.`id`)
                        from `tb_qr_code_products`
                        where `tb_qr_code_products`.`import_to_chaina_id` = `tb_import_to_chaina`.`id`
                    ) as product_amount
                ')
                // sub select get weight product all
                , \DB::raw('
                    (   select sum(`tb_product_import_to_chaina`.`weight_all`)
                        from `tb_product_import_to_chaina`
                        where `tb_product_import_to_chaina`.`import_to_chaina_id` = `tb_import_to_chaina`.`id`
                    ) as product_KG
                ')
                // sub select get cubic product all
                , \DB::raw('
                    (   select sum(`tb_product_import_to_chaina`.`cubic`)
                        from `tb_product_import_to_chaina`
                        where `tb_product_import_to_chaina`.`import_to_chaina_id` = `tb_import_to_chaina`.`id`
                    ) as product_cubic
                ')
                // get po amount
                , \DB::raw('count(`tb_import_to_chaina`.`id`) as po_amount')
                // sub select get cubic product all real
                , \DB::raw('
                    (   select count(`tb_qr_code_products`.`id`)
                        from `tb_qr_code_products`
                        join `tb_rel_container_products` on `tb_rel_container_products`.`qr_code_product_id` = `tb_qr_code_products`.`id`
                        where `tb_qr_code_products`.`import_to_chaina_id` = `tb_import_to_chaina`.`id`
                    ) as product_amount_real
                ')
                // get p
            )
            ->join('users', 'users.id', '=', 'import_to_chaina.user_id')
            // sub select where
            ->whereRaw('
                (   select count(`tb_qr_code_products`.`id`)
                    from `tb_qr_code_products`
                    join `tb_rel_container_products` on `tb_rel_container_products`.`qr_code_product_id` = `tb_qr_code_products`.`id`
                    where `tb_qr_code_products`.`import_to_chaina_id` = `tb_import_to_chaina`.`id`
                    and `tb_rel_container_products`.`container_id` = '.$id.'
                ) > 0
            ')
            ->groupBy('users.id')
            ->get();

        \Excel::create('Sale-Chaina-Container-Ptint-'.$date, function($excel) use ($data) {
           $excel->sheet('Sale-Chaina-Container-Ptint', function($sheet) use ($data) {
               $sheet->loadView('Admin.sale_chaina_container_excel', $data);
           });
       })->download('xls');

    }

}
