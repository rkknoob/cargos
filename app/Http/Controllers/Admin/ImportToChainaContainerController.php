<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Storage;
use PDF;
use DB;
use \Mpdf\Mpdf;
use App\Providers\Admin\PermissionCRUDProvider as Permission;

class ImportToChainaContainerController extends Controller
{

    public function index()
    {
    	$data['main_menu'] = 'ImportToChainaContainer';
    	$data['sub_menu'] = '';
    	$data['title'] = trans('lang.import_container_china');
    	$data['title_page'] = trans('lang.import_container_china');
    	$data['permission'] = Permission::CheckPermission($data['main_menu'],$data['sub_menu']);
    	$data['menus'] = \App\Models\AdminMenu::ActiveMenu()->get();
    	return view('Admin.import_to_chaina_container', $data);
    }

    public function ContainerSave(Request $request)
    {
        $input_all = $request->all();
        $validator = Validator::make($request->all(), [
        ]);
        if (!$validator->fails()) {
            \DB::beginTransaction();
            try {
                $data_insert = [
                    'container_code' => $this->runContainerCode(),
                    'transport_type_id' => $input_all['transport_types'],
                    'container_type_id' => $input_all['container_types'],
                    'container_size_id' => $input_all['container_sizes'],
                    'status_id' => 1
                ];

                $id_container = \App\Models\Container::insertGetId($data_insert);
                \DB::commit();
                $return['id_container'] = $id_container;
                $return['status'] = 1;
                $return['content'] = 'สำเร็จ';
            } catch (Exception $e) {
                \DB::rollBack();
                $return['status'] = 0;
                $return['content'] = trans('lang.unsuccessful');
            }
        }else{
            $return['status'] = 0;
        }
        $return['title'] = 'เพิ่มตู้';
        return json_encode($return);
    	// return 1;
    }

    public function destroyContainerProduct(Request $request)
    {
        $input_all = $request->all();

        $id_product = $input_all['id_product'];
        $validator = Validator::make($request->all(), [
        ]);
        if (!$validator->fails()) {
            \DB::beginTransaction();
            try {
                $id_container = \App\Models\RelContainerProduct::where('id', $id_product)->delete();
                \DB::commit();
                $return['status'] = 1;
                $return['content'] = 'สำเร็จ';
            } catch (Exception $e) {
                \DB::rollBack();
                $return['status'] = 0;
                $return['content'] = trans('lang.unsuccessful');
            }
        }else{
            $return['status'] = 0;
        }
        $return['title'] = 'เพิ่มตู้';
        return json_encode($return);
    }

    public function create()
    {
    	$data['main_menu'] = 'ImportToChainaContainer';
    	$data['sub_menu'] = '';
    	$data['title'] = 'เพิ่มรับของเข้าตู้ (จีน)';
    	$data['title_page'] = 'เพิ่มรับของเข้าตู้ (จีน)';
    	$data['permission'] = Permission::CheckPermission($data['main_menu'],$data['sub_menu']);
    	$data['menus'] = \App\Models\AdminMenu::ActiveMenu()->get();
        $data['Users'] = \App\Models\User::get();
        $data['ContainerSizes'] = \App\Models\ContainerSize::get();
        $data['ContainerCode'] = $this->runContainerCode();
        $data['ContainerTypes'] = \App\Models\ContainerType::get();
        $data['TransportTypes'] = \App\Models\TransportType::get();
        // $data['po_number'] = $this->runCode();
        return view('Admin.import_to_chaina_container_create', $data);
    }

    public function MobileCreate()
    {
    	$data['main_menu'] = 'ImportToChainaContainer';
    	$data['sub_menu'] = '';
    	$data['title'] = 'เพิ่มรับของเข้าตู้ (จีน)';
    	$data['title_page'] = 'เพิ่มรับของเข้าตู้ (จีน)';
    	$data['permission'] = Permission::CheckPermission($data['main_menu'],$data['sub_menu']);
    	$data['menus'] = \App\Models\AdminMenu::ActiveMenu()->get();
        $data['Users'] = \App\Models\User::get();
        $data['ContainerSizes'] = \App\Models\ContainerSize::get();
        $data['ContainerCode'] = $this->runContainerCode();
        $data['ContainerTypes'] = \App\Models\ContainerType::get();
        $data['TransportTypes'] = \App\Models\TransportType::get();
        // $data['po_number'] = $this->runCode();
        return view('Admin.import_to_chaina_container_mobile_create', $data);
    }

    public function store(Request $request)
    {
        $input_all = $request->all();

        $container_id = $input_all['container_id'];

        $weight = $width = $height = $length = $cubic =  0;
        $validator = Validator::make($request->all(), [
            // 'import_to_chaina[po_number]'   => 'required',
            // 'import_to_chaina[customer_id]'   => 'required',
        ]);
        if (!$validator->fails()) {
            \DB::beginTransaction();
            try {
                $data_for_delete = \App\Models\ContainerPhoto::where('id', $container_id)->get();
                foreach ($data_for_delete as $key => $data_for_deletes) {
                    $photo =  json_decode($data_for_deletes->photo);
                    $photo = !empty($photo[0]) ? $photo[0] : '';
                    if (Storage::disk("uploads")->exists("Container/" . $photo)) {
                        Storage::disk("uploads")->delete("Container/" . $photo);
                    }
                }
                \App\Models\ContainerPhoto::where('container_id', $container_id)->delete();
                $qrcode_for_sum = \App\Models\QrCodeProduct::select(
                        'qr_code_products.*'
                        , 'import_to_chaina.po_no'
                        , 'lot_products.id as lot_product_id'
                        , 'lot_products.width'
                        , 'lot_products.height'
                        , 'lot_products.length'
                        , 'lot_products.weight_per_item'
                        , 'lot_products.cubic as product_cobic'
                        , 'rel_container_products.id as rel_container_product_id'
                        , \DB::raw('
                                (select sum(qty)
                                from tb_product_import_to_chaina
                                where tb_product_import_to_chaina.import_to_chaina_id = tb_import_to_chaina.id
                                ) as po_qty
                            ')
                        , \DB::raw('(`tb_lot_products`.`product_sort_end` - `tb_lot_products`.`product_sort_start`) + 1 as lot_product_qty')
                    )
                    ->join('rel_container_products', 'rel_container_products.qr_code_product_id', '=', 'qr_code_products.id')
                    ->join('import_to_chaina', 'import_to_chaina.id', '=', 'qr_code_products.import_to_chaina_id')
                    ->join('product_import_to_chaina', 'product_import_to_chaina.id', '=', 'qr_code_products.product_import_to_chaina_id')
                    // join lot product
                    ->join('lot_products', 'lot_products.product_import_to_chaina_id', '=', 'qr_code_products.product_import_to_chaina_id')
                    ->whereRaw('`tb_qr_code_products`.`sort_id` BETWEEN `tb_lot_products`.`product_sort_start` AND `tb_lot_products`.`product_sort_end`')
                    //end join lot product
                    ->where('rel_container_products.container_id', $container_id)
                    ->get();
                foreach($qrcode_for_sum as $key1 => $value1){
                    $weight +=  (!empty($value1->weight_per_item) ? $value1->weight_per_item : 0);
                    $width +=  (!empty($value1->width) ? $value1->width : 0);
                    $height +=  (!empty($value1->height) ? $value1->height : 0);
                    $length +=  (!empty($value1->length) ? $value1->length : 0);
                    $cubic +=  (!empty($value1->product_cobic) ? $value1->product_cobic : 0);
                    $po_update_status[$value1->po_no] = $value1->po_no;
                }
                // dd([$weight, $width, $height, $length,  $cubic, $po_update_status]);
                $data_update_container = [
                    'transport_type_id' => $input_all['transport_types'],
                    'container_type_id' => $input_all['container_types'],
                    'container_size_id' => $input_all['container_sizes'],
                    // 'container_code' => $this->runContainerCode(),
                    'weight' => $weight,
                    'width' => $width,
                    'height' => $height,
                    'length' => $length,
                    'cubic' => $cubic,
                    'remark' => $input_all['container_remark'],
                    'tag' => $input_all['container_tag'],
                    'container_no' => $input_all['container_no'],
                    // 'status_id' => 1,
                    'date_chaina' => date('Y-m-d H:i:s'),
                    'created_at' => date('Y-m-d H:i:s'),
                ];

                \App\Models\Container::where('id', $container_id)->update($data_update_container);
                foreach ($po_update_status as $key_po => $value_po) {
                    \App\Models\ImportToChaina::where(['po_no'=> $key_po,'status_id'=> 1])->update(['status_id' => 3]);
                    // send sms
                    $po_status_sms = \App\Models\ImportToChaina::select(
                            'import_to_chaina.user_id',
                            'import_to_chaina.id',
                            DB::raw('count(tb_qr_code_products.id) as amount_package')
                        )
                        ->join('qr_code_products', 'qr_code_products.import_to_chaina_id', '=', 'import_to_chaina.id')
                        ->join('rel_container_products', 'rel_container_products.qr_code_product_id', '=', 'qr_code_products.id')
                        ->groupBy([
                            'import_to_chaina.user_id', 'import_to_chaina.id'
                        ])
                        ->where(['po_no'=> $key_po])->first();
                    $check_po_status_sms = \App\Models\ImportToChaina::select('import_to_chaina.*')
                        ->join('send_sms', 'send_sms.import_to_chaina_id', '=', 'import_to_chaina.id')
                        ->where('po_no', $key_po)
                        ->where('send_sms.type_status', 2)
                        ->first();
                    // user
                    $User = \App\Models\User::find($po_status_sms->user_id);
                    if(empty($check_po_status_sms) && !empty($po_status_sms) && !empty($User) && $User->main_mobile != null){
                        app('App\Http\Controllers\Admin\ManageSmsController')->SendSMS($key_po, 2, $po_status_sms->amount_package, $msisdn = $User->main_mobile);
                        \App\Models\SendSMS::insert([
                            'import_to_chaina_id'=> $po_status_sms->id
                            , 'type_status'=> 2
                            , 'created_at'=> date('Y-m-d H:i:s')
                        ]);
                    }
                    //end send sms
                }
                // $import_to_chaina    = \App\Models\ReceiversChin::find($import_to_chaina_id);
                // insert img before_close_container_img
                if (isset($input_all['before_close_container_img']) && count($input_all['before_close_container_img']) > 0) {
                    foreach ($input_all['before_close_container_img'] as $key => $value) {
                        if (Storage::disk("uploads")->exists("temp/" . $value) && !Storage::disk("uploads")->exists("Container/" . $value)) {
                            Storage::disk("uploads")->copy("temp/" . $value, "Container/" . $value);
                            Storage::disk("uploads")->delete("temp/" . $value);
                        }
                    }
                    $data_insert_before_img = [
                        'container_id' => $container_id,
                        'type_container_photo_id' => '1',
                        'photo' => json_encode($input_all['before_close_container_img']),
                        'created_at' => date('Y-m-d H:i:s'),
                    ];
                    \App\Models\ContainerPhoto::insert($data_insert_before_img);
                }
                else{
                    $delete_photo = \App\Models\ContainerPhoto::where('container_id', $container_id)
                        ->where('type_container_photo_id', '1')
                        ->first();
                    if(!empty($delete_photo)){
                        $photo =  json_decode($delete_photo->photo);
                        $photo = !empty($photo[0]) ? $photo[0] : '';
                        if (Storage::disk("uploads")->exists("Container/" . $photo)) {
                            Storage::disk("uploads")->delete("Container/" . $photo);
                        }
                    }
                }
                // insert img after_close_container_img
                if (isset($input_all['after_close_container_img']) && count($input_all['after_close_container_img']) > 0) {
                    foreach ($input_all['after_close_container_img'] as $key => $value) {
                        if (Storage::disk("uploads")->exists("temp/" . $value) && !Storage::disk("uploads")->exists("Container/" . $value)) {
                            Storage::disk("uploads")->copy("temp/" . $value, "Container/" . $value);
                            Storage::disk("uploads")->delete("temp/" . $value);
                        }
                    }
                    $data_insert_after_img = [
                        'container_id' => $container_id,
                        'type_container_photo_id' => '2',
                        'photo' => json_encode($input_all['after_close_container_img']),
                        'created_at' => date('Y-m-d H:i:s'),
                    ];
                    \App\Models\ContainerPhoto::insert($data_insert_after_img);
                }
                else{
                    $delete_photo = \App\Models\ContainerPhoto::where('container_id', $container_id)
                        ->where('type_container_photo_id', '2')
                        ->first();
                    if(!empty($delete_photo)){
                        $photo =  json_decode($delete_photo->photo);
                        $photo = !empty($photo[0]) ? $photo[0] : '';
                        if (Storage::disk("uploads")->exists("Container/" . $photo)) {
                            Storage::disk("uploads")->delete("Container/" . $photo);
                        }
                    }
                }
                // insert img tag_container_img
                if (isset($input_all['tag_container_img']) && count($input_all['tag_container_img']) > 0) {
                    foreach ($input_all['tag_container_img'] as $key => $value) {
                        if (Storage::disk("uploads")->exists("temp/" . $value) && !Storage::disk("uploads")->exists("Container/" . $value)) {
                            Storage::disk("uploads")->copy("temp/" . $value, "Container/" . $value);
                            Storage::disk("uploads")->delete("temp/" . $value);
                        }
                    }
                    $data_insert_tag_img = [
                        'container_id' => $container_id,
                        'type_container_photo_id' => '3',
                        'photo' => json_encode($input_all['tag_container_img']),
                        'created_at' => date('Y-m-d H:i:s'),
                    ];
                    \App\Models\ContainerPhoto::insert($data_insert_tag_img);
                }
                else{
                    $delete_photo = \App\Models\ContainerPhoto::where('container_id', $container_id)
                        ->where('type_container_photo_id', '3')
                        ->first();
                    if(!empty($delete_photo)){
                        $photo =  json_decode($delete_photo->photo);
                        $photo = !empty($photo[0]) ? $photo[0] : '';
                        if (Storage::disk("uploads")->exists("Container/" . $photo)) {
                            Storage::disk("uploads")->delete("Container/" . $photo);
                        }
                    }
                }
                // end insert img all
                \DB::commit();
                $return['status'] = 1;
                $return['content'] = 'สำเร็จ';
            } catch (Exception $e) {
                \DB::rollBack();
                $return['status'] = 0;
                $return['content'] = trans('lang.unsuccessful').$e->getMessage();;
            }
        }else{
            $return['status'] = 0;
        }
        $return['title'] = trans('lang.create_data');
        return json_encode($return);
    	// return 1;
    }

    public function show(Request $request)
    {
        if(!empty($request->input('container_id'))){
            $data['qrcode'] = $request->input('qrcode');
            $data['QrCodeProduct'] = \App\Models\QrCodeProduct::select(
                    'qr_code_products.*'
                    , 'import_to_chaina.po_no'
                    , 'lot_products.id as lot_product_id'
                    , 'users.customer_general_code'
                    , 'product_types.name as product_type_name'
                    , 'products.name as product_name'
                    , 'lot_products.width'
                    , 'lot_products.height'
                    , 'lot_products.length'
                    , 'lot_products.weight_per_item'
                    , 'lot_products.cubic as product_cobic'
                    , \DB::raw('
                            (select sum(qty)
                            from tb_product_import_to_chaina
                            where tb_product_import_to_chaina.import_to_chaina_id = tb_import_to_chaina.id
                            ) as po_qty
                        ')
                    , \DB::raw('(`tb_lot_products`.`product_sort_end` - `tb_lot_products`.`product_sort_start`) + 1 as lot_product_qty')
                )
                ->leftjoin('rel_container_products', 'rel_container_products.qr_code_product_id', '=', 'qr_code_products.id')
                ->join('import_to_chaina', 'import_to_chaina.id', '=', 'qr_code_products.import_to_chaina_id')
                ->join('users', 'users.id', '=', 'import_to_chaina.user_id')
                ->join('product_import_to_chaina', 'product_import_to_chaina.id', '=', 'qr_code_products.product_import_to_chaina_id')
                ->join('products', 'products.id', '=', 'product_import_to_chaina.product_id')
                ->join('product_types', 'product_types.id', '=', 'product_import_to_chaina.product_type_id')
                // join lot product
                ->join('lot_products', 'lot_products.product_import_to_chaina_id', '=', 'qr_code_products.product_import_to_chaina_id')
                ->whereRaw('`tb_qr_code_products`.`sort_id` BETWEEN `tb_lot_products`.`product_sort_start` AND `tb_lot_products`.`product_sort_end`')
                //end join lot product
                ->where('qr_code_products.qr_code', $request->input('qrcode'))
                ->whereNull('rel_container_products.id')
                ->first();
            if(!empty($data['QrCodeProduct']) && !empty($request->input('container_id'))){
                // insert product from QRcode
                $data_insert_rel_container = [
                    'container_id' => $request->input('container_id'),
                    'qr_code_product_id' => $data['QrCodeProduct']->id,
                    'created_at' => date('Y-m-d H:i:s'),
                ];
                $data['rel_container_product_id'] = \App\Models\RelContainerProduct::insertGetId($data_insert_rel_container);
                // end insert product from QRcode
            }
            return json_encode($data);
        }
        $data['QrCodeProduct'] = NULL;
        return json_encode($data);
    }

    public function GetContainerDetail($id)
    {
        $data['container_id'] = $id;
        $data['Containers'] = \App\Models\Container::
            with('TransportType', 'ContainerType', 'ContainerSize')
            ->where('containers.id', $id)
            ->first();
         $data['QrCodeProduct'] = \App\Models\QrCodeProduct::select(
                'qr_code_products.*'
                , 'import_to_chaina.po_no'
                , 'lot_products.id as lot_product_id'
                , 'users.customer_general_code'
                , 'product_types.name as product_type_name'
                , 'products.name as product_name'
                , 'lot_products.width'
                , 'lot_products.height'
                , 'lot_products.length'
                , 'lot_products.weight_per_item'
                , 'lot_products.cubic as product_cobic'
                , \DB::raw('
                        (select sum(qty)
                        from tb_product_import_to_chaina
                        where tb_product_import_to_chaina.import_to_chaina_id = tb_import_to_chaina.id
                        ) as po_qty
                    ')
                , \DB::raw('(`tb_lot_products`.`product_sort_end` - `tb_lot_products`.`product_sort_start`) + 1 as lot_product_qty')
            )
            ->join('rel_container_products', 'rel_container_products.qr_code_product_id', '=', 'qr_code_products.id')
            ->join('import_to_chaina', 'import_to_chaina.id', '=', 'qr_code_products.import_to_chaina_id')
            ->join('users', 'users.id', '=', 'import_to_chaina.user_id')
            ->join('product_import_to_chaina', 'product_import_to_chaina.id', '=', 'qr_code_products.product_import_to_chaina_id')
            ->join('products', 'products.id', '=', 'product_import_to_chaina.product_id')
            ->join('product_types', 'product_types.id', '=', 'product_import_to_chaina.product_type_id')
            // join lot product
            ->join('lot_products', 'lot_products.product_import_to_chaina_id', '=', 'qr_code_products.product_import_to_chaina_id')
            ->whereRaw('`tb_qr_code_products`.`sort_id` BETWEEN `tb_lot_products`.`product_sort_start` AND `tb_lot_products`.`product_sort_end`')
            //end join lot product
            ->where('rel_container_products.container_id', $id)
            ->get();
        return json_encode($data);
    }

    public function edit($id)
    {
    	$data['main_menu'] = 'ImportToChainaContainer';
    	$data['sub_menu'] = '';
    	$data['title'] = 'เพิ่มรับของเข้าตู้ (จีน)';
    	$data['title_page'] = 'เพิ่มรับของเข้าตู้ (จีน)';
    	$data['permission'] = Permission::CheckPermission($data['main_menu'],$data['sub_menu']);
    	$data['menus'] = \App\Models\AdminMenu::ActiveMenu()->get();
        $data['Users'] = \App\Models\User::get();
        $data['ContainerSizes'] = \App\Models\ContainerSize::get();
        $data['ContainerTypes'] = \App\Models\ContainerType::get();
        $data['TransportTypes'] = \App\Models\TransportType::get();
        // $data['po_number'] = $this->runCode();

        $data['Container'] = \App\Models\Container::find($id);
        $data['ContainerCode'] = $data['Container']->container_code;
        $data['ContainerPhoto'] = \App\Models\ContainerPhoto::where('container_photos.container_id', $id)->get();
        $data['RelProductContainer'] = \App\Models\QrCodeProduct::select(
                'qr_code_products.*'
                , 'import_to_chaina.po_no'
                , 'lot_products.id as lot_product_id'
                , 'users.customer_general_code'
                , 'product_types.name as product_type_name'
                , 'products.name as product_name'
                , 'lot_products.width'
                , 'lot_products.height'
                , 'lot_products.length'
                , 'lot_products.weight_per_item'
                , 'lot_products.cubic as product_cobic'
                , 'rel_container_products.id as rel_container_product_id'
                , \DB::raw('
                        (select sum(qty)
                        from tb_product_import_to_chaina
                        where tb_product_import_to_chaina.import_to_chaina_id = tb_import_to_chaina.id
                        ) as po_qty
                    ')
                , \DB::raw('(`tb_lot_products`.`product_sort_end` - `tb_lot_products`.`product_sort_start`) + 1 as lot_product_qty')
            )
            ->join('rel_container_products', 'rel_container_products.qr_code_product_id', '=', 'qr_code_products.id')
            ->join('import_to_chaina', 'import_to_chaina.id', '=', 'qr_code_products.import_to_chaina_id')
            ->join('users', 'users.id', '=', 'import_to_chaina.user_id')
            ->join('product_import_to_chaina', 'product_import_to_chaina.id', '=', 'qr_code_products.product_import_to_chaina_id')
            ->join('products', 'products.id', '=', 'product_import_to_chaina.product_id')
            ->join('product_types', 'product_types.id', '=', 'product_import_to_chaina.product_type_id')
            // join lot product
            ->join('lot_products', 'lot_products.product_import_to_chaina_id', '=', 'qr_code_products.product_import_to_chaina_id')
            ->whereRaw('`tb_qr_code_products`.`sort_id` BETWEEN `tb_lot_products`.`product_sort_start` AND `tb_lot_products`.`product_sort_end`')
            //end join lot product
            ->where('rel_container_products.container_id', $id)
            ->orderBy('qr_code_products.id', 'asc')
            ->get();
        // dd($data);
        foreach ($data['ContainerPhoto'] as $key => $ContainerPhoto) {
            $photo =  json_decode($ContainerPhoto->photo);
            if($ContainerPhoto->type_container_photo_id == 1){
                $data['photo_type_container_1'] = !empty($photo[0]) ? $photo[0] : '';
                if (!empty($photo[0])) {
                    if (Storage::disk("uploads")->exists("Container/" . $data['photo_type_container_1'])) {
                        if (Storage::disk("uploads")->exists("temp/" . $data['photo_type_container_1'])) {
                            Storage::disk("uploads")->delete("temp/" . $data['photo_type_container_1']);
                        }
                        Storage::disk("uploads")->copy("Container/" . $data['photo_type_container_1'], "temp/" . $data['photo_type_container_1']);
                    }
                }
            }else if($ContainerPhoto->type_container_photo_id == 2){
                $data['photo_type_container_2'] = !empty($photo[0]) ? $photo[0] : '' ;
                if (!empty($photo[0])) {
                    if (Storage::disk("uploads")->exists("Container/" . $data['photo_type_container_2'])) {
                        if (Storage::disk("uploads")->exists("temp/" . $data['photo_type_container_2'])) {
                            Storage::disk("uploads")->delete("temp/" . $data['photo_type_container_2']);
                        }
                        Storage::disk("uploads")->copy("Container/" . $data['photo_type_container_2'], "temp/" . $data['photo_type_container_2']);
                    }
                }

            }else if($ContainerPhoto->type_container_photo_id == 3){
                $data['photo_type_container_3'] = !empty($photo[0]) ? $photo[0] : '' ;
                if (!empty($photo[0])) {
                    if (Storage::disk("uploads")->exists("Container/" . $data['photo_type_container_3'])) {
                        if (Storage::disk("uploads")->exists("temp/" . $data['photo_type_container_3'])) {
                            Storage::disk("uploads")->delete("temp/" . $data['photo_type_container_3']);
                        }
                        Storage::disk("uploads")->copy("Container/" . $data['photo_type_container_3'], "temp/" . $data['photo_type_container_3']);
                    }
                }

            }
        }
        return view('Admin.import_to_chaina_container_create', $data);
    	// return 3;
    }

    public function MobileEdit($id)
    {
    	$data['main_menu'] = 'ImportToChainaContainer';
    	$data['sub_menu'] = '';
    	$data['title'] = 'เพิ่มรับของเข้าตู้ (จีน)';
    	$data['title_page'] = 'เพิ่มรับของเข้าตู้ (จีน)';
    	$data['permission'] = Permission::CheckPermission($data['main_menu'],$data['sub_menu']);
    	$data['menus'] = \App\Models\AdminMenu::ActiveMenu()->get();
        $data['Users'] = \App\Models\User::get();
        $data['ContainerSizes'] = \App\Models\ContainerSize::get();
        $data['ContainerTypes'] = \App\Models\ContainerType::get();
        $data['TransportTypes'] = \App\Models\TransportType::get();
        // $data['po_number'] = $this->runCode();

        $data['Container'] = \App\Models\Container::find($id);
        $data['ContainerCode'] = $data['Container']->container_code;
        $data['ContainerPhoto'] = \App\Models\ContainerPhoto::where('container_photos.container_id', $id)->get();
        $data['RelProductContainer'] = \App\Models\QrCodeProduct::select(
                'qr_code_products.*'
                , 'import_to_chaina.po_no'
                , 'lot_products.id as lot_product_id'
                , 'users.customer_general_code'
                , 'product_types.name as product_type_name'
                , 'products.name as product_name'
                , 'lot_products.width'
                , 'lot_products.height'
                , 'lot_products.length'
                , 'lot_products.weight_per_item'
                , 'lot_products.cubic as product_cobic'
                , 'rel_container_products.id as rel_container_product_id'
                , \DB::raw('
                        (select sum(qty)
                        from tb_product_import_to_chaina
                        where tb_product_import_to_chaina.import_to_chaina_id = tb_import_to_chaina.id
                        ) as po_qty
                    ')
                , \DB::raw('(`tb_lot_products`.`product_sort_end` - `tb_lot_products`.`product_sort_start`) + 1 as lot_product_qty')
            )
            ->join('rel_container_products', 'rel_container_products.qr_code_product_id', '=', 'qr_code_products.id')
            ->join('import_to_chaina', 'import_to_chaina.id', '=', 'qr_code_products.import_to_chaina_id')
            ->join('users', 'users.id', '=', 'import_to_chaina.user_id')
            ->join('product_import_to_chaina', 'product_import_to_chaina.id', '=', 'qr_code_products.product_import_to_chaina_id')
            ->join('products', 'products.id', '=', 'product_import_to_chaina.product_id')
            ->join('product_types', 'product_types.id', '=', 'product_import_to_chaina.product_type_id')
            // join lot product
            ->join('lot_products', 'lot_products.product_import_to_chaina_id', '=', 'qr_code_products.product_import_to_chaina_id')
            ->whereRaw('`tb_qr_code_products`.`sort_id` BETWEEN `tb_lot_products`.`product_sort_start` AND `tb_lot_products`.`product_sort_end`')
            //end join lot product
            ->where('rel_container_products.container_id', $id)
            ->orderBy('qr_code_products.id', 'asc')
            ->get();
        // dd($data);
        foreach ($data['ContainerPhoto'] as $key => $ContainerPhoto) {
            $photo =  json_decode($ContainerPhoto->photo);
            if($ContainerPhoto->type_container_photo_id == 1){
                $data['photo_type_container_1'] = !empty($photo[0]) ? $photo[0] : '';
                if (!empty($photo[0])) {
                    if (Storage::disk("uploads")->exists("Container/" . $data['photo_type_container_1'])) {
                        if (Storage::disk("uploads")->exists("temp/" . $data['photo_type_container_1'])) {
                            Storage::disk("uploads")->delete("temp/" . $data['photo_type_container_1']);
                        }
                        Storage::disk("uploads")->copy("Container/" . $data['photo_type_container_1'], "temp/" . $data['photo_type_container_1']);
                    }
                }
            }else if($ContainerPhoto->type_container_photo_id == 2){
                $data['photo_type_container_2'] = !empty($photo[0]) ? $photo[0] : '' ;
                if (!empty($photo[0])) {
                    if (Storage::disk("uploads")->exists("Container/" . $data['photo_type_container_2'])) {
                        if (Storage::disk("uploads")->exists("temp/" . $data['photo_type_container_2'])) {
                            Storage::disk("uploads")->delete("temp/" . $data['photo_type_container_2']);
                        }
                        Storage::disk("uploads")->copy("Container/" . $data['photo_type_container_2'], "temp/" . $data['photo_type_container_2']);
                    }
                }

            }else if($ContainerPhoto->type_container_photo_id == 3){
                $data['photo_type_container_3'] = !empty($photo[0]) ? $photo[0] : '' ;
                if (!empty($photo[0])) {
                    if (Storage::disk("uploads")->exists("Container/" . $data['photo_type_container_3'])) {
                        if (Storage::disk("uploads")->exists("temp/" . $data['photo_type_container_3'])) {
                            Storage::disk("uploads")->delete("temp/" . $data['photo_type_container_3']);
                        }
                        Storage::disk("uploads")->copy("Container/" . $data['photo_type_container_3'], "temp/" . $data['photo_type_container_3']);
                    }
                }

            }
        }
        return view('Admin.import_to_chaina_container_mobile_create', $data);
    	// return 3;
    }

    public function update($id, Request $request)
    {
        $return['title'] = trans('lang.edit_data');
        // dd($data_insert_all);
        return json_encode($return);
    	// return 1;
    }

    public function Lists(Request $request)
    {
        $result = \App\Models\Container::select(
                'containers.*'
                ,'transport_types.name_th as transport_type_name_th'
                ,'transport_types.name_en as transport_type_name_en'
                ,'transport_types.name_ch as transport_type_name_ch'
                ,'container_types.name_th as container_type_name_th'
                ,'container_types.name_en as container_type_name_en'
                ,'container_types.name_ch as container_type_name_ch'
                ,'container_sizes.name as container_size_name'
                ,'container_statuses.color as container_status_color'
                ,'container_statuses.name as container_status_name'
                ,\DB::raw('
                    (select count(`tb_rel_container_products`.`id`)
                    from `tb_rel_container_products`
                    where `tb_rel_container_products`.`container_id` = `tb_containers`.`id`)
                    as qty_product_real
                ')
            )
            ->leftJoin('transport_types','transport_types.id','=','containers.transport_type_id')
            ->leftJoin('container_types','container_types.id','=','containers.container_type_id')
            ->leftJoin('container_sizes','container_sizes.id','=','containers.container_size_id')
            ->leftJoin('container_statuses','container_statuses.id','=','containers.status_id')
            ->where('status_id', 1)
            ;
        return \Datatables::of($result)
        ->addIndexColumn()
        ->editColumn('updated_at', function($rec){
            return $rec->updated_at;
        })
        ->editColumn('size', function($rec){
            $weight = number_format(pow($rec->cubic, 1/3), 2) .'x'. number_format(pow($rec->cubic, 1/3), 2) .'x'. number_format(pow($rec->cubic, 1/3), 2);
            return $weight;
        })
        ->editColumn('weight', function($rec){
            $weight = number_format($rec->weight, 2);
            return $weight;
        })
        ->editColumn('qty_product_real', function($rec){
            $qty_product_real = number_format($rec->qty_product_real);

            return $qty_product_real;
        })
        ->editColumn('cubic', function($rec){
            $cubic =  number_format($rec->cubic, 2);
            return $cubic;
        })
        ->editColumn('status_id', function($rec){
            $color_status = (!empty($rec->container_status_color) ? $rec->container_status_color : '#000');
            return '<label class="label" style="background-color: '. $color_status .';">'. $rec->container_status_name .'</label>';
        })
        ->addColumn('null',function($rec){
            $str='';
            return $str;
        })

        ->addColumn('action',function($rec){
            $lang = \Config::get('app.locale');
            $str = '';
            $str .='
                <button  class="btn btn-xs btn-default btn-condensed btn-container-detail btn-tooltip" data-id="'.$rec->id.'" data-rel="tooltip" title="'.trans('lang.detail_container').'">
                    <i class="ace-icon fa fa-info bigger-120"></i>
                </button>
            ';
            if($rec->status_id < 2){
                $str .='
                <a href="'. url('admin/'.$lang.'/ImportToChainaContainer/edit/'.$rec->id) .'" data-loading-text="<i class=\'fa fa-refresh fa-spin\'></i>" class="btn btn-xs btn-warning btn-condensed btn-edit btn-tooltip" data-rel="tooltip" data-id="'.$rec->id.'" title="'.trans('lang.edit').'">
                <i class="ace-icon fa fa-edit bigger-120"></i>
                </a>
                ';
            }
            $str .='
                <a href="'.url('admin/'.$lang.'/ImportToChainaContainer/PDF_Container/'. $rec->id).'" class="btn btn-xs btn-info btn-condensed btn-tooltip" data-id="'.$rec->id.'" data-rel="tooltip" title="'.trans('lang.chinese_cabinet_documents').'" target="_blank">
                    <i class="ace-icon fa fa-print bigger-120"></i>
                </a>
                <a href="'.url('admin/'.$lang.'/ImportToChainaContainer/Excel_Container/'. $rec->id).'" class="btn btn-xs btn-success btn-condensed btn-tooltip" data-id="'.$rec->id.'" data-rel="tooltip" title="'.trans('lang.chinese_cabinet_documents').'">
                    <i class="ace-icon fa fa-list bigger-120"></i>
                </a>
                <a href="'.url('admin/'.$lang.'/ImportToChainaContainer/PDF_ContainerSendStation/'. $rec->id).'" class="btn btn-xs btn-info btn-condensed btn-tooltip" data-id="'.$rec->id.'" data-rel="tooltip" title="'.trans('lang.Sending_documents').' " target="_blank">
                    <i class="ace-icon fa fa-print bigger-120"></i>
                </a>
                <a href="'.url('admin/'.$lang.'/ImportToChainaContainer/Excel_ContainerSendStation/'. $rec->id).'" class="btn btn-xs btn-success btn-condensed btn-tooltip" data-id="'.$rec->id.'" data-rel="tooltip" title="'.trans('lang.Sending_documents').' ">
                    <i class="ace-icon fa fa-list bigger-120"></i>
                </a>
            ';
            // <button  class="btn btn-xs btn-danger btn-condensed btn-delete btn-tooltip" data-id="'.$rec->id.'" data-rel="tooltip" title="ลบ">
            // <i class="ace-icon fa fa-trash bigger-120"></i>
            // </button>
            return $str;
        })
        ->rawColumns(['status_id', 'action'])
        ->make(true);
    }

    public function destroy($id)
    {
        \DB::beginTransaction();
        try {
            // \App\Models\QrCodeProduct::where('import_to_chaina_id', $id)->delete();
            // $products = \App\Models\ProductImportToChainaContainer::where('import_to_chaina_id', $id)->get();
            // foreach($products as $product){
            //     \App\Models\LotProduct::where('product_import_to_chaina_id', $product->id)->delete();
            // }
            // \App\Models\ProductImportToChainaContainer::where('import_to_chaina_id', $id)->delete();
            // \App\Models\ImportToChainaContainer::where('id', $id)->delete();
            \DB::commit();
            $return['status'] = 1;
            $return['content'] = 'สำเร็จ';
        } catch (Exception $e) {
            \DB::rollBack();
            $return['status'] = 0;
            $return['content'] = trans('lang.unsuccessful'). $e->getMessage();;
        }
        $return['title'] = 'ลบข้อมูล';
        return $return;
    }

    public function runContainerCode()
    {
        $date = date('dmy');
        $num = 0;
        $cnt = 0;
        $rs = \App\Models\Container::select('container_code')->orderBy('id', 'DESC')->first();
        if(!empty($rs)){
            $con_code = $rs->container_code;  // R-170600001
            $ckdate = '20'. substr($con_code, 8, 2) .'-'. substr($con_code, 6, 2) .'-'. substr($con_code, 4, 2);//06
            $num = substr($con_code, 1, 2);
            if(strtotime(date('Y-m-d')) == strtotime($ckdate)){
                $cnt = $num+1;
                $shownum = sprintf("%02d", $cnt);
            }
            else{
                $cnt = 1;
                $shownum = sprintf("%02d", $cnt);
            }
        }
        else{
            $cnt = 0;
			$cnt++;
            $shownum = sprintf("%02d", $cnt);
        }
        return 'E'.$shownum .'-'. $date;
    }

    public function PDF_Container($id)
    {
        $data['QrCodeProduct'] = \App\Models\QrCodeProduct::select(
                'qr_code_products.*'
                , 'import_to_chaina.po_no'
                , 'lot_products.id as lot_product_id'
                , 'users.customer_general_code'
                , 'users.product_type_id as user_product_type_id'
                , 'product_types.name as product_type_name'
                , 'products.name_en as product_name'
                , 'product_import_to_chaina.pcs as product_pcs'
                , 'transport_types.name_th as transport_type_name_th'
                , 'transport_types.name_en as transport_type_name_en'
                , 'transport_types.name_ch as transport_type_name_ch'
                , 'lot_products.width', 'lot_products.height', 'lot_products.length', 'lot_products.weight_per_item', 'lot_products.cubic as product_cobic'
                , 'lot_products.created_at as product_created_at'
                , 'containers.container_no'
                , 'containers.container_code', 'containers.created_at as container_created_at'
                , \DB::raw('(`tb_lot_products`.`product_sort_end` - `tb_lot_products`.`product_sort_start`) + 1 as lot_product_qty')
                , \DB::raw('
                    (   select count(*)
                        from tb_qr_code_products qr
                        join tb_rel_container_products rcp on rcp.qr_code_product_id = qr.id
                        join tb_lot_products lp ON lp.product_import_to_chaina_id = qr.product_import_to_chaina_id
                        WHERE qr.sort_id BETWEEN lp.product_sort_start AND lp.product_sort_end
                        and tb_lot_products.id = lp.id
                        and tb_containers.id = rcp.container_id
                    ) as qr_code_container_qty
                ')
            )
            ->join('rel_container_products', 'rel_container_products.qr_code_product_id', '=', 'qr_code_products.id')
            ->join('import_to_chaina', 'import_to_chaina.id', '=', 'qr_code_products.import_to_chaina_id')
            ->join('users', 'users.id', '=', 'import_to_chaina.user_id')
            ->join('product_import_to_chaina', 'product_import_to_chaina.id', '=', 'qr_code_products.product_import_to_chaina_id')
            ->join('products', 'products.id', '=', 'product_import_to_chaina.product_id')
            ->join('product_types', 'product_types.id', '=', 'product_import_to_chaina.product_type_id')
            ->join('containers', 'containers.id', '=', 'rel_container_products.container_id')
            ->join('transport_types', 'transport_types.id', '=', 'product_import_to_chaina.transport_type_id')
            // join lot product
            ->join('lot_products', 'lot_products.product_import_to_chaina_id', '=', 'qr_code_products.product_import_to_chaina_id')
            ->whereRaw('`tb_qr_code_products`.`sort_id` BETWEEN `tb_lot_products`.`product_sort_start` AND `tb_lot_products`.`product_sort_end`')
            //end join lot product
            ->where('rel_container_products.container_id', $id)
            ->orderBy('import_to_chaina.id')
            ->orderBy('qr_code_products.sort_id')
            ->get();
        $data2 = view('Admin.pdf_container_chaina', $data);
        //dd($data);
        $mpdf = new Mpdf([
            'autoLangToFont' => true,
            'mode' => 'utf-8',
            'format' => 'A4-L',
            'margin_top' => 20,
            'margin_left' => 10,
            'margin_right' => 10,
            'margin_bottom' => 10,
        ]);
        $mpdf->setHtmlHeader('<div style="text-align: right; width: 100%;">{PAGENO}</div>');
        $mpdf->WriteHTML($data2);
        $mpdf->Output('Container_'. $id .'_'. date('Y_m_d') .'.pdf', 'I');
    }

    public function PDF_ContainerSendStation($id)
    {
        $data['Company'] = \App\Models\Company::first();
        $data['LotProducts'] = \App\Models\LotProduct::select(
                'import_to_chaina.po_no'
                , 'lot_products.id as lot_product_id'
                , 'users.customer_general_code'
                , 'users.product_type_id as user_product_type_id'
                // , 'product_types.name as product_type_name'
                , 'products.name as product_name'
                , 'products.name_en as product_name_en'
                , 'product_import_to_chaina.pcs as product_pcs'
                , 'product_import_to_chaina.id as product_import_to_chaina_id'
                , 'transport_types.name_th as transport_type_name_th'
                , 'transport_types.name_en as transport_type_name_en'
                , 'transport_types.name_ch as transport_type_name_ch'
                , 'lot_products.width'
                , 'lot_products.height'
                , 'lot_products.length'
                , 'lot_products.weight_per_item'
                , 'lot_products.cubic as product_cobic'
                , 'lot_products.product_sort_end'
                , 'lot_products.product_sort_start'
                , 'lot_products.remark'
                , 'containers.container_code'
                , 'containers.tag'
                , 'containers.container_no'
                , 'containers.created_at as container_created_at'
                , \DB::raw('(`tb_lot_products`.`product_sort_end` - `tb_lot_products`.`product_sort_start`) + 1 as lot_product_qty')
                , \DB::raw('count(`tb_qr_code_products`.`id`) as qr_code_container_qty')
                , 'lot_products.created_at as product_created_at'
            )
            // join qr_code_products
            ->join('qr_code_products', 'qr_code_products.product_import_to_chaina_id', '=', 'lot_products.product_import_to_chaina_id')
            ->whereRaw('`tb_qr_code_products`.`sort_id` BETWEEN `tb_lot_products`.`product_sort_start` AND `tb_lot_products`.`product_sort_end`')
            //end join qr_code_products
            ->join('rel_container_products', 'rel_container_products.qr_code_product_id', '=', 'qr_code_products.id')
            ->join('product_import_to_chaina', 'product_import_to_chaina.id', '=', 'lot_products.product_import_to_chaina_id')
            ->join('import_to_chaina', 'import_to_chaina.id', '=', 'product_import_to_chaina.import_to_chaina_id')
            ->join('users', 'users.id', '=', 'import_to_chaina.user_id')
            ->join('products', 'products.id', '=', 'product_import_to_chaina.product_id')
            // ->join('product_types', 'product_types.id', '=', 'product_import_to_chaina.product_type_id')
            ->join('containers', 'containers.id', '=', 'rel_container_products.container_id')
            ->join('transport_types', 'transport_types.id', '=', 'product_import_to_chaina.transport_type_id')
            ->where('rel_container_products.container_id', $id)
            ->groupBy('lot_products.id')
            ->groupBy('import_to_chaina.po_no')
            ->groupBy('users.customer_general_code')
            ->groupBy('products.name')
            ->groupBy('product_import_to_chaina.pcs')
            ->groupBy('transport_types.name_th')
            ->groupBy('transport_types.name_en')
            ->groupBy('transport_types.name_ch')
            ->groupBy('containers.container_code')
            ->groupBy('lot_products.width')
            ->groupBy('lot_products.height')
            ->groupBy('lot_products.length')
            ->groupBy('lot_products.weight_per_item')
            ->groupBy('lot_products.cubic')
            ->groupBy('lot_products.product_sort_end')
            ->groupBy('lot_products.product_sort_start')
            ->groupBy('containers.created_at')
            ->groupBy('lot_products.remark')
            ->groupBy('lot_products.created_at')
            ->groupBy('users.product_type_id')
            ->orderBy('import_to_chaina.id')
            ->orderBy('product_import_to_chaina.id')
            ->orderBy('lot_products.product_sort_start')
            ->get();

        $data2 = view('Admin.pdf_container_send_station', $data);
        //dd($data);
        $mpdf = new Mpdf([
            'autoLangToFont' => true,
            'autoScriptToLang' => true,
            'mode' => 'utf-8',
            'format' => 'A4-L',
            'margin_top' => 20,
            'margin_left' => 10,
            'margin_right' => 10,
            'margin_bottom' => 10,
        ]);
        // $mpdf->SetAutoFont();
        $mpdf->setHtmlHeader('<div style="text-align: right; width: 100%;">{PAGENO}</div>');
        $mpdf->WriteHTML($data2);
        $mpdf->Output('SendStation_'. $id .'_'. date('Y_m_d') .'.pdf', 'I');
    }

    public function Excel_Container($id)
    {
        $date = date('d-m-Y');

        $data['QrCodeProduct'] = \App\Models\QrCodeProduct::select(
                'qr_code_products.*'
                , 'import_to_chaina.po_no'
                , 'lot_products.id as lot_product_id'
                , 'users.customer_general_code'
                , 'users.product_type_id as user_product_type_id'
                , 'product_types.name as product_type_name'
                , 'products.name as product_name'
                , 'product_import_to_chaina.pcs as product_pcs'
                , 'transport_types.name_th as transport_type_name_th'
                , 'transport_types.name_en as transport_type_name_en'
                , 'transport_types.name_ch as transport_type_name_ch'
                , 'lot_products.width', 'lot_products.height', 'lot_products.length', 'lot_products.weight_per_item', 'lot_products.cubic as product_cobic'
                , 'lot_products.created_at as product_created_at'
                , 'containers.container_code', 'containers.created_at as container_created_at'
                , \DB::raw('(`tb_lot_products`.`product_sort_end` - `tb_lot_products`.`product_sort_start`) + 1 as lot_product_qty')
                , \DB::raw('
                    (   select count(*)
                        from tb_qr_code_products qr
                        join tb_rel_container_products rcp on rcp.qr_code_product_id = qr.id
                        join tb_lot_products lp ON lp.product_import_to_chaina_id = qr.product_import_to_chaina_id
                        WHERE qr.sort_id BETWEEN lp.product_sort_start AND lp.product_sort_end
                        and tb_lot_products.id = lp.id
                        and tb_containers.id = rcp.container_id
                    ) as qr_code_container_qty
                ')
            )
            ->join('rel_container_products', 'rel_container_products.qr_code_product_id', '=', 'qr_code_products.id')
            ->join('import_to_chaina', 'import_to_chaina.id', '=', 'qr_code_products.import_to_chaina_id')
            ->join('users', 'users.id', '=', 'import_to_chaina.user_id')
            ->join('product_import_to_chaina', 'product_import_to_chaina.id', '=', 'qr_code_products.product_import_to_chaina_id')
            ->join('products', 'products.id', '=', 'product_import_to_chaina.product_id')
            ->join('product_types', 'product_types.id', '=', 'product_import_to_chaina.product_type_id')
            ->join('containers', 'containers.id', '=', 'rel_container_products.container_id')
            ->join('transport_types', 'transport_types.id', '=', 'product_import_to_chaina.transport_type_id')
            // join lot product
            ->join('lot_products', 'lot_products.product_import_to_chaina_id', '=', 'qr_code_products.product_import_to_chaina_id')
            ->whereRaw('`tb_qr_code_products`.`sort_id` BETWEEN `tb_lot_products`.`product_sort_start` AND `tb_lot_products`.`product_sort_end`')
            //end join lot product
            ->where('rel_container_products.container_id', $id)
            ->get();

        \Excel::create('Chinese-Cabinet-Documents-'.$date, function($excel) use ($data) {
           $excel->sheet('Chinese-Cabinet-Documents', function($sheet) use ($data) {
               $sheet->loadView('Admin.excel_container_chaina', $data);
           });
       })->download('xls');

    }

    public function Excel_ContainerSendStation($id)
    {
        $date = date('d-m-Y');
        $data['Company'] = \App\Models\Company::first();
        $data['LotProducts'] = \App\Models\LotProduct::select(
                'import_to_chaina.po_no'
                , 'lot_products.id as lot_product_id'
                , 'users.customer_general_code'
                , 'users.product_type_id as user_product_type_id'
                // , 'product_types.name as product_type_name'
                , 'products.name as product_name'
                , 'products.name_en as product_name_en'
                , 'product_import_to_chaina.pcs as product_pcs'
                , 'product_import_to_chaina.id as product_import_to_chaina_id'
                , 'transport_types.name_th as transport_type_name_th'
                , 'transport_types.name_en as transport_type_name_en'
                , 'transport_types.name_ch as transport_type_name_ch'
                , 'lot_products.width'
                , 'lot_products.height'
                , 'lot_products.length'
                , 'lot_products.weight_per_item'
                , 'lot_products.cubic as product_cobic'
                , 'lot_products.product_sort_end'
                , 'lot_products.product_sort_start'
                , 'lot_products.remark'
                , 'containers.container_code'
                , 'containers.tag'
                , 'containers.container_no'
                , 'containers.created_at as container_created_at'
                , \DB::raw('(`tb_lot_products`.`product_sort_end` - `tb_lot_products`.`product_sort_start`) + 1 as lot_product_qty')
                , \DB::raw('count(`tb_qr_code_products`.`id`) as qr_code_container_qty')
                , 'lot_products.created_at as product_created_at'
            )
            // join qr_code_products
            ->join('qr_code_products', 'qr_code_products.product_import_to_chaina_id', '=', 'lot_products.product_import_to_chaina_id')
            ->whereRaw('`tb_qr_code_products`.`sort_id` BETWEEN `tb_lot_products`.`product_sort_start` AND `tb_lot_products`.`product_sort_end`')
            //end join qr_code_products
            ->join('rel_container_products', 'rel_container_products.qr_code_product_id', '=', 'qr_code_products.id')
            ->join('product_import_to_chaina', 'product_import_to_chaina.id', '=', 'lot_products.product_import_to_chaina_id')
            ->join('import_to_chaina', 'import_to_chaina.id', '=', 'product_import_to_chaina.import_to_chaina_id')
            ->join('users', 'users.id', '=', 'import_to_chaina.user_id')
            ->join('products', 'products.id', '=', 'product_import_to_chaina.product_id')
            // ->join('product_types', 'product_types.id', '=', 'product_import_to_chaina.product_type_id')
            ->join('containers', 'containers.id', '=', 'rel_container_products.container_id')
            ->join('transport_types', 'transport_types.id', '=', 'product_import_to_chaina.transport_type_id')
            ->where('rel_container_products.container_id', $id)
            ->groupBy('lot_products.id')
            ->groupBy('import_to_chaina.po_no')
            ->groupBy('users.customer_general_code')
            ->groupBy('products.name')
            ->groupBy('product_import_to_chaina.pcs')
            ->groupBy('transport_types.name_th')
            ->groupBy('transport_types.name_en')
            ->groupBy('transport_types.name_ch')
            ->groupBy('containers.container_code')
            ->groupBy('lot_products.width')
            ->groupBy('lot_products.height')
            ->groupBy('lot_products.length')
            ->groupBy('lot_products.weight_per_item')
            ->groupBy('lot_products.cubic')
            ->groupBy('lot_products.product_sort_end')
            ->groupBy('lot_products.product_sort_start')
            ->groupBy('containers.created_at')
            ->groupBy('lot_products.remark')
            ->groupBy('lot_products.created_at')
            ->groupBy('users.product_type_id')
            ->orderBy('import_to_chaina.id')
            ->orderBy('product_import_to_chaina.id')
            ->orderBy('lot_products.product_sort_start')
            ->get();

        \Excel::create('Container-Send-Station-'.$date, function($excel) use ($data) {
           $excel->sheet('Container-Send-Station', function($sheet) use ($data) {
               $sheet->loadView('Admin.excel_container_send_station', $data);
           });
       })->download('xls');

    }

}
