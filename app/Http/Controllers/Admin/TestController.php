<?php

namespace App\Http\Controllers\Admin;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Storage;
class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['main_menu'] = 'Test';
        $data['sub_menu'] = 'Test';
        $data['title_page'] = 'Test';
        $data['menus'] = \App\Models\AdminMenu::ActiveMenu()->get();
        $data['CrudPermissions'] = \App\Models\CrudPermission::get();$data['AdminMenus'] = \App\Models\AdminMenu::get();
        return view('Admin.test',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input_all = $request->all();
        $input_all['firstname'] = \Hash::make($input_all['firstname']);$input_all['nickname'] = $request->input('nickname','2');
            if(isset($input_all['email'])){
                $input_all['email'] = str_replace(',', '', $input_all['email']);
            }

            if(isset($input_all['username'])){
                $input_all['username'] = str_replace(',', '', $input_all['username']);
            }

        $input_all['created_at'] = date('Y-m-d H:i:s');
        $input_all['updated_at'] = date('Y-m-d H:i:s');

        $validator = Validator::make($request->all(), [

        ]);
        if (!$validator->fails()) {
            \DB::beginTransaction();
            try {
                $data_insert = $input_all;
                \App\Models\AdminUser::insert($data_insert);
                \DB::commit();
                $return['status'] = 1;
                $return['content'] = 'สำเร็จ';
            } catch (Exception $e) {
                \DB::rollBack();
                $return['status'] = 0;
                $return['content'] = 'ไม่สำรเ็จ'.$e->getMessage();;
            }
        }else{
            $return['status'] = 0;
        }
        $return['title'] = 'เพิ่มข้อมูล';
        return json_encode($return);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = \App\Models\AdminUser::find($id);

        return json_encode($result);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input_all = $request->all();
        $input_all['nickname'] = $request->input('nickname','2');
            if(isset($input_all['email'])){
                $input_all['email'] = str_replace(',', '', $input_all['email']);
            }

            if(isset($input_all['username'])){
                $input_all['username'] = str_replace(',', '', $input_all['username']);
            }

        $input_all['updated_at'] = date('Y-m-d H:i:s');

        $validator = Validator::make($request->all(), [

        ]);
        if (!$validator->fails()) {
            \DB::beginTransaction();
            try {
                $data_insert = $input_all;
                \App\Models\AdminUser::where('id',$id)->update($data_insert);
                \DB::commit();
                $return['status'] = 1;
                $return['content'] = 'สำเร็จ';
            } catch (Exception $e) {
                \DB::rollBack();
                $return['status'] = 0;
                $return['content'] = 'ไม่สำรเ็จ'.$e->getMessage();;
            }
        }else{
            $return['status'] = 0;
        }
        $return['title'] = 'เพิ่มข้อมูล';
        return json_encode($return);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();
        try {
            \App\Models\AdminUser::where('id',$id)->delete();
            \DB::commit();
            $return['status'] = 1;
            $return['content'] = 'สำเร็จ';
        } catch (Exception $e) {
            \DB::rollBack();
            $return['status'] = 0;
            $return['content'] = 'ไม่สำรเ็จ'.$e->getMessage();;
        }
        $return['title'] = 'ลบข่อมูล';
        return $return;
    }

    public function Lists(){
        $result = \App\Models\AdminUser::select();
        return \Datatables::of($result)

        ->addColumn('email',function($rec){
            if(is_numeric($rec->email)){
                return number_format($rec->email);
            }else{
                return $rec->email;
            }
        })

        ->addColumn('username',function($rec){
            if(is_numeric($rec->username)){
                return number_format($rec->username,2);
            }else{
                return $rec->username;
            }
        })

        ->addColumn('action',function($rec){
            $str='
                <button data-loading-text="<i class=\'fa fa-refresh fa-spin\'></i>" class="btn btn-xs btn-warning btn-condensed btn-edit btn-tooltip" data-rel="tooltip" data-id="'.$rec->id.'" title="แก้ไข">
                    <i class="ace-icon fa fa-edit bigger-120"></i>
                </button>
                <button  class="btn btn-xs btn-danger btn-condensed btn-delete btn-tooltip" data-id="'.$rec->id.'" data-rel="tooltip" title="ลบ">
                    <i class="ace-icon fa fa-trash bigger-120"></i>
                </button>
            ';
            return $str;
        })->make(true);
    }

    public function getDataCSV()
    {
        return 1;
        // $strp = storage_path('app/public');
        // // return 1;
        // $file_n = $strp.'/Customer_Data_Basev2_cleaned.csv';
        // // $file_n = fopen($file_n, "r");
        // $file = fopen($file_n, "r");
        // $data_insert = [];
        // $i = 0;
        //
        // \DB::beginTransaction();
        // try {
        //
        //     while( !feof($file) ){
        //         $data_file_array = fgetcsv($file);
        //         if($i > 0 && !empty($data_file_array[2])){
        //             // print_r($data_file_array);
        //             // echo "<br>";
        //             // echo "<br>";
        //             // echo "<br>";
        //             $prefix = '';
        //             switch ($data_file_array[2]) {
        //                 case 'นาย':
        //                     $prefix = 2;
        //                     break;
        //                 case 'นาง':
        //                     $prefix = 1;
        //                     break;
        //                 case 'นางสาว':
        //                     $prefix = 3;
        //                     break;
        //                 default:
        //                     $prefix = 5;
        //             }
        //             $province = \App\Models\Province::where('province_name', 'like',  '%'.$data_file_array[9].'%')->first();
        //             echo $i;
        //             echo '<br>';
        //             $data_insert = [
        //                 'customer_general_code' => ($data_file_array[1] == 'ทั่วไป' ? NULL : $data_file_array[0] ),
        //                 'customer_brand_code' => ($data_file_array[1] == 'แบรนด์' ? $data_file_array[0] : NULL ),
        // 				'prefix_id' => $prefix,
        // 				'firstname' => ($data_file_array[3] == 'N/A' ? NULL : $data_file_array[3]),
        // 				'lastname' => ($data_file_array[4] == 'N/A' ? NULL : $data_file_array[4]),
        // 				'nickname' => $data_file_array[5],
        //                 'main_mobile' => $data_file_array[6],
        //                 'sub_mobile' => $data_file_array[7],
        //                 'address' => $data_file_array[8],
        //                 'province_id' => (!empty($province) ? $province->id : NULL),
        //                 'email' => $data_file_array[10],
        //                 'wechat_id' => $data_file_array[11],
        //                 'line_id' => $data_file_array[12],
        //                 'password' => \Hash::make($data_file_array[13]),
        //                 'product_type_id' => ($data_file_array[1] == 'ทั่วไป' ? 5 : 6 ),
        //                 'remark' => $data_file_array[14],
        // 				'created_at' => date('Y-m-d H:i:s'),
        // 			];
        //             $user_id = \App\Models\User::insertGetId($data_insert);
        //             $data_address_insert = [
        // 				'user_id' => $user_id,
        // 				'name' => ($data_file_array[3] == 'N/A' ? NULL : $data_file_array[3]) .' '. ($data_file_array[4] == 'N/A' ? NULL : $data_file_array[4]),
        //                 'address' => $data_file_array[8],
        // 				'province_id' => (!empty($province) ? $province->id : NULL),
        // 				'created_at' => date('Y-m-d H:i:s'),
        // 			];
        //             \App\Models\UserAddress::insert($data_address_insert);
        //         }
        //         $i++;
        //     }
        //     \DB::commit();
        //     $return['status'] = 1;
        //     $return['content'] = 'สำเร็จ';
        // } catch (Exception $e) {
        //     \DB::rollBack();
        //     $return['status'] = 0;
        //     $return['content'] = 'ไม่สำเร็จ'. $e->getMessage();
        // }
        // fclose($file);
        // return $return;

        // $strp = storage_path('app/public');
        // // return 1;
        // $file_n = $strp.'/Customer_Data_Basev2_cleaned.csv';
        // $file = fopen($file_n, "r");
        //
        // $all_data = array();
        // while ( ($data = fgetcsv($file, 200, ",")) !== FALSE ){
        //         print_r($data);
        //             echo "<br>";
        //     // $name = $data[0];
        //     // $city = $data[1];
        //     // $all_data = $name. " ".$city;
        //     //
        //     // array_push($array, $all_data);
        //  }
        //  fclose($file);
        //  return $all_data;
    }
}
