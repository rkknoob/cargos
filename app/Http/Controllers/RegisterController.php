<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RegisterController extends Controller
{
	public function register()
	{
		$data['Perfixes'] = \App\Models\Perfix::orderBy('name', 'asc')->get();
		$data['Provinces'] = \App\Models\Province::orderBy('province_name', 'asc')->get();
		return view('Frontend.register', $data);
	}

	public function AddRegister(Request $request)
	{
        $input_all = $request->all();

        $input_all['password'] = \Hash::make($input_all['password']);
        $input_all['created_at'] = date('Y-m-d H:i:s');
        $input_all['updated_at'] = date('Y-m-d H:i:s');

        $validator = Validator::make($request->all(), [
			'email' => 'required|unique:users',
        ]);
        if (!$validator->fails()) {
            \DB::beginTransaction();
            try {
                $data_insert = [
					'prefix_id' => $input_all['perfixe_id'],
					'firstname' => $input_all['firstname'],
					'lastname' => $input_all['lastname'],
					'email' => $input_all['email'],
					'password' => $input_all['password'],
					'address' => $input_all['address'],
					'province_id' => $input_all['province_id'],
					'amphur_id' => $input_all['amphure_names'],
					'main_mobile' => $input_all['main_mobile'],
					'product_type_id' => $input_all['product_type_id'],
					'created_at' => date('Y-m-d H:i:s'),
				];

                $user_id = \App\Models\User::insertGetId($data_insert);
                $data_address_insert = [
					'user_id' => $user_id,
					'name' => $input_all['firstname'] .' '. $input_all['lastname'],
					'address' => $input_all['address'],
					'province_id' => $input_all['province_id'],
					'amphur_id' => $input_all['amphure_names'],
					'zipcode' => $input_all['zipcode'],
					'created_at' => date('Y-m-d H:i:s'),
				];
                \App\Models\UserAddress::insert($data_address_insert);
                \DB::commit();
                $return['status'] = 1;
                $return['content'] = 'สำเร็จ';
            } catch (Exception $e) {
                \DB::rollBack();
                $return['status'] = 0;
                $return['content'] = 'ไม่สำเร็จ'. $e->getMessage();
            }
        }else{
            $return['status'] = 0;
			$return['content'] = 'Email นี้ถูกใช้ไปแล้วกรุณาลองใหม่';
        }
        $return['title'] = 'สมัครสมาชิก';
        return json_encode($return);
	}

	public function GetAmphure($province)
	{
		$Amphures = \App\Models\Amphure::orderBy('amphure_name', 'asc')
			->where('province_id', $province)
			->get();
		return json_encode($Amphures);
	}

}
