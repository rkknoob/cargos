<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NewsController extends Controller
{
	
	public function news()
	{
		$data['News'] = \App\Models\News::where('active', 'T')->paginate(9);

		return view('Frontend.news', $data);
	}

	public function news_detail($id)
	{
		$data['News'] = \App\Models\News::with('AdminUser')->where('id', $id)->first();
		
		return view('Frontend.news_details', $data);
	}
	
}