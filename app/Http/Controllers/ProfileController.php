<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Socialite;
use Auth;
use Hash;
use Validator;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{
    public function profile()
    {
        $user_id = Auth()->user()->id;
        $data['User'] = \App\Models\User::with('Province', 'Amphure')->where('id', $user_id)->first();
        $data['Prefixs'] = \App\Models\Perfix::get();
        $data['Amphures'] = \App\Models\Amphure::get();
        $data['Provinces'] = \App\Models\Province::get();

        return view('Frontend.profile', $data);
    }

    public function CheckPassword(Request $request) {
        $member = \App\Models\User::find(Auth::user()->id);
        if (Hash::check($request->old_password, $member->password)) {
            if(Hash::check($request->new_password, $member->password )){
                $return['status'] = 2;
                $return['content'] = 'ผิดพลาด รหัสใหม่ควรแตกต่างกับรหัสเก่า กรุณาตรวจสอบใหม่อีกครั้ง';
            }else{
                $data = array(
                    'password' => \Hash::make($request->new_password),
                    'updated_at' => date('Y-m-d H:i:s')
                );
                \App\Models\User::where('id', Auth::user()->id)->update($data);
                $return['status'] = 1;
                $return['content'] = 'เรียบร้อย';
            }
        } else {
            $return['status'] = 0;
            $return['content'] = 'ผิดพลาด รหัสเก่าไม่ตรงกับในระบบ กรุณาตรวจสอบใหม่อีกครั้ง';
        }
        $return['title'] = 'เปลี่ยนรหัสผ่าน';
        return json_encode($return);
    }

    public function EditProfile(Request $request)
    {
        $input_all = $request->all();
        $id = Auth()->user()->id;

        $input_all['updated_at'] = date('Y-m-d H:i:s');

        $validator = Validator::make($request->all(), [
            'firstname' => 'required',
            'lastname' => 'required',
            'main_mobile' => 'required',
            'province_id' => 'required',
            'amphur_id' => 'required',

        ]);
        if (!$validator->fails()) {
            \DB::beginTransaction();
            try {
                $data_insert = $input_all;
                \App\Models\User::where('id',$id)->update($data_insert);
                \DB::commit();
                $return['status'] = 1;
                $return['content'] = 'สำเร็จ';
            } catch (Exception $e) {
                \DB::rollBack();
                $return['status'] = 0;
                $return['content'] = 'ไม่สำเร็จ'.$e->getMessage();
            }
        }else{
            $return['status'] = 0;
        }
        $return['title'] = 'แก้ไขข้อมูลส่วนตัว';
        return json_encode($return);
    }

    public function AddAddress(Request $request)
    {
        $input_all = $request->all();
        $input_all['created_at'] = date('Y-m-d H:i:s');
        $input_all['updated_at'] = date('Y-m-d H:i:s');

        $validator = Validator::make($request->all(), [

        ]);
        if($input_all['id']  == null){
            unset($input_all['id']);
        }
        if (!$validator->fails()) {
            \DB::beginTransaction();
            try {
                $data_insert = $input_all;
                if(isset($data_insert['id'])){
                    \App\Models\UserAddress::where('id',$data_insert['id'])->update($data_insert);
                }else{
                    \App\Models\UserAddress::insert($data_insert);
                }

                \DB::commit();
                $return['status'] = 1;
                $return['content'] = 'สำเร็จ';
            } catch (Exception $e) {
                \DB::rollBack();
                $return['status'] = 0;
                $return['content'] = 'ไม่สำเร็จ'.$e->getMessage();
            }
        }else{
            $return['status'] = 0;
        }
        $return['title'] = 'เพิ่มข้อมูล';
        return json_encode($return);
    }

    public function AddressDelete($id)
    {
        \DB::beginTransaction();
        try {
            \App\Models\UserAddress::where('id',$id)->delete();
            \DB::commit();
            $return['status'] = 1;
            $return['content'] = 'สำเร็จ';
        } catch (Exception $e) {
            \DB::rollBack();
            $return['status'] = 0;
            $return['content'] = 'ไม่สำเร็จ'.$e->getMessage();
        }
        $return['title'] = 'ลบข้อมูล';
        return $return;
    }

    public function ListsAddress(Request $request)
    {
        $id = $request->input('id');
        $result = \App\Models\UserAddress::with('Province', 'Amphure')->where('user_id', $id)->select();
        $result = \App\Models\UserAddress::select(
                'user_addresses.*'
                , 'amphures.amphure_name'
                , 'provinces.province_name'
            )
            ->leftjoin('amphures', 'amphures.id', '=', 'user_addresses.amphur_id')
            ->leftjoin('provinces', 'provinces.id', '=', 'user_addresses.province_id')
            ->where('user_addresses.user_id', $id);
        return \Datatables::of($result)

        ->addColumn('action',function($rec){
            $str='
                <button data-loading-text="<i class=\'fa fa-refresh fa-spin\'></i>" class="btn btn-xs btn-warning btn-condensed btn-address-edit btn-tooltip" data-rel="tooltip"
                    data-id="'.$rec->id.'"title="'.trans('lang.edit').'"><i class="ace-icon fa fa-edit bigger-120"></i>
                </button>
                <button  class="btn btn-xs btn-danger btn-condensed btn-address-delete btn-tooltip" data-id="'.$rec->id.'" data-rel="tooltip" title="'.trans('lang.delete').'">
                    <i class="ace-icon fa fa-trash bigger-120"></i>
                </button>
            ';
            return $str;
        })->rawColumns(['action'])->addIndexColumn()->make(true);
    }

    public function EditAddress($id)
    {
        $data['UserAddress'] = \App\Models\UserAddress::find($id);
        $data['Amphures']    = \App\Models\Amphure::where('province_id', $data['UserAddress']->province_id)->get();

        return $data;
    }

}
