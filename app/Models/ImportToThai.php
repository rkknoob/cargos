<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ImportToThai extends Model
{
    protected $table = 'import_to_thai';

    public function Container()
    {
        return $this->hasOne('\App\Models\Container', 'id', 'container_id');
    }

    public function ImportToChaina()
    {
        return $this->hasOne('\App\Models\ImportToChaina', 'id', 'import_to_chaina_id');
    }

    public function ProductImportToThai()
    {
        return $this->hasMany('\App\Models\ProductImportToThai', 'import_to_thai_id', 'id');
    }

}
