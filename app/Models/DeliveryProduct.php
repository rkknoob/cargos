<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryProduct extends Model
{
    //protected $table = '';
    //protected $primaryKey = '';
    //public $timestamps = true;

    public function QrCodeProduct()
    {
        return $this->hasOne('\App\Models\QrCodeProduct', 'id', 'qr_code_product_id');
    }

    public function DeliveryToUser()
    {
        return $this->hasOne('\App\Models\DeliveryToUser', 'id', 'delivery_to_user_id');
    }
}
