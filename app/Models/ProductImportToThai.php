<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductImportToThai extends Model
{
    protected $table = 'product_import_to_thai';

    public function ImportToThai()
    {
        return $this->hasOne('\App\models\ImportToThai', 'id', 'import_to_thai_id');
    }

    public function QrCodeProduct()
    {
        return $this->hasOne('\App\Models\QrCodeProduct', 'id', 'qr_code_product_id');
    }
}
