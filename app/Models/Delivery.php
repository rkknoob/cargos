<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{
    //protected $table = '';
    //protected $primaryKey = '';
    //public $timestamps = true;

    public function DeliveryStatus()
    {
        return $this->hasOne('\App\Models\DeliveryStatus', 'id', 'status_id');
    }

    public function DeliveryToUser()
    {
        return $this->hasMany('\App\Models\DeliveryToUser', 'delivery_id', 'id');
    }

}
