<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BillingList extends Model
{
    public function Billing()
    {
        return $this->hasOne('\App\Models\Billing', 'id', 'billing_id');
    }

    public function DeliverySlip()
    {
        return $this->hasOne('\App\Models\DeliverySlip', 'id', 'delivery_slip_id');
    }
}
