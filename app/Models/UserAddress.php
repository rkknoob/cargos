<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAddress extends Model
{
    public function Province()
    {
        return $this->hasOne('\App\Models\Province', 'id', 'province_id');
    }

    public function Amphure()
    {
        return $this->hasOne('\App\Models\Amphure', 'id', 'amphur_id');
    }
}
