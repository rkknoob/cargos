<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DaliverySheet extends Model
{
    //protected $table = '';
    //protected $primaryKey = '';
    //public $timestamps = true;

    public function User()
    {
    	return $this->hasOne('\App\Models\User', 'id', 'user_id');
    }

    public function DeliveryPos()
    {
    	return $this->hasMany('\App\Models\DeliveryPos', 'delivery_sheet_id', 'id');
    }

    public function Province()
    {
        return $this->hasOne('\App\Models\Province', 'id', 'province_id');
    }

    public function Amphure()
    {
        return $this->hasOne('\App\Models\Amphure', 'id', 'amphur_id');
    }
}
