<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QrCodeProduct extends Model
{
    public function ImportToChaina()
    {
        return $this->hasOne('\App\Models\ImportToChaina', 'id', 'import_to_chaina_id');
    }

    public function ProductImportToChaina()
    {
        return $this->hasOne('\App\Models\ProductImportToChaina', 'id', 'product_import_to_chaina_id');
    }

    public function RelContainerProduct()
    {
        return $this->hasMany('\App\Models\RelContainerProduct', 'qr_code_product_id', 'id');
    }

    public function RelUserTypeProduct()
    {
        return $this->hasOne('\App\Models\RelUserTypeProduct', 'id', 'rel_user_type_product_id');
    }
    public function RelUserTypeProductOld()
    {
        return $this->hasOne('\App\Models\RelUserTypeProduct', 'id', 'rel_user_type_product_id_old');
    }

    // public function LotProduct()
    // {
    //     return $this->hasOne('\App\Models\LotProduct', 'id', 'lot_product_id');
    // }


}
