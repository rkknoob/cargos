<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Billing extends Model
{
    //protected $table = '';
    //protected $primaryKey = '';
    //public $timestamps = true;

    public $Status = [
        'T' => 'การขนส่ง',
        'F' => 'ยังไม่ปิดบิล'
    ];

    public function User()
    {
        return $this->hasOne('\App\Models\User', 'id', 'user_id');
    }

    public function BillingList()
    {
        return $this->hasMany('\App\Models\BillingList', 'billing_id', 'id');
    }
}
