<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductImportToChaina extends Model
{
    protected $table = 'product_import_to_chaina';

    public function Product()
    {
        return $this->hasOne('\App\Models\Product', 'id', 'product_id');
    }

    public function ProductType()
    {
        return $this->hasOne('\App\Models\ProductType', 'id', 'product_type_id');
    }

    public function LotProduct()
    {
        return $this->hasMany('\App\Models\LotProduct', 'product_import_to_chaina_id', 'id');
    }

    public function TransportType()
    {
        return $this->hasOne('\App\Models\TransportType', 'id', 'transport_type_id');
    }

    public function ImportToChaina()
    {
        return $this->hasOne('\App\Models\ImportToChaina', 'id', 'import_to_chaina_id');
    }

    public function ContainerType()
    {
        return $this->hasOne('\App\Models\ContainerType', 'id', 'container_type_id');
    }

    public function QrCodeProductPrice()
    {
        return $this->hasOne('\App\Models\QrCodeProduct', 'product_import_to_chaina_id', 'id');
    }
}
