<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryPos extends Model
{
    //protected $table = '';
    //protected $primaryKey = '';
    //public $timestamps = true;

    public function ImportToChaina()
    {
    	return $this->hasOne('\App\Models\ImportToChaina', 'id', 'import_to_chaina_id');
    }

    public function DaliverySheet()
    {
    	return $this->hasOne('\App\Models\DaliverySheet', 'id', 'delivery_sheet_id');
    }
}
