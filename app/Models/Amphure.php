<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Amphure extends Model
{
    protected $table = 'amphures';
    //protected $primaryKey = '';
    //public $timestamps = true;

    public function Province()
    {
        return $this->hasOne('\App\Models\Province', 'id', 'province_id');
    }
}
