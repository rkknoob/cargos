<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DateTime;

class ImportToChaina extends Model
{
    protected $table = 'import_to_chaina';
    //protected $primaryKey = '';
    //public $timestamps = true;

    public function User()
    {
        return $this->hasOne('\App\Models\User', 'id', 'user_id');
    }

    public function ImportToChainaStatus()
    {
        return $this->hasOne('\App\Models\ImportToChainaStatus', 'id', 'status_id');
    }

    public function ProductImportToChaina()
    {
        return $this->hasMany('\App\Models\ProductImportToChaina', 'import_to_chaina_id', 'id');
    }

    public function QrCodeProduct()
    {
        return $this->hasMany('\App\Models\QrCodeProduct', 'import_to_chaina_id', 'id');
    }

    public function ImportToThai()
    {
        return $this->hasMany('\App\Models\ImportToThai', 'import_to_chaina_id', 'id');
    }

    public function DeliveryToUser()
    {
        return $this->hasMany('\App\Models\DeliveryToUser', 'import_to_chaina_id', 'id');
    }

    public function ImportToChainaDate()
    {
        $temp = new DateTime($this->date_import);

        return (($temp) ? $temp : null);
    }

    public function UpdatedAt()
    {
        $temp = new DateTime($this->updated_at);

        return (($temp) ? $temp : null);
    }
}
