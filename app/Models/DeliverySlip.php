<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliverySlip extends Model
{
    public $StatusPayment = [
        'T' => 'ชำระเสร็จเรียบร้อย',
        'F' => 'ยังไม่การชำระ',
    ];

    public function Delivery()
    {
        return $this->hasOne('\App\Models\Delivery', 'id', 'delivery_id');
    }

    public function DeliverySlip()
    {
        return $this->hasOne('\App\Models\DeliverySlip', 'id', 'delivery_id');
    }

    public function DeliverySlipStatus()
    {
        return $this->hasOne('\App\Models\DeliverySlipStatus', 'id', 'status_id');
    }

    public function User()
    {
        return $this->hasOne('\App\Models\User', 'id', 'user_id');
    }

    public function UserAddress()
    {
        return $this->hasOne('\App\Models\UserAddress', 'id', 'user_address_id');
    }

    public function DeliverySlipList()
    {
        return $this->hasMany('\App\Models\DeliverySlipList', 'delivery_slip_id', 'id');
    }

    public function BillingList()
    {
        return $this->hasMany('\App\Models\BillingList', 'delivery_slip_id', 'id');
    }


}
