<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function ProductType()
    {
        return $this->hasOne('\App\Models\ProductType', 'id', 'product_type_id');
    }
}
