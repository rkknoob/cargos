<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SendSMS extends Model
{
    protected $table = 'send_sms';
}
