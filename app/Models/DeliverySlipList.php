<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliverySlipList extends Model
{
    public function DeliverySlip()
    {
        return $this->hasOne('\App\Models\DeliverySlip', 'id', 'delivery_slip_id');
    }

    public function DeliveryToUser()
    {
        return $this->hasOne('\App\Models\DeliveryToUser', 'id', 'delivery_to_user_id');
    }
}
