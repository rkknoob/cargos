<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Container extends Model
{
    public function TransportType()
    {
        return $this->hasOne('\App\Models\TransportType', 'id', 'transport_type_id');
    }

    public function ContainerType()
    {
        return $this->hasOne('\App\Models\ContainerType', 'id', 'container_type_id');
    }
    public function ContainerSize()
    {
        return $this->hasOne('\App\Models\ContainerSize', 'id', 'container_size_id');
    }

    public function ImportToThai()
    {
        return $this->hasMany('\App\Models\ImportToThai', 'container_id', 'id');
    }

    public function RelContainerProduct()
    {
        return $this->hasMany('\App\Models\RelContainerProduct', 'container_id', 'id');
    }

    public function ContainerStatus()
    {
        return $this->hasOne('App\Models\ContainerStatus', 'id', 'status_id');
    }
}
