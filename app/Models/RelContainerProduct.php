<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RelContainerProduct extends Model
{
    public function Container()
    {
        return $this->hasOne('\App\Models\Container', 'id', 'container_id');
    }

    public function QrCodeProduct()
    {
        return $this->hasOne('\App\Models\QrCodeProduct', 'id', 'qr_code_product_id');
    }
}
