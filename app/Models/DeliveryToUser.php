<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryToUser extends Model
{
    //protected $table = '';
    //protected $primaryKey = '';
    //public $timestamps = true;

    public function Delivery(){
        return $this->hasOne('\App\Models\Delivery', 'id', 'delivery_id');
    }

    public function ImportToChaina()
    {
        return $this->hasOne('\App\Models\ImportToChaina', 'id', 'import_to_chaina_id');
    }

    public function ImportToThai()
    {
        return $this->hasOne('\App\Models\ImportToThai', 'import_to_chaina_id', 'import_to_chaina_id');
    }

    public function User()
    {
        return $this->hasOne('\App\Models\User', 'id', 'user_id');
    }

    public function DeliveryProduct()
    {
        return $this->hasMany('\App\Models\DeliveryProduct', 'delivery_to_user_id', 'id');
    }
}
