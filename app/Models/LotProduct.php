<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LotProduct extends Model
{
        public function ProductImportToChaina()
        {
            return $this->hasOne('\App\Models\ProductImportToChaina', 'id', 'product_import_to_chaina_id');
        }
}
