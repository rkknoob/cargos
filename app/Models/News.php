<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use File;

class News extends Model
{
    //protected $table = '';
    //protected $primaryKey = '';
    //public $timestamps = true;

    public function AdminUser()
    {
    	return $this->hasOne('\App\Models\AdminUser', 'id', 'admin_id');
    }

	public function pathImage()
    {
        return 'uploads/News/';
    }

    public function viewImage()
    {
        $image  = $this->pathImage().'no-image.jpg';
        if ($this->image != null) {
            if (File::exists($this->pathImage().$this->image)) {
                $image = $this->pathImage().$this->image;
            }
        }
        return $image;
    }
}
