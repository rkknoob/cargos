<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    //protected $table = '';
    //protected $primaryKey = '';
    //protected $timestamps = true;

    public function ProductType()
    {
        return $this->hasOne('\App\Models\ProductType', 'id', 'product_type_id');
    }

    public function Province()
    {
        return $this->hasOne('\App\Models\Province', 'id', 'province_id');
    }

    public function Amphure()
    {
        return $this->hasOne('\App\Models\Amphure', 'id', 'amphur_id');
    }
}
